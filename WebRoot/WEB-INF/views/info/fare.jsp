<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<script src="js/jquery.js"></script>

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">

<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>

<!-- gy 添加表单验证 -->
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />
<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">
		<!-- 查询列表 -->
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>输入条件查询运价信息</h5>
				</div>
				<div class="ibox-content">
					<form action="getFare" class="form-horizontal" method="post">
						<div class="row">
							<div class="form-group">
								<label class="col-sm-2 control-label">运价类型：</label>
								<div class="col-sm-5">
									<select class="form-control" name="type">
										<option>国有企业</option>
										<option>外资企业</option>
										<option>合资企业</option>
										<option>民营企业</option>
									</select>
								</div>
								<div class="col-sm-3">
									<button type="submit" class="btn btn-primary">查询</button>
									&nbsp;&nbsp;&nbsp;&nbsp;

									<shiro:hasPermission name="rate:admin">
										<button id="btn_addFare" type="button" class="btn btn-primary"
											data-toggle="modal" data-target="#myModal5">添加</button>
									</shiro:hasPermission>

								</div>
							</div>
						</div>
					</form>

				</div>
			</div>
		</div>

		<!--gy 修改后 点击添加按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal5" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form id="saveFareForm" class="form-horizontal" action="saveFare"
						method="post">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>新增运价信息  <span style="color:red">（* 为必填项）</span></h5>
							</div>
							<div class="ibox-content">
								<!-- 第一行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 运价类型 -->
											<label class="col-sm-5 control-label"><span style="color:red">*  </span>运价类型 ：</label>
											<div class="col-sm-7">
												<select class="form-control" name="type">
													<option>国有企业</option>
													<option>外资企业</option>
													<option>合资企业</option>
													<option>民营企业</option>
												</select>
											</div>
										</div>
									</div>
									<!-- 运价有效期起 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span style="color:red">*  </span>运价有效期起 ：</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="startTime" autocomplete="off"
													onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(saveFareForm, name.valueOf())}})">
											</div>
										</div>
									</div>
								</div> 
								<!-- 第二行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 运价有效期止 -->
											<label class="col-sm-5 control-label"><span style="color:red">*  </span>运价有效期止 ：</label>
											<div class="col-sm-7">
												<input id="d4322" type="text" class="form-control" autocomplete="off"
													name="endTime" onfocus="WdatePicker({ dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(saveFareForm, name.valueOf())}})">
											</div>
										</div>
									</div>
									<!-- 昼间起步价-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span style="color:red">*  </span>昼间起步价 ：</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="dayStart" autocomplete="off">
											</div>
										</div>
									</div>
								</div>
								<!-- 第三行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 夜间起步价-->
											<label class="col-sm-5 control-label"><span style="color:red">*  </span>夜间起步价 ：</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="nightStart" autocomplete="off">
											</div>
										</div>
									</div>
									<!-- 昼间单价-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span style="color:red">*  </span>昼间单价 ：</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="dayPrice" autocomplete="off">
											</div>
										</div>
									</div>
								</div>
								<!-- 第四行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 夜间单价-->
											<label class="col-sm-5 control-label"><span style="color:red">*  </span>夜间单价 ：</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="nightPrice" autocomplete="off">
											</div>
										</div>
									</div>
									<!-- 起程-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span style="color:red">*  </span>起程 ：</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="fareLeave" autocomplete="off">
											</div>
										</div>
									</div>
								</div>
								<!-- 第五行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 低速-->
											<label class="col-sm-5 control-label"><span style="color:red">*  </span>低速 ：</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="lowVelocity" autocomplete="off">
											</div>
										</div>
									</div>
									<!-- 加价-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span style="color:red">*  </span>加价 ：</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="addPrice" autocomplete="off">
											</div>
										</div>
									</div>
								</div>
								<!-- 第五行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 回空-->
											<label class="col-sm-5 control-label"><span style="color:red">*  </span>回空 ：</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="haulback" autocomplete="off">
											</div>
										</div>
									</div>
									<!-- 等候-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label">等候 ：</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="wait" autocomplete="off">
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-white"
										data-dismiss="modal">关闭</button>
									<button type="submit" class="btn btn-primary">保存</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!--gy 修改前 点击添加按钮弹出来的form表单 -->
		<!-- <div class="modal inmodal fade" id="myModal5555" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form id="saveFareForm" action="saveFare" method="post">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>新增运价信息</h5>
							</div>
							<div class="ibox-content">
								<div class="row">
									<div class="col-sm-4">
										运价类型： <select name="type">
											<option>国有企业</option>
											<option>外资企业</option>
											<option>合资企业</option>
											<option>民营企业</option>
										</select>
									</div>
									<div class="col-sm-4">
										运价有效期起：<input type="text" readonly="readonly" name="startTime"
											onFocus="WdatePicker({maxDate:'#F{$dp.$D(\'d4322\',{d:0});}'})">
									</div>
									<div class="col-sm-4">
										运价有效期止：<input id="d4322" type="text" readonly="readonly"
											name="endTime" onfocus="WdatePicker({maxDate:'%y-%M-%d'})">
									</div>
								</div>
								<br>

								<div class="row">
									<div class="col-sm-4">
										昼间起步价：<input type="text" name="dayStart">
									</div>
									<div class="col-sm-4">
										夜间起步价：<input type="text" name="nightStart">
									</div>
									<div class="col-sm-4">
										昼间单价：<input type="text" name="dayPrice">
									</div>
								</div>
								<br>

								<div class="row">
									<div class="col-sm-4">
										夜间单价：<input type="text" name="nightPrice">
									</div>
									<div class="col-sm-4">
										起程：<input type="text" name="fareLeave">
									</div>
									<div class="col-sm-4">
										低速：<input type="text" name="lowVelocity">
									</div>
								</div>
								<br>

								<div class="row">
									<div class="col-sm-4">
										加价：<input type="text" name="addPrice">
									</div>
									<div class="col-sm-4">
										回空：<input type="text" name="haulback">
									</div>
									<div class="col-sm-4">
										等候：<input type="text" name="wait">
									</div>
								</div>
								<br>

							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
							<button type="submit" class="btn btn-primary">保存</button>
						</div>
					</form>
				</div>
			</div>
		</div> -->

		<!-- 数据展示 -->
		<c:if test="${fares!=null}">
			<div class="row">
			<div class="col-sm-12">

				<div class="ibox-title">
					<div>
						<h5>&nbsp;&nbsp;&nbsp;运价信息表</h5>
					</div>
				</div>
				<div class="ibox-content" style="height: 660px">

					<table id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
						<thead>
							<tr>
								<th>运价类型</th>
								<th>运价有效期起</th>
								<th>运价有效期止</th>
								<th>昼间起步价</th>
								<th>夜间起步价</th>
								<!-- 5 -->

								<th>起程</th>
								<th>昼间单价</th>
								<th>夜间单价</th>
								<th>加价</th>
								<th>等候</th>
								<!-- 10 -->

								<th>回空</th>
								<th>低速</th>
								<!-- 12 -->

								<th width="100">操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${fares}" var="fares">
								<tr>
									<td>${fares.type }</td>
									<td>${fares.startTime }</td>
									<td>${fares.endTime }</td>
									<td>${fares.dayStart }</td>
									<td>${fares.nightStart }</td>
									<!-- 5 -->

									<td>${fares.fareLeave }</td>
									<td>${fares.dayPrice }</td>
									<td>${fares.nightPrice }</td>
									<td>${fares.addPrice }</td>
									<td>${fares.wait }</td>
									<!-- 10 -->

									<td>${fares.haulback }</td>
									<td>${fares.lowVelocity }</td>
									<td><shiro:hasPermission name="rate:admin">
											<button id="btn-update" type="button"
												class="btn btn-success btn-sm" data-toggle="modal"
												data-target="#myModal1"
												onclick="update('${fares.id }','${fares.type }','${fares.startTime}','${fares.endTime}','${fares.dayStart}','${fares.nightStart}'
									,'${fares.fareLeave}','${fares.dayPrice}','${fares.nightPrice}','${fares.addPrice}','${fares.wait}'
									,'${fares.haulback}','${fares.lowVelocity}')">修改</button>
											<button type="button" class="btn btn-danger btn-sm"
												onclick="delete1('${fares.id}','${fares.type}')">删除</button>
										</shiro:hasPermission></td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="13">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		</c:if>
		<!-- 删除表格 -->
		<form id="deleteForm" action="deleteFare" method="post">
			<input type="hidden" id="deleteId" name="id">
		</form>

		<!-- 点击"修改"按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal1" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form action="updateFare" id="updateFareForm" class="form-horizontal"   method="post">
						<input id="id" name="id" type="hidden">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>修改运价信息  <span style="color:red">（* 为必填项）</span></h5>
							</div>
							
							<div class="ibox-content">
							<fieldset>
								<!-- 第一行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 运价类型 -->
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>运价类型 ：</label>
											<div class="col-sm-8">
												<select class="form-control" id="type" name="type">
													<option>国有企业</option>
													<option>外资企业</option>
													<option>合资企业</option>
													<option>民营企业</option>
												</select>
											</div>
										</div>
									</div>
									<!-- 运价有效期起 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>运价有效期起 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="startTime" name="startTime"
													onFocus="WdatePicker({ dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(updateFareForm, name.valueOf())}})">
											</div>
										</div>
									</div>
								</div>
								<br>
								<!-- 第二行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 运价有效期止 -->
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>运价有效期止 ：</label>
											<div class="col-sm-8">
												<input  type="text" class="form-control"
													name="endTime" id="endTime" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(updateFareForm, name.valueOf())}})">
											</div>
										</div>
									</div>
									<!-- 昼间起步价-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>昼间起步价 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="dayStart" name="dayStart">
											</div>
										</div>
									</div>
								</div>
								
								<!-- 第三行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 夜间起步价-->
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>夜间起步价 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="nightStart" name="nightStart">
											</div>
										</div>
									</div>
									<!-- 昼间单价-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>昼间单价 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="dayPrice" name="dayPrice">
											</div>
										</div>
									</div>
								</div>
								
								<!-- 第四行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 夜间单价-->
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>夜间单价 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="nightPrice" name="nightPrice">
											</div>
										</div>
									</div>
									<!-- 起程-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>起程 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="fareLeave" name="fareLeave">
											</div>
										</div>
									</div>
								</div>
								
								<!-- 第五行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 低速-->
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>低速 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="lowVelocity" name="lowVelocity">
											</div>
										</div>
									</div>
									<!-- 加价-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>加价 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="addPrice" name="addPrice">
											</div>
										</div>
									</div>
								</div>
							
								<!-- 第五行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 回空-->
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>回空 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="haulback" name="haulback">
											</div>
										</div>
									</div>
									<!-- 等候-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label">等候 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="wait" name="wait">
											</div>
										</div>
									</div>
								</div>
								
								</fieldset>
								<div class="modal-footer">
									<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
									<button type="submit" class="btn btn-primary">保存</button>
								</div>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<!-- <script src="js/jquery.min.js?v=2.1.4"></script> -->
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script>
	$(document).ready(function() {
		$('#table1').DataTable({
			"pagingType" : "full_numbers"
		});
	});

		//点击"修改"按钮
		function update(id, type, startTime, endTime, dayStart, nightStart,
				fareLeave, dayPrice, nightPrice, addPrice, wait, haulback,
				lowVelocity) {
			$("#id").val(id);
			$("#type").val(type);
			$("#startTime").val(startTime);
			$("#endTime").val(endTime);
			$("#dayStart").val(dayStart);
			$("#nightStart").val(nightStart);

			$("#fareLeave").val(fareLeave);
			$("#dayPrice").val(dayPrice);
			$("#nightPrice").val(nightPrice);
			$("#addPrice").val(addPrice);
			$("#wait").val(wait);

			$("#haulback").val(haulback);
			$("#lowVelocity").val(lowVelocity);
		};

		//点击"删除"按钮
		function delete1(id, name) {
			swal({
				title : "您确定删除该条数据吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
				$("#deleteId").val(id);
				//提交表单
				$("#deleteForm").submit();
			});
		}
		$(document).ready(function(){
			var result='<%=request.getAttribute("result")%>';
				if(result != null && result != "null"){
				if (result.indexOf("成功")>0) {
				
				swal({
					title : result,
					type : "success"
				});
			}else{
				swal({
					title :result,
					type : "error"
				});
			}
			}
		});
	</script>

	<!-- gy 添加界面表单验证 -->
	<script type="text/javascript">
		/*添加公司界面的表单验证*/
		$('#btn_addFare').click(function() {
			$('#saveFareForm').bootstrapValidator({
				feedbackIcons : {/*输入框不同状态，显示图片的样式*/
					valid : 'glyphicon glyphicon-ok',
					invalid : 'glyphicon glyphicon-remove',
					validating : 'glyphicon glyphicon-refresh'
				},
				/*生效规则：字段值一旦变化就触发验证*/
				live : 'enabled',
				/*当表单验证不通过时，该按钮为disabled*/
				submitButtons : 'button[type="submit"]',
				/*验证*/
				fields : {
					startTime : {/*键名username和input name值对应*/
						message : 'The username is not valid',
						validators : {
							notEmpty : {/*非空提示*/
								message : '运价有效期起不能为空'
							}
						}
					},
					endTime : {
						message : '',
						validators : {
							notEmpty : {
								message : '运价有效期止不能为空'
							}
						}
					},
					dayStart : {
						validators : {
							notEmpty : {
								message : '昼间起步价不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					nightStart : {
						validators : {
							notEmpty : {
								message : '夜间间起步价不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					dayPrice : {
						validators : {
							notEmpty : {
								message : '昼间单价不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					nightPrice : {
						validators : {
							notEmpty : {
								message : '夜间单价不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					fareLeave : {
						validators : {
							notEmpty : {
								message : '起程不能为空'
							}
						}
					},
					lowVelocity : {
						validators : {
							notEmpty : {
								message : '低速不能为空'
							}
						}
					},
					addPrice : {
						validators : {
							notEmpty : {
								message : '加价不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					haulback : {
						validators : {
							notEmpty : {
								message : '回空不能为空'
							}
						}
					}

				}
			});
		});
		$(document).ready(function () {
			$('#updateFareForm').bootstrapValidator({
				feedbackIcons : {/*输入框不同状态，显示图片的样式*/
					valid : 'glyphicon glyphicon-ok',
					invalid : 'glyphicon glyphicon-remove',
					validating : 'glyphicon glyphicon-refresh'
				},
				/*生效规则：字段值一旦变化就触发验证*/
				live : 'enabled',
				/*当表单验证不通过时，该按钮为disabled*/
				submitButtons : 'button[type="submit"]',
				/*验证*/
				fields : {
					startTime : {/*键名username和input name值对应*/
						message : 'The username is not valid',
						validators : {
							notEmpty : {/*非空提示*/
								message : '运价有效期起不能为空'
							}
						}
					},
					endTime : {
						message : '',
						validators : {
							notEmpty : {
								message : '运价有效期止不能为空'
							}
						}
					},
					dayStart : {
						validators : {
							notEmpty : {
								message : '昼间起步价不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					nightStart : {
						validators : {
							notEmpty : {
								message : '夜间间起步价不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					dayPrice : {
						validators : {
							notEmpty : {
								message : '昼间单价不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					nightPrice : {
						validators : {
							notEmpty : {
								message : '夜间单价不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					fareLeave : {
						validators : {
							notEmpty : {
								message : '起程不能为空'
							}
						}
					},
					lowVelocity : {
						validators : {
							notEmpty : {
								message : '低速不能为空'
							}
						}
					},
					addPrice : {
						validators : {
							notEmpty : {
								message : '加价不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					haulback : {
						validators : {
							notEmpty : {
								message : '回空不能为空'
							}
						}
					}

				}
			});
		});

		/*日历刷新验证*/
		function refreshValidator(id, name) {
			$(id).data('bootstrapValidator').updateStatus(name,
					'NOT_VALIDATED', null).validateField(name);
		}
	</script>
</body>

</html>