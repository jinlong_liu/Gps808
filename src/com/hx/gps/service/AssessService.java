package com.hx.gps.service;

import java.util.List;

import com.hx.gps.entities.Asscycle;
import com.hx.gps.entities.Complaints;
import com.hx.gps.entities.Comptype;
import com.hx.gps.entities.Deduct;
import com.hx.gps.entities.Evalscore;
import com.hx.gps.entities.Scorerank;
import com.hx.gps.entities.tool.EvalVector;

public interface AssessService {
	
	//添加投诉信息
	public String addComplaint(Complaints complaints, Integer driver, Integer taxi, Integer reportType);
	
	//根据id删除投诉记录
	public boolean deleteComById(Integer id);
	
	//获取投诉类型
	public List<Comptype> getCompType();
	
	//根据传入的type来查询不同的投诉信息
	public List<Complaints> getCompByType(Integer type);
	
	//修改投诉的状态
	public boolean alterTypeOfAssess(Integer compId,Integer type,Integer initType);
	
	//保存投诉处理方案，但并不执行
	public boolean addDeduct(Deduct deduct);
	
	//投诉审批处理
	public boolean approvalComp(Integer compId,Integer state,String apprOpinion);
	
	//添加投诉类型
	public void addOrUpdateCompType(Comptype compType);
	
	//删除投诉类型
	public boolean deleteCompType(Integer id);
	
	//获取审核周期信息
	public List<Asscycle> getAsscycle();
	
	//更新审核周期信息
	public void updateAssCycle(Asscycle asscycle);
	
	//获取分数等级对应表的信息
	public List<Scorerank> getScorerank();
	
	//更新或添加分数等级对应信息
	public void addOrUpScorerank(Scorerank scorerank);
	
	//删除Scorerank
	public void deleteScorerank(Integer id);
	
	//获取评价分数对应信息
	public List<Evalscore> getEvalscore();
	
	//添加或更新评价分数对应信息
	public void addOrUpdateEval(Evalscore evalscore);
	
	//删除评价分数对应信息
	public void delectEval(Integer id);
	
	//查看当期评价
	public List<EvalVector> queryCurrAssess();
	//删除考核周期
	public void deleteAsscycleById(Integer id);
	//获取全部投诉信息
	public List<Complaints> getAllCompliant();
	
}

