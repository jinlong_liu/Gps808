package com.hx.gps.entities;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Accidenttype entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "accidenttype")
public class Accidenttype implements java.io.Serializable {

	// Fields

	private Integer id;
	private String accidentType;
	private Set<Accident> accidents = new HashSet<Accident>(0);

	// Constructors

	/** default constructor */
	public Accidenttype() {
	}

	/** full constructor */
	public Accidenttype(String accidentType, Set<Accident> accidents) {
		this.accidentType = accidentType;
		this.accidents = accidents;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "accidentType")
	public String getAccidentType() {
		return this.accidentType;
	}

	public void setAccidentType(String accidentType) {
		this.accidentType = accidentType;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "accidenttype")
	public Set<Accident> getAccidents() {
		return this.accidents;
	}

	public void setAccidents(Set<Accident> accidents) {
		this.accidents = accidents;
	}

}