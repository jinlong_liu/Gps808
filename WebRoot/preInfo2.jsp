<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">

<script type="text/javascript"
	src="http://api.map.baidu.com/api?v=2.0&ak=xj2Gd1nMEwDHFcGeQvr0WORGk0t1Dlhw"></script>
<!-- 加载鼠标绘制工具 -->
<script type="text/javascript"
	src="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.js"></script>
<link rel="stylesheet"
	href="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.css" />
<!-- 加载检索信息窗口 -->
<script type="text/javascript"
	src="http://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.js"></script>
<link rel="stylesheet"
	href="http://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>

<!-- gy 添加表单验证 -->
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />
<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>
</head>

<body class="gray-bg">
	<div class="wrapper animated ">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-content">
					<button type="button" class="btn btn-primary" data-toggle="modal"
						data-target="#myModal5">添加下发区域预设信息</button>
					<div class="modal inmodal fade" id="myModal5" tabindex="-1"
						role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
		            	<form action="addPreInfo2" id="addPreInfoForm" class="form-horizontal"  method="post">
									<div class="modal-body">
										<div class="ibox-title">
											<h5>预设地图区域</h5>
											<div class="ibox-tools">
											<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
												</a>
											</div>
										</div>
										<div class="ibox-content">
											<div id="allmap">
												<div id="map" style="height:600px; width: 800px"></div>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<input type="hidden" id="type" name="type"> <input
											type="hidden" id="pointNum" name="pointNum"> <input
											type="hidden" id="circle" name="circle"> <input
											type="hidden" id="radius" name="radius"> <input
											type="hidden" id="oneP" name="oneP"> <input
											type="hidden" id="twoP" name="twoP"> <input
											type="hidden" id="threeP" name="threeP"> <input
											type="hidden" id="fourP" name="fourP"> <input
											type="hidden" id="fiveP" name="fiveP"> <input
											type="hidden" id="sixP" name="sixP">
										<button id="btn_addPreInfo" type="button" class="btn btn-white"
											data-dismiss="modal">关闭</button>
										<!-- <button id="submit_preinfo" type="submit" class="btn btn-primary" >保存</button> -->
									 	<button id="ajax_preinfo" type="button" class="btn btn-primary" onclick="ajax_submit()" >添加</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>预设信息表</h5>
				</div>
 
				<div class="ibox-content" style="height: 710px">
					<table id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
 
						<thead>
							<tr>
								 <th>类型</th>
								 <th>编号</th>
								 <th>点数</th>
							<!-- 	 <th>点信息</th> -->
								 <th>操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${pointsInfo}" var="preinfo">
								<tr>
									<td>多边形</td>
									<td>${preinfo.serial}</td>
									<td>${preinfo.pointsNum}</td>
								<!-- 	<td> </td> -->
									<td>
							 	<button type="button" class="btn btn-danger btn-sm"
						 	onclick="delete1('${preinfo.serial}')">删除</button>
								    </td>
								</tr>
							</c:forEach>
							
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
			<!-- 删除表格 -->
	  	<form id="deleteForm" action="delePre2" method="post">
			<input type="hidden" id="deleteId" name="id">
		</form>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>

	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script>
	
	//全局对象：
	window.pointInfo=null;
	
	$(document).ready(function() {
		$('#table1').DataTable({
			"pagingType" : "full_numbers"
		});
	});
	

	//点击"删除"按钮
	function delete1(id) {
		swal({
			title : "您确定删除吗?",
			type : "warning",
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "确认",
			showCancelButton : true,
			cancelButtonText : "取消"
		}, function() {
			$("#deleteId").val(id);
			//提交表单
			$("#deleteForm").submit();
	   	});
	}
    //页面加载完成后执行,获取执行状态
	$(document).ready(function(){
		var result = '<%=request.getAttribute("result")%>';
		if(result != null && result != "null"){
			if (result="操作执行成功！") {
			swal({
				title : result,
				type : "success"
			});
		}else{
			swal({
				title :result,
				type : "error"
			});
		}
		}
	});
				
		// 百度地图API功能
		var map = new BMap.Map('map');
		map.centerAndZoom("铜仁", 12);
		map.addControl(new BMap.NavigationControl());
		//map.setCenter('青岛');
		map.enableScrollWheelZoom(); //添加滚轮缩放地图功能
		//通过ip定位城市

		//添加在地图上添加覆盖物的控件
		/* var overlays = [];
		var overlaycomplete = function(e) {
			overlays.push(e.overlay);
		}; */
		var overlays = [];
		var overlaycomplete = function(e) {
			//clearAll();
			overlays.push(e.overlay);
		}
		var styleOptions = {
			strokeColor : "red", //边线颜色。
			fillColor : "silver", //填充颜色。当参数为空时，圆形将没有填充效果。
			strokeWeight : 3, //边线的宽度，以像素为单位。
			strokeOpacity : 0.3, //边线透明度，取值范围0 - 1。
			fillOpacity : 0.6, //填充的透明度，取值范围0 - 1。
			strokeStyle : 'solid' //边线的样式，solid或dashed。
		}
		//实例化鼠标绘制工具
		var drawingManager = new BMapLib.DrawingManager(map, {
			isOpen : false, //是否开启绘制模式
			enableDrawingTool : true, //是否显示工具栏
			//enableCalculate:true, //开启面积计算
			drawingToolOptions : {
				anchor : BMAP_ANCHOR_TOP_RIGHT, //位置
				offset : new BMap.Size(5, 5), //偏离值
		        drawingModes : [   //绘制类型
				//BMAP_DRAWING_CIRCLE,
				//BMAP_DRAWING_POLYLINE,
				BMAP_DRAWING_POLYGON,
				//BMAP_DRAWING_RECTANGLE 
         ]
			},
			//对于各个图形的样式
			circleOptions : styleOptions, //圆的样式
			polylineOptions : styleOptions, //线的样式 *
			polygonOptions : styleOptions, //多边形的样式
			rectangleOptions : styleOptions
		//矩形的样式
		});
		drawingManager.addEventListener('overlaycomplete', function(e) { //鼠标绘制完成后执行该事件
			clearAll();
			//置空
			pointInfo={};
			overlaycomplete(e);//先清除以前所有的再添加刚画的
			  if (e.drawingMode == "polygon") {
			  var points = [];
					points = e.overlay.getPath();
				 	//进行点的保存，通过遍历来进行拼接。
				 	var len=points.length;
					if(len>100){
						alert("不可超过一百个点");
						return;
					}
				    var poi=[];
				 	for(var i=0;i<len;i++){
				 		poi[i]={"latitude":points[i].lat,"longitude":points[i].lng};
				 	}  
					 pointInfo={"pointsNum":len,"point":poi};  
				}
			  else {
				alert("不能识别该覆盖物");
			}
		});
		function clearAll() {
			for (var i = 0; i < overlays.length; i++) {
				map.removeOverlay(overlays[i]);
				//map.clearOverlays();
			}
			overlays.length = 0;
		}
		
		
		$("#submit_preinfo").click(function(){
			if($("#type").val()==""){
				swal({
					title : "请在地图上标明地理范围",
					type : "warning"
				});
				return false;
			}
		});
	</script>
	
	<script type="text/javascript">
		//异步提交
		function ajax_submit(){
			var url="addPreInfo2";
		//测试使用:
		
		/*pointInfo =    
       	{    
         "pointsNum":5,   
         "point":   
             [   
                 {"latitude": 123.22,"longitude":111.111},   
                 {"latitude":111.11,"longitude":111.22}   
             ]   
    	 }  
		*/
				  $.ajax({ 
			            type:"POST", 
			            url:url, 
			            dataType:"json",      
			            contentType:"application/json",               
			            data:JSON.stringify(pointInfo), 
			            complete:function(data){
			          /*    var result = eval('('+data+')');
			            console.log("result"+result);
			    */         	swal({
							title : "操作成功",
							type : "success"
						});  
			            },
			            success:function(data){ 
			            	/* swal({
								title : "操作成功",
								type : "success"
							}); */
							//alert("操作成功");
			            	window.location.reload();
			            } 
			         }); 
		}
		</script>
</body>

</html>