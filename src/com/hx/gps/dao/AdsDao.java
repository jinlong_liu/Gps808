package com.hx.gps.dao;

import java.util.List;

import com.hx.gps.entities.PublicAdvs;
import com.hx.gps.entities.adsCustomer;
import com.hx.gps.entities.adsTime;
import com.hx.gps.entities.tool.Advertise;
import com.hx.gps.entities.tool.WaitAdvertise;

public interface AdsDao {

	/**
	 * 广告相关
	 * 
	 * @param advertise
	 * @param weekDay
	 * @param startYMD
	 * @param endYMD
	 * @param period
	 * @param immeAds
	 * @param noAdsTime
	 */
	boolean addAds(Advertise advertise, String[] weekDay, String startYMD, String endYMD, String period, String immeAds,
			String noAdsTime);

	// 可多选
	// boolean addAds(Advertise advertise, String[] weekDay, String startYMD,
	// String endYMD, String[] period, String immeAds,
	// String noAdsTime);
	List<Advertise> getAllAdvertises();

	boolean SendAds(List<String> isuNums, int id);

	// 获取所有客户
	List<adsCustomer> getAllAdsCustomer();

	// 保存客户
	void saveAdsCustomer(adsCustomer adsCustomer);

	// 更新客户
	boolean updateAdsCustomer(adsCustomer adsCustomer);

	// 删除客户
	boolean deleteAdsCustomerByid(int adsCustomerId);

	// 重复性验证
	boolean hasAdsCustomer(adsCustomer adsCustomer);

	// 验证时段是否已存在
	boolean hasAdsControl(adsTime adsTime);

	// 添加时段信息
	void saveAdsControl(adsTime adsTime);

	// 获取时段信息
	List<adsTime> getAllAdsTimes();

	// 条件查询时段
	List<adsTime> getAdsTimes(String adsTimeName);

	// 更新时段信息
	boolean updateAdsTime(adsTime adsTime);

	// 删除时段信息
	boolean deleteAdsControlByid(int adsTimeId);

	// 更新广告内容
	boolean updateAds(Advertise advertise);

	// 删除广告
	boolean deleteAdsById(int adsId);

	// 下发指令
	boolean SendInstruct(List<String> isuNums, int instruct, int play);

	// 公益广告保存
	boolean savePublicAdvs(PublicAdvs advs);
	//信息号重复性校验
	boolean hasAdsId(int playId);
	//获取所有公益广告
	List<PublicAdvs> getAllPublicAds();
	//发送广告信息
	boolean SendPublicAds(List<String> isuNums, int id);
	//获取所有发送记录
	List<WaitAdvertise> getAllHistory();
	//获取发送详情
	List<WaitAdvertise> findDetailsById(WaitAdvertise advertise);
	  boolean updatePublicAds(PublicAdvs paramPublicAdvs);
	 //判断广告条数
	boolean overflow();
	//获取最近一次发送的广告
	List<WaitAdvertise> getTempHistory();
	//根据输入时间获取历史记录
	List<WaitAdvertise> getHistoryByTime(String startDate, String endDate);

}
