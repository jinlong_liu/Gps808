package com.hx.gps.controller;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hx.gps.dao.TaxiDao;
import com.hx.gps.dao.WaitOrderDao;
import com.hx.gps.util.*;
import com.mongodb.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hx.gps.dao.SerialDao;
import com.hx.gps.entities.Company;
import com.hx.gps.entities.PageBean;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.tool.Alarm;
import com.hx.gps.entities.tool.Alarm2;
import com.hx.gps.entities.tool.AreaAddress;
import com.hx.gps.entities.tool.Blur;
import com.hx.gps.entities.tool.ComInfo2;
import com.hx.gps.entities.tool.ExportPoint;
import com.hx.gps.entities.tool.InfoQuery;
import com.hx.gps.entities.tool.OrderVector;
import com.hx.gps.entities.tool.OverSpeed;
import com.hx.gps.entities.tool.Point;
import com.hx.gps.entities.tool.PointsInfo;
import com.hx.gps.entities.tool.TaxiInfo2;
import com.hx.gps.entities.tool.WaitOrder;
import com.hx.gps.service.AddressService;
import com.hx.gps.service.InfoService;
import com.hx.gps.service.OrderService;
import com.hx.gps.service.SystemService;
import com.hx.gps.service.TaxiService;

import net.sf.json.JSONObject;

@Controller
public class SystemController {

    WaitOrderDao waitOrderDao = new WaitOrderDao();
    @Autowired
    private SystemService systemService;
    @Autowired
    private TaxiService taxiService;
    @Autowired
    private InfoService infoService;
    @Autowired
    private AddressService addressService;
    @Autowired
    private OrderService orderService;
    // 日志操作类（记录关键操作）
    private static Log logger = LogFactory.getLog(SystemController.class);

    // 点击退出按钮
    @RequestMapping(value = "/exit")
    public String exitSystem(HttpServletRequest request) {
        logger.info("退出登录");
        // 移除session属性
        request.getSession().removeAttribute("isLogin");
        // 重定向到登录界面
        return "redirect:/login";
    }

    // 处理报警
    @RequestMapping("/dealAlarm")
    public String dealAlarm(Alarm alarm, HttpServletResponse response, HttpServletRequest request) {
        String result = null;
        // String dealState= new
        // String(alarm.getDealState().getBytes("ISO-8859-1"), "UTF-8");
        // String alarmType=new
        // String(alarm.getAlarmType().getBytes("ISO-8859-1"), "UTF-8");
        alarm.setDealState(alarm.getDealState());
        alarm.setAlarmType(alarm.getAlarmType());
        System.out.println("Alarm: " + alarm.toString());
        result = systemService.dealAlarm(alarm);
        request.setAttribute("result", result);
        return "forward:getDealAlarm";
    }

    // 未处理报警 格式化磁盘
    @RequestMapping("/formatDisc")
    public String formatDisc(Alarm alarm, HttpServletResponse response, HttpServletRequest request) {
        String result = null;
        alarm.setDealState(alarm.getDealState());
        alarm.setAlarmType(alarm.getAlarmType());
        System.out.println("Alarm: " + alarm.toString());
        WaitOrder w = new WaitOrder();
        w.setTime(new Date());
        w.setIsuNum(alarm.getIsuNum());
        w.setSerialNum(SerialDao.getSerialNum());
        w.setOrderType("格式化磁盘");
        w.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
        waitOrderDao.insert(w);
        logger.info("格式化磁盘命令" + w.toString());
        //将报警信息改完已处理
        result = systemService.dealAlarm(alarm);
        if (result.equals("操作执行成功")) {
            request.setAttribute("result", "格式化成功，请前往命令日志页面查看");
        } else {
            request.setAttribute("result", "格式化执行失败");
        }
        return "forward:getDealAlarm";
    }
    //主界面格式化磁盘
    @RequestMapping("/formatDiscMain")
    public String formatDiscMain(String isuNum, HttpServletResponse response, HttpServletRequest request) {
        String result = null;
        WaitOrder w = new WaitOrder();
        w.setTime(new Date());
        w.setIsuNum(isuNum);
        w.setSerialNum(SerialDao.getSerialNum());
        w.setOrderType("格式化磁盘");
        w.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
        waitOrderDao.insert(w);
        logger.info("格式化磁盘命令" + w.toString());
        return "tempMoni";
    }

    // 按照时间、车牌号、处理状态，显示警报信息的列表
    @RequestMapping("/getAlarmByTaxi")
    public String getAlarmByTaxi(Map<String, Object> map, String taxiNum, String startDate, String endDate,
                                 Integer alarmType) {
        map.put("startDate", startDate);
        map.put("endDate", endDate);
        //map.put("taxiNum", taxiNum);
        if (endDate == null || endDate == "") {
            endDate = DateUtil.dateToString(new Date());
        }
        map.put("alarm", addressService.getAlarmByTaxi(taxiNum, startDate, endDate, alarmType));
        map.put("alarmType", alarmType);
        return "alarmLog";
    }

    // WK后台分页修改 按照时间、车牌号、处理状态，显示警报信息的列表
    @RequestMapping("/getAlarmByTaxi1")
    @ResponseBody
    public PageBean<Alarm2> getAlarmByTaxi1(InfoQuery infoQuery) {
        // System.out.println("【报警日志后台分页进入】");
        // System.out.println("【报警日志后台分页进入】"+infoQuery.getAlarmType());
        // System.out.println("【报警日志后台分页进入】"+infoQuery.getStartDate());
        // System.out.println("【报警日志后台分页进入】"+infoQuery.getEndDate());
        // System.out.println("【报警日志后台分页进入】"+infoQuery.getTaxiNum());
        // System.out.println("【报警日志后台分页进入】"+infoQuery.getSearch());
        // System.out.println("【报警日志后台分页进入】"+infoQuery.getLimit());
        // System.out.println("【报警日志后台分页进入】"+infoQuery.getOffset());

        Blur blur = addressService.getAlarmByTaxi1(infoQuery);
        // System.out.println("【controller报警日志后台分页alarms】"+blur);
        PageBean<Alarm2> pageBean = new PageBean<Alarm2>();
        if (blur == null) {
            pageBean.setTotal(0);
            pageBean.setRows(null);
        } else {
            pageBean.setTotal(blur.getSize());
            pageBean.setRows(blur.getListAlarm());
        }
        return pageBean;
    }

    // 获取未处理的报警信息
    @RequestMapping("/getDealAlarm")
    public String getDealAlarm(Map<String, Object> map) {
        System.out.println("===============getDealAlarm==================");
        List<Alarm> alarm = addressService.getdealAlarm();
        map.put("alarm", alarm);
        // 遍历如果有进出区域报警，则携带当时的终端号去找报警时刻的车辆位置信息。
        ObjectMapper mapper = new ObjectMapper();
        // 在socket取数据，改为直接通过ajax异步获取，提高效率。
        // 获取进出区域信息（只有一条区域）为了填充地图
        List<PointsInfo> pointsInfos = addressService.getPreInfo2();
        if (pointsInfos != null) {
            // 添加至集合中
            try {
                map.put("points", mapper.writeValueAsString(pointsInfos));
            } catch (JsonProcessingException e) {
                logger.error("[未处理报警]，格式转换异常" + e.getMessage());
            }
        }
        return "WEB-INF/views/system/dealAlarm";
    }
    //通过终端号查出所有未处理报警信息
    @RequestMapping("/getdealAlarmByIsu")
    public String getdealAlarmByIsu(Map<String, Object> map, String alarmType,String isuNum) {
        List<Alarm> alarm = addressService.getDealAlarmByIsu(alarmType,isuNum);
        System.out.println("报警信息为：+++++++++++++++++++="+alarm);
        map.put("alarm", alarm);
        map.put("taxiNum",addressService.getTaxiByIsu(isuNum).getTaxiNum());
        return "WEB-INF/views/system/dealAlarms";
    }

    @RequestMapping("/dealAllAlarm")
    @ResponseBody
    public int dealAlarm() {
        List<Alarm> alarms = addressService.getdealAlarm();
        alarms.forEach((alarm -> systemService.dealAlarm(alarm)));
        return alarms.size();
    }
    // 添加视频回放
    @ResponseBody
    @RequestMapping("/getReshow")
    public Map<String,String> getReshow(Map<String, Object> map,String taxiNum) {
        String isu = null;
        String isuNum = null;
        String video;
        Map<String,String> map1 = new HashMap<>();
        // 添加视频显示 (不需要进行切割)
        if (taxiService.getIsuByTaxi(taxiNum) != null) {
            isu = taxiService.getIsuByTaxi(taxiNum).getIsuNum();
            isuNum = isu.substring(1, isu.length());
        } else {
            isuNum = "defaultRRR";
        }
        // 根据车牌号获取终端号，拼接回显字符串。
        // http://defaultRRR.xstrive.com:8080/newhls/record/record0/index.m3u8
        if (taxiService.getIsuByTaxi(taxiNum).getCompany().getRemark().equalsIgnoreCase("new")) {
            map.put("vedio0", "http://" + isu + ".hxybvideo.com:8090/playback.html");
            video= "http://" + isu + ".hxybvideo.com:8090/playback.html";
            map1.put("url",video);
            map1.put("isu",isu);
        } else {
            map.put("vedio0", "http://" + isu + ".xstrive.com:8080");
            video= "http://" + isu + ".hxybvideo.com:8090/playback.html";
            map1.put("url",video);
            map1.put("isu",isu);
        }
        return map1;
    }

    //根据车牌号查询终端号
    @RequestMapping("/getTaxiByTaxiNum")
    @ResponseBody
    public Taxi getTaxiByTaxiNum(String taxiNum) {
        return addressService.getTaxisByTaxiNum(taxiNum);
    }
    @RequestMapping("/getAreaAlarm")
    @ResponseBody
    public List<AreaAddress> getAreaAlarm(String isuNum, String time) {
        // 根据报警时间和终端号唯一确定一条进出区域报警信息。
        AreaAddress address = addressService.getAreaAlarm(isuNum, time);
        // 转成json 传到前端。
        List<AreaAddress> addresses = new ArrayList<>();
        addresses.add(address);
        // 为避免data出现空数据的情况，将其填充到集合中来取
        return addresses;
    }

    // 查看拍照记录
    @RequestMapping("/getPictureByTaxi")
    public String getPictureByTaxi(Map<String, Object> map, String taxiNum, String startDate, String endDate) {
        if (taxiNum != null) {
            map.put("picture", systemService.getPicTureByIsu(taxiNum, startDate, endDate));
            //
        } else if (startDate != null) {
            // 按照起始时间查找拍照记录
            map.put("picture", systemService.getPicTureByDate(startDate, endDate));
        } else {
            // 若为初次进入则显示最后五条拍照记录
            map.put("picture", systemService.getLastPic());
        }
        return "WEB-INF/views/system/picture";
    }

    // 根据时间段查询命令日志
    @RequestMapping("/getOrderInfoByDate")
    public String getOrderInfoByDate(Map<String, Object> map, String startDate, String endDate) {
        map.put("startDate", startDate);
        map.put("endDate", endDate);
        startDate += " 00:00:00";
        if (endDate == null || endDate == "") {
            endDate = DateUtil.dateToString(new Date());
        } else {
            endDate += " 23:59:59";
        }
        map.put("order", systemService.getOrderByDate(startDate, endDate));
        return "WEB-INF/views/system/orderLog";
    }

    // 进入命令日志界面
    @RequestMapping("/inotOrderLog")
    public String inotOrderLog(Map<String, Object> map) {
        // 进入时首先查询出五条记录
        map.put("order", systemService.getOrderLast5());
        return "WEB-INF/views/system/orderLog";
    }

    // 命令日志导出表格
    @RequestMapping("/derivedFormOfOrder")
    public void derivedFormOfOrder(String startDate, String endDate, HttpServletResponse response)
            throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException {
        System.out.println("进来啦");
        startDate += " 00:00:00";
        if (endDate == null || endDate == "") {
            endDate = DateUtil.dateToString(new Date());
        } else {
            endDate += " 23:59:59";
        }
        List<OrderVector> orderVector = systemService.getOrderByDate(startDate, endDate);
        try {
            PoiUtil.getSensorExcel(orderVector, startDate, endDate, response);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException e) {
            logger.error("命令日志导出表格时抛出异常，异常类型：" + e.toString());
        }
    }

    // 报警日志导出表格
    @RequestMapping("/derivedFormOfAlarm")
    public void derivedFormOfAlarm(String startDate, String endDate, String taxiNum, Integer alarmType,
                                   HttpServletResponse response) throws NoSuchMethodException, SecurityException, IllegalAccessException,
            IllegalArgumentException, InvocationTargetException {
        if (endDate == null || endDate == "") {
            endDate = DateUtil.dateToString(new Date());
        }
        List<Alarm> alarms = addressService.getAlarmByTaxi(taxiNum, startDate, endDate, alarmType);

        // PoiUtil.getExcelOfAlarm(alarms,startDate,endDate,response);

        try {
            PoiUtil.getExcelOfAlarm(alarms, startDate, endDate, response);
            logger.info("报警日志导出表格成功");
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException e) {
            logger.error("报警日志导出表格时抛出异常，异常类型：" + e.toString());
            // System.out.println("导出表格时抛出异常，异常类型："+e.toString());
        }
    }

    // 未处理报警信息的导出表格
    @RequestMapping("/derivedFormOfDealAlarm")
    public void derivedFormOfDealAlarm(Map<String, Object> map, HttpServletResponse response)
            throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
            InvocationTargetException {
        List<Alarm> alarm = addressService.getdealAlarm();
        try {
            PoiUtil.getExcelOfDealAlarm(alarm, response);
            // PoiUtil.getExcelOfAlarm(alarms,startDate,endDate,response);
            logger.info("未处理报警信息导出表格成功");
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException e) {
            logger.error("未处理报警信息导出表格时抛出异常，异常类型：" + e.toString());
            // System.out.println("导出表格时抛出异常，异常类型："+e.toString());
        }
    }

    // 终端远程升级页面
    @RequestMapping(value = "/sysUpdate", method = RequestMethod.GET)
    public String sysUpdate(Map<String, Object> map) {
        // 升级当前公司车
        Subject subject = SecurityUtils.getSubject();
        // Session session = subject.getSession();
        List<Taxi> taxis = new ArrayList<>();
        // List<Company> companies=new ArrayList<Company>();
        if (subject.isPermitted("allcompy:get")) {
            taxis = taxiService.getAllTaxi();
        } else if (subject.isPermitted("bjcompy:get")) {
            List<Company> companies = new ArrayList<>();
            taxis.clear();
            companies = infoService.getAllCompanyBj();
            if (companies != null) {
                for (Company company : companies) {
                    InfoQuery info = new InfoQuery();
                    info.setComId(company.getId());
                    taxis.addAll(infoService.getTaxi(info));
                }
            }
        } else {
            String name = (String) subject.getPrincipal();
            // int id=(int)session.getAttribute("compyid");
            Company company = infoService.getComByUser(name);
            // System.out.println("公司id"+);
            // companies.add(company);
            InfoQuery query = new InfoQuery();
            query.setComId(company.getId());
            taxis = infoService.getTaxi(query);
        }
        // List<Taxi> taxis = infoService.getTaxi(infoQuery);
        map.put("taxis", taxis);
        List<String> isuVersion = systemService.getIsuVersion(taxis);
        List<String> isuDate = systemService.getIsuversionDate(taxis);
        map.put("version", isuVersion);
        map.put("isuDate", isuDate);
        return "WEB-INF/views/system/sysUpdate";
    }

    // 上传升级文件
    @RequestMapping(value = "/sysUpdateUpload", method = RequestMethod.POST)
    public String sysUpdateUpload(RedirectAttributes attr,
                                  @RequestParam(value = "file", required = false) MultipartFile file, HttpServletRequest request) {
        // 上传部分
        String path = request.getSession().getServletContext().getRealPath("upload/bin");
        String fileName = file.getOriginalFilename();
        File targetFile = new File(path, fileName);
        if (!targetFile.exists()) {
            targetFile.mkdirs();
        }
        // 保存至服务端
        try {
            file.transferTo(targetFile);
            attr.addFlashAttribute("result", "上传成功！");
            // 文件名
            attr.addFlashAttribute("fileName", fileName);
            // 文件大小
            attr.addFlashAttribute("fileSize", file.getSize() / 1000 + "kb");
            // 修改时间
            attr.addFlashAttribute("fileTime", DateUtil.dateToString(new Date()));
            systemService.saveUpdateData(targetFile);
            logger.info("保存升级文件到服务端");
        } catch (Exception e) {
            logger.error("升级文件保存异常" + file.getName() + e.getMessage());
            e.printStackTrace();
        }
        return "redirect:/sysUpdate";
    }

    @RequestMapping(value = "/sysUpdate", method = RequestMethod.POST, produces = "text/html;charset=UTF-8")
    @ResponseBody
    public String sysUpdatePost(@RequestParam(value = "isu[]", required = false, defaultValue = "") String[] isu) {
        return systemService.saveIsuToWairUpgrade(isu);
    }

    // wk全选升级添加
    @RequestMapping(value = "/sysUpdate2", method = RequestMethod.POST, produces = "text/html;charset=UTF-8")
    @ResponseBody
    public String sysUpdatePost2() {
        return systemService.saveIsuToWairUpgrade2();
    }

    @RequestMapping(value = "getIsuVersion")
    @ResponseBody
    public List<String> getIsuVersion(String isu) {
        // return systemService.getIsuVersion(isu);
        String result = null;
        WaitOrder w = new WaitOrder();
        w.setIsuNum(isu);
        w.setTime(new Date());
        w.setSerialNum(SerialDao.getSerialNum());
        w.setOrderType("获取版本号");
        w.setIdentify("远程升级");
        w.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
        w.setContext(isu);
        // 获取执行结果
        result = orderService.geiVersion(w);
        List<String> list = new ArrayList<>();
        list.add(result);
        return list;

    }

    @RequestMapping(value = "getAllIsuVersion")
    @ResponseBody
    public List<String> getAllIsuVersion(String isu) {
        WaitOrder w = new WaitOrder();
        String result = null;
        List<Taxi> taxiNums = taxiService.getAllTaxi();
        List<String> list = new ArrayList<>();
        if (taxiNums.size() == 0) {
            list.add("获取失败,未查找到车辆信息");
            return list;
        }
        for (Taxi taxi : taxiNums) {
            if (taxi.getIsuNum() != null && !taxi.getIsuNum().equals("")) {
                w.setIsuNum(taxi.getIsuNum());
                w.setTime(new Date());
                w.setSerialNum(SerialDao.getSerialNum());
                w.setOrderType("获取版本号");
                w.setIdentify("远程升级");
                w.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
                w.setContext(taxi.getIsuNum());
                result = orderService.geiVersion(w);
            }
        }
        // 获取执行结果
        list.add(result);
        return list;
    }

    // 系统设置-下发命令
    @RequestMapping(value = "SendisuControlByAjax")
    public void SendInstructByAjax(HttpServletResponse response, HttpServletRequest request, String taxiNums,
                                   String instructId) {
        /**
         * 页面上传的数据有：车牌号(可能是多个组成的字符串)、文本标识
         */
        String[] taxiNum = taxiNums.split(",");
        String result = null;

        WaitOrder w = new WaitOrder();
        w.setTime(new Date());
        w.setSerialNum(SerialDao.getSerialNum());
        w.setIdentify(instructId);
        if (instructId.equals("0")) {
            w.setOrderType("清除星历");
        } else if (instructId.equals("1")) {
            w.setOrderType("终端复位");
        } else {
            w.setOrderType("未知指令");
        }
        w.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
        w.setContext("");

        int strList = systemService.sendIsuContorl(w, taxiNum);
        if (strList == 0) {
            logger.info("下发指令成功" + w.toString());
            result = "发送成功";
        } else if (strList > 0) {
            logger.info("下发指令失败" + w.toString());
            result = "发送失败";
        }
        ObjectMapper mapper = new ObjectMapper();
        response.setContentType("text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        try {
            response.getWriter().print(mapper.writeValueAsString(result));
        } catch (IOException e) {
            logger.error("【下发终端指令】IO异常" + e.getMessage());
            e.printStackTrace();
        }
    }

    // 进入下发指令界面
    @RequestMapping("/intoIsuControl")
    public String enterInsSend(Map<String, Object> map, HttpServletRequest request) {
        logger.info("进入终端控制界面");
        // 所有公司的车辆总数
        int totality = 0;
        // --------------按权限显示车辆---------------------
        Subject subject = SecurityUtils.getSubject();
        // Session session = subject.getSession();

        List<Company> companies = new ArrayList<Company>();
        if (subject.isPermitted("allcompy:get")) {
            companies = infoService.getAllCompany();
        } else if (subject.isPermitted("bjcompy:get")) {
            companies = infoService.getAllCompanyBj();
        } else {
            String name = (String) subject.getPrincipal();
            // int id=(int)session.getAttribute("compyid");
            Company company = infoService.getComByUser(name);
            // System.out.println("公司id"+);
            if (company != null) {
                companies.add(company);
            }
        }
        // --------------------------------------
        List<ComInfo2> comInfo2s = new ArrayList<>();
        for (Company c : companies) {
            Set<Taxi> taxis = c.getTaxis();
            Set<TaxiInfo2> taxis2 = new HashSet<>();

            Iterator<Taxi> it = taxis.iterator();
            while (it.hasNext()) {
                Taxi next = it.next();
                TaxiInfo2 t2 = new TaxiInfo2(next.getId(), next.getTaxiNum());
                taxiService.setTaxiStateByIsuNum(t2, next.getIsuNum());
                taxis2.add(t2);
            }
            comInfo2s.add(new ComInfo2(c.getId(), c.getName(), taxis2));
        }
        // 计算所有公司的车辆数
        for (ComInfo2 comInfo : comInfo2s) {
            totality += comInfo.getCount();
        }
        // 如果由历史记录页面跳转，记住选中的id号。
        map.put("comInfo2s", comInfo2s);
        map.put("totality", totality);
        return "WEB-INF/views/system/sendIsuInstruct";
    }

    // 进入超速设置界面
    @RequestMapping(value = "/intoOverSpeed")
    public String intoOverSpeed(Map<String, Object> map, HttpServletRequest request) {
        // List<OverSpeed> list = systemService.getOverSpeed();
        System.out.println("【超速设置】进入页面");
        // map.put("OverSpeed", list);
        return "WEB-INF/views/system/overSpeed";
    }

    // 获取超速设置
    @RequestMapping("/getOverSpeed")
    public @ResponseBody
    List<OverSpeed> getOverSpeed() {
        List<OverSpeed> list = systemService.getOverSpeed();
        System.out.println("【超速设置】返回预设数据" + list.toString());
        return list;
    }

    // 保存超速默认设置
    @RequestMapping(value = "/saveOverSpeed")
    public String saveOverSpeed(OverSpeed overSpeed, RedirectAttributes attr) {
        String result = null;
        System.out.println("【超速设置】添加" + overSpeed.toString());
        try {
            systemService.saveOverSpeed(overSpeed);
            logger.info("【超速设置】添加" + overSpeed.toString());
            result = "预设操作成功！";
        } catch (Exception e) {
            logger.error("【超速设置】添加出现异常" + e.getMessage());
            result = "操作失败！";
        }
        attr.addFlashAttribute("result", result);
        return "redirect:/intoOverSpeed";
    }

    //进入导出经纬度界面
    @RequestMapping(value = "/intoExportPoint")
    public String intoExportPoint() {
        System.out.println("【导出经纬度】进入页面");
        return "WEB-INF/views/system/exportPoint";
    }

    //导出标记点经纬度
    @RequestMapping("/exportPoint")
    public void exportPoint(String exportPoint, HttpServletResponse response) throws JSONException, NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        logger.info("【导出经纬度】导出标记点数据：" + exportPoint.toString());
        org.json.JSONObject jsonObj = new org.json.JSONObject(exportPoint);
        JSONArray childrens = jsonObj.getJSONArray("point");
        PoiUtil.getExcelOfPoint(childrens, response);
    }

    // 进入终端控制界面
    @RequestMapping("/setControl")
    public String intoSetControl() {
        return "WEB-INF/views/system/setControl";
    }
    // 按照报警类型 显示含有此报警类型的车辆列表
    @RequestMapping("/getAlarmsByType")
    public String getAlarmsByType(Map<String, Object> map, String alarmType,String startDate,String endDate) {
        map.put("alarmType", alarmType);
        map.put("startDate", startDate);
        map.put("endDate", endDate);
        return "alarmLogs";
    }
    @RequestMapping("/getAllAlarmType")
    @ResponseBody
    public List<Alarm> getAllAlarmType(InfoQuery infoQuer) {
        List<Alarm> allAlarmType = addressService.getAllAlarmType(null, null, null);
        System.out.println(allAlarmType);
        return allAlarmType;
    }
}
