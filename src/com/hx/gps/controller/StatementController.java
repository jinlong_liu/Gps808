package com.hx.gps.controller;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hx.gps.entities.Company;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.tool.Address;
import com.hx.gps.entities.tool.ComInfo2;
import com.hx.gps.entities.tool.Meter;
import com.hx.gps.entities.tool.MeterState;
import com.hx.gps.entities.tool.MileageVector;
import com.hx.gps.entities.tool.MyPassVol;
import com.hx.gps.entities.tool.OperateTest;
import com.hx.gps.entities.tool.TaxiInfo2;
import com.hx.gps.entities.tool.TerminalState;
import com.hx.gps.entities.tool.videoState;
import com.hx.gps.service.AddressService;
import com.hx.gps.service.InfoService;
import com.hx.gps.service.TaxiService;
import com.hx.gps.util.PoiUtil;
import com.hx.gps.util.RelativeDateFormat;
import com.hx.gps.util.RelativeDateFormat2;
import com.mysql.jdbc.StringUtils;

@Controller
public class StatementController {
	@Autowired
	private AddressService addressService;
	@Autowired
	private InfoService infoService;
	@Autowired
	private TaxiService taxiService;
	private static Log logger = LogFactory.getLog(StatementController.class);

	// 进入在线率统计界面
	@RequestMapping("/getTreeOfOnline")
	public String getTreeOfOnline(Map<String, Object> map) {
		logger.info("进入在线率统计界面");
		// List<Company> companies = infoService.getAllCompany();
		// ------------- --------------------
		Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
		} else if (subject.isPermitted("bjcompy:get")) {
			companies = infoService.getAllCompanyBj();
		} else {
			String name = (String) subject.getPrincipal();

			// int id=(int)session.getAttribute("compyid");
			Company company = infoService.getComByUser(name);
			// System.out.println("公司id"+);
			if (company != null) {
				companies.add(company);

			}
		}

		// --------------------------------------

		List<ComInfo2> comInfo2s = new ArrayList<>();
		for (Company c : companies) {
			Set<Taxi> taxis = c.getTaxis();
			Set<TaxiInfo2> taxis2 = new HashSet<>();

			Iterator<Taxi> it = taxis.iterator();
			while (it.hasNext()) {
				Taxi next = it.next();
				TaxiInfo2 t2 = new TaxiInfo2(next.getId(), next.getTaxiNum());
				taxis2.add(t2);
			}
			comInfo2s.add(new ComInfo2(c.getId(), c.getName(), taxis2));
		}

		map.put("comInfo2s", comInfo2s);
		return "WEB-INF/views/statement/online";
	}

	// 在线率界面车辆在线列表
	@ResponseBody
	@RequestMapping("/getOnlineDetail")
	public List<Address> getOnlineDetail(Integer type, String companys, HttpServletResponse response) {
		String[] choose = companys.split(",");
		List<Integer> companyId = new ArrayList<>();
		for (int i = 0; i < choose.length; i++) {
			companyId.add(Integer.parseInt(choose[i]));
		}
		return taxiService.getOnlineCarsByCompys(type, companyId);
	}

	// 在线率界面车辆不在线列表
	@ResponseBody
	@RequestMapping("/getOfflineDetail")
	public List<Address> getOfflineDetail(Integer type, String companys, HttpServletResponse response) {
		String[] choose = companys.split(",");
		List<Integer> companyId = new ArrayList<>();
		for (int i = 0; i < choose.length; i++) {
			companyId.add(Integer.parseInt(choose[i]));
		}
		return taxiService.getOfflineCarsByCompys(type, companyId);
	}

	// 显示在线率报表
	@RequestMapping("/getOnlineState")
	public void getOnlineState(Integer type, String companys, HttpServletResponse response) {
		// 上传的值为选中公司的id
		String[] choose = companys.split(",");
		List<Integer> companyId = new ArrayList<>();
		for (int i = 0; i < choose.length; i++) {
			companyId.add(Integer.parseInt(choose[i]));
		}
		// 获取在线车辆数（选定公司）
		List<Integer> result = addressService.creenOnlin(type, companyId);
		logger.info("id为" + companyId + "总车辆数/在线车辆数：" + result);
		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().print(mapper.writeValueAsString(result));
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("获取指定公司车辆在线数出现异常" + e.getMessage());
		}
	}

	// 进入查看里程利用率界面
	@RequestMapping("/getmileageUtil")
	public String getmileUtil(Map<String, Object> map) {
		logger.info("进入查看里程利用率界面");
		// List<Company> companies = infoService.getAllCompany();
		// -------------- --------------------
		Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
		} else if (subject.isPermitted("bjcompy:get")) {
			companies = infoService.getAllCompanyBj();
		} else {
			String name = (String) subject.getPrincipal();

			// int id=(int)session.getAttribute("compyid");
			Company company = infoService.getComByUser(name);
			// System.out.println("公司id"+);
			if (company != null) {
				companies.add(company);
			}
		}

		// --------------------------------------
		List<ComInfo2> comInfo2s = new ArrayList<>();
		for (Company c : companies) {
			Set<Taxi> taxis = c.getTaxis();
			Set<TaxiInfo2> taxis2 = new HashSet<>();

			Iterator<Taxi> it = taxis.iterator();
			while (it.hasNext()) {
				Taxi next = it.next();
				TaxiInfo2 t2 = new TaxiInfo2(next.getId(), next.getTaxiNum());
				taxis2.add(t2);
			}
			comInfo2s.add(new ComInfo2(c.getId(), c.getName(), taxis2));
		}

		map.put("comInfo2s", comInfo2s);
		return "WEB-INF/views/statement/mileageUtil";
	}

	// 里程利用率报表
	@RequestMapping("/mileageUtil")
	public void mileageUtil(String companys, String startDate, String endDate, HttpServletResponse response) {
		String[] choose = companys.split(",");
		List<Integer> companyId = new ArrayList<>();
		for (int i = 0; i < choose.length; i++) {
			companyId.add(Integer.parseInt(choose[i]));
		}

		MileageVector mile = addressService.mileageUtil(companyId, startDate, endDate);

		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().print(mapper.writeValueAsString(mile));
			logger.info("生成里程利用率报表" + "对应公司id" + companyId + "里程" + mile);
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("生成里程利用率报表时出现异常" + e.getMessage());
		}
	}

	// 里程利用率导出表格 getKeYunExcel
	@RequestMapping("/getKeYunExcel")
	public void derivedFormOfKeYun(String taxiNum, String startDate, String endDate, String type,
			Map<String, Object> map, HttpServletResponse response) throws NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		// 获取终端号
		String[] choose = taxiNum.split(",");
		List<String> taxiNums = new ArrayList<>();
		for (int i = 0; i < choose.length; i++) {
			if (choose[i].length() >= 6)
				taxiNums.add(choose[i]);
		}
		// 获取客运数据
		List<MyPassVol> passVols = addressService.getFareFlow(startDate, endDate, type, taxiNums);
		// 根据type 来区分是按月查询还是按天查询
		logger.info("查询客运数据：" + passVols);
		try {
			PoiUtil.getExcelOfKeYun(passVols, response, type, startDate, endDate);
			logger.info("客运数据导出表格成功");
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			logger.error("客运数据导出表格时抛出异常，异常类型：" + e.toString());
		}

	}

	// 营运信息导出表格
	@RequestMapping("/getMeterExcel")
	public void derivedFormOfMeter(String taxiNum, String startDate, String endDate, Map<String, Object> map,
			HttpServletResponse response) throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		// List<Alarm> alarm=addressService.getdealAlarm();
		String[] choose = taxiNum.split(",");
		List<String> taxiNums = new ArrayList<>();
		for (int i = 0; i < choose.length; i++) {
//			if (choose[i].length() == 7)
//				taxiNums.add(choose[i].substring(1));
//			else if (choose[i].length() == 8)
//				taxiNums.add(choose[i].substring(1, 2) + choose[i].substring(3));
		}
		List<Meter> meters = addressService.getMetersByTaxi(taxiNums, startDate, endDate);
		logger.info("查询计价器营运记录：" + meters);
		try {
			PoiUtil.getExcelOfMeter(meters, response, startDate, endDate);
			logger.info("营运信息导出表格成功");
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			logger.error("营运信息导出表格时抛出异常，异常类型：" + e.toString());
		}
	}

	// 进入查看营收报表界面
	@RequestMapping("/getRevenue")
	public String getRevenue(Map<String, Object> map) {
		logger.info("进入查看营收报表界面");
		// List<Company> companies = infoService.getAllCompany();
		Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
		} else if (subject.isPermitted("bjcompy:get")) {
			companies = infoService.getAllCompanyBj();
		} else {
			String name = (String) subject.getPrincipal();

			// int id=(int)session.getAttribute("compyid");
			Company company = infoService.getComByUser(name);
			// System.out.println("公司id"+);
			if (company != null) {
				companies.add(company);
			}
		}

		// --------------------------------------
		List<ComInfo2> comInfo2s = new ArrayList<>();
		for (Company c : companies) {
			Set<Taxi> taxis = c.getTaxis();
			Set<TaxiInfo2> taxis2 = new HashSet<>();

			Iterator<Taxi> it = taxis.iterator();
			while (it.hasNext()) {
				Taxi next = it.next();
				TaxiInfo2 t2 = new TaxiInfo2(next.getId(), next.getTaxiNum());
				taxis2.add(t2);
			}
			comInfo2s.add(new ComInfo2(c.getId(), c.getName(), taxis2));
		}

		map.put("comInfo2s", comInfo2s);
		return "WEB-INF/views/statement/revenueReport";
	}

	// 显示营收报表
	@RequestMapping("/revenueReport")
	public String revenueReport(String taxiNums, String startDate, String endDate, Map<String, Object> map) {
		String[] choose = taxiNums.split(",");
		List<String> taxiNum = new ArrayList<>();
		for (int i = 0; i < choose.length; i++) {
			if (choose[i].length() >= 6)
				taxiNum.add(choose[i]);
		}

		List<ComInfo2> compInfo = addressService.getRevenueReport(taxiNum, startDate, endDate);
		logger.info("生成营收报表" + "车牌号" + taxiNums + "营收信息" + compInfo);
		map.put("compInfo", compInfo);
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		map.put("taxiNums", taxiNums);
		return "forward:getRevenue";
	}

	// 营收报表导出表格。
	@RequestMapping("getRevenueExcel")
	public void derivedFormOfRevenue(String taxiNum, String startDate, String endDate, Map<String, Object> map,
			HttpServletResponse response) throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		// List<Alarm> alarm=addressService.getdealAlarm();
		String[] choose = taxiNum.split(",");
		List<String> taxiNums = new ArrayList<>();
		for (int i = 0; i < choose.length; i++) {
			if (choose[i].length() >= 6)
				taxiNums.add(choose[i]);
		}
		List<ComInfo2> compInfo = addressService.getRevenueReport(taxiNums, startDate, endDate);
		try {
			PoiUtil.getExcelOfRevenue(compInfo, response, startDate, endDate);
			logger.info("营收报表导出表格成功");
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			logger.error("营收报表导出表格时抛出异常，异常类型：" + e.toString());
		}
	}

	// 进入"单车营运次数"界面
	@RequestMapping("/enterOperate")
	public String enterOperate(Map<String, Object> map) {
		logger.info("进入单车营运界面");
		// List<Company> companies = infoService.getAllCompany();
		// -------------判断是否有显示全部公司的权限--------------------
		Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
		} else if (subject.isPermitted("bjcompy:get")) {
			companies = infoService.getAllCompanyBj();
		} else {
			String name = (String) subject.getPrincipal();

			// int id=(int)session.getAttribute("compyid");
			Company company = infoService.getComByUser(name);
			// System.out.println("公司id"+);
			if (company != null) {
				companies.add(company);
			}
		}

		// --------------------------------------
		List<ComInfo2> comInfo2s = new ArrayList<>();
		for (Company c : companies) {
			Set<Taxi> taxis = c.getTaxis();
			Set<TaxiInfo2> taxis2 = new HashSet<>();

			Iterator<Taxi> it = taxis.iterator();
			while (it.hasNext()) {
				Taxi next = it.next();
				TaxiInfo2 t2 = new TaxiInfo2(next.getId(), next.getTaxiNum());
				taxis2.add(t2);
			}
			comInfo2s.add(new ComInfo2(c.getId(), c.getName(), taxis2));
		}

		map.put("comInfo2s", comInfo2s);
		return "WEB-INF/views/statement/zhexian_yingyun";
	}

	// axjx查询营运数据
	@RequestMapping("/getOperateByAjax")
	public void getOperateTime(HttpServletResponse response, HttpServletRequest request, String startDate,
			String endDate, String taxiNums) {
		// 开始时间、结束时间、车辆数组查询出OperateTest 数组
		String[] choose = taxiNums.split(",");
		List<String> taxiNum = new ArrayList<>();
		for (int i = 0; i < choose.length; i++) {
			if (choose[i].length() >= 6)
				taxiNum.add(choose[i]);
		}

		List<OperateTest> operateTests = addressService.flowOfAnalyze(startDate, endDate, taxiNum);

		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			logger.info("成功得出单车营运数据" + "车牌号：" + taxiNum + "数据" + operateTests);
			response.getWriter().print(mapper.writeValueAsString(operateTests));
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("获取单车营运数据出现异常" + e.getMessage());
		}
	}

	// 进入"设备状态监控"界面
	@RequestMapping("/enterVideoState")
	public String enterVideoState(Map<String, Object> map) {
		logger.info("进入[设备状态监控]界面");
		List<Company> companies = new ArrayList<Company>();
		companies = infoService.getAllCompany();
		List<ComInfo2> comInfo2s = new ArrayList<>();
		for (Company c : companies) {
			Set<Taxi> taxis = c.getTaxis();
			Set<TaxiInfo2> taxis2 = new HashSet<>();
			Iterator<Taxi> it = taxis.iterator();
			while (it.hasNext()) {
				Taxi next = it.next();
				TaxiInfo2 t2 = new TaxiInfo2(next.getId(), next.getTaxiNum());
				taxis2.add(t2);
			}
			comInfo2s.add(new ComInfo2(c.getId(), c.getName(), taxis2));
		}

		map.put("comInfo2s", comInfo2s);
		return "videoState";
	}

	// 进入"计价器状态"界面
	@RequestMapping("/enterMeterState")
	public String enterMeterState(Map<String, Object> map) {
		logger.info("进入[计价器状态]界面");
		List<Company> companies = new ArrayList<Company>();
		companies = infoService.getAllCompany();
		List<ComInfo2> comInfo2s = new ArrayList<>();
		for (Company c : companies) {
			Set<Taxi> taxis = c.getTaxis();
			Set<TaxiInfo2> taxis2 = new HashSet<>();
			Iterator<Taxi> it = taxis.iterator();
			while (it.hasNext()) {
				Taxi next = it.next();
				TaxiInfo2 t2 = new TaxiInfo2(next.getId(), next.getTaxiNum());
				taxis2.add(t2);
			}
			comInfo2s.add(new ComInfo2(c.getId(), c.getName(), taxis2));
		}

		map.put("comInfo2s", comInfo2s);
		return "meterState";
	}

	// 进入设备状态(摄像头)查看界面
	@RequestMapping("/enterPassVol")
	public String enterPassVol(Map<String, Object> map) {
		logger.info("进入客运量分析界面");
		// List<Company> companies = infoService.getAllCompany();
		// -------------- ---------------------
		Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
		} else if (subject.isPermitted("bjcompy:get")) {
			companies = infoService.getAllCompanyBj();
		} else {
			String name = (String) subject.getPrincipal();

			// int id=(int)session.getAttribute("compyid");
			Company company = infoService.getComByUser(name);
			// System.out.println("公司id"+);
			if (company != null) {
				companies.add(company);
			}
		}

		// --------------------------------------
		List<ComInfo2> comInfo2s = new ArrayList<>();
		for (Company c : companies) {
			Set<Taxi> taxis = c.getTaxis();
			Set<TaxiInfo2> taxis2 = new HashSet<>();

			Iterator<Taxi> it = taxis.iterator();
			while (it.hasNext()) {
				Taxi next = it.next();
				TaxiInfo2 t2 = new TaxiInfo2(next.getId(), next.getTaxiNum());
				taxis2.add(t2);
			}
			comInfo2s.add(new ComInfo2(c.getId(), c.getName(), taxis2));
		}

		map.put("comInfo2s", comInfo2s);
		return "WEB-INF/views/statement/zhexian_keyun";
	}

	// 查询计价器状态
	@RequestMapping("/getMeterState")
	public String getMeterState(Map<String, Object> map, String taxiNum) {
		// 调用serivice
		List<MeterState> meterStates = new ArrayList<>();
		if (!StringUtils.isNullOrEmpty(taxiNum)) {
			meterStates = infoService.getMeterState(taxiNum);
			System.out.println("****查询到的车辆计价器状态：" + meterStates);
		}
		// 判断查询结果是否为空
		if (meterStates != null) {
			// 对设备状态进行转义
			for (MeterState index : meterStates) {
				// 判断状态号：0空车，1载客，2电召，3暂停
				switch (index.getState()) {// 获取设备状态号
				case "0":
					index.setState("空车");
					break;
				case "1":
					index.setState("载客");
					break;
				case "2":
					index.setState("电召");
					break;
				case "3":
					index.setState("暂停");
					break;
				}
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
				Date currentTime = new Date(); // new Date()为获取当前系统时间
				String sqldate = index.getTime(); // 获取数据库中的时间
				Date sqlTime;
				try {
					sqlTime = df.parse(sqldate); // 转换成date类型
					long s1 = currentTime.getTime(); // 当前时间换算成时间的毫秒
					long s2 = sqlTime.getTime(); // 数据库时间
					if (s1 - s2 > 180000) { // 超过3分钟判定为离线
						index.setState("离线");
					}
					String time = RelativeDateFormat.format(sqlTime); // 将date类型转换成几秒前
					index.setTime(time);
					System.out.println(time);
				} catch (ParseException e) {
					e.printStackTrace();
				}

			}
			map.put("meterStates", meterStates);
		}
		// 返回数据并转发
		return "forward:/enterMeterState";
	}

	// 进入"终端状态"界面
	@RequestMapping("/enterTerminalState")
	public String enterTerminalState(Map<String, Object> map) {
		logger.info("进入[终端状态]界面");
		List<Company> companies = new ArrayList<Company>();
		companies = infoService.getAllCompany();
		List<ComInfo2> comInfo2s = new ArrayList<>();
		for (Company c : companies) {
			Set<Taxi> taxis = c.getTaxis();
			Set<TaxiInfo2> taxis2 = new HashSet<>();
			Iterator<Taxi> it = taxis.iterator();
			while (it.hasNext()) {
				Taxi next = it.next();
				TaxiInfo2 t2 = new TaxiInfo2(next.getId(), next.getTaxiNum());
				taxis2.add(t2);
			}
			comInfo2s.add(new ComInfo2(c.getId(), c.getName(), taxis2));
		}

		map.put("comInfo2s", comInfo2s);
		return "terminalState";
	}

	// 查询终端状态
	@RequestMapping("/getTerminalState")
	public String getTerminalState(Map<String, Object> map, String taxiNum) {
		// 调用serivice
		List<TerminalState> terminalStates = new ArrayList<>();
		if (!StringUtils.isNullOrEmpty(taxiNum)) {
			terminalStates = infoService.getTerminalState(taxiNum);
			// System.out.println("****查询到的车辆终端状态：" + terminalStates);
		}
		// 判断查询结果是否为空
		if (terminalStates != null) {
			// 对设备状态进行转义
			for (TerminalState index : terminalStates) {
				// 判断状态号：0空车，1载客，2电召，3暂停，4 未知
				switch (index.getState()) {// 获取设备状态号
				case "00000000":
					index.setState("空车");
					break;
				case "00000001":
					index.setState("载客");
					break;
				case "00000002":
					index.setState("电召");
					break;
				case "00000003":
					index.setState("暂停");
					break;
				}
			
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
				Date currentTime = new Date(); // new Date()为获取当前系统时间
				if(!index.getState().equals("从未上线")){
					String sqldate = index.getTime(); // 获取数据库中的时间
					Date sqlTime;
					try {
						sqlTime = df.parse(sqldate); // 转换成date类型
						long s1 = currentTime.getTime(); // 当前时间换算成时间的毫秒
						long s2 = sqlTime.getTime(); // 数据库时间
						if (s1 - s2 > 180000) { // 超过3分钟判定为离线
							index.setState("离线");
						}
						String time = RelativeDateFormat2.format(sqlTime); // 将date类型转换成几秒前
						index.setTime(time);
						// System.out.println(time);
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}

			}
			map.put("terminalStates", terminalStates);
		} 
		// 返回数据并转发
		return "forward:/enterTerminalState";
	}

	// 根据选中车辆显示设备状态信息
	@RequestMapping("/getIsuVideoState")
	public String getIsuVideoState(Map<String, Object> map, String isuNum) {
		// 打印车牌号
		logger.info("待查询设备状态号 " + isuNum);
		// 调用serivice
		List<videoState> videoStates = new ArrayList<>();
		videoStates = infoService.getIsuVideoState(isuNum);
		if (videoStates != null) {
			map.put("videoStates", videoStates);
		}
		// 返回数据并转发
		return "forward:/enterVideoState";
	}

	// axjx查询客运量
	@RequestMapping("/getPassVolByAjax")
	public void getPassVolByAjax(HttpServletResponse response, HttpServletRequest request, String startDate,
			String endDate, String type, String taxiNums) {

		String[] choose = taxiNums.split(",");
		List<String> taxiNum = new ArrayList<>();
		for (int i = 0; i < choose.length; i++) {
			if (choose[i].length() >= 6)
				taxiNum.add(choose[i]);
		}

		List<MyPassVol> passVols = addressService.getFareFlow(startDate, endDate, type, taxiNum);

		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().print(mapper.writeValueAsString(passVols));
			logger.info("得到客运量分析数据如下： " + "车牌号" + taxiNum + "数据" + passVols);
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("客运量分析出现异常" + e.getMessage());
		}

	}

}
