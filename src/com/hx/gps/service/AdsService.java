package com.hx.gps.service;

import java.util.List;

import com.hx.gps.entities.PublicAdvs;
import com.hx.gps.entities.adsCustomer;
import com.hx.gps.entities.adsTime;
import com.hx.gps.entities.tool.Advertise;
import com.hx.gps.entities.tool.WaitAdvertise;

//信息管理
public interface AdsService {

	/**
	 * 广告相关
	 * 
	 * @param advertise
	 * @param weekDay
	 * @param startYMD
	 * @param endYMD
	 * @param period
	 * @param immeAds
	 * @param noAdsTime
	 */
	// 可多选时间段
	// String addAds(Advertise advertise, String[] weekDay, String startYMD,
	// String endYMD, String[] period, String immeAds,
	// String noAdsTime);
	String addAds(Advertise advertise, String[] weekDay, String startYMD, String endYMD, String period, String immeAds,
			String noAdsTime);

	List<Advertise> getAllAdvertises();

	String SendAds(int[] playIds, List<String> taxiNum);

	// 获取所有客户
	List<adsCustomer> getAllAdsCustomer();

	// 新增客户信息
	String saveAdsCustomer(adsCustomer adsCustomer);

	// 更新客户信息
	String updateAdsCustomer(adsCustomer adsCustomer);

	// 删除客户信息
	String deleteAdsCustomer(int adsCustomerId);

	// 添加时段信息
	String saveAdsControl(adsTime adsTime);

	// 获取所有的时段信息
	List<adsTime> getAllAdsTimes();

	// 根据名称查询时段信息
	List<adsTime> getAdsTimeByName(String adsTimeName);

	// 更新时段信息
	String updateAdsTime(adsTime adsTime);

	// 删除时段信息
	String deleteAdsTime(int adsTimeId);
	//更新广告内容
	String updateAds(Advertise advertise);
	//删除广告信息
	String deleteAdsById(int adsId);
	//下发指令
	String SendInstruct(int instruct, int play, List<String> taxiNum);
	//保存公益广告
	String savePublicAds(PublicAdvs advs);
	//获取所有的公益广告
	List<PublicAdvs> getAllPublicAds();
	//存到待发送表 （公益广告）
	String SendPublicAds(int[] playIds, List<String> taxiNum);
	//获取所有的发送记录
	List<WaitAdvertise> getAllHistory();
	//查询发送详情以及各种状态对应的车辆数量。
	List<WaitAdvertise> findDetailsById(WaitAdvertise advertise);
	//更新公益广告
	String updatePublicAds(PublicAdvs paramPublicAdvs);
	//验证是否有playid 存在了 ajax
	String hasPlayId(int id);
	//获取最近的一条发送记录
	List<WaitAdvertise> getTempHistory();
	//根据时间查询历史记录
	List<WaitAdvertise> getHistoryByTime(String startDate, String endDate);
	//验证时段编码是否存在
	String hasTimeId(int id);
	//验证客户编码是否存在
	String hasCustomerId(int id);
 

}
