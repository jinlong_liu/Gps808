<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<script src="js/jquery.js"></script>

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">

<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>

<!-- gy 添加表单验证 -->
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />
<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">
		<!-- 查询列表 -->
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>输入条件查询年审信息</h5>
				</div>
				<div class="ibox-content">
					<form action="getYearinspect" class="form-horizontal" method="post">
						<div class="row">
							<div class="form-group">
								<!-- 起始日期 -->
								<label class="col-sm-1 control-label">起始日期</label>
								<div class="col-sm-2">
									<input type="text" class="form-control" id="startTime" autocomplete="off"
										name="startTime"
										onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss'})">
								</div>
								<!-- 终止日期 -->
								<label class="col-sm-1 control-label">终止日期</label>
								<div class="col-sm-2">
									<input type="text" class="form-control" id="endTime" autocomplete="off"
										type="text" name="endTime"
										onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'startTime\')}',dateFmt:'yyyy-MM-dd '+'00:00:00.0'})">
								</div>
								<!-- 公司名称 -->
								<label class="col-sm-1 control-label">公司名称</label>
								<div class="col-sm-2">
									<select class="form-control" name="comId">
										<c:forEach items="${companies}" var="companies">
											<option value="${companies.id }">${companies.name }</option>
										</c:forEach>
									</select>
								</div>
								<div class="col-sm-2">
									<button type="submit" class="btn btn-primary">查询</button>
									<shiro:hasPermission name="yearinspect:admin">
										<button id="btn_addYearinspect" type="button"
											class="btn btn-primary" data-toggle="modal"
											data-target="#myModal5">添加</button>
									</shiro:hasPermission>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>


	<!--gy修改后 点击添加按钮弹出来的form表单 -->
	<div class="modal inmodal fade" id="myModal5" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="saveYearinspectForm" class="form-horizontal"
					action="saveYearinspect" method="post">
					<div class="modal-body">
						<div class="ibox-title">
							<h5>新增年审信息<span style="color:red">（* 为必填项）</span></h5>
						</div>
						<div class="ibox-content">
							<div class="row">
								<!-- 公司名称-->
								<div class="form-group">
									<label class="col-sm-2 control-label"><span style="color:red">*  </span>公司名称： </label>
									<div class="col-sm-7">
										<select class="form-control" name="company.id">
											<option></option>
											<c:forEach items="${companies}" var="companies">
												<option value="${companies.id }">${companies.name }</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<!-- 车牌号码-->
								<div class="form-group">
									<label class="col-sm-2 control-label"><span style="color:red">*  </span>车牌号码 ：</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" name="taxiNum" autocomplete="off">
									</div>
								</div>
								<!-- 上次年审-->
								<div class="form-group">
									<label class="col-sm-2 control-label"><span style="color:red">*  </span>上次年审 ：</label>
									<div class="col-sm-7">
										<input id="lastTime" class="form-control" type="text" autocomplete="off"
											name="lastTime"
											onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(saveYearinspectForm, name.valueOf())}})">
									</div>
								</div>
								<!-- 下次年审-->
								<div class="form-group">
									<label class="col-sm-2 control-label"><span style="color:red">*  </span>下次年审 ：</label>
									<div class="col-sm-7">
										<input id="nextTime" class="form-control" type="text" autocomplete="off"
											name="nextTime"
											onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'lastTime\')}',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(saveYearinspectForm, name.valueOf())}})">
									</div>
								</div>
								<!-- 金额-->
								<div class="form-group">
									<label class="col-sm-2 control-label"><span style="color:red">*  </span>金额 ：</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" name="money" autocomplete="off">
									</div>
								</div>
								<!-- 提醒-->

							<!-- 	<div class="form-group">
									<label class="col-sm-2 control-label">是否提醒</label>
									<div class="col-sm-7">
										<div class="radio">
											<label> <input type="radio" name="remind" value="0"
												checked="checked">不提醒
											</label>
										</div>
										<div class="radio">

											<label><input type="radio" name="remind" value="1">提醒</label>
										</div>
									</div>
								</div> -->
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
								<button type="submit" class="btn btn-primary">保存</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!--gy修改前 点击添加按钮弹出来的form表单 -->
	<div class="modal inmodal fade" id="myModal544444" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="saveYearinspectForm" action="saveYearinspect"
					method="post">
					<div class="modal-body">
						<div class="ibox-title">
							<h5>新增年审信息</h5>
						</div>
						<div class="ibox-content">
							<div class="row">
								<div class="col-sm-4">
									公司名称： <select name="company.id">
										<option></option>
										<c:forEach items="${companies}" var="companies">
											<option value="${companies.id }">${companies.name }</option>
										</c:forEach>
									</select>
								</div>
								<div class="col-sm-4">
									车牌号码：<input type="text" name="taxiNum">
								</div>
								<div class="col-sm-4">
									上次年审：<input id="lastTime" readonly="readonly" type="text"
										name="lastTime"
										onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss'})">
								</div>
							</div>
							<br>

							<div class="row">
								<div class="col-sm-4">
									金额：<input type="text" name="money">
								</div>
								<div class="col-sm-4">
									下次年审：<input id="nextTime" readonly="readonly" type="text"
										name="nextTime"
										onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'lastTime\')}',dateFmt:'yyyy-MM-dd HH:mm:ss'})">
								</div>
								<!-- <div class="col-sm-4">
									<input type="radio" name="remind" value="0" checked="checked">不提醒
									<input type="radio" name="remind" value="1">提醒
								</div> -->
							</div>

						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
						<button type="submit" class="btn btn-primary">保存</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- 数据展示 -->
	<c:if test="${yearinspects!=null}">
		<div class="row">
			<div class="col-sm-12">
		<div class="ibox-title">
			<div>
				<h5>&nbsp;&nbsp;&nbsp;年审信息表</h5>
			</div>
		</div>
		<div class="ibox-content" style="height: 660px">
			<table id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
		
				<thead>
					<tr>
						<th>公司名称</th>
						<th>车牌号码</th>
						<th>上次年审</th>
						<th>金额</th>
						<th>下次年审</th>
						<!-- 5 -->

						<!-- <th>是否提醒</th> -->
						<!-- 6 -->

						<th width="100">操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${yearinspects}" var="yearinspects">
						<tr>
							<td>${yearinspects.company.name }</td>
							<td>${yearinspects.taxiNum }</td>
							<td>${yearinspects.lastTime }</td>
							<td>${yearinspects.money }</td>
							<td>${yearinspects.nextTime }</td>
							<!-- 5 -->

							<%-- <td>${yearinspects.remind }</td> --%>
							<!-- 6 -->
							<td><shiro:hasPermission name="yearinspect:admin">
									<button id="btn-update" type="button"
										class="btn btn-success btn-sm" data-toggle="modal"
										data-target="#myModal1"
										onclick="update('${yearinspects.id }','${yearinspects.company.id }','${yearinspects.company.name }'
									,'${yearinspects.taxiNum}','${yearinspects.lastTime }','${yearinspects.money}'
									,'${yearinspects.nextTime}','${yearinspects.remind}')">修改</button>
									<button type="button" class="btn btn-danger btn-sm"
										onclick="delete1('${yearinspects.id}','${yearinspects.taxiNum}')">删除</button>
								</shiro:hasPermission></td>
						</tr>
					</c:forEach>
				</tbody>
				<tfoot>
							<tr>
								<td colspan="6">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
			</table>
		</div>
	</div>
	</div>
	</c:if>
	<!-- 删除表格 -->
	<form id="deleteForm" action="deleteYearinspect" method="post">
		<input type="hidden" id="deleteId" name="id">
	</form>

	<!-- 点击"修改"按钮弹出来的form表单 -->
	<div class="modal inmodal fade" id="myModal1" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form action="updateYearinspect" id="updateYearinspectForm" class="form-horizontal" method="post">
					<input id="id" name="id" type="hidden">
					<div class="modal-body">
						<div class="ibox-title">
							<h5>修改年审信息<span style="color:red">（* 为必填项）</span></h5>
						</div>
						<!-- <div class="ibox-content">
							<div class="row">
								<div class="col-sm-4">
									公司名称： <select name="company.id" id="comSelect">
									</select>
								</div>
								<div class="col-sm-4">
									车牌号码：<input type="text" name="taxiNum" id="taxiNum">
								</div>
								<div class="col-sm-4">
									上次年审：<input type="text" name="lastTime" id="lastTime">
								</div>
							</div>
							<br>

							<div class="row">
								<div class="col-sm-4">
									金额：<input type="text" name="money" id="money">
								</div>
								<div class="col-sm-4">
									下次年审：<input type="text" name="nextTime" id="nextTime">
								</div>
								<div class="col-sm-4">
									<input type="radio" name="remind" value="0" checked="checked">不提醒
									<input type="radio" name="remind" value="1">提醒
								</div>
							</div>
							<br>

						</div> -->
						
						 <div class="ibox-content">
							<div class="row">
								<!-- 公司名称-->
								<div class="form-group">
									<label class="col-sm-2 control-label"><span style="color:red">*  </span>公司名称 ：</label>
									<div class="col-sm-7">
										<select class="form-control" name="company.id" id="comSelect">
											<option></option>
											<c:forEach items="${companies}" var="companies">
												<option value="${companies.id }">${companies.name }</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<!-- 车牌号码-->
								<div class="form-group">
									<label class="col-sm-2 control-label"><span style="color:red">*  </span>车牌号码 ：</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="taxiNum" name="taxiNum">
									</div>
								</div>
								<!-- 上次年审-->
								<div class="form-group">
									<label class="col-sm-2 control-label"><span style="color:red">*  </span>上次年审 ：</label>
									<div class="col-sm-7">
										<input id="lastTime1" class="form-control" type="text"
											name="lastTime"
											onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(updateYearinspectForm, name.valueOf())}})">
									</div>
								</div>
								<!-- 下次年审-->
								<div class="form-group">
									<label class="col-sm-2 control-label"><span style="color:red">*  </span>下次年审 ：</label>
									<div class="col-sm-7">
										<input id="nextTime1" class="form-control" type="text"
											name="nextTime"
											onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'lastTime1\')}',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(updateYearinspectForm, name.valueOf())}})">
									</div>
								</div>
								<!-- 金额-->
								<div class="form-group">
									<label class="col-sm-2 control-label"><span style="color:red">*  </span>金额 ：</label>
									<div class="col-sm-7">
										<input type="text" class="form-control" id="money" name="money">
									</div>
								</div>
								<!-- 提醒-->

								<!-- <div class="form-group">
									<label class="col-sm-2 control-label">是否提醒</label>
									<div class="col-sm-7">
										<div class="radio">
											<label> <input type="radio" name="remind" value="0"
												checked="checked">不提醒
											</label>
										</div>
										<div class="radio">

											<label><input type="radio" name="remind" value="1">提醒</label>
										</div>
									</div>
								</div> -->
							</div>
						
						<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
						<button id="update_button" type="submit" class="btn btn-primary">保存</button>
					</div>
						
						
						</div>
					</div>

			</form>
			</div>
		</div>
	</div>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<!-- 	<script src="js/jquery.min.js?v=2.1.4"></script> -->
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script>
	//加载footable的功能
	$(document).ready(function() {
	$('#table1').DataTable({
		"pagingType" : "full_numbers"
	});
});

		//点击"修改"按钮
		function update(id, comId, comName, taxiNum, lastTime, money, nextTime,
				remind) {
			$("#id").val(id);

			//修改公司 下拉框的值
			$.post("getAllComByAjax", function(data) {
				if (data.length != 0) {//返回的数据不为空 
					var newdata = eval("(" + data + ")");
					$("#comSelect").html("");//避免选择框中的值持续叠加
					$("<option value='"+comId+"'>" + comName + "</option>")
							.appendTo($("#comSelect"));//数据回显

					for (var i = 0; i < data.length; i++) {//数据库所有数据
						$(
								"<option value ='"+newdata[i].comId +
											"'> "
										+ newdata[i].cname + "</option>")
								.appendTo($("#comSelect"));
					}
				}
			});
			$("#taxiNum").val(taxiNum);
			$("#lastTime1").val(lastTime);
			$("#money").val(money);
			$("#nextTime1").val(nextTime);
			if(remind==1){
				$("input[name='remind'][value='1']").attr("checked",true);
			}else if(remind==0){
				$("input[name='remind'][value='0']").attr("checked",true);
			}
		};

		//点击"删除"按钮
		function delete1(id, name) {
			swal({
				title: "您确定删除该条数据吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
				$("#deleteId").val(id);
				//提交表单
				$("#deleteForm").submit();
			});
		}
		
		
		$(document).ready(function(){
			var result = '<%=request.getAttribute("result")%>';
			if(result != null && result != "null"){
				if (result.indexOf("成功")>0) {
				swal({
					title : result,
					type : "success"
				});
			}else{
				swal({
					title :result,
					type : "error"
				});
			}
			}
		});
	</script>

	<script type="text/javascript">
		/*添加年审界面的表单验证*/
		$('#btn_addYearinspect').click(function() {
			$('#saveYearinspectForm').bootstrapValidator({
				feedbackIcons : {/*输入框不同状态，显示图片的样式*/
					valid : 'glyphicon glyphicon-ok',
					invalid : 'glyphicon glyphicon-remove',
					validating : 'glyphicon glyphicon-refresh'
				},
				/*生效规则：字段值一旦变化就触发验证*/
				live : 'enabled',
				/*当表单验证不通过时，该按钮为disabled*/
				submitButtons : 'button[type="submit"]',
				/*验证*/
				fields : {
					taxiNum : {/*键名username和input name值对应*/
						validators : {
							notEmpty : {/*非空提示*/
								message : '车牌号码不能为空'
							}
						}
					},
					lastTime : {
						validators : {
							notEmpty : {
								message : '上次年审时间不能为空'
							}
						}
					},
					nextTime : {
						validators : {
							notEmpty : {
								message : '下次年审时间不能为空'
							}
						}
					},
					money : {
						validators : {
							notEmpty : {
								message : '金额不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					}
				}
			});
		});
		
		//修改验证
		$('#update_button').click(function() {
			$('#updateYearinspectForm').bootstrapValidator({
				feedbackIcons : {/*输入框不同状态，显示图片的样式*/
					valid : 'glyphicon glyphicon-ok',
					invalid : 'glyphicon glyphicon-remove',
					validating : 'glyphicon glyphicon-refresh'
				},
				/*生效规则：字段值一旦变化就触发验证*/
				live : 'enabled',
				/*当表单验证不通过时，该按钮为disabled*/
				submitButtons : 'button[type="submit"]',
				/*验证*/
				fields : {
					taxiNum : {/*键名username和input name值对应*/
						validators : {
							notEmpty : {/*非空提示*/
								message : '车牌号码不能为空'
							}
						}
					},
					lastTime : {
						validators : {
							notEmpty : {
								message : '上次年审时间不能为空'
							}
						}
					},
					nextTime : {
						validators : {
							notEmpty : {
								message : '下次年审时间不能为空'
							}
						}
					},
					money : {
						validators : {
							notEmpty : {
								message : '金额不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					}
				}
			});
		});
				/*日历刷新验证*/
		function refreshValidator(id,name){
		$(id).data('bootstrapValidator').updateStatus(name,
		'NOT_VALIDATED',null).validateField(name);
		}
	</script>
</body>

</html>