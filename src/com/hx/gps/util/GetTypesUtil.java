package com.hx.gps.util;

import com.hx.gps.entities.tool.Alarm;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：刘金龙
 * @date ：Created By 2020/11/12 18:46
 * @description：
 * @modified By：
 * @version: $
 */
public class GetTypesUtil extends Thread{
    private static List<Thread> runningThreads = new ArrayList<Thread>();
    private String type;
    private List<Alarm> alarms;

    @Override
    public void run() {
        regist(this);
        DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ALARM);
        DBCursor dbCursor = coll.find(new BasicDBObject("alarmType", type)).limit(1).sort(new BasicDBObject("time", -1));
        if (dbCursor.toArray().size() != 0) {
            DBObject dbObject = dbCursor.toArray().get(0);
            Alarm alarm = new Alarm();
            alarm.setAlarmType(dbObject.get("alarmType").toString());
            alarm.setTime(dbObject.get("time").toString());
            alarms.add(alarm);
        }
        unRegist(this);
    }

    public void regist(Thread t){
        synchronized(runningThreads){
            runningThreads.add(t);
        }
    }
    public void unRegist(Thread t){
        synchronized(runningThreads){
            runningThreads.remove(t);
        }
    }
    public static boolean hasThreadRunning() {
        return (runningThreads.size() > 0);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Alarm> getAlarms() {
        return alarms;
    }

    public void setAlarms(List<Alarm> alarms) {
        this.alarms = alarms;
    }
}
