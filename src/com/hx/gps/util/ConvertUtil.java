package com.hx.gps.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @Description: 类型转换工具类
 * @version: v1.0.0
 * @author: AN
 * @date: 2019年7月14日 下午5:03:14
 */
public class ConvertUtil {

	/**
	 * @Description: 利用反射，父类转子类，注意：此方法不是线程安全
	 * @version: v1.0.0
	 * @author: AN
	 * @date: 2019年7月14日 下午5:04:36 
	 */
	public static <T> T fatherToChild(T father, T child) throws Exception {
		if (child.getClass().getSuperclass() != father.getClass()) {
			throw new Exception("child 不是 father 的子类");
		}
		Class<?> fatherClass = father.getClass();
		Field[] declaredFields = fatherClass.getDeclaredFields();
		for (int i = 0; i < declaredFields.length; i++) {
			Field field = declaredFields[i];
			Method method = fatherClass.getDeclaredMethod("get" + upperHeadChar(field.getName()));
			Object obj = method.invoke(father);
			field.setAccessible(true);
			field.set(child, obj);
		}

		return child;
	}

	/**
	 * 首字母大写，in:deleteDate，out:DeleteDate
	 */
	public static String upperHeadChar(String in) {
		String head = in.substring(0, 1);
		String out = head.toUpperCase() + in.substring(1, in.length());
		return out;
	}
}
