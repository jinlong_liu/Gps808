/**
 * 
 */
package com.hx.gps.dao.impl;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hx.gps.dao.UserDao;
import com.hx.gps.entities.User;

/**
 * @author alex
 *
 */
@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public User findUserByAccount(String name, String password) {
		String hql = "FROM User WHERE name=:name AND password=:password";
		Query query = getSession().createQuery(hql);
		User user = (User) query.setString("name", name).setString("password", password).uniqueResult();
		return user;
	}

	@Override
	public void updatePass(User user, String newPassword) {

	}

	@Override
	public User findUserByName(String name) {
		String hql = "FROM User WHERE name=:name";
		Query query = getSession().createQuery(hql);
		User user = (User) query.setString("name", name).uniqueResult();
		//getSession().clear(); // 此处必须手动清除回话，否则报错
		return user;

	}

	@Override
	public User findUserById(Integer id) {
		String hql = "FROM User WHERE id=:id";
		Query query = getSession().createQuery(hql);
		User user = (User) query.setInteger("id", id).uniqueResult();
		//getSession().clear();
		return user;
	}
	

}
