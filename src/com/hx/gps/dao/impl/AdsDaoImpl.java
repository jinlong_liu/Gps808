package com.hx.gps.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hx.gps.dao.AdsDao;
import com.hx.gps.dao.TaxiDao;
import com.hx.gps.entities.PublicAdvs;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.adsCustomer;
import com.hx.gps.entities.adsTime;
import com.hx.gps.entities.tool.Advertise;
import com.hx.gps.entities.tool.WaitAdvertise;
import com.hx.gps.util.DBUtil;
import com.hx.gps.util.DateUtil;
import com.hx.gps.util.FarmConstant;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

@Repository
public class AdsDaoImpl implements AdsDao {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private TaxiDao taxiDao;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public boolean hasAdsCustomer(adsCustomer adsCustomer) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "adsCustomer");
		BasicDBObject adsUser = new BasicDBObject();
		adsUser.append("adsCustomerId", adsCustomer.getAdsCustomerId());
		System.out.println("删除客户" + adsCustomer);
		DBCursor cursor = coll.find(adsUser);
		if (cursor.hasNext()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<WaitAdvertise> findDetailsById(WaitAdvertise advertise) {
		// 准备待发送表
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "waitAdvertise");
		// 准备历史表
		DBCollection coll2 = DBUtil.getDBCollection(FarmConstant.MONGODB, "adsHistory");
		BasicDBObject query = new BasicDBObject();
		List<WaitAdvertise> advertises = new ArrayList<>();
		// 限制时间
		 Date start = DateUtil.stringToDate(advertise.getEndTime());
		 long reEnd=start.getTime()+10000;
		 Date end= new Date(reEnd);
	 	 query.append("time", new BasicDBObject().append("$gte", start).append("$lte", end));
		// query.append("time",date);
		// 限制type
		String type = advertise.getType();
		if (type.equalsIgnoreCase("公益广告")) {
			query.append("public", 1).append("playId", advertise.getPlayId());
		} else if (type.equalsIgnoreCase("指令下发")) {
			query.append("instructId", advertise.getPlayId());
		} else {
			query.append("playId", advertise.getPlayId());
		}
		// 准备结果集
		DBCursor cursor = coll.find(query);
		// 准备结果集2
		DBCursor cursor2 = coll2.find(query);
		// 准备查询条件
		// 首先遍历待发送表
		while (cursor.hasNext()) {
			BasicDBObject temp = (BasicDBObject) cursor.next();
			WaitAdvertise wait = new WaitAdvertise();
			// 判断类型
			if (temp.containsField("public")) {
				wait.setType("公益广告");
				wait.setPlayId(temp.getInt("playId"));
			} else if (temp.containsField("instructId")) {
				wait.setType("指令下发");
				wait.setPlayId(temp.getInt("instructId"));
			} else {
				wait.setType("广告下发");
				wait.setPlayId(temp.getInt("playId"));
			}
			// 判断失败次数
			if (temp.containsField("failNum")) {
				wait.setFailNum(temp.getInt("failNum"));
			} else {
				wait.setFailNum(0);
			}
			// 判断当前状态
			int state = temp.getInt("state");
			if (state == 0) {
				wait.setState("未处理");
			} else if (state == 1) {
				wait.setState("处理中");
			}
			// 判断时间
			if (temp.containsField("failTime")) {
				wait.setEndTime(DateUtil.dateToString(temp.getDate("failTime")));
			} else {
				wait.setEndTime(DateUtil.dateToString(temp.getDate("time")));
				 
			}
			// 转换车牌
			String isu = temp.getString("isu");
			wait.setIsuNum(isu);
			Taxi taxi = taxiDao.getTaxiByIsu(isu);
			if (taxi.getTaxiNum() != null) {
				wait.setTaxiNum(taxi.getTaxiNum());
			}
			// 添加操作人
			wait.setAdsName(advertise.getAdsName());
			advertises.add(wait);
		}
		// 查询历史表
		while (cursor2.hasNext()) {
			BasicDBObject temp = (BasicDBObject) cursor2.next();
			WaitAdvertise wait = new WaitAdvertise();
			// 判断类型
			if (temp.containsField("public")) {
				wait.setType("公益广告");
				wait.setPlayId(temp.getInt("playId"));
			} else if (temp.containsField("instructId")) {
				wait.setType("指令下发");
				wait.setPlayId(temp.getInt("instructId"));
			} else {
				wait.setType("广告下发");
				wait.setPlayId(temp.getInt("playId"));
			}
			// 判断失败次数
			if (temp.containsField("failNum")) {
				wait.setFailNum(temp.getInt("failNum"));
			} else {
				wait.setFailNum(0);
			}
			// 判断当前状态
			int state = temp.getInt("state");
			if (state == 0) {
				wait.setState("发送失败");
			} else if (state == 1) {
				wait.setState("发送失败");
			} else if (state == 2) {
				wait.setState("发送成功");
			}
			// 判断时间
			if (temp.containsField("endtime")) {
				wait.setEndTime(DateUtil.dateToString(temp.getDate("endtime")));
			}
			// 转换车牌
			String isu = temp.getString("isu");
			wait.setIsuNum(isu);
			Taxi taxi = taxiDao.getTaxiByIsu(isu);
			if (taxi.getTaxiNum() != null) {

				wait.setTaxiNum(taxi.getTaxiNum());
			}
			// 添加操作人
			wait.setAdsName(advertise.getAdsName());
			advertises.add(wait);
		}
		//对集合中的 time字段进行倒序排列，保证最新的在前边
		 Collections.sort(advertises,new Comparator<WaitAdvertise>(){
				@Override
				public int compare(WaitAdvertise o1, WaitAdvertise o2) {
					return o2.getEndTime().compareTo(o1.getEndTime());
				} 
	        });
		return advertises;
	}

	@Override
	public boolean updatePublicAds(PublicAdvs advs) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "advertise");
		BasicDBObject ads = new BasicDBObject();
		ads.append("playId", Integer.valueOf(advs.getPlayId())).append("public", Integer.valueOf(1));
		DBCursor cursor = coll.find(ads);
		if (cursor.hasNext()) {
			DBObject setValue = new BasicDBObject();
			setValue.put("adsContext", advs.getAdsContext());
			setValue.put("playRate", Integer.valueOf(advs.getPlayRate()));
			setValue.put("playMode", Integer.valueOf(advs.getPlayMode()));
			DBObject upsertValue = new BasicDBObject("$set", setValue);
			coll.update(ads, upsertValue, true, true);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateAdsCustomer(adsCustomer adsCustomer) {
		// 准备表
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "adsCustomer");
		BasicDBObject adsUser = new BasicDBObject();
		adsUser.append("adsCustomerId", adsCustomer.getAdsCustomerId());
		System.out.println("adsCustomer" + adsCustomer);
		DBCursor cursor = coll.find(adsUser);
		if (cursor.hasNext()) {
			DBObject setValue = new BasicDBObject();

			// 设置新的用户名。
			setValue.put("adsCustomerName", adsCustomer.getAdsCustomerName());
			// 设置新的手机号。
			setValue.put("adsCustomerPhone", adsCustomer.getAdsCustomerPhone());
			DBObject upsertValue = new BasicDBObject("$set", setValue);

			coll.update(adsUser, upsertValue, true, true);
			return true;
		} else {
			// 当前用户id不存在
			return false;
		}
	}
@Override
public List<WaitAdvertise> getHistoryByTime(String startDate, String endDate) {
	// 准备待发送表
	DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "sendHistory");
	// 准备接收对象
	BasicDBObject query = new BasicDBObject();
	// 准备集合
	List<WaitAdvertise> advertises = new ArrayList<>();
	//按照时间的倒序排列
	DBObject condition=new BasicDBObject();
	DBObject object=new BasicDBObject();
	object.put("time", -1);
	if(!startDate.equals("")){
		Date start=DateUtil.stringToDate(startDate);
		condition.put("$gte", start);
	}
	if(!endDate.equals("")){
		Date end=DateUtil.stringToDate(endDate);
		condition.put("$lte", end);
	}
	query.append("time",condition);
	//查询数据
	DBCursor cursor = coll.find(query).sort(object);
	//遍历结果集合
	while (cursor.hasNext()) {
		query = (BasicDBObject) cursor.next();
		WaitAdvertise advertise = new WaitAdvertise();
		if (query.containsField("public")) {
			advertise.setType("公益广告");
			advertise.setPlayId(query.getInt("playId"));
		} else if (query.containsField("instructId")) {
			advertise.setType("指令下发");
			advertise.setPlayId(query.getInt("instructId"));
		} else {
			advertise.setType("广告下发");
			advertise.setPlayId(query.getInt("playId"));
		}
		advertise.setAdsName(query.getString("user"));
		// 记录发送时间
		Date date = query.getDate("time");
		String format = DateUtil.dateToString(date);
		advertise.setEndTime(format);
		// 加入集合
		advertises.add(advertise);
		//打印信息 
	}
	return advertises;
}
	@Override
	public List<WaitAdvertise> getAllHistory() {
		// 准备待发送表
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "sendHistory");
		// 准备接收对象
		BasicDBObject query = new BasicDBObject();
		// 准备集合
		List<WaitAdvertise> advertises = new ArrayList<>();
		DBObject object=new BasicDBObject();
		object.put("time", -1);
		//按时间的倒序进行排列和显示
		DBCursor cursor = coll.find().sort(object);
		while (cursor.hasNext()) {
			query = (BasicDBObject) cursor.next();
			WaitAdvertise advertise = new WaitAdvertise();
			if (query.containsField("public")) {
				advertise.setType("公益广告");
				advertise.setPlayId(query.getInt("playId"));
			} else if (query.containsField("instructId")) {
				advertise.setType("指令下发");
				advertise.setPlayId(query.getInt("instructId"));
			} else {
				advertise.setType("广告下发");
				advertise.setPlayId(query.getInt("playId"));
			}
			advertise.setAdsName(query.getString("user"));
			// 记录发送时间
			Date date = query.getDate("time");
			String format = DateUtil.dateToString(date);
			advertise.setEndTime(format);
			// 加入集合
			advertises.add(advertise);
			//打印信息 
		}
		return advertises;
	}
@Override
public List<WaitAdvertise> getTempHistory() {
			// 准备待发送表
			DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "sendHistory");
			// 准备接收对象
			BasicDBObject query = new BasicDBObject();
			// 准备集合
			List<WaitAdvertise> advertises = new ArrayList<>();
			DBObject object=new BasicDBObject();
			object.put("time", -1);
			//按时间的倒序进行排列和显示最新的一条
			DBCursor cursor = coll.find().sort(object).limit(1);
			while (cursor.hasNext()) {
				query = (BasicDBObject) cursor.next();
				WaitAdvertise advertise = new WaitAdvertise();
				if (query.containsField("public")) {
					advertise.setType("公益广告");
					advertise.setPlayId(query.getInt("playId"));
				} else if (query.containsField("instructId")) {
					advertise.setType("指令下发");
					advertise.setPlayId(query.getInt("instructId"));
				} else {
					advertise.setType("广告下发");
					advertise.setPlayId(query.getInt("playId"));
				}
				advertise.setAdsName(query.getString("user"));
				// 记录发送时间
				Date date = query.getDate("time");
				String format = DateUtil.dateToString(date);
				advertise.setEndTime(format);
				// 加入集合
				advertises.add(advertise);
				//打印信息 
			}
			return advertises;
}
	@Override
	public boolean updateAds(Advertise advertise) {
		// 准备表
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "advertise");
		System.out.println("dao：" + advertise.toString());
		BasicDBObject ads = new BasicDBObject();
		ads.append("playId", advertise.getPlayId());
		DBCursor cursor = coll.find(ads);
		if (cursor.hasNext()) {
			DBObject setValue = new BasicDBObject();
			setValue.put("adsName", advertise.getAdsName());
			setValue.put("adsContext", advertise.getAdsContext());
			setValue.put("adsCustomer", advertise.getAdsCustomer());
			setValue.put("playRate", advertise.getPlayRate());
			setValue.put("playStop", advertise.getPlayStop());
			setValue.put("playMode", advertise.getPlayMode());
			setValue.put("playNumber", advertise.getPlayNumber());

			setValue.put("playFontType", advertise.getPlayFontType());
			setValue.put("playFontSize", advertise.getPlayFontSize());
			setValue.put("playFontColor", advertise.getPlayFontColor());
			setValue.put("playType", advertise.getPlayType());
			DBObject upsertValue = new BasicDBObject("$set", setValue);
			coll.update(ads, upsertValue, true, true);
			return true;
		} else {

			return false;
		}
	}

	@Override
	public boolean deleteAdsCustomerByid(int adsCustomerId) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "adsCustomer");
		BasicDBObject delectCustomer = new BasicDBObject();
		delectCustomer.append("adsCustomerId", adsCustomerId);
		DBCursor cursor = coll.find(delectCustomer);
		if (cursor.hasNext()) {
			coll.remove(delectCustomer);
			return true;
		} else {
			return false;
		}

	}

	@Override
	public List<Advertise> getAllAdvertises() {
		System.out.println("正在获得所有广告信息");
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "advertise");

		List<Advertise> advertises = new ArrayList<>();
		DBObject query=new BasicDBObject();
		query.put("playId", 1);
		DBCursor cursor = coll.find().sort(query);
		while (cursor.hasNext()) {
			BasicDBObject object = (BasicDBObject) cursor.next();
			if (!object.containsField("public")) {
				Advertise advertise = new Advertise(object);
				advertises.add(advertise);
			}
		}
		System.out.println("dao" + advertises);
		return advertises;
	}

	@Override
	public boolean SendAds(List<String> isuNums, int id) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "waitAdvertise");
		// 存储到发送给记录表
		DBCollection coll2 = DBUtil.getDBCollection(FarmConstant.MONGODB, "sendHistory");
		// 依次存储至mongo。
		BasicDBObject history = new BasicDBObject();
		Date time=new Date();
		// System.out.println("下发终端如下" + isuNums);
		for (String isuNum : isuNums) {
			BasicDBObject object = new BasicDBObject();
			object.append("isu", isuNum).append("playId", id).append("time", time).append("state", 0);
			coll.insert(object);
			object.clear();
		}
		history.append("type", "广告下发").append("playId", id).append("time", time).append("user", SecurityUtils.getSubject().getPrincipal().toString());
		coll2.insert(history);
		return true;
	}

	@Override
	public boolean SendInstruct(List<String> isuNums, int instruct, int play) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "waitAdvertise");
		// 依次存储至mongo。
		// 存储到发送给记录表
		DBCollection coll2 = DBUtil.getDBCollection(FarmConstant.MONGODB, "sendHistory");
		// 依次存储至mongo。
		Date time=new Date();
		BasicDBObject history = new BasicDBObject();
		System.out.println("下发终端如下" + isuNums);
		for (String isuNum : isuNums) {
			BasicDBObject object = new BasicDBObject();
			object.append("isu", isuNum).append("instructId", instruct).append("deleteId", play)
					.append("time", time).append("state", 0);
			coll.insert(object);
			object.clear();
		}
		history.append("type", "指令下发").append("instructId", instruct).append("time", time).append("user",
				SecurityUtils.getSubject().getPrincipal().toString());
		coll2.insert(history);
		return true;
	}

	@Override
	public boolean SendPublicAds(List<String> isuNums, int id) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "waitAdvertise");
		// 依次存储至mongo。
		DBCollection coll2 = DBUtil.getDBCollection(FarmConstant.MONGODB, "sendHistory");
		// 依次存储至mongo。
		BasicDBObject history = new BasicDBObject();
		Date date=new Date();
		System.out.println("下发终端如下" + isuNums);
		for (String isuNum : isuNums) {
			BasicDBObject object = new BasicDBObject();
			object.append("isu", isuNum).append("public", 1).append("playId", id).append("time", date)
					.append("state", 0);
			try {
				coll.insert(object);
			} catch (Exception e) {
				System.out.println("给终端" + isuNum + "发送广告" + id + "时出现异常");
				return false;
			}
			object.clear();
		}
		history.append("type", "公益广告").append("playId", id).append("public", 1).append("time", date)
				.append("user", SecurityUtils.getSubject().getPrincipal().toString());
		coll2.insert(history);
		return true;
	}

	// 验证广告id
	@Override
	public boolean overflow() {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "advertise");
		BasicDBObject ads = new BasicDBObject();
		ads.append("public", new BasicDBObject().append("$exists", false));
		// 准备结果对象
		DBCursor cursor2 = null;
		cursor2 = coll.find(ads);
		int count = cursor2.count();
		if (count >= 25) {
			System.out.println("广告超过25条");
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean hasAdsId(int playId) {

		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "advertise");
		// 准备主对象
		BasicDBObject adsOb = new BasicDBObject();
		// 准备结果对象
		DBCursor cursor = null;
		adsOb.append("playId", playId);
		try {
			cursor = coll.find(adsOb);
			if (cursor.hasNext()) {
				System.out.println("广告添加失败，id已存在");
				return true;
			}
		} catch (Exception e) {
			System.out.println("广告添加时出现错误");
			return true;
		}
		return false;
	}

	@Override
	public List<PublicAdvs> getAllPublicAds() {
		System.out.println("正在获得所有公益广告");
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "advertise");
		List<PublicAdvs> advs = new ArrayList<>();
		BasicDBObject ob = new BasicDBObject();
		DBCursor cursor = null;
		ob.append("public", 1);
		//排序   顺序排列
		DBObject query=new BasicDBObject();
		query.put("playId", 1);
		if (coll.find(ob) != null) {
			cursor =coll.find(ob).sort(query);
			while (cursor.hasNext()) {
				BasicDBObject object = (BasicDBObject) cursor.next();
				PublicAdvs advertise = new PublicAdvs(object);

				advs.add(advertise);
			}
		}

		return advs;

	}

	// 添加公益广告
	@Override
	public boolean savePublicAdvs(PublicAdvs advs) {
		// 准备集合
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "advertise");
		// 准备对象
		BasicDBObject advsob = new BasicDBObject();
		advsob.append("public", 1).append("playId", advs.getPlayId()+25).append("playMode", advs.getPlayMode())
				.append("playRate", advs.getPlayRate()).append("adsContext", advs.getAdsContext())
				.append("adsName", advs.getAdsName()).append("addTime", new Date());
		try {
			coll.save(advsob);
		} catch (Exception e) {
			System.out.println("写入数据库出现异常，公益广告");
			return false;
		}
		return true;
	}

	@Override
	// 添加广告
	public boolean addAds(Advertise advertise, String[] weekDay, String startYMD, String endYMD, String period,
			String immeAds, String noAdsTime) {
		// 时段相关的参数首先进行格式转化
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "advertise");
		// 准备主对象
		BasicDBObject adsOb = new BasicDBObject();
		// 准备周期对象
		BasicDBObject weekOb = new BasicDBObject();
		// 准备拼接字符串
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < weekDay.length; i++) {
			int day = Integer.parseInt(weekDay[i]);
			sb.append(day + 1 + "/");
			weekOb.append("weekDay" + i, day);

		}
		// 准备时间段
		/**
		 * 该版本是可多选的时候
		 */
		// BasicDBObject periodOb = new BasicDBObject();
		// if (period.length > 0) {
		// for (int i = 0; i < period.length; i++) {
		// System.out.println("数组" + period[i]);
		// int per = Integer.parseInt(period[i]);
		// periodOb.append("period" + i, per);
		// }
		// }
		// 转int ，存时间段id.
		int periodId = Integer.parseInt(period);
		String week = sb.toString();
		adsOb.append("addTime", new Date());
		adsOb.append("playId", advertise.getPlayId());
		adsOb.append("eachweek", week);
		adsOb.append("adsContext", advertise.getAdsContext());
		adsOb.append("adsCustomer", advertise.getAdsCustomer());
		adsOb.append("adsName", advertise.getAdsName());
		adsOb.append("adsType", advertise.getAdsType());
		adsOb.append("partHeight", advertise.getPartHeight());
		adsOb.append("partWidth", advertise.getPartWidth());
		adsOb.append("partX", advertise.getPartX());
		adsOb.append("partY", advertise.getPartY());
		adsOb.append("playFontColor", advertise.getPlayFontColor());
		adsOb.append("playFontSize", advertise.getPlayFontSize());
		adsOb.append("playFontType", advertise.getPlayFontType());
		adsOb.append("playMode", advertise.getPlayMode());
		adsOb.append("playNumber", advertise.getPlayNumber());
		adsOb.append("playRate", advertise.getPlayRate());
		adsOb.append("playStop", advertise.getPlayStop());
		adsOb.append("playTimeType", advertise.getPlayTimeType());
		adsOb.append("playType", advertise.getPlayType());
		adsOb.append("weekDay", weekOb);
		adsOb.append("periodId", periodId);
		// 开始时间
		String[] startTime = startYMD.split("-");
		adsOb.append("startYMD", startYMD);
		// 开始年份
		// String StartYear = startTime[0];
		int StartYear = Integer.parseInt(startTime[0]);
		adsOb.append("startYear", StartYear);
		// 开始月份
		// String StartMonth = startTime[1];
		int StartMonth = Integer.parseInt(startTime[1]);
		adsOb.append("startMonth", StartMonth);
		// 开始天
		// String StartDay = startTime[2];
		int StartDay = Integer.parseInt(startTime[2]);
		adsOb.append("startDay", StartDay);
		// 结束时间
		String[] endTime = endYMD.split("-");
		adsOb.append("endYMD", endYMD);
		// 结束年份
		// String endYear = startTime[0];
		int endYear = Integer.parseInt(endTime[0]);
		adsOb.append("endYear", endYear);
		// 结束月份
		// String endMonth = startTime[1];
		int endMonth = Integer.parseInt(endTime[1]);
		adsOb.append("endMonth", endMonth);
		// 结束天
		// String endDay = startTime[2];
		int endDay = Integer.parseInt(endTime[2]);
		adsOb.append("endDay", endDay);
		adsOb.append("immeAds", immeAds);
		adsOb.append("noAdsTime", noAdsTime);
		try {
			coll.save(adsOb);
			return true;
		} catch (Exception e) {
			System.out.println("存储数据时出现异常" + e.getMessage());
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public List<adsCustomer> getAllAdsCustomer() {
		// 准备表
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "adsCustomer");
		List<adsCustomer> adsCustomers = new ArrayList<>();
		DBCursor cursor = coll.find();
		while (cursor.hasNext()) {
			BasicDBObject adsCustomer = (BasicDBObject) cursor.next();
			adsCustomer ads = new adsCustomer(adsCustomer);
			adsCustomers.add(ads);
		}
		// 准备结果集
		return adsCustomers;
	}

	@Override
	public void saveAdsCustomer(adsCustomer adsCustomer) {
		// 准备表
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "adsCustomer");
		// 准备数据对象
		BasicDBObject object = new BasicDBObject();
		object.append("adsCustomerId", adsCustomer.getAdsCustomerId())
				.append("adsCustomerName", adsCustomer.getAdsCustomerName())
				.append("adsCustomerPhone", adsCustomer.getAdsCustomerPhone()).append("addTime", new Date());
		// 添加操作
		try {
			coll.insert(object);
		} catch (Exception e) {
			System.out.println("客户添加时出现异常" + e.getStackTrace());
		}
	}

	@Override
	public boolean hasAdsControl(adsTime adsTime) {
		// 准备表
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "adsTimeControl");
		BasicDBObject contion = new BasicDBObject();
		contion.append("adsTimeId", adsTime.getAdsTimeId());
		DBCursor cursor = coll.find(contion);
		if (cursor.hasNext()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void saveAdsControl(adsTime adsTime) {
		// 准备表
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "adsTimeControl");
		// 准备数据对象
		BasicDBObject object = new BasicDBObject();
		object.append("adsTimeId", adsTime.getAdsTimeId()).append("adsTimeName", adsTime.getAdsTimeName())
				.append("adsTimeStart", adsTime.getAdsTimeStart()).append("adsTimeEnd", adsTime.getAdsTimeEnd());
		// 添加操作
		try {
			coll.insert(object);
		} catch (Exception e) {
			System.out.println("时段添加时出现异常" + e.getStackTrace());
		}
	}

	@Override
	public List<adsTime> getAllAdsTimes() {
		// 准备表
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "adsTimeControl");
		List<adsTime> adsTime = new ArrayList<>();
		DBCursor cursor = coll.find();
		while (cursor.hasNext()) {
			// 如果数据就返回、
			BasicDBObject adsTimeOb = (BasicDBObject) cursor.next();
			adsTime adstime = new adsTime(adsTimeOb);
			adsTime.add(adstime);
		}
		// 准备结果集
		return adsTime;
	}

	@Override
	public List<adsTime> getAdsTimes(String adsTimeName) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "adsTimeControl");

		DBObject condition = new BasicDBObject();
		condition.put("adsTimeName", adsTimeName);
		// BasicDBObject finder = (BasicDBObject) coll.findOne(condition);
		DBCursor cursor = coll.find(condition);
		List<adsTime> adsTimes = new ArrayList<>();
		while (cursor.hasNext()) {
			BasicDBObject ads = (BasicDBObject) cursor.next();
			adsTime adsTime = new adsTime(ads);
			adsTimes.add(adsTime);
		}
		return adsTimes;
	}

	@Override
	public boolean updateAdsTime(adsTime adsTime) {
		// 准备表
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "adsTimeControl");
		BasicDBObject adsTimeOb = new BasicDBObject();
		adsTimeOb.append("adsTimeId", adsTime.getAdsTimeId());
		System.out.println("adsTime" + adsTime);
		DBCursor cursor = coll.find(adsTimeOb);
		if (cursor.hasNext()) {
			DBObject setValue = new BasicDBObject();
			setValue.put("adsTimeName", adsTime.getAdsTimeName());
			// 设置开始时间
			setValue.put("adsTimeStart", adsTime.getAdsTimeStart());
			// 设置结束时间
			setValue.put("adsTimeEnd", adsTime.getAdsTimeEnd());
			DBObject upsertValue = new BasicDBObject("$set", setValue);
			coll.update(adsTimeOb, upsertValue, true, true);
			return true;
		} else {
			// 当前时段id不存在
			return false;
		}
	}

	@Override
	public boolean deleteAdsControlByid(int adsTimeId) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "adsTimeControl");
		BasicDBObject deleteAdsTime = new BasicDBObject();
		deleteAdsTime.append("adsTimeId", adsTimeId);
		DBCursor cursor = coll.find(deleteAdsTime);
		if (cursor.hasNext()) {
			coll.remove(deleteAdsTime);
			return true;
		} else {
			return false;
		}

	}

	@Override
	public boolean deleteAdsById(int adsId) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "advertise");
		BasicDBObject ads = new BasicDBObject();
		ads.append("playId", adsId);
		DBCursor cursor = coll.find(ads);
		if (cursor.hasNext()) {
			coll.remove(ads);
			return true;
		} else {
			return false;
		}

	}
}
