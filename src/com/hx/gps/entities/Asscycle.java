package com.hx.gps.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Asscycle entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "asscycle")
public class Asscycle implements java.io.Serializable {

	// Fields
	private Integer id;
	private Integer initScorCar;
	private Integer initScorPany;
	private Integer initScorer;
	private Integer lowCar;
	private Integer lowPany;
	private Integer lower;
	private String name;
	private Integer topCar;
	private Integer topPany;
	private Integer toper;

	// Constructors

	/** default constructor */
	public Asscycle() {
	}

	/** full constructor */
	public Asscycle(Integer initScorCar, Integer initScorPany,
			Integer initScorer, Integer lowCar, Integer lowPany, Integer lower,
			String name, Integer topCar, Integer topPany, Integer toper) {
		this.initScorCar = initScorCar;
		this.initScorPany = initScorPany;
		this.initScorer = initScorer;
		this.lowCar = lowCar;
		this.lowPany = lowPany;
		this.lower = lower;
		this.name = name;
		this.topCar = topCar;
		this.topPany = topPany;
		this.toper = toper;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "initScorCar")
	public Integer getInitScorCar() {
		return this.initScorCar;
	}

	public void setInitScorCar(Integer initScorCar) {
		this.initScorCar = initScorCar;
	}

	@Column(name = "initScorPany")
	public Integer getInitScorPany() {
		return this.initScorPany;
	}

	public void setInitScorPany(Integer initScorPany) {
		this.initScorPany = initScorPany;
	}

	@Column(name = "initScorer")
	public Integer getInitScorer() {
		return this.initScorer;
	}

	public void setInitScorer(Integer initScorer) {
		this.initScorer = initScorer;
	}

	@Column(name = "lowCar")
	public Integer getLowCar() {
		return this.lowCar;
	}

	public void setLowCar(Integer lowCar) {
		this.lowCar = lowCar;
	}

	@Column(name = "lowPany")
	public Integer getLowPany() {
		return this.lowPany;
	}

	public void setLowPany(Integer lowPany) {
		this.lowPany = lowPany;
	}

	@Column(name = "lower")
	public Integer getLower() {
		return this.lower;
	}

	public void setLower(Integer lower) {
		this.lower = lower;
	}

	@Column(name = "name", length = 20)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "topCar")
	public Integer getTopCar() {
		return this.topCar;
	}

	public void setTopCar(Integer topCar) {
		this.topCar = topCar;
	}

	@Column(name = "topPany")
	public Integer getTopPany() {
		return this.topPany;
	}

	public void setTopPany(Integer topPany) {
		this.topPany = topPany;
	}

	@Column(name = "toper")
	public Integer getToper() {
		return this.toper;
	}

	public void setToper(Integer toper) {
		this.toper = toper;
	}

	@Override
	public String toString() {
		return "Asscycle [id=" + id + ", initScorCar=" + initScorCar
				+ ", initScorPany=" + initScorPany + ", initScorer="
				+ initScorer + ", lowCar=" + lowCar + ", lowPany=" + lowPany
				+ ", lower=" + lower + ", name=" + name + ", topCar=" + topCar
				+ ", topPany=" + topPany + ", toper=" + toper + "]";
	}

}