<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">
	
<script src="js/jquery.js"></script>
<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">

<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>

<!-- gy 添加表单验证 -->
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />
<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>
<!-- 添加模糊查询 -->
<script src=" js/select2.js"></script>
<link href=" css/select2.css" rel="stylesheet" />

<style>
    /*防止select2不会自动失去焦点*/
    .select2-container {
        z-index: 16000 !important;
    }
 
    .select2-drop-mask {
        z-index: 15990 !important;
    }
 
    .select2-drop-active {
        z-index: 15995 !important;
    }
    /*select2在Bootstrap的modal中默认被遮盖，现在强制显示在最前*/
    .select2-drop {
        z-index: 10050 !important;
    }
 
    .select2-search-choice-close {
        margin-top: 0 !important;
        right: 2px !important;
        min-height: 10px;
    }
 
    .select2-search-choice-close:before {
            color: black !important;
        }
    </style>

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">
		<!-- 查询列表 -->
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>输入条件查询黑名单</h5>
				</div>
				<div class="ibox-content">
					<form action="getBlacklist" class="form-horizontal" method="post">
						<div class="row">
							<div class="form-group">
								<!-- 公司 -->
								<label class="col-sm-1 control-label">类别：</label>
								<div class="col-sm-3">
									<select name="type" class="form-control">
										<option value="0">报废车辆</option>
										<option value="1">未年检车辆</option>
										<option value="2">违法未处理车辆</option>
									</select>
								</div>
								<label class="col-sm-1 control-label">车牌：</label>
								<div class="col-sm-3">
									<input type="text" class="form-control" name="taxiNum" autocomplete="off">
								</div>
								<div class="col-sm-2">
									<button type="submit" class="btn btn-primary">查询</button>

									<shiro:hasPermission name="blacklist:admin">
										<button type="button" class="btn btn-primary"
											data-toggle="modal" data-target="#myModal5"  >添加</button>
									</shiro:hasPermission>
								</div>

							</div>
						</div>
					</form>

				</div>
			</div>
		</div>

		<!-- 点击添加按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal5" 
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form action="saveBlacklist" class="form-horizontal" method="post">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>新增黑名单 <span style="color:red">（* 为必填项）</span></h5>
							</div>
							<div class="ibox-content">
								<div class="row">
									<div class="form-group">
										<!-- 车辆 -->
										<!-- 添加模糊查询 -->
										<label class="col-sm-2 control-label"><span style="color:red">*  </span>车辆 ：</label>
										<div class="col-sm-8">
		
												<select class="form-control js-example-basic-single" name="taxiNum" id="taxiSelect"> 
													 <c:forEach items="${sessionScope.taxis}" var="taxis">
									 			<option value="${taxis.taxiNum}">${taxis.taxiNum}</option>
								     		 		</c:forEach>  
												</select>
										</div>
									</div>
									<!-- 类别 -->
									<div class="form-group">
										<label class="col-sm-2 control-label"> <span style="color:red">*  </span>类别 ：</label>
										<div class="col-sm-8">
											<select name="type" class="form-control">
												<option value="0">报废车辆</option>
												<option value="1">未年检车辆</option>
												<option value="2">违法未处理车辆</option>
											</select>
										</div>
									</div>
									<!-- 备注 -->
									<div class="form-group">
										<label class="col-sm-2 control-label">备注：</label>
										<div class="col-sm-8">
											<textarea style="height: 60px;resize: none;" name="remark"
												class="form-control" aria-required="true"></textarea>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-white"
										data-dismiss="modal">关闭</button>
									<button type="submit" class="btn btn-primary">保存</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- 数据展示 -->
		<c:if test="${blacklists!=null}">
			<div class="row"  >
				<div class="col-sm-12">
					<div class="ibox-title">
						<div>
							<h5>&nbsp;&nbsp;&nbsp;车辆黑名单表</h5>

						</div>
					</div>
					<div class="ibox-content" style="height: 660px">
						<table id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
					
							<thead>
								<tr>
									<th width="70">车牌号</th>
									<th width="70">类型</th>
									<th width="70">备注</th>
									<th width="70">添加时间</th>
									<!-- 4 -->

									<th width="100">操作</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${blacklists}" var="blacklists">
									<tr>
										<td>${blacklists.taxiNum}</td>
										<td><c:if test="${blacklists.type==0 }">报废车辆</c:if> <c:if
												  test="${blacklists.type==1 }">未年检车辆 </c:if> <c:if
												  test="${blacklists.type==2 }">违法未处理车辆</c:if></td>
										<td>${blacklists.remark }</td>
										<td>${blacklists.time }</td>
										<!-- 4 -->

										<td><shiro:hasPermission name="blacklist:admin">
												<button id="btn-update" type="button"
													class="btn btn-success btn-sm" data-toggle="modal"
													data-target="#myModal1"
													onclick="update('${blacklists.id }','${blacklists.taxiNum }','${blacklists.remark }')">修改</button>
												<button type="button" class="btn btn-danger btn-sm"
													onclick="delete1('${blacklists.id}','${blacklists.taxiNum}')">删除</button>
											</shiro:hasPermission></td>
									</tr>
								</c:forEach>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="5">
										<ul class="pagination pull-right"></ul>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</c:if>
		<!-- 删除表格 -->
		<form id="deleteForm" action="deleteBlacklist" method="post">
			<input type="hidden" id="deleteId" name="id">
		</form>

		<!-- 点击"修改"按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal1" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form action="updateBlacklist" class="form-horizontal"
						method="post">
						<input id="id" name="id" type="hidden">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>修改黑名单信息 <span style="color:red">（* 为必填项）</span></h5>
							</div>
							<div class="ibox-content">
								<div class="row">
									<div class="form-group">
										<!-- 车辆 -->
										<label class="col-sm-2 control-label"><span style="color:red">*  </span>车辆 ：</label>
										<div class="col-sm-8">
											<input name="taxiNum" class="form-control" id="taxiNum"
											readonly="readonly">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<!-- 类别 -->
										<label class="col-sm-2 control-label"><span style="color:red">*  </span>类别 ：</label>
										<div class="col-sm-8">
											<select class="form-control" name="type">
												<option value="0">报废车辆</option>
												<option value="1">未年检车辆</option>
												<option value="2">违法未处理车辆</option>
											</select>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="form-group">
										<!-- 备注 -->
										<label class="col-sm-2 control-label">备注：</label>
										<div class="col-sm-8">
											<textarea style="height: 60px;resize: none;" name="remark" id="remark"
												class="form-control" aria-required="true"></textarea>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-white"
										data-dismiss="modal">关闭</button>
									<button type="submit" class="btn btn-primary">保存</button>
								</div>
							</div>

						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script>
	//添加模糊查询
	
	  $.fn.modal.Constructor.prototype.enforceFocus = function() {};
	    //加载footable的功能
		$(document).ready(function() {
	  $(".js-example-basic-single").select2({width:"100%"});	
		$('#table1').DataTable({
			"pagingType" : "full_numbers"
		});
		
		var result='<%=request.getAttribute("result")%>';
		if(result != null && result != "null"){
			if (result.indexOf("成功")>0) {
			swal({
				title : result,
				type : "success"
			});
		}else{
			swal({
				title :result,
				type : "warning"
			});
		}
		}
		
	});

		//点击"添加"按钮
		function save() {
			//修改车牌号下拉框的值
			$.post("getAllTaxiByAjax", function(data) {
				if (data.length != 0) {
					//返回的数据不为空 
					var newdata = eval("(" + data + ")");
					$("#taxiSelect").html("");//避免选择框中的值持续叠加
					/* $("<option value='"+taxiId+"'>" + taxiNum + "</option>")
							.appendTo($("#taxiSelect")); */
					$("<option> _$tag____").appendTo($("#taxiSelect"));
					for (var i = 0; i < data.length; i++) {
						$("_$tag___ " + newdata[i].taxiNum + "_$tag____")
								.appendTo($("#taxiSelect"));
					}
				}
			});
		}

		//点击"修改"按钮
		function update(id, taxiNum, remark) {
			$("#id").val(id);
			$("#taxiNum").val(taxiNum);
			$("#remark").val(remark);
		};

		//点击"删除"按钮
		function delete1(id, name) {
			swal({
				title : "删除确认",
				text : "您确定删除 " + name + " 的数据吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
				$("#deleteId").val(id);
				//提交表单
				$("#deleteForm").submit();
			});
		}
	</script>
</body>

</html>