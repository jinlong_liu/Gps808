package com.hx.gps.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateOptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hx.gps.dao.SerialDao;
import com.hx.gps.entities.Company;
import com.hx.gps.entities.Graph;
import com.hx.gps.entities.Preinfo;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.Taxitype;
import com.hx.gps.entities.tool.Address;
import com.hx.gps.entities.tool.ComInfo2;
import com.hx.gps.entities.tool.DatatablesQuery;
import com.hx.gps.entities.tool.DatatablesView;
import com.hx.gps.entities.tool.InfoQuery;
import com.hx.gps.entities.tool.Meter;
import com.hx.gps.entities.tool.OnlineRate;
import com.hx.gps.entities.tool.Point;
import com.hx.gps.entities.tool.PointsInfo;
import com.hx.gps.entities.tool.TaxiDetails;
import com.hx.gps.entities.tool.TaxiInfo2;
import com.hx.gps.service.AddressService;
import com.hx.gps.service.InfoService;
import com.hx.gps.service.MeterService;
import com.hx.gps.service.TaxiService;
import com.hx.gps.service.impl.AddressServiceImpl;
import com.hx.gps.util.DateUtil;
import com.hx.gps.util.FarmConstant;
import com.hx.gps.util.MapUtil;

@Controller
public class AddressController {
	@Autowired
	private MeterService meterService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private MapUtil mapUtil;
	@Autowired
	private InfoService infoService;
	@Autowired
	private TaxiService taxiService;
	// 操作日志类
	private static Log logger = LogFactory.getLog(AddressServiceImpl.class);

	// 获取车辆详细状态
	@RequestMapping({ "/getTaxiDetails" })
	public void getTaxiDetails(HttpServletResponse response, String taxiNum) {

		String isu = "";
		TaxiDetails detail = new TaxiDetails();
		Taxi taxi = this.taxiService.getIsuByTaxi(taxiNum);
		if (taxi.getIsuNum() != null) {
			isu = taxi.getIsuNum();
			detail = this.addressService.getDetailsByisu(isu);
		}
		detail.setIsuNum(isu);
		detail.setTaxiNum(taxiNum);
		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().print(mapper.writeValueAsString(detail));
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("查看车辆详细信息出现异常：" + e.getMessage());
		}
	}

	@RequestMapping("/getTreeOfTempJson")
	public void getTreeOfTemp2(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		// 所有公司的车辆总数
		int totality = 0;
		try {
			// --------------按权限显示车辆(单公司或多公司)---------------------
			Subject subject = SecurityUtils.getSubject();
			// Session session = subject.getSession();
			List<Company> companies = new ArrayList<Company>();
			if (subject.isPermitted("allcompy:get")) {
				companies = infoService.getAllCompany();
			} else if (subject.isPermitted("bjcompy:get")) {
				companies = infoService.getAllCompanyBj();
			} else {
				String name = (String) subject.getPrincipal();

				// int id=(int)session.getAttribute("compyid");
				Company company = infoService.getComByUser(name);
				// System.out.println("公司id"+);
				if (company != null) {

					companies.add(company);
				}
			}
			logger.info("实时监控开始:");
			// --------------------------------------
			List<ComInfo2> comInfo2s = new ArrayList<>();
			for (Company c : companies) {
				Set<Taxi> taxis = c.getTaxis();
				Set<TaxiInfo2> taxis2 = new HashSet<>();

				Iterator<Taxi> it = taxis.iterator();
				int count = 0;
				Map<String, TaxiInfo2> stateMap = new HashMap<>();
				while (it.hasNext()) {
					Taxi next = it.next();
					TaxiInfo2 t2 = new TaxiInfo2(next.getId(), next.getTaxiNum());
					stateMap.put(next.getIsuNum(), t2);
					// gy添加逻辑，根据终端号获取在线和定位状态,将在线和定位状态放在TaxiInfo2中
					// if (taxiService.setTaxiStateByIsuNum(t2,
					// next.getIsuNum())) {
					// count++;
					// }
					// taxis2.add(t2);
				}
				try {
					count = taxiService.setTaxiStateByIsuNums(stateMap);
				} catch (NullPointerException e) {
					count = 0;
					logger.error("[获取车辆列表]空指针异常" + e.getMessage());
				} catch (Exception e) {
					count = 0;
					logger.error("[获取车辆列表]异常" + e.getMessage());
				}
				// int size = taxiService.getOnlineNumByCom(c.getId());
				taxis2.addAll(stateMap.values());
				if (taxis2.size() > 0) {
					for (TaxiInfo2 taxiInfo2 : taxis2) {
						if (taxiInfo2.getOnlineState() == null) {
							taxiInfo2.setOnlineState("不在线");
						}
						if (taxiInfo2.getPositioningState() == null) {
							taxiInfo2.setPositioningState("未定位");
						}
					}
				}
				comInfo2s.add(new ComInfo2(c.getId(), c.getName(), taxis2, count));
				stateMap.clear();
				// System.err.println("计算公司在线车辆数："+c.getName()+(System.currentTimeMillis()-startTime2));
				// 记录在线车辆数

			}
			// 计算所有公司的车辆数
			for (ComInfo2 comInfo : comInfo2s) {
				totality += comInfo.getCount();
				// 计算每个公司在线的车辆数
			}

			map.put("comInfo2s", comInfo2s);
			map.put("totality", totality);

			ObjectMapper objectMapper = new ObjectMapper();
			String asString = objectMapper.writeValueAsString(comInfo2s);
			// 拼接jstree用的json字符串
			StringBuilder builder = new StringBuilder("{\"text\":\"所有公司");
			builder.append("[");
			builder.append(totality);
			builder.append("]\"");
			builder.append(",\"state\" : {\"opened\": \"true\"}");
			builder.append(",\"children\":");
			builder.append(asString);
			builder.append(",\"id\":\"allcom\"}");
			System.err.println("在线轮询");
			// 设置编码
			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().write(builder.toString());
			// 异常捕捉
		} catch (TransactionException e) {
			// 登录出现异常
			logger.error("[获取车辆列表]事务建立异常" + e.getMessage());
		} catch (NullPointerException e) {
			logger.error("[获取车辆列表]空指针异常" + e.getMessage());
		} catch (Exception e) {
			logger.error("[获取车辆列表]异常" + e.getMessage());
		}

	}

	// 视频监控界面-由tempmoni界面请求数据返回到newtest界面
	@RequestMapping("/getTreeOfTempNew")
	public String getTreeOfTempNew(Map<String, Object> map, String taxiNum) {
		System.out.println("getTreeOfTempNew");
		// 所有公司的车辆总数
		logger.info("监控车辆视频:{}" + taxiNum);
		String isu = null;
		String isuNum = null;
		// 添加视频显示 (不需要进行切割)
		if (taxiService.getIsuByTaxi(taxiNum) != null) {
			isu = taxiService.getIsuByTaxi(taxiNum).getIsuNum();
			isuNum = isu.substring(1, isu.length());
		} else {
			isuNum = "defaultRRR";
		}
		// 根据车牌号获取终端号，拼接回显字符串。
		// http://defaultRRR.xstrive.com:8080/newhls/record/record0/index.m3u8
		if (taxiService.getIsuByTaxi(taxiNum).getCompany().getRemark().equalsIgnoreCase("new")) {
			map.put("vedio0", "http://" + isu + ".hxybvideo.com:8090/playback.html");
			map.put("vedio1", "http://" + isu + ".hxybvideo.com:8090/newhls/record/record0/index.m3u8");
			map.put("vedio2", "http://" + isu + ".hxybvideo.com:8090/newhls/record/record1/index.m3u8");
			map.put("vedio3", "http://" + isu + ".hxybvideo.com:8090/newhls/record/record2/index.m3u8");
			map.put("vedio4", "http://" + isu + ".hxybvideo.com:8090/newhls/record/record3/index.m3u8");
		} else {
			map.put("vedio0", "http://" + isu + ".xstrive.com:8080");
			map.put("vedio1", "http://" + isu + ".xstrive.com:8080/newhls/record/record0/index.m3u8");
			map.put("vedio2", "http://" + isu + ".xstrive.com:8080/newhls/record/record1/index.m3u8");
			map.put("vedio3", "http://" + isu + ".xstrive.com:8080/newhls/record/record2/index.m3u8");
			map.put("vedio4", "http://" + isu + ".xstrive.com:8080/newhls/record/record3/index.m3u8");
		}
		return "newtest";
	}

	// 带视频监控界面
	@RequestMapping("/getTreeOfTemp3")
	public String getTreeOfTemp3(Map<String, Object> map, String taxiNum) {
		// 所有公司的车辆总数
		logger.info("监控车辆视频:{}" + taxiNum);
		String isu = null;
		String isuNum = null;
		// 添加视频显示 (不需要进行切割)
		if (taxiService.getIsuByTaxi(taxiNum) != null) {
			isu = taxiService.getIsuByTaxi(taxiNum).getIsuNum();
			isuNum = isu.substring(1, isu.length());
		} else {
			isuNum = "defaultRRR";
		}

		if (taxiService.getIsuByTaxi(taxiNum).getCompany().getRemark().equalsIgnoreCase("new")) {
			map.put("vedio0", "http://" + isu + ".hxybvideo.com:8090/playback.html");
			map.put("vedio1", "http://" + isu + ".hxybvideo.com:8090/newhls/record/record0/index.m3u8");
			map.put("vedio2", "http://" + isu + ".hxybvideo.com:8090/newhls/record/record1/index.m3u8");
			map.put("vedio3", "http://" + isu + ".hxybvideo.com:8090/newhls/record/record2/index.m3u8");
			map.put("vedio4", "http://" + isu + ".hxybvideo.com:8090/newhls/record/record3/index.m3u8");
			System.out.println("--------------1-------------");
		} else {
			map.put("vedio0", "http://" + isu + ".xstrive.com:8080");
			map.put("vedio1", "http://" + isu + ".xstrive.com:8080/newhls/record/record0/index.m3u8");
			map.put("vedio2", "http://" + isu + ".xstrive.com:8080/newhls/record/record1/index.m3u8");
			map.put("vedio3", "http://" + isu + ".xstrive.com:8080/newhls/record/record2/index.m3u8");
			map.put("vedio4", "http://" + isu + ".xstrive.com:8080/newhls/record/record3/index.m3u8");
			System.out.println("------------2--------");
		}
		// 所有公司的车辆总数
//		logger.info("监控车辆视频:{}" + taxiNum);
//		String isu = null;
//		String isuNum = null;
//		// 添加视频显示 (不需要进行切割)
//		if (taxiService.getIsuByTaxi(taxiNum) != null) {
//			isu = taxiService.getIsuByTaxi(taxiNum).getIsuNum();
//			isuNum = isu.substring(1, isu.length());
//		} else {
//			isuNum = "defaultRRR";
//		}
//		// 根据车牌号获取终端号，拼接回显字符串。
//		// http://defaultRRR.xstrive.com:8080/newhls/record/record0/index.m3u8
//		map.put("vedio0", "http://" + isu + ".hxybvideo.com:8090/playback.html");
//		map.put("vedio1", "http://" + isu + ".hxybvideo.com:8090/newhls/record/record0/index.m3u8");
//		map.put("vedio2", "http://" + isu + ".hxybvideo.com:8090/newhls/record/record1/index.m3u8");
//		map.put("vedio3", "http://" + isu + ".hxybvideo.com:8090/newhls/record/record2/index.m3u8");
//		map.put("vedio4", "http://" + isu + ".hxybvideo.com:8090/newhls/record/record3/index.m3u8");
		return "test";
	}

	// 进入车辆实时监控界面
	@RequestMapping("/getTreeOfTemp")
	public String getTreeOfTemp(Map<String, Object> map) {
		// 所有公司的车辆总数
		int totality = 0;
		// --------------按权限显示车辆---------------------
		Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		try {
			if (subject.isPermitted("allcompy:get")) {
				companies = infoService.getAllCompany();
			} else {
				String name = (String) subject.getPrincipal();
				// int id=(int)session.getAttribute("compyid");
				Company company = infoService.getComByUser(name);
				// System.out.println("公司id"+);
				if (company != null) {
					companies.add(company);
				}
			}
			logger.info("进入车辆监控页面，监控开始");
			// --------------------------------------
			List<ComInfo2> comInfo2s = new ArrayList<>();
			for (Company c : companies) {
				Set<Taxi> taxis = c.getTaxis();
				Set<TaxiInfo2> taxis2 = new HashSet<>();

				Iterator<Taxi> it = taxis.iterator();
				while (it.hasNext()) {
					Taxi next = it.next();
					TaxiInfo2 t2 = new TaxiInfo2(next.getId(), next.getTaxiNum());
					// gy添加逻辑,根据终端号获取在线和定位状态,将在线和定位状态放在TaxiInfo2中
					try {
						taxiService.setTaxiStateByIsuNum(t2, next.getIsuNum());
						taxis2.add(t2);
					} catch (NullPointerException e) {
						logger.error("设置车辆状态；空指针" + next.getIsuNum());
						t2.setOnlineState("未知");
						t2.setPositioningState("未知");
						taxis2.add(t2);
						continue;
					}
				}
				comInfo2s.add(new ComInfo2(c.getId(), c.getName(), taxis2));
			}
			// 计算所有公司的车辆数
			for (ComInfo2 comInfo : comInfo2s) {
				totality += comInfo.getCount();
			}
			logger.info("公司:" + comInfo2s);
			map.put("comInfo2s", comInfo2s);
			map.put("totality", totality);
			// 添加视频显示 默认显示
			// map.put("vedio0", "http://defaultRRR.xstrive.com:8080");
			// map.put("vedio1",
			// "http://defaultRRR.xstrive.com:8080/newhls/record/record0/index.m3u8");
			// map.put("vedio2",
			// "http://defaultRRR.xstrive.com:8080/newhls/record/record1/index.m3u8");
			// map.put("vedio3",
			// "http://defaultRRR.xstrive.com:8080/newhls/record/record2/index.m3u8");
			// map.put("vedio4",
			// "http://defaultRRR.xstrive.com:8080/newhls/record/record3/index.m3u8");
			return "tempMoni";
		} catch (TransactionException e) {
			logger.error("进入监控页面报错" + e.getMessage());
			return "tempMoni";
		}

	}

	/**
	 * 轨迹回放时的搜索
	 *
	 * @param startDate
	 * @param endDate
	 * @param taxiNum
	 * @param model
	 * @param request
	 * @return
	 * @author 高杨
	 */
	/*
	 * @RequestMapping("/getAllAddress") public @ResponseBody Map<String,
	 * Object> getAllAddress(String startDate, String endDate, String taxiNum,
	 * Model model, HttpServletRequest request) { Map<String, Object> map = new
	 * HashMap<>(); List<Address> address = new ArrayList<>();
	 *
	 * address = addressService.findAddressByDate(startDate, endDate, taxiNum);
	 *
	 * if (address.size() != 0) { String taxiNum1 =
	 * taxiService.getTaxiNumByIsuNum(address.get(0) .getIsuNum());
	 * map.put("address", address); map.put("taxiNum", taxiNum1); } return map;
	 * }
	 */
	@RequestMapping("/getAllAddress")
	public @ResponseBody Map<String, Object> getAllAddress(String startDate, String endDate, String taxiNum,
			String fare, String floatingFare1, String floatingFare2) {

		List<Meter> meters = null;
		Map<String, Object> map = new HashMap<>();
		List<Address> address = new ArrayList<>();

		// 现在有三种类型的搜索：[1]精确运价不为空时[2]浮动运价不为空时[3]只有时间和车牌号时
		// 1.第一种类型：精确运价不为空时
		if (fare != null && fare != "") {
			// 1.1.根据运价和车牌号码和时间去获取营运信息
			meters = meterService.getMeterByFare(fare, startDate, endDate, taxiNum);

		} else if (floatingFare1 != null && floatingFare1 != "" && floatingFare2 != null && floatingFare2 != "") {
			// 2.第二种类型：浮动运价不为空时

			// 在第一种类型的基础之上添加对运价的浮动
			meters = meterService.getMeterByFloatingFare(floatingFare1, floatingFare2, startDate, endDate, taxiNum);

		} else {
			// 3.第三种类型，只有时间和车牌号
			address = addressService.findAddressByDate(startDate, endDate, taxiNum);

			if (address.size() != 0) {
				map.put("result", address);
				map.put("taxiNum", taxiNum);
			}
		}

		// 1.2统计营运信息的条数
		// 1.2.1 没有营运信息，反馈界面提示
		if (meters != null) {
			int size = meters.size();
			if (size == 0) {
				map.put("result", "zero");
			} else if (size > 1) {
				// 1.2.2 多条营运信息，反馈界面提示
				map.put("result", "many");
			} else {
				// 1.2.3一条营运信息,根据营运信息中的上下车时间Address去找轨迹点
				Meter meter = meters.get(0);
				address = addressService.findAddressByDate(meter.getUpTime(), meter.getDownTime(), taxiNum);

				if (address.size() != 0) {
					map.put("result", address);
					map.put("taxiNum", taxiNum);
				}
			}
		}
		return map;
	}

	// 进入定时定点查车界面
	@RequestMapping("/enterSelectCar")
	public String selectCar(HttpServletResponse response, Map<String, Object> map) {
		System.out.println("定点查车进入");
		List<Taxitype> type = infoService.getAllTaxiType();
		map.put("taxiType", type);
		return "selectCarByTA";
	}

	// 赵杰添加：
	// 删除预设信息
	@RequestMapping("/deletePreInfo")
	public String deletePreInfo(Integer id, RedirectAttributes attr) {
		String result = null;
		Preinfo preinfo = new Preinfo();
		preinfo.setId(id);
		try {
			addressService.deletePreInfo(preinfo);
			attr.addFlashAttribute("result", "操作执行成功！");
			logger.info("删除预设信息" + preinfo.toString());
		} catch (HibernateOptimisticLockingFailureException e) {
			attr.addFlashAttribute("result", "您要操作的对象已被删除");
			logger.error("执行删除预设信息，对象已被删除" + e.getMessage());
		} catch (Exception e) {
			logger.error("执行删除预设信息出现异常" + e.getMessage());
			attr.addFlashAttribute("result", "出现未知错误");
		}

		return "redirect:/getPreInfo";

	}

	// 定时定点查车
	@RequestMapping("/selectCar")
	public void selectCar(HttpServletResponse response, Graph graph, String startDate, String endDate,
			InfoQuery query) {
		List<Taxi> taxis = new ArrayList();
		if (graph != null) {
			taxis = addressService.lostAndFound(graph, startDate, endDate, query);
			logger.info("定时定点查车最后结果" + taxis);
		}
		// System.out.println("最后结果taxis："+taxis.size()+" , "+ new Date());

		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");

		try {
			response.getWriter().print(mapper.writeValueAsString(taxis));
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("定时定点查车异常" + e.getMessage());
		}
	}

	// 聚集报警消息，上传的值
	@RequestMapping("/putInformation")
	public String putInformation(HttpServletRequest request, String state, String point1, String point2) {
		List<Point> points = new ArrayList<>();
		points.add(new Point(point1));
		points.add(new Point(point2));
		if (state.equals("1")) {
			addressService.alarmOfGather1(1, points);
		} else if (state.equals("2")) {
			addressService.alarmOfGather1(2, points);
		} else if (state.equals("3")) {
			// 多边形
			String point3 = request.getParameter("point3");
			String point4 = request.getParameter("point4");
			String point5 = request.getParameter("point5");
			String point6 = request.getParameter("point6");
			if (point3 != null && point3 != "") {
				points.add(new Point(point3));
			}
			if (point4 != null && point4 != "") {
				points.add(new Point(point4));
			}
			if (point5 != null && point5 != "") {
				points.add(new Point(point5));
			}
			if (point6 != null && point6 != "") {
				points.add(new Point(point6));
			}
			addressService.alarmOfGather1(3, points);
		}

		return "success";
	}

	// 查询所有预设信息
	@RequestMapping("/getPreInfo")
	public String getPreInfo(Map<String, Object> map) {
		map.put("preinfo", addressService.getPreInfo());
		return "preInfo";
	}

	// 添加预设信息
	@RequestMapping("/addPreInfo")
	public String addPreInfo(Graph graph, Preinfo preinfo, Map<String, Object> map,
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate) {
		if (startDate.length() != 0) {
			preinfo.setStartTime(DateUtil.getTimestampByStr(startDate));// 开始时间
			preinfo.setEndTime(DateUtil.getTimestampByStr(endDate));// 结束时间
		}
		preinfo.setTime(DateUtil.getTimestampByDate(new Date()));// 上传时间
		preinfo.setPeople(SecurityUtils.getSubject().getPrincipal().toString());// 操作人员
		map.put("preinfo", addressService.addPreInfo(preinfo, graph));
		logger.info("添加预设信息：" + preinfo.toString());
		return "preInfo";
	}

	// 添加预设信息
	@ResponseBody
	@RequestMapping("/addPreInfo2")
	public String addPreInfo2(@RequestBody PointsInfo pointsInfo, Map<String, Object> map) {
		String result = null;
		// 设置流水号
		pointsInfo.setSerial(SerialDao.getSerialNum());
		// 将点数量和点信息保存到集合中
		try {
			addressService.addPreInfo2(pointsInfo);
			result = "添加成功";
		} catch (Exception e) {
			e.printStackTrace();
			result = "添加失败";
			return result;
		}
		logger.info("添加下发区域预设信息：" + pointsInfo.toString());
		return result;
	}

	// 删除下发区域预设信息
	@RequestMapping("/delePre2")
	public String delePre2(Integer id, RedirectAttributes attr) {
		String result = null;
		logger.info("删除下发区域的预设信息");
		if (addressService.deletePreInfo2(id)) {
			result = "删除成功";
		} else {
			result = "删除失败";
		}
		attr.addFlashAttribute("result", result);
		return "redirect:/getPreInfo2";
	}

	// 模拟轮询
	@RequestMapping("/preInfoPolling")
	public String preInfoPolling() {
		addressService.preInfoPolling();
		return "";
	}

	@RequestMapping("/getPreInfo2")
	public String getPreInfo2(Map<String, Object> map) {
		map.put("pointsInfo", addressService.getPreInfo2());
		return "preInfo2";
	}
	// 获得所有车辆信息
	/*
	 * @RequestMapping("/getAllTaxi") public String
	 * getAllTaxi(Map<String,Object> map){
	 * map.put("address",addressService.findAllTaxi());
	 * System.out.println(addressService.findAllTaxi()); return "tempMoni"; }
	 */

	// gy:备份老逻辑 2017年4月21日09:05:47
	@RequestMapping("/monitoring")
	public void monitoring(String isuNums, HttpServletResponse response) {
		// System.out.println("进来啦");
		// 多辆查询时，去掉最后一个逗号 ,实际上上传的为 车牌号组成的字符串
		// if (isuNums.length() > 7) {
		// isuNums = isuNums.substring(0, isuNums.lastIndexOf(','));
		// System.out.println("监控总车辆"+isuNums);
		// }

		String[] isuNum = isuNums.split(",");
		List<Address> address;
		try {
			address = addressService.monitoring(isuNum);
			logger.info("实时监控车辆列表：" + isuNum);
			ObjectMapper mapper = new ObjectMapper();
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().print(mapper.writeValueAsString(address));
		} catch (IOException e) {
			logger.error("实时监控出现异常" + e.getMessage());
		}
	}

	/**
	 * wk 车辆列表后台分页与轮询
	 *
	 * @param datatablesQuery
	 * @param isuNums
	 * @return
	 */
	@RequestMapping("/monitoring1")
	public @ResponseBody DatatablesView<Address> monitoring1(DatatablesQuery datatablesQuery, String isuNums) {
		// long time11 = System.currentTimeMillis();
		/*
		 * System.out.println("进来controller啦");
		 * System.out.println(datatablesQuery.getDraw());
		 * System.out.println(datatablesQuery.getStart());
		 * System.out.println(datatablesQuery.getLength());
		 * System.out.println(datatablesQuery.getOrderColumn());
		 * System.out.println(datatablesQuery.getOrderType());
		 * System.out.println(datatablesQuery.getSearch());
		 * System.out.println("controller响应数据"+isuNums);
		 */

		if (datatablesQuery.getOrderColumn() == null) {
			datatablesQuery.setOrderColumn("");
		}
		if (datatablesQuery.getSearch() == null) {
			datatablesQuery.setSearch("");
		}
		String[] isuNum = isuNums.split(",");
		DatatablesView<Address> datatablesView = null;
		try {
			// System.out.println("controller车牌号"+isuNum);
			datatablesView = addressService.monitoring1(datatablesQuery, isuNum);
			// System.out.println("controller返回的set数据："+datatablesView);
			logger.info("实时监控车辆列表：" + isuNum);
		} catch (Exception e) {
			logger.error("实时监控出现异常" + e.getMessage());
		}
		// long time12 = System.currentTimeMillis();
		// System.out.println("整个后台所用时间："+(time12-time11));
		return datatablesView;
	}

	// 在线率统计进行修改。
	@RequestMapping(value = "/getMonitoring", method = RequestMethod.POST)
	public void monitor(HttpServletRequest req, HttpServletResponse response) {
		String[] isuNums = req.getParameterValues("isuNums[]");
		// for (String string : isuNums) {
		// System.out.println("终端" + string);
		// }
		List<Address> address = null;
		if (isuNums != null) {
			address = addressService.monitor(isuNums);
		}
		System.out.println("最终的结果" + address);
		// System.out.println("monitoring-address: " + address );
		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");

		try {
			response.getWriter().print(mapper.writeValueAsString(address));
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("获取临时状态栏出现异常" + e.getMessage());
		}
	}

	// 轮询查看是否有未处理的报警信息
	@RequestMapping("/pollingOfAlarm")
	public void pollingOfAlarm(HttpServletResponse response) {
		System.out.println("===================报警页面======================");
		Integer n = addressService.pollingOfAlarm();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().print(n);
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("轮询报警信息出现异常" + e.getMessage());
		}
	}

	// 轮询查看是否有未处理的投诉信息
	@RequestMapping("/pollingOfReport")
	public void pollingOfReport(HttpServletResponse response) {
		int n = 0;
		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().print(mapper.writeValueAsString(n));
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("轮询投诉信息出现异常：" + e.getMessage());
		}
	}

	// 进入计价器查看营运信息界面
	@RequestMapping("/intoCheckMeter")
	public String intoCheckMeter(Map<String, Object> map) {
		logger.info("进入计价器营运信息页面");
		Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
		} else if (subject.isPermitted("bjcompy:get")) {
			companies = infoService.getAllCompanyBj();
		} else {
			String name = (String) subject.getPrincipal();

			// int id=(int)session.getAttribute("compyid");
			Company company = infoService.getComByUser(name);
			// System.out.println("公司id"+);
			if (company != null) {
				companies.add(company);
			}
		}

		// List<Company> companies = infoService.getAllCompany();
		List<ComInfo2> comInfo2s = new ArrayList<>();
		for (Company c : companies) {
			Set<Taxi> taxis = c.getTaxis();
			Set<TaxiInfo2> taxis2 = new HashSet<>();

			Iterator<Taxi> it = taxis.iterator();
			while (it.hasNext()) {
				Taxi next = it.next();
				TaxiInfo2 t2 = new TaxiInfo2(next.getId(), next.getTaxiNum());
				taxis2.add(t2);
			}
			comInfo2s.add(new ComInfo2(c.getId(), c.getName(), taxis2));
		}

		map.put("comInfo2s", comInfo2s);
		return "WEB-INF/views/meter/checkMeter2";
	}

	// 查看指定汽车计价器的营运记录
	@RequestMapping("/checkMeterByTaxi")
	public String checkMeterByTaxi(Map<String, Object> map, String taxiNums, String startDate, String endDate) {
		String[] choose = taxiNums.split(",");
		List<String> taxiNum = new ArrayList<>();
		for (int i = 0; i < choose.length; i++) {
			if (choose[i].length() >= 6)
				// 去掉首字
				taxiNum.add(choose[i].substring(1));
		}
		logger.info("查询计价器营运记录：" + map);
		map.put("meter", addressService.getMetersByTaxi(taxiNum, startDate, endDate));
		map.put("taxiNums", taxiNums);
		map.put("meterStartDate", startDate);
		map.put("meterEndDate", endDate);
		return "forward:intoCheckMeter";
	}

	// 在index界面实时显示在线率等
	@RequestMapping("/getOnlineRate")
	public void getOnlineRate(HttpServletResponse response) {
		OnlineRate online = new OnlineRate();
		int i = 0;
		if (FarmConstant.OnlineState == i) {
			online = addressService.getOnlineRateOf1();
			logger.info("根据终端心跳获取在线率如下" + online.toString());
		} else {
			online = addressService.getOnlineRateOf0();
			logger.info("根据实时位置表获取在线率如下：" + online.toString());
		}

		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().print(mapper.writeValueAsString(online));
		} catch (IOException e) {
			System.out.println("index显示在线率时，转换json异常");
			logger.error("index显示在线率出现json转换异常" + e.getMessage());
		}

	}

}
