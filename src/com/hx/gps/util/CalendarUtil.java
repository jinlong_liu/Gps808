package com.hx.gps.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CalendarUtil {
	
	/**
	 * 获取两个日期之间的所有日期，包含前后日期
	 */
	public static List<String> getDays(String startTime,String endTime) throws ParseException{
		Calendar startCalendar = Calendar.getInstance();
		Calendar endCalendar = Calendar.getInstance();
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate = df.parse(startTime);
		startCalendar.setTime(startDate);
		Date endDate = df.parse(endTime);
		endCalendar.setTime(endDate);
		
		List<String> days = new ArrayList<String>();
		days.add(df.format(startDate));
		
		while(true){
			startCalendar.add(Calendar.DAY_OF_MONTH, 1);
			if(startCalendar.getTimeInMillis() <= endCalendar.getTimeInMillis()){//TODO 转数组或是集合，楼主看着写吧
				days.add(df.format(startCalendar.getTime()));
			}else{
				break;
			}
		}
		return days;
	}
	
	/**
	 * 获取两个日期之间的所有月份
	 */
	public static List<String> getMonths(String minDate, String maxDate) throws ParseException {
	    ArrayList<String> result = new ArrayList<String>();
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");//格式化为年月

	    Calendar min = Calendar.getInstance();
	    Calendar max = Calendar.getInstance();

	    min.setTime(sdf.parse(minDate));
	    min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH), 1);

	    max.setTime(sdf.parse(maxDate));
	    max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH), 2);

	    Calendar curr = min;
	    while (curr.before(max)) {
	     result.add(sdf.format(curr.getTime()));
	     curr.add(Calendar.MONTH, 1);
	    }
	    return result;
	  }
}
