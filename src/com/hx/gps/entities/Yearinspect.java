package com.hx.gps.entities;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Yearinspect entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "yearinspect" )
public class Yearinspect implements java.io.Serializable {

	// Fields

	private Integer id;
	private Company company;
	private String taxiNum;
	private Timestamp lastTime;
	private Timestamp nextTime;
	private Float money;
	private Integer remind;

	// Constructors

	/** default constructor */
	public Yearinspect() {
	}

	/** full constructor */
	public Yearinspect(Company company, String taxiNum, Timestamp lastTime,
			Timestamp nextTime, Float money, Integer remind) {
		this.company = company;
		this.taxiNum = taxiNum;
		this.lastTime = lastTime;
		this.nextTime = nextTime;
		this.money = money;
		this.remind = remind;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "comId")
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Column(name = "taxiNum", length = 11)
	public String getTaxiNum() {
		return this.taxiNum;
	}

	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}

	@Column(name = "lastTime", length = 19)
	public Timestamp getLastTime() {
		return this.lastTime;
	}

	public void setLastTime(Timestamp lastTime) {
		this.lastTime = lastTime;
	}

	@Column(name = "nextTime", length = 19)
	public Timestamp getNextTime() {
		return this.nextTime;
	}

	public void setNextTime(Timestamp nextTime) {
		this.nextTime = nextTime;
	}

	@Column(name = "money", precision = 255, scale = 0)
	public Float getMoney() {
		return this.money;
	}

	public void setMoney(Float money) {
		this.money = money;
	}

	@Column(name = "remind")
	public Integer getRemind() {
		return this.remind;
	}

	public void setRemind(Integer remind) {
		this.remind = remind;
	}

}