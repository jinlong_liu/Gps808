package com.hx.gps.entities;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.mongodb.BasicDBObject;

public class PublicAdvs {
	// 播放项id
	private int playId;
	// 播放模式
	private int playMode;
	// 播放速度
	private int playRate;
	// 广告内容
	private String adsContext;
	// 广告名称
	private String adsName;
	// 记录时间
	private String addTime;
	public String getAddTime() {
		return addTime;
	}

	public void setAddTime(String addTime) {
		this.addTime = addTime;
	}

	public int getPlayId() {
		return playId;
	}

	public void setPlayId(String playId) {
		try {
			this.playId = Integer.parseInt(playId);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	public int getPlayMode() {
		return playMode;
	}

	public void setPlayMode(String playMode) {
		try {
			this.playMode = Integer.parseInt(playMode);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	public int getPlayRate() {
		return playRate;
	}

	public void setPlayRate(String playRate) {
		try {
			this.playRate = Integer.parseInt(playRate);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	public String getAdsContext() {
		return adsContext;
	}

	public void setAdsContext(String adsContext) {
		this.adsContext = adsContext;

	}

	public String getAdsName() {
		return adsName;
	}

	public void setAdsName(String adsName) {
		this.adsName = adsName;
	}

	public PublicAdvs() {

	}

	public PublicAdvs(BasicDBObject finds) {
		super();

		Date d = finds.getDate("addTime");
		// 给定模式
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (d!=null) {
			this.addTime = sdf.format(d);
		}
		this.playMode = finds.getInt("playMode");
		this.playRate = finds.getInt("playRate");
		this.playId = finds.getInt("playId");
		this.adsContext = finds.getString("adsContext");
		this.adsName = finds.getString("adsName");
	}

 

	public PublicAdvs(int playId, int playMode, int playRate, String adsContext, String adsName, String addTime) {
		super();
		this.playId = playId;
		this.playMode = playMode;
		this.playRate = playRate;
		this.adsContext = adsContext;
		this.adsName = adsName;
		this.addTime = addTime;
	}

	@Override
	public String toString() {
		return "PublicAdvs [playId=" + playId + ", playMode=" + playMode + ", playRate=" + playRate + ", adsContext="
				+ adsContext + ", adsName=" + adsName + ", addTime=" + addTime + "]";
	}

}
