package com.hx.gps.entities;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Taxitype entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "taxitype" )
public class Taxitype implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String type;
	private String remark;
	private String people;
	private Timestamp time;
	/**
	 * 新增车辆长宽高，单位为米，可能存在浮点数
	 */
	private Double lengthOfCar;
	private Double widthOfCar;
	private Double heightOfCar;
	// Constructors

	/** default constructor */
	public Taxitype() {
	}

	/** full constructor */
	public Taxitype(String type, String remark, String people, Timestamp time) {
		this.type = type;
		this.remark = remark;
		this.people = people;
		this.time = time;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "type")
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "remark")
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "people")
	public String getPeople() {
		return this.people;
	}

	public void setPeople(String people) {
		this.people = people;
	}

	@Column(name = "time", length = 19)
	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	 
	@Column(name = "lengthOfCar")
	public Double getLengthOfCar() {
		return lengthOfCar;
	}

	public void setLengthOfCar(Double lengthOfCar) {
		this.lengthOfCar = lengthOfCar;
	}
	@Column(name = "widthOfCar")
	public Double getWidthOfCar() {
		return widthOfCar;
	}
	
	public void setWidthOfCar(Double widthOfCar) {
		this.widthOfCar = widthOfCar;
	}
	@Column(name = "heightOfCar")
	public Double getHeightOfCar() {
		return heightOfCar;
	}

	public void setHeightOfCar(Double heightOfCar) {
		this.heightOfCar = heightOfCar;
	}

	@Override
	public String toString() {
		return "Taxitype [id=" + id + ", type=" + type + ", remark=" + remark + ", people=" + people + ", time=" + time
				+ ", lengthOfCar=" + lengthOfCar + ", widthOfCar=" + widthOfCar + ", heightOfCar=" + heightOfCar + "]";
	}
	

}