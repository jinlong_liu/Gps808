<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<title>BootstrapValidator demo</title>

<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap.css" />
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">

<!-- gy 添加表单验证 -->
<script src="js/jquery.js"></script>
<script type="text/javascript"
	src="vendor/bootstrap/js/bootstrap.min.js"></script>
	  <script src=" js/select2.js"></script>
    <link href=" css/select2.css" rel="stylesheet" />
<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
<style type="text/css">

.img {
margin-top: 5%;
}
</style>
</head>
<body class="gray-bg">

    <div class="wrapper wrapper-content animated ">
                <div class="ibox-title">
                <h5>新增投诉信息</h5>
            	</div>
           
				<div class="ibox-content" style="height: 780px">
				<div class="row">
				<div class="col-sm-6">
			<!-- form: -->
					 <form action="addComplaint2" class="form-horizontal" method="post" id="submitForm">
						<fieldset>
						<div class="form-group">
							<label class="col-sm-3 control-label"><span style="color:red">*  </span>举报车辆:</label>
							<div class="col-sm-5">
								<select name="taxi1" id="taxi1" class="form-control js-example-basic-single">
									<option value="">请选择投诉车牌号</option>
									  <c:forEach items="${sessionScope.taxis}" var="taxis">
																<option value="${taxis.id}">${taxis.taxiNum}</option>
															</c:forEach>  
								</select>
							</div>
						</div>
	
	
						<div class="form-group">
							<label class="col-sm-3 control-label"><span style="color:red">*  </span>举报司机:</label>
							<div class="col-sm-5">
								<select class="form-control js-example-basic-single" name="driver1" id="driver1">
									<option value="">请选择投诉司机</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label"><span style="color:red">*  </span>投诉类型:</label>
							<div class="col-sm-5">
								<select name="reportType1" class="form-control js-example-basic-single" id="reportType1">
									<option value="">请选择投诉类型</option>
									  <c:forEach items="${sessionScope.compTypes}" var="compTypes">
								<option value="${compTypes.id}">${compTypes.type}</option>
							</c:forEach>  
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label"><span style="color:red">*  </span>上车时间:</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="startTime" autocomplete="off"
									id="startDate" value="${sensorStartDate}" class="Wdate"
									onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(submitForm, name.valueOf())},maxDate:'#F{$dp.$D(\'endDate\')||\'new Date()\'}'})">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label"><span style="color:red">*  </span>下车时间:</label>
							<div class="col-sm-5">
	
								<input type="text" class="form-control" name="endTime" autocomplete="off"
									id="endDate" value="${sensorEndDate}" class="Wdate" value=""
									onfocus="WdatePicker({lang:'zh-cn',onpicked:function(dp){refreshValidator(submitForm, name.valueOf())},minDate:'#F{$dp.$D(\'startDate\')}',maxDate:'#F{$dp.$D(\'startDate\',{H:144})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})">
	
							</div>
						</div>
	
						<div class="form-group">
							<label class="col-sm-3 control-label"><span style="color:red">*  </span>&nbsp;&nbsp;&nbsp;反馈人:</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="backer" id="backer" autocomplete="off">
							</div>
						</div>
	
						<div class="form-group">
							<label class="col-sm-3 control-label"><span style="color:red">*  </span>反馈电话:</label>
							<div class="col-sm-5">
								<input type="text" class="form-control" name="backPhone" autocomplete="off"
									id="backPhone">
							</div>
						</div>
	
	
						<div class="form-group">
							<label class="col-sm-3 control-label"> 上车地点:</label>
							<div class="col-sm-7">
								<input type="text" class="form-control" name="startPlace" autocomplete="off"
									id="startPlace">
							</div>
						</div>
	
						<div class="form-group">
							<label class="col-sm-3 control-label"> 下车地点:</label>
							<div class="col-sm-7">
								<input type="text" class="form-control" name="endPlace" autocomplete="off"
									id="endPlace">
	
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label"> 投诉事由:</label>
							<div class="col-sm-7">
								<input type="text" class="form-control" name="reportReason" autocomplete="off"
									id="reportReason">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label"> 投诉内容:</label>
							<div class="col-sm-7 ">
								<input type="text" class="form-control" name="reportContent" autocomplete="off"
									id="reportContent">
							</div>
						</div>
	
	
						<div class="form-group">
							<div class="col-sm-7 col-sm-offset-2">
								
								<button type="reset" id="resetBtn" class="btn " >重&nbsp;&nbsp;&nbsp;置</button>&nbsp;&nbsp;
								<button type="submit" class="btn btn-primary">保&nbsp;&nbsp;&nbsp;存</button>
	
							</div>
						</div>
						</fieldset>
					</form>
				<!-- :form -->
					</div>
						<img src="img/Complaint.jpg"  class="col-sm-5 img "  alt="Cinque Terre">
				</div>
</div>
		</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>

		<script type="text/javascript">
  //加载下拉框
    	  $(".js-example-basic-single").select2();
 
	 $(document).ready(function formValidator() {
	        $('#submitForm').bootstrapValidator({
	            message: '这个值无效',
	            feedbackIcons: {
	                valid: 'glyphicon glyphicon-ok',
	                invalid: 'glyphicon glyphicon-remove',
	                validating: 'glyphicon glyphicon-refresh'
	            },
	            /*生效规则：字段值一旦变化就触发验证*/
	            live: 'enabled',
	            /*当表单验证不通过时，该按钮为disabled*/
	            submitButtons: 'button[type="submit"]',
	            fields: {
	            	taxi1: {

	                      validators: {
	                          notEmpty: {
	                              message: '请选择投诉车辆'
	                          }
	                      }
	                  },   driver1: {
	                      validators: {
	                          notEmpty: {
	                              message: '请选择投诉司机'
	                          }
	                      }
	                  },startTime: {
	                      validators: {
	                          notEmpty: {
	                              message: '请选择上车时间'
	                          }
	                      }
	                  },endTime: {
	                      validators: {
	                          notEmpty: {
	                              message: '请选择下车时间'
	                          }
	                      }
	                  },
	                  reportType1: {
	                      validators: {
	                          notEmpty: {
	                              message: '请选择投诉类型'
	                          }
	                      }
	                  },
	                backer: {
	                    validators: {
	                        notEmpty: {
	                            message: '请填写反馈人'
	                        }
	                    }
	                },
	                backPhone: {
	                    validators: {
	                        notEmpty: {
	                            message: '请填写反馈电话'
	                        },stringLength: {
	                            min: 11,
	                            max: 11,
	                            message: '请输入11位手机号码'
	                        },
	                        numeric : {
								message : '请输入数字'
							}
	                    }
	                },
	                startPlace: {
	                    validators: {
	                    
	                    }
	                },endPlace: {
	                    validators: {
	                      
	                    }
	                   },reportReason: {
	                    validators: {
	                    
	                    }
	                },reportContent: {
	                    validators: {
	                      
	                    }
	                }
	            }
	        });
	    });
	function alter(id, taxi1,driver1, reportType1, reportReason,
			reportContent, startTime, endTime, backer, backPhone,
			startPlace, endPlace) {
		change2(taxi1,driver1);
		$("#id").val(id);
		$("#taxi1").val(taxi1);
		//$("#driver1").val(driver1);
		$("#reportType1").val(reportType1);
		$("#reportReason").val(reportReason);
		$("#reportContent").val(reportContent);
		$("#startTime").val(startTime);
		$("#endTime").val(endTime);
		$("#backer").val(backer);
		$("#backPhone").val(backPhone);
		$("#startPlace").val(startPlace);
		$("#endPlace").val(endPlace);
	};


	$('#taxi1').change(
			function() {
				var id = $('#taxi1').val();
				if (id != "") {
					var driver = $('#driver1');
					$.post("getDriverByTaxi", {
						id : id
					}, function(data) {
						if (data.length != 0) {
							//返回的数据不为空 
							var newdata = eval("(" + data + ")");
							driver.html("");//避免选择框中的值持续叠加
							$("<option value=''>请选择举报司机</option>")
									.appendTo(driver);

							for (var i = 0; i < data.length; i++) {
								$(
										"<option value ='"+newdata[i].driId +
										"'> "
												+ newdata[i].driName
												+ "</option>").appendTo(
										driver);
							}
						}
					});
				}
			});

	function change2(id,driver2) {
		var driver = $("#driver1");
		$.post("getDriverByTaxi", {
			id : id
		}, function(data) {
			if (data.length != 0) {
				//返回的数据不为空 
				var newdata = eval("(" + data + ")");
				driver.html("");//避免选择框中的值持续叠加
				/* $("<option value=''>请选择举报司机</option>").appendTo(driver); */

				for (var i = 0; i < data.length; i++) {
					$(
							"<option value ='"+newdata[i].driId +
										"'> "
									+ newdata[i].driName + "</option>")
							.appendTo(driver);
				}
				$("#driver1").val(driver2);
			}
		});
	}

 
	
	//页面加载完成后执行,获取执行状态
	$(document).ready(function(){
		var result='<%=request.getAttribute("result")%>';
		if(result!="null"){
			swal({
				title :result,
				type : "success"
			});
		}
	});
	/*日历刷新验证*/
		function refreshValidator(id,name){
		$(id).data('bootstrapValidator').updateStatus(name,
		'NOT_VALIDATED',null).validateField(name);
		}
 						 
	</script>
</body>
</html>