package com.hx.gps.entities;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Preinfo entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "preinfo" )
public class Preinfo implements java.io.Serializable {

	// Fields

	private Integer id;
	private Graph graph;
	private String name;
	private String num;
	private String people;
	private Integer rateLimit;
	private Float maxSpeed;
	private Integer continueTime;
	private Integer timeLimit;
	private Timestamp startTime;
	private Timestamp endTime;
	private Integer inToDriver;
	private Integer outToDriver;
	private Integer inToPlatform;
	private Integer outToPlatform;
	private Integer gatherAlarm;
	private Integer carUpper;
	private Integer carNum;
	private Timestamp time;

	// Constructors

	/** default constructor */
	public Preinfo() {
	}

	/** minimal constructor */
	public Preinfo(Graph graph, Integer rateLimit, Integer timeLimit,
			Integer inToDriver, Integer outToDriver, Integer inToPlatform,
			Integer outToPlatform, Integer gatherAlarm) {
		this.graph = graph;
		this.rateLimit = rateLimit;
		this.timeLimit = timeLimit;
		this.inToDriver = inToDriver;
		this.outToDriver = outToDriver;
		this.inToPlatform = inToPlatform;
		this.outToPlatform = outToPlatform;
		this.gatherAlarm = gatherAlarm;
	}

	/** full constructor */
	public Preinfo(Graph graph, String name, String num, String people,
			Integer rateLimit, Float maxSpeed, Integer continueTime,
			Integer timeLimit, Timestamp startTime, Timestamp endTime,
			Integer inToDriver, Integer outToDriver, Integer inToPlatform,
			Integer outToPlatform, Integer gatherAlarm, Integer carUpper,
			Integer carNum, Timestamp time) {
		this.graph = graph;
		this.name = name;
		this.num = num;
		this.people = people;
		this.rateLimit = rateLimit;
		this.maxSpeed = maxSpeed;
		this.continueTime = continueTime;
		this.timeLimit = timeLimit;
		this.startTime = startTime;
		this.endTime = endTime;
		this.inToDriver = inToDriver;
		this.outToDriver = outToDriver;
		this.inToPlatform = inToPlatform;
		this.outToPlatform = outToPlatform;
		this.gatherAlarm = gatherAlarm;
		this.carUpper = carUpper;
		this.carNum = carNum;
		this.time = time;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "graphId" )
	public Graph getGraph() {
		return this.graph;
	}

	public void setGraph(Graph graph) {
		this.graph = graph;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "num")
	public String getNum() {
		return this.num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	@Column(name = "people")
	public String getPeople() {
		return this.people;
	}

	public void setPeople(String people) {
		this.people = people;
	}

	@Column(name = "rateLimit" )
	public Integer getRateLimit() {
		return this.rateLimit;
	}

	public void setRateLimit(Integer rateLimit) {
		this.rateLimit = rateLimit;
	}

	@Column(name = "maxSpeed", precision = 12, scale = 0)
	public Float getMaxSpeed() {
		return this.maxSpeed;
	}

	public void setMaxSpeed(Float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	@Column(name = "continueTime")
	public Integer getContinueTime() {
		return this.continueTime;
	}

	public void setContinueTime(Integer continueTime) {
		this.continueTime = continueTime;
	}

	@Column(name = "timeLimit" )
	public Integer getTimeLimit() {
		return this.timeLimit;
	}

	public void setTimeLimit(Integer timeLimit) {
		this.timeLimit = timeLimit;
	}

	@Column(name = "startTime", length = 19)
	public Timestamp getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	@Column(name = "endTime", length = 19)
	public Timestamp getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	@Column(name = "inToDriver" )
	public Integer getInToDriver() {
		return this.inToDriver;
	}

	public void setInToDriver(Integer inToDriver) {
		this.inToDriver = inToDriver;
	}

	@Column(name = "outToDriver" )
	public Integer getOutToDriver() {
		return this.outToDriver;
	}

	public void setOutToDriver(Integer outToDriver) {
		this.outToDriver = outToDriver;
	}

	@Column(name = "inToPlatform" )
	public Integer getInToPlatform() {
		return this.inToPlatform;
	}

	public void setInToPlatform(Integer inToPlatform) {
		this.inToPlatform = inToPlatform;
	}

	@Column(name = "outToPlatform" )
	public Integer getOutToPlatform() {
		return this.outToPlatform;
	}

	public void setOutToPlatform(Integer outToPlatform) {
		this.outToPlatform = outToPlatform;
	}

	@Column(name = "gatherAlarm" )
	public Integer getGatherAlarm() {
		return this.gatherAlarm;
	}

	public void setGatherAlarm(Integer gatherAlarm) {
		this.gatherAlarm = gatherAlarm;
	}

	@Column(name = "carUpper")
	public Integer getCarUpper() {
		return this.carUpper;
	}

	public void setCarUpper(Integer carUpper) {
		this.carUpper = carUpper;
	}

	@Column(name = "carNum")
	public Integer getCarNum() {
		return this.carNum;
	}

	public void setCarNum(Integer carNum) {
		this.carNum = carNum;
	}

	@Column(name = "time", length = 19)
	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "Preinfo [id=" + id  + ", name=" + name
				+ ", num=" + num + ", people=" + people + ", rateLimit="
				+ rateLimit + ", maxSpeed=" + maxSpeed + ", continueTime="
				+ continueTime + ", timeLimit=" + timeLimit + ", startTime="
				+ startTime + ", endTime=" + endTime + ", inToDriver="
				+ inToDriver + ", outToDriver=" + outToDriver
				+ ", inToPlatform=" + inToPlatform + ", outToPlatform="
				+ outToPlatform + ", gatherAlarm=" + gatherAlarm
				+ ", carUpper=" + carUpper + ", carNum=" + carNum + ", time="
				+ time + "]";
	}

}