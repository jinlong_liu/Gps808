package com.hx.gps.auth;


import java.util.HashSet;
import java.util.Set;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import com.hx.gps.entities.Power;
import com.hx.gps.entities.Role;
import com.hx.gps.entities.User;
import com.hx.gps.service.UserService;

/**
 * Created by FMY on 2017/3/10 0010.
 */
public class CustomRealm extends AuthorizingRealm {
    final String realmName = "customRealm";
    @Autowired
    private UserService userService;

    public CustomRealm() {
    }


    @Override
    public void setName(String realmName) {
        super.setName(realmName);
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();

        String username = (String) principals.getPrimaryPrincipal();
        User user = userService.findUserByName(username);

        Role role = user.getRole();

        Set<String> permissions = new HashSet<>();
        Set<Power> powers = role.getPowers();

        for (Power p : powers) {
            permissions.add(p.getRes());
        }

        info.addRole(role.getName());
        info.addStringPermissions(permissions);
        System.out.println(role.getName());
        System.out.println(permissions);
        return info;
    }

    /**
     * 用于认证
     *
     * @param token
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String name = (String) token.getPrincipal();

        User user = userService.findUserByName(name);

        if (user == null) {
            throw new UnknownAccountException();
        }
        if (user.getState()==1) {
			throw new DisabledAccountException();
		}

        SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(name, user.getPassword(), this.getName());


        return simpleAuthenticationInfo;
    }

    /**
     * 清除缓存,在其他代码中注入realm即可使用。
     * 更改权限时需要清除缓存
     */
    public void clearCached() {
        PrincipalCollection principals = SecurityUtils.getSubject().getPrincipals();
        super.clearCache(principals);
    }
}
