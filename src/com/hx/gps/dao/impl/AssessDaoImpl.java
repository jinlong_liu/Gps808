package com.hx.gps.dao.impl;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hx.gps.dao.AssessDao;
import com.hx.gps.entities.Accident;
import com.hx.gps.entities.Assarch;
import com.hx.gps.entities.Asscycle;
import com.hx.gps.entities.Complaints;
import com.hx.gps.entities.Comptype;
import com.hx.gps.entities.Deduct;
import com.hx.gps.entities.Evalscore;
import com.hx.gps.entities.Scorerank;

@Repository
public class AssessDaoImpl implements AssessDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public void addOrUpdateCompType(Comptype compType) {
		getSession().saveOrUpdate(compType);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Comptype> getCompType() {
		List<Comptype> comptype = getSession().createQuery("FROM Comptype").list();
		return comptype;
	}

	@Override
	public boolean addComplaint(Complaints complaint) {
		Integer result = (Integer) getSession().save(complaint);
		if (result != null) {
			return true;
		} else {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Complaints> getCompByType(Integer type) {
		String hql = "SELECT c FROM Complaints c WHERE type=:type";
		Query query = getSession().createQuery(hql);
		List<Complaints> c = query.setInteger("type", type).list();
		if (c.size() > 0) {
			return c;
		}
		return null;

	}
@Override
public List<Complaints> getAllComplaints() {
	String hql = " FROM Complaints c WHERE 1=1";
	Query query = getSession().createQuery(hql);
	@SuppressWarnings("unchecked")
	List<Complaints> c = query.list();
	if (c.size() > 0) {
		return c;
	}
	return null;
}
	@Override
	public Complaints getCompById(Integer id) {
		Complaints comp = (Complaints) getSession().get(Complaints.class, id);
		return comp;
	}

	@Override
	public void updateTypeForComp(Integer id, Integer type) {
		Complaints comp = (Complaints) getSession().get(Complaints.class, id);
		comp.setType(type);
		getSession().update(comp);
	}

	@Override
	public boolean addDeduct(Deduct deduct) {
		deduct.setDealDate(new Date());
		Integer result = (Integer) getSession().save(deduct);
		if (result != null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void updateDeduct(Integer compId) {
		Complaints comp = (Complaints) getSession().get(Complaints.class, compId);
		// 未设置外键，不能使用级联查询。
		/*
		 * String hql="SELECT d FROM Deduct d WHERE compId=:compId"; Query
		 * query=getSession().createQuery(hql);
		 * System.out.println("扣分。。。。"+compId); Deduct d=(Deduct)
		 * query.setInteger("compId",compId).uniqueResult(); d.setState(1);
		 * System.out.println("扣分成功"); getSession().update(d);
		 */
		Deduct deduct = comp.getDeduct();
		deduct.setState(1);
		deduct.setDealDate(new Date());
		getSession().update(deduct);
	}

	@Override
	public void deleteDeduct(Integer compId) {
		String hql = "SELECT d FROM Deduct d WHERE compId=:compId";
		Query query = getSession().createQuery(hql);
		Deduct d = (Deduct) query.setInteger("compId", compId).uniqueResult();
		getSession().delete(d);
	}

	@Override
	public void deleteAsscycleById(Integer id) {
		/*
		 * String hql="SELECT a FROM assCycle a WHERE id=:id"; Query
		 * query=getSession().createQuery(hql);
		 */
		Asscycle a = (Asscycle) getSession().get(Asscycle.class, id);
		// Asscycle a=(Asscycle) query.setInteger("id",id).uniqueResult();
		getSession().delete(a);

	}

	@Override
	public void updateComplaint(Complaints complaint) {
		try {
			getSession().clear();
			getSession().update(complaint);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("进行修改操作异常，捕获到的异常类型为：" + e.toString());
		}
	}

	@Override
	public void connectComp(Complaints complaint) {
		Integer deductId = (Integer) getSession().save(complaint.getDeduct());
		Deduct d = complaint.getDeduct();
		d.setId(deductId);
		complaint.setDeduct(d);
		// 设置处理状态，进行扣分。
		getSession().update(complaint);
	}

	@Override
	public boolean deleteCompType(Integer id) {
		// 首先查看是否有关联,若有关联则不可以删除
		Comptype compType = (Comptype) getSession().get(Comptype.class, id);
		Set<Complaints> cs = compType.getComplaintses();
		if (cs.size() > 0) {
			return false;
		} else {
			getSession().delete(compType);
			return true;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Asscycle> getAsscycle() {
		List<Asscycle> asscycle = getSession().createQuery("FROM Asscycle").list();
		return asscycle;
	}

	@Override
	public void updateAsscycle(Asscycle asscycle) {
		getSession().saveOrUpdate(asscycle);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Scorerank> getScorerank() {
		List<Scorerank> scorerank = getSession().createQuery("FROM Scorerank").list();
		return scorerank;
	}

	@Override
	public void addOrUpdateScorerank(Scorerank scorerank) {
		getSession().saveOrUpdate(scorerank);
	}

	@Override
	public void deleteScorerank(Integer id) {
		Scorerank scorerank = (Scorerank) getSession().get(Scorerank.class, id);
		getSession().delete(scorerank);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Evalscore> getEvalscore() {
		List<Evalscore> evalscore = getSession().createQuery("FROM Evalscore").list();
		return evalscore;
	}

	@Override
	public void addOrUpdateEval(Evalscore evalscoer) {
		getSession().saveOrUpdate(evalscoer);
	}

	@Override
	public void deleteEvalScore(Integer id) {
		Evalscore evalscore = (Evalscore) getSession().get(Evalscore.class, id);
		getSession().delete(evalscore);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Deduct> getDeductByDate(Date currDate, Date lastDate) {
		String hql = "SELECT d FROM Deduct d WHERE dealDate<=:currDate and dealDate>=:lastDate and state=:state";
		Query query = getSession().createQuery(hql);
		List<Deduct> d = query.setDate("currDate", currDate).setDate("lastDate", lastDate).setInteger("state", 1)
				.list();
		return d;
	}

	@Override
	public Asscycle getAsscycleByName(String name) {
		String hql = "SELECT a FROM Asscycle a WHERE name=:name";
		Query query = getSession().createQuery(hql);

		return (Asscycle) query.setString("name", name).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Assarch> getAssarsByDate(String time) {
		// System.out.println();
		String hql = "SELECT a FROM Assarch a WHERE assDate=:assDate";
		Query query = getSession().createQuery(hql);
		List<Assarch> assarchs = query.setString("assDate", time).list();
		return assarchs;
	}

	@Override
	public void addAssarch(List<Assarch> asses) {
		Iterator<?> it = asses.iterator();
		Integer i = 0;
		while(it.hasNext()) {
			getSession().save(asses.get(i));
			i++;
			if (i % 100 == 0) { // 每一百条刷新并写入数据库
				getSession().flush();
				getSession().clear();
			}
		}
		/*for (int i = 0; i < asses.size();) {
			getSession().save(asses.get(i));
			i++;
			if (i % 100 == 0) { // 每一百条刷新并写入数据库
				getSession().flush();
				getSession().clear();
			}
		}*/

	}

	@Override
	public boolean daleteComById(Integer id) {
		Complaints c = (Complaints) getSession().get(Complaints.class, id);
		try {
			getSession().delete(c);
			return true;
		} catch (Exception e) {
			return false;
		}

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String[] getAllQuaNums() {
		String hql = "SELECT quaNum FROM Driver";
		List<String> list = getSession().createQuery(hql).list();
		String[] quaNums=list.toArray(new String[list.size()]);
		System.out.println("资格证号"+quaNums.toString());
		return quaNums;
	}

}
