package com.hx.gps.util;

import java.util.List;

import org.springframework.stereotype.Component;

import com.hx.gps.entities.tool.Point;

//地图相关的工具类
@Component
public class MapUtil {
	
	double EARTHRADIUS = 6370996.81; //地球半径
	
	/** 判断点是否在圆中 **/
	public Boolean contains(Point point1,Point point2,double rval) {
	
		//point与圆心距离小于圆形半径，则点在圆内，否则在圆外
     double dis =getDistance(point1.getLongitude(),point1.getLatitude(),point2.getLongitude(),point2.getLatitude());
     if(dis <= rval){
         return true;
     } else {
         return false;
     }
	
	}
	
	/**
     * 计算两点之间的距离,两点坐标必须为经纬度
     * @param {point1} Point 点对象
     * @param {point2} Point 点对象
     * @returns {Number} 两点之间距离，单位为米
     */
     public double getDistance(double p1lng,double p1lat,double p2lng,double p2lat){
//        //判断类型
//        if(!(point1 instanceof BMap.Point) ||
//            !(point2 instanceof BMap.Point)){
//            return 0;
//        }

        double lng1 = getLoop(p1lng, -180, 180);
        double lat1 = getRange(p1lat, -74, 74);
        double lng2 = getLoop(p2lng, -180, 180);
        double lat2 = getRange(p2lat, -74, 74);

        double x1, x2, y1, y2;
        x1 = degreeToRad(lng1);
        y1 = degreeToRad(lat1);
        x2 = degreeToRad(lng2);
        y2 = degreeToRad(lat2);

        return EARTHRADIUS * Math.acos((Math.sin(y1) * Math.sin(y2) + Math.cos(y1) * Math.cos(y2) * Math.cos(x2 - x1)));    
    }
	
	 /**
     * 将v值限定在a,b之间，经度使用
     */
    public double getLoop(double v,Integer a,Integer b){
        while( v > b){
          v -= b - a;
        }
        while(v < a){
          v += b - a;
        }
        return v;
    }
    
    /**
     * 将v值限定在a,b之间，纬度使用
     */
    public double getRange(double v,Integer a,Integer b){
        if(a != null){
          v = Math.max(v, a);
        }
        if(b != null){
          v = Math.min(v, b);
        }
        return v;
    }
    
    /**
     * 将弧度转化为度
     * @param {radian} Number 弧度     
     * @returns {Number} 度
     */
     public double radToDegree(double rad){
        return (180 * rad) / Math.PI;       
    }
     
     /**
      * 将度转化为弧度
      * @param {degree} Number 度     
      * @returns {Number} 弧度
      */
     public double degreeToRad(double degree){
         return Math.PI * degree/180;    
     }
     
   
     /**
      * 判断点是否在矩形内
      * @param {Point} point 点对象
      * @param {Bounds} bounds 矩形边界对象
      * @returns {Boolean} 点在矩形内返回true,否则返回false
      */
     public boolean isPointInRect(Point point1,Point point2,Point point3){
    	 //p1表示东北，p2表示西南，p3表示要判断的点
//         //检查类型是否正确
//         if (!(point instanceof BMap.Point) || 
//             !(bounds instanceof BMap.Bounds)) {
//             return false;
//         }
         return (point3.getLongitude() >= point2.getLongitude() && point3.getLongitude() <= point1.getLongitude() && point3.getLatitude() >= point2.getLatitude() && point3.getLatitude() <= point1.getLatitude());
     }
     
     /**
      * 判断点是否多边形内
      * @param {Point} point 点对象
      * @param {Polyline} polygon 多边形对象
      * @returns {Boolean} 点在多边形内返回true,否则返回false
      */
     public boolean isPointInPolygon(List<Point> points,Point point){
    	 //points--pts
    	 
    	 int N = points.size();			//n边形
         boolean boundOrVertex = true; //如果点位于多边形的顶点或边上，也算做点在多边形内，直接返回true
         int intersectCount = 0;//cross points count of x 
         double precision = 2e-10; //浮点类型计算时候与0比较时候的容差
         Point p1, p2;//neighbour bound vertices
         Point p = point; //测试点

         p1 = points.get(0);//left vertex  
    	 for(int i = 1; i <= N; ++i){//check all rays            
             if(p.equals(p1)){
                 return boundOrVertex;//p is an vertex
             }

             p2 = points.get(i % N);//right vertex            
             if(p.getLatitude() < Math.min(p1.getLatitude(), p2.getLatitude()) || p.getLatitude() > Math.max(p1.getLatitude(), p2.getLatitude())){//ray is outside of our interests                
                 p1 = p2; 
                 continue;//next ray left point
             }

             if(p.getLatitude() > Math.min(p1.getLatitude(), p2.getLatitude()) && p.getLatitude() < Math.max(p1.getLatitude(), p2.getLatitude())){//ray is crossing over by the algorithm (common part of)
                 if(p.getLongitude() <= Math.max(p1.getLongitude(), p2.getLongitude())){//x is before of ray                    
                     if(p1.getLatitude() == p2.getLatitude() && p.getLongitude() >= Math.min(p1.getLongitude(), p2.getLongitude())){//overlies on a horizontal ray
                         return boundOrVertex;
                     }

                     if(p1.getLongitude() == p2.getLongitude()){//ray is vertical                        
                         if(p1.getLongitude() == p.getLongitude()){//overlies on a vertical ray
                             return boundOrVertex;
                         }else{//before ray
                             ++intersectCount;
                         } 
                     }else{//cross point on the left side                        
                         double xinters = (p.getLatitude() - p1.getLatitude()) * (p2.getLongitude() - p1.getLongitude()) / (p2.getLatitude() - p1.getLatitude()) + p1.getLongitude();//cross point of lng                        
                         if(Math.abs(p.getLongitude() - xinters) < precision){//overlies on a ray
                             return boundOrVertex;
                         }

                         if(p.getLongitude()< xinters){//before ray
                             ++intersectCount;
                         } 
                     }
                 }
             }else{//special case when ray is crossing through the vertex                
                 if(p.getLatitude() == p2.getLatitude() && p.getLongitude()<= p2.getLongitude()){//p crossing over p2                    
                     Point p3 = points.get((i+1) % N); //next vertex                    
                     if(p.getLatitude() >= Math.min(p1.getLatitude(), p3.getLatitude()) && p.getLatitude() <= Math.max(p1.getLatitude(), p3.getLatitude())){//p.lat lies between p1.lat & p3.lat
                         ++intersectCount;
                     }else{
                         intersectCount += 2;
                     }
                 }
             }//else            
             p1 = p2;//next ray left point
         }

         if(intersectCount % 2 == 0){//偶数在多边形外
             return false;
         } else { //奇数在多边形内
             return true;
         }            
     }

}
