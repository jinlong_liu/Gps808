package com.hx.gps.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hx.gps.dao.TaxiDao;
import com.hx.gps.entities.Company;
import com.hx.gps.entities.Driver;
import com.hx.gps.entities.IsuCache;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.tool.Address;
import com.hx.gps.entities.tool.Blur;
import com.hx.gps.entities.tool.ComInfo2;
import com.hx.gps.entities.tool.DatatablesQuery;
import com.hx.gps.entities.tool.Gps;
import com.hx.gps.entities.tool.MeterTemp;
import com.hx.gps.entities.tool.TaxiDetails;
import com.hx.gps.entities.tool.TaxiInfo2;
import com.hx.gps.util.DBUtil;
import com.hx.gps.util.DateUtil;
import com.hx.gps.util.FarmConstant;
import com.hx.gps.util.PositionUtil;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

@Repository
public class TaxiDaoImpl implements TaxiDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public TaxiDetails getDetailsByIsu(String isu) {
		TaxiDetails details = new TaxiDetails();

		DBCollection gps = DBUtil.getDBCollection(FarmConstant.MONGODB, "gpsStateTemp");
		DBCollection upEngine = DBUtil.getDBCollection(FarmConstant.MONGODB, "upEngineTemp");
		DBCollection phone = DBUtil.getDBCollection(FarmConstant.MONGODB, "phoneStateTemp");
		DBCollection antenna = DBUtil.getDBCollection(FarmConstant.MONGODB, "antennaTemp");
		DBCollection address = DBUtil.getDBCollection(FarmConstant.MONGODB, "addressTemp");
		BasicDBObject query = new BasicDBObject();
		//查找条件
		BasicDBObject find = new BasicDBObject();

		query.put("isuNum", isu);
		if (gps.find(query).hasNext()) {
			find = (BasicDBObject) gps.findOne(query);
			details.setGpsState(find.getString("state"));
		} else {
			details.setGpsState("未获取到");
		}

		if (upEngine.find(query).hasNext()) {
			find = (BasicDBObject) upEngine.findOne(query);
			details.setEnergyState(find.getString("state"));
		} else {
			details.setEnergyState("未获取到");
		}

		if (antenna.find(query).hasNext()) {
			find = (BasicDBObject) antenna.findOne(query);
			details.setAnntaState(find.getString("state"));
		} else {
			details.setAnntaState("未获取到");
		}
		if (phone.find(query).hasNext()) {
			find = (BasicDBObject) phone.findOne(query);
			details.setPhoneState(find.getString("state"));
		} else {
			details.setPhoneState("未获取到");
		}
		if (address.find(query).hasNext()) {
			find = (BasicDBObject) address.findOne(query);
			String state = find.getString("state");
			String alarm=find.getString("alarm");
			//时间
			Date time=find.getDate("time");
			// 低八位
			String s1 = state.substring(6, 8);
			// 次八位
			String s2 = state.substring(4, 6);
			// 10进制转16进制
			Integer i1 = Integer.decode("0x" + s1);
			Integer i2 = Integer.decode("0x" + s2);
			// 转字节
			byte b1 = i1.byteValue();
			byte b2 = i2.byteValue();
			// 16进制转2进制
			String ss = toBinaryString(b1);
			String sss = toBinaryString(b2);
			// 拆分字符串
			char[] c = ss.toCharArray();
			char[] c2 = sss.toCharArray();
			// ACC状态
			char Acc = c[7];
			if (Acc == '1') {
				details.setAccState("Acc开");
			} else {
				details.setAccState("Acc关");
			}
			// 定位状态
			char location = c[6];
			//先判断在不在线
			if(time.getTime()>= DateUtil.stringToDate(DateUtil.countDate(120)).getTime()){
			if (location == '1') {
				details.setDirectionState("定位");
			} else {
				details.setDirectionState("未定位");
			}
			}else{
				details.setDirectionState("未定位");
			}
			// 纬度
			char lat = c[5];
			if (lat == '1') {
				details.setLatState("南纬");
			} else {
				details.setLatState("北纬");
			}
			// 经度
			char lon = c[4];
			if (lon == '1') {
				details.setLonState("西经");
			} else {
				details.setLonState("东经");
			}
			// 营运
			char meter = c[3];
			if (meter == '1') {
				details.setMeterState("营运");

			} else {
				details.setMeterState("空车");
			}
			// 加密
			char key = c[2];
			if (key == '1') {
				details.setKeyState("已加密");
			} else {
				details.setKeyState("未加密");
			}
			// 供油
			char oil = c2[5];
			if (oil == '0') {
				details.setOilState("供油正常");
			} else {
				details.setOilState("油路断开");
			}
			// 供电
			char eng = c2[4];
			if (eng == '0') {
				details.setEngState("供电正常");
			} else {
				details.setEngState("电路断开");
			}
			// 车门
			char door = c2[3];
			if (door == '0') {
				details.setDoorState("车门解锁");
			} else {
				details.setDoorState("车门加锁");
			}
			 
				// 低八位
						String a1 = alarm.substring(6, 8);
						// 次八位
						String a2 = alarm.substring(4, 6);
						String a3 =alarm.substring(2,4);
						String a4=alarm.substring(0,2);
						// 10进制转16进制
						Integer ai1 = Integer.decode("0x" + a1);
						Integer ai2 = Integer.decode("0x" + a2);
						Integer ai3=Integer.decode("0x"+a3);
						Integer ai4=Integer.decode("0x"+a4);
						// 转字节
						byte ab1 = ai1.byteValue();
						byte ab2 = ai2.byteValue();
						byte ab3=ai3.byteValue();
						byte ab4=ai4.byteValue();
						// 16进制转2进制
						String aa = toBinaryString(ab1);
						String aaa = toBinaryString(ab2);
						String aaaa=toBinaryString(ab3);
						String aaaaa=toBinaryString(ab4);
						// 拆分字符串
						char[] a = aa.toCharArray();
						char[] b = aaa.toCharArray();
						char[] cc=aaaa.toCharArray();
						char[] d=aaaaa.toCharArray();
						//初始化报警状态
						details.setAlarmState("无报警");
						// 紧急报警
						char emergency = a[7];
						if(emergency=='1'){
							details.setAlarmState("紧急报警");
						}
						//超速报警
						char rateOut = a[6];
						if(rateOut=='1'){
							details.setAlarmState("超速报警");
						}
						//疲劳驾驶
						char  fatigueDrive = a[5];
						if(fatigueDrive=='1'){
							details.setAlarmState("疲劳驾驶");
						}
						//预警
						char  earlyWarn = a[4];
						if(earlyWarn=='1'){
							details.setAlarmState("预警");
						}
						//GNSS模块发生故障
						char gnssFault = a[3];
						if(gnssFault=='1'){
							details.setAlarmState("GNSS故障");
						}
						//GNSS天线未接或剪断
						char gnssCut = a[2];
						if(gnssCut=='1'){
							details.setAlarmState("gnss天线未接或断开");
						}
						//GNSS天线短路
						char gnssShort = a[1];
						if(gnssShort=='1'){
							details.setAlarmState("gnss天线短路");
						}
						//终端主电源欠压
						char underVoltage = a[0];
						if(underVoltage=='1'){
							details.setAlarmState("终端电源欠压");
						}
						//终端主电压掉电
						char powerFail=b[7];
						if(powerFail=='1'){
							details.setAlarmState("终端主电压掉电");
						}
						//终端lcd显示器故障
						char lcdFault=b[6];
						if(lcdFault=='1'){
							details.setAlarmState("lcd显示器故障");
						}
						//tts模块故障
						char ttsFault=b[5];
						if(ttsFault=='1'){
							details.setAlarmState("tts模块故障");
						}
						//摄像头故障
						char cameraFault=b[4];
						if(cameraFault=='1'){
							details.setAlarmState("摄像头故障");
						}
						//累计驾驶超时
						char driveOvertime=cc[5];
						if(driveOvertime=='1'){
							details.setAlarmState("驾驶超时");
						}
						//超时停车
						char stopOvertime=cc[4];
						if(stopOvertime=='1'){
							details.setAlarmState("超时停车"); 						}
						//进出区域
						char inOutRange=cc[3];
						if(inOutRange=='1'){
							details.setAlarmState("进出区域");
						}
						//进出路线
						char inOutPath=cc[2];
						if(inOutPath=='1'){
							details.setAlarmState("进出路线");
						}
						//路段行驶时间不足/过长
						char roadTime=cc[1];
						if(roadTime=='1'){
							details.setAlarmState("路段行驶时间不足/过长");
						}
						//路线偏离报警
						char roadDeviate=cc[0];
						if(roadDeviate=='1'){
							details.setAlarmState("路线偏离");
						}
						//车辆VSS故障
						char vssFault=d[7];
						if(vssFault=='1'){
							details.setAlarmState("VSS故障");
						}
						//车辆油量异常
						char oilFault=d[6];
						if(oilFault=='1'){
							details.setAlarmState("油量异常");
						}
						//车辆被盗
						char carSteal=d[5];
						if(carSteal=='1'){
							details.setAlarmState("车辆被盗");
						}
						//车辆非法点火
						char illegalStart=d[4];
						if(illegalStart=='1'){
							details.setAlarmState("非法点火");
						}
						//车辆非法位移
						char illegalMove=d[3];
						if(illegalMove=='1'){
							details.setAlarmState("非法位移");
						}
						//碰撞侧翻报警
						char impactTurn=d[2]; 
						if(impactTurn=='1'){
							details.setAlarmState("碰撞侧翻");
						}  
						//---------------------- 
		 
		} else {
			details.setAccState("未获取到");
			details.setDirectionState("未获取到");
			details.setDoorState("未获取到");
			details.setEngState("未获取到");
			details.setKeyState("未获取到");
			details.setLatState("未获取到");
			details.setLonState("未获取到");
			details.setMeterState("未获取到");
			details.setOilState("未获取到");
			details.setAlarmState("未获取到");
		}
	 
		
		return details;
	}

	public static String toBinaryString(byte b) {
		String binary = null;
		int i = b;
		String s = Integer.toBinaryString(i);
		if (i >= 0) {
			int len = s.length();
			if (len < 8) {
				int offset = 8 - len;
				for (int j = 0; j < offset; j++) {
					s = "0" + s;
				}
			}
			binary = s;
		} else {
			binary = s.substring(24);
		}
		return binary;
	}
	
	/**
	 * wk
	 * 车辆列表后台分页-模糊查询-显示条数-查询
	 * @param datatablesQuery
	 * @param size
	 * @return
	 */
	@Override
	public Integer getTaxiSize(DatatablesQuery datatablesQuery, String[] taxiNums) {
		
		List<String> isuNums = new ArrayList<>();
		/*for(String taxiNum : taxiNums){

			String hql = "SELECT t FROM Taxi t WHERE taxiNum=:taxiNum";
			Query query = getSession().createQuery(hql);
			Taxi taxi = (Taxi) query.setString("taxiNum", taxiNum).uniqueResult();

			if(taxi!=null){
				isuNums.add(taxi.getIsuNum());
			}
		}*/
		
		for(String taxiNum : taxiNums){
			String hql = "SELECT isuNum FROM Taxi t  WHERE taxiNum=:taxiNum";
			String isu = (String) getSession().createQuery(hql).setString("taxiNum", taxiNum).uniqueResult();
			if (null != isu) {
				isuNums.add(isu);
			}
		}
		
		List<Address> address = new ArrayList<>();
		for (String isuNum : isuNums) {
			//查询模糊记录条数
			DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ADDRESSTRMP);
			BasicDBObject finds = new BasicDBObject();

			finds.put("isuNum", isuNum);
			BasicDBObject finds2 = (BasicDBObject) coll.findOne(finds);
			if (finds2 != null) {
				Address a = new Address(finds2);
				if (a != null) {
					// 定位之后坐标转换
					if (a.getLatitude() != 0 && a.getLongitude() != 0) {
						Gps gps = PositionUtil.gps84_To_Gcj02(a.getLatitude(), a.getLongitude());
						a.setLatitude(gps.getWgLat());
						a.setLongitude(gps.getWgLon());
						gps = PositionUtil.gcj02_To_Bd09(a.getLatitude(), a.getLongitude());
						a.setLatitude(new BigDecimal(gps.getWgLat()).setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue());
						a.setLongitude(new BigDecimal(gps.getWgLon()).setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue());
						address.add(a);
					} else {
						
					}
				}
			} else {
				continue;
			}		
		}
		if (address.size() == 0) {
			return null;
		}
		// 在线状态表。
		DBCollection find = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.onlineTemp);
		BasicDBObject query = new BasicDBObject();
		// 添加车牌号、在线状态信息
		for (Address a : address) {
			// 查询条件
			Date time = new Date(000000);
			query.append("isuNum", a.getIsuNum());
			// System.out.println("手机号"+a.getIsuNum());

			if (find.find(query).hasNext()) {
				DBObject object = find.findOne(query);
				time = (Date) object.get("time");
				// System.out.println("time为"+time);
			}

			Taxi t = getTaxiByIsu(a.getIsuNum());
			a.setTaxiNum(t.getTaxiNum());
			// 在线状态的判断由在线实时表来得出。（时差300秒以内视为在线）

			// 而是否定位由位置信息得出
			try {
				String state = a.getState();
				String s1 = state.substring(6, 8);
				Integer i1 = Integer.decode("0x" + s1);
				byte b1 = i1.byteValue();
				String ss = toBinaryString(b1);
				char[] c = ss.toCharArray();
				char sta = c[6];
				// System.out.println(c);
				if (time.getTime() >= DateUtil.stringToDate(DateUtil.countDate(FarmConstant.ONLINE)).getTime()) {
					// System.out.println("符合条件");
					// System.out.println("位置"+a.getLatitude());
					a.setState("在线");
					Date time2 = DateUtil.stringToDate(a.getTime());
					if (time2.getTime() >= DateUtil.stringToDate(DateUtil.countDate(FarmConstant.LOCATION)).getTime()) {
						if (sta == '1') {
							a.setLocateState("定位");
						} else {
							a.setLocateState("不定位");
						}
					} else {
						a.setLocateState("不定位");
					}
				} else {
					// if (sta=='1') {
					// a.setLocateState("定位");
					// }else{a.setLocateState("不定位");}
					// System.out.println("状态：" + a.getState());
					a.setState("离线");
					a.setLocateState("不定位");
				}

			} catch (Exception e) {
				a.setState("在线状态异常");
			}
			// 根据计价器实时表!，得出车辆的运营状态
			int n = getTempMeterByIsuNum(a.getIsuNum()).getState();
			String operateState = null;
			switch (n) {
			case 0:
				operateState = "空车";
				break;
			case 1:
				operateState = "载客";
				break;
			case 2:
				operateState = "电召";
				break;
			case 3:
				operateState = "暂停";
				break;
			default:
				operateState = "空车";
				break;
			}
			a.setOperateState(operateState);
			query.clear();
		}
		//System.out.println("获取模糊Size的查询集合："+address.toString());
		
	   List<Address> results = new ArrayList<Address>();
	   Pattern pattern = Pattern.compile(datatablesQuery.getSearch());
	   for(int i=0; i < address.size(); i++){
		  //集合模糊匹配 不能匹配速度、经度、纬度
	      Matcher matcher = pattern.matcher((address.get(i)).getTaxiNum());
//	      Matcher matcher1 = pattern.matcher((address.get(i)).getSpeed());
	      Matcher matcher2 = pattern.matcher((address.get(i)).getOperateState());
//	      Matcher matcher3 = pattern.matcher((address.get(i)).getLatitude());
//	      Matcher matcher4 = pattern.matcher((address.get(i)).getLongitude());
	      Matcher matcher5 = pattern.matcher((address.get(i)).getState());
	      Matcher matcher6 = pattern.matcher((address.get(i)).getLocateState());
	      Matcher matcher7 = pattern.matcher((address.get(i)).getTime());
	      if(matcher.find()||matcher2.find()||matcher5.find()||matcher6.find()||matcher7.find()){
	         results.add(address.get(i));
	      }
	   }
	   return results.size();
		
	}
	
	/**
	 * wk
	 * 车辆列表后台模糊查询分页
	 * @param datatablesQuery
	 * @param isuNums
	 * @return
	 */
	@Override
	public Blur getTaxiList(DatatablesQuery datatablesQuery, String[] taxiNums) {
		List<String> isuNums = new ArrayList<>();
//		long time19 = System.currentTimeMillis();
		
		for(String taxiNum : taxiNums){
			String hql = "SELECT isuNum FROM Taxi t  WHERE taxiNum=:taxiNum";
			String isu = (String) getSession().createQuery(hql).setString("taxiNum", taxiNum).uniqueResult();
			if (null != isu) {
				isuNums.add(isu);
			}
		}
//		long time110 = System.currentTimeMillis();
//		System.out.println("模糊查终端号所用时间："+(time110-time19));
		
		/*for(String taxiNum : taxiNums){
			String hql = "SELECT t FROM Taxi t WHERE taxiNum=:taxiNum";
			Query query = getSession().createQuery(hql);
			Taxi taxi = (Taxi) query.setString("taxiNum", taxiNum).uniqueResult();

			if(taxi!=null){
				isuNums.add(taxi.getIsuNum());
			}
		}*/
		
		//System.out.println("模糊终端号："+isuNums.toString());
		

		List<Address> address = new ArrayList<>();
//		long time11 = System.currentTimeMillis();
		for (String isuNum : isuNums) {
			//查询模糊记录条数
			DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ADDRESSTRMP);
			BasicDBObject finds = new BasicDBObject();

			finds.put("isuNum", isuNum);
			BasicDBObject finds2 = (BasicDBObject) coll.findOne(finds);
			if (finds2 != null) {
				Address a = new Address(finds2);
				if (a != null) {
					// 定位之后坐标转换
					if (a.getLatitude() != 0 && a.getLongitude() != 0) {
						Gps gps = PositionUtil.gps84_To_Gcj02(a.getLatitude(), a.getLongitude());
						a.setLatitude(gps.getWgLat());
						a.setLongitude(gps.getWgLon());
						gps = PositionUtil.gcj02_To_Bd09(a.getLatitude(), a.getLongitude());
						a.setLatitude(new BigDecimal(gps.getWgLat()).setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue());
						a.setLongitude(new BigDecimal(gps.getWgLon()).setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue());
						address.add(a);
					} else {
						
					}
				}
			} else {
				continue;
			}		
		}
//		long time12 = System.currentTimeMillis();
//		System.out.println("模糊查询mongo所用时间："+(time12-time11));
		if (address.size() == 0) {
			return null;
		}
//		long time13 = System.currentTimeMillis();
		// 在线状态表。
		DBCollection find = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.onlineTemp);
		BasicDBObject query = new BasicDBObject();
		// 添加车牌号、在线状态信息
		for (Address a : address) {
			// 查询条件
			Date time = new Date(000000);
			query.append("isuNum", a.getIsuNum());
			// System.out.println("手机号"+a.getIsuNum());

			if (find.find(query).hasNext()) {
				DBObject object = find.findOne(query);
				time = (Date) object.get("time");
				// System.out.println("time为"+time);
			}

			Taxi t = getTaxiByIsu(a.getIsuNum());
			a.setTaxiNum(t.getTaxiNum());
			// 在线状态的判断由在线实时表来得出。（时差300秒以内视为在线）

			// 而是否定位由位置信息得出
			try {
				String state = a.getState();
				String s1 = state.substring(6, 8);
				Integer i1 = Integer.decode("0x" + s1);
				byte b1 = i1.byteValue();
				String ss = toBinaryString(b1);
				char[] c = ss.toCharArray();
				char sta = c[6];
				// System.out.println(c);
				if (time.getTime() >= DateUtil.stringToDate(DateUtil.countDate(FarmConstant.ONLINE)).getTime()) {
					// System.out.println("符合条件");
					// System.out.println("位置"+a.getLatitude());
					a.setState("在线");
					Date time2 = DateUtil.stringToDate(a.getTime());
					if (time2.getTime() >= DateUtil.stringToDate(DateUtil.countDate(FarmConstant.LOCATION)).getTime()) {
						if (sta == '1') {
							a.setLocateState("定位");
						} else {
							a.setLocateState("不定位");
						}
					} else {
						a.setLocateState("不定位");
					}
				} else {
					// if (sta=='1') {
					// a.setLocateState("定位");
					// }else{a.setLocateState("不定位");}
					// System.out.println("状态：" + a.getState());
					a.setState("离线");
					a.setLocateState("不定位");
				}

			} catch (Exception e) {
				a.setState("在线状态异常");
			}
			// 根据计价器实时表!，得出车辆的运营状态
			int n = getTempMeterByIsuNum(a.getIsuNum()).getState();
			String operateState = null;
			switch (n) {
			case 0:
				operateState = "空车";
				break;
			case 1:
				operateState = "载客";
				break;
			case 2:
				operateState = "电召";
				break;
			case 3:
				operateState = "暂停";
				break;
			default:
				operateState = "空车";
				break;
			}
			a.setOperateState(operateState);
			query.clear();
		}
//		long time14 = System.currentTimeMillis();
//		System.out.println("模糊组装数据所用时间："+(time14-time13));
		//System.out.println("获取模糊Size的查询集合："+address.toString());
//		long time15 = System.currentTimeMillis();

	   List<Address> results = new ArrayList<Address>();
	   Pattern pattern = Pattern.compile(datatablesQuery.getSearch());
	   for(int i=0; i < address.size(); i++){
	      Matcher matcher = pattern.matcher((address.get(i)).getTaxiNum());
//	      Matcher matcher1 = pattern.matcher((address.get(i)).getSpeed());
	      Matcher matcher2 = pattern.matcher((address.get(i)).getOperateState());
//	      Matcher matcher3 = pattern.matcher((address.get(i)).getLatitude());
//	      Matcher matcher4 = pattern.matcher((address.get(i)).getLongitude());
	      Matcher matcher5 = pattern.matcher((address.get(i)).getState());
	      Matcher matcher6 = pattern.matcher((address.get(i)).getLocateState());
	      Matcher matcher7 = pattern.matcher((address.get(i)).getTime());
	      if(matcher.find()||matcher2.find()||matcher5.find()||matcher6.find()||matcher7.find()){
	         results.add(address.get(i));
	      }
	   }
//	   long time16 = System.currentTimeMillis();
//	   System.out.println("模糊符合条件所用时间："+(time16-time15));	   
//	   System.out.println("长度"+results.size());
//	   System.out.println("条件一"+datatablesQuery.getStart());
//	   System.out.println("条件二"+datatablesQuery.getLength());
	   List<Address> results2 = new ArrayList<Address>();
	   Integer size = results.size();
	   if(datatablesQuery.getStart()+datatablesQuery.getLength()<=results.size()){
		   results2 = results.subList(datatablesQuery.getStart(), datatablesQuery.getStart()+datatablesQuery.getLength());
	   }else{
		   results2 = results.subList(datatablesQuery.getStart(), results.size());
	   }
	   
	   
	   Blur blur = new Blur();
	   blur.setSize(size);
	   blur.setList(results2);
	   
//	   long time17 = System.currentTimeMillis();
//	   System.out.println("模糊组装返回条件所用时间："+(time17-time16));
	   return blur;
	}
	
	/**
	 * wk
	 * 车辆列表后台无模糊分页与轮询查询mysql终端号
	 * @param datatablesQuery
	 * @param isuNums
	 * @return
	 */
	@Override
	public Taxi getTaxiByTaxi2(DatatablesQuery datatablesQuery, String taxiNum) {
		String hql = "SELECT t FROM Taxi t WHERE taxiNum=:taxiNum";
		Query query = getSession().createQuery(hql);
		//System.out.println("dao数据："+query);
		Taxi taxi = (Taxi) query.setString("taxiNum", taxiNum).uniqueResult();

		return taxi;
	}

	@Override
	public Taxi getTaxiByTaxi(String taxiNum) {
		String hql = "SELECT t FROM Taxi t WHERE taxiNum=:taxiNum";
		Query query = getSession().createQuery(hql);
		Taxi taxi = (Taxi) query.setString("taxiNum", taxiNum).uniqueResult();
		return taxi;
	}

	@Override
	public Taxi getTaxiByIsu(String isuNum) {
		String hql = "SELECT t FROM Taxi t WHERE isuNum=:isuNum";
		Query query = getSession().createQuery(hql);
		Taxi taxi = (Taxi) query.setString("isuNum", isuNum).uniqueResult();
		return taxi;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Taxi> selectTaxi(String taxiNum, String taxiType, String taxiColor, String company) {
		List<String> list = new ArrayList<>();
		String str = "";
		List<Taxi> taxis = new ArrayList<>();
		if (!(taxiType == null || taxiType.length() <= 0)) {
			list.add(taxiType);
			str += " AND taxiType=?";
		}
		if (!(taxiColor == null || taxiColor.length() <= 0)) {
			list.add(taxiColor);
			str += " AND taxiColor=?";
		}
		// 因为在车辆表中添加了外键关联，所以修改查询的方式
		/*
		 * Company c=new Company(); c.setId(4); c.setName(company);
		 * if(!(company==null||company.length()<=0)){ list.add(company); str+=
		 * " AND company=?"; }
		 */
		String hql = "SELECT t FROM Taxi t WHERE taxiNum LIKE ?" + str;
		Query query = getSession().createQuery(hql);

		if (list.size() == 0) {
			taxis = query.setString(0, "%" + taxiNum + "%").list();
		}
		if (list.size() == 1) {
			taxis = query.setString(0, "%" + taxiNum + "%").setString(1, list.get(0)).list();
		}
		if (list.size() == 2) {
			taxis = query.setString(0, "%" + taxiNum + "%").setString(1, list.get(0)).setString(2, list.get(1)).list();
		}
		/*
		 * if(list.size()==3){ taxis=query.setString(0,"%"+taxiNum+"%")
		 * .setString(1,list.get(0)) .setString(2,list.get(1))
		 * .setEntity(3,c).list(); }
		 */
		if (!(company == null || company.length() <= 0)) {
			List<Taxi> ts = new ArrayList<>();
			for (Taxi t : taxis) {
				if (t.getCompany().getName().equals(company)) {
					ts.add(t);
				}
			}
			return ts;
		} else {
			return taxis;
		}

	}

	@Override
	public void addCompany(Company company) {
		getSession().save(company);
	}

	@Override
	public void addDriver(Driver driver) {
		getSession().save(driver);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Taxi> getAllTaxi() {
		Criteria criteria = getSession().createCriteria(Taxi.class);
		List<Taxi> t = criteria.list();
		System.out.println("车辆" + t);
		return t;
	}

	@Override
	public List<Driver> getDriverByTaxi(String taxiNum) {
		// 修改数据库后，修改了该方法
		String hql = "SELECT t FROM Taxi t WHERE taxiNum=:taxiNum";
		Taxi taxi = (Taxi) getSession().createQuery(hql).setString("taxiNum", taxiNum).uniqueResult();
		Set<Driver> driver = taxi.getDrivers();
		List<Driver> drivers = new ArrayList<>();
		for (Driver d : driver) {
			drivers.add(d);
		}
		return drivers;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Driver> getAllDriver() {
		// System.out.println("任务2开始执行了");
		List<Driver> driver = getSession().createQuery("FROM Driver").list();
		return driver;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getIsuNumsByPany(List<Integer> ids) {
		List<String> isuNums = new ArrayList<>();
		Criteria criteria = getSession().createCriteria(Company.class);
		criteria.add(Restrictions.in("id", ids));
		List<Company> companys = criteria.list();
		// 去重，也不知道哪里来的重
		List<Company> newList = new ArrayList<>();
		Set<Company> set = new HashSet<Company>();
		for (Company c : companys) {
			if (set.add(c)) {
				newList.add(c);
			}
		}

		for (Company c : newList) {
			Set<Taxi> taxis = c.getTaxis();
			for (Taxi t : taxis) {
				isuNums.add(t.getIsuNum());
			}
		}
		return isuNums;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getTaxiNumsByPany(List<Integer> companyId) {
		List<String> taxiNums = new ArrayList<>();
		Criteria criteria = getSession().createCriteria(Company.class);
		criteria.add(Restrictions.in("id", companyId));
		List<Company> companys = criteria.list();
		// 去重，也不知道哪里来的重
		List<Company> newList = new ArrayList<>();
		Set<Company> set = new HashSet<Company>();
		for (Company c : companys) {
			if (set.add(c)) {
				newList.add(c);
			}
		}

		for (Company c : newList) {
			Set<Taxi> taxis = c.getTaxis();
			for (Taxi t : taxis) {
				taxiNums.add(t.getTaxiNum());
			}
		}
		return taxiNums;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ComInfo2> getPanyBytaxiNums(List<String> taxiNums) {
		// 首先通过车牌号的集合查询出相应的车辆
		Criteria criteria = getSession().createCriteria(Taxi.class);
		criteria.add(Restrictions.in("taxiNum", taxiNums));
		List<Taxi> taxis = criteria.list();
		// 获取对应的公司并去重
		Set<Company> sets = new HashSet<>();
		for (Taxi t : taxis) {
			sets.add(t.getCompany());
		}
		// 把公司信息和出租车信息存放到工具类中
		List<ComInfo2> companys = new ArrayList<>();
		for (Company c : sets) {
			ComInfo2 com = new ComInfo2();
			com.setComId(c.getId());
			com.setCname(c.getName());
			for (Taxi taxi : taxis) {
				if (taxi.getCompany().getId() == c.getId()) {
					TaxiInfo2 t2 = new TaxiInfo2();
					t2.setTaxiId(taxi.getId());
					t2.setTaxiNum(taxi.getTaxiNum());
					Set<TaxiInfo2> set2 = com.getTaxis();
					set2.add(t2);
					com.setTaxis(set2);
					// taxis.remove(taxi);
				}
			}
			companys.add(com);
		}
		return companys;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Driver> getDriverByTaxiId(Integer id) {
		String hql = "SELECT d FROM Driver d WHERE taxiId=:taxiId";
		List<Driver> drivers = getSession().createQuery(hql).setInteger("taxiId", id).list();
		return drivers;
	}

	@Override
	public Company getCompanyBytaxi(String taxiNum) {
		String hql = "SELECT t FROM Taxi t WHERE taxiNum=:taxiNum";
		Taxi taxi = (Taxi) getSession().createQuery(hql).setString("taxiNum", taxiNum).uniqueResult();
		Company c = taxi.getCompany();
		return c;
	}

	@Override
	public List<IsuCache> getAllIsuCache() {
		@SuppressWarnings("unchecked")
		List<IsuCache> Isu = getSession().createQuery("FROM IsuCache").list();
		return Isu;

	}

	@Override
	public void addIsuCache(Taxi taxi, int type) {
		IsuCache isuCache = new IsuCache();
		isuCache.setIsu(taxi.getIsuNum());
		isuCache.setType(type);
		isuCache.setDate(taxi.getAddDate());
		if (type == 1) {
			System.out.println("加入插入操作到缓存表");
			getSession().save(isuCache);
		} else if (type == 3) {
			System.out.println("加入删除操作到缓存表");
			getSession().save(isuCache);
		}
	}

	@Override
	public void deleteIsuCacheById(IsuCache isucache) {
		getSession().delete(isucache);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<IsuCache> getIsuCacheForDelete() {
		String hql = "SELECT i FROM IsuCache i WHERE type=:type";
		List<IsuCache> IsuDelete = getSession().createQuery(hql).setInteger("type", 3).list();
		return IsuDelete;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<IsuCache> getIsuCacheForSave() {
		String hql = "SELECT i FROM IsuCache i WHERE type=:type";
		List<IsuCache> IsuSave = getSession().createQuery(hql).setInteger("type", 1).list();
		return IsuSave;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<IsuCache> getIsuCacheForUpdate() {
		String hql = "SELECT i FROM IsuCache i WHERE type=:type";
		List<IsuCache> IsuUpdate = getSession().createQuery(hql).setInteger("type", 2).list();
		return IsuUpdate;

	}

	@Override
	public void addIsu(String Isu) {
		DBCollection db = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ISU);
		BasicDBObject object = new BasicDBObject();
		if (Isu != null) {
			object.put("isuNum", Isu);
			db.save(object);

		}
	}

	@Override
	public void deleteIsu(String isu) {
		DBCollection db = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ISU);
		BasicDBObject object = new BasicDBObject();
		if (isu != null) {
			object.put("isuNum", isu);
			db.remove(object);

		} else {
			System.out.println("已删除。。。。。。");

		}
	}

	@Override
	public void updateIsu(String isu, String isu2) {
		DBCollection db = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ISU);
		BasicDBObject object = new BasicDBObject();
		object.put("isuNum", isu2);
		if (db.find(object) != null) {

			db.remove(object);
			object.clear();
			object.put("isuNum", isu);
			db.save(object);
		} else {
			object.clear();
			object.put("isuNum", isu);
			db.save(object);
		}
	}

	@Override
	public boolean findIsu(String isu) {
		DBCollection db = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ISU);
		BasicDBObject object = new BasicDBObject();
		object.put("isuNum", isu);
		DBCursor dc = db.find(object).limit(1);
		if (dc.length() == 1) { // 判断指针的长度
			return true;
		}
		return false;
	}

	@Override
	public String getIsuById(Integer id) {
		String hql = "SELECT isuNum FROM Taxi t  WHERE id=:id";
		List isu = getSession().createQuery(hql).setInteger("id", id).list();
		String is = null;
		if (isu.get(0)!= null) {
			is=isu.get(0).toString();
			return is;
		}
		return null;
	}

	@Override
	public void addIsuCache(Taxi taxi, String isu2, int type) {

		IsuCache isuCache = new IsuCache();
		isuCache.setIsu(isu2);
		isuCache.setIsu2(taxi.getIsuNum());
		isuCache.setType(type);
		isuCache.setDate(taxi.getAddDate());
		getSession().save(isuCache);
	}
	
	//WK新增 首页车辆列表模糊查询添加
	@Override
	public MeterTemp getTempMeterByIsuNum(String isuNum) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.METERTEMP);
		BasicDBObject finds = new BasicDBObject();
		finds.put("isuNum", isuNum);
		finds = (BasicDBObject) coll.findOne(finds);
		MeterTemp meter = new MeterTemp();
		if (finds != null) {
			meter = new MeterTemp(finds);
		}
		return meter;
	}

	//wk新增 查询所有终端号
	@SuppressWarnings("unchecked")
	@Override
	public String[] getAllIsuNums() {
		String hql = "SELECT isuNum FROM Taxi";
		List<String> list = getSession().createQuery(hql).list();
		String[] isuNums=list.toArray(new String[list.size()]);
		//System.out.println("查询所有终端号"+isuNums.toString());
		return isuNums;
	}
	
	//wk新增 根据车牌号查终端号
	@Override
	public String getIsuByTaxiNum(String taxiNum) {
		String hql = "SELECT isuNum FROM Taxi t  WHERE taxiNum=:taxiNum";
		String isu = (String) getSession().createQuery(hql).setString("taxiNum", taxiNum).uniqueResult();
		String is = null;
		if (null != isu) {
			//System.out.println("********************查取的终端号："+isu);
			is=isu;
			return is;
		}
		//System.out.println("********************查取的终端号空了？");
		return null;
	}
	
	//wk新增 根据车牌号查终端号 不带汉字车牌号
	@Override
	public String getIsuByTaxiNum2(String taxiNum) {
		String hql = "SELECT isuNum FROM Taxi t WHERE t.taxiNum LIKE '%" + taxiNum +"%'";
		List<String> isu = getSession().createQuery(hql).list();
		if(isu!=null&&isu.size()>0){
			return isu.get(0);
		}
		return null;
	}

}
