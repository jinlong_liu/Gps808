<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<script src="js/jquery.js"></script>

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">

<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>

<!--  添加表单验证 -->
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />
<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>
</head>

<body class="gray-bg">
<div class="wrapper animated ">
		<!-- 查询列表 -->
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>查询时段信息</h5>
				</div>
				<div class="ibox-content">
					<form action="getAdsControl" class="form-horizontal" method="post" >
						<div class="row">
							<div class="form-group">

								<label class="col-sm-1 control-label">名称</label>
								<div class="col-sm-2">
									<input type="text" class="form-control" name="adsTimeName">
								</div>

								<!-- 修改 --> 
								<div class="col-sm-2">
									<button type="submit" class="btn btn-primary">查询</button>

									<button id="btn_add" type="button" class="btn btn-primary"
										data-toggle="modal" data-target="#myModal5">添加</button>

								</div>

							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- 点击添加按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal5" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form id="saveAdsControl" class="form-horizontal"
						action="saveAdsControl" method="post">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>新增时段信息<span style="color:red">（* 为必填项）</span></h5>
							</div>
							<div class="ibox-content">
								<!-- 第一行 -->
								<div class="row">
									<div class="col-sm-10">
										<div class="form-group">
											<!-- 时段编码-->
											<label class="col-sm-4 control-label"><span style="color:red">* </span>时段号：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="adsTimeId" />
											</div>
										</div>
									</div>
                                  </div>
									<!-- 第二行 -->
									  <div class="row"> 
										<div class="col-sm-10">
											<div class="form-group">
												<!-- 时段名称 -->
										<label class="col-sm-4 control-label"><span style="color:red">* </span>时段名称：</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" name="adsTimeName" />
												</div>
											</div>
										</div>

								</div>
								<!-- 第三行 -->
								<div class="row">
									<div class="col-sm-10">
										<div class="form-group">
											<!-- 开始时间-->
											<label class="col-sm-4 control-label"><span style="color:red">* </span>开始时间：</label>
											<div class="col-sm-8">
												<input id="TimeStart" class="form-control" type="text"
													name="adsTimeStart"
													onfocus="WdatePicker({lang:'zh-cn',dateFmt:'HH:mm',readOnly:true })">
											</div>
										</div>
									</div>
								</div>
								   <!-- 第四行 -->
									<!--结束时间 -->
									<div class="row">
									<div class="col-sm-10">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span style="color:red">* </span>结束时间：</label>
											<div class="col-sm-8">
												<input id="TimeEnd" type="text" name="adsTimeEnd"
													class="form-control"
													onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'TimeStart\')}',dateFmt:'HH:mm' ,readOnly:true})">
											</div>
										</div>
									</div>
								</div>


								<div class="modal-footer">
									<button type="button" class="btn btn-white"
										data-dismiss="modal">关闭</button>
									<button type="submit" class="btn btn-primary">保存</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>



		<!-- 数据展示 -->
		<div class="row">
			<div class="col-sm-12">

				<div class="ibox-title">
					<div>
						<h5>&nbsp;&nbsp;&nbsp;时段信息表</h5>
					</div>
				</div>
				<div class="ibox-content"  style="height: 653px">

					<table id="table1" 
						style="table-layout: fixed; background: #FFFFFF"
						class="footable table table-stripped" data-page-size="10"
						data-filter=#filter>
						<thead>
							<tr>
								<th width="70">时段号</th>
								<th width="70">时段名称</th>
								<th width="70">开始时间</th>
								<th width="70">结束时间</th>

								<th width="100">操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${adsTime}" var="ads">
								<tr>
									<td>${ads.adsTimeId}</td>
									<td>${ads.adsTimeName}</td>
									<td>${ads.adsTimeStart}</td>
									<td>${ads.adsTimeEnd}</td>
									<td>
										<button id="btn-update" type="button"
											class="btn btn-success btn-sm" data-toggle="modal"
											data-target="#myModal1"
											onclick="update('${ads.adsTimeId}','${ads.adsTimeName}','${ads.adsTimeStart}','${ads.adsTimeEnd}')">修改</button>
										<button type="button" class="btn btn-danger btn-sm"
											 onclick="deleteId('${ads.adsTimeId}','${ads.adsTimeName}')">删除</button>
</td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="5">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		<!-- 删除表格 -->
		<form id="deleteForm" action="deleteAdsTimeControl" method="post">
			<input type="hidden" id="deleteId" name="id">
		</form>

		<!-- 点击"修改"按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal1" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form action="updateAdsTimeControl" method="post" class="form-horizontal"id="updateAdsTimeControlForm">
						<!-- <input id="id" name="id" type="hidden" > -->
						<div class="modal-body">
							<div class="ibox-title">
								<h5>修改时段信息<span style="color:red">（* 为必填项）</span></h5>
							</div>
							<div class="ibox-content">
								<div class="row">
									<div class="col-sm-10">
										<div class="form-group">
											<!-- 时段编码-->
											<label class="col-sm-4 control-label"><span style="color:red">* </span>时段号：</label>
											<div class="col-sm-8">
											<input type="text"  id="adsTimeId"
											 class="form-control" disabled="disabled">
											 <input type="hidden" name="adsTimeId" id="adsTime">
											</div>
										</div>
								   </div>
								</div>
								<div class="row">
								 <div class="col-sm-10">
									<div class="form-group">
												<!-- 时段名称 -->
										<label class="col-sm-4 control-label"><span style="color:red">* </span>时段名称：</label>
												<div class="col-sm-8">
													 <input type="text" name="adsTimeName" id="adsTimeName"class="form-control">
												</div>
											</div>
										</div>
								
								</div>


								<div class="row">
								  <div class="col-sm-10">
										<div class="form-group">
											<!-- 开始时间-->
											<label class="col-sm-4 control-label"><span style="color:red">* </span>开始时间：</label>
											<div class="col-sm-8">
												<input id="adsTimeStart" class="form-control" 
										        type="text" name="adsTimeStart"
                                                onfocus="WdatePicker({lang:'zh-cn',dateFmt:'HH:mm',readOnly:true})">
											</div>
										</div>
									</div>
								
								</div>
								<div class="row">
								 <div class="col-sm-10">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span style="color:red">* </span>结束时间：</label>
											<div class="col-sm-8">
												<input id="adsTimeEnd" type="text" name="adsTimeEnd"
											class="form-control"
											onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'adsTimeStart\')}',dateFmt:'HH:mm',readOnly:true})">
											</div>
										</div>
									</div>
									
								</div>
							<div class="modal-footer">
							<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
							<button type="submit" class="btn btn-primary">保存</button>
						    </div>

							</div>

						</div>


						

					</form>
				</div>
			</div>
		</div>
</div>
		<script src="js/plugins/sweetalert/sweetalert.min.js"></script>

		<script src="js/bootstrap.min.js?v=3.3.5"></script>
		<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
		<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
		<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
		<script src="js/plugins/footable/footable.all.min.js"></script>
		<script>
			//加载footable的功能
			$(document).ready(function() {
				$('#table1').DataTable({
					"pagingType" : "full_numbers"
				});
			});

			//点击"修改"按钮
			function update(id, name, timeStart,timeEnd) {
				$("#adsTimeId").val(id);
				$("#adsTime").val(id);
				$("#adsTimeName").val(name);
				$("#adsTimeStart").val(timeStart);
				$("#adsTimeEnd").val(timeEnd);
			};
			</script>
			<script>
			//点击"删除"按钮
			function deleteId(id,name) {
			swal({
				title : "您确定删除 时段" + name + " 的数据吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
				$("#deleteId").val(id);
				//提交表单
				$("#deleteForm").submit();
			})
		}
			$(document).ready(function(){
				var result='<%=request.getAttribute("state")%>';
					if(result != null && result != "null"){
						if (result.indexOf("成功")>0) {
						swal({
							title : result,
							type : "success"
						});
					}else{
						swal({
							title :result,
							type : "error"
						});
					}
					}

			});
		</script>

<script type="text/javascript">
	$(document).ready(function formValidator() {
			$('#saveAdsControl').bootstrapValidator({
				feedbackIcons : {/*输入框不同状态，显示图片的样式*/
					valid : 'glyphicon glyphicon-ok',
					invalid : 'glyphicon glyphicon-remove',
					validating : 'glyphicon glyphicon-refresh'
				},
				/*生效规则：字段值一旦变化就触发验证*/
				live : 'enabled',
				/*当表单验证不通过时，该按钮为disabled*/
				submitButtons : 'button[type="submit"]',
				/*验证*/
				fields : {
					adsTimeId : {
						message : '',
						validators : {
							notEmpty : {/*非空提示*/
								message : '时段号不能为空'
							},
							numeric : {
								message : '请输入数字'
							},remote : {
								url : 'hasTimeId',//验证地址
								message : '时段号已存在',
								delay : 800,
								type : 'POST'

							},
						}
					},
					adsTimeName : {
						message : '',
						validators : {
							notEmpty : {
								message : '时段名称不能为空'
							}
						}
					},
					adsTimeStart : {
						validators : {
							notEmpty : {
								message : '开始时间不能为空'
							}
						}
					},
					adsTimeEnd : {
						validators : {
							notEmpty : {
								message : '结束时间不能为空'
							}
						}
					}
					}
					

				
			});
		});


	$(document).ready(function formValidator() {
		$('#updateAdsTimeControlForm').bootstrapValidator({
			feedbackIcons : {/*输入框不同状态，显示图片的样式*/
				valid : 'glyphicon glyphicon-ok',
				invalid : 'glyphicon glyphicon-remove',
				validating : 'glyphicon glyphicon-refresh'
			},
			/*生效规则：字段值一旦变化就触发验证*/
			live : 'enabled',
			/*当表单验证不通过时，该按钮为disabled*/
			submitButtons : 'button[type="submit"]',
			/*验证*/
			fields : {
				adsTimeName : {
					message : '',
					validators : {
						notEmpty : {
							message : '时段名称不能为空'
						}
					}
				},
				adsTimeStart : {
					validators : {
						notEmpty : {
							message : '开始时间不能为空'
						}
					}
				},
				adsTimeEnd : {
					validators : {
						notEmpty : {
							message : '结束时间不能为空'
						}
					}
				}
				}
				

			
		});
	});


</script>
 <script> 

	 <!--  <script type="text/javascript">
			/*添加保险界面的表单验证*/
			$('#btn_add').click(function() {
				$('#saveAdsControl').bootstrapValidator({
					feedbackIcons : {/*输入框不同状态，显示图片的样式*/
						valid : 'glyphicon glyphicon-ok',
						invalid : 'glyphicon glyphicon-remove',
						validating : 'glyphicon glyphicon-refresh'
					},
					/*生效规则：字段值一旦变化就触发验证*/
					live : 'enabled',
					/*当表单验证不通过时，该按钮为disabled*/
					submitButtons : 'button[type="submit"]',
					/*验证*/
					fields : {
						adsTime : {/*键名username和input name值对应*/
							message : 'The username is not valid',
							validators : {
								notEmpty : {/*非空提示*/
									message : '车主或车牌号不能为空'
								}
							}
						},
						type : {
							validators : {
								notEmpty : {
									message : '保险类型不能为空'
								}
							}
						},
						coverage : {
							validators : {
								notEmpty : {
									message : '保险额度不能为空'
								},
								numeric : {
									message : '请输入数字'
								}
							}
						},
						payAmount : {
							validators : {
								notEmpty : {
									message : '缴费金额不能为空'
								},
								numeric : {
									message : '请输入数字'
								}
							}
						},
						validityStart : {
							validators : {
								notEmpty : {
									message : '有效期起不能为空'
								}
							}
						},
						validityEnd : {
							validators : {
								notEmpty : {
									message : '有效期止不能为空'
								}
							}
						},
						payTime : {
							validators : {
								notEmpty : {
									message : '缴费日期不能为空'
								}
							}
						}
					}
				});
			});
			 --> 
			function refreshValidator(id, name) {
				$(id).data('bootstrapValidator').updateStatus(name,
						'NOT_VALIDATED', null).validateField(name);
			}
		</script>  
</body>

</html>