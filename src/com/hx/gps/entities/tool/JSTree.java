package com.hx.gps.entities.tool;

import java.io.Serializable;

public class JSTree implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 唯一标识
	 */
	private String id;
	/**
	 * 父亲
	 */
	private String parent;
	/**
	 * 文本内容，名字
	 */
	private String text;
	/**
	 * 图标
	 */
	private String icon;
	/**
	 * 状态
	 */
	private State state;

	public JSTree() {
		super();
	}

	public JSTree(String id, String parent, String text, String icon, Boolean checked) {
		super();
		this.id = id;
		this.parent = parent;
		this.text = text;
		this.icon = icon;
		this.state = new State(checked);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "JSTree [id=" + id + ", parent=" + parent + ", text=" + text + ", icon=" + icon + ", state=" + state
				+ "]";
	}

	class State {
		private Boolean checked;

		public State(Boolean checked) {
			super();
			this.checked = checked;
		}

		@Override
		public String toString() {
			return "State [checked=" + checked + "]";
		}

	}
}
