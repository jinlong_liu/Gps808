package com.hx.gps.entities;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Breakrule entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "breakrule")
public class Breakrule implements java.io.Serializable {

	// Fields

	private Integer id;
	private Timestamp breakTime;
	private String taxiNum;
	private String driver;
	private String type;
	private Integer reScore;
	private Float reMoney;
	private String remark;
	private Integer state;
	private Timestamp dealTime;

	// Constructors

	/** default constructor */
	public Breakrule() {
	}

	/** full constructor */
	public Breakrule(Timestamp breakTime, String taxiNum, String driver,
			String type, Integer reScore, Float reMoney, String remark,
			Integer state, Timestamp dealTime) {
		this.breakTime = breakTime;
		this.taxiNum = taxiNum;
		this.driver = driver;
		this.type = type;
		this.reScore = reScore;
		this.reMoney = reMoney;
		this.remark = remark;
		this.state = state;
		this.dealTime = dealTime;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "breakTime", length = 19)
	public Timestamp getBreakTime() {
		return this.breakTime;
	}

	public void setBreakTime(Timestamp breakTime) {
		this.breakTime = breakTime;
	}

	@Column(name = "taxiNum")
	public String getTaxiNum() {
		return this.taxiNum;
	}

	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}

	@Column(name = "driver")
	public String getDriver() {
		return this.driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	@Column(name = "type")
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "reScore")
	public Integer getReScore() {
		return this.reScore;
	}

	public void setReScore(Integer reScore) {
		this.reScore = reScore;
	}

	@Column(name = "reMoney", precision = 12, scale = 0)
	public Float getReMoney() {
		return this.reMoney;
	}

	public void setReMoney(Float reMoney) {
		this.reMoney = reMoney;
	}

	@Column(name = "remark")
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "state")
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "dealTime", length = 19)
	public Timestamp getDealTime() {
		return this.dealTime;
	}

	public void setDealTime(Timestamp dealTime) {
		this.dealTime = dealTime;
	}

}