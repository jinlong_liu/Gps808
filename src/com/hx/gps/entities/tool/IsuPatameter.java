package com.hx.gps.entities.tool;

import com.hx.gps.util.DateUtil;
import com.mongodb.BasicDBObject;

/**
 * 终端参数
 * 
 * @author 作者 dream:
 * @version 创建时间：2018年7月3日 下午10:58:43 天道丶放飞梦想
 */
public class IsuPatameter {
	String isuNum;
	String taxiNum;
	String faceStatus;
	String powerStatus;
	String workStatus;
	String version;
	String date;

	public IsuPatameter(BasicDBObject finds) {
		this.isuNum = finds.getString("isuNum");
		this.faceStatus = finds.getString("faceStatus");
		this.powerStatus = finds.getString("powerStatus");
		this.workStatus = finds.getString("workStatus");
		this.version = finds.getString("version");
		this.date = DateUtil.dateToString(finds.getDate("date"));
	}

	public IsuPatameter(String isuNum, String taxiNum, String faceStatus, String powerStatus, String workStatus,
			String version, String date) {
		super();
		this.isuNum = isuNum;
		this.taxiNum = taxiNum;
		this.faceStatus = faceStatus;
		this.powerStatus = powerStatus;
		this.workStatus = workStatus;
		this.version = version;
		this.date = date;
	}

	public String getIsuNum() {
		return isuNum;
	}

	public void setIsuNum(String isuNum) {
		this.isuNum = isuNum;
	}

	public String getTaxiNum() {
		return taxiNum;
	}

	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}

	public String getFaceStatus() {
		return faceStatus;
	}

	public void setFaceStatus(String faceStatus) {
		this.faceStatus = faceStatus;
	}

	public String getPowerStatus() {
		return powerStatus;
	}

	public void setPowerStatus(String powerStatus) {
		this.powerStatus = powerStatus;
	}

	public String getWorkStatus() {
		return workStatus;
	}

	public void setWorkStatus(String workStatus) {
		this.workStatus = workStatus;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "{isuNum:'" + isuNum + "', taxiNum:'" + taxiNum + "', faceStatus:'" + faceStatus + "', powerStatus:'"
				+ powerStatus + "', workStatus:'" + workStatus + "', version:'" + version + "', date:'" + date + "}";
	}

}
