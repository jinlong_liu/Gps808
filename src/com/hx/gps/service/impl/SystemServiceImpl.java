package com.hx.gps.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hx.gps.dao.AddressDao;
import com.hx.gps.dao.SystemDao;
import com.hx.gps.dao.TaxiDao;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.tool.Alarm;
import com.hx.gps.entities.tool.ExportPoint;
import com.hx.gps.entities.tool.Gps;
import com.hx.gps.entities.tool.OrderVector;
import com.hx.gps.entities.tool.OverSpeed;
import com.hx.gps.entities.tool.Picture;
import com.hx.gps.entities.tool.Point;
import com.hx.gps.entities.tool.WaitOrder;
import com.hx.gps.service.SystemService;
import com.hx.gps.util.DateUtil;
import com.hx.gps.util.FarmConstant;
import com.hx.gps.util.PoiUtil;
import com.hx.gps.util.PositionUtil;

@Service
public class SystemServiceImpl implements SystemService {

	@Autowired
	private SystemDao systemDao;
	@Autowired
	private TaxiDao taxiDao;
	@Autowired
	private AddressDao addressDao;
	// 操作日志
	private static Log logger = LogFactory.getLog(SystemServiceImpl.class);

	@Override
	public String dealAlarm(Alarm alarm) {
		if (systemDao.updateAlarm(alarm)) {
			logger.info("更新报警" + alarm.toString());
			return "操作执行成功";
		} else {
			logger.info("更新报警记录失败" + alarm.toString());
			return "操作执行失败";
		}
	}

	@Override
	public List<Picture> getPicTureByIsu(String taxiNum, String startDate, String endDate) {
		logger.info("获取图片通过终端号" + taxiNum);
		// 首先根据车牌号来获取终端号
		Taxi taxi = taxiDao.getTaxiByTaxi(taxiNum);
		if (taxi == null) {
			return null;
		}
		String isuNum = taxi.getIsuNum();
		// 根据开始时间和结束时间来判断需要查询的数据库
		Integer start = Integer.parseInt(startDate.substring(2, 4) + startDate.substring(5, 7));
		Integer end = Integer.parseInt(endDate.substring(2, 4) + endDate.substring(5, 7));
		List<Picture> ps = new ArrayList<>();

		for (int i = start; i <= end; i++) {
			String dbname = FarmConstant.PICTURE + i;
			ps.addAll(systemDao.getPicTureByIsu(startDate, endDate, isuNum, dbname));
		}
		if (ps.size() > 0) {
			for (Picture p : ps) {
				p.setTaxiNum(taxiNum);
			}
		}
		logger.info("得到图片总数" + ps.size());
		return ps;
	}

	@Override
	public Object getPicTureByDate(String startDate, String endDate) {
		// 根据开始时间和结束时间来判断需要查询的数据库
		Integer start = Integer.parseInt(startDate.substring(2, 4) + startDate.substring(5, 7));
		Integer end = Integer.parseInt(endDate.substring(2, 4) + endDate.substring(5, 7));
		List<Picture> ps = new ArrayList<>();

		for (int i = start; i <= end; i++) {
			String dbname = FarmConstant.PICTURE + i;
			ps.addAll(systemDao.getPicTureByDate(startDate, endDate, dbname));
		}
		// 填充车牌号
		if (ps.size() > 0) {
			for (Picture p : ps) {
				p.setTaxiNum(taxiDao.getTaxiByIsu(p.getIsuNum()).getTaxiNum());
			}
		}
		logger.info("[拍照查询]得到图片总数" + ps.size());
		return ps;

	}

	@Override
	public List<Picture> getLastPic() {
		String dbname = FarmConstant.PICTURE + DateUtil.getNewDate();
		List<Picture> pics = systemDao.getLast5OfPic(dbname);
		if (pics.size() > 0) {
			for (Picture p : pics) {
				Taxi taxi = taxiDao.getTaxiByIsu(p.getIsuNum());
				if (taxi != null) {
					p.setTaxiNum(taxi.getTaxiNum());
				} else {
					p.setTaxiNum("未获取到");
				}
			}
		}
		return pics;
	}

	@Override
	public List<OrderVector> getOrderByDate(String startDate, String endDate) {
		List<OrderVector> orders = new ArrayList<>();
		logger.info("获取命令日志");
		// 首先获取时间段内所有的命令日志
		orders = systemDao.getOrderByLog(startDate, endDate);
		List<OrderVector> order2 = systemDao.getOrderByWait(startDate, endDate);
		if (order2 != null) {
			orders.addAll(order2);
		}
		// 添加相应的车牌号
		for (OrderVector o : orders) {
			Taxi taxi = taxiDao.getTaxiByIsu(o.getIsuNum());
			if (taxi != null) {
				o.setTaxiNum(taxi.getTaxiNum());
			} else {
				o.setTaxiNum("未获取到");
			}
		}
		logger.info("得到该时间段所有命令日志信息： " + orders);
		return orders;
	}

	@Override
	public List<OrderVector> getOrderLast5() {
		logger.info("获取最近五条命令日志");
		List<OrderVector> orders = new ArrayList<>();
		List<OrderVector> order2 = systemDao.getOrderLast5OfLog();
		if (order2 != null) {
			orders.addAll(order2);
		}
		// 添加相应的车牌号
		for (OrderVector o : orders) {
			Taxi taxi = taxiDao.getTaxiByIsu(o.getIsuNum());
			if (taxi != null) {
				o.setTaxiNum(taxi.getTaxiNum());
			}
		}
		logger.info("得到命令日志信息如下：" + orders);
		return orders;
	}

	@Override
	public boolean saveUpdateData(File file) {
		Calendar now = Calendar.getInstance();
		String version = "V" + now.get(Calendar.YEAR) + (now.get(Calendar.MONTH) + 1) + now.get(Calendar.DAY_OF_MONTH)
				+ now.get(Calendar.HOUR_OF_DAY) + now.get(Calendar.MINUTE) + now.get(Calendar.SECOND);
		byte[] data = new byte[(int) file.length()];
		InputStream in = null;
		try {
			// 以字节为单位读取文件内容，一次读一个字节
			in = new FileInputStream(file);
			int tempbyte;
			int i = 0;
			while ((tempbyte = in.read()) != -1) {
				data[i++] = (byte) tempbyte;
			}
			in.close();
		} catch (IOException e) {
			logger.error("【保存升级文件】格式转换异常" + e.getMessage());
			e.printStackTrace();
			return false;
		}
		if (systemDao.saveUpdateData(version, data, new Date())) {
			logger.info("保存升级文件成功");
			return true;
		} else {
			logger.info("保存升级文件失败");
			return false;
		}
	}

	@Override
	public List<String> getIsuVersion(List<Taxi> taxis) {
		// 版本集合
		List<String> IsuVersion = new ArrayList<String>();
		for (Taxi taxi : taxis) {
			if (taxi.getIsuNum() != null) {
				String isu = taxi.getIsuNum();
				// 调用dao，获取终端版本号。
				String version = systemDao.getIsuVersion(isu);
				IsuVersion.add(version);

			}
		}
		return IsuVersion;
	}

	@Override
	public List<String> getIsuversionDate(List<Taxi> taxis) {
		// 版本集合
		List<String> IsuVersionDate = new ArrayList<String>();
		for (Taxi taxi : taxis) {
			if (taxi.getIsuNum() != null) {
				String isu = taxi.getIsuNum();
				// 调用dao，获取终端版本号时间。
				Date versionDate = systemDao.getIsuVersionDate(isu);
				IsuVersionDate.add(DateUtil.dateToString(versionDate));

			}
		}
		return IsuVersionDate;
	}

	@Override
	public String saveIsuToWairUpgrade(String[] isu) {
		String latestVer = systemDao.latestVer();
		int count = isu.length;
		int success = 0;
		if (isu.length == 0) {
			return "请至少选择一个终端进行升级";
		}
		for (String i : isu) {
			if (systemDao.saveIsuToWairUpgrade(i, latestVer)) {
				success++;
			}
		}
		if (success == count) {
			logger.info("【下发升级】通知终端数" + success + "成功");
			return "已成功通知选中的所有终端进行升级，选中终端数：" + success;
		} else {
			logger.info("【下发升级】通知终端:成功数量" + success + "失败数量" + (count - success));
			return "命令执行完毕，已成功通知" + success + "台终端进行升级，另有" + (count - success) + "台终端通知失败";
		}
	}

	// wk全选升级下发保存
	@Override
	public String saveIsuToWairUpgrade2() {
		String[] isu = taxiDao.getAllIsuNums();
		String latestVer = systemDao.latestVer();
		int count = isu.length;
		int success = 0;
		if (isu.length == 0) {
			return "请至少选择一个终端进行升级";
		}
		for (String i : isu) {
			if (systemDao.saveIsuToWairUpgrade(i, latestVer)) {
				success++;
			}
		}
		if (success == count) {
			logger.info("【下发升级】通知终端数" + success + "成功");
			return "已成功通知选中的所有终端进行升级，选中终端数：" + success;
		} else {
			logger.info("【下发升级】通知终端:成功数量" + success + "失败数量" + (count - success));
			return "命令执行完毕，已成功通知" + success + "台终端进行升级，另有" + (count - success) + "台终端通知失败";
		}
	}

	// 指令下发
	@Override
	public int sendIsuContorl(WaitOrder w, String[] taxiNum) {
		logger.info("车辆" + taxiNum + "下发指令" + w.toString());
		List<Taxi> strList = new ArrayList<>();
		int k = 1;
		for (int i = 0; i < taxiNum.length; i++) {
			// 首先判断是否在线
			Taxi t = taxiDao.getTaxiByTaxi(taxiNum[i]);
			if (t == null) {
				strList.add(new Taxi(taxiNum[i], "车牌号错误"));
				continue;
			} else {
				String online = null;
				// 通过实时地址表来判断车辆是否在线。
				online = addressDao.getOnlineOfAddress(t.getIsuNum());
				if (!online.equals("在线")) {
					strList.add(new Taxi(taxiNum[i], "该车辆不在线"));
					logger.info("下发指令：车辆不在线" + taxiNum[i]);
					continue;
				}
			}
			w.setIsuNum(t.getIsuNum());
			systemDao.sendIsuInstruct(w);
			logger.info("下发断油断电：添加命令到待发送命令表" + w.toString());
		}
		return strList.size();
	}

	// 进入超速设置界面
	@Override
	public List<OverSpeed> getOverSpeed() {
		return systemDao.getOverSpeed();
	}

	// wk保存超速设置
	@Override
	public void saveOverSpeed(OverSpeed overSpeed) {
		systemDao.saveOverSpeed(overSpeed);
		logger.info("新增超速设置" + overSpeed.toString());
	}

	// wk新增 导出坐标
	@Override
	public void exportPoint(ExportPoint exportPoint, HttpServletResponse response) {
		String str = exportPoint.getPoint().toString();
		System.out.println("【导出经纬度】字符串：" + str);
		//转换经纬度
		List<Point> points=new ArrayList<>();
		if(exportPoint.getPoint()!=null){
			for (Point point : points) {
				Gps gps = PositionUtil.bd09_To_Gps84(point.getLatitude(), point.getLongitude());
				Point p=new Point();
				p.setLatitude(gps.getWgLat());
				p.setLongitude(gps.getWgLon());
				points.add(p);
			}
		}
		response.setCharacterEncoding("utf-8");
		// 设置响应内容的类型
		response.setContentType("text/plain");
		// 设置文件的名称和格式
		response.addHeader("Content-Disposition",
				"attachment; filename=" + PoiUtil.genAttachmentFileName("pointInfo"+ "_", "JSON_FOR_point_")
						+ MessageFormat.format("{0,date,yyyy-MM-dd HH:mm:ss}",
								new Object[] { Calendar.getInstance().getTime() })
						+ ".txt");// 通过后缀可以下载不同的文件格式
		BufferedOutputStream buff = null;
		ServletOutputStream outStr = null;
		try {
			outStr = response.getOutputStream();
			buff = new BufferedOutputStream(outStr);
			buff.write(delNull(str).getBytes("UTF-8"));
			buff.flush();
			buff.close();
		} catch (Exception e) {
			logger.error("导出文件文件出错，e:{}", e);
		} finally {
			try {
				buff.close();
				outStr.close();
			} catch (Exception e) {
				logger.error("关闭流对象出错 e:{}", e);
			}
		}
	}
    /**
     * 如果字符串对象为 null，则返回空字符串，否则返回去掉字符串前后空格的字符串
     * @param str
     * @return
     */
    public static String delNull(String str) {
            String returnStr="";
            if (StringUtils.isNotBlank(str)) {
                returnStr=str.trim();
            }
            return returnStr;
    }

	@Override
	public int getAlarmNum() {
		return addressDao.getAlarmNum().size();
	}

	@Override
	public int getAlarmNums() {
		return addressDao.getAlarmNums().size();
	}
}
