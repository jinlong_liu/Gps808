<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet"><!--表格排序相关 -->
<!-- Data Tables -->
 <link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet"><!--与表格样式有点关系  -->
<!-- <link href="css/animate.min.css" rel="stylesheet"> -->
<link href="css/style.min.css?v=4.0.0" rel="stylesheet"><!-- 基本样式 -->
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet"><!--表格样式相关  -->


<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
		<div class="col-sm-12">
				<!-- 日期+精确查询+导出表格开始 -->
				<!-- 	<div class="ibox float-e-margins"> -->
				<form action="getOrderInfoByDate" class="form-inline" id="getOneForm">
					<div class="ibox-title">
						<h5 class="text-center">查询命令记录</h5>
					</div>
					<div class="ibox-content">
					<div class="row">
							<span class="col-sm-4">
								<label class=" control-label">开始时间：</label><input type="text" class="form-control" name="startDate" id="startDate"
									value="${startDate}" class="Wdate"
									onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true})">
							</span>
				
						
								<span class="col-sm-4"><label class=" control-label">结束时间：</label> <input type="text" class="form-control" name="endDate" id="endDate"
								 value="${endDate}" class="Wdate"
									onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'startDate\')}',dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true})">
									</span>
					
					
								<span class="col-sm-4">
								<button id="getOneBtn" type="button"
									class="btn btn-primary ">查询</button>
							
								<button type="button" class="btn btn-success demo1">导出</button>
							</span>
					</div>
					</div>
				</form>
				<form action="derivedFormOfOrder" method="post" id="derivedForm">
					<div class="row">
							<div class="col-sm-4">
								<input type="hidden" name="startDate" id="start1">
							</div>
							<div class="col-sm-4">
								<input type="hidden" name="endDate" id="end1">
							</div>
						</div>
				</form>				
			</div>
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>命令信息</h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
							</a>
						</div>
					</div>
					<div class="ibox-content" style="height: 661px">
						<table
							class="table table-striped table-bordered table-hover dataTables-example">
							<thead>
								<tr>
									<th>车牌号</th>
									<th>添加时间</th>
									<th>命令序号</th>
									<th>命令类型</th>
									<th>标识</th>
									<th>发送人</th>
									<th>内容 </th>
									<th>状态 </th>
									<th>发送次数</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${order}" var="order">
									<tr>
										<td>${order.taxiNum}</td>
										<td>${order.time}</td>
										<td>${order.serialNum}</td>
										<td>${order.orderType}</td>
										<td>${order.identify}</td>
										<td>${order.people}</td>
										<td>${order.context}</td>
										<td>${order.state}</td>
										<td>${order.sendTime}</td>
									</tr>
								</c:forEach>
							</tbody>
							<tfoot>
							<tr>
								<td colspan="9">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
						</table>
					</div>
				</div>
			</div>
			
		</div>

	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script><!-- 显示表格样式必含 -->
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script><!--包含表格的翻页等功能  -->
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script><!-- 包含表格的样式 -->
	<script src="js/content.min.js?v=1.0.0"></script>
	<script>
		$(document)
				.ready(
						function() {
							//保留分页后的checkbox的值
							var checkedIds = "";
							$("input[name='chbox']")
									.change(
											function() {
												var oneches = document
														.getElementsByName("chbox");
												for (var i = 0; i < oneches.length; i++) {
													if (oneches[i].checked == true) {
														//避免重复累计id （不含该id时进行累加）
														if (checkedIds
																.indexOf(oneches[i].value) == -1) {
															checkedIds = checkedIds
																	+ oneches[i].value
																	+ ",";
														}
													}
													if (oneches[i].checked == false) {
														//取消复选框时 含有该id时将id从全局变量中去除
														if (checkedIds
																.indexOf(oneches[i].value) != -1) {
															checkedIds = checkedIds
																	.replace(
																			(oneches[i].value + ","),
																			"");
														}
													}
												}
												$("#manyIsu").val(checkedIds);
											});

							//点击"精确搜索"
							$("#getOneBtn").click(function() {
								var startTime = $("#startDate").val();
								var endTime = $("#endDate").val();
								var taxiNum = $("#taxiNum").val();
								var endYear = null;
								//开始时间的年份
								var startYear = startTime.substr(0, 4);
								//结束时间的年份
								if (endTime == "") {
									endYear = new Date().getFullYear();
								} else {
									endYear = endTime.substr(0, 4);
								}

								if (startTime == "") {
									swal({
										title : "精确搜索时，开始时间不能为空",
										type : "warning"
									});
								} else if (endYear != startYear) {
									swal({
										title : "不支持跨年查询，请重新选择开始时间或结束时间",
										type : "warning"
									});
								} else if (taxiNum == "") {
									swal({
										title : "请输入车牌号",
										type : "warning"
									});
								} else {
									$("#getOneForm").submit();
								}
							});

						/* 	//点击"多辆查询"按钮
							$("#getManyCar")
									.click(
											function() {
												var startTime = $("#startDate")
														.val();
												if (startTime == "") {
													swal({
														title : "多辆搜索时，开始时间不能为空",
														type : "warning"
													});
												} else {
													if ($(
															"input[type='checkbox']")
															.is(':checked')) {

														$("#startDateManyCar")
																.val(
																		$(
																				"#startDate")
																				.val());
														$("#endDateManyCar")
																.val(
																		$(
																				"#endDate")
																				.val());
														$("#manyFrom").submit();
													} else {
														swal({
															title : "多辆搜索时，请在要查询的车号前打上√号",
															type : "warning"
														});
													}
												}
											}); */

							//点击"导出表格"按钮
							$(".demo1").click(
									function() {
										var startDate = $("#startDate").val();
										var endDate = $("#endDate").val();
										var nowTime = $("#endDate").val();//页面显示的结束时间
										/* var taxiNum = $("#taxiNum").val(); */
										if (nowTime == "" || nowTime == null) {
											nowTime = "现在时刻";
										}
										if (startDate == "") {
											swal({
												title : "导出表格时，开始时间不能为空",
												type : "warning"
											});
										} else {
											swal({
												title : "",
												text : "您确定导出\n" + startDate
														+ " 到 " + nowTime
														+ " 该期间的命令日志吗?",
												type : "warning",
												showCancelButton : true,
												cancelButtonText : "取消"
											}, function() {
												//赋值
												$("#start1").val(startDate);
												$("#end1").val(endDate);
												//提交表单
												$("#derivedForm").submit();
											})
										}
									});

							$(".dataTables-example").dataTable();
							var oTable = $("#editable").dataTable();
							oTable
									.$("td")
									.editable(
											"../example_ajax.php",
											{
												"callback" : function(sValue, y) {
													var aPos = oTable
															.fnGetPosition(this);
													oTable.fnUpdate(sValue,
															aPos[0], aPos[1])
												},
												"submitdata" : function(value,
														settings) {
													return {
														"row_id" : this.parentNode
																.getAttribute("id"),
														"column" : oTable
																.fnGetPosition(this)[2]
													}
												},
												"width" : "90%",
												"height" : "100%"
											})
						});

		function fnClickAddRow() {
			$("#editable").dataTable()
					.fnAddData(
							[ "Custom row", "New row", "New row", "New row",
									"New row" ])
		};
	</script>
</body>

</html>