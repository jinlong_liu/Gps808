package com.hx.gps.entities.tool;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.mongodb.BasicDBObject;

public class Advertise {
	// 播放项id
	private int playId;
	// 分区横坐标
	private int partX;
	// 分区纵坐标
	private int partY;
	// 分区宽度
	private int partWidth;
	// 分区高度
	private int partHeight;
	// 播放模式
	private int playMode;
	// 播放速度
	private int playRate;
	// 播放时间或次数
	private String playType;
	// 播放次数 最大为循环播放，若设置为时间的时候不能超过32767
	private int playNumber;
	// 播放时间类型
	private String playTimeType;
	// 每页停留
	private int playStop;
	// 广告内容
	private String adsContext;
	// 广告名称
	private String adsName;
	// 广告类型
	private String adsType;
	// 客户编码
	private String adsCustomer;
	// 开始时间
	private String dateStart;
	// 结束时间
	private String dateEnd;
	// 周期
	private String weekScorll;
	// 时段相关的都暂时不记录。
	private String playFontColor;
	private String playFontSize;
	private String playFontType;
	// 记录时间
	private String addTime;
	//时段名
//	private String intervalId;

	public String getDateStart() {
		return dateStart;
	}

	public void setDateStart(String dateStart) {
		this.dateStart = dateStart;
	}

	public String getWeekScorll() {
		return weekScorll;
	}

	public void setWeekScorll(String weekScorll) {
		this.weekScorll = weekScorll;
	}

	public String getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(String dateEnd) {
		this.dateEnd = dateEnd;
	}

	public String getAddTime() {
		return addTime;
	}

	public void setAddTime(String addTime) {
		this.addTime = addTime;
	}

	public int getPlayId() {
		return playId;
	}

	public void setPlayId(String playId) {
		try {
			this.playId = Integer.parseInt(playId);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	public int getPartX() {
		return partX;
	}

	public void setPartX(String partX) {
		try {
			this.partX = Integer.parseInt(partX);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	public int getPartY() {
		return partY;
	}

	public void setPartY(String partY) {
		try {
			this.partY = Integer.parseInt(partY);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	public int getPartWidth() {
		return partWidth;
	}

	public void setPartWidth(String partWidth) {
		try {
			this.partWidth = Integer.parseInt(partWidth);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	public int getPartHeight() {
		return partHeight;
	}

	public void setPartHeight(String partHeight) {
		try {
			this.partHeight = Integer.parseInt(partHeight);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	public int getPlayMode() {
		return playMode;
	}

	public void setPlayMode(String playMode) {
		try {
			this.playMode = Integer.parseInt(playMode);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	public int getPlayRate() {
		return playRate;
	}

	public void setPlayRate(String playRate) {
		try {
			this.playRate = Integer.parseInt(playRate);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	public String getPlayType() {
		return playType;
	}

	public void setPlayType(String playType) {
		this.playType = playType;
	}

	public int getPlayNumber() {
		return playNumber;
	}

	public void setPlayNumber(String playNumber) {
		try {
			this.playNumber = Integer.parseInt(playNumber);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	public String getPlayTimeType() {
		return playTimeType;
	}

	public void setPlayTimeType(String playTimeType) {
		this.playTimeType = playTimeType;
	}

	public int getPlayStop() {
		return playStop;
	}

	public void setPlayStop(String playStop) {
		try {
			this.playStop = Integer.parseInt(playStop);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	public String getAdsContext() {
		return adsContext;
	}

	public void setAdsContext(String adsContext) {
		this.adsContext = adsContext;

		/*
		 * try { byte[] sour = adsContext.getBytes("utf-8"); String dest = new
		 * String(sour , "gb2312"); } catch (UnsupportedEncodingException e) {
		 * e.printStackTrace(); }
		 */
	}

	public String getAdsName() {
		return adsName;
	}

	public void setAdsName(String adsName) {
		this.adsName = adsName;
	}

	public String getAdsType() {
		return adsType;
	}

	public void setAdsType(String adsType) {
		this.adsType = adsType;
	}

	public String getAdsCustomer() {
		return adsCustomer;
	}

	public void setAdsCustomer(String adsCustomer) {
		this.adsCustomer = adsCustomer;
	}

	public String getPlayFontColor() {
		return playFontColor;
	}

	public void setPlayFontColor(String playFontColor) {
		this.playFontColor = playFontColor;
	}

	public String getPlayFontSize() {
		return playFontSize;
	}

	public void setPlayFontSize(String playFontSize) {
		this.playFontSize = playFontSize;
	}

	public String getPlayFontType() {
		return playFontType;
	}

	public void setPlayFontType(String playFontType) {
		this.playFontType = playFontType;
	}

	public Advertise() {

	}

	public Advertise(BasicDBObject finds) {
		super();
		// this.addTime = finds.getDate("addTime").from(instant);
		Date d = finds.getDate("addTime");
		// 给定模式
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// public final String format(Date date)
		this.addTime = sdf.format(d);
		this.playMode=finds.getInt("playMode");
		this.playRate=finds.getInt("playRate");
		this.playStop=finds.getInt("playStop");
		this.playFontColor=finds.getString("playFontColor");
		this.playFontSize=finds.getString("playFontSize");
		this.playFontType=finds.getString("playFontType");
		this.playNumber=finds.getInt("playNumber");
		this.playType=finds.getString("playType");
		this.playId = finds.getInt("playId");
		this.adsContext = finds.getString("adsContext");
		this.adsName = finds.getString("adsName");
		this.adsCustomer = finds.getString("adsCustomer");
		this.dateStart = finds.getString("startYMD");
		this.dateEnd = finds.getString("endYMD");
		this.weekScorll = finds.getString("eachweek");

	}

	public Advertise(int playId, int partX, int partY, int partWidth, int partHeight, int playMode, int playRate,
			String playType, int playNumber, String playTimeType, int playStop, String adsContext, String adsName,
			String adsType, String adsCustomer, String dateStart, String dateEnd, String weekScorll,
			String playFontColor, String playFontSize, String playFontType, String addTime) {
		super();
		this.playId = playId;
		this.partX = partX;
		this.partY = partY;
		this.partWidth = partWidth;
		this.partHeight = partHeight;
		this.playMode = playMode;
		this.playRate = playRate;
		this.playType = playType;
		this.playNumber = playNumber;
		this.playTimeType = playTimeType;
		this.playStop = playStop;
		this.adsContext = adsContext;
		this.adsName = adsName;
		this.adsType = adsType;
		this.adsCustomer = adsCustomer;
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
		this.weekScorll = weekScorll;
		this.playFontColor = playFontColor;
		this.playFontSize = playFontSize;
		this.playFontType = playFontType;
		this.addTime = addTime;
	}

	@Override
	public String toString() {
		return "Advertise [playId=" + playId + ", partX=" + partX + ", partY=" + partY + ", partWidth=" + partWidth
				+ ", partHeight=" + partHeight + ", playMode=" + playMode + ", playRate=" + playRate + ", playType="
				+ playType + ", playNumber=" + playNumber + ", playTimeType=" + playTimeType + ", playStop=" + playStop
				+ ", adsContext=" + adsContext + ", adsName=" + adsName + ", adsType=" + adsType + ", adsCustomer="
				+ adsCustomer + ", dateStart=" + dateStart + ", dateEnd=" + dateEnd + ", weekScorll=" + weekScorll
				+ ", playFontColor=" + playFontColor + ", playFontSize=" + playFontSize + ", playFontType="
				+ playFontType + ", addTime=" + addTime + "]";
	}

}
