<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<script src="js/jquery.js"></script>

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">

<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>

 
<link rel="stylesheet" href="css/myhx.css" />
<!--  添加表单验证 -->
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />
<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>
</head>

<body class="gray-bg">
	<div class="wrapper animated ">
		<div class="row">
			<div class="col-sm-12">
				<!-- 点击添加按钮弹出来的form表单 -->
				<div class="modal inmodal fade" id="myModal5" tabindex="-1"
					role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<form action="saveAdsCustomer" method="post"  class="form-horizontal" id="saveAdsCustomerForm">
								<div class="modal-body">
									<div class="ibox-title">
										<h5>新增客户信息<span style="color:red">（* 为必填项）</span></h5>
									</div>
									<div class="ibox-content">
										<div class="row">
											 
												<!-- 客户编码 放在上 -->
												 <div class="form-group">
											
												
												<label class="col-sm-3 control-label"><span style="color:red">* </span>客户编码：</label>
												<div class="col-sm-7"> <input
												id="adsCustomerId1"	name="adsCustomerId" 
													class="form-control" type="text">
											</div>
											</div>
											<div class="form-group">
											
												<!-- 客户名称 -->
												<label class="col-sm-3 control-label"><span style="color:red">* </span>客户名称：</label>
												<div class="col-sm-7">
												 <input name="adsCustomerName"  id="adsCustomerName1"
													class="form-control" type="text">
											</div>
											</div>
										</div>
									
										<div class="row">
											 <div class="form-group">
											
												<!-- 客户电话 -->
												<label class="col-sm-3 control-label"><span style="color:red">* </span>客户电话：</label>
												<div class="col-sm-7"> <input
													name="adsCustomerPhone" id="adsCustomerPhone1"
													class="form-control" type="text">
											</div>
                                           </div>
										</div>
									</div>
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-white"
										data-dismiss="modal">关闭</button>
									<button type="submit" class="btn btn-primary" >保存</button>
								</div>
							</form>
						</div>
					</div>
				</div>

				<!-- 点击"修改"按钮弹出来的form表单 -->   
				<div class="modal inmodal fade" id="myModal1" tabindex="-1"
					role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<form action="updateAdsCustomer" method="post"  class="form-horizontal" id="submitForm">
								<div class="modal-body">
									<div class="ibox-title">
										<h5>修改客户信息<span style="color:red">（* 为必填项）</span></h5>
									</div>
									<div class="ibox-content">
										<input type="hidden" name="adsCustomerId" id="id">
										<div class="row">
										
												<!-- 客户编码 -->
											<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red">* </span>客户编码：</label>
													<div class="col-sm-7"> 
												 		<input  id="adsCustomerId"
													class="form-control" type="text" disabled="disabled">
													</div>
											</div>
										
											 <div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red">* </span>客户名称：</label>
												<div class="col-sm-7">  <input
													name="adsCustomerName" id="adsCustomerName"
													class="form-control" type="text">
											</div>
											</div>
										</div>
									

										<div class="row">
										<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red">* </span>客户电话：</label>
												<div class="col-sm-7"> 
										 <input
													name="adsCustomerPhone" id="adsCustomerPhone"
													class="form-control" type="text">
											</div>
										 
										</div>
									</div>
								</div>
                            </div>
								<div class="modal-footer">
									<button type="button" class="btn btn-white"
										data-dismiss="modal">关闭</button>
									<button type="submit" class="btn btn-primary">更新</button>
								</div>
							</form>
						</div>
					</div>
				</div>

			</div>
		</div>

		<!-- 数据展示 -->
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>客户信息表</h5>
				</div>
				<div class="ibox-content"  style="height: 785px">
					<table  id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
						<thead>
							<tr><td><button id="btn_addCompany" type="button"
							class="btn btn-primary" data-toggle="modal"
							data-target="#myModal5">添加</button>
							</td></tr>
							<tr>
								<th>客户名称</th>
								<th>客户编码</th>
								<th>客户电话</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<!--遍历  -->
							<c:forEach items="${adsCustomers}" var="adsCustomer">
								<tr>
									<td>${adsCustomer.adsCustomerName} </td>
									<td>${adsCustomer.adsCustomerId}</td>
									<td>${adsCustomer.adsCustomerPhone}</td>
									<td><button id="btn-update" type="button"
											class="btn btn-success btn-sm" data-toggle="modal"
											data-target="#myModal1" onclick="update('${adsCustomer.adsCustomerName}','${adsCustomer.adsCustomerId}','${adsCustomer.adsCustomerPhone}')">修改</button>
										<button type="button" class="btn btn-danger btn-sm"
											onclick="delete1('${adsCustomer.adsCustomerName}','${adsCustomer.adsCustomerId}')">删除</button></td>
								</tr>
							</c:forEach>
						</tbody>
<!-- 						<tfoot> -->
<!-- 							<tr> -->
<!-- 								<td colspan="12"> -->
<!-- 									<ul class="pagination pull-right"></ul> -->
<!-- 								</td> -->
<!-- 							</tr> -->
<!-- 						</tfoot> -->
					</table>
				</div>
			
			</div>
		</div>
			<!-- 删除表格 -->
				<form id="deleteForm" action="deleteAdsCustomer" method="post">
					<input type="hidden" id="deleteId" name="id">
				</form>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<!-- 	<script src="js/jquery.min.js?v=2.1.4"></script> -->
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script>
		//加载footable的功能
		$(document).ready(function() {
		$('#table1').DataTable({
			"pagingType" : "full_numbers"
		});
	});

		//点击"修改"按钮
		function update(name,id, phone) {
			$("#adsCustomerName").val(name);
			$("#adsCustomerId").val(id);
			$("#id").val(id);
			$("#adsCustomerPhone").val(phone);
		};

		//点击"删除"按钮
		function delete1(name,id) {
			swal({
				title : "您确定删除 客户" + name + " 的数据吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
				$("#deleteId").val(id);
				//提交表单
				$("#deleteForm").submit();
			});
		}
		$(document).ready(function(){
			var result='<%=request.getAttribute("state")%>';
			if (result=="更新成功") {
		
					swal({
						title : result,
						type : "success"
					});
				} else if(result=="用户不存在，更新失败") {
					swal({
						title : result,
						type : "warning"
					});
				}else if(result=="添加成功！") {
					swal({
						title : result,
						type : "success"
					});
				}else if(result=="添加失败！") {
					swal({
						title : result,
						type : "warning"
					});
				}else if(result=="当前客户id已存在") {
					swal({
						title : result,
						type : "warning"
					});
				}else if(result=="删除成功！") {
					swal({
						title : result,
						type : "success"
					});
				}else if(result=="删除失败！") {
					swal({
						title : result,
						type : "warning"
					});
				}
			
		});
		
	</script>
	
	<script type="text/javascript">
$(document).ready(function formValidator() {
			$('#saveAdsCustomerForm').bootstrapValidator({
				feedbackIcons : {/*输入框不同状态，显示图片的样式*/
					valid : 'glyphicon glyphicon-ok',
					invalid : 'glyphicon glyphicon-remove',
					validating : 'glyphicon glyphicon-refresh'
				},
				/*生效规则：字段值一旦变化就触发验证*/
				live : 'enabled',
				/*当表单验证不通过时，该按钮为disabled*/
				submitButtons : 'button[type="submit"]',
				/*验证*/
				fields : {	
					adsCustomerId : {
						validators : {
							notEmpty : { 
								message : '客户编码不能为空'
							},numeric : {
								message : '请输入数字'
							}, remote: { 
							     url: 'hasCustomerId',
		                         message: '当前客户id已存在', 
		                         delay :  800, 
		                         type: 'POST' 
		                         
		                     }
						}
					},
					adsCustomerName : {
						
						validators : {
							notEmpty : {
								message : '客户名称不能为空'
							}
						}
					},
					adsCustomerPhone : {
						message : '',
						validators : {
							notEmpty : {
								message : '客户电话不能为空'
							}
						}
					}
					}
					});
					});
				$(document).ready(function formValidator() {
			$('#submitForm').bootstrapValidator({
				feedbackIcons : {/*输入框不同状态，显示图片的样式*/
					valid : 'glyphicon glyphicon-ok',
					invalid : 'glyphicon glyphicon-remove',
					validating : 'glyphicon glyphicon-refresh'
				},
				/*生效规则：字段值一旦变化就触发验证*/
				live : 'enabled',
				/*当表单验证不通过时，该按钮为disabled*/
				submitButtons : 'button[type="submit"]',
				/*验证*/
				fields : {
				adsCustomerName : {
						
						validators : {
							notEmpty : {
								message : '客户名称不能为空'
							}
						}
					},
					adsCustomerPhone : {
						message : '',
						validators : {
							notEmpty : {
								message : '客户电话不能为空'
							}
						}
					}
					}
					});
					});	
</script>
</body>

</html>