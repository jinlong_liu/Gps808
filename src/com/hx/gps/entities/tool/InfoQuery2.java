package com.hx.gps.entities.tool;

public class InfoQuery2 {
	//黑名单查询条件
	private Integer type;
	private String taxiNum;
	
	//稽查查询条件
	//车牌号
	private String checkTime;//稽查时间起
	private 	int   state; //处理类型
	
	//违章信息查询条件
	private String breakTime;//违章时间
	
	//年审管理查询条件
	//公司号
	private Integer comId;
	private String startTime;
	private String endTime;
	
	
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getTaxiNum() {
		return taxiNum;
	}
	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}
	
	public String getCheckTime() {
		return checkTime;
	}
	public void setCheckTime(String checkTime) {
		this.checkTime = checkTime;
	}
	public Integer getComId() {
		return comId;
	}
	public void setComId(Integer comId) {
		this.comId = comId;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getBreakTime() {
		return breakTime;
	}
	public void setBreakTime(String breakTime) {
		this.breakTime = breakTime;
	}
	@Override
	public String toString() {
		return "InfoQuery2 [type=" + type + ", taxiNum=" + taxiNum + ", checkTime=" + checkTime + ", state=" + state
				+ ", breakTime=" + breakTime + ", comId=" + comId + ", startTime=" + startTime + ", endTime=" + endTime
				+ "]";
	}
	 
	 
}
