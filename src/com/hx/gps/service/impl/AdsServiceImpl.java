package com.hx.gps.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hx.gps.dao.AdsDao;
import com.hx.gps.dao.TaxiDao;
import com.hx.gps.entities.PublicAdvs;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.adsCustomer;
import com.hx.gps.entities.adsTime;
import com.hx.gps.entities.tool.Advertise;
import com.hx.gps.entities.tool.WaitAdvertise;
import com.hx.gps.service.AdsService;

@Service
public class AdsServiceImpl implements AdsService {
	@Autowired
	private AdsDao adsDao;
	@Autowired
	private TaxiDao taxiDao;

	// 日志记录
	private static Log logger = LogFactory.getLog(InfoServiceImpl.class);

	@Override
	public String updateAdsTime(adsTime adsTime) {

		if (adsDao.updateAdsTime(adsTime)) {
			return "更新成功";
		} else {
			return "时段不存在，更新失败";
		}
	}

	@Override
	public String deleteAdsTime(int adsTimeId) {
		if (adsDao.deleteAdsControlByid(adsTimeId)) {
			return "删除成功！";
		} else {
			return "删除失败！";

		}
	}

	@Override
	public List<WaitAdvertise> getHistoryByTime(String startDate, String endDate) {
		List<WaitAdvertise> advertises = new ArrayList<>();
		if (startDate.equals("") && endDate.equals("")) {
			advertises = adsDao.getAllHistory();
		} else {
			advertises = adsDao.getHistoryByTime(startDate, endDate);
		}
		return advertises;
	}
@Override
public String hasCustomerId(int id) {
adsCustomer customer=new adsCustomer();
customer.setAdsCustomerId(id);
	if (adsDao.hasAdsCustomer(customer)) {
		return "当前客户id已存在";
	}else{
		return "id可用";
	}
}
	@Override
	public String deleteAdsCustomer(int adsCustomerId) {
		if (adsDao.deleteAdsCustomerByid(adsCustomerId)) {
			return "删除成功！";
		} else {
			return "删除失败！";

		}
	}

	@Override
	public String deleteAdsById(int adsId) {
		if (adsDao.deleteAdsById(adsId)) {
			return "删除成功！";
		} else {
			return "删除失败！";

		}
	}

	@Override
	public List<adsTime> getAdsTimeByName(String adsTimeName) {
		List<adsTime> adsTimes = adsDao.getAdsTimes(adsTimeName);
		return adsTimes;
	}

	@Override
	public List<adsTime> getAllAdsTimes() {
		List<adsTime> adsTimes = adsDao.getAllAdsTimes();
		logger.info("预设时段信息"+adsTimes);
		return adsTimes;
	}

	@Override
	public List<PublicAdvs> getAllPublicAds() {

		List<PublicAdvs> advs = adsDao.getAllPublicAds();
		return advs;
	}

	@Override
	public List<Advertise> getAllAdvertises() {
		List<Advertise> advertises = adsDao.getAllAdvertises();
		return advertises;
	}

	@Override
	public String updateAds(Advertise advertise) {
		if (adsDao.updateAds(advertise)) {

			return "更新成功";
		} else {
			return "更新失败";
		}
	}

	@Override
	public String updateAdsCustomer(adsCustomer adsCustomer) {
		if (adsDao.updateAdsCustomer(adsCustomer)) {

			return "更新成功";
		} else {
			return "用户不存在，更新失败";
		}
	}

	@Override
	public List<WaitAdvertise> getAllHistory() {
		List<WaitAdvertise> advertises = adsDao.getAllHistory();
		return advertises;
	}

	@Override
	public List<WaitAdvertise> getTempHistory() {
		List<WaitAdvertise> advertises = adsDao.getTempHistory();
		return advertises;
	}

	@Override
	public List<WaitAdvertise> findDetailsById(WaitAdvertise advertise) {
		List<WaitAdvertise> advertises = adsDao.findDetailsById(advertise);
		return advertises;
	}

	@Override
	public String SendAds(int[] id, List<String> taxiNum) {
		List<String> isuNums = new ArrayList<>();

		for (String str : taxiNum) {

			Taxi taxi = taxiDao.getTaxiByTaxi(str);
			String isuNum = null;
			try {
				isuNum = taxi.getIsuNum();
				//System.out.println("查询的终端号为：" + isuNum);
				logger.info("查询的终端号为：" + isuNum);
				isuNums.add(isuNum);
			} catch (NullPointerException e) {
				//System.out.println("转换值出现异常");
				logger.error("转换值出现异常");
			}
		}

		int i = 0, j = 0;
		// 逐条发送广告信息
		for (int idtemp : id) {
			//System.out.println("开始发送信息号" + id + "的广告");
			logger.info("开始发送信息号" + id + "的广告");
			if (adsDao.SendAds(isuNums, idtemp)) {
				i++;
				j = idtemp;
				continue;
			} else {
				j = idtemp;
				break;
			}

		}
		if (i == id.length) {
			return "已成功提交下发广告的请求，共选择了" + i + "条广告," + isuNums.size() + "辆车," + "具体发送情况请进入广告模块的历史记录页面查看";
		} else {
			return "在保存下发广告请求的过程中，广告id" + j + "出现了异常，保存失败";
		}
	}

	@Override
	public String updatePublicAds(PublicAdvs advs) {
		if (this.adsDao.updatePublicAds(advs)) {
			return "更新成功";
		}
		return "更新失败";
	}

	@Override
	public String SendPublicAds(int[] playIds, List<String> taxiNum) {
		// 车牌号转终端号
		List<String> isuNums = new ArrayList<>();
		for (String str : taxiNum) {

			Taxi taxi = taxiDao.getTaxiByTaxi(str);
			String isuNum = null;
			try {
				isuNum = taxi.getIsuNum();
				//System.out.println("查询的终端号为：" + isuNum);
				logger.info("查询的终端号为：" + isuNum);
				isuNums.add(isuNum);
			} catch (NullPointerException e) {
				//System.out.println("转换值出现异常");
				logger.error("转换值出现异常");
			}
		}
		int i = 0, j = 0;
		// 逐条发送广告信息
		for (int id : playIds) {
			//System.out.println("开始发送信息号" + id + "的广告");
			logger.info("开始发送信息号" + id + "的广告");
			if (adsDao.SendPublicAds(isuNums, id)) {
				i++;
				j = id;
				continue;
			} else {
				j = id;
				break;
			}

		}
		if (i == playIds.length) {
			return "已成功提交下发公益广告的请求，共选择了" + i + "条广告," + isuNums.size() + "辆车," + "具体发送情况请进入广告模块的历史记录页面查看";
		} else {
			return "在保存下发公益广告请求的过程中，广告id" + j + "出现了异常，保存失败！";
		}

	}

	@Override
	public String SendInstruct(int instruct, int play, List<String> taxiNum) {
		List<String> isuNums = new ArrayList<>();
		for (String str : taxiNum) {

			Taxi taxi = taxiDao.getTaxiByTaxi(str);
			String isuNum = null;
			try {
				isuNum = taxi.getIsuNum();
				//System.out.println("查询的终端号为：" + isuNum);
				logger.info("查询的终端号为：" + isuNum);
				isuNums.add(isuNum);
			} catch (NullPointerException e) {
				//System.out.println("转换值出现异常");
				logger.error("转换值出现异常");
			}
		}
		if (adsDao.SendInstruct(isuNums, instruct, play)) {

			return "已成功提交下发指令的请求，指令id为" + instruct + "通知车辆总数为 " + isuNums.size() + "具体发送情况请进入广告模块的历史记录页面查看";
		} else {

			return "提交下发指令的请求出现异常，指令id为" + instruct + "通知车辆总数为 " + isuNums.size() + "操作失败";

		}
	}

	// public String addAds(Advertise advertise, String[] weekDay, String
	// startYMD, String endYMD, String[] period,
	// String immeAds, String noAdsTime) {
	// if (adsDao.addAds(advertise, weekDay, startYMD, endYMD, period, immeAds,
	// noAdsTime)) {
	// return "广告信息保存成功";
	// } else {
	//
	// return "广告信息保存失败";
	// }
	//
	// }
	@Override
	public String addAds(Advertise advertise, String[] weekDay, String startYMD, String endYMD, String period,
			String immeAds, String noAdsTime) {
		if (adsDao.hasAdsId(advertise.getPlayId())) {
			return "当前信息号已存在";
		}
		if (adsDao.overflow()) {
			return "广告数量超出限制，请先删除";
		}
		if (adsDao.addAds(advertise, weekDay, startYMD, endYMD, period, immeAds, noAdsTime)) {
			return "广告信息保存成功";
		} else {

			return "广告信息保存失败";
		}

	}

	@Override
	public String savePublicAds(PublicAdvs advs) {
		if (adsDao.hasAdsId(advs.getPlayId())) {
			return "当前信息号已存在";
		}
		if (adsDao.savePublicAdvs(advs)) {
			return "公益广告添加成功";
		} else {

			return "公益广告保存失败";
		}
	}

	@Override
	public String hasPlayId(int id) {
		if (adsDao.hasAdsId(id)) {
			return "id已存在";
		} else {
			return "id可用";
		}
	}
@Override
public String hasTimeId(int id) {
	adsTime time=new adsTime();
	time.setAdsTimeId(id);
	if (adsDao.hasAdsControl(time)) {
		return "id已存在";
	} else {
		return "id可用";
	}
}
	
	@Override
	public String saveAdsControl(adsTime adsTime) {
		if (adsDao.hasAdsControl(adsTime)) {
			return "当前时段号已存在！";
		}
		try {
			adsDao.saveAdsControl(adsTime);
			return "添加成功";
		} catch (Exception exc) {
			return "添加失败";
		}

	}

	@Override
	public List<adsCustomer> getAllAdsCustomer() {
		if (adsDao.getAllAdsCustomer() != null) {
			List<adsCustomer> adsCustomers = adsDao.getAllAdsCustomer();
			//System.out.println("客户信息" + adsCustomers);
			logger.info("客户信息" + adsCustomers);
			return adsCustomers;
		} else {
			return null;
		}

	}

	@Override
	public String saveAdsCustomer(adsCustomer adsCustomer) {
		// 首先进行重复性校验
		if (adsDao.hasAdsCustomer(adsCustomer)) {
			logger.info( "当前客户id已存在");
			return "当前客户id已存在";
		}
		try {
			adsDao.saveAdsCustomer(adsCustomer);
			logger.info("[广告客户]添加成功！");
			return "添加成功！";
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[广告客户]添加异常"+e.getMessage());
			return "添加失败！";
		}

	}

}
