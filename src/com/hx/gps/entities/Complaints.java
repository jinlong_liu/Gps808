package com.hx.gps.entities;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Complaints entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "complaints" )
public class Complaints implements java.io.Serializable {

	// Fields
	/**
	 * 对于车辆、司机、类别，采用立即检索方式
	 * 而对于投诉处理，采用延迟加载策略
	 */

	private Integer id;
	private Comptype comptype;
	private Taxi taxi;
	private Driver driver;
	private Deduct deduct;
	private String backer;
	private String backPhone;
	private Timestamp startTime;
	private Timestamp endTime;
	private String startPlace;
	private String endPlace;
	private String reportReason;
	private String reportContent;
	private Integer type;
	private String userBack;
	private String apprOpinion;

	// Constructors

	/** default constructor */
	public Complaints() {
	}

	/** full constructor */
	public Complaints(Comptype comptype, Taxi taxi, Driver driver,
			Deduct deduct, String backer, String backPhone,
			Timestamp startTime, Timestamp endTime, String startPlace,
			String endPlace, String reportReason, String reportContent,
			Integer type, String userBack, String apprOpinion) {
		this.comptype = comptype;
		this.taxi = taxi;
		this.driver = driver;
		this.deduct = deduct;
		this.backer = backer;
		this.backPhone = backPhone;
		this.startTime = startTime;
		this.endTime = endTime;
		this.startPlace = startPlace;
		this.endPlace = endPlace;
		this.reportReason = reportReason;
		this.reportContent = reportContent;
		this.type = type;
		this.userBack = userBack;
		this.apprOpinion = apprOpinion;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "reportType")
	public Comptype getComptype() {
		return this.comptype;
	}

	public void setComptype(Comptype comptype) {
		this.comptype = comptype;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "reportTaxi")
	public Taxi getTaxi() {
		return this.taxi;
	}

	public void setTaxi(Taxi taxi) {
		this.taxi = taxi;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "reportDriver")
	public Driver getDriver() {
		return this.driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "deductId")
	public Deduct getDeduct() {
		return this.deduct;
	}

	public void setDeduct(Deduct deduct) {
		this.deduct = deduct;
	}

	@Column(name = "backer", length = 20)
	public String getBacker() {
		return this.backer;
	}

	public void setBacker(String backer) {
		this.backer = backer;
	}

	@Column(name = "backPhone", length = 30)
	public String getBackPhone() {
		return this.backPhone;
	}

	public void setBackPhone(String backPhone) {
		this.backPhone = backPhone;
	}

	@Column(name = "startTime", length = 19)
	public Timestamp getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	@Column(name = "endTime", length = 19)
	public Timestamp getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	@Column(name = "startPlace", length = 50)
	public String getStartPlace() {
		return this.startPlace;
	}

	public void setStartPlace(String startPlace) {
		this.startPlace = startPlace;
	}

	@Column(name = "endPlace", length = 50)
	public String getEndPlace() {
		return this.endPlace;
	}

	public void setEndPlace(String endPlace) {
		this.endPlace = endPlace;
	}

	@Column(name = "reportReason", length = 50)
	public String getReportReason() {
		return this.reportReason;
	}

	public void setReportReason(String reportReason) {
		this.reportReason = reportReason;
	}

	@Column(name = "reportContent", length = 65535)
	public String getReportContent() {
		return this.reportContent;
	}

	public void setReportContent(String reportContent) {
		this.reportContent = reportContent;
	}

	@Column(name = "type")
	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "userBack", length = 50)
	public String getUserBack() {
		return this.userBack;
	}

	public void setUserBack(String userBack) {
		this.userBack = userBack;
	}

	@Column(name = "apprOpinion", length = 100)
	public String getApprOpinion() {
		return this.apprOpinion;
	}

	public void setApprOpinion(String apprOpinion) {
		this.apprOpinion = apprOpinion;
	}

	@Override
	public String toString() {
		return "Complaints [id=" + id + ", comptype=" + comptype + ", taxi="
				+ taxi + ", driver=" + driver + ", deduct=" + deduct
				+ ", backer=" + backer + ", backPhone=" + backPhone
				+ ", startTime=" + startTime + ", endTime=" + endTime
				+ ", startPlace=" + startPlace + ", endPlace=" + endPlace
				+ ", reportReason=" + reportReason + ", reportContent="
				+ reportContent + ", type=" + type + ", userBack=" + userBack
				+ ", apprOpinion=" + apprOpinion + "]";
	}

	
	

}