<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>H+ 后台主题UI框架 - 基本表单</title>
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link rel="shortcut icon" href="favicon.ico">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- <base target="_blank"> -->

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>终端远程控制</h5>
				</div>
				<div class="ibox-content"
					style="height: 770px; padding: 15px 35px 20px">
					<form class="form-horizontal" action="sendControl" method="post"
						id="getOneForm">
						<div class="form-group">
							<label class="col-sm-3">选中的车牌号：</label>
							<div class="col-sm-8">
								<input class="form-control" type="text" name="taxiNums"
									id="taxiNums" value="${taxiNum}">

							</div>
							<br>
						</div>
						<div class="row">
							<label class="col-sm-3 label-control">强开/强关:</label>
							<div class="col-sm-3">
								<input type="radio" name="deviceControl" id="deviceControl"
									value="0">强制开机
							</div>
							<div class="col-sm-2">
								<input type="radio" name="deviceControl" id="deviceControl"
									value="1">强制关机

							</div>
							<div class="col-sm-2">
								<input type="radio" name="deviceControl" id="deviceControl"
									value="2" checked="checked">正常

							</div>
							<div class="col-sm-2">
								<input type="radio" name="deviceControl" id="deviceControl"
									value="3" checked="checked">不操作

							</div>
							<br> <br>
						</div>
						<div class="row">
							<label class="col-sm-3 label-control ">人脸比对:</label>
							<div class="col-sm-3">
								<input type="radio" name="faceControl" id="faceControl"
									value="0">关闭

							</div>
							<div class="col-sm-3">
								<input type="radio" name="faceControl" id="faceControl"
									value="1">开启

							</div>
							<div class="col-sm-3">
								<input type="radio" name="faceControl" id="faceControl"
									value="2" checked="checked">不操作

							</div>
							<br> <br> <br> <br> <br> <br> <br>
							<br> <br>
						</div>

					</form>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-4">
							<button class="btn btn-primary" type="button" id="getOneBtn">发&nbsp;&nbsp;&nbsp;送</button>
							&nbsp;&nbsp; 
							
							<!-- <a href="getTreeOfTemp"><button type="button"
									class="btn  btn-white">返&nbsp;&nbsp;&nbsp;回</button></a> -->

						</div>
					</div>
					<br>
				</div>
			</div>
		</div>
	</div>

	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/content.min.js?v=1.0.0"></script>
	<script type="text/javascript">
		//点击"发送"按钮
		$("#getOneBtn").click(function() {
			var taxiNums = $("#taxiNums").val();
			if (taxiNums == "") {
				swal({
					title : "没有选中任何车辆,请点击'返回'按钮返回",
					type : "warning"
				});
			} else {
				$("#getOneForm").submit();
			}
		});
	</script>
</body>

</html>