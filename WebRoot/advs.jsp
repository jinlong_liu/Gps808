<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/bootstrap.css" />
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<script type="text/javascript" src="js/jquery.min.js"></script>
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/i18n/defaults-*.min.js"></script>
<script src="js/plugins/sweetalert/sweetalert.min.js"></script>


<!--  添加表单验证 -->
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />
<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>
<style type="text/css">
input.error { border: 1px solid red; }
label.error {
    background:url("./demo/images/unchecked.gif") no-repeat 0px 0px;
    padding-left: 16px;
    padding-bottom: 2px;
    font-weight: bold;
    color: #EA5200;
}
label.checked {
    background:url("./demo/images/checked.gif") no-repeat 0px 0px;
}
</style>
</head>

<body bgcolor="gray">
	<div class="container">
		<div class="row">
			<form action="addAds" class="form-horizontal" method="post"
				id="submitForm">
				<div class="form-group">
					<!-- <label class="col-sm-3 control-label">横坐标：</label> -->
					<div class="col-sm-5">
						<!-- 	<input name="partX" id="partX" class="form-control"
							placeholder="范围:(-32768→32767)" type="text"> 
 -->
						<input type="hidden" id="partX" name="partX" value="0">
					</div>
				</div>
				<div class="form-group">
					<!-- <label class="col-sm-3 control-label">纵坐标：</label> -->
					<div class="col-sm-5">
						<!-- 	<input name="partY" id="partY" class="form-control"
							placeholder="范围:(-32768→32767)" type="text">  -->
						<input type="hidden" id="partY" name="partY" value="0">
					</div>
				</div>
				<div class="form-group">
					<!-- <label class="col-sm-3 control-label">宽度：</label> -->
					<div class="col-sm-5">
						<!-- 	<input name="partWidth" id="partWidth" class="form-control"
							placeholder="范围:(0-65535)" type="text">  -->
						<input type="hidden" id="partWidth" name="partWidth" value="0">
					</div>
				</div>
				<div class="form-group">
					<!-- <label class="col-sm-3 control-label">高度：</label> -->
					<div class="col-sm-5">
						<!-- 	<input name="partHeight" id="partHeight" class="form-control"
							placeholder="范围：(0-65535)" type="text">  -->
						<input type="hidden" id="partHeight" name="partHeight" value="0">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"><span style="color:red">* </span>播放模式：</label>
					<div class="col-sm-5">
						<select class="form-control" name="playMode" id="playMode">
							<option value="0" selected="selected">连续左移</option>
							<option value="1">立即显示</option>
							<option value="2">左移</option>
							<option value="3">右移</option>
							<option value="4">上移</option>
							<option value="5">下移</option>
							<!-- <option value="6">从左向右展开</option>
                <option value="7">从上到下展开</option>
                 <option value="8">从右向左展开</option>
                <option value="9">飘雪</option>
                <option value="10">冒泡</option>
                <option value="11">分散左拉</option>
                 <option value="12">陨落</option>
                <option value="13">向上镭射</option>
                <option value="14">向下镭射</option>
                <option value="15">向右镭射</option>
                 <option value="16">画卷合</option>
                <option value="17">画卷开</option>
                <option value="18">卷轴左</option>
                <option value="19">卷轴右</option>
                <option value="20">水瓶穿插</option>
                <option value="21">上下交错</option>
                <option value="22">左右合</option>
                <option value="23">左右开</option>
                <option value="24">上下合</option>
                <option value="25">上下开</option>
                <option value="26">随机</option>
                <option value="27">阿拉伯左移</option>
                <option value="28">水平百叶窗</option>
                <option value="29">垂直百叶窗</option> -->
							<option value="30">亮灭闪烁</option>
							<option value="31">亮白闪烁</option>
							<!-- <option value="32">腾空翻转</option>
                <option value="33">向下拉伸</option>
                <option value="34">向上拉伸</option>
                <option value="35">圆形收缩</option>
                <option value="36">圆形扩散</option>
                 <option value="37">左翻页</option>
                <option value="38">右翻页</option>
                <option value="39">扇形展开</option>
                <option value="40">扇形展合</option>
                <option value="41">斜向左展</option>
                <option value="42">斜向右展</option>
                <option value="43">顺时针覆盖</option>
                <option value="44">逆时针覆盖</option>
            	<option value="45">马赛克</option> -->
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"><span style="color:red">* </span>播放速度:</label>
					<div class="col-sm-5">
						<input name="playRate" id="playRate" class="form-control"
							type="text" placeholder="范围：(0-15)">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"><span style="color:red">* </span>停留：</label>
					<div class="col-sm-5">
						<input name="playStop" id="playStop" class="form-control"
							type="text" placeholder="范围：(0-60)">

					</div>
				</div>




				<div class="form-group">
					<label class="col-sm-3 control-label">文本输入提示：</label>
					<div class="col-sm-8">

						还可以输入<span style="font-family: Georgia; font-size: 20px;"
							id="wordCheck">100</span>个汉字
					</div>
					<label class="col-sm-3 control-label"><span style="color:red">* </span>广告内容：</label>
					<div class="col-sm-5">

						<textarea style="height: 250px;resize: none;" name="adsContext" id="adsContext"
							class="form-control" type="text" placeholder="请输入广告内容"
							required="" aria-required="true"
							onKeyUp="javascript:checkWord(this);"
							onMouseDown="javascript:checkWord(this);"
							style="overflow-y:scroll "></textarea>
					</div>
					<!-- <div>
								<textarea onKeyUp="javascript:checkWord(this);"
									onMouseDown="javascript:checkWord(this);" name="content"
									style="overflow-y:scroll"></textarea>
							</div> -->


				</div>


				<!-- 				<div class="form-group"> -->
				<!-- 					<label class="col-sm-3 control-label">广告内容：</label> -->
				<!-- 					<div class="col-sm-5"> -->
				<!-- 						<input name="adsContext" id="adsContext" class="form-control" -->
				<!-- 							type="text" placeholder="请输入广告内容"> -->


				<!-- 					</div> -->
				<!-- 				</div> -->
				<div class="form-group">
					<label class="col-sm-3 control-label"><span style="color:red">* </span>广告名称：</label>
					<div class="col-sm-5">
						<input name="adsName" id="adsName" class="form-control"
							placeholder="请输入广告名称" type="text">

					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"><span style="color:red">* </span>广告类型：</label>
					<div class="col-sm-5">
						<select class="form-control" name="adsType" id="adsType"
							placeholder="请选择广告类型">
							<option value="0" selected="selected">普通</option>
							<option value="1">紧急</option>
							<option value="2">报警</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"><span style="color:red">* </span>客户：</label>
					<div class="col-sm-5">
						<select class="form-control" name="adsCustomer" id="adsCustomer">
							<!-- <option value="0">客户一</option>
							<option value="1">客户二</option>
							<option value="2">客户三</option>
							<option value="3">客户四</option>-->
							<c:forEach items="${adsCustomers}" var="adsCustomers">
								<option value="${adsCustomers.adsCustomerName}">
									${adsCustomers.adsCustomerName}</option>
							</c:forEach>

						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"><span style="color:red">* </span>开始时间：</label>
					<div class="col-sm-5">
						<input name="startYMD" class="form-control" type="text"
							id="startYMD" value="${sensorStartDate}" class="Wdate"
							onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true,onpicked:function(dp){refreshValidator(submitForm, name.valueOf())}})">

					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"><span style="color:red">* </span>结束时间：</label>
					<div class="col-sm-5">
						<input name="endYMD" class="form-control" type="text" id="endYMD"
							value="${sensorEndDate}" class="Wdate" value=""
							onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'startYMD\')}',onpicked:function(dp){refreshValidator(submitForm, name.valueOf())},readOnly:true})">

					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"><span style="color:red">* </span>设置周期 :</label>
					<div class="col-sm-5">
						<select name="weekDay" id="weekDay"
							class="selectpicker show-tick form-control" multiple
							data-live-search="false">
							<option value="0" selected="selected">星期一</option>
							<option value="1" selected="selected">星期二</option>
							<option value="2" selected="selected">星期三</option>
							<option value="3" selected="selected">星期四</option>
							<option value="4" selected="selected">星期五</option>
							<option value="5" selected="selected">星期六</option>
							<option value="6" selected="selected">星期日</option>
						</select>
						<!-- <input type="button" value="确定" onclick="fun()" /> -->
						<script type="text/javascript">
					$(window).on('load', function () {
					    $('#weekDay').selectpicker({
					        'selectedText': 'cat'
					    });
					});
					function fun(){
						var str=$("#weekDay").val();
						
					}
</script>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"><span style="color:red">* </span>播出时间段：</label>
					<div class="col-sm-5">
						<!--可多选 
						<select name="period" id="period"
							class="selectpicker show-tick form-control" multiple
							data-live-search="false"> -->
						<select name="period" id="period" class=" form-control">
							<option value="">请选择时段</option>
							<c:forEach items="${adsTime}" var="ads">
								<option value="${ads.adsTimeId}">${ads.adsTimeName}</option>
							</c:forEach>
							<!--不再用预设时间段，直接从数据库获取 
							<option value="0" selected="selected">0:00-1:00</option>
							<option value="1">1:00-2:00</option>
							<option value="2">2:00-3:00</option>
							<option value="3">3:00-4:00</option>
							<option value="4">4:00-5:00</option>
							<option value="5">5:00-6:00</option>
							<option value="6">6:00-7:00</option>
							<option value="7">7:00-8:00</option>
							<option value="8">8:00-9:00</option>
							<option value="9">9:00-10:00</option>
							<option value="10">10:00-11:00</option>
							<option value="11">11:00-12:00</option> -->

						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"><span style="color:red">* </span>信息号：</label>
					<div class="col-sm-5">
						<input name="playId" id="playId" class="form-control" type="text"
							placeholder="请输入数字(0-24)">
 
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"><span style="color:red">* </span>设置字体：</label>
					<div class="col-sm-5">
						<select class="form-control" name="playFontType" id="playFontType">
							<option value="0" selected="selected">宋体</option>
							<!--   <option>选项 2</option>
                <option>选项 3</option>
                <option>选项 4</option> -->
						</select>
					</div>
					 
					</div> 
					<div class="form-group">
						<label class="col-sm-3 control-label"><span style="color:red">* </span>字体大小：</label>
						<div class="col-sm-5">
							<select class="form-control" name="playFontSize"
								id="playFontSize">
								<option value="0">自适应</option>
								<!--  <option>选项 2</option>
                <option>选项 3</option>
                <option>选项 4</option> -->
							</select>
							
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label"><span style="color:red">* </span>字体颜色：</label>
						<div class="col-sm-5">
							<select class="form-control" name="playFontColor"
								id="playFontColor">
								<option value="7">单基色</option>
								<!--    <option>选项 2</option>
                <option>选项 3</option>
                <option>选项 4</option> -->
							</select>
						</div>
					</div>
			 

				<div class="form-group">
					<label class="col-sm-3 control-label"><span style="color:red">* </span>循环类型：</label>
					<div class="col-sm-5">

						<input value="0" id="playType0" name="playType" type="radio">次数

						<input value="1" id="playType1" name="playType" type="radio">时间
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"><span style="color:red">*  </span>设置时间或次数：</label>
					<div class="col-sm-5">

						<input name="playNumber" id="playNumber" class="form-control"
							type="text">

						<div class="col-sm-3" id="playTT">
							<input value="0" id="playTimeType0" name="playTimeType"
								type="radio" checked="checked">秒 <input value="1"
								id="playTimeType1" name="playTimeType" type="radio">毫秒
						</div>
						<script>
       
        	  $(function () {
        		  
                  $("input:radio[name='playType']").click(function () {
                      if ($(this).val()==1) {
                         // alert("选中了");
                          $("#playTT").show();
                      }else{
                    	  $("#playTT").hide();
                      }
               
                  });
              });
 
 
            </script>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">是否为即时广告：</label>
					<div class="col-sm-5">
						<!-- <label class="checkbox-inline"> <input value="1"
							name="immeAds" id="immeAds" type="checkbox">
						</label> -->
						<input value="1" name="immeAds" type="radio" id="immeAds">是
						<input value="0" id="immeAds" name="immeAds" type="radio">否

					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">设置无广告段播出：</label>
					<div class="col-sm-5">
						<!-- <label class="checkbox-inline"> <input value="1"
							name="noAdsTime" id="noAdsTime" type="checkbox">
						</label> -->
						<input value="1" name="noAdsTime" type="radio" id="noAdsTime">是
						<input value="0" id="noAdsTime" name="noAdsTime" type="radio">否
					</div>
				</div>
				<br>

				<div class="form-group">
					<div class="col-lg-9 col-lg-offset-3">
						<button type="reset" class="btn btn-green">重置</button>
						<button type="submit" class="btn btn-primary" onclick="hasId()">提交</button>

					</div>
				</div>

			</form>
		</div>
	</div>



	<!-- 	<script src="js/jquery.min.js?v=2.1.4"></script> -->
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
	<script>
	
 
	
$("#playTT").hide();

function submit1(){
	var partx=$("#partX").val();
	alert(partx);
	var party=$("#partY").val();
	alert(party);
	var partwidth=$("#partWidth").val();
	alert(partwidth);
	var partheight=$("#partHeight").val();
	alert(partheight);
	
	var playid=$("#playId").val();
	alert(playid);
	var playmode=$("#playMode").val();
	alert(playmode);
	var playrate=$("#playRate").val();
	alert(playrate);
	var playStop=$("#playStop").val();
	alert(playStop);
	var fontsize=$("#playFontSize").val();
	alert(fontsize);
	var fonttype=$("#playFontType").val();
	alert(fonttype);
	var fontcolor=$("#playFontColor").val();
	alert(fontcolor);
	var playType=$("input[name='playType']").val();
	alert(playType);
	var playTimeType=$("input[name='playTimeType']").val();
	alert(playTimeType);
	var playnumber=$("#playNumber").val();
	alert(playnumber);
	
	var adscontext=$("#adsContext").val();
	alert(adscontext);
	var adsname=$("#adsName").val();
	alert(adsname);
	var adstype=$("#adsType").val();
	alert(adstype);
	var adscustomer=$("#adsCustomer").val();
	alert(adscustomer);
	var startymd=$("#startYMD").val();
	alert(startymd);
	var endymd=$("#endYMD").val();
	alert(endymd);
	var weekday=$("#weekDay").val();
	alert(weekday);
	var peroid=$("#period").val();
	alert(peroid);
	
	var immeads=$("#immeAds").val();
	alert(immeads);
	var notimeads=$("#noTimeAds").val();
	alert(notimeads);
	
	
	
}



$(document).ready(function(){
	var result='<%=request.getAttribute("result")%>';
			if (result != null && result != "null") {
				if (result.indexOf("成功") > 0) {
					swal({
						title : result,
						type : "success"
					});
				} else {
					swal({
						title : result,
						type : "warning"
					});
				}
			}

		});
	</script>
	<script>
		function setA(f) {
			alert("触发");
			var a = '';
			for (var i = 0; i < f.immeAds.length; i++)
				a += ',' + (f.a1[0].checked ? '1' : '0');
			f.immeAds.value = a;
		}

		function sub() {
			alert("哈哈");
			if ($("input[name='immeAds']").is(':checked')) {
				alert("已经选中");
				$("input[name='immeAds']").Attr("value", "3");
			} else {
				alert("未选中");
				$("input[name='immeAds']").Attr("value", "3");

			}
		}
	</script>
	<script type="text/javascript">
		var maxstrlen = 100;

		function Q(s) {
			return document.getElementById(s);
		}

		function checkWord(c) {

			len = maxstrlen;
			var str = c.value;
			myLen = getStrleng(str);
			var wck = Q("wordCheck");
			if (myLen > len * 2) {
				c.value = str.substring(0, i - 1);
			} else {
				wck.innerHTML = Math.floor((len * 2 - myLen) / 2);
			}

		}

		function getStrleng(str) {

			myLen = 0;
			i = 0;
			for (; (i < str.length) && (myLen <= maxstrlen * 2); i++) {
				if (str.charCodeAt(i) > 0 && str.charCodeAt(i) < 128)
					myLen++;
				else
					myLen += 2;
			}
			return myLen;
		}

		$(document).ready(function formValidator() {
			$('#submitForm').bootstrapValidator({
				feedbackIcons : {/*输入框不同状态，显示图片的样式*/
					valid : 'glyphicon glyphicon-ok',
					invalid : 'glyphicon glyphicon-remove',
					validating : 'glyphicon glyphicon-refresh'
				},
				/*生效规则：字段值一旦变化就触发验证*/
				live : 'enabled',
				/*当表单验证不通过时，该按钮为disabled*/
				submitButtons : 'button[type="submit"]',
				/*验证*/
				fields : {
					playMode : {
						message : '',
						validators : {
							notEmpty : {/*非空提示*/
								message : '播放模式不能为空'
							}
						}
					},
					playRate : {
						message : '',
						validators : {
							notEmpty : {
								message : '播放速度不能为空'
							},
							between : {
								min : 0,
								max : 15,
								message : '请输入0-15之间的数'
							}
						}
					},
					playStop : {
						validators : {
							notEmpty : {
								message : '停留不能为空'
							},
							between : {
								min : 0,
								max : 60,
								message : '请输入0-60之间的数'
							}
						}
					},
					adsContext : {
						validators : {
							notEmpty : {
								message : '广告内容不能为空'
							}
						}
					},
					adsName : {
						validators : {
							notEmpty : {
								message : '广告名称不能为空'
							}
						}
					},
					adsType : {
						validators : {
							notEmpty : {
								message : '广告类型不能为空'
							}
						}
					},
					adsCustomer : {
						validators : {
							notEmpty : {
								message : '客户不能为空'
							}
						}
					},
					startYMD : {
						validators : {
							notEmpty : {
								message : '开始时间不能为空'
							}
						}
					},
					endYMD : {
						validators : {
							notEmpty : {
								message : '结束时间不能为空'
							}
						}
					},
					weekDay : {
						validators : {
							notEmpty : {
								message : '设置周期不能为空'
							}
						}
					},
					period : {
						validators : {
							notEmpty : {
								message : '播出时间段不能为空'
							}
						}
					},
					playId : {
						validators : {
							notEmpty : {
								message : '信息号不能为空'
							},
							between : {
								min : 0,
								max : 24,
								message : '请输入0-24之间的数'
							},
							remote : {
								url : 'hasPlayId',//验证地址
								message : '信息号已存在',
								delay : 800,
								type : 'POST'

							},
						}
					},
					playFontType : {
						validators : {
							notEmpty : {
								message : '设置字体不能为空'
							}
						}
					},
					playFontSize : {
						validators : {
							notEmpty : {
								message : '字体大小不能为空'
							}
						}
					},
					playFontColor : {
						validators : {
							notEmpty : {
								message : '字体颜色不能为空'
							}
						}
					},
					playNumber : {
						validators : {
							notEmpty : {
								message : '循环时间或次数不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					}
				}

			});
		});
		/*日历刷新验证*/
		function refreshValidator(id, name) {
			$(id).data('bootstrapValidator').updateStatus(name,
					'NOT_VALIDATED', null).validateField(name);
		}
		/* function hasId() {
			var id = $("#playId").val();
			var url = "hasPlayId";
			$.post(url, {
				"id" : id
			}, function(data) {
				 alert(date);
			});
		}; */
		//修改错误位置的提示信息的位置
 	/*  	$("#submitForm").validate({ 
			errorPlacement : function(error, element) { 
		    if (element.is(":select")){ 
			error.appendTo(element.parent().parent()); }
			else{  
			error.appendTo(element.parent().next()); 
			} 
			 
			}   
 	 	}); */
	</script>
</body>
</html>