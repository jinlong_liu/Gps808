package com.hx.gps.entities.tool;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TaxiInfo2 {
	@JsonIgnore
	private int taxiId;
	@JsonProperty("id")
	private String taxiNum;
	//拼接显示字符串用的
	@JsonProperty("text")
	private String text;
	//应该显示小车图标
	@JsonProperty("icon")
	private String icon = "fa fa-car";
	@JsonIgnore
	private Set<ReveInfo> revenues = new HashSet<ReveInfo>(0);

	// gy 添加车辆在线状态和定位状态、营运状态
	// 在线状态
	private String onlineState;

	// 定位状态
	private String positioningState;
	
	//营运状态
	private String operateState;

	public String getOperateState() {
		return operateState;
	}

	public void setOperateState(String operateState) {
		this.operateState = operateState;
	}

	public int getTaxiId() {
		return taxiId;
	}

	public void setTaxiId(int taxiId) {
		this.taxiId = taxiId;
	}

	public String getTaxiNum() {
		return taxiNum;
	}

	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}

	public Set<ReveInfo> getRevenues() {
		return revenues;
	}

	public void setRevenues(Set<ReveInfo> revenues) {
		this.revenues = revenues;
	}

	public TaxiInfo2() {
		super();
	}

	public String getOnlineState() {
		return onlineState;
	}

	public void setOnlineState(String onlineState) {
		this.onlineState = onlineState;
	}

	public String getPositioningState() {
		return positioningState;
	}

	public void setPositioningState(String positioningState) {
		this.positioningState = positioningState;
	}

	public TaxiInfo2(int taxiId, String taxiNum) {
		super();
		this.taxiId = taxiId;
		this.taxiNum = taxiNum;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TaxiInfo2 [taxiId=");
		builder.append(taxiId);
		builder.append(", taxiNum=");
		builder.append(taxiNum);
		builder.append(", revenues=");
		builder.append(revenues);
		builder.append(", onlineState=");
		builder.append(onlineState);
		builder.append(", positioningState=");
		builder.append(positioningState);
		builder.append(", operateState=");
		builder.append(operateState);
		builder.append("]");
		return builder.toString();
	}

	public String getText() {
		StringBuilder builder = new StringBuilder(taxiNum);
		if (onlineState.equals("在线")) {
			builder.append(" [<span class=\"green\">在线</span>]");
	/*		if (positioningState.equals("定位")) {
				builder.append(" [<span class=\"green\">定位</span>]");
			}else{
				builder.append(" [<span class=\"red\">未定位</span>]");
			}*/
		}else {
			builder.append(" [<span class=\"red\">离线</span>]");
		/*	if (positioningState.equals("定位")) {
				builder.append(" [<span class=\"green\">定位</span>]");
			}else{
				builder.append(" [<span class=\"red\">----</span>]");
			}*/
		}
		return builder.toString();
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

}
