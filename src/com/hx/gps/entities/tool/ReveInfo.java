package com.hx.gps.entities.tool;

public class ReveInfo {
	private String company;		//公司名称
	private String taxiNum;		//车牌号
	private String date;		//对应日期，年月日
	private int number;			//营收次数
	private float emptyMile;	//空驶里程
	private float mileUtil;		//载客里程
	private float money;		//租金
	private int roommate;		//合租次数
	
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getTaxiNum() {
		return taxiNum;
	}
	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public float getEmptyMile() {
		return emptyMile;
	}
	public void setEmptyMile(float emptyMile) {
		this.emptyMile = emptyMile;
	}
	public float getMileUtil() {
		return mileUtil;
	}
	public void setMileUtil(float mileUtil) {
		this.mileUtil = mileUtil;
	}
	public float getMoney() {
		return money;
	}
	public void setMoney(float money) {
		this.money = money;
	}
	public int getRoommate() {
		return roommate;
	}
	public void setRoommate(int roommate) {
		this.roommate = roommate;
	}
	@Override
	public String toString() {
		return "ReveInfo [company=" + company + ", taxiNum=" + taxiNum
				+ ", date=" + date + ", number=" + number + ", emptyMile="
				+ emptyMile + ", mileUtil=" + mileUtil + ", money=" + money
				+ ", roommate=" + roommate + "]";
	}
	
	
}
