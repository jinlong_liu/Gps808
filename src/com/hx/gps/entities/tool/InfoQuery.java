package com.hx.gps.entities.tool;

import java.sql.Timestamp;

/**
 * 信息管理，所有的查询条件都放在这
 *
 */
public class InfoQuery {
	//
	private String startDate; 
	private String endDate;
	private Integer alarmType;
	
	// "车辆管理页面"查询字段
	private Integer comId;// 公司id
	private String taxiNum;// 车牌号
	private String taxiType;
	private String taxiColor;
	private String companyName;
	private String masterName;// 车主姓名
	private String isuNum;// 终端号
	private String isuPhone;// sim卡号，isu手机号

	// "从业人员页面"查询字段
	private Integer taxiId;// 车牌号id
	private String quaNum;// 资格证号
	private String dname;// 姓名

	// 从业人员页面 查询字段
	private String category;// 保险类别 ：驾驶员or车辆
	private String master;// 给什么交的保险：驾驶员名字or车牌号
	private String payTimeStartString;// 缴费日期起 字符串类型
	private String payTimeEndString;// 缴费日期止 字符串类型
	private Timestamp payTimeStart;// 缴费日期起
	private Timestamp payTimeEnd;// 缴费日期止
	private String type;// 保险类型
	// 服务端分页字段
	private int limit;
	private int offset;
	private String search;
	//审核从业人员
	private Integer id;
	private String state;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Integer getComId() {
		return comId;
	}

	public void setComId(Integer comId) {
		this.comId = comId;
	}

	public String getTaxiNum() {
		return taxiNum;
	}

	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}

	public String getMasterName() {
		return masterName;
	}

	public void setMasterName(String masterName) {
		this.masterName = masterName;
	}

	public String getIsuNum() {
		return isuNum;
	}

	public void setIsuNum(String isuNum) {
		this.isuNum = isuNum;
	}

	public String getIsuPhone() {
		return isuPhone;
	}

	public void setIsuPhone(String isuPhone) {
		this.isuPhone = isuPhone;
	}

	public String getQuaNum() {
		return quaNum;
	}

	public void setQuaNum(String quaNum) {
		this.quaNum = quaNum;
	}

	public String getDname() {
		return dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	public String getTaxiType() {
		return taxiType;
	}

	public void setTaxiType(String taxiType) {
		this.taxiType = taxiType;
	}

	public String getTaxiColor() {
		return taxiColor;
	}

	public void setTaxiColor(String taxiColor) {
		this.taxiColor = taxiColor;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Integer getTaxiId() {
		return taxiId;
	}

	public void setTaxiId(Integer taxiId) {
		this.taxiId = taxiId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getMaster() {
		return master;
	}

	public void setMaster(String master) {
		this.master = master;
	}

	public Timestamp getPayTimeStart() {
		return payTimeStart;
	}

	public void setPayTimeStart(Timestamp payTimeStart) {
		this.payTimeStart = payTimeStart;
	}

	public Timestamp getPayTimeEnd() {
		return payTimeEnd;
	}

	public void setPayTimeEnd(Timestamp payTimeEnd) {
		this.payTimeEnd = payTimeEnd;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPayTimeStartString() {
		return payTimeStartString;
	}

	public void setPayTimeStartString(String payTimeStartString) {
		this.payTimeStartString = payTimeStartString;
	}

	public String getPayTimeEndString() {
		return payTimeEndString;
	}

	public void setPayTimeEndString(String payTimeEndString) {
		this.payTimeEndString = payTimeEndString;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InfoQuery [comId=");
		builder.append(comId);
		builder.append(", taxiNum=");
		builder.append(taxiNum);
		builder.append(", masterName=");
		builder.append(masterName);
		builder.append(", isuNum=");
		builder.append(isuNum);
		builder.append(", isuPhone=");
		builder.append(isuPhone);
		builder.append(", taxiId=");
		builder.append(taxiId);
		builder.append(", quaNum=");
		builder.append(quaNum);
		builder.append(", dname=");
		builder.append(dname);
		builder.append(", category=");
		builder.append(category);
		builder.append(", master=");
		builder.append(master);
		builder.append(", payTimeStartString=");
		builder.append(payTimeStartString);
		builder.append(", payTimeEndString=");
		builder.append(payTimeEndString);
		builder.append(", payTimeStart=");
		builder.append(payTimeStart);
		builder.append(", payTimeEnd=");
		builder.append(payTimeEnd);
		builder.append(", type=");
		builder.append(type);
		builder.append(", limit=");
		builder.append(limit);
		builder.append(", id=");
		builder.append(id);
		builder.append(", state=");
		builder.append(state);
		builder.append(", offset=");
		builder.append(offset);
		builder.append(", search=");
		builder.append(search);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the alarmType
	 */
	public Integer getAlarmType() {
		return alarmType;
	}

	/**
	 * @param alarmType the alarmType to set
	 */
	public void setAlarmType(Integer alarmType) {
		this.alarmType = alarmType;
	}

	/**
	 * @return the search
	 */
	public String getSearch() {
		return search;
	}

	/**
	 * @param search the search to set
	 */
	public void setSearch(String search) {
		this.search = search;
	}

}
