package com.hx.gps.entities.tool;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ComInfo2 {
	@JsonProperty("id")
	private int comId;
	@JsonProperty("text")
	private String text;
	@JsonIgnore
	private String cname;
	@JsonProperty("children")
	private Set<TaxiInfo2> taxis = new HashSet<TaxiInfo2>(0);
	
	//gy 添加公司的车辆数
	private int count;
	// 添加公司在线车辆数显示的功能
	private int onlineCount;
	
	public int getOnlineCount() {
		return onlineCount;
	}
	public void setOnlineCount(int onlineCount) {
		this.onlineCount = onlineCount;
	}
	public int getCount() {
		return count;
	}
	public void setCount() {
		this.count = taxis.size();
	}
	public int getComId() {
		return comId;
	}
	public void setComId(int comId) {
		this.comId = comId;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public Set<TaxiInfo2> getTaxis() {
		return taxis;
	}
	public void setTaxis(Set<TaxiInfo2> taxis) {
		this.taxis = taxis;
	}
	public ComInfo2() {
		super();
	}
	public ComInfo2(int comId, String cname, Set<TaxiInfo2> taxis) {
		super();
		this.comId = comId;
		this.cname = cname;
		this.taxis = taxis;
		setCount();
	}
	public ComInfo2(int comId, String cname, Set<TaxiInfo2> taxis,int onlineCount) {
		super();
		this.comId = comId;
		this.cname = cname;
		this.taxis = taxis;
		setCount();
		this.onlineCount=onlineCount;
	}
	@Override
	public String toString() {
		return "ComInfo2 [comId=" + comId + ", cname=" + cname + ", taxis="
				+ taxis + "]";
	}
	public String getText() {
		return cname+" ["+onlineCount+"/"+taxis.size()+"]";
	}
	public void setText(String text) {
		this.text = text;
	}
}
