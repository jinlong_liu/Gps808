package com.hx.gps.entities.tool;


import java.sql.Timestamp;

public class TimeTest {
	private String name;
	private Timestamp t1;
	private Timestamp t2;
	private Timestamp t3;
	
	public Timestamp getT1() {
		return t1;
	}
	public void setT1(Timestamp t1) {
		this.t1 = t1;
	}
	public Timestamp getT2() {
		return t2;
	}
	public void setT2(Timestamp t2) {
		this.t2 = t2;
	}
	public Timestamp getT3() {
		return t3;
	}
	public void setT3(Timestamp t3) {
		this.t3 = t3;
	}
	@Override
	public String toString() {
		return "TimeTest [name=" + name + ", t1=" + t1 + ", t2=" + t2 + ", t3="
				+ t3 + "]";
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
