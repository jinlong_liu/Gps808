<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>客运分析</title>
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="css/plugins/jsTree/style.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">

<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">


<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">





<!-- <link rel="shortcut icon" href="favicon.ico"> -->
<!-- <link href="css/style.min.css?v=4.0.0" rel="stylesheet"> -->
<!-- <link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet"> -->
<!-- <link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet"> -->

<!-- <link href="css/animate.min.css" rel="stylesheet"> -->
<!-- <link href="css/plugins/jsTree/style.min.css" rel="stylesheet"> -->
<style>
.jstree-open>.jstree-anchor>.fa-folder:before {
	content: "\f07c"
}

.jstree-default .jstree-icon.none {
	width: 0
}
</style>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">
	<div class="row">
		<div class="col-sm-9">
		<div class="ibox-title">
						<h5>查询条件</h5> 
					</div>
		<div class="ibox-content" style="height: 780px">
			<div class="pull-right">
						 <div class="col-sm-1" style="width: 90px;margin-top: 7px"> 开始时间:</div> 
		<div class="col-sm-2">
			<input type="text" name="startDate" id="startDate" class="form-control"
				value="${meterStartDate}" class="Wdate"
				onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss'})">
				</div>
			<div class="col-sm-1" style="width: 90px;margin-top: 7px"> 	 结束时间:</div> 
				<div class="col-sm-2">
			 <input type="text" name="endDate" id="endDate" class="form-control"
				  value="${meterEndDate}" class="Wdate"
				onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'startDate\')}',dateFmt:'yyyy-MM-dd HH:mm:ss'})">
			</div>
			<input type="hidden" name="taxiNums" id="taxiNums">
			
			<input type="radio" name="type" id="type" value="day" checked="checked">按天查询  &nbsp;&nbsp;&nbsp;&nbsp;
			<input type="radio" name="type" id="type" value="month">按月查询
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input id="getOneBtn"  type="button" value="查询" onclick="click()" class="btn btn-primary btn-sm-2" >
			<!-- 导出按钮（测试版） -->&nbsp;&nbsp;&nbsp;
				<button type="button" class="btn btn-success" id="demo1">导出</button>
				<!-- 导出表格结束 -->
			 </div>
			

 			<div class="ibox float-e-margins">
		 
						<!-- <div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
							</a> <a class="dropdown-toggle" data-toggle="dropdown"
								href="graph_flot.html#"> <i class="fa fa-wrench"></i>
							</a>
							<ul class="dropdown-menu dropdown-user">
								<li><a href="graph_flot.html#">选项1</a></li>
								<li><a href="graph_flot.html#">选项2</a></li>
							</ul>
							<a class="close-link"> <i class="fa fa-times"></i>
							</a>
						</div> -->
					 
					<div class="ibox-content">
						<div class="echarts" id="echarts-line-chart"  ></div>
					</div>
				</div>
			</div>
			</div>
			<!--右侧公司选择栏开始  -->
			<div class="col-sm-3">
				<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h4>请选择车辆</h4>
							<input type="text" class="form-control form-control-sm"
							id="txtIndustryArea">
						</div>
					<div class="ibox-content" style="height: 735px">
						<div id="jstree1" class="trdemo"
							style=" overflow-x: auto; overflow-y: auto; height: 500px;"> 
							<ul>
								<li class="jstree-open">所有公司
									<ul>
										<c:forEach items="${comInfo2s}" var="comInfo2s">
											<li id="${comInfo2s.comId}">${comInfo2s.cname }
												<ul>
													<c:forEach items="${comInfo2s.taxis}" var="taxis">
														<li id="${taxis.taxiNum}" data-jstree='{"type":"car"}'>
															${taxis.taxiNum }</li>
													</c:forEach>
												</ul>
											</li>
										</c:forEach>
									</ul>
								</li>
							</ul>
						</div>

					</div>
				</div>
			</div>
		</div>
	
			<!-- 导出表格表单 -->
		 <form action="getKeYunExcel" id="KeYunPoi" method="post">
			<input type="hidden" name="taxiNum" id="taxiNumPoi"> <input
				type="hidden" name="startDate" id="startDatePoi"> <input
				type="hidden" name="endDate" id="endDatePoi"><input type="hidden" name="type" id="typepoi">
		</form> 

	</div>
	<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	
	<script src="js/plugins/echarts/echarts-all.js"></script>
	<script src="js/content.min.js?v=1.0.0"></script>
	<script src="js/demo/echarts-demo2.min.js"></script>
	<script src="js/plugins/jsTree/jstree.min.js"></script>
	
	
	
	
	
	
	
	
	
	
<!-- 	<script src="js/jquery.min.js?v=2.1.4"></script> -->
<!-- 	<script src="js/bootstrap.min.js?v=3.3.5"></script> -->
<!-- 	<script src="js/plugins/echarts/echarts-all.js"></script> -->
<!-- 	<script src="js/content.min.js?v=1.0.0"></script> -->
	<!-- 	<script src="js/demo/echarts-demo.min.js"></script> -->
<!-- 	<script src="js/demo/echarts-demo2.min.js"></script> -->
<!-- 	<script src="js/plugins/jsTree/jstree.min.js"></script> -->
<!-- 	<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script> -->
	<script type="text/javascript">
	$("#getOneBtn").click(function() {
		    var startDate = $("#startDate").val();
		 	var endDate =  $("#endDate").val();
		 	var taxiNums =  $("#taxiNums").val();
		 	var type =  $('input[name="type"]:checked').val();
	          if (startDate == "") {
				swal({
					title : "开始时间不能为空",
					type : "warning",
				});
				
			}else if (endDate == "") {
				swal({
					title : "结束时间不能为空",
					type : "warning",
				});
				
			}else if (taxiNums == "") {
				swal({
					title : "请选择车辆",
					type : "warning",
				});
			
			}else if (type == "") {
				swal({
					title : "请选择查询方式！",
					type : "warning",
				});
			
			}else{
			         select2();
			
			}
	});
	
	
	
		function select2() {
		 	var startDate = $("#startDate").val();
		 	var endDate =  $("#endDate").val();
		 	var taxiNums =  $("#taxiNums").val();
		 	var type =  $('input[name="type"]:checked').val();
		 	
			//ajax 请求数据
			$.post("getPassVolByAjax", 
				{"startDate":startDate,"endDate":endDate,"type":type,"taxiNums":taxiNums},
				
				function(data) { 
				
				if (data.length != 0) {
					var newdata = eval("(" + data + ")");
					//alert(newdata);	
					var xdata = new Array(newdata.length);//x轴数,天or月
					var passVol = new Array(newdata.length);//客运量
					
					for (var i = 0; i < newdata.length; i++) {
						xdata[i] = newdata[i].date;
						passVol[i] = newdata[i].passVol;
					}
					//赋值
					//$("#startDate").val(startDate);
					//$("#endDate").val(endDate);
					//$("#taxiNums").val(taxiNums);
					
					//绘制折线图
					var e = echarts.init(document
							.getElementById("echarts-line-chart")), a = {
						title : {
							text : "客运量调查分析"
						},
						tooltip : {
							trigger : "axis"
						},
						legend : {
							data : [ "总客次数"]
						},
						grid : {
							x : 40,
							x2 : 40,
							y2 : 24
						},
						calculable : true,
						//x轴			
						xAxis : [ {
							type : "category",
							boundaryGap : 1,
							data : xdata
						} ],
						yAxis : [ {
							type : "value",
							axisLabel : {
								formatter : "{value} "
							}
						} ],
						
						series : [ {
							name : "总客次数",
							type : "line",
							data : passVol,
							itemStyle : {
								normal : {
									label : {
										show : true
									}
								}
							},
						}/* , {
							name : "载客里程",
							type : "line",
							data : fullmil,
							itemStyle : {
								normal : {
									label : {
										show : true
									}
								}
							},
						}, {
							name : "空驶里程",
							type : "line",
							data : emptymli,
							itemStyle : {
								normal : {
									label : {
										show : true
									}
								}
							}
						}  */]
					};
					e.setOption(a), $(window).resize(e.resize);
				}
			});
		}
		
		//选择公司开始
		$(document).ready(function() {
			/* $("#miletable").hide(); */
			$("#taxiNums").val("");

			$("#jstree1").jstree({
				"core" : {
					"check_callback" : true
				},
				"plugins" : [ "types", "dnd", "checkbox", "sort",
					 "search", "unique" ],
				"types" : {
					"car" : {
						"icon" : "fa fa-car"
					},
					"default" : {
						"icon" : "fa fa-folder"
					}
				}
			});

			//获取选择节点的id
			$("#jstree1").on('changed.jstree', function(e, data) {
				r = [];
				var i, j;
				for (i = 0, j = data.selected.length; i < j; i++) {
					var node = data.instance.get_node(data.selected[i]);
					if (data.instance.is_leaf(node)) {
						r.push(node.id);
					}
				}
				$("#taxiNums").val(r);
			}).on(
					"search.jstree",
					function(e, data) {
						if (data.nodes.length) {
							var matchingNodes = data.nodes; // change
							$(this).find(".jstree-node").hide().filter(
									'.jstree-last').filter(function() {
								return this.nextSibling;
							}).removeClass('jstree-last');
							data.nodes
									.parentsUntil(".jstree")
									.addBack()
									.show()
									.filter(".jstree-children")
									.each(
											function() {
												$(this)
														.children(
																".jstree-node:visible")
														.eq(-1)
														.addClass(
																"jstree-last");
											});
							// nodes need to be in an expanded state
							matchingNodes.find(".jstree-node").show(); // change
						}
					}).on(
					"clear_search.jstree",
					function(e, data) {
						if (data.nodes.length) {
							$(this).find(".jstree-node").css("display", "")
									.filter('.jstree-last').filter(
											function() {
												return this.nextSibling;
											}).removeClass('jstree-last');
						}
					});
			
			$("#txtIndustryArea").keyup(function() {
				
				var v = $("#txtIndustryArea").val();
			
				$("#jstree1").jstree(true).search(v);
		});
		});
		//点击"导出表格"按钮
		$("#demo1").click(
				function() {
					var startTime = $("#startDate").val();
					var endTime = $("#endDate").val();
					var nowTime = $("#endDate").val();//页面显示的结束时间
					var taxiNum = $("#taxiNums").val();
					var type =  $('input[name="type"]:checked').val();
					if (nowTime == "" || nowTime == null) {
						nowTime = "现在时刻";
					}
					if (startTime == "" || taxiNum == "") {
						swal({
							title : "导出表格时，开始时间和车牌号不能为空",
							type : "warning"
						});
					} else {
						swal({
							title : "",
							text : "您确定导出\n" + startTime
									+ " 到 " + nowTime
									+ " 期间，\n 车牌号为"
									+ taxiNum + "客运数据吗?",
							type : "warning",
							showCancelButton : true,
							cancelButtonText : "取消"
						}, function() {
							//赋值
							$("#startDatePoi").val(startTime);
							$("#endDatePoi").val(endTime);
							$("#taxiNumPoi").val(taxiNum);
							$("#typePoi").val(type);
							//提交表单
							$("#KeYunPoi").submit();
						});
					}

				});
	</script>
</body>

</html>