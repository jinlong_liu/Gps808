package com.hx.gps.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hx.gps.dao.TaxiDao;
import com.hx.gps.entities.IsuCache;
import com.hx.gps.service.InfoService;

/**
 * @author LFX
 * @version 创建时间：2017年2月11日 上午12:04:50 不经彻骨寒 ☆ 怎觉梅花香
 */
@Service
public class TaskJob {
	@Autowired
	private TaxiDao taxiDao;
	@Autowired
	private InfoService infoService;
	int type = 0;

	public void job1() {

		infoService.timeTask();
	}

	public void job() {
		System.out.println("当前对象" + taxiDao);
		List<IsuCache> isu = new ArrayList<IsuCache>();
		System.out.println("方法" + taxiDao.getAllDriver());
		System.out.println("方法2" + taxiDao.getTaxiByIsu("015800000000"));
		// System.out.println("不为空");
		isu = taxiDao.getAllIsuCache();
		System.out.println("集合元素" + isu);

		for (IsuCache isuCache : isu) {
			System.out.println("执行中=====");
			System.out.println("看看行不行" + isuCache.toString());
			type = isuCache.getType();
			/**
			 * 做出如下改动。原因：我在taxidao的addisu方法向上抛出异常，该异常会在尝试向mongo插入数据失败后抛出。
			 * 当一次重传失败后，异常被如下代码捕获，抛出异常，执行异常后代码部分。
			 * 重传失败后不能把该数据删除，要等下次定时任务触发后再次重传，直到重传成功后删除缓存表中的数据。
			 */
			try {
				if (type == 1) {
					taxiDao.addIsu(isuCache.getIsu());
				} else if (type == 2) {
					taxiDao.updateIsu(isuCache.getIsu2(), isuCache.getIsu());
				} else if (type == 3) {
					taxiDao.deleteIsu(isuCache.getIsu());
				} else {
					System.out.println("没有找到对应类型");
				}
				// 执行成功跳到这一步，删除原有记录。
				taxiDao.deleteIsuCacheById(isuCache);
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("重试失败...");
			}

		}
	}
}
