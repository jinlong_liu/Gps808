package com.hx.gps.entities.tool;

public class ComInfo {
	private int comId;
	private String cname;
	public int getComId() {
		return comId;
	}
	public void setComId(int comId) {
		this.comId = comId;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public ComInfo(int comId, String cname) {
		super();
		this.comId = comId;
		this.cname = cname;
	}
	public ComInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
