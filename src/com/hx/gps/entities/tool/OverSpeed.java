/**
 * 
 */
package com.hx.gps.entities.tool;

/**
 * @Author: WK
 * @Title:
 * @Description:
 *
 */
public class OverSpeed {
	
	private int maxSpeed; //最高车速速度限制	U16	单位km/h。
	private String duration; //持续时间	U8	单位秒，范围5-200

	public int getMaxSpeed() {
		return maxSpeed;
	}
	
	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	
	public String getDuration() {
		return duration;
	}
	
	public void setDuration(String duration) {
		this.duration = duration;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "OverSpeed [maxSpeed=" + maxSpeed + ", duration=" + duration
				+ ", getMaxSpeed()=" + getMaxSpeed() + ", getDuration()="
				+ getDuration() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

}
