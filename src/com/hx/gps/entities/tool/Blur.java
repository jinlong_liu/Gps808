/**
 * 
 */
package com.hx.gps.entities.tool;

import java.util.List;

/**
 * @Author: WK
 * @Title:
 * @Description:
 *
 */
public class Blur {
	
	private Integer size;
	private List<Address> list;
	private List<Alarm2> listAlarm;
	/**
	 * @return the size
	 */
	public Integer getSize() {
		return size;
	}
	/**
	 * @param size the size to set
	 */
	public void setSize(Integer size) {
		this.size = size;
	}
	/**
	 * @return the list
	 */
	public List<Address> getList() {
		return list;
	}
	/**
	 * @param list the list to set
	 */
	public void setList(List<Address> list) {
		this.list = list;
	}
	/**
	 * @return the listAlarm
	 */
	public List<Alarm2> getListAlarm() {
		return listAlarm;
	}
	/**
	 * @param listAlarm the listAlarm to set
	 */
	public void setListAlarm(List<Alarm2> listAlarm) {
		this.listAlarm = listAlarm;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Blur [size=" + size + ", list=" + list + ", listAlarm="
				+ listAlarm + "]";
	}
	
	

}
