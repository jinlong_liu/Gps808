package com.hx.gps.entities.tool;

import java.util.Date;

import com.hx.gps.util.DateUtil;
import com.mongodb.BasicDBObject;

public class Meter {
	//计价器记录表
	private String taxiNum;		//车牌号
	private String isuNum;		//终端号
	private String quaNum;		//资格证号
	private String upTime;		//上车时间
	private String downTime;		//下车时间
	private float emptymil;		//空驶里程
	private float mileage;		//该客次计程
	private String waitTime;	//等待时间
	private float price;		//单价
	private float money;		//租金
//	private String roommate;	
	private String evaluate;	//评价 （是否合租何曾）
	
	public String getTaxiNum() {
		return taxiNum;
	}
	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}
	
	public String getIsuNum() {
		return isuNum;
	}
	public void setIsuNum(String isuNum) {
		this.isuNum = isuNum;
	}
	public String getWaitTime() {
		return waitTime;
	}
	public void setWaitTime(String waitTime) {
		this.waitTime = waitTime;
	}
	public String getQuaNum() {
		return quaNum;
	}
	public void setQuaNum(String quaNum) {
		this.quaNum = quaNum;
	}
	
	public String getUpTime() {
		return upTime;
	}
	public void setUpTime(String upTime) {
		this.upTime = upTime;
	}
	public String getDownTime() {
		return downTime;
	}
	public void setDownTime(String downTime) {
		this.downTime = downTime;
	}
	public float getEmptymil() {
		return emptymil;
	}
	public void setEmptymil(float emptymil) {
		this.emptymil = emptymil;
	}
	public float getMileage() {
		return mileage;
	}
	public void setMileage(float mileage) {
		this.mileage = mileage;
	}
	
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public float getMoney() {
		return money;
	}
	public void setMoney(float money) {
		this.money = money;
	}
	public String getEvaluate() {
		return evaluate;
	}
	public void setEvaluate(String evaluate) {
		this.evaluate = evaluate;
	}
	
	
	public Meter() {
		super();
	}
	
	
	public Meter(BasicDBObject finds) {
		super();
		this.taxiNum = finds.getString("taxiNum");
		this.isuNum = finds.getString("isuNum");
		this.quaNum = finds.getString("quaNum");
		this.upTime = DateUtil.dateToString(finds.getDate("upTime"));
		this.downTime =  DateUtil.dateToString(finds.getDate("downTime"));
		this.emptymil = (float) finds.getDouble("emptymil");
		this.mileage = (float) finds.getDouble("mileage");
		this.waitTime = DateUtil.changeWaitTime(finds.getString("waitTime"));
		this.price = (float) finds.getDouble("price");
		this.money = (float) finds.getDouble("money");
//		this.roommate = roommate;
		this.evaluate = finds.getString("evaluate");
	}
	@Override
	public String toString() {
		return "Operate [taxiNum=" + taxiNum + ", isuNum=" + isuNum
				+ ", quaNum=" + quaNum + ", upTime=" + upTime + ", downTime="
				+ downTime + ", emptymil=" + emptymil + ", mileage=" + mileage
				+ ", waitTime=" + waitTime + ", price=" + price + ", money="
				+ money + ", evaluate=" + evaluate + "]";
	}
	
	
}
