package com.hx.gps.entities.tool;

import java.util.Date;

import com.mongodb.BasicDBObject;

public class MeterTemp {
	
	private String taxiNum;
	private String isuNum;
	private String quaNum;
	private int state;
	private Date time;
	
	public String getTaxiNum() {
		return taxiNum;
	}
	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}
	public String getIsuNum() {
		return isuNum;
	}
	public void setIsuNum(String isuNum) {
		this.isuNum = isuNum;
	}
	public String getQuaNum() {
		return quaNum;
	}
	public void setQuaNum(String quaNum) {
		this.quaNum = quaNum;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public MeterTemp() {
		super();
	}
	
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public MeterTemp(BasicDBObject finds) {
		super();
		this.taxiNum = finds.getString("taxiNum");;
		this.isuNum = finds.getString("isuNum");;
		this.quaNum = finds.getString("quaNum");;
		this.state = finds.getInt("state");
		this.time = finds.getDate("time");
	}
	

}
