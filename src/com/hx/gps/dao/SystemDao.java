package com.hx.gps.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.hx.gps.entities.tool.Alarm;
import com.hx.gps.entities.tool.OrderVector;
import com.hx.gps.entities.tool.OverSpeed;
import com.hx.gps.entities.tool.Picture;
import com.hx.gps.entities.tool.WaitOrder;

public interface SystemDao {

	//更新报警信息的处理状态，修改为：已处理
	public boolean updateAlarm(Alarm alarm);

	//根据isuNum及时间段查看图片
	public List<Picture> getPicTureByIsu(String startDate,String endDate,String isuNum,String dbname);
	
	//根据时间获取拍照记录
	public List<Picture> getPicTureByDate(String startDate, String endDate, String dbname);
	//查询拍照记录表中的最后五条记录
	public List<Picture> getLast5OfPic(String dbname);

	//根据时间段查看命令日志表中的记录
	public List<OrderVector> getOrderByLog(String startDate,String endDate);

	//根据时间段查看等待发送日志表中的记录
	public List<OrderVector> getOrderByWait(String startDate,String endDate);

	//根据时间查询命令日志表中的最后五条记录
	public List<OrderVector> getOrderLast5OfWait();
	public List<OrderVector> getOrderLast5OfLog();
	
	//将升级文件中的数据保存到mongo中
	public boolean saveUpdateData(String version,byte[] data,Date date);
	
	//从mongo中获取最新升级文件名
	public String latestVer();
	//获取当前终端版本号
	public String getIsuVersion(String isuNUM);
	//将终端添加至待发送版本数据表
	public boolean saveIsuToWairUpgrade(String isu,String latestVer);
	//获取终端版本号记录的时间
	public Date getIsuVersionDate(String isu);
	//发送终端指令
	public void sendIsuInstruct(WaitOrder w);
	

	//wk 进入超速设置界面
	public List<OverSpeed> getOverSpeed();
	//wk 保存超速设置
	public void saveOverSpeed(OverSpeed overSpeed);
 }
