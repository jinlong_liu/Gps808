package com.hx.gps.service;

import java.util.List;

import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.tool.WaitOrder;


public interface OrderService {
	//下发文本，返回值为车辆、状态组成对象的集合，现在以Taxi代替，使用taxiNum和taxiType代替车牌号、状态
	public List<Taxi> sendText(WaitOrder waitOrder,String[] taxiNums);
	//下发继电器控制
	public List<Taxi> sendRelay(WaitOrder w, String[] taxiNum);
	//下发区域
	public List<Taxi> sendArea(WaitOrder w, String[] taxiNum);
	//删除区域
	public List<Taxi> deleArea(WaitOrder w, String[] taxiNum);
	//获取终端版本号
	public String geiVersion(WaitOrder w);
	//下发开关机
	public List<Taxi> sendPower(WaitOrder w, String[] taxiNum);
	//下发远程调价
	public List<Taxi> sendRent(WaitOrder w, String[] taxiNum);
	//下发超速设置
	public List<Taxi> sendSpeed(WaitOrder w, String[] taxiNum);
	//下发远程锁机
	public List<Taxi> sendLock(WaitOrder w, String[] taxiNum);
	
	
}
