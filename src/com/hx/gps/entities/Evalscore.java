package com.hx.gps.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Evalscore entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "evalscore" )
public class Evalscore implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer low;
	private Integer score;
	private Integer top;

	// Constructors

	/** default constructor */
	public Evalscore() {
	}

	/** full constructor */
	public Evalscore(Integer low, Integer score, Integer top) {
		this.low = low;
		this.score = score;
		this.top = top;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "low")
	public Integer getLow() {
		return this.low;
	}

	public void setLow(Integer low) {
		this.low = low;
	}

	@Column(name = "score")
	public Integer getScore() {
		return this.score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	@Column(name = "top")
	public Integer getTop() {
		return this.top;
	}

	public void setTop(Integer top) {
		this.top = top;
	}

	@Override
	public String toString() {
		return "Evalscore [id=" + id + ", low=" + low + ", score=" + score
				+ ", top=" + top + "]";
	}

}