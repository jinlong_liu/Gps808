<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<!-- 查看营运信息 -->
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>HX</title>
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link rel="shortcut icon" href="favicon.ico">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<!--表格样式相关  -->
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!--表格排序相关 -->
<link href="css/plugins/jsTree/style.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<!-- 基本样式 -->
<!-- <base target="_blank"> -->
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">

<style>
.jstree-open>.jstree-anchor>.fa-folder:before {
	content: "\f07c"
}

.jstree-default .jstree-icon.none {
	width: 0
}
</style>

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-8">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>查看营运记录</h5>
						<div class="ibox-tools">
							<form action="checkMeterByTaxi" method="post">
								<input type="text" id="taxiNums" name="taxiNums">
								开始时间： <input type="text" name="startDate" id="startDate"
									value="${meterStartDate}" class="Wdate"
									onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'endDate\')||\'new Date()\'}',readOnly:true})">
								结束时间： <input type="text" name="endDate" id="endDate"
									placeholder="默认为当前时间" value="${meterEndDate}" class="Wdate"
									onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'startDate\')}',maxDate:'#F{\'new Date()\'}',dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true})">

								<input type="submit" value="查看">
							</form>
							<!-- <a class="collapse-link"> <i class="fa fa-chevron-up"></i>
							</a> -->
						</div>
					</div>
					<br>
					<div class="ibox-content">
						<div id="miletable">
							<table
								class="table table-striped table-bordered table-hover dataTables-example">
								<thead>
									<tr>
										<th>车牌号</th>
										<th>资格证号</th>
										<th>上车时间</th>
										<th>下车时间</th>
										<th>空驶里程</th>
										<th>该客次里程</th>
										<th>等待时间</th>
										<th>单价</th>
										<th>租金</th>
										<th>是否合租合乘</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${meter}" var="meter">
										<tr>
											<td>${meter.taxiNum}</td>
											<td>${meter.quaNum}</td>
											<td>${meter.upTime}</td>
											<td>${meter.downTime}</td>
											<td>${meter.emptymil}</td>
											<td>${meter.mileage}</td>
											<td>${meter.waitTime}</td>
											<td>${meter.price}</td>
											<td>${meter.money}</td>
											<td>${meter.evaluate==00?"非合租合乘":"合租合乘"}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!--右侧公司选择栏开始  -->
			<div class="col-sm-4">
				<div class="ibox float-e-margins">
					<div class="ibox-content">
						<div class="ibox-title">
							<h5>车辆选择</h5>
						</div>
						<br>
						<div id="jstree1">
							<ul>
								<li class="jstree-open">所有公司
									<ul>
										<c:forEach items="${comInfo2s}" var="comInfo2s">
											<li id="${comInfo2s.comId}">${comInfo2s.cname }
												<ul>
													<c:forEach items="${comInfo2s.taxis}" var="taxis">
														<li id="${taxis.taxiNum}" data-jstree='{"type":"car"}'>
															${taxis.taxiNum }</li>
													</c:forEach>
												</ul>
											</li>
										</c:forEach>
									</ul>
								</li>
							</ul>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<!--包含表格的翻页等功能  -->
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<!-- 包含表格的样式 -->
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<!-- 显示表格样式必含 -->
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/echarts/echarts-all.js"></script>
	<script src="js/content.min.js?v=1.0.0"></script>
	<script src="js/demo/echarts-demo2.min.js"></script>
	<script src="js/plugins/jsTree/jstree.min.js"></script>
	<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
	
	<script type="text/javascript">
		//开始查询数据
		/* function check(){
			var companys=$("#taxiNums").val();
			var startDate=$("#startDate").val();
			var endDate=$("#endDate").val();
			//alert(type+companys);
			if(companys!=""){
				var args = {
					"companys" : companys,
					"startDate" : startDate,
					"endDate" : endDate
				};
				var url = "mileageUtil";
				$.post(url,args,function(data){
					var newdata = eval("(" + data + ")");
					//alert(data);
					document.getElementById('totalMileage').innerHTML=newdata.totalMileage;
					document.getElementById('mileageUtil').innerHTML=newdata.mileageUtil;
					document.getElementById('avgmileage1').innerHTML=newdata.totalMileage/newdata.taxiNumber/newdata.dayNumber;	//单车日均营运
					document.getElementById('avgmileage2').innerHTML=newdata.mileageUtil/newdata.taxiNumber/newdata.dayNumber;	//单车日均载客
					document.getElementById('rate').innerHTML=newdata.rate+"%";
					document.getElementById('taxiNumber').innerHTML="该表统计了"+newdata.taxiNumber+"辆汽车在"+newdata.dayNumber+"天内的行程记录";
					//$("#totalMileage").val(newdata.totalMileage);
				});
				$("#miletable").show();
			}
			
			
		} */

		//选择公司开始
		$(document).ready(function() {
			/* $("#miletable").hide(); */
			$("#taxiNums").val("");

			$("#jstree1").jstree({
				"core" : {
					"check_callback" : true
				},
				"plugins" : [ "types", "dnd", "checkbox" ],
				"types" : {
					"car" : {
						"icon" : "fa fa-car"
					},
					"default" : {
						"icon" : "fa fa-folder"
					}
				}
			});

			//获取选择节点的id
			$("#jstree1").on('changed.jstree', function(e, data) {
				r = [];
				var i, j;
				for (i = 0, j = data.selected.length; i < j; i++) {
					var node = data.instance.get_node(data.selected[i]);
					if (data.instance.is_leaf(node)) {
						r.push(node.id);
					}
				}
				$("#taxiNums").val(r);
			});

			$(".dataTables-example").dataTable();
			var oTable = $("#editable").dataTable();
			oTable.$("td").editable("../example_ajax.php", {
				"callback" : function(sValue, y) {
					var aPos = oTable.fnGetPosition(this);
					oTable.fnUpdate(sValue, aPos[0], aPos[1])
				},
				"submitdata" : function(value, settings) {
					return {
						"row_id" : this.parentNode.getAttribute("id"),
						"column" : oTable.fnGetPosition(this)[2]
					}
				},
				"width" : "90%",
				"height" : "100%"
			})
		});
	
		function fnClickAddRow() {
			$("#editable").dataTable()
					.fnAddData(
							[ "Custom row", "New row", "New row", "New row",
									"New row" ])
		};
	</script>
</body>

</html>