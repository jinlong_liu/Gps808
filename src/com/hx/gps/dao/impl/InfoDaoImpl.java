package com.hx.gps.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hx.gps.dao.InfoDao;
import com.hx.gps.entities.Accident;
import com.hx.gps.entities.Accidenttype;
import com.hx.gps.entities.Blacklist;
import com.hx.gps.entities.Breakrule;
import com.hx.gps.entities.Check;
import com.hx.gps.entities.Company;
import com.hx.gps.entities.Driver;
import com.hx.gps.entities.Fare;
import com.hx.gps.entities.Insure;
import com.hx.gps.entities.Power;
import com.hx.gps.entities.Role;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.Taxitype;
import com.hx.gps.entities.User;
import com.hx.gps.entities.Yearinspect;
import com.hx.gps.entities.tool.DatatablesQuery;
import com.hx.gps.entities.tool.InfoQuery;
import com.hx.gps.entities.tool.InfoQuery2;
import com.hx.gps.entities.tool.MeterState;
import com.hx.gps.entities.tool.TerminalState;
import com.hx.gps.entities.tool.videoState;
import com.hx.gps.util.DBUtil;
import com.hx.gps.util.DateUtil;
import com.hx.gps.util.FarmConstant;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

@Repository
public class InfoDaoImpl implements InfoDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	// 查询所有用户
	@SuppressWarnings("unchecked")
	@Override
	public List<User> getAllUser() {
		String hql = "from User";
		Query query = getSession().createQuery(hql);
		List<User> users = query.list();
		return users;
	}

	// 查询低级别用户
	@Override
	public List<User> getUserByCom(Company company) {
		Subject subject = SecurityUtils.getSubject();
		String hql = "from User where 1=1";
		// 超级用户上一步已经实现所有用户的显示
		// 普通用户不具备管理条件。
		// 公司管理员 可显示全部公司用户包括他自己。
		if (subject.isPermitted("user:add")) {
			if (company != null) {
				hql = hql + "and comId=" + company.getId();
			}
		} else {
			// 没有管理权限的人也没有用户管理功能，所以直接丢弃。
		}
		Query query = getSession().createQuery(hql);
		List<User> users = new ArrayList<>();
		users = query.list();
		return users;
	}

	// 查询所有角色
	@SuppressWarnings("unchecked")
	@Override
	public List<Role> getAllRole() {
		Subject subject = SecurityUtils.getSubject();
		String hql = "from Role where 1=1";
		if (subject.isPermitted("admin:add")) {
			hql = hql + " and name = 'user' or name = 'admin' or name='办公室' or name='投诉科' or name='监控室'";
		} else if (subject.isPermitted("user:add")) {
			hql = hql + " and name = 'user' or name='办公室' or name='投诉科' or name='监控室'";
		}
		Query query = getSession().createQuery(hql);
		List<Role> roles = query.list();
		return roles;
	}

	// 查询所有公司
	@SuppressWarnings("unchecked")
	@Override
	public List<Company> getAllCompany() {
		String hql = "from Company";
		Query query = getSession().createQuery(hql);
		List<Company> companies = query.list();
		return companies;
	}

	@Override
	public List<Accident> listAccidents(InfoQuery infoQuery) {
		String hql = "from Accident where 1=1";
		if (infoQuery.getTaxiNum() != null && !"".equals(infoQuery.getTaxiNum())) {// 模糊查询
			hql = hql + " and taxiNum like '%" + infoQuery.getTaxiNum() + "%'";
		}
		Query query = getSession().createQuery(hql);
		List<Accident> accidents = query.list();
		// System.out.println(query.list()+"初始数据");
		return accidents;
	}

	// 新增安全事故
	@Override
	public void saveAccident(Accident accident) {
		getSession().save(accident);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Taxi> getTaxi(InfoQuery infoQuery) {
		List<Taxi> taxis = new ArrayList<>();
		// System.out.println(infoQuery);
		String hql = "from Taxi where 1=1";
		Subject subject = SecurityUtils.getSubject();
		if (!infoQuery.getTaxiNum().equals("")) {// 模糊查询
			hql = hql + " and taxiNum like '%" + infoQuery.getTaxiNum() + "%'";
		}
		if (!infoQuery.getMasterName().equals("")) {
			hql = hql + " and masterName like '%" + infoQuery.getMasterName() + "%'";
		}
		if (!infoQuery.getIsuNum().equals("")) {
			hql = hql + " and isuNum like '%" + infoQuery.getIsuNum() + "%'";
		}
		if (!infoQuery.getIsuPhone().equals("")) {
			hql = hql + " and isuPhone like '%" + infoQuery.getIsuPhone() + "%'";
		}
		if (subject.isPermitted("bjcompy:get")) {
			if (infoQuery.getComId() != 0) {
				hql = hql + " and comId =" + infoQuery.getComId();
			}
			hql = hql + " and comId IN (:ids)";
			List<Company> cm = getAllCompanyBj();
			List<Integer> ids = new ArrayList<>();
			if (cm != null) {
				for (Company c : cm) {
					ids.add(c.getId());
				}
			}
			Query query = getSession().createQuery(hql);
			query.setMaxResults(infoQuery.getLimit());
			query.setFirstResult(infoQuery.getOffset());
			query.setParameterList("ids", ids);
			taxis = query.list();
		} else if (subject.isPermitted("allcompy:get")) {
			if (infoQuery.getComId() != 0) {
				hql = hql + " and comId =" + infoQuery.getComId();
			}
			Query query = getSession().createQuery(hql);
			query.setMaxResults(infoQuery.getLimit());
			query.setFirstResult(infoQuery.getOffset());
			taxis = query.list();
		} else {
			String name = (String) subject.getPrincipal();
			Company company = getComByUser(name);
			if (company != null) {
				hql = hql + " and comId =" + company.getId();
			}
			Query query = getSession().createQuery(hql);
			query.setMaxResults(infoQuery.getLimit());
			query.setFirstResult(infoQuery.getOffset());
			taxis = query.list();
		}

		// System.out.println(query.list()+"初始数据");
		return taxis;
	}

	@Override
	public List<Taxi> getTaxiByQuery(InfoQuery infoQuery) {
		// System.out.println(infoQuery);
		String hql = "from Taxi where 1=1";
		/*
		 * if (!infoQuery.getCompanyName().equals("")) { hql = hql +
		 * " and comId =" + infoQuery.getComId(); }
		 */
		if (!infoQuery.getTaxiNum().equals("")) {// 模糊查询
			hql = hql + " and taxiNum like '%" + infoQuery.getTaxiNum() + "%'";
		}
		if (!infoQuery.getTaxiColor().equals("")) {
			hql = hql + " and taxiColor like '%" + infoQuery.getTaxiColor() + "%'";
		}
		if (!infoQuery.getTaxiType().equals("")) {
			hql = hql + " and taxiType like '%" + infoQuery.getTaxiType() + "%'";
		}
		// System.out.println("hql:" + hql);
		Query query = getSession().createQuery(hql);

		query.setMaxResults(infoQuery.getLimit());
		query.setFirstResult(infoQuery.getOffset());
		List<Taxi> taxis = query.list();
		System.out.println(taxis.size() + "初始数据");
		return taxis;
	}

	@Override
	public void addPower(Power power) {
		getSession().save(power);

	}

	@Override
	public void deletePowerByID(Integer powerID) {
		final String hql = "delete from Power p where p.id = :id";
		Query query = getSession().createQuery(hql);
		query.setParameter("id", powerID);
		query.executeUpdate();

	}

	@Override
	public Power getPowerByID(Integer powerID) {
		Power power = (Power) getSession().get(Power.class, powerID);
		return power;
	}

	@Override
	public void saveUser(User user) {
		getSession().save(user);
	}

	@Override
	public void updateUser(User user) {
		getSession().update(user);
	}

	@Override
	public void deleteUser(User user) {
		getSession().delete(user);
	}

	@Override
	public List<Accidenttype> getAccidentType() {
		String hql = "from Accidenttype";
		Query query = getSession().createQuery(hql);
		List<Accidenttype> accidenttypes = query.list();
		return accidenttypes;
	}

	@Override
	public void updateAccident(Accident accident) {
		getSession().update(accident);
	}

	@Override
	public void deleteAccident(Accident accident) {
		getSession().delete(accident);
	}

	@Override
	public void saveRole(Role role) {
		getSession().save(role);
	}

	@Override
	public void updateRole(Role role) {
		getSession().update(role);
	}

	@Override
	public void deleteRole(Role role) {
		getSession().delete(role);
	}

	@Override
	public void saveCompany(Company company) {
		getSession().save(company);
	}

	@Override
	public void updateState(User user) {
		// Session session = getSession();
		// session.beginTransaction().begin();
		// try {
		// Query query = session.createQuery(
		// "update User t set t.state ='" + user.getState() + "'" + " where id
		// ='" + user.getId() + "'");
		// query.executeUpdate();
		// session.getTransaction().commit();
		// } catch (Exception e) {
		// System.out.println("出现异常插入" + e.getMessage());
		// session.getTransaction().rollback();
		// } finally {
		// session.clear();
		// }
		getSession().update(user);
	}

	@Override
	public void updateCompany(Company company) {
		getSession().update(company);
	}

	@Override
	public void deleteCompany(Company company) throws Exception {
		getSession().delete(company);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Taxitype> getAllTaxiType() {
		String hql = "from Taxitype";
		Query query = getSession().createQuery(hql);
		List<Taxitype> taxitypes = query.list();
		return taxitypes;
	}

	@Override
	public boolean saveTaxiType(Taxitype taxitype) {
		Integer result = (Integer) getSession().save(taxitype);
		if (result != null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void updateTaxiType(Taxitype taxitype) {
		getSession().update(taxitype);

	}

	@Override
	public void deleteTaxiType(Taxitype taxitype) {
		getSession().delete(taxitype);
	}

	@Override
	public boolean saveTaxi(Taxi taxi) {
		boolean state = false;
		try {
			Integer n = (Integer) getSession().save(taxi);
			if (n != null) {
				state = true;
			} else {
				state = false;
			}

		} catch (Exception e) {
			System.out.println("格式转换异常");
		}
		return state;
	}

	@Override
	public void updateTaxi(Taxi taxi) {
		getSession().update(taxi);
	}

	@Override
	public void deleteTaxi(Taxi taxi) {
		getSession().delete(taxi);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Driver> getDriver(InfoQuery infoQuery) {
		String hql = "from Driver where 1=1";
		if (infoQuery.getTaxiId() != 0) {
			hql = hql + " and taxiId = " + infoQuery.getTaxiId();
		}
		if (!infoQuery.getQuaNum().equals("")) {
			hql = hql + " and quaNum like '%" + infoQuery.getQuaNum() + "%'";
		}
		if (!infoQuery.getDname().equals("")) {
			hql = hql + " and dname like '%" + infoQuery.getDname() + "%'";
		}
		Query query = getSession().createQuery(hql);
		List<Driver> drivers = query.list();
		return drivers;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Driver> listDrivers(DatatablesQuery datatablesQuery, InfoQuery infoQuery) {
		StringBuffer hql = new StringBuffer();
		// hql.append( "from Driver d left join d.taxi taxi where 1=1");
		hql.append(
				"SELECT * FROM driver LEFT JOIN taxi on driver.taxiId = taxi.id LEFT JOIN company ON driver.panyId = company.id ");
		hql.append("WHERE ");
		// 1.拼接模糊 查询
		if (!infoQuery.getTaxiNum().equals("")) {
			hql.append("taxi.taxiNum like '%");
			hql.append(infoQuery.getTaxiNum());
			hql.append("%' ");
			hql.append("AND ");
		}
		if (!infoQuery.getQuaNum().equals("")) {
			hql.append("quaNum like '%");
			hql.append(infoQuery.getQuaNum());
			hql.append("%' ");
			hql.append("AND ");
		}
		if (!infoQuery.getDname().equals("")) {
			hql.append("dname like '%");
			hql.append(infoQuery.getDname());
			hql.append("%' ");
			hql.append("AND ");
		}
		if(infoQuery.getState()!=null&&infoQuery.getState().equals("1")){
			hql.append("state =1");
			hql.append("AND ");
		}
		// 2.拼接模糊搜索
		if (datatablesQuery.getSearch() != "") {
			hql.append("(company.name like '%");
			hql.append(datatablesQuery.getSearch());
			hql.append("%'");
			hql.append(" OR taxi.taxiNum like '%");
			hql.append(datatablesQuery.getSearch());
			hql.append("%'");
			hql.append(" OR dname like '%");
			hql.append(datatablesQuery.getSearch());
			hql.append("%'");
			hql.append(" OR sex like '%");
			hql.append(datatablesQuery.getSearch());
			hql.append("%'");
			hql.append(" OR age like '%");
			hql.append(datatablesQuery.getSearch());
			hql.append("%'");
			hql.append(" OR phone like '%");
			hql.append(datatablesQuery.getSearch());
			hql.append("%'");
			hql.append(" OR evaGrade like '%");
			hql.append(datatablesQuery.getSearch());
			hql.append("%'");
			hql.append(" OR moniUnit like '%");
			hql.append(datatablesQuery.getSearch());
			hql.append("%'");
			hql.append(" OR moniPhone like '%");
			hql.append(datatablesQuery.getSearch());
			hql.append("%')");
		}

		// 3.如果有最后有and，去除
		if (hql.indexOf("AND ", hql.length() - 4) != -1) {
			hql.delete(hql.indexOf("AND ", hql.length() - 4), hql.length());
		}

		// 4.如果有最后有WHERE，去除
		if (hql.indexOf("WHERE ", hql.length() - 6) != -1) {
			hql.delete(hql.indexOf("WHERE ", hql.length() - 6), hql.length());
		}

		// 5.拼接排序
		switch (datatablesQuery.getOrderColumn()) {
		case "company":
			hql.append(" ORDER BY ");
			hql.append("company.name ");
			hql.append(datatablesQuery.getOrderType());
			break;
		case "taxi":
			hql.append(" ORDER BY ");
			hql.append("taxi.taxiNum ");
			hql.append(datatablesQuery.getOrderType());
			break;
		case "dname":
			hql.append(" ORDER BY ");
			hql.append("dname ");
			hql.append(datatablesQuery.getOrderType());
			break;
		case "sex":
			hql.append(" ORDER BY ");
			hql.append("sex ");
			hql.append(datatablesQuery.getOrderType());
			break;
		case "age":
			hql.append(" ORDER BY ");
			hql.append("age ");
			hql.append(datatablesQuery.getOrderType());
			break;
		case "phone":
			hql.append(" ORDER BY ");
			hql.append("phone ");
			hql.append(datatablesQuery.getOrderType());
			break;
		case "evaGrade":
			hql.append(" ORDER BY ");
			hql.append("evaGrade ");
			hql.append(datatablesQuery.getOrderType());
			break;
		case "moniUnit":
			hql.append(" ORDER BY ");
			hql.append("moniUnit ");
			hql.append(datatablesQuery.getOrderType());
			break;
		case "moniPhone":
			hql.append(" ORDER BY ");
			hql.append("moniPhone ");
			hql.append(datatablesQuery.getOrderType());
			break;
		default:
			break;
		}

		System.out.println(hql.toString());

		// Query query = getSession().createQuery(hql.toString());
		Query query = getSession().createSQLQuery(hql.toString()).addEntity(Driver.class);
		// 分页
		query.setFirstResult(datatablesQuery.getStart());
		query.setMaxResults(datatablesQuery.getLength());
		List<Driver> drivers = query.list();
		return drivers;
	}

	@Override
	public Integer getDriversSize(DatatablesQuery datatablesQuery, InfoQuery infoQuery) {
		// 查询所有的记录条数
		StringBuffer hql = new StringBuffer();
		// 1.模糊查询
		hql.append("select count(dname) from Driver where 1=1");
		if (!infoQuery.getTaxiNum().equals("")) {
			hql.append(" and taxi.taxiNum like '%");
			hql.append(infoQuery.getTaxiNum());
			hql.append("%'");
		}
		if (!infoQuery.getQuaNum().equals("")) {
			hql.append(" and quaNum like '%");
			hql.append(infoQuery.getQuaNum());
			hql.append("%'");
		}
		if (!infoQuery.getDname().equals("")) {
			hql.append(" and dname like '%");
			hql.append(infoQuery.getDname());
			hql.append("%'");
		}
		if(infoQuery.getState()!=null&&infoQuery.getState().equals("1")){
			hql.append("state = 1");
			hql.append("AND ");
		}
		// 2.拼接模糊搜索
		if (datatablesQuery.getSearch() != "") {
			hql.append(" AND (company.name like '%");
			hql.append(datatablesQuery.getSearch());
			hql.append("%'");
			hql.append(" OR taxi.taxiNum like '%");
			hql.append(datatablesQuery.getSearch());
			hql.append("%'");
			hql.append(" OR dname like '%");
			hql.append(datatablesQuery.getSearch());
			hql.append("%'");
			hql.append(" OR sex like '%");
			hql.append(datatablesQuery.getSearch());
			hql.append("%'");
			hql.append(" OR age like '%");
			hql.append(datatablesQuery.getSearch());
			hql.append("%'");
			hql.append(" OR phone like '%");
			hql.append(datatablesQuery.getSearch());
			hql.append("%'");
			hql.append(" OR evaGrade like '%");
			hql.append(datatablesQuery.getSearch());
			hql.append("%'");
			hql.append(" OR moniUnit like '%");
			hql.append(datatablesQuery.getSearch());
			hql.append("%'");
			hql.append(" OR moniPhone like '%");
			hql.append(datatablesQuery.getSearch());
			hql.append("%')");
		}

		Query query = getSession().createQuery(hql.toString());
		System.out.println(hql.toString());
		Integer size = ((Long) query.iterate().next()).intValue();
		return size;
	}

	@Override
	public void saveDriver(Driver driver) {
		getSession().save(driver);
	}

	@Override
	public void updateDriver(Driver driver) {
		System.out.println("driv" + driver.toString());
		getSession().saveOrUpdate(driver);
	}

	@Override
	public void deleteDriver(Driver driver) {
		getSession().delete(driver);
	}

	@Override
	public int getTaxiIdBytaxiNum(InfoQuery infoQuery) {
		String hql = "from Taxi  where taxiNum = '" + infoQuery.getTaxiNum() + "'";
		Taxi taxi = (Taxi) getSession().createQuery(hql).uniqueResult();
		if (taxi != null) {
			return taxi.getId();
		} else {
			return 0;
		}
	}

	/**
	 * @param id
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String[] getTaxiByCompany(Integer id) {
		System.out.println("fabfda" + id);

		String hql = "SELECT t FROM Taxi t WHERE comId=:comId";
		Query query = getSession().createQuery(hql);
		// System.out.println("quarya"+query.list());
		List<Taxi> taxis = query.setInteger("comId", id).list();
		System.out.println("taxi列表" + taxis);
		// @SuppressWarnings("unchecked")
		// List<Taxi> taxis= query.list();
		// System.out.println("taxxxxx"+query.list());
		String[] isu = new String[taxis.size()];
		// System.out.println("taxissss"+taxis);
		// System.out.println("得到车辆集合" + taxis);
		for (int i = 0; i < taxis.size(); i++) {
			isu[i] = taxis.get(i).getIsuNum();
		}

		return isu;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Taxi> getAllTaxi() {
		String hql = "from Taxi t order by t.id ";
		Query query = getSession().createQuery(hql);
		List<Taxi> taxis = query.list();
		return taxis;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Insure> getInsure(InfoQuery infoQuery) {
		String hql = "from Insure where 1=1";
		if (infoQuery.getCategory() != null && !"".equals(infoQuery.getCategory())) {
			hql = hql + " and category = '" + infoQuery.getCategory() + "'";
		}
		if (infoQuery.getMaster() != null && !"".equals(infoQuery.getMaster())) {
			hql = hql + " and master like '%" + infoQuery.getMaster() + "%'";
		}
		if (infoQuery.getType() != null && !"".equals(infoQuery.getType())) {
			hql = hql + " and type like '%" + infoQuery.getType() + "%'";
		}
		if (infoQuery.getPayTimeStartString() != null && !"".equals(infoQuery.getPayTimeStartString())) {
			hql = hql + " and payTime >= '" + DateUtil.getTimestampByStr(infoQuery.getPayTimeStartString()) + "'";
		}
		if (infoQuery.getPayTimeEndString() != null && !"".equals(infoQuery.getPayTimeEndString())) {
			hql = hql + " and payTime <= '" + DateUtil.getTimestampByStr(infoQuery.getPayTimeEndString()) + "'";
		}

		Query query = getSession().createQuery(hql);
		List<Insure> insures = query.list();
		return insures;
	}

	@Override
	public void saveInsure(Insure insure) {
		getSession().save(insure);
	}

	@Override
	public void updateInsure(Insure insure) {
		getSession().update(insure);
	}

	@Override
	public void deleteInsure(Insure insure) {
		getSession().delete(insure);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Blacklist> getBlacklist(InfoQuery2 infoQuery) {
		String hql = "from Blacklist where 1=1";
		if (infoQuery.getType() != null) {
			hql = hql + " and type = " + infoQuery.getType();
		}
		if (!infoQuery.getTaxiNum().equals("")) {
			hql = hql + " and taxiNum like '%" + infoQuery.getTaxiNum() + "%'";
		}

		Query query = getSession().createQuery(hql);
		List<Blacklist> blacklists = query.list();
		return blacklists;
	}

	@Override
	public void saveBlacklist(Blacklist blacklist) {
		getSession().save(blacklist);
	}

	@Override
	public void updateBlacklist(Blacklist blacklist) {
		getSession().update(blacklist);
	}

	@Override
	public void deleteBlacklist(Blacklist blacklist) {
		getSession().delete(blacklist);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Check> getCheck(InfoQuery2 infoQuery) {

		String hql = "from Check where 1=1";
		if (!infoQuery.getTaxiNum().equals("")) {
			hql = hql + " and taxiNum like '%" + infoQuery.getTaxiNum() + "%'";
		}
		if (!infoQuery.getCheckTime().equals("")) {
			hql = hql + " and time > '" + DateUtil.getTimestampByStr(infoQuery.getCheckTime()) + "'";
		}
		if (infoQuery.getState() == 0 || infoQuery.getState() == 1) {
			hql = hql + " and state= '" + infoQuery.getState() + "'";
		}
		// System.out.println(infoQuery.getState()+"toodwadawdw");
		Query query = getSession().createQuery(hql);
		List<Check> checks = query.list();
		return checks;
	}

	@Override
	public void saveCheck(Check check) {
		getSession().save(check);
	}

	@Override
	public void updateCheck(Check check) {
		getSession().update(check);
	}

	@Override
	public void deleteCheck(Check check) {
		getSession().delete(check);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Fare> getFare(String type) {
		String hql = "from Fare where 1=1";
		if (!type.equals("")) {
			hql = hql + " and type = '" + type + "'";
		}

		Query query = getSession().createQuery(hql);
		List<Fare> fares = query.list();
		return fares;
	}

	@Override
	public Company getComByUser(String name) {
		String hql = "from User where 1=1";

		hql = hql + " and name = '" + name + "'";

		Query query = getSession().createQuery(hql);
		User user = (User) query.uniqueResult();
		System.out.println("查询出来当前用户信息" + user.toString());
		Company company = user.getCompany();
		// System.out.println("用户所属公司:"+company.getName());
		return company;

	}

	@Override
	public void saveFare(Fare fare) {
		getSession().save(fare);
	}

	@Override
	public void updateFare(Fare fare) {
		getSession().update(fare);
	}

	@Override
	public void deleteFare(Fare fare) {
		getSession().delete(fare);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Yearinspect> getYearinspect(InfoQuery2 infoQuery2) {

		String hql = "from Yearinspect where 1=1";
		if (infoQuery2.getComId() != null) {
			hql = hql + " and comId = " + infoQuery2.getComId();
		}
		if (!infoQuery2.getStartTime().equals("")) {
			hql = hql + " and lastTime >= '" + DateUtil.getTimestampByStr(infoQuery2.getStartTime()) + "'";
		}
		if (!infoQuery2.getEndTime().equals("")) {
			hql = hql + " and lastTime <= '" + DateUtil.getTimestampByStr(infoQuery2.getEndTime()) + "'";
		}

		Query query = getSession().createQuery(hql);
		List<Yearinspect> yearinspects = query.list();
		return yearinspects;
	}

	@Override
	public void saveYearinspect(Yearinspect yearinspect) {
		getSession().save(yearinspect);
	}

	@Override
	public void updateYearinspect(Yearinspect yearinspect) {
		getSession().update(yearinspect);
	}

	@Override
	public void deleteYearinspect(Yearinspect yearinspect) {
		getSession().delete(yearinspect);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Breakrule> getBreakrule(InfoQuery2 infoQuery2) {
		String hql = "from Breakrule where 1=1";
		if (!infoQuery2.getTaxiNum().equals("")) {
			hql = hql + " and taxiNum like '%" + infoQuery2.getTaxiNum() + "%'";
		}

		if (!infoQuery2.getBreakTime().equals("")) {
			hql = hql + " and breakTime = '" + DateUtil.getTimestampByStr(infoQuery2.getBreakTime()) + "'";
		}
		if (infoQuery2.getState() == 0 || infoQuery2.getState() == 1) {
			hql = hql + " and state= '" + infoQuery2.getState() + "'";
		}

		Query query = getSession().createQuery(hql);
		List<Breakrule> breakrules = query.list();
		return breakrules;
	}

	@Override
	public void saveBreakrule(Breakrule breakrule) {
		getSession().save(breakrule);
	}

	@Override
	public void updateBreakrule(Breakrule breakrule) {
		getSession().update(breakrule);
	}

	@Override
	public void deleteBreakrule(Breakrule breakrule) {
		getSession().delete(breakrule);
	}

	// 根据终端号查看设备状态信息
	@Override
	public List<videoState> getIsuVideoState(String isu, String isuNum) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.vedioState);
		BasicDBObject finds = new BasicDBObject();
		BasicDBObject sortBy = new BasicDBObject();
		// 准备结果集
		List<videoState> videoStates = new ArrayList<>();
		// 封装查询条件
		finds.append("isuNum", isu);
		// 按时间排序
		sortBy.put("time", -1);
		// 查询记录 最新的十条
		DBCursor cursor = coll.find(finds).sort(sortBy).limit(100);
		while (cursor.hasNext()) {
			BasicDBObject dbObject = (BasicDBObject) cursor.next();
			// 填充记录
			videoState videoState = new videoState(dbObject, isuNum);
			videoStates.add(videoState);
		}
		return videoStates;
	}

	public boolean saveAccidentType(Accidenttype accidentType) {
		Integer result = (Integer) getSession().save(accidentType);
		if (result != null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean haveCompany(Company company) {
		String hql = "from Company where 1=1";
		if (company.getName() != null && !company.getName().equals("")) {
			hql = hql + " and name ='" + company.getName() + "'";
		}
		Query query = getSession().createQuery(hql);
		if (query.list().size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public int getTaxiNumber(InfoQuery infoQuery) {
		String hql = "select count(*) from Taxi where 1=1";
		if (!infoQuery.getTaxiNum().equals("")) {// 模糊查询
			hql = hql + " and taxiNum like '%" + infoQuery.getTaxiNum() + "%'";
		}
		if (!infoQuery.getMasterName().equals("")) {
			hql = hql + " and masterName like '%" + infoQuery.getMasterName() + "%'";
		}
		if (!infoQuery.getIsuNum().equals("")) {
			hql = hql + " and isuNum like '%" + infoQuery.getIsuNum() + "%'";
		}
		if (!infoQuery.getIsuPhone().equals("")) {
			hql = hql + " and isuPhone like '%" + infoQuery.getIsuPhone() + "%'";
		}
		Subject subject = SecurityUtils.getSubject();
		if (subject.isPermitted("bjcompy:get")) {
			if (infoQuery.getComId() != 0) {
				hql = hql + " and comId =" + infoQuery.getComId();
			}
			hql = hql + " and comId IN (:ids)";
			List<Company> cm = getAllCompanyBj();
			List<Integer> ids = new ArrayList<>();
			if (cm != null) {
				for (Company c : cm) {
					ids.add(c.getId());
				}
			}
			Query query = getSession().createQuery(hql);
			query.setParameterList("ids", ids);
			return ((Number) query.uniqueResult()).intValue();
		} else if (subject.isPermitted("allcompy:get")) {
			if (infoQuery.getComId() != 0) {
				hql = hql + " and comId =" + infoQuery.getComId();
			}
			Query query = getSession().createQuery(hql);
			return ((Number) query.uniqueResult()).intValue();
		} else {
			String name = (String) subject.getPrincipal();
			Company company = getComByUser(name);
			if (company != null) {
				hql = hql + " and comId =" + company.getId();
			}
			Query query = getSession().createQuery(hql);
			return ((Number) query.uniqueResult()).intValue();
		}
	}

	@Override
	public List<Accidenttype> getAllAccidentType() {
		String hql = "from Accidenttype";
		Query query = getSession().createQuery(hql);
		List<Accidenttype> accidenttypes = query.list();
		return accidenttypes;
	}

	@Override
	public void updateAccidentType(Accidenttype accidenttype) {
		getSession().update(accidenttype);
	}

	@Override
	public void deleteAccidentType(Accidenttype accidenttype) {
		getSession().delete(accidenttype);
	}

	@Override
	public List<MeterState> getMeterStates(String taxiNums) {
		String[] choose = taxiNums.split(",");
		List<String> taxiNum = new ArrayList<>();
		for (int i = 0; i < choose.length; i++) {
			if (choose[i].length() == 7)
				taxiNum.add(choose[i].substring(1));
			else if (choose[i].length() == 8)
				taxiNum.add(choose[i].substring(1, 2) + choose[i].substring(3));
		}
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.Mongo_col_MeterTemp);
		DBObject queryCondition = new BasicDBObject();
		queryCondition = new BasicDBObject();
		BasicDBList values = new BasicDBList();
		values.addAll(taxiNum);
		queryCondition.put("taxiNum", new BasicDBObject("$in", values));
		// 准备结果集
		List<MeterState> meterStates = new ArrayList<>();
		// 按时间排序
		// sortBy.put("time", -1);
		DBCursor cursor = coll.find(queryCondition);
		while (cursor.hasNext()) {
			BasicDBObject dbObject = (BasicDBObject) cursor.next();
			// 填充记录
			MeterState videoState = new MeterState(dbObject);
			meterStates.add(videoState);
		}
		return meterStates;
	}

	@Override
	public List<TerminalState> getTerminalStates(List<String> isuNums) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ADDRESSTRMP);
		DBObject queryCondition = new BasicDBObject();
		queryCondition = new BasicDBObject();
		BasicDBList values = new BasicDBList();
		values.addAll(isuNums);
		queryCondition.put("isuNum", new BasicDBObject("$in", values));
		// 准备结果集
		List<TerminalState> terminalStates = new ArrayList<>();
		// 按时间排序
		// sortBy.put("time", -1);
		DBCursor cursor = coll.find(queryCondition);
		// System.out.println("查询到的车辆终端状态cursor：" + cursor);
		while (cursor.hasNext()) {
			BasicDBObject dbObject = (BasicDBObject) cursor.next();
			// 填充记录
			TerminalState teminalState = new TerminalState(dbObject);
			terminalStates.add(teminalState);
		}
		// System.out.println("查询到的车辆终端状态terminalStates：" + terminalStates);
		return terminalStates;
	}

	@Override
	public void addCjTaxiNum(Taxi taxi) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.CJTAXINUM);
		BasicDBObject finds = new BasicDBObject();
		if (taxi != null) {
			if (taxi.getTaxiNum() != null) {
				finds.append("taxiNum", taxi.getTaxiNum());
			}
			if (taxi.getIsuNum() != null) {
				finds.append("isuNum", taxi.getIsuNum());
			}
			if (finds.size() == 2) {
				coll.save(finds);
			}
		}
	}

	@Override
	public List<Company> getAllCompanyBj() {
		String hql = "from Company where 1=1";
		hql = hql + " and remark ='bj'";
		Query query = getSession().createQuery(hql);
		if (query.list().size() > 0) {
			List<Company> com = new ArrayList<>();
			com = query.list();
			return com;
		} else {
			return null;
		}
	}

	// 审核从业人员
	@Override
	public void ApproalDriverById(Integer id,boolean state) {
		String hql = "from Driver where 1=1";
		if (id != null) {
			hql = hql + " and id =" + id;
		}
		Query query = getSession().createQuery(hql);
		if (query.uniqueResult() != null) {
			Driver dr = (Driver) query.uniqueResult();
			dr.setState(state);
			getSession().update(dr);
		}
	}
}
