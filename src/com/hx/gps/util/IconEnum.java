package com.hx.gps.util;

/**
 * 
 * @ClassName: IconEnum.java
 * @Description: 图标的枚举类，使用Font Awesome图标
 *
 * @version: v1.0.0
 * @author: AN
 * @date: 2019年2月27日 下午9:00:27
 * 
 */
public enum IconEnum {
	/**
	 * 文件夹图标
	 */
	Folder("fa fa-folder"),
	/**
	 * key图标
	 */
	Key("fa fa-key");

	private String value;

	IconEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public static String getIconByType(String type) {
		switch (type) {
		case "key":
			return Key.value;
		case "folder":
			return Folder.value;
		default:
			return "";
		}
	}

}
