package com.hx.gps.entities.tool;

public class TaxiDetails {
	// 车牌号
	private String taxiNum;
	// 终端号
	private String isuNum;
	// gps状态
	private String gpsState;
	// 天线状态
	private String anntaState;
	// 上电状态
	private String energyState;
	// 手机信号
	private String phoneState;
	//acc状态
	private String accState;
	//北南纬
	private String latState;
	//东西经
	private String lonState;
	//运营状态
	private String meterState;
	//加密状态
	private String keyState;
	//供油
	private String oilState;
	//供电
	private String engState;
	//车门状态
	private String doorState;
	//报警状态
	private String alarmState;
	//定位状态
	private String directionState;
	//驾驶员名称
	private String drName;
	//性别
	private String drSex;
	//年龄
	private String drAge;
	//资格证号
	private String drQua;
	//驾驶状态
	private String drState;
	//头像
	private String drPhoto;
	//公司
	private String drCom;
	//电话
	private String drPhone;
	public TaxiDetails(String taxiNum, String isuNum, String gpsState, String anntaState, String energyState,
			String phoneState, String accState, String latState, String lonState, String meterState, String keyState,
			String oilState, String engState, String doorState, String alarmState, String directionState, String drName,
			String drSex, String drAge, String drQua, String drState, String drPhoto, String drCom, String drPhone,
			String drNum, String drTime) {
		super();
		this.taxiNum = taxiNum;
		this.isuNum = isuNum;
		this.gpsState = gpsState;
		this.anntaState = anntaState;
		this.energyState = energyState;
		this.phoneState = phoneState;
		this.accState = accState;
		this.latState = latState;
		this.lonState = lonState;
		this.meterState = meterState;
		this.keyState = keyState;
		this.oilState = oilState;
		this.engState = engState;
		this.doorState = doorState;
		this.alarmState = alarmState;
		this.directionState = directionState;
		this.drName = drName;
		this.drSex = drSex;
		this.drAge = drAge;
		this.drQua = drQua;
		this.drState = drState;
		this.drPhoto = drPhoto;
		this.drCom = drCom;
		this.drPhone = drPhone;
		this.drNum = drNum;
		this.drTime = drTime;
	}

	public String getDrPhone() {
		return drPhone;
	}

	public void setDrPhone(String drPhone) {
		this.drPhone = drPhone;
	}

	//驾驶证号
	private String drNum;
	public TaxiDetails(String taxiNum, String isuNum, String gpsState, String anntaState, String energyState,
			String phoneState, String accState, String latState, String lonState, String meterState, String keyState,
			String oilState, String engState, String doorState, String alarmState, String directionState, String drName,
			String drSex, String drAge, String drQua, String drState, String drPhoto, String drCom, String drNum,
			String drTime) {
		super();
		this.taxiNum = taxiNum;
		this.isuNum = isuNum;
		this.gpsState = gpsState;
		this.anntaState = anntaState;
		this.energyState = energyState;
		this.phoneState = phoneState;
		this.accState = accState;
		this.latState = latState;
		this.lonState = lonState;
		this.meterState = meterState;
		this.keyState = keyState;
		this.oilState = oilState;
		this.engState = engState;
		this.doorState = doorState;
		this.alarmState = alarmState;
		this.directionState = directionState;
		this.drName = drName;
		this.drSex = drSex;
		this.drAge = drAge;
		this.drQua = drQua;
		this.drState = drState;
		this.drPhoto = drPhoto;
		this.drCom = drCom;
		this.drNum = drNum;
		this.drTime = drTime;
	}

	public String getDrName() {
		return drName;
	}

	public void setDrName(String drName) {
		this.drName = drName;
	}

	public String getDrSex() {
		return drSex;
	}

	public void setDrSex(String drSex) {
		this.drSex = drSex;
	}

	public String getDrAge() {
		return drAge;
	}

	public void setDrAge(String drAge) {
		this.drAge = drAge;
	}

	public String getDrQua() {
		return drQua;
	}

	public void setDrQua(String drQua) {
		this.drQua = drQua;
	}

	public String getDrState() {
		return drState;
	}

	public void setDrState(String drState) {
		this.drState = drState;
	}

	public String getDrPhoto() {
		return drPhoto;
	}

	public void setDrPhoto(String drPhoto) {
		this.drPhoto = drPhoto;
	}

	public String getDrCom() {
		return drCom;
	}

	public void setDrCom(String drCom) {
		this.drCom = drCom;
	}

	public String getDrNum() {
		return drNum;
	}

	public void setDrNum(String drNum) {
		this.drNum = drNum;
	}

	public String getDrTime() {
		return drTime;
	}

	public void setDrTime(String drTime) {
		this.drTime = drTime;
	}

	//签到、签退时间
	private String drTime;
	public String getDirectionState() {
		return directionState;
	}

	public void setDirectionState(String directionState) {
		this.directionState = directionState;
	}

	public TaxiDetails() {
	}

	public TaxiDetails(String taxiNum, String isuNum, String gpsState, String anntaState, String energyState,
			String phoneState) {
		super();
		this.taxiNum = taxiNum;
		this.isuNum = isuNum;
		this.gpsState = gpsState;
		this.anntaState = anntaState;
		this.energyState = energyState;
		this.phoneState = phoneState;
	}

	
	public TaxiDetails(String taxiNum, String isuNum, String gpsState, String anntaState, String energyState,
			String phoneState, String accState, String latState, String lonState, String meterState, String keyState,
			String oilState, String engState, String doorState, String alarmState) {
		super();
		this.taxiNum = taxiNum;
		this.isuNum = isuNum;
		this.gpsState = gpsState;
		this.anntaState = anntaState;
		this.energyState = energyState;
		this.phoneState = phoneState;
		this.accState = accState;
		this.latState = latState;
		this.lonState = lonState;
		this.meterState = meterState;
		this.keyState = keyState;
		this.oilState = oilState;
		this.engState = engState;
		this.doorState = doorState;
		this.alarmState = alarmState;
	}

	
	public String getAccState() {
		return accState;
	}

	public void setAccState(String accState) {
		this.accState = accState;
	}

	public String getLatState() {
		return latState;
	}

	public void setLatState(String latState) {
		this.latState = latState;
	}

	public String getLonState() {
		return lonState;
	}

	public void setLonState(String lonState) {
		this.lonState = lonState;
	}

	public String getMeterState() {
		return meterState;
	}

	public void setMeterState(String meterState) {
		this.meterState = meterState;
	}

	public String getKeyState() {
		return keyState;
	}

	public void setKeyState(String keyState) {
		this.keyState = keyState;
	}

	public String getOilState() {
		return oilState;
	}

	public void setOilState(String oilState) {
		this.oilState = oilState;
	}

	public String getEngState() {
		return engState;
	}

	public void setEngState(String engState) {
		this.engState = engState;
	}

	public String getDoorState() {
		return doorState;
	}

	public void setDoorState(String doorState) {
		this.doorState = doorState;
	}

	public String getAlarmState() {
		return alarmState;
	}

	public void setAlarmState(String alarmState) {
		this.alarmState = alarmState;
	}

	public String getTaxiNum() {
		return taxiNum;
	}

	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}

	public String getIsuNum() {
		return isuNum;
	}

	public void setIsuNum(String isuNum) {
		this.isuNum = isuNum;
	}

	public String getGpsState() {
		return gpsState;
	}

	public void setGpsState(String gpsState) {
		this.gpsState = gpsState;
	}

	public String getAnntaState() {
		return anntaState;
	}

	public void setAnntaState(String anntaState) {
		this.anntaState = anntaState;
	}

	public String getEnergyState() {
		return energyState;
	}

	public void setEnergyState(String energyState) {
		this.energyState = energyState;
	}

	public String getPhoneState() {
		return phoneState;
	}

	public void setPhoneState(String phoneState) {
		this.phoneState = phoneState;
	}

	@Override
	public String toString() {
		return "{taxiNum:'" + taxiNum + "', isuNum:'" + isuNum + "', gpsState:'" + gpsState + "', anntaState:'"
				+ anntaState + "', energyState:'" + energyState + "', phoneState:'" + phoneState + "', accState:'"
				+ accState + "', latState:'" + latState + "', lonState:'" + lonState + "', meterState:'" + meterState
				+ "', keyState:'" + keyState + "', oilState:'" + oilState + "', engState:'" + engState
				+ "', doorState:'" + doorState + "', alarmState:'" + alarmState + "', directionState:'" + directionState
				+ "', drName:'" + drName + "', drSex:'" + drSex + "', drAge:'" + drAge + "', drQua:'" + drQua
				+ "', drState:'" + drState + "', drPhoto:'" + drPhoto + "', drCom:'" + drCom + "', drPhone:'" + drPhone
				+ "', drNum:'" + drNum + "', drTime:'" + drTime + "}";
	}

}
