package com.hx.gps.dao.impl;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Repository;

import com.hx.gps.dao.SerialDao;
import com.hx.gps.dao.SystemDao;
import com.hx.gps.entities.tool.Alarm;
import com.hx.gps.entities.tool.OrderVector;
import com.hx.gps.entities.tool.OverSpeed;
import com.hx.gps.entities.tool.Picture;
import com.hx.gps.entities.tool.WaitOrder;
import com.hx.gps.util.DBUtil;
import com.hx.gps.util.DateUtil;
import com.hx.gps.util.FarmConstant;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.WriteResult;

@Repository
public class SystemDaoImpl implements SystemDao {

	@Override
	public boolean updateAlarm(Alarm alarm) {
		// 更新报警日志表
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ALARM);
		DBCollection coll2 = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.WAITORDER);
		BasicDBObject finds = new BasicDBObject();
		finds.put("time", DateUtil.stringToDate(alarm.getTime()));
		finds.put("dealState", alarm.getDealState());
		finds.put("alarmType", alarm.getAlarmType());
		finds.put("isuNum", alarm.getIsuNum());
		// 测试代码
		// finds=(BasicDBObject) coll.findOne(finds);
		// System.out.println(finds);
		BasicDBObject res = new BasicDBObject();
		BasicDBObject set = new BasicDBObject();
		set.put("dealState", "已处理");
		res.put("$set", set);
		// 更新待发送命令表，等待解除报警

		BasicDBObject order = new BasicDBObject();
		order.put("time", new Date());
		order.put("serialNum", SerialDao.getSerialNum());// 设置流水号
		order.put("isuNum", alarm.getIsuNum());
		order.put("orderType", "解除报警");// 类型
		order.put("people", SecurityUtils.getSubject().getPrincipal().toString());
		order.put("state", 0);
		order.put("context", "0");
		order.put("identify", "0");
		order.put("sendTime", 0);
		coll2.save(order);
		WriteResult result = coll.update(finds, res);// 默认修改一条记录便结束
		return result.getN() > 0;
		// return false;
	}

	@Override
	public List<Picture> getPicTureByIsu(String startDate, String endDate, String isuNum, String dbname) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, dbname);
		BasicDBObject finds = new BasicDBObject();
		List<Picture> list = new ArrayList<>();

		finds.put("isuNum", isuNum);
		finds.put("time", new BasicDBObject("$lte", DateUtil.stringToDate(endDate)).append("$gte",
				DateUtil.stringToDate(startDate)));

		DBCursor cursor = coll.find(finds);
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			list.add(new Picture(finds));
		}
		return list;
	}

	@Override
	public List<Picture> getPicTureByDate(String startDate, String endDate, String dbname) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, dbname);
		BasicDBObject finds = new BasicDBObject();
		List<Picture> list = new ArrayList<>();
		finds.put("time", new BasicDBObject("$lte", DateUtil.stringToDate(endDate)).append("$gte",
				DateUtil.stringToDate(startDate)));

		DBCursor cursor = coll.find(finds);
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			list.add(new Picture(finds));
		}
		return list;
	}

	@Override
	public List<Picture> getLast5OfPic(String dbname) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, dbname);
		BasicDBObject finds = new BasicDBObject();
		List<Picture> list = new ArrayList<>();
		BasicDBObject sortBy = new BasicDBObject();

		sortBy.put("time", -1);
		DBCursor cursor = coll.find().sort(sortBy).skip(0).limit(200);
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			list.add(new Picture(finds));

		}

		return list;
	}

	@Override
	public List<OrderVector> getOrderByLog(String startDate, String endDate) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ORDERLOG);
		BasicDBObject finds = new BasicDBObject();
		List<OrderVector> list = new ArrayList<>();

		finds.put("time", new BasicDBObject("$lte", DateUtil.stringToDate(endDate)).append("$gte",
				DateUtil.stringToDate(startDate)));
		DBCursor cursor = coll.find(finds);
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			list.add(new OrderVector(finds));
		}
		return list;
	}

	@Override
	public List<OrderVector> getOrderByWait(String startDate, String endDate) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.WAITORDER);
		BasicDBObject finds = new BasicDBObject();
		finds.put("time", new BasicDBObject("$lte", DateUtil.stringToDate(endDate)).append("$gte",
				DateUtil.stringToDate(startDate)));
		DBCursor cursor = coll.find(finds);
		List<OrderVector> list = new ArrayList<>();
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			list.add(new OrderVector(finds));
		}
		return list;
	}

	@Override
	public List<OrderVector> getOrderLast5OfWait() {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.WAITORDER);
		BasicDBObject finds = new BasicDBObject();
		List<OrderVector> list = new ArrayList<>();
		BasicDBObject sortBy = new BasicDBObject();

		sortBy.put("time", -1);
		DBCursor cursor = coll.find().sort(sortBy).skip(0).limit(300);
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			list.add(new OrderVector(finds));
		}
		return list;
	}

	@Override
	public List<OrderVector> getOrderLast5OfLog() {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.WAITORDER);
		BasicDBObject finds = new BasicDBObject();
		List<OrderVector> list = new ArrayList<>();
		BasicDBObject sortBy = new BasicDBObject();

		sortBy.put("time", -1);
		DBCursor cursor = coll.find().sort(sortBy).skip(0).limit(5);
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			list.add(new OrderVector(finds));
		}
		return list;
	}

	@Override
	public String getIsuVersion(String isuNUM) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "isuVersion");
		// 条件
		BasicDBObject finds = new BasicDBObject();
		finds.put("isuNum", isuNUM);
		// 结果
		BasicDBObject isuVersion = new BasicDBObject();
		String unVersion = "版本号为空";
		DBCursor cursor = coll.find(finds).limit(1);
		if (cursor.hasNext()) {
			isuVersion = (BasicDBObject) cursor.next();
			String version = isuVersion.getString("version");
			if (version.equalsIgnoreCase("") || version.equalsIgnoreCase(null) || version.equalsIgnoreCase("null")) {
				return unVersion;
			} else {
				return version;
			}
		}
		return "未知";
	}

	@Override
	public Date getIsuVersionDate(String isu) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "isuVersion");
		// 条件
		BasicDBObject finds = new BasicDBObject();
		finds.put("isuNum", isu);
		// 结果
		BasicDBObject isuVersion = new BasicDBObject();
		DBCursor cursor = coll.find(finds).limit(1);
		if (cursor.hasNext()) {
			isuVersion = (BasicDBObject) cursor.next();
			Date verDate = null;
			if (isuVersion.containsField("date")) {
				verDate = isuVersion.getDate("date");
			}
			if (verDate == null) {
				return DateUtil.stringToDate("1990-01-01 00:00:00");
			} else {
				return verDate;
			}
		} else {
			return DateUtil.stringToDate("1990-01-01 00:00:00");
		}
	}

	@Override
	public boolean saveUpdateData(String version, byte[] data, Date date) {

		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.UPDATEFILE);

		// 删除旧版本
		// BasicDBObject del = new BasicDBObject();
		// del.put("version", version);
		// DBCursor dbCursor = coll.find(del);
		// while(dbCursor.hasNext()){
		// coll.remove(dbCursor.next());
		// }

		BasicDBObject basicDBObject = new BasicDBObject();// 存入数据库的数据

		// 填充数据
		basicDBObject.put("version", version);
		basicDBObject.put("data", data);
		basicDBObject.put("date", date);

		// 存入版本数据表
		try {
			coll.insert(basicDBObject);
			System.out.println("升级数据保存成功");
			return true;
		} catch (Exception e) {
			System.out.println("升级数据保存失败");
			return false;
		}

	}

	@Override
	public boolean saveIsuToWairUpgrade(String isu, String latestVer) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.WAIRUPGRADE);
		BasicDBObject basicDBObject = new BasicDBObject();// 存入数据库的数据
		// 填充数据
		basicDBObject.put("isuNum", isu);
		basicDBObject.put("version", latestVer);
		basicDBObject.put("state", 0);
		// 存入版本数据表
		try {
			coll.insert(basicDBObject);
			System.out.println("[" + isu + "]待升级终端数据保存成功");
			return true;
		} catch (Exception e) {
			System.out.println("[" + isu + "]待升级终端数据保存失败");
			return false;
		}
	}

	@Override
	public String latestVer() {
		DBCollection findVer = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.UPDATEFILE);
		BasicDBObject sortBy = new BasicDBObject();
		String latestVersion = "null";
		sortBy.put("date", -1);
		DBCursor cursor = findVer.find().sort(sortBy).skip(0).limit(1);
		while (cursor.hasNext()) {
			BasicDBObject latest = (BasicDBObject) cursor.next();
			latestVersion = (String) latest.get("version");
		}
		return latestVersion;
	}

	@Override
	public void sendIsuInstruct(WaitOrder waitOrder) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB,FarmConstant.WAITORDER);
		BasicDBObject finds=new BasicDBObject();
		finds.put("time",waitOrder.getTime());
		finds.put("serialNum",waitOrder.getSerialNum());
		finds.put("isuNum",waitOrder.getIsuNum());
		finds.put("orderType",waitOrder.getOrderType()); 
		finds.put("identify",waitOrder.getIdentify()); 
		finds.put("people",waitOrder.getPeople());
		finds.put("context",waitOrder.getContext()); 
		finds.put("state",0);
		finds.put("sendTime",0);
		coll.save(finds);
	}
	
	//wk进入超速设置界面
		@Override
		public List<OverSpeed> getOverSpeed() {
			List<OverSpeed> list = new ArrayList<>();
			DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.OVERSPEED);
			BasicDBObject finds = (BasicDBObject) coll.findOne();
			OverSpeed overSpeed = new OverSpeed();
			Field[] fields = overSpeed.getClass().getDeclaredFields();  
		    for (Field field : fields) {  
		      String varName = field.getName();  
		      Object object = finds.get(varName);  
		      if (object != null) {  
		        try {
					BeanUtils.setProperty(overSpeed, varName, object);
				} catch (Exception e) {
					e.printStackTrace();
				} 
		      }  
		    }  
			list.add(overSpeed);
			return list;
		}

		//wk保存超速设置
		@Override
		public void saveOverSpeed(OverSpeed overSpeed) {
			DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.OVERSPEED);
			BasicDBObject delete = (BasicDBObject) coll.findOne();
			if(delete!=null){
				coll.remove(delete);
			}
			BasicDBObject finds=new BasicDBObject();
			finds.put("maxSpeed", overSpeed.getMaxSpeed());
			finds.put("duration", overSpeed.getDuration());
			coll.save(finds);
		}

}
