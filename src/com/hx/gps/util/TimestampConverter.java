package com.hx.gps.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.converter.Converter;
public class TimestampConverter implements Converter<String,Timestamp>{

	@Override
	public Timestamp convert(String arg0) {
		 
		if(StringUtils.isNotEmpty(arg0.trim())){
			Timestamp timestamp = Timestamp.valueOf(arg0);
			return timestamp;
		}
		return null;
	}
}