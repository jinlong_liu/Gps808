<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">

<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>

<!--gy 自定义css -->
<link rel="stylesheet" href="css/myhx.css" />
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">

		<div class="row">

			<!-- 点击添加按钮弹出来的form表单 -->
			<div class="modal inmodal fade" id="myModal5" tabindex="-1"
				role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<form action="saveAccidentType" class="form-horizontal" method="post"
							id="form1">
							<div class="modal-body">
								<div class="ibox-title">
									<h5>新增事故类型信息 <span style="color:red">（* 为必填项）</span></h5>
								</div>
								<div class="ibox-content">
									<div class="row">
										<div class="form-group">
											<!-- 车辆类型-->
											<label class="col-sm-3 control-label"><span style="color:red">*  </span>事故类型 ：</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" name="accidentType"
													id="accidentType1">
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-white"
											data-dismiss="modal">关闭</button>
										<button type="button" class="btn btn-primary"
											onclick="submitForm1()">保存</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>

			<!-- 点击"修改"按钮弹出来的form表单 -->
			<div class="modal inmodal fade" id="myModal1" tabindex="-1"
				role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<form action="updateAccidentType" class="form-horizontal"
							method="post" id="form2">
							<input class="form-control" id="id2" name="id" type="hidden">
							<div class="modal-body">
								<div class="ibox-title">
									<h5>修改事故类型<span style="color:red">（* 为必填项）</span></h5>
								</div>
								<div class="ibox-content">
									<div class="row">
										<div class="form-group">
											<label class="col-sm-3 control-label"><span style="color:red">*  </span>事故类型 *：</label>
											<div class="col-sm-7">
												<input class="form-control" type="text" name="accidentType"
													id="accidentType2">
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-white"
											data-dismiss="modal">关闭</button>
										<button type="button" class="btn btn-primary"
											onclick="submitForm2()">保存</button>
									</div>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>

		</div>

		<!-- 数据展示 -->
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>事故类型表</h5>
				</div>
				<!--  gy 将添加按钮与搜索框放一块 -->
				<div class="ibox-content">
					<table id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
						<thead>
						<tr>
						<td><button type="button" class="btn btn-primary btn-add1"
							data-toggle="modal" data-target="#myModal5">添&nbsp;&nbsp;&nbsp;加</button></td>
						</tr>
							<tr>
								<th>序号</th>
								<th>事故类型</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${accidenttypes}" var="accidenttypes">
								<tr>
									<td>${accidenttypes.id }</td>
									<td>${accidenttypes.accidentType }</td>

									<td><button id="btn-update" type="button"
											class="btn btn-success btn-sm" data-toggle="modal"
											data-target="#myModal1"
											onclick="update('${accidenttypes.id}','${accidenttypes.accidentType }')">修改</button>
										<button type="button" class="btn btn-danger btn-sm"
											onclick="delete1('${accidenttypes.id}','${accidenttypes.accidentType }')">删除</button></td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<!-- 删除表格 -->
			<form id="deleteForm" action="deleteAccidentType" method="post">
				<input type="hidden" id="deleteId" name="id">
			</form>
		</div>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script>
		//加载footable的功能
		
	$(document).ready(function() {
		$('#table1').DataTable({
			"pagingType" : "full_numbers"
		});
	});

		//点击"修改"按钮
		function update(id, accidentType) {
			$("#id2").val(id);
			$("#accidentType2").val(accidentType);
		};
		
		//添加车辆类型验证
		function submitForm1(){
			var type=$("#accidentType1").val();
			if(type==""||type==" "){
				swal({
					title :"事故类型不能为空",
					type : "warning"
				});
			}else{
				$("#form1").submit();
			}
		}
		
		//修改操作验证
		function submitForm2(){
			var type=$("#accidentType2").val();
			if(type==""||type==" "){
				swal({
					title :"事故类型不能为空",
					type : "warning"
				});
			}else{
				$("#form2").submit();
			}
		}

		//点击"删除"按钮
		function delete1(id, name) {
			swal({
				title : "您确定删除 " + name + " 的数据吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
				$("#deleteId").val(id);
				//提交表单
				$("#deleteForm").submit();
			});
		};
		
		//加载完之后显示执行状态
		$(document).ready(function(){
			var result='<%=request.getAttribute("result")%>';
			if (result != null && result != "null") {
				if (result.indexOf("成功") > 0) {

					swal({
						title : result,
						type : "success"
					});
				} else {
					swal({
						title : result,
						type : "error"
					});
				}
			}
		});
	</script>
</body>

</html>