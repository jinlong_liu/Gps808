package com.hx.gps.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hx.gps.entities.Company;
import com.hx.gps.entities.PublicAdvs;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.adsCustomer;
import com.hx.gps.entities.adsTime;
import com.hx.gps.entities.tool.Advertise;
import com.hx.gps.entities.tool.ComInfo2;
import com.hx.gps.entities.tool.TaxiInfo2;
import com.hx.gps.entities.tool.WaitAdvertise;
import com.hx.gps.service.AdsService;
import com.hx.gps.service.InfoService;
import com.hx.gps.service.TaxiService;

//信息管理
@Controller
public class AdsController {
	@Autowired
	private TaxiService taxiService;
	@Autowired
	private AdsService adsService;
	@Autowired
	private InfoService infoService;
	// 日志记录
	private static Log logger = LogFactory.getLog(InfoController.class);

	@RequestMapping("/enterHistory")
	public String getAllHistory(Map<String, Object> map) {
		// 获取所有的待发送记录
//		List<WaitAdvertise> advertises = adsService.getAllHistory();
//		if (advertises.size() > 0) {
//			map.put("waitAdvertises", advertises);
//		}
		return "adsHistory";
	}

	@RequestMapping("/historyTemp")
	// 获取最近的一条发送记录
	public String getTempHistory(Map<String, Object> map) {
		// 获取所有的待发送记录
		List<WaitAdvertise> advertises = adsService.getTempHistory();
		if (advertises.size() > 0) {
			map.put("waitAdvertises", advertises);
		}
		return "tempAdsHistory";
	}

	@RequestMapping({ "/deletePublicAdsById" })
	public String deletePublicAdsById(String id, HttpServletRequest request) {
		int adsId = Integer.parseInt(id);
		String state = this.adsService.deleteAdsById(adsId);
		request.setAttribute("state", state);
		logger.info("删除广告: " + id + "执行结果：" + state);
		return "forward:/intoPubSend";
	}

	@RequestMapping({ "/updatePublicAds" })
	public String updatePublicAds(PublicAdvs advs, HttpServletRequest request) {
		String state = this.adsService.updatePublicAds(advs);
		request.setAttribute("state", state);
		logger.info("更新广告: " + advs.getPlayId() + "执行结果：" + state);
		return "forward:/intoPubSend";
	}

	@RequestMapping("/findDetailsById")
	public void findDetailsById(HttpServletResponse response, WaitAdvertise advertise) {
		// 显示获取值
		List<WaitAdvertise> waitAdvertises = adsService.findDetailsById(advertise);

		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			// 记录如下
			logger.info("广告发送记录" + waitAdvertises);
			response.getWriter().print(mapper.writeValueAsString(waitAdvertises));
		} catch (IOException e) {
			logger.error("广告发送记录异常"+e.getMessage());
			e.printStackTrace();

		}

	}
	@RequestMapping("/test")
	//异步数据测试
	private void ajaxGetSellRecord(HttpServletRequest request,  
            HttpServletResponse response) throws IOException {  
        response.setCharacterEncoding("utf-8");  
        PrintWriter pw = response.getWriter();  
          
        //得到客户端传递的页码和每页记录数，并转换成int类型  
        int pageSize = Integer.parseInt(request.getParameter("pageSize"));  
        int pageNumber = Integer.parseInt(request.getParameter("pageNumber"));  
        //分页查找商品销售记录，需判断是否有带查询条件  
        List<adsTime>adsTime = null;  
        adsTime= adsService.getAllAdsTimes(); 
        ObjectMapper mapper=new ObjectMapper();
         
        //将商品销售记录转换成json字符串  
        String adsTimes =mapper.writeValueAsString(adsTime);  
        //得到总记录数  
        int total = adsTime.size();
          
        //需要返回的数据有总记录数和行数据  
        String json = "{\"total\":" + total + ",\"rows\":" + adsTimes + "}";  
        pw.print(json);  
    }  
	//根据最近记录查询详情
	@RequestMapping("/findDetailsId")
	public String findDetailsTemp(HttpServletResponse response, WaitAdvertise advertise,Map<String, Object> map) {
		// 显示获取值
		List<WaitAdvertise> waitAdvertises = adsService.findDetailsById(advertise);
		map.put("details",waitAdvertises);
		 return "forward:/historyTemp";

	}
	//根据历史记录查询详情
	@RequestMapping("/findDetailsTime")
	public String findDetailsAll(HttpServletResponse response, WaitAdvertise advertise,Map<String, Object> map) {
		// 显示获取值
		List<WaitAdvertise> waitAdvertises = adsService.findDetailsById(advertise);
		map.put("details",waitAdvertises);
		 return "forward:/getHistoryByTime";

	}
//	//test
//    @RequestMapping("/data")
//    @ResponseBody
//    public PageResult data(HttpServletRequest request,@ModelAttribute PageRequest pageRequest){
//        PageResult result = new PageResult();
//        JSONArray aaData = new JSONArray();
//        System.out.println("开始绘制数据");
//        pageRequest.setiDisplayLength(10);
//        pageRequest.setiDisplayLength(10);
//        pageRequest.setiDisplayStart(0);
//        pageRequest.setsEcho(5);
//        for (int i = pageRequest.getiDisplayStart(); i < pageRequest.getiDisplayStart()+pageRequest.getiDisplayLength(); i++) {
//            JSONArray row = new JSONArray();
//            row.add("a"+i);
//            row.add("b"+i);
//            row.add("c"+i);
//            row.add("d"+i);
//            row.add("e"+i);
//            row.add("f"+i);
//            aaData.add(row);
//        }
//        result.setAaData(aaData);
//        result.setSEcho(pageRequest.getsEcho()+1);
//        result.setITotalRecords(30);
//        result.setITotalDisplayRecords(30);
//        return result;
//    }
	@RequestMapping(value = "/hasPlayId", produces = "application/json;charset=UTF-8")
	public @ResponseBody String hasPlayId(HttpServletRequest request, String playId) {
		int id = Integer.parseInt(playId);
		String state = adsService.hasPlayId(id);
	
		boolean result = true;
		if (state.equalsIgnoreCase("id已存在")) {
			result = false;
		}
		Map<String, Boolean> map = new HashMap<>();
		map.put("valid", result);
		ObjectMapper mapper = new ObjectMapper();
		String resultString = "";
		try {
			resultString = mapper.writeValueAsString(map);
		} catch (JsonProcessingException e) {
			logger.error("【获取广告id】出现异常"+e.getMessage());
			e.printStackTrace();
		}
		return resultString;
	}
	@RequestMapping(value = "/hasTimeId", produces = "application/json;charset=UTF-8")
	public @ResponseBody String hasTimeId(HttpServletRequest request, String adsTimeId) {
		int id = Integer.parseInt(adsTimeId);
		String state = adsService.hasTimeId(id);
		logger.info("【判断时间段id】："+state);
		boolean result = true;
		if (state.equalsIgnoreCase("id已存在")) {
			result = false;
		}
		Map<String, Boolean> map = new HashMap<>();
		map.put("valid", result);
		ObjectMapper mapper = new ObjectMapper();
		String resultString = "";
		try {
			resultString = mapper.writeValueAsString(map);
		} catch (JsonProcessingException e) {
		logger.error("【判断时段id】格式转换异常"+e.getMessage());
			e.printStackTrace();
		}
		return resultString;
	}
 @RequestMapping(value = "/hasCustomerId", produces = "application/json;charset=UTF-8")
	public @ResponseBody String hasCustomerId(HttpServletRequest request, String adsCustomerId) {
		int id = Integer.parseInt(adsCustomerId);
		String state = adsService.hasCustomerId(id);
		logger.info("【判断客户id】："+state);
		boolean result = true;
		if (state.equalsIgnoreCase("当前客户id已存在")) {
			result = false;
		}
		Map<String, Boolean> map = new HashMap<>();
		map.put("valid", result);
		ObjectMapper mapper = new ObjectMapper();
		String resultString = "";
		try {
			resultString = mapper.writeValueAsString(map);
		} catch (JsonProcessingException e) {
		logger.error("【判断客户id】格式异常"+e.getMessage());
			e.printStackTrace();
		}
		return resultString;
	} 

	@RequestMapping("/addAds")
	public String addAds(HttpServletResponse response, Advertise advertise, String[] weekDay, String startYMD,
			String endYMD, String period, String immeAds, String noAdsTime, HttpServletRequest request) {
		// System.out.println("播放模式："+playMode);
		// String[]period=request.getParameterValues("period");
		// System.out.println(advertise.toString());
		// System.out.println(period);
		// System.out.println(weekDay);

		String state = adsService.addAds(advertise, weekDay, startYMD, endYMD, period, immeAds, noAdsTime);
		logger.info("保存广告: " + advertise.getPlayId() + "执行结果：" + state);
		request.setAttribute("result", state);
		return "forward:/enterAds";
	}

		// 进入新增广告界面
	@RequestMapping("/enterAds")
	public String enterAds(Map<String, Object> map) {
		// 添加用户信息
		List<adsCustomer> adsCustomers = adsService.getAllAdsCustomer();
		if (adsCustomers.size() > 0) {
			logger.info("添加用户"+adsCustomers);
			map.put("adsCustomers", adsCustomers);
		}
		// 添加预设时段信息
		List<adsTime> adsTimes = adsService.getAllAdsTimes();
		if (adsTimes.size() > 0) {
			logger.info("添加预设时段信息"+adsTimes);
			map.put("adsTime", adsTimes);
		}
		return "advs";
	}

	// 修改广告内容 （不改时段）
	@RequestMapping("/updateAds")
	public String updateAds(Advertise advertise, HttpServletRequest request) {

		String state = adsService.updateAds(advertise);
		request.setAttribute("state", state);
		logger.info("修改广告: " + advertise.getPlayId() + "执行结果：" + state);
		return "forward:/intoAdsSend";

	}

	// 删除广告
	@RequestMapping("/deleteAdsById")
	public String deleteAdsById(String id, HttpServletRequest request) {
		// 准备待删除的id
		int adsId = Integer.parseInt(id);
		String state = adsService.deleteAdsById(adsId);
		logger.info("删除广告: " + id + "执行结果：" + state);
		request.setAttribute("state", state);
		return "forward:/intoAdsSend";
	}

	// 进入下发广告界面
	/*
	 * @RequestMapping("/intoAdsSend") public String enterAdsSend(Map<String,
	 * Object> map) { logger.info("进入下发广告界面"); List<Company> companies =
	 * infoService.getAllCompany(); // 查询所有广告信息 List<Advertise> advertises =
	 * adsService.getAllAdvertises(); // 添加用户信息 List<adsCustomer> adsCustomers =
	 * adsService.getAllAdsCustomer(); if (adsCustomers.size() > 0) {
	 * map.put("adsCustomers", adsCustomers); } map.put("advertises",
	 * advertises); List<ComInfo2> comInfo2s = new ArrayList<>(); for (Company c
	 * : companies) { Set<Taxi> taxis = c.getTaxis(); Set<TaxiInfo2> taxis2 =
	 * new HashSet<>();
	 * 
	 * Iterator<Taxi> it = taxis.iterator(); while (it.hasNext()) { Taxi next =
	 * it.next(); TaxiInfo2 t2 = new TaxiInfo2(next.getId(), next.getTaxiNum());
	 * taxis2.add(t2); } comInfo2s.add(new ComInfo2(c.getId(), c.getName(),
	 * taxis2)); }
	 * 
	 * map.put("comInfo2s", comInfo2s); return "sendAds"; }
	 */
	// 进入下发广告界面
	@RequestMapping("/intoAdsSend")
	public String enterAdsSend(Map<String, Object> map,HttpServletRequest request) {
		// 查询所有广告信息
		List<Advertise> advertises = adsService.getAllAdvertises();
		// 添加用户信息
		List<adsCustomer> adsCustomers = adsService.getAllAdsCustomer();
		if (adsCustomers.size() > 0) {
			logger.info("添加用户信息"+adsCustomers);
			map.put("adsCustomers", adsCustomers);
		}
		map.put("advertises", advertises);
		// 所有公司的车辆总数
		int totality = 0;
		// --------------按权限显示车辆---------------------
		Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
		}else if (subject.isPermitted("bjcompy:get")) {
			companies = infoService.getAllCompanyBj();
		} else {
			String name = (String) subject.getPrincipal();

			// int id=(int)session.getAttribute("compyid");
			Company company = infoService.getComByUser(name);
			// System.out.println("公司id"+);
			if (company != null) {

				companies.add(company);
			}
		}
		logger.info("下发广告----");
		// --------------------------------------
		List<ComInfo2> comInfo2s = new ArrayList<>();
		for (Company c : companies) {
			Set<Taxi> taxis = c.getTaxis();
			Set<TaxiInfo2> taxis2 = new HashSet<>();

			Iterator<Taxi> it = taxis.iterator();
			while (it.hasNext()) {
				Taxi next = it.next();
				TaxiInfo2 t2 = new TaxiInfo2(next.getId(), next.getTaxiNum());
				// gy添加逻辑，根据终端号获取在线和定位状态,将在线和定位状态放在TaxiInfo2中
				taxiService.setTaxiStateByIsuNum(t2, next.getIsuNum());
				taxis2.add(t2);
			}

			comInfo2s.add(new ComInfo2(c.getId(), c.getName(), taxis2));
		}
		// 计算所有公司的车辆数
		for (ComInfo2 comInfo : comInfo2s) {
			totality += comInfo.getCount();
		}
		//如果由历史记录页面跳转，记住选中的id号。
		if(request.getParameter("playId")!=null){
			logger.info("跳转至广告管理界面,id："+request.getParameter("playId"));
			map.put("selected", request.getParameter("playId"));
		}
		map.put("comInfo2s", comInfo2s);
		map.put("totality", totality);
		return "sendAds";
	}

	// 进入下发指令界面
	@RequestMapping("/intoInsSend")
	public String enterInsSend(Map<String, Object> map,HttpServletRequest request) {
		logger.info("进入下发指令界面");
		// 所有公司的车辆总数
		int totality = 0;
		// --------------按权限显示车辆---------------------
		Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
		} else if (subject.isPermitted("bjcompy:get")) {
			companies = infoService.getAllCompanyBj();
		}else {
			String name = (String) subject.getPrincipal();

			// int id=(int)session.getAttribute("compyid");
			Company company = infoService.getComByUser(name);
			// System.out.println("公司id"+);
			if (company != null) {

				companies.add(company);
			}
		}
		logger.info("下发指令----");
		// --------------------------------------
		List<ComInfo2> comInfo2s = new ArrayList<>();
		for (Company c : companies) {
			Set<Taxi> taxis = c.getTaxis();
			Set<TaxiInfo2> taxis2 = new HashSet<>();

			Iterator<Taxi> it = taxis.iterator();
			while (it.hasNext()) {
				Taxi next = it.next();
				TaxiInfo2 t2 = new TaxiInfo2(next.getId(), next.getTaxiNum());
				// gy添加逻辑，根据终端号获取在线和定位状态,将在线和定位状态放在TaxiInfo2中
				taxiService.setTaxiStateByIsuNum(t2, next.getIsuNum());
				taxis2.add(t2);
			}

			comInfo2s.add(new ComInfo2(c.getId(), c.getName(), taxis2));
		}
		// 计算所有公司的车辆数
		for (ComInfo2 comInfo : comInfo2s) {
			totality += comInfo.getCount();
		}
		//如果由历史记录页面跳转，记住选中的id号。
		if(request.getParameter("playId")!=null){
			logger.info("跳转至下发指令界面,id："+request.getParameter("playId"));
			map.put("selected", request.getParameter("playId"));
		}
		map.put("comInfo2s", comInfo2s);
		map.put("totality", totality);
		return "sendInstruct";
	}

	// 进入下发公益广告界面
	@RequestMapping("/intoPubSend")
	public String enterPubSend(Map<String, Object> map,HttpServletRequest request) {
		logger.info("进入下发公益广告界面");
		// 所有公司的车辆总数
		int totality = 0;
		// --------------按权限显示车辆---------------------
		Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
		} else if (subject.isPermitted("bjcompy:get")) {
			companies = infoService.getAllCompanyBj();
		}else {
			String name = (String) subject.getPrincipal();
			// int id=(int)session.getAttribute("compyid");
			Company company = infoService.getComByUser(name);
			// System.out.println("公司id"+);
			if (company != null) {

				companies.add(company);
			}
		}
		logger.info("下发公益广告----");
		// --------------------------------------
		List<ComInfo2> comInfo2s = new ArrayList<>();
		for (Company c : companies) {
			Set<Taxi> taxis = c.getTaxis();
			Set<TaxiInfo2> taxis2 = new HashSet<>();

			Iterator<Taxi> it = taxis.iterator();
			while (it.hasNext()) {
				Taxi next = it.next();
				TaxiInfo2 t2 = new TaxiInfo2(next.getId(), next.getTaxiNum());
				// gy添加逻辑，根据终端号获取在线和定位状态,将在线和定位状态放在TaxiInfo2中
				taxiService.setTaxiStateByIsuNum(t2, next.getIsuNum());
				taxis2.add(t2);
			}

			comInfo2s.add(new ComInfo2(c.getId(), c.getName(), taxis2));
		}
		// 计算所有公司的车辆数
		for (ComInfo2 comInfo : comInfo2s) {
			totality += comInfo.getCount();
		}
		//如果由历史记录页面跳转，记住选中的id号。
		if(request.getParameter("playId")!=null){
			logger.info("跳转至广告管理界面,id："+request.getParameter("playId"));
			map.put("selected", request.getParameter("playId"));
		}
		map.put("comInfo2s", comInfo2s);
		map.put("totality", totality);
		// 获取所有广告信息
		logger.info("数据如下");
		List<PublicAdvs> advs = adsService.getAllPublicAds();
		if (advs.size() > 0) {
			map.put("publicAdvs", advs);
		}
		return "publicAds";
	}

	@RequestMapping("/getHistoryByTime")

	public String getHistoryByTime(Map<String, Object> map, String startDate, String endDate) {
		// 获取指定时间段的待发送记录
		List<WaitAdvertise> advertises = adsService.getHistoryByTime(startDate,endDate);
		if (advertises.size() > 0) {
			map.put("waitAdvertises", advertises);
		}
		return "adsHistory";
	}

	@RequestMapping("/SendAdsByAjax")
	public void SendAdsByAjax(HttpServletResponse response, HttpServletRequest request, String taxiNums) {
		// 开始时间、结束时间、车辆数组查询出OperateTest 数组
		// int id = Integer.parseInt(playId);
		String[] playIdArray = request.getParameterValues("playIdArray[]");
		int[] playIds = new int[playIdArray.length];
		int count = 0;
		for (String id : playIdArray) {
			int addId = Integer.parseInt(id);
			playIds[count] = addId;
			// System.out.println("选中的id" + addId);
			count++;
		}
		// System.out.println("数组长度" + playIdArray.length);
		String[] choose = taxiNums.split(",");
		List<String> taxiNum = new ArrayList<>();
		for (int i = 0; i < choose.length; i++) {
			if (choose[i].length() >= 6)
				taxiNum.add(choose[i]);
		}
		// 根据车牌号获取终端号
		String state = adsService.SendAds(playIds, taxiNum);
		logger.info("下发广告: " + state);
		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().print(mapper.writeValueAsString(state));
		} catch (IOException e) {
			logger.error("【下发广告】数据转换异常"+e.getMessage());
			e.printStackTrace();

		}
	}

	@RequestMapping("/SendInstructByAjax")
	public void SendInstructByAjax(HttpServletResponse response, HttpServletRequest request, String taxiNums,
			String instructId, String playId) {
		//
		int instruct = Integer.parseInt(instructId);
		int play = Integer.parseInt(playId);
		String[] choose = taxiNums.split(",");
		List<String> taxiNum = new ArrayList<>();
		for (int i = 0; i < choose.length; i++) {
			if (choose[i].length() >= 6)
				taxiNum.add(choose[i]);
		}
		// 根据车牌号获取终端号
		String state = adsService.SendInstruct(instruct, play, taxiNum);
		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			logger.info("下发指令: " + instruct + "状态:" + state);
			response.getWriter().print(mapper.writeValueAsString(state));
		} catch (IOException e) {
			logger.error("【下发指令】IO异常"+e.getMessage());
			e.printStackTrace();

		}
	}
	// 下发公益广告

	@RequestMapping("/SendPublicAdsByAjax")
	public void SendPublicAdsByAjax(HttpServletResponse response, HttpServletRequest request, String taxiNums) {
		String[] playIdArray = request.getParameterValues("playIdArray[]");
		int[] playIds = new int[playIdArray.length];
		int count = 0;
		for (String id : playIdArray) {
			int addId = Integer.parseInt(id);
			playIds[count] = addId;
			// System.out.println("选中的id" + addId);
			count++;
		}

		String[] choose = taxiNums.split(",");
		List<String> taxiNum = new ArrayList<>();
		for (int i = 0; i < choose.length; i++) {
			if (choose[i].length() >= 6)
				taxiNum.add(choose[i]);
		}
		// 根据车牌号获取终端号
		String state = adsService.SendPublicAds(playIds, taxiNum);
		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			logger.info("下发公益广告: " + playIds + "状态：" + state);
			response.getWriter().print(mapper.writeValueAsString(state));
		} catch (IOException e) {
			logger.error("【下发公益广告】IO异常"+e.getMessage());
			e.printStackTrace();

		}
	}

	// 新增客户信息
	@RequestMapping("/saveAdsCustomer")
	public String saveAdsCustomer(adsCustomer adsCustomer, HttpServletRequest request) {
		logger.info("save客户: " + adsCustomer);
		String state = null;
		state = adsService.saveAdsCustomer(adsCustomer);
		request.setAttribute("state", state);
		return "forward:/getAllAdsCustomer";
	}

	@RequestMapping("/addpublicAds")
	public String savePublicAds(PublicAdvs advs, HttpServletRequest request) {
		String state = adsService.savePublicAds(advs);
		request.setAttribute("state", state);
		logger.info("save公益广告: " + advs.toString());
		return "forward:/intoPubSend";
	}

	// 查询所有广告客户，进入"客户管理"界面
	@RequestMapping("/getAllAdsCustomer")
	public String getAllAdsCustomer(Map<String, Object> map) {
		// 查询所有客户
		List<adsCustomer> adsCustomers = adsService.getAllAdsCustomer();
		map.put("adsCustomers", adsCustomers);
		return "adsCustomer";
	}

	// 修改客户信息
	@RequestMapping("/updateAdsCustomer")
	public String updateAdsCustomer(adsCustomer adsCustomer, HttpServletRequest request) {
		logger.info("updateAdsCustomer: " + adsCustomer);
		String state = adsService.updateAdsCustomer(adsCustomer);
		logger.info("【更新客户信息】" + state);
		request.setAttribute("state", state);
		return "forward: /getAllAdsCustomer";
	}

	// 广告排版（时段的增删改查）
	@RequestMapping("/saveAdsControl")
	public String saveAdsControl(adsTime adsTime, HttpServletRequest request) {
		logger.info("新增广告" + adsTime);
		String state = adsService.saveAdsControl(adsTime);
		logger.info("【添加广告】执行结果:" + state);
		request.setAttribute("state", state);
		return "forward: /getAllAdsControl";
	}

	// 进入时段设置页面
	@RequestMapping("/getAllAdsControl")
	public String getAllAdsControl(Map<String, Object> map) {
		List<adsTime> adsTimes = adsService.getAllAdsTimes();
		if (adsTimes.size() > 0) {
			map.put("adsTime", adsTimes);
		}
		return "adsTimeControl";

	}

	// 条件查询
	@RequestMapping("/getAdsControl")
	public String getAdsControl(Map<String, Object> map, String adsTimeName) {
		List<adsTime> adsTimes = new ArrayList<>();
		if (adsTimeName != null && !adsTimeName.equals("")) {
			adsTimes = adsService.getAdsTimeByName(adsTimeName);
			if (adsTimes.size() > 0) {
				map.put("adsTime", adsTimes);
			}
		} else {
			adsTimes = adsService.getAllAdsTimes();
			if (adsTimes.size() > 0) {
				map.put("adsTime", adsTimes);
			}
		}
		return "adsTimeControl";

	}

	// 修改广告信息
	@RequestMapping("/updateAdsTimeControl")
	public String updateAdsTimeControl(adsTime adsTime, HttpServletRequest request) {
		logger.info("更新时段信息: " + adsTime);
		String state = adsService.updateAdsTime(adsTime);
		logger.info("修改广告信息,执行结果"+state);
		request.setAttribute("state", state);
		return "forward:/getAllAdsControl";
	}

	// 删除客户信息
	@RequestMapping("/deleteAdsCustomer")
	public String deleteAdsCustomer(String id, HttpServletRequest request) {
		// 准备待删除的id
		int adsCustomerId = Integer.parseInt(id);
		String state = adsService.deleteAdsCustomer(adsCustomerId);
		logger.info("删除客户信息，结果:"+state);
		request.setAttribute("state", state);
		return "forward:/getAllAdsCustomer";

	}

	// 删除时段信息
	@RequestMapping("/deleteAdsTimeControl")
	public String deleteAdsTimeControl(String id, HttpServletRequest request) {
		int adsTimeId = Integer.parseInt(id);
		String state = adsService.deleteAdsTime(adsTimeId);
		logger.info("删除时段信息,结果："+state);
		request.setAttribute("state", state);
		return "forward:/getAllAdsControl";
	}
}
