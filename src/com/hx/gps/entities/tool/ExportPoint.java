/**
 * 
 */
package com.hx.gps.entities.tool;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: WK
 * @Title:
 * @Description:
 *
 */
public class ExportPoint {
	// 点集合
	private List<Point> point = new ArrayList<>();
	
	public List<Point> getPoint() {
		return point;
	}
	public void setPoint(List<Point> point) {
		this.point = point;
	}
	
	@Override
	public String toString() {
		return "ExportPoint [point=" + point + "]";
	}
	
}
