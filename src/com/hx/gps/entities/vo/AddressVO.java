package com.hx.gps.entities.vo;

import com.hx.gps.entities.tool.Address;
/**
 * @Description: 实时监控列表地址显示扩展
 * @version: v1.0.0
 * @author: AN
 * @date: 2019年7月14日 下午4:35:15 
 */
public class AddressVO extends Address {

	//报警状态
	private String alarmState;

	public String getAlarmState() {
		return alarmState;
	}

	public void setAlarmState(String alarmState) {
		this.alarmState = alarmState;
	}

	@Override
	public String toString() {
		return "AddressVO [ Address=" + super.toString() + ", alarmState=" + alarmState + "]";
	}
	
}
