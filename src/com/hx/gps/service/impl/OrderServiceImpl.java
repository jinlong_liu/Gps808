package com.hx.gps.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hx.gps.dao.AddressDao;
import com.hx.gps.dao.OrderDao;
import com.hx.gps.dao.TaxiDao;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.tool.WaitOrder;
import com.hx.gps.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private TaxiDao taxiDao;
	@Autowired
	private OrderDao orderDao;
	@Autowired
	private AddressDao addressDao;
	private static Log logger = LogFactory.getLog(OrderServiceImpl.class);

	@Override
	public List<Taxi> sendArea(WaitOrder w, String[] taxiNum) {
		//下发区域开始
		logger.info("执行下发区域" + "对应车辆" + taxiNum + "对应数据" + w.toString());
		List<Taxi> strList = new ArrayList<>();
		int k = 1;
		for (int i = 0; i < taxiNum.length; i++) {
			// 首先判断是否在线
			Taxi t = taxiDao.getTaxiByTaxi(taxiNum[i]);
			if (t == null) {
				strList.add(new Taxi(taxiNum[i], "车牌号错误"));
				continue;
			} else {
				String online = null;
				// 通过实时地址表来判断车辆是否在线。
				online = addressDao.getOnlineOfAddress(t.getIsuNum());
				if (!online.equals("在线")) {
					strList.add(new Taxi(taxiNum[i], "该车辆不在线"));
					logger.info("下发区域：车辆不在线" + taxiNum[i]);
					continue;
				}
			}
			
			w.setIsuNum(t.getIsuNum());
			orderDao.addWaitOrder(w);
			logger.info("下发区域：添加命令到待发送命令表" + w.toString());
		}

		return strList;
	}

	@Override
//删除区域 默认不判断 全部删除
	public List<Taxi> deleArea(WaitOrder w, String[] taxiNum) {
		logger.info("执行删除区域命令：" + "对应车辆" + taxiNum + "对应数据" + w.toString());
		List<Taxi> strList = new ArrayList<>();
		int k = 1;
		for (int i = 0; i < taxiNum.length; i++) {
			// 首先判断是否在线
			Taxi t = taxiDao.getTaxiByTaxi(taxiNum[i]);
			if (t == null) {
				strList.add(new Taxi(taxiNum[i], "车牌号错误"));
				continue;
			} else {
				String online = null;
				// 通过实时地址表来判断车辆是否在线。
				online = addressDao.getOnlineOfAddress(t.getIsuNum());
				if (!online.equals("在线")) {
					strList.add(new Taxi(taxiNum[i], "该车辆不在线"));
					logger.info("删除区域：车辆不在线" + taxiNum[i]);
					continue;
				}
			}
			
			w.setIsuNum(t.getIsuNum());
			orderDao.addWaitOrder(w);
			logger.info("删除区域：添加命令到待发送命令表" + w.toString());
		}

		return strList;
	}
	@Override
	public List<Taxi> sendRelay(WaitOrder w, String[] taxiNum) {
		logger.info("执行下发继电器控制功能" + "对应车辆" + taxiNum + "对应命令" + w.toString());
		List<Taxi> strList = new ArrayList<>();
		int k = 1;
		for (int i = 0; i < taxiNum.length; i++) {
			// 首先判断是否在线
			Taxi t = taxiDao.getTaxiByTaxi(taxiNum[i]);
			if (t == null) {
				strList.add(new Taxi(taxiNum[i], "车牌号错误"));
				continue;
			} else {
				String online = null;

				// 通过实时地址表来判断车辆是否在线。
				online = addressDao.getOnlineOfAddress(t.getIsuNum());
				if (!online.equals("在线")) {
					strList.add(new Taxi(taxiNum[i], "该车辆不在线"));
					logger.info("下发断油断电：车辆不在线" + taxiNum[i]);

					continue;
				}
			}

			w.setIsuNum(t.getIsuNum());
			orderDao.addWaitOrder(w);
			logger.info("下发断油断电：添加命令到待发送命令表" + w.toString());
		}

		return strList;
	}
	//下发远程开关机
	@Override
	public List<Taxi> sendPower(WaitOrder w, String[] taxiNum) {
		logger.info("执行远程开关机功能" + "对应车辆" + taxiNum + "对应命令" + w.toString());
		List<Taxi> strList = new ArrayList<>();
		int k = 1;
		for (int i = 0; i < taxiNum.length; i++) {
			// 首先判断是否在线
			Taxi t = taxiDao.getTaxiByTaxi(taxiNum[i]);
			if (t == null) {
				strList.add(new Taxi(taxiNum[i], "车牌号错误"));
				continue;
			} else {
				String online = null;

				// 通过实时地址表来判断车辆是否在线。
				online = addressDao.getOnlineOfAddress(t.getIsuNum());
				if (!online.equals("在线")) {
					strList.add(new Taxi(taxiNum[i], "该车辆不在线"));
					logger.info("远程开关机：车辆不在线" + taxiNum[i]);
					continue;
				}
			}
			w.setIsuNum(t.getIsuNum());
			orderDao.addWaitOrder(w);
			logger.info("远程开关机：添加命令到待发送命令表" + w.toString());
		}

		return strList;
	}
	//远程调价
	@Override
	public List<Taxi> sendRent(WaitOrder w, String[] taxiNum) {
		logger.info("执行远程调价功能" + "对应车辆" + taxiNum + "对应命令" + w.toString());
		List<Taxi> strList = new ArrayList<>();
		int k = 1;
		for (int i = 0; i < taxiNum.length; i++) {
			// 首先判断是否在线
			Taxi t = taxiDao.getTaxiByTaxi(taxiNum[i]);
			if (t == null) {
				strList.add(new Taxi(taxiNum[i], "车牌号错误"));
				continue;
			} else {
				String online = null;

				// 通过实时地址表来判断车辆是否在线。
				online = addressDao.getOnlineOfAddress(t.getIsuNum());
				if (!online.equals("在线")) {
					strList.add(new Taxi(taxiNum[i], "该车辆不在线"));
					logger.info("远程调价：车辆不在线" + taxiNum[i]);
					continue;
				}
			}
			w.setIsuNum(t.getIsuNum());
			orderDao.addWaitOrder(w);
			logger.info("远程调价：添加命令到待发送命令表" + w.toString());
		}

		return strList;
	}
	//超速设置
	public List<Taxi> sendSpeed(WaitOrder w, String[] taxiNum) {
		logger.info("执行超速设置：" + "对应车辆" + taxiNum + "对应命令" + w.getOrderType());
		List<Taxi> strList = new ArrayList<>();
		int k = 1;
		for (int i = 0; i < taxiNum.length; i++) {
			// 首先判断是否在线
			Taxi t = taxiDao.getTaxiByTaxi(taxiNum[i]);
			if (t == null) {
				strList.add(new Taxi(taxiNum[i], "车牌号错误"));
				continue;
			} else {
				String online = null;

				// 通过实时地址表来判断车辆是否在线。
				online = addressDao.getOnlineOfAddress(t.getIsuNum());
				if (!online.equals("在线")) {
					strList.add(new Taxi(taxiNum[i], "该车辆不在线"));
					logger.info("超速设置：车辆不在线" + taxiNum[i]);
					continue;
				}
			}
			w.setIsuNum(t.getIsuNum());
			orderDao.addWaitOrder(w);
			logger.info("超速设置：添加命令到待发送命令表" + w.toString());
		}

		return strList;
		
		
	};
	//远程锁机
		public List<Taxi> sendLock(WaitOrder w, String[] taxiNum) {
			logger.info("执行远程锁机：" + "对应车辆" + taxiNum + "对应命令" + w.getOrderType());
			List<Taxi> strList = new ArrayList<>();
			int k = 1;
			for (int i = 0; i < taxiNum.length; i++) {
				// 首先判断是否在线
				Taxi t = taxiDao.getTaxiByTaxi(taxiNum[i]);
				if (t == null) {
					strList.add(new Taxi(taxiNum[i], "车牌号错误"));
					continue;
				} else {
					String online = null;

					// 通过实时地址表来判断车辆是否在线。
					online = addressDao.getOnlineOfAddress(t.getIsuNum());
					if (!online.equals("在线")) {
						strList.add(new Taxi(taxiNum[i], "该车辆不在线"));
						logger.info("远程锁机：车辆不在线" + taxiNum[i]);
						continue;
					}
				}
				w.setIsuNum(t.getIsuNum());
				orderDao.addWaitOrder(w);
				logger.info("远程锁机：添加命令到待发送命令表" + w.toString());
			}

			return strList;
			
			
		};
	
	@Override
	public List<Taxi> sendText(WaitOrder waitOrder, String[] taxiNums) {
		logger.info("执行下发文本功能" + "对应车辆" + taxiNums + "对应命令" + waitOrder.toString());
		List<Taxi> strList = new ArrayList<>();
		int k = 1;
		for (int i = 0; i < taxiNums.length; i++) {
			// 首先判断是否在线
			Taxi t = taxiDao.getTaxiByTaxi(taxiNums[i]);
			if (t == null) {
				strList.add(new Taxi(taxiNums[i], "车牌号错误"));
				continue;
			} else {
				String online = null;
				// if(FarmConstant.OnlineState==k){
				// //使用计价器实时表判断是否在线
				// online=addressDao.getOnlineOfMeter(t.getIsuNum());
				// }else{
				// //使用终端心跳判断是否在线
				// online=addressDao.getOnlineOfHeart(t.getIsuNum());
				// }
				// 通过实时地址表来判断车辆是否在线。
				online = addressDao.getOnlineOfAddress(t.getIsuNum());
				if (!online.equals("在线")) {
					strList.add(new Taxi(taxiNums[i], "该车辆不在线"));
					logger.info("下发文本：车辆不在线" + taxiNums[i]);

					continue;
				}
			}

			waitOrder.setIsuNum(t.getIsuNum());
			orderDao.addWaitOrder(waitOrder);
			logger.info("下发文本：添加命令到待发送命令表" + waitOrder.toString());
		}

		return strList;
	}
	//获取终端版本号
	@Override
public String geiVersion(WaitOrder w) {
		String state=null;
		try {
		orderDao.addWaitOrder(w);
		state="命令发送成功，等待返回结果";
	} catch (Exception e) {
		logger.error("获取终端版本号出现异常"+e.getMessage());
		state="出现异常，操作终止";
		return state;
	}
		return state;
}
}
