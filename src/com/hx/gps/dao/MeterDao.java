package com.hx.gps.dao;

import java.util.List;

import com.hx.gps.entities.tool.Meter;

/**
 * MongoDB中的营运信息表[Meter] DAO
 * 
 * @author 高杨
 *
 */
public interface MeterDao {

	/**
	 * 根据租金 车牌号 时间 获取营运信息
	 * @param startDate
	 * @param endDate
	 * @param taxiNum
	 * @param fare
	 * @param dbname
	 * @return
	 */
	List<Meter> getMetersByFare(String startDate, String endDate,
			String taxiNum, String fare, String dbname);
	
	/**
	 * 根据时间 车牌号 浮动租金获取营运信息
	 * @param floatingFare1
	 * @param floatingFare2
	 * @param startDate
	 * @param endDate
	 * @param taxiNum
	 * @return
	 */
	List<Meter> getMetersByFloatingFare(String floatingFare1,
			String floatingFare2, String startDate, String endDate,
			String taxiNum, String dbname);
}
