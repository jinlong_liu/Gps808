package com.hx.gps.service.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.hx.gps.dao.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hx.gps.entities.Graph;
import com.hx.gps.entities.Preinfo;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.tool.Address;
import com.hx.gps.entities.tool.Alarm;
import com.hx.gps.entities.tool.Alarm2;
import com.hx.gps.entities.tool.AreaAddress;
import com.hx.gps.entities.tool.Blur;
import com.hx.gps.entities.tool.ComInfo2;
import com.hx.gps.entities.tool.DatatablesQuery;
import com.hx.gps.entities.tool.DatatablesView;
import com.hx.gps.entities.tool.Gps;
import com.hx.gps.entities.tool.InfoQuery;
import com.hx.gps.entities.tool.Meter;
import com.hx.gps.entities.tool.MeterTemp;
import com.hx.gps.entities.tool.MileageVector;
import com.hx.gps.entities.tool.MyPassVol;
import com.hx.gps.entities.tool.OnlineRate;
import com.hx.gps.entities.tool.OperateTest;
import com.hx.gps.entities.tool.Point;
import com.hx.gps.entities.tool.PointsInfo;
import com.hx.gps.entities.tool.ReveInfo;
import com.hx.gps.entities.tool.TaxiDetails;
import com.hx.gps.entities.tool.TaxiInfo2;
import com.hx.gps.service.AddressService;
import com.hx.gps.util.CalendarUtil;
import com.hx.gps.util.DBUtil;
import com.hx.gps.util.DateUtil;
import com.hx.gps.util.FarmConstant;
import com.hx.gps.util.MapUtil;
import com.hx.gps.util.PositionUtil;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

@Service
public class AddressServiceImpl implements AddressService {
	@Autowired
	private MeterDao meterDao;
	@Autowired
	private InfoDao infoDao;
	@Autowired
	private AddressDao addressDao;
	@Autowired
	private TaxiDao taxiDao;
	@Autowired
	private MapDao mapDao;
	@Autowired
	private DateUtil dateUtil;
	@Autowired
	private MapUtil mapUtil;
	// 日志记录
	private static Log logger = LogFactory.getLog(AddressServiceImpl.class);

	@Override
	public TaxiDetails getDetailsByisu(String isu) {
		TaxiDetails details = new TaxiDetails();
		try {
			details = this.taxiDao.getDetailsByIsu(isu);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("查看车辆详细状态出现异常");
		}
		return details;
	}

	@Override
	// 轨迹回放，查询指定 时间段车辆
	public List<Address> findAddressByDate(String startDate, String endDate, String taxiNum) {
		logger.info("查询指定时间段轨迹:" + taxiNum);
		// 首先 根据车牌号得到设备号
		Taxi taxi = taxiDao.getTaxiByTaxi(taxiNum);
		String isuNum = null;
		// System.out.println("taxi"+taxi+" "+taxi.getIsuNum());
		if (taxi == null) {
			return new ArrayList<Address>();
		}
		isuNum = taxi.getIsuNum();
		logger.info("查询的终端号为：" + isuNum);
		List<Address> addresses = new ArrayList<>();
		// 准备转换后的集合
		List<Address> address2 = new ArrayList<>();
		// 准备临时交换点
		Address nowadd = new Address();
		Address nextadd = new Address();
		// 准备三个模拟坐标点。

		// 准备数据库历史表
		String dbname;
		// 根据时间选择相应的历史表进行查询 获得开始年份、开始月份、结束月份
		String years = startDate.substring(2, 4);
		String start = startDate.substring(5, 7);
		String end = startDate.substring(5, 7);
		if (start.equals(end)) {
			// 若在同一月份
			dbname = FarmConstant.ADDRESSHIS + years + start;

			addresses = addressDao.findAddressByDate(startDate, endDate, isuNum, dbname);
		} else {
			// 跨两个月份，且不跨年
			dbname = FarmConstant.ADDRESSHIS + years + start;
			addresses = addressDao.findAddressByDate(startDate, endDate, isuNum, dbname);
			dbname = FarmConstant.ADDRESSHIS + years + end;
			List<Address> addres = addressDao.findAddressByDate(startDate, endDate, isuNum, dbname);
			logger.info("查询到位置点如下：" + addres);
			if (addres != null) {
				addresses.addAll(addres);
			}
		}
		System.out.println("dbname = " + dbname);;
		// 进行模拟点的添加
		for (int i = 0; i <= addresses.size() - 1; i++) {
			// 添加第一个点
			if (i == addresses.size() - 1) {
				address2.add(addresses.get(i));

			} else {
				address2.add(addresses.get(i));
				// 准备进行坐标计算的两个点
				nowadd = addresses.get(i);
				nextadd = addresses.get(i + 1);
				// 生成三个模拟点，依次添加
				Address add1 = new Address();
				Address add2 = new Address();
				Address add3 = new Address();

				add2.setLatitude((nowadd.getLatitude() + nextadd.getLatitude()) / 2);
				add2.setLongitude((nowadd.getLongitude() + nextadd.getLongitude()) / 2);
				add2.setDirection(nowadd.getDirection());
				add1.setLatitude((nowadd.getLatitude() + add2.getLatitude()) / 2);
				add1.setLongitude((nowadd.getLongitude() + add2.getLongitude()) / 2);
				add1.setDirection(nowadd.getDirection());
				add3.setLatitude((add2.getLatitude() + nextadd.getLatitude()) / 2);
				add3.setLongitude((add2.getLongitude() + nextadd.getLongitude()) / 2);
				add3.setDirection(nowadd.getDirection());
				// 添加模拟点
				address2.add(add1);
				address2.add(add2);
				address2.add(add3);
			}
		}
		return addresses;
	}

	@Override
	public List<Taxi> lostAndFound(Graph graph, String startDate, String endDate, InfoQuery infoQuery) {
		// long time11 = System.currentTimeMillis();
		List<Taxi> lists = infoDao.getTaxiByQuery(infoQuery);
		System.out.println("===============================================");
		// long time12 = System.currentTimeMillis();
		// System.out.println("查询mysql车辆表所用时间："+(time12-time11));
		logger.info("查询指定时间段的车辆(定时定点查车)");
		// 首先，根据条件对所有车辆进行筛选，筛选进行的表为车辆表，该service最后返回的list也为车辆表
		// 条件：车牌号（taxiNum），车辆类型（taxiType），车辆颜色（taxiColor）,所属公司（company）
		// long time1 = System.currentTimeMillis();
		// List<Taxi> taxi1 = taxiDao.selectTaxi(taxi.getTaxiNum(),
		// taxi.getTaxiType(), taxi.getTaxiColor(),
		// taxi.getCompany().getName());
		// long time2 = System.currentTimeMillis();
		// System.err.println("查mysql车牌号" + (time2 - time1));
		List<Taxi> taxis = new ArrayList<>();// 表示最后查出的车辆
		Set<String> address = new HashSet<>();// 表示最后查出的地址集合
		// TODO 终端号转车牌号需要继续优化。
		// 进行循环
		// for (Taxi ads : taxi1) {
		// String isuNum = ads.getIsuNum();
		// 首先 根据开始时间和结束时间来决定需要查询的表
		List<DBObject> addresses = new ArrayList<>();// 所有位置信息
		String dbname;
		// 根据时间选择相应的历史表进行查询 获得开始年份、开始月份、结束月份
		String years = startDate.substring(2, 4);
		String start = startDate.substring(5, 7);
		String end = endDate.substring(5, 7);
		System.out.println("==========2==========================");
		if (start.equals(end)) {
			// 若在同一月份
			dbname = FarmConstant.ADDRESSHIS + years + start;
			addresses = addressDao.findAddressByTime(startDate, endDate, dbname);
		} else {
			// 跨两个月份，且不跨年，因为最多只能跨两个月，所以最多查询两个月
			dbname = FarmConstant.ADDRESSHIS + years + start;
			addresses = addressDao.findAddressByTime(startDate, endDate, dbname);
			dbname = FarmConstant.ADDRESSHIS + years + end;
			addresses.addAll(addressDao.findAddressByTime(startDate, endDate, dbname));
		}
		// long time3 = System.currentTimeMillis();
		// 得到一辆车的所有符合条件的位置信息,进行判断是否在标记区域内,
		/**
		 * 该处需要修改，该方法判断了所有的位置信息,在定时定点查车中，该方式执行速度太过缓慢， 测试二十万条数据，需要执行十秒钟 已修改
		 * selsctAddressOfn 为 selsctAddressOf1
		 */
		// if (selsctAddressOf1(graph, addresses)) {
		// taxis.add(ads);
		// }
		address = selsctAddressOf2(graph, addresses);
		// long time4 = System.currentTimeMillis();
		// System.err.println("查询到最终车辆结果所用时间" + (time4 - time3));
		// }
		logger.info("[定点查车]查询到符合条件的车辆：" + address);
		// System.out.println("最后结果："+taxis);
		Iterator<Taxi> it = lists.iterator();
		Set<String> list = new HashSet<String>();
		System.out.println("============3=======================");
		while (it.hasNext()) {
			Taxi taxi2 = it.next();
			if (address.contains(taxi2.getIsuNum())) {
				taxis.add(taxi2);
				for (DBObject dbObject : addresses) {
					if(dbObject.get("isuNum").equals(taxi2.getIsuNum())){
						System.out.println("===========x==========");
//						taxi2.setAddDate(Timestamp.valueOf(dbObject.get("time").toString()));
						Date time = new Date(dbObject.get("time").toString());
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						String format = simpleDateFormat.format(time);
						Timestamp timestamp = Timestamp.valueOf(format);
						taxi2.setAddDate(timestamp);
						System.out.println("===========y=========");
					}
				}
			}
		}
		System.out.println("最后遍历终端号取车牌号!" + list.toString());
		// if(address!=null){
		// System.err.println("查询到符合条件的所有车辆"+address.size());
		// for (String isuNum : address) {
		// taxis.add(taxiDao.getTaxiByIsu(isuNum));
		// }
		// }
		// long time5 = System.currentTimeMillis();
		// System.err.println("返回车牌号集合所用时间" + (time5 - time4));
		return taxis;
	}

	@Override
	public void alarmOfGather1(int state, List<Point> points) {

		List<Address> addresses = new ArrayList<>();// 表示在标识范围内的所有汽车

		// 首先得到实时表中的所有车辆信息
		List<Address> address1 = addressDao.findAllTaxi();

		if (state == 1) {
			for (Address a : addresses) {
				Point p = new Point(a.getLongitude(), a.getLatitude());
				Boolean b = mapUtil.contains(points.get(0), p, points.get(1).getLongitude());
				if (b) {
					addresses.add(a);
				}
			}

		} else if (state == 2) {
			for (Address a : address1) {
				Point p = new Point(a.getLongitude(), a.getLatitude());
				Boolean b = mapUtil.isPointInRect(points.get(0), points.get(1), p);// 1东北2西南
				if (b) {
					addresses.add(a);
				}
			}

		} else if (state == 3) {
			for (Address a : address1) {
				Point p = new Point(a.getLongitude(), a.getLatitude());
				Boolean b = mapUtil.isPointInPolygon(points, p);
				if (b) {
					addresses.add(a);
				}
			}
		}
		logger.info("在范围内的车辆个数" + addresses.size());
		// 还需要添加报警消息

	}

	@Override
	public List<Preinfo> addPreInfo(Preinfo preinfo, Graph graph) {
		Graph g = mapDao.addGraph(graph);
		// System.out.println("g"+g);
		preinfo.setGraph(g);
		// 进行判断
		if (preinfo.getRateLimit() == null)
			preinfo.setRateLimit(0);
		if (preinfo.getTimeLimit() == null)
			preinfo.setTimeLimit(0);
		if (preinfo.getInToDriver() == null)
			preinfo.setInToDriver(0);
		if (preinfo.getInToPlatform() == null)
			preinfo.setInToPlatform(0);
		if (preinfo.getOutToDriver() == null)
			preinfo.setOutToDriver(0);
		if (preinfo.getOutToPlatform() == null)
			preinfo.setOutToPlatform(0);
		if (preinfo.getGatherAlarm() == null)
			preinfo.setGatherAlarm(0);
		mapDao.addPreInfo(preinfo);
		logger.info("新增预设信息： " + preinfo.toString());
		// 然后返回所有预设信息
		return mapDao.findAllPreinfoByDate();
	}

	@Override
	public void preInfoPolling() {
		logger.info("轮询预设信息表");
		// 首先，获取预设信息表中的记录，查看是否在执行时间段内
		List<Preinfo> preinfos = mapDao.findAllPreinfoByDate();
		if (preinfos.size() != 0) {
			for (Preinfo preinfo : preinfos) {
				// 首先，查看是否有时间限制，若有则判断是否在执行时间段内
				if (preinfo.getTimeLimit() == 1) { // 判断是否在时间段内
					logger.info("收到报警信息详细" + preinfo.toString());
					if (!(DateUtil.timeInInterval(preinfo.getStartTime(), preinfo.getEndTime())))
						logger.info("报警信息不在预设时间段内");
					break;
				}
				if (preinfo.getRateLimit() == 1) { // 判断是否有超速限制
					logger.info("收到报警信息： 超速限制");
					alarmOfOverSpeed(preinfo);
				}
				if (preinfo.getOutToPlatform() == 1) { // 平台的出区域报警
					logger.info("收到报警信息： 出区域报警");
					alarmOfOut(preinfo);
				}
				if (preinfo.getInToPlatform() == 1) { // 平台的进区域报警
					logger.info("收到报警信息： 进区域报警");
					alarmOfInto(preinfo);
				}
				if (preinfo.getGatherAlarm() == 1) { // 平台的聚集报警
					// System.out.println("平台的聚集报警");
					logger.info("收到报警信息： 聚集报警");
					alarmOfGather(preinfo);
				}
			}
		}
	}

	// 进区域报警
	public void alarmOfInto(Preinfo preinfo) {
		// 首先查出区域内的所有车辆
		List<Address> address = addressDao.findAllTaxi();
		List<Address> addresult = selsctAddressOfn(preinfo.getGraph(), address);

		String date = DateUtil.countDate(FarmConstant.INTERVAL);// 得到日期字符串
		// 判断要查询的数据库
		String years = date.substring(2, 4);
		String month = date.substring(5, 7);
		String dbname = FarmConstant.ADDRESSHIS + years + month;

		for (Address a : addresult) {
			Address add = addressDao.getLastByDate(a.getIsuNum(), date, dbname);// 查询到最后一条记录
			List<Address> selectAdd = new ArrayList<>();
			selectAdd.add(add);
			selectAdd = selsctAddressOfn(preinfo.getGraph(), selectAdd);
			if (selectAdd.size() == 0) {
				// 添加报警信息
				addressDao.addAlarm("进区域报警", preinfo.getId(), add.getIsuNum());
				logger.info("添加进区域报警信息" + add.getIsuNum());
			}
		}
	}

	// 出区域报警
	public void alarmOfOut(Preinfo preinfo) {
		// 首先查出区域外的所有车辆
		List<Address> address = addressDao.findAllTaxi();
		List<Address> addresult = selsctAddressOfn(preinfo.getGraph(), address);
		List<Address> newAdd = new ArrayList<>();
		for (Address a : address) {
			if (!addresult.contains(a)) {
				newAdd.add(a);
			}
		}

		String date = DateUtil.countDate(FarmConstant.INTERVAL);// 得到日期字符串
		// 判断要查询的数据库
		String years = date.substring(2, 4);
		String month = date.substring(5, 7);
		String dbname = FarmConstant.ADDRESSHIS + years + month;

		for (Address a : addresult) {
			Address add = addressDao.getLastByDate(a.getIsuNum(), date, dbname);// 查询到最后一条记录
			List<Address> selectAdd = new ArrayList<>();
			selectAdd.add(add);
			selectAdd = selsctAddressOfn(preinfo.getGraph(), selectAdd);
			if (selectAdd.size() != 0) {
				// 添加报警信息
				addressDao.addAlarm("出区域报警", preinfo.getId(), add.getIsuNum());
				logger.info("添加出区域报警信息" + add.getIsuNum());

			}
		}
	}

	// 进出区域报警(根据终端号和时间)
	@Override
	public AreaAddress getAreaAlarm(String isuNum, String time) {
		// 查出对应的车辆位置信息。
		AreaAddress address = addressDao.getAreaAlarm(isuNum, time);
		if (address != null) {
			return address;
		} else {
			return null;
		}
	}

	// 删除预设信息
	@Override
	public void deletePreInfo(Preinfo preinfo) {
		mapDao.deletePreInfo(preinfo);
	}

	// 聚集报警
	public void alarmOfGather(Preinfo preinfo) {
		List<Address> addresult = new ArrayList<>();// 表示在标识范围内的所有汽车
		// 首先得到实时表中的所有车辆信息
		List<Address> address = addressDao.findAllTaxi();
		addresult = selsctAddressOfn(preinfo.getGraph(), address);
		if (addresult.size() > preinfo.getCarUpper()) {
			// 添加聚集报警消息
			addressDao.addAlarm("聚集报警", preinfo.getId(), null);
			logger.info("添加聚集报警信息" + preinfo.getName());

		}
	}

	// 超速报警
	public void alarmOfOverSpeed(Preinfo preinfo) {
		// 首先，获取实时表中的所有记录
		List<Address> address = addressDao.findAllTaxi();
		Graph graph = preinfo.getGraph();
		address = selsctAddressOfn(graph, address);
		// 已经得到在区域范围内的isu终端号，现在验证是否超速
		for (Address a : address) {
			if (a.getSpeed() > preinfo.getMaxSpeed()) {
				String date = DateUtil.countDate(preinfo.getContinueTime());// 得到日期字符串
				// 判断要查询的数据库
				String years = date.substring(2, 4);
				String month = date.substring(5, 7);
				String dbname = FarmConstant.ADDRESSHIS + years + month;
				Address add = addressDao.getLastByDate(a.getIsuNum(), date, dbname);// 查询到最后一条记录
				// System.out.println("最后一条记录"+add);
				if (add.getSpeed() > preinfo.getMaxSpeed()) {
					// 插入报警消息
					addressDao.addAlarm("超速报警", preinfo.getId(), a.getIsuNum());
					logger.info("添加超速报警信息" + a.getIsuNum());

				}
			}
		}
	}

	/**
	 * 判断位置信息是否在图形内，该方法判断的是传入的所有位置信息 用于聚集报警等，不可用于定时定点查车
	 *
	 * @param graph
	 * @param address
	 * @return 所有在图形内的address
	 */
	// 判断地址信息是否在图形内
	public List<Address> selsctAddressOfn(Graph graph, List<Address> address) {
		List<Address> addresult = new ArrayList<>();

		// 首先得到图形信息
		if (graph.getType() == 0) {
			// 圆形
			Point o = new Point(graph.getCircle());
			double r = graph.getRadius();
			for (Address a : address) {
				if (mapUtil.contains(o, new Point(a.getLongitude(), a.getLatitude()), r)) {
					addresult.add(a);
				}
			}
		} else if (graph.getType() == 1) {
			// 矩形
			Point p1 = new Point(graph.getOneP());
			Point p2 = new Point(graph.getTwoP());
			for (Address a : address) {
				if (mapUtil.isPointInRect(p1, p2, new Point(a.getLongitude(), a.getLatitude()))) {
					addresult.add(a);
				}
			}
		} else if (graph.getType() == 2) {
			// 首先得到多边形点的集合
			List<Point> points = new ArrayList<>();
			points.add(new Point(graph.getOneP()));
			points.add(new Point(graph.getTwoP()));
			switch (graph.getPointNum()) {
			case 3:
				points.add(new Point(graph.getThreeP()));
				break;
			case 4:
				points.add(new Point(graph.getThreeP()));
				points.add(new Point(graph.getFourP()));
				break;
			case 5:
				points.add(new Point(graph.getThreeP()));
				points.add(new Point(graph.getFourP()));
				points.add(new Point(graph.getFiveP()));
				break;
			case 6:
				points.add(new Point(graph.getThreeP()));
				points.add(new Point(graph.getFourP()));
				points.add(new Point(graph.getFiveP()));
				points.add(new Point(graph.getSixP()));
				break;
			default:
				break;
			}
			for (Address a : address) {
				if (mapUtil.isPointInPolygon(points, new Point(a.getLongitude(), a.getLatitude()))) {
					addresult.add(a);
				}
			}
		}
		return addresult;
	}

	/**
	 * 判断位置信息是否在图形内，该方法判断的是传入的所有位置信息中是否包含在图形内的位置 用于定时定点查车
	 *
	 * @param graph
	 * @param address
	 * @return boolean
	 */
	// 判断地址信息是否在图形内
	public boolean selsctAddressOf1(Graph graph, List<Address> address) {
		long time1 = System.currentTimeMillis();
		// 首先得到图形信息
		if (graph.getType() == 0) {
			// 圆形
			Point o = new Point(graph.getCircle());
			Gps gps = PositionUtil.bd09_To_Gps84(o.getLatitude(), o.getLongitude());
			o.setLatitude(gps.getWgLat());
			o.setLongitude(gps.getWgLon());
			// 转换后
			double r = graph.getRadius();
			for (Address a : address) {
				if (mapUtil.contains(o, new Point(a.getLongitude(), a.getLatitude()), r)) {
					return true;
				}
			}
			long time2 = System.currentTimeMillis();
			System.err.println("判断地址信息是否在图形内时间占用" + (time2 - time1));
		} else if (graph.getType() == 1) {
			// 矩形
			Point p1 = new Point(graph.getOneP());
			Gps gps = PositionUtil.bd09_To_Gps84(p1.getLatitude(), p1.getLongitude());
			p1.setLatitude(gps.getWgLat());
			p1.setLongitude(gps.getWgLon());
			Point p2 = new Point(graph.getTwoP());
			gps = PositionUtil.bd09_To_Gps84(p2.getLatitude(), p2.getLongitude());
			p2.setLatitude(gps.getWgLat());
			p2.setLongitude(gps.getWgLon());
			for (Address a : address) {
				if (mapUtil.isPointInRect(p1, p2, new Point(a.getLongitude(), a.getLatitude()))) {
					return true;
				}
			}
		} else if (graph.getType() == 2) {
			// 首先得到多边形点的集合
			List<Point> points = new ArrayList<>();
			Point p1 = new Point(graph.getOneP());
			Point p2 = new Point(graph.getTwoP());

			Gps gps = PositionUtil.bd09_To_Gps84(p1.getLatitude(), p1.getLongitude());
			p1.setLatitude(gps.getWgLat());
			p1.setLongitude(gps.getWgLon());
			gps = PositionUtil.bd09_To_Gps84(p2.getLatitude(), p2.getLongitude());

			p2.setLatitude(gps.getWgLat());
			p2.setLongitude(gps.getWgLon());

			points.add(p1);
			points.add(p2);

			switch (graph.getPointNum()) {
			case 3:
				Point p3 = new Point(graph.getThreeP());
				gps = PositionUtil.bd09_To_Gps84(p3.getLatitude(), p3.getLongitude());
				p3.setLatitude(gps.getWgLat());
				p3.setLongitude(gps.getWgLon());
				points.add(p3);
				break;
			case 4:
				Point p33 = new Point(graph.getThreeP());
				gps = PositionUtil.bd09_To_Gps84(p33.getLatitude(), p33.getLongitude());
				p33.setLatitude(gps.getWgLat());
				p33.setLongitude(gps.getWgLon());

				Point p4 = new Point(graph.getFourP());
				gps = PositionUtil.bd09_To_Gps84(p4.getLatitude(), p4.getLongitude());
				p4.setLatitude(gps.getWgLat());
				p4.setLongitude(gps.getWgLon());

				points.add(p33);
				points.add(p4);
				break;
			case 5:
				Point p53 = new Point(graph.getThreeP());
				gps = PositionUtil.bd09_To_Gps84(p53.getLatitude(), p53.getLongitude());
				p53.setLatitude(gps.getWgLat());
				p53.setLongitude(gps.getWgLon());

				Point p54 = new Point(graph.getFourP());
				gps = PositionUtil.bd09_To_Gps84(p54.getLatitude(), p54.getLongitude());
				p54.setLatitude(gps.getWgLat());
				p54.setLongitude(gps.getWgLon());

				Point p55 = new Point(graph.getFiveP());
				gps = PositionUtil.bd09_To_Gps84(p55.getLatitude(), p55.getLongitude());
				p55.setLatitude(gps.getWgLat());
				p55.setLongitude(gps.getWgLon());

				points.add(p53);
				points.add(p54);
				points.add(p55);
				break;
			case 6:
				Point p63 = new Point(graph.getThreeP());
				gps = PositionUtil.bd09_To_Gps84(p63.getLatitude(), p63.getLongitude());
				p63.setLatitude(gps.getWgLat());
				p63.setLongitude(gps.getWgLon());

				Point p64 = new Point(graph.getFourP());
				gps = PositionUtil.bd09_To_Gps84(p64.getLatitude(), p64.getLongitude());
				p64.setLatitude(gps.getWgLat());
				p64.setLongitude(gps.getWgLon());

				Point p65 = new Point(graph.getFiveP());
				gps = PositionUtil.bd09_To_Gps84(p65.getLatitude(), p65.getLongitude());
				p65.setLatitude(gps.getWgLat());
				p65.setLongitude(gps.getWgLon());

				Point p66 = new Point(graph.getSixP());
				gps = PositionUtil.bd09_To_Gps84(p66.getLatitude(), p66.getLongitude());
				p66.setLatitude(gps.getWgLat());
				p66.setLongitude(gps.getWgLon());

				points.add(p63);
				points.add(p64);
				points.add(p65);
				points.add(p66);
				break;
			default:
				break;
			}
			for (Address a : address) {
				if (mapUtil.isPointInPolygon(points, new Point(a.getLongitude(), a.getLatitude()))) {
					return true;
				}
			}
		} else if (graph.getType() == 6) {
			// 圆形 ,半径
			double r = graph.getRadius();
			Gps gps = null;
			// 判断一共几个点
			switch (graph.getPointNum()) {
			// 然后判断途经点的方法。
			// 如果一个点，半径自定义为方圆200米。 圆形范围
			// 多个点的话，循环判断，满足这一条件在执行下一条件，都用圆形。
			// 进化版本， 2017年4月26日 15:24:08改动
			case 1:
				Point p1 = new Point(graph.getOneP());
				gps = PositionUtil.bd09_To_Gps84(p1.getLatitude(), p1.getLongitude());
				p1.setLatitude(gps.getWgLat());
				p1.setLongitude(gps.getWgLon());
				for (Address a : address) {
					if (mapUtil.contains(p1, new Point(a.getLongitude(), a.getLatitude()), r)) {
						return true;
					}
				}
				break;
			case 2:
				Point p21 = new Point(graph.getOneP());
				gps = PositionUtil.bd09_To_Gps84(p21.getLatitude(), p21.getLongitude());
				p21.setLatitude(gps.getWgLat());
				p21.setLongitude(gps.getWgLon());
				Point p22 = new Point(graph.getTwoP());
				gps = PositionUtil.bd09_To_Gps84(p22.getLatitude(), p22.getLongitude());
				p22.setLatitude(gps.getWgLat());
				p22.setLongitude(gps.getWgLon());
				for (Address a : address) {
					if (mapUtil.contains(p21, new Point(a.getLongitude(), a.getLatitude()), r)) {
						for (Address aa : address) {
							if (mapUtil.contains(p22, new Point(aa.getLongitude(), aa.getLatitude()), r)) {
								return true;
							}
						}
					}
				}
				break;
			case 3:
				Point oo1 = new Point(graph.getOneP());
				gps = PositionUtil.bd09_To_Gps84(oo1.getLatitude(), oo1.getLongitude());
				oo1.setLatitude(gps.getWgLat());
				oo1.setLongitude(gps.getWgLon());

				Point oo2 = new Point(graph.getTwoP());
				gps = PositionUtil.bd09_To_Gps84(oo2.getLatitude(), oo2.getLongitude());
				oo2.setLatitude(gps.getWgLat());
				oo2.setLongitude(gps.getWgLon());

				Point oo3 = new Point(graph.getThreeP());
				gps = PositionUtil.bd09_To_Gps84(oo3.getLatitude(), oo3.getLongitude());
				oo3.setLatitude(gps.getWgLat());
				oo3.setLongitude(gps.getWgLon());

				for (Address a : address) {
					if (mapUtil.contains(oo1, new Point(a.getLongitude(), a.getLatitude()), r)) {
						for (Address aa : address) {
							if (mapUtil.contains(oo2, new Point(aa.getLongitude(), aa.getLatitude()), r)) {
								for (Address aaa : address) {
									if (mapUtil.contains(oo3, new Point(aaa.getLongitude(), aaa.getLatitude()), r)) {
										return true;
									}
								}
							}
						}
					}
				}
				break;
			case 4:
				Point c1 = new Point(graph.getOneP());
				gps = PositionUtil.bd09_To_Gps84(c1.getLatitude(), c1.getLongitude());
				c1.setLatitude(gps.getWgLat());
				c1.setLongitude(gps.getWgLon());

				Point c2 = new Point(graph.getTwoP());
				gps = PositionUtil.bd09_To_Gps84(c2.getLatitude(), c2.getLongitude());
				c2.setLatitude(gps.getWgLat());
				c2.setLongitude(gps.getWgLon());

				Point c3 = new Point(graph.getThreeP());
				gps = PositionUtil.bd09_To_Gps84(c3.getLatitude(), c3.getLongitude());
				c3.setLatitude(gps.getWgLat());
				c3.setLongitude(gps.getWgLon());

				Point c4 = new Point(graph.getFourP());
				gps = PositionUtil.bd09_To_Gps84(c4.getLatitude(), c4.getLongitude());
				c4.setLatitude(gps.getWgLat());
				c4.setLongitude(gps.getWgLon());

				for (Address a : address) {
					if (mapUtil.contains(c1, new Point(a.getLongitude(), a.getLatitude()), r)) {
						for (Address aa : address) {
							if (mapUtil.contains(c2, new Point(aa.getLongitude(), aa.getLatitude()), r)) {
								for (Address aaa : address) {
									if (mapUtil.contains(c3, new Point(aaa.getLongitude(), aaa.getLatitude()), r)) {
										for (Address aaaa : address) {
											if (mapUtil.contains(c4, new Point(aaaa.getLongitude(), aaaa.getLatitude()),
													r)) {
												return true;
											}
										}
									}
								}
							}
						}
					}
				}
				break;
			case 5:
				Point d1 = new Point(graph.getOneP());
				gps = PositionUtil.bd09_To_Gps84(d1.getLatitude(), d1.getLongitude());
				d1.setLatitude(gps.getWgLat());
				d1.setLongitude(gps.getWgLon());

				Point d2 = new Point(graph.getTwoP());
				gps = PositionUtil.bd09_To_Gps84(d2.getLatitude(), d2.getLongitude());
				d2.setLatitude(gps.getWgLat());
				d2.setLongitude(gps.getWgLon());

				Point d3 = new Point(graph.getThreeP());
				gps = PositionUtil.bd09_To_Gps84(d3.getLatitude(), d3.getLongitude());
				d3.setLatitude(gps.getWgLat());
				d3.setLongitude(gps.getWgLon());

				Point d4 = new Point(graph.getFourP());
				gps = PositionUtil.bd09_To_Gps84(d4.getLatitude(), d4.getLongitude());
				d4.setLatitude(gps.getWgLat());
				d4.setLongitude(gps.getWgLon());

				Point d5 = new Point(graph.getFiveP());
				gps = PositionUtil.bd09_To_Gps84(d5.getLatitude(), d5.getLongitude());
				d5.setLatitude(gps.getWgLat());
				d5.setLongitude(gps.getWgLon());

				for (Address a : address) {
					if (mapUtil.contains(d1, new Point(a.getLongitude(), a.getLatitude()), r)) {
						for (Address b : address) {
							if (mapUtil.contains(d2, new Point(b.getLongitude(), b.getLatitude()), r)) {
								for (Address c : address) {
									if (mapUtil.contains(d3, new Point(c.getLongitude(), c.getLatitude()), r)) {
										for (Address d : address) {
											if (mapUtil.contains(d4, new Point(d.getLongitude(), d.getLatitude()), r)) {
												for (Address e : address) {
													if (mapUtil.contains(d5,
															new Point(e.getLongitude(), e.getLatitude()), r)) {
														return true;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				break;
			case 6:
				Point e1 = new Point(graph.getOneP());
				gps = PositionUtil.bd09_To_Gps84(e1.getLatitude(), e1.getLongitude());
				e1.setLatitude(gps.getWgLat());
				e1.setLongitude(gps.getWgLon());

				Point e2 = new Point(graph.getTwoP());
				gps = PositionUtil.bd09_To_Gps84(e2.getLatitude(), e2.getLongitude());
				e2.setLatitude(gps.getWgLat());
				e2.setLongitude(gps.getWgLon());

				Point e3 = new Point(graph.getThreeP());
				gps = PositionUtil.bd09_To_Gps84(e3.getLatitude(), e3.getLongitude());
				e3.setLatitude(gps.getWgLat());
				e3.setLongitude(gps.getWgLon());

				Point e4 = new Point(graph.getFourP());
				gps = PositionUtil.bd09_To_Gps84(e4.getLatitude(), e4.getLongitude());
				e4.setLatitude(gps.getWgLat());
				e4.setLongitude(gps.getWgLon());

				Point e5 = new Point(graph.getFiveP());
				gps = PositionUtil.bd09_To_Gps84(e5.getLatitude(), e5.getLongitude());
				e5.setLatitude(gps.getWgLat());
				e5.setLongitude(gps.getWgLon());

				Point e6 = new Point(graph.getSixP());
				gps = PositionUtil.bd09_To_Gps84(e6.getLatitude(), e6.getLongitude());
				e6.setLatitude(gps.getWgLat());
				e6.setLongitude(gps.getWgLon());
				for (Address a : address) {
					if (mapUtil.contains(e1, new Point(a.getLongitude(), a.getLatitude()), r)) {
						for (Address b : address) {
							if (mapUtil.contains(e2, new Point(b.getLongitude(), b.getLatitude()), r)) {
								for (Address c : address) {
									if (mapUtil.contains(e3, new Point(c.getLongitude(), c.getLatitude()), r)) {
										for (Address d : address) {
											if (mapUtil.contains(e4, new Point(d.getLongitude(), d.getLatitude()), r)) {
												for (Address e : address) {
													if (mapUtil.contains(e5,
															new Point(e.getLongitude(), e.getLatitude()), r)) {
														for (Address f : address) {
															if (mapUtil.contains(e6,
																	new Point(f.getLongitude(), f.getLatitude()), r)) {
																return true;
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				break;

			default:
				break;
			}

		}

		return false;
	}

	// 返回条件下所有点的集合
	// 判断地址信息是否在图形内
	public Set<String> selsctAddressOf2(Graph graph, List<DBObject> address) {
		long time1 = System.currentTimeMillis();
		Set<String> results = new HashSet<>();
		// 首先得到图形信息
		if (graph.getType() == 0) {
			// 圆形
			Point o = new Point(graph.getCircle());
			Gps gps = PositionUtil.bd09_To_Gps84(o.getLatitude(), o.getLongitude());
			o.setLatitude(gps.getWgLat());
			o.setLongitude(gps.getWgLon());
			// 转换后
			double r = graph.getRadius();
			for (DBObject a : address) {
				if (mapUtil.contains(o, new Point(Double.parseDouble(a.get("longitude").toString()),
						Double.parseDouble(a.get("latitude").toString())), r)) {
					results.add(a.get("isuNum").toString());
				}
			}
			long time2 = System.currentTimeMillis();
			System.err.println("时间占用" + (time2 - time1));
			return results;
		} else if (graph.getType() == 1) {
			// 矩形
			results.clear();
			Point p1 = new Point(graph.getOneP());
			Gps gps = PositionUtil.bd09_To_Gps84(p1.getLatitude(), p1.getLongitude());
			p1.setLatitude(gps.getWgLat());
			p1.setLongitude(gps.getWgLon());
			Point p2 = new Point(graph.getTwoP());
			gps = PositionUtil.bd09_To_Gps84(p2.getLatitude(), p2.getLongitude());
			p2.setLatitude(gps.getWgLat());
			p2.setLongitude(gps.getWgLon());
			for (DBObject a : address) {
				if (mapUtil.isPointInRect(p1, p2, new Point(Double.parseDouble(a.get("longitude").toString()),
						Double.parseDouble(a.get("latitude").toString())))) {
					results.add(a.get("isuNum").toString());
				}
			}
			return results;
		} else if (graph.getType() == 2) {
			results.clear();
			// 首先得到多边形点的集合
			List<Point> points = new ArrayList<>();
			Point p1 = new Point(graph.getOneP());
			Point p2 = new Point(graph.getTwoP());

			Gps gps = PositionUtil.bd09_To_Gps84(p1.getLatitude(), p1.getLongitude());
			p1.setLatitude(gps.getWgLat());
			p1.setLongitude(gps.getWgLon());
			gps = PositionUtil.bd09_To_Gps84(p2.getLatitude(), p2.getLongitude());

			p2.setLatitude(gps.getWgLat());
			p2.setLongitude(gps.getWgLon());

			points.add(p1);
			points.add(p2);

			switch (graph.getPointNum()) {
			case 3:
				Point p3 = new Point(graph.getThreeP());
				gps = PositionUtil.bd09_To_Gps84(p3.getLatitude(), p3.getLongitude());
				p3.setLatitude(gps.getWgLat());
				p3.setLongitude(gps.getWgLon());
				points.add(p3);
				break;
			case 4:
				Point p33 = new Point(graph.getThreeP());
				gps = PositionUtil.bd09_To_Gps84(p33.getLatitude(), p33.getLongitude());
				p33.setLatitude(gps.getWgLat());
				p33.setLongitude(gps.getWgLon());

				Point p4 = new Point(graph.getFourP());
				gps = PositionUtil.bd09_To_Gps84(p4.getLatitude(), p4.getLongitude());
				p4.setLatitude(gps.getWgLat());
				p4.setLongitude(gps.getWgLon());

				points.add(p33);
				points.add(p4);
				break;
			case 5:
				Point p53 = new Point(graph.getThreeP());
				gps = PositionUtil.bd09_To_Gps84(p53.getLatitude(), p53.getLongitude());
				p53.setLatitude(gps.getWgLat());
				p53.setLongitude(gps.getWgLon());

				Point p54 = new Point(graph.getFourP());
				gps = PositionUtil.bd09_To_Gps84(p54.getLatitude(), p54.getLongitude());
				p54.setLatitude(gps.getWgLat());
				p54.setLongitude(gps.getWgLon());

				Point p55 = new Point(graph.getFiveP());
				gps = PositionUtil.bd09_To_Gps84(p55.getLatitude(), p55.getLongitude());
				p55.setLatitude(gps.getWgLat());
				p55.setLongitude(gps.getWgLon());

				points.add(p53);
				points.add(p54);
				points.add(p55);
				break;
			case 6:
				Point p63 = new Point(graph.getThreeP());
				gps = PositionUtil.bd09_To_Gps84(p63.getLatitude(), p63.getLongitude());
				p63.setLatitude(gps.getWgLat());
				p63.setLongitude(gps.getWgLon());

				Point p64 = new Point(graph.getFourP());
				gps = PositionUtil.bd09_To_Gps84(p64.getLatitude(), p64.getLongitude());
				p64.setLatitude(gps.getWgLat());
				p64.setLongitude(gps.getWgLon());

				Point p65 = new Point(graph.getFiveP());
				gps = PositionUtil.bd09_To_Gps84(p65.getLatitude(), p65.getLongitude());
				p65.setLatitude(gps.getWgLat());
				p65.setLongitude(gps.getWgLon());

				Point p66 = new Point(graph.getSixP());
				gps = PositionUtil.bd09_To_Gps84(p66.getLatitude(), p66.getLongitude());
				p66.setLatitude(gps.getWgLat());
				p66.setLongitude(gps.getWgLon());

				points.add(p63);
				points.add(p64);
				points.add(p65);
				points.add(p66);
				break;
			default:
				break;
			}
			for (DBObject a : address) {
				if (mapUtil.isPointInPolygon(points, new Point(Double.parseDouble(a.get("longitude").toString()),
						Double.parseDouble(a.get("latitude").toString())))) {
					results.add(a.get("isuNum").toString());
				}
			}
			return results;
		} else if (graph.getType() == 6) {
			results.clear();
			// 圆形 ,半径
			double r = graph.getRadius();
			Gps gps = null;
			// 判断一共几个点
			switch (graph.getPointNum()) {
			// 然后判断途经点的方法。
			// 如果一个点，半径自定义为方圆200米。 圆形范围
			// 多个点的话，循环判断，满足这一条件在执行下一条件，都用圆形。
			// 进化版本， 2017年4月26日 15:24:08改动
			case 1:
				Point p1 = new Point(graph.getOneP());
				gps = PositionUtil.bd09_To_Gps84(p1.getLatitude(), p1.getLongitude());
				p1.setLatitude(gps.getWgLat());
				p1.setLongitude(gps.getWgLon());
				for (DBObject a : address) {
					if (mapUtil.contains(p1, new Point(Double.parseDouble(a.get("longitude").toString()),
							Double.parseDouble(a.get("latitude").toString())), r)) {
						results.add(a.get("isuNum").toString());
					}
				}
				break;
			case 2:
				Point p21 = new Point(graph.getOneP());
				gps = PositionUtil.bd09_To_Gps84(p21.getLatitude(), p21.getLongitude());
				p21.setLatitude(gps.getWgLat());
				p21.setLongitude(gps.getWgLon());
				Point p22 = new Point(graph.getTwoP());
				gps = PositionUtil.bd09_To_Gps84(p22.getLatitude(), p22.getLongitude());
				p22.setLatitude(gps.getWgLat());
				p22.setLongitude(gps.getWgLon());
				for (DBObject a : address) {
					if (mapUtil.contains(p21, new Point(Double.parseDouble(a.get("longitude").toString()),
							Double.parseDouble(a.get("latitude").toString())), r)) {
						for (DBObject aa : address) {
							if (mapUtil.contains(p22, new Point(Double.parseDouble(aa.get("longitude").toString()),
									Double.parseDouble(aa.get("latitude").toString())), r)) {
								results.add(aa.get("isuNum").toString());
							}
						}
					}
				}
				break;
			case 3:
				Point oo1 = new Point(graph.getOneP());
				gps = PositionUtil.bd09_To_Gps84(oo1.getLatitude(), oo1.getLongitude());
				oo1.setLatitude(gps.getWgLat());
				oo1.setLongitude(gps.getWgLon());

				Point oo2 = new Point(graph.getTwoP());
				gps = PositionUtil.bd09_To_Gps84(oo2.getLatitude(), oo2.getLongitude());
				oo2.setLatitude(gps.getWgLat());
				oo2.setLongitude(gps.getWgLon());

				Point oo3 = new Point(graph.getThreeP());
				gps = PositionUtil.bd09_To_Gps84(oo3.getLatitude(), oo3.getLongitude());
				oo3.setLatitude(gps.getWgLat());
				oo3.setLongitude(gps.getWgLon());

				for (DBObject a : address) {
					if (mapUtil.contains(oo1, new Point(Double.parseDouble(a.get("longitude").toString()),
							Double.parseDouble(a.get("latitude").toString())), r)) {
						for (DBObject aa : address) {
							if (mapUtil.contains(oo2, new Point(Double.parseDouble(aa.get("longitude").toString()),
									Double.parseDouble(aa.get("latitude").toString())), r)) {
								for (DBObject aaa : address) {
									if (mapUtil.contains(oo3,
											new Point(Double.parseDouble(aaa.get("longitude").toString()),
													Double.parseDouble(aaa.get("latitude").toString())),
											r)) {
										results.add(aaa.get("isuNum").toString());
									}
								}
							}
						}
					}
				}
				break;
			case 4:
				Point c1 = new Point(graph.getOneP());
				gps = PositionUtil.bd09_To_Gps84(c1.getLatitude(), c1.getLongitude());
				c1.setLatitude(gps.getWgLat());
				c1.setLongitude(gps.getWgLon());

				Point c2 = new Point(graph.getTwoP());
				gps = PositionUtil.bd09_To_Gps84(c2.getLatitude(), c2.getLongitude());
				c2.setLatitude(gps.getWgLat());
				c2.setLongitude(gps.getWgLon());

				Point c3 = new Point(graph.getThreeP());
				gps = PositionUtil.bd09_To_Gps84(c3.getLatitude(), c3.getLongitude());
				c3.setLatitude(gps.getWgLat());
				c3.setLongitude(gps.getWgLon());

				Point c4 = new Point(graph.getFourP());
				gps = PositionUtil.bd09_To_Gps84(c4.getLatitude(), c4.getLongitude());
				c4.setLatitude(gps.getWgLat());
				c4.setLongitude(gps.getWgLon());

				for (DBObject a : address) {
					if (mapUtil.contains(c1, new Point(Double.parseDouble(a.get("longitude").toString()),
							Double.parseDouble(a.get("latitude").toString())), r)) {
						for (DBObject aa : address) {
							if (mapUtil.contains(c2, new Point(Double.parseDouble(aa.get("longitude").toString()),
									Double.parseDouble(aa.get("latitude").toString())), r)) {
								for (DBObject aaa : address) {
									if (mapUtil.contains(c3,
											new Point(Double.parseDouble(aaa.get("longitude").toString()),
													Double.parseDouble(aaa.get("latitude").toString())),
											r)) {
										for (DBObject aaaa : address) {
											if (mapUtil.contains(c4,
													new Point(Double.parseDouble(aaaa.get("longitude").toString()),
															Double.parseDouble(aaaa.get("latitude").toString())),
													r)) {
												results.add(aaaa.get("isuNum").toString());
											}
										}
									}
								}
							}
						}
					}
				}
				break;
			case 5:
				Point d1 = new Point(graph.getOneP());
				gps = PositionUtil.bd09_To_Gps84(d1.getLatitude(), d1.getLongitude());
				d1.setLatitude(gps.getWgLat());
				d1.setLongitude(gps.getWgLon());

				Point d2 = new Point(graph.getTwoP());
				gps = PositionUtil.bd09_To_Gps84(d2.getLatitude(), d2.getLongitude());
				d2.setLatitude(gps.getWgLat());
				d2.setLongitude(gps.getWgLon());

				Point d3 = new Point(graph.getThreeP());
				gps = PositionUtil.bd09_To_Gps84(d3.getLatitude(), d3.getLongitude());
				d3.setLatitude(gps.getWgLat());
				d3.setLongitude(gps.getWgLon());

				Point d4 = new Point(graph.getFourP());
				gps = PositionUtil.bd09_To_Gps84(d4.getLatitude(), d4.getLongitude());
				d4.setLatitude(gps.getWgLat());
				d4.setLongitude(gps.getWgLon());

				Point d5 = new Point(graph.getFiveP());
				gps = PositionUtil.bd09_To_Gps84(d5.getLatitude(), d5.getLongitude());
				d5.setLatitude(gps.getWgLat());
				d5.setLongitude(gps.getWgLon());

				for (DBObject a : address) {
					if (mapUtil.contains(d1, new Point(Double.parseDouble(a.get("longitude").toString()),
							Double.parseDouble(a.get("latitude").toString())), r)) {
						for (DBObject b : address) {
							if (mapUtil.contains(d2, new Point(Double.parseDouble(b.get("longitude").toString()),
									Double.parseDouble(b.get("latitude").toString())), r)) {
								for (DBObject c : address) {
									if (mapUtil.contains(d3,
											new Point(Double.parseDouble(c.get("longitude").toString()),
													Double.parseDouble(c.get("latitude").toString())),
											r)) {
										for (DBObject d : address) {
											if (mapUtil
													.contains(d4,
															new Point(Double.parseDouble(d.get("longitude").toString()),
																	Double.parseDouble(d.get("latitude").toString())),
															r)) {
												for (DBObject e : address) {
													if (mapUtil.contains(d5,
															new Point(Double.parseDouble(e.get("longitude").toString()),
																	Double.parseDouble(e.get("latitude").toString())),
															r)) {
														results.add(e.get("isuNum").toString());
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				break;
			case 6:
				Point e1 = new Point(graph.getOneP());
				gps = PositionUtil.bd09_To_Gps84(e1.getLatitude(), e1.getLongitude());
				e1.setLatitude(gps.getWgLat());
				e1.setLongitude(gps.getWgLon());

				Point e2 = new Point(graph.getTwoP());
				gps = PositionUtil.bd09_To_Gps84(e2.getLatitude(), e2.getLongitude());
				e2.setLatitude(gps.getWgLat());
				e2.setLongitude(gps.getWgLon());

				Point e3 = new Point(graph.getThreeP());
				gps = PositionUtil.bd09_To_Gps84(e3.getLatitude(), e3.getLongitude());
				e3.setLatitude(gps.getWgLat());
				e3.setLongitude(gps.getWgLon());

				Point e4 = new Point(graph.getFourP());
				gps = PositionUtil.bd09_To_Gps84(e4.getLatitude(), e4.getLongitude());
				e4.setLatitude(gps.getWgLat());
				e4.setLongitude(gps.getWgLon());

				Point e5 = new Point(graph.getFiveP());
				gps = PositionUtil.bd09_To_Gps84(e5.getLatitude(), e5.getLongitude());
				e5.setLatitude(gps.getWgLat());
				e5.setLongitude(gps.getWgLon());

				Point e6 = new Point(graph.getSixP());
				gps = PositionUtil.bd09_To_Gps84(e6.getLatitude(), e6.getLongitude());
				e6.setLatitude(gps.getWgLat());
				e6.setLongitude(gps.getWgLon());
				for (DBObject a : address) {
					if (mapUtil.contains(e1, new Point(Double.parseDouble(a.get("longitude").toString()),
							Double.parseDouble(a.get("latitude").toString())), r)) {
						for (DBObject b : address) {
							if (mapUtil.contains(e2, new Point(Double.parseDouble(b.get("longitude").toString()),
									Double.parseDouble(b.get("latitude").toString())), r)) {
								for (DBObject c : address) {
									if (mapUtil.contains(e3,
											new Point(Double.parseDouble(c.get("longitude").toString()),
													Double.parseDouble(c.get("latitude").toString())),
											r)) {
										for (DBObject d : address) {
											if (mapUtil
													.contains(e4,
															new Point(Double.parseDouble(d.get("longitude").toString()),
																	Double.parseDouble(d.get("latitude").toString())),
															r)) {
												for (DBObject e : address) {
													if (mapUtil.contains(e5,
															new Point(Double.parseDouble(e.get("longitude").toString()),
																	Double.parseDouble(e.get("latitude").toString())),
															r)) {
														for (DBObject f : address) {
															if (mapUtil.contains(e6, new Point(
																	Double.parseDouble(f.get("longitude").toString()),
																	Double.parseDouble(f.get("latitude").toString())),
																	r)) {
																results.add(f.get("isuNum").toString());
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				break;

			default:
				break;
			}

		}

		return results;
	}

	@Override
	public List<Preinfo> getPreInfo() {
		return mapDao.findAllPreinfoByDate();
	}

	// 根据id 查询预设信息
	@Override
	public Preinfo getPreInfoById(Integer id) {
		Preinfo preinfo = mapDao.findPreInfoById(id);
		return preinfo;
	}

	// 获取所有的下发区域预设信息
	@Override
	public List<PointsInfo> getPreInfo2() {
		List<PointsInfo> pointsInfos = new ArrayList<>();
		pointsInfos = mapDao.findAllPreinfo2();
		return pointsInfos;
	}

	// 删除下发区域的预设信息
	@Override
	public boolean deletePreInfo2(Integer serial) {
		boolean aa = mapDao.deletePreInfo2(serial);
		return aa;
	}

	// 添加下发区域的预设信息
	public void addPreInfo2(PointsInfo pointsInfo) {
		mapDao.addPreInfo2(pointsInfo);
	};

	@Override
	public List<Address> findAllTaxi() {
		// 查询实时表中的所有记录
		List<Address> address = addressDao.findAllTaxi();
		for (Address a : address) {
			// 根据isuNum查询出相应的车牌号
			Taxi t = taxiDao.getTaxiByIsu(a.getIsuNum());
			a.setTaxiNum(t.getTaxiNum());
		}
		return address;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Address> monitoring(String[] taxiNums) {
		Map<String, String> taxiIsu = new ConcurrentHashMap<>();
		// 验证车牌号是否为空
		if (taxiNums[0] == null || taxiNums[0].equals("")) {
			return null;
		}
		// 再根据车牌号获取相对应的终端号
		List<String> isuNums = new ArrayList<>();
		String s = null;
		long l1 = System.currentTimeMillis();
		Map<String, String> convertTaxis = addressDao.getIsuTaxis();
		for (String t : taxiNums) {
			// s = taxiDao.getIsuByTaxiNum(t);
			// if (s != null) {
			// isuNums.add(s);
			// taxiIsu.put(s,t);
			// }
			if (convertTaxis.containsKey(t)) {
				isuNums.add(convertTaxis.get(t));
				taxiIsu.put(convertTaxis.get(t), t);
			}
		}
		List<Address> address = new ArrayList<>();
		address = addressDao.findAddressByIsuNum(isuNums);
		Iterator<Address> addressIt = address.iterator();
		while (addressIt.hasNext()) {
			Address a = addressIt.next();
			// }
			// Address a = addressDao.findAddressByIsuNum(isuNum);
			// if (address != null) {
			// for (Address a : address) {
			// 定位之后坐标转换
			if (a.getLatitude() != 0 && a.getLongitude() != 0) {
				Gps gps = PositionUtil.gps84_To_Gcj02(a.getLatitude(), a.getLongitude());
				a.setLatitude(gps.getWgLat());
				a.setLongitude(gps.getWgLon());
				gps = PositionUtil.gcj02_To_Bd09(a.getLatitude(), a.getLongitude());
				a.setLatitude(new BigDecimal(gps.getWgLat()).setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue());
				a.setLongitude(new BigDecimal(gps.getWgLon()).setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue());
			} else {
				addressIt.remove();
			}
			// }
		}
		long l4 = System.currentTimeMillis();
		// 在线状态表。
		DBCollection find = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.onlineTemp);
		BasicDBObject query = new BasicDBObject();

		// 添加车牌号、在线状态信息
		for (Address a : address) {
			// 查询条件
			query.append("isuNum", a.getIsuNum());
			// System.out.println("手机号"+a.getIsuNum());

			// if (find.find(query).hasNext()) {
			// DBObject object = find.findOne(query);
			// time = (Date) object.get("time");
			// // System.out.println("time为"+time);
			// }
			Date time = DateUtil.stringToDate(a.getTime());
			a.setTaxiNum(taxiIsu.get(a.getIsuNum()));
			// 在线状态的判断由在线实时表来得出。（时差300秒以内视为在线）

			// 而是否定位由位置信息得出
			try {
				String state = a.getState();
				String s1 = state.substring(6, 8);
				Integer i1 = Integer.decode("0x" + s1);
				byte b1 = i1.byteValue();
				String ss = toBinaryString(b1);
				char[] c = ss.toCharArray();
				char sta = c[6];
				// System.out.println(c);
				if (time.getTime() >= DateUtil.stringToDate(DateUtil.countDate(FarmConstant.ONLINE)).getTime()) {
					// System.out.println("符合条件");
					// System.out.println("位置"+a.getLatitude());
					a.setState("在线");
					Date time2 = DateUtil.stringToDate(a.getTime());
					if (time2.getTime() >= DateUtil.stringToDate(DateUtil.countDate(FarmConstant.LOCATION)).getTime()) {
						if (sta == '1') {
							a.setLocateState("定位");
						} else {
							a.setLocateState("不定位");
						}
					} else {
						a.setLocateState("不定位");
					}
				} else {
					// if (sta=='1') {
					// a.setLocateState("定位");
					// }else{a.setLocateState("不定位");}
					// System.out.println("状态：" + a.getState());
					a.setState("离线");
					a.setLocateState("不定位");
				}

			} catch (Exception e) {
				a.setState("在线状态异常");
				logger.error("获取在线状态时出现异常" + e.toString());
			}
			// 根据计价器实时表!，得出车辆的运营状态
			int n = 0;
			if (addressDao.getTempMeterByIsuNum(a.getIsuNum()) != null) {
				n = addressDao.getTempMeterByIsuNum(a.getIsuNum()).getState();
			}
			String operateState = null;
			switch (n) {
			case 0:
				operateState = "空车";
				break;
			case 1:
				operateState = "载客";
				break;
			case 2:
				operateState = "电召";
				break;
			case 3:
				operateState = "暂停";
				break;
			default:
				operateState = "空车";
				break;
			}
			a.setOperateState(operateState);
			// 判断版本，根据OnlineState判断使用计价器心跳还是终端心跳（更改需求为直接根据位置信息判断）改为终端心跳,或者计价器实时表。当参数为1时，使用计价器实时表
			// int i=1;
			// if(FarmConstant.OnlineState==i){
			// //使用计价器实时表
			// a.setState(addressDao.getOnlineOfMeter(a.getIsuNum()));
			// }else{
			// //使用终端心跳
			// a.setState(addressDao.getOnlineOfHeart(a.getIsuNum()));
			// }
			query.clear();
		}
		System.err.println("车辆数" + address.size());
		// 锁定排序
		if (address.size() > 0) {
			ComparatorResultType comparator = new ComparatorResultType();
			Collections.sort(address, comparator);
		}
		return address;
	}

	/**
	 * wk 车辆列表后台分页与轮询
	 *
	 * @param datatablesQuery
//	 * @param isuNums
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public DatatablesView<Address> monitoring1(DatatablesQuery datatablesQuery, String[] taxiNums) {
		// System.out.println("车牌号长度："+taxiNums.length);
		// 验证车牌号是否为空
		if (taxiNums[0] == null || taxiNums[0].equals("")) {
			String string1 = "1";
			taxiNums = string1.split(",");
		}
		// System.out.println("数组排序前："+taxiNums.toString());
		Arrays.sort(taxiNums);
		// System.out.println("数组排序后："+taxiNums.toString());
		DatatablesView<Address> datatablesView = new DatatablesView<>();
		datatablesView.setData(null);
		long d = System.currentTimeMillis();
		Map<String, String> convertTaxis = addressDao.getIsuTaxis();
		System.err.println("查询全部占用时间" + (System.currentTimeMillis() - d));
		// 是不是模糊查询获取数据总条数不一样
		Integer size = null;
		if (datatablesQuery.getSearch() == "") {
			// 再根据车牌号获取相对应的终端号
			List<String> isuNums = new ArrayList<>();
			List<String> isuNums2 = new ArrayList<>();
			// 提交表单
			/*
			 * for (String t : taxiNums) { if
			 * (taxiDao.getTaxiByTaxi2(datatablesQuery, t) == null) {
			 * isuNums.add("1"); }else{ s =
			 * taxiDao.getTaxiByTaxi2(datatablesQuery, t).getIsuNum();
			 * //System.out.println("车牌号转终端号:"+s); if (s != null) {
			 * isuNums.add(s); } } }
			 */
			String sg = null;
			// 提交表单
			for (String t : taxiNums) {
				// sg = taxiDao.getIsuByTaxiNum(t);
				// if (null == sg) {
				// isuNums.add("1");
				// } else {
				// //
				// System.out.println("********************查取的终端号："+sg.toString());
				// isuNums.add(sg);
				// }
				if (convertTaxis.containsKey(t)) {
					isuNums.add(convertTaxis.get(t));
				} else {
					isuNums.add("1");
				}

			}
			// System.out.println("【所有终端号：】"+isuNums.toString());
			List<Address> address = new ArrayList<>();
			// for (String isuNum : isuNums) {
			// // System.out.println("【截取前全部终端号】"+isuNum);
			// Address a = addressDao.findAddressByIsuNum(isuNum);
			// if (a != null) {
			// isuNums2.add(isuNum);
			// }
			// }
			address = addressDao.findAddressByIsuNum(isuNums);
			for (Address address2 : address) {
				isuNums2.add(address2.getIsuNum());
			}
			String[] newisuNums = (String[]) isuNums2.toArray(new String[0]);
			// System.out.println("2"+"="+newisuNums.length+"="+datatablesQuery.getStart()+"="+datatablesQuery.getLength());
			size = newisuNums.length;
			String cutisuNums[] = Arrays.copyOfRange(newisuNums, datatablesQuery.getStart(),
					datatablesQuery.getStart() + datatablesQuery.getLength());
			List<String> tmp = new ArrayList<String>();
			for (String str : cutisuNums) {
				if (str != null && str.length() != 0) {
					tmp.add(str);
				}
			}
			cutisuNums = tmp.toArray(new String[0]);
			// System.out.println("3"+cutisuNums.toString());
			address.clear();
			address = addressDao.findAddressByIsuNum(Arrays.asList(cutisuNums));
			Iterator<Address> addressIt = address.iterator();
			while (addressIt.hasNext()) {
				Address a = addressIt.next();

				// }
				// for (Address a : address) {
				// System.out.println("【截取后截取终端号】"+isuNum2);
				// Address a = addressDao.findAddressByIsuNum(isuNum2);
				// 定位之后坐标转换
				if (a.getLatitude() != 0 && a.getLongitude() != 0) {
					Gps gps = PositionUtil.gps84_To_Gcj02(a.getLatitude(), a.getLongitude());
					a.setLatitude(gps.getWgLat());
					a.setLongitude(gps.getWgLon());
					gps = PositionUtil.gcj02_To_Bd09(a.getLatitude(), a.getLongitude());
					a.setLatitude(new BigDecimal(gps.getWgLat()).setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue());
					a.setLongitude(new BigDecimal(gps.getWgLon()).setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue());
					// address.add(a);
				} else {
					addressIt.remove();
					// 否则设置默认经纬度
					/*
					 * a.setLatitude(36.176478); a.setLongitude(120.486935);
					 * address.add(a);
					 */
				}
			}
			// 验证地址信息是否为空
			// System.out.println("地址信息数据长度："+address.size());
			/*
			 * if (address.size() == 0) { return null; }
			 */
			// 在线状态表。
			DBCollection find = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.onlineTemp);
			BasicDBObject query = new BasicDBObject();
			// 添加车牌号、在线状态信息
			for (Address a : address) {
				// 查询条件
				Date time = new Date(000000);
				query.append("isuNum", a.getIsuNum());
				// System.out.println("手机号"+a.getIsuNum());

				if (find.find(query).hasNext()) {
					DBObject object = find.findOne(query);
					time = (Date) object.get("time");
					// System.out.println("time为"+time);
				}

				// Taxi t = taxiDao.getTaxiByIsu(a.getIsuNum());
				a.setTaxiNum(getKey(convertTaxis, a.getIsuNum()));
				System.err.println(a.getTaxiNum());
				// 在线状态的判断由在线实时表来得出。（时差300秒以内视为在线）
				// 而是否定位由位置信息得出
				try {
					String state = a.getState();
					String s1 = state.substring(6, 8);
					Integer i1 = Integer.decode("0x" + s1);
					byte b1 = i1.byteValue();
					String ss = toBinaryString(b1);
					char[] c = ss.toCharArray();
					char sta = c[6];
					// System.out.println(c);
					if (time.getTime() >= DateUtil.stringToDate(DateUtil.countDate(FarmConstant.ONLINE)).getTime()) {
						// System.out.println("符合条件");
						// System.out.println("位置"+a.getLatitude());
						a.setState("在线");
						Date time2 = DateUtil.stringToDate(a.getTime());
						if (time2.getTime() >= DateUtil.stringToDate(DateUtil.countDate(FarmConstant.LOCATION))
								.getTime()) {
							if (sta == '1') {
								a.setLocateState("定位");
							} else {
								a.setLocateState("不定位");
							}
						} else {
							a.setLocateState("不定位");
						}
					} else {
						// if (sta=='1') {
						// a.setLocateState("定位");
						// }else{a.setLocateState("不定位");}
						// System.out.println("状态：" + a.getState());
						a.setState("离线");
						a.setLocateState("不定位");
					}

				} catch (Exception e) {
					a.setState("在线状态异常");
					logger.error("获取在线状态时出现异常" + e.toString());
				}
				// 根据计价器实时表!，得出车辆的运营状态
				int n = 0;
				if (addressDao.getTempMeterByIsuNum(a.getIsuNum()) != null) {
					n = addressDao.getTempMeterByIsuNum(a.getIsuNum()).getState();
				}
				String operateState = null;
				switch (n) {
				case 0:
					operateState = "空车";
					break;
				case 1:
					operateState = "载客";
					break;
				case 2:
					operateState = "电召";
					break;
				case 3:
					operateState = "暂停";
					break;
				default:
					operateState = "空车";
					break;
				}
				a.setOperateState(operateState);
				// 判断版本，根据OnlineState判断使用计价器心跳还是终端心跳（更改需求为直接根据位置信息判断）改为终端心跳,或者计价器实时表。当参数为1时，使用计价器实时表
				// int i=1;
				// if(FarmConstant.OnlineState==i){
				// //使用计价器实时表
				// a.setState(addressDao.getOnlineOfMeter(a.getIsuNum()));
				// }else{
				// //使用终端心跳
				// a.setState(addressDao.getOnlineOfHeart(a.getIsuNum()));
				// }
				query.clear();
			}
			// 锁定排序
			if (address.size() > 0) {
				ComparatorResultType comparator = new ComparatorResultType();
				Collections.sort(address, comparator);
				// System.out.println("service地址数据："+address+"字符化"+address.toString());
			} else {
				// System.out.println("service地址数据："+address+"字符化"+address.toString());
			}
			// System.out.println("service的无模糊SIZE："+size);
			// System.out.println("service的无模糊地址大小："+address.size());
			datatablesView.setData(address);
			datatablesView.setDraw(datatablesQuery.getDraw());
			datatablesView.setRecordsFiltered(size);
			datatablesView.setRecordsTotal(size);
			// System.out.println("service SET成功！");
			long time112 = System.currentTimeMillis();
			return datatablesView;
		}
		if (datatablesQuery.getSearch() != "") {
			// long time113 = System.currentTimeMillis();
			// size = taxiDao.getTaxiSize(datatablesQuery,taxiNums);
			// long time115 = System.currentTimeMillis();
			// System.out.println("模糊size所用时间："+(time115-time113));
			// System.out.println("service的模糊SIZE"+size);
			Blur blur = taxiDao.getTaxiList(datatablesQuery, taxiNums);
			// System.out.println("service的模糊address"+address);
			datatablesView.setData(blur.getList());
			datatablesView.setDraw(datatablesQuery.getDraw());
			datatablesView.setRecordsFiltered(blur.getSize());
			datatablesView.setRecordsTotal(blur.getSize());
			// System.out.println("service SET模糊成功！");
			// long time114 = System.currentTimeMillis();
			// System.out.println("模糊所用时间："+(time114-time113));
			return datatablesView;
		}
		return null;
	}

	public static String toBinaryString(byte b) {
		String binary = null;
		int i = b;
		String s = Integer.toBinaryString(i);
		if (i >= 0) {
			int len = s.length();
			if (len < 8) {
				int offset = 8 - len;
				for (int j = 0; j < offset; j++) {
					s = "0" + s;
				}
			}
			binary = s;
		} else {
			binary = s.substring(24);
		}
		return binary;
	}

	@Override
	public List<Address> monitor(String[] isuNums) {
		// 验证终端号是否为空
		if (isuNums[0] == null || isuNums[0].equals("")) {
			return null;
		}
		// 再根据终端号去获取对应车辆的位置信息。

		List<Address> address = new ArrayList<>();

		for (String isuNum : isuNums) {
			Address a = addressDao.findAddressByIsuNum(isuNum);
			if (a != null) {
				address.add(a);
			}
		}
		// 验证地址信息是否为空
		if (address.size() == 0) {
			return null;
		}
		// 在线状态表。
		DBCollection find = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.onlineTemp);
		BasicDBObject query = new BasicDBObject();

		// 添加车牌号、在线状态信息
		for (Address a : address) {
			// 查询条件
			Date time = new Date(000000);
			query.append("isuNum", a.getIsuNum());
			// System.out.println("手机号"+a.getIsuNum());

			if (find.find(query).hasNext()) {
				DBObject object = find.findOne(query);
				time = (Date) object.get("time");
				// System.out.println("time为"+time);
			}
			Taxi t = taxiDao.getTaxiByIsu(a.getIsuNum());
			a.setTaxiNum(t.getTaxiNum());
			// 在线状态的判断由在线实时表来得出。（时差300秒以内视为在线）

			// 而是否定位由位置信息得出
			try {
				String state = a.getState();
				String s1 = state.substring(6, 8);
				Integer i1 = Integer.decode("0x" + s1);
				byte b1 = i1.byteValue();
				String ss = toBinaryString(b1);
				char[] c = ss.toCharArray();
				char sta = c[6];
				if (time.getTime() >= DateUtil.stringToDate(DateUtil.countDate(FarmConstant.ONLINE)).getTime()) {
					a.setState("在线");
					Date date = DateUtil.stringToDate(a.getTime());
					if (date.getTime() >= DateUtil.stringToDate(DateUtil.countDate(FarmConstant.LOCATION)).getTime()) {
						if (Objects.equals(sta, '1')) {
							a.setLocateState("定位");
						} else {
							a.setLocateState("未定位");
						}
					} else {
						a.setLocateState("未定位");
					}
				} else {
					a.setState("离线");
					a.setLocateState("未定位");
				}

			} catch (Exception e) {
				a.setState("在线状态异常");
				logger.error("获取在线状态时出现异常" + e.toString());
			}
			// 根据计价器实时表!，得出车辆的运营状态
			int n = 0;
			if (addressDao.getTempMeterByIsuNum(a.getIsuNum()) != null) {
				n = addressDao.getTempMeterByIsuNum(a.getIsuNum()).getState();
			}
			String operateState = null;
			switch (n) {
			case 0:
				operateState = "空车";
				break;
			case 1:
				operateState = "载客";
				break;
			case 2:
				operateState = "电召";
				break;
			case 3:
				operateState = "暂停";
				break;
			default:
				operateState = "空车";
				break;
			}
			a.setOperateState(operateState);
			// 判断版本，根据OnlineState判断使用计价器心跳还是终端心跳（更改需求为直接根据位置信息判断）改为终端心跳,或者计价器实时表。当参数为1时，使用计价器实时表
			// int i=1;
			// if(FarmConstant.OnlineState==i){
			// //使用计价器实时表
			// a.setState(addressDao.getOnlineOfMeter(a.getIsuNum()));
			// }else{
			// //使用终端心跳
			// a.setState(addressDao.getOnlineOfHeart(a.getIsuNum()));
			// }
		}

		return address;
	}

	@Override
	public Integer pollingOfAlarm() {
		List<Alarm> alarms = addressDao.getAlarmByDealState1();
		return alarms.size();
	}

	public static String getKey(Map<String, String> convertTaxis, String value) {
		String key = null;
		// Map,HashMap并没有实现Iteratable接口.不能用于增强for循环.
		for (String getKey : convertTaxis.keySet()) {
			if (convertTaxis.get(getKey).equals(value)) {
				key = getKey;
			}
		}
		return key;
		// 这个key肯定是最后一个满足该条件的key
	}

	@Override
	public List<Integer> creenOnlin(Integer type, List<Integer> companyId) {
		List<String> isuNums = new ArrayList<>();
		List<Integer> result = new ArrayList<>();

		int time = 0;
		// 开始解析查询条件
		switch (type) {
		case 0:
			time = 5 * 60;
			break;
		case 1:
			time = 2 * 60 * 60;
			break;
		case 2:
			time = 24 * 60 * 60;
			break;
		case 3:
			time = 48 * 60 * 60;
			break;
		default:
			break;
		}
		Date stratTime = DateUtil.stringToDate(DateUtil.countDate(time));

		isuNums = taxiDao.getIsuNumsByPany(companyId);
		// 判断在线状态，由原先的使用位置信息上传的时间，修改为使用计价器实时表或者终端心跳
		// List<Address> addresses=addressDao.screenOnline(isuNums, stratTime);
		int n = 0;
		int i = 2;
		if (FarmConstant.OnlineState == i) {
			// 使用计价器实时表判断在线状态
			n = addressDao.screenOnlineOfMeter(isuNums, stratTime);
		} else {
			// 根据终端心跳判断在线状态
			// n=addressDao.screenOnlineOfHeart(isuNums, stratTime);
			// 判断在线状态，使用位置信息上传的时间 .
			n = addressDao.screenOnlineOfAddress(isuNums, stratTime);
		}

		// 总终端个数，在线终端个数
		result.add(isuNums.size());
		result.add(n);
		return result;
	}

	@Override
	public MileageVector mileageUtil(List<Integer> companyId, String startDate, String endDate) {
		float totalEmptymil = 0; // 空驶里程
		float totalMileageUtil = 0; // 载客里程
		// float maxMileage=0; //单车单日最大运营里程
		// float maxMileageUtil=0; //单车单日最大载客里程
		int dayNumber = 0; // 查询的天数
		int taxiNumber = 0; // 车辆的数目
		float rate;
		MileageVector mileage = new MileageVector();

		// 首先根据公司id查询出要查询的车辆（车牌号）
		List<String> taxiNums = taxiDao.getTaxiNumsByPany(companyId);
		List<String> taxiNums2 = new ArrayList<>();
		for (String str : taxiNums) {
			if (str.length() == 7)
				taxiNums2.add(str.substring(1));
			else if (str.length() == 8)
				taxiNums2.add(str.substring(1, 2) + str.substring(3));
		}
		// 去掉车牌首位

		taxiNumber = taxiNums.size();
		// 根据开始时间和结束时间，得出需要查询的数据库
		Integer startTime = Integer.parseInt(startDate.substring(2, 4) + startDate.substring(5, 7));
		Integer endTime = Integer.parseInt(endDate.substring(2, 4) + endDate.substring(5, 7));
		List<Meter> operates = new ArrayList<>();
		// 得出所有的下车记录
		for (int i = startTime; i <= endTime; i++) {
			String dbname = FarmConstant.METER + i;
			operates.addAll(addressDao.getMeterByTaxiNums(taxiNums2, startDate, endDate, dbname));
		}
		// 遍历下车记录，得出具体的里程信息
		for (Meter o : operates) {
			totalEmptymil += o.getEmptymil();
			totalMileageUtil += o.getMileage();
			// 计算单车单日的最大运营和最大载客里程
			// 设计时出错，暂时放弃该功能
		}
		// 开始计算要向前台发送的数据
		long from = DateUtil.stringToDate(startDate).getTime();
		long to = DateUtil.stringToDate(endDate).getTime();
		dayNumber = (int) ((to - from) / (1000 * 60 * 60 * 24));

		rate = ((int) (totalMileageUtil / (totalEmptymil + totalMileageUtil) * 10000)) / 100;

		mileage.setTotalMileage(totalEmptymil + totalMileageUtil);
		mileage.setMileageUtil(totalMileageUtil);
		mileage.setDayNumber(dayNumber);
		mileage.setTaxiNumber(taxiNumber);
		mileage.setRate(rate);

		return mileage;
	}

	@Override
	public List<ComInfo2> getRevenueReport(List<String> taxiNums, String startDate, String endDate) {
		// 首先根据车牌号的集合得出公司信息
		List<ComInfo2> comInfos = taxiDao.getPanyBytaxiNums(taxiNums);
		List<String> taxiNums2 = new ArrayList<>();
		for (String str : taxiNums) {
			if (str.length() == 7)
				taxiNums2.add(str.substring(1));
			else if (str.length() == 8)
				taxiNums2.add(str.substring(1, 2) + str.substring(3));
		}
		// 再根据车牌号，得出时间段内所有营运信息
		// 根据开始时间和结束时间，得出需要查询的数据库
		Integer startTime = Integer.parseInt(startDate.substring(2, 4) + startDate.substring(5, 7));
		Integer endTime = Integer.parseInt(endDate.substring(2, 4) + endDate.substring(5, 7));
		List<Meter> meters = new ArrayList<>();
		// 得出所有的下车记录
		for (int i = startTime; i <= endTime; i++) {
			String dbname = FarmConstant.METER + i;
			meters.addAll(addressDao.getMeterByTaxiNums(taxiNums2, startDate, endDate, dbname));
		}
		// 获取开始的月日和结束的年月日
		Integer year = Integer.parseInt(startDate.substring(0, 4));
		Integer startMM = Integer.parseInt(startDate.substring(5, 7) + startDate.substring(8, 10));
		Integer endMM = Integer.parseInt(endDate.substring(5, 7) + endDate.substring(8, 10));
		// 遍历查询到的记录，并计算
		for (ComInfo2 company : comInfos) { // 第一层，公司循环
			for (TaxiInfo2 taxi : company.getTaxis()) { // 第二层，汽车循环
				String aa = null;
				for (int i = startMM; i <= endMM; i++) { // 第三层，日期循环
					ReveInfo revenue = new ReveInfo();
					for (Meter o : meters) { // 第四层，营收信息循环
						String day = o.getDownTime();// 运营时间按照下车时间为准
						if (o.getTaxiNum().equals(taxi.getTaxiNum().substring(1)) || o.getTaxiNum()
								.equals(taxi.getTaxiNum().substring(1, 2) + taxi.getTaxiNum().substring(3))
								&& Integer.parseInt(day.substring(5, 7) + day.substring(8, 10)) == i) { // 判断是否符合日期和车牌号
							// 开始添加属性
							if (revenue.getCompany() == null) {
								revenue.setCompany(company.getCname());
								if (aa == null) {
									aa = year + "-" + i / 100 + "-" + i % 100;
								} else {
									aa = aa + "," + year + "-" + i / 100 + "-" + i % 100;
								}
								revenue.setDate(aa);
								revenue.setTaxiNum(taxi.getTaxiNum());
							}
							revenue.setEmptyMile(revenue.getEmptyMile() + o.getEmptymil());
							revenue.setMileUtil(revenue.getMileUtil() + o.getMileage());
							revenue.setMoney(revenue.getMoney() + o.getMoney());
							revenue.setNumber(revenue.getNumber() + 1);
							// 合租版本 评价字段代表是否合租合乘。
							if (o.getEvaluate().equalsIgnoreCase("01")) {
								revenue.setRoommate(revenue.getRoommate() + 1);// 合租次数
							}
						}
					}
					// 把该天的计算结果存放到集合中
					if (revenue.getCompany() != null) {
						taxi.getRevenues().clear();
						taxi.getRevenues().add(revenue);
					}

					// 月份部分循环
					int n;
					if (i / 100 == 1 || i / 100 == 3 || i / 100 == 5 || i / 100 == 7 || i / 100 == 8 || i / 100 == 10
							|| i / 100 == 12) {
						n = 31;
					} else if (i / 100 == 2) {
						if (year % 4 == 0) {
							n = 29;
						} else {
							n = 28;
						}
					} else {
						n = 30;
					}
					if (i % 100 / 1 == n) {
						i += (100 - n);
					}
				}
			}
		}
		return comInfos;
	}

	@Override
	public List<Meter> getMetersByTaxi(List<String> taxiNums, String startDate, String endDate) {
		List<Meter> meters = new ArrayList<>();
		// 首先根据开始时间，结束时间得出要查询的数据库表
		Integer startTime = Integer.parseInt(startDate.substring(2, 4) + startDate.substring(5, 7));
		Integer endTime = Integer.parseInt(endDate.substring(2, 4) + endDate.substring(5, 7));
		for (int i = startTime; i <= endTime; i++) {
			String dbname = FarmConstant.METER + i;
			meters.addAll(addressDao.getMeterByTaxiNums(taxiNums, startDate, endDate, dbname));
		}
		logger.info("查询到车辆" + taxiNums + "运行里程" + meters);
		return meters;
	}

	@Override
	public List<Alarm> getAlarmByTaxi(String taxiNum, String startDate, String endDate, Integer alarmType) {
		List<Taxi> taxis = taxiDao.getAllTaxi();
		Map<String, String> map = new ConcurrentHashMap<>();
		for (Taxi taxi : taxis) {
			map.put(taxi.getIsuNum(), taxi.getTaxiNum());
		}

		String isuNum = null;
		List<Alarm> alarms = new ArrayList<>();
		// System.out.println("---------车牌号----------"+taxiNum);
		if (taxiNum != null && !taxiNum.equals("")) {
			// 首先根据车牌号得出终端号
			Taxi taxi = taxiDao.getTaxiByTaxi(taxiNum);
			// System.out.println("---------Taxi类----------"+taxi1);
			if (taxi == null) {
				return null;
				// System.out.println("---------isuNum----------"+isuNum);
			} else {
				isuNum = taxi.getIsuNum();
				// System.out.println("---------isuNum----------"+isuNum);
				alarms = addressDao.getAlarmByTaxi(isuNum, startDate, endDate, alarmType);
				if (alarms.size() > 0) {
					for (Alarm a : alarms) {
						a.setTaxiNum(taxiNum);
					}
					return alarms;
				} else {
					return null;
				}
			}
		} else {
			alarms = addressDao.getAlarmByTaxi(isuNum, startDate, endDate, alarmType);
			if (alarms.size() > 0) {
				for (Alarm a : alarms) {
					if (map.containsKey(a.getIsuNum())) {
						a.setTaxiNum(map.get(a.getIsuNum()));
					} else {
						a.setTaxiNum("未知");
					}
				}
			}
		}
		return alarms;
	}


	// Wk后台分页，根据条件，查看报警日志
	@Override
	public Blur getAlarmByTaxi1(InfoQuery infoQuery) {
		Blur blur = new Blur();

		List<Taxi> taxis = taxiDao.getAllTaxi();
		Map<String, String> map = new ConcurrentHashMap<>();
		for (Taxi taxi : taxis) {
			map.put(taxi.getIsuNum(),taxi.getTaxiNum());
		}
		System.out.println("终端号："+map.get(000000000001));
		String isuNum = null;
		if (infoQuery.getTaxiNum() != null && !infoQuery.getTaxiNum().equals("")) {
			// 首先根据车牌号得出终端号
			Taxi taxi = taxiDao.getTaxiByTaxi(infoQuery.getTaxiNum());
			// System.out.println("---------Taxi类----------"+taxi1);
			if (taxi == null) {
				return null;
				// System.out.println("---------isuNum----------"+isuNum);
			} else {
				isuNum = taxi.getIsuNum();
				// System.out.println("---------isuNum----------"+isuNum);
				blur = addressDao.getAlarmByTaxi1(isuNum, infoQuery);
				if (blur.getListAlarm().size() > 0) {
					for (Alarm2 a : blur.getListAlarm()) {
						a.setTaxiNum(infoQuery.getTaxiNum());
					}
					return blur;
				} else {
					return null;
				}
			}
		} else {
			blur = addressDao.getAlarmByTaxi1(isuNum, infoQuery);
			if (blur.getListAlarm().size() > 0) {
				for (Alarm2 a : blur.getListAlarm()) {
					if (map.containsKey(a.getIsuNum())) {
						a.setTaxiNum(map.get(a.getIsuNum()));
					} else {
						a.setTaxiNum("未知");
					}
				}
			}
		}
		// System.out.println("【service报警日志后台分页alarms】"+blur);
		return blur;
	}

	@Override
	public List<Alarm> getdealAlarm() {
		List<Alarm> alarm = addressDao.getAlarmByDealState();
		for ( int i = 0 ; i < alarm.size() - 1 ; i ++ ) {
			for ( int j = alarm.size() - 1 ; j > i; j -- ) { //报警类型为同一终端号的删去
				if (alarm.get(j).getAlarmType().equals(alarm.get(i).getAlarmType()) && alarm.get(j).getIsuNum().equals(alarm.get(i).getIsuNum())) {
					alarm.remove(j);}
			}
		}// 再获取向对应的车牌号
		if (alarm.size() > 0) {
			for (Alarm a : alarm) {
				if (a.getIsuNum() != null) {
					if (taxiDao.getTaxiByIsu(a.getIsuNum())!= null) {
						a.setTaxiNum(taxiDao.getTaxiByIsu(a.getIsuNum()).getTaxiNum());
					}
				}
			}
		}
		if(alarm == null){
			return null;
		}else {
			return alarm;
		}
	}

	@Override
	public List<OperateTest> flowOfAnalyze(String startDate, String endDate, List<String> taxiNums) {
		// 验证输入非空
		if (startDate == null || endDate == null || taxiNums.size() == 0) {
			return null;
		}
		List<String> taxiNums2 = new ArrayList<>();
		List<OperateTest> operates = new ArrayList<>();
		// 首先，遍历车牌号,获得每辆车的OperateTest
		for (String taxiNum : taxiNums) {
			OperateTest operate = new OperateTest();
			List<Meter> meters = new ArrayList<>();

			// 根据时间遍历数据库，得出计价器信息
			Integer start = Integer.parseInt(startDate.substring(2, 4) + startDate.substring(5, 7));
			Integer end = Integer.parseInt(endDate.substring(2, 4) + endDate.substring(5, 7));
			for (int i = start; i <= end; i++) {
				String dbname = FarmConstant.METER + i;
				// 根据时间段与车牌号得出计价器记录
				if (taxiNum.length() == 7) {
					meters.addAll(addressDao.getMeterByTaxiNum(taxiNum.substring(1), startDate, endDate, dbname));
				} else if (taxiNum.length() == 8) {
					meters.addAll(addressDao.getMeterByTaxiNum(taxiNum.substring(1, 2) + taxiNum.substring(3),
							startDate, endDate, dbname));
				}
			}

			// 根据计价器记录集合meters得出operate的属性
			float mileage1 = 0; // 空驶里程
			float mileage2 = 0; // 载客里程
			float total = 0; // 总金额
			Integer waitTime = 0;
			for (Meter m : meters) {
				mileage1 += m.getEmptymil();
				mileage2 += m.getMileage();
				total += m.getMoney();
				waitTime += Integer.parseInt(m.getWaitTime().substring(0, 2) + m.getWaitTime().substring(3, 5)
						+ m.getWaitTime().substring(6, 8));
			}
			operate.setCompany(taxiDao.getCompanyBytaxi(taxiNum).getName());
			operate.setEmptymli(mileage1);
			operate.setFullmil(mileage2);
			operate.setMoney(total);
			operate.setNumber(meters.size());
			operate.setTaxiNum(taxiNum);
			operate.setWaitTime((waitTime / 360) + "时" + (waitTime % 60 / 60) + "分" + (waitTime % 60 % 60) + "秒");
			operates.add(operate);
		}

		return operates;
	}

	@Override
	public List<MyPassVol> getFareFlow(String startDate, String endDate, String type, List<String> taxiNums) {
		List<MyPassVol> ms = new ArrayList<>();
		// 首先判断查询的类别
		// 获取x轴数据
		List<String> dates = new ArrayList<>();
		List<String> taxiNums2 = new ArrayList<>();
		for (String str : taxiNums) {
			if (str.length() == 7) {
				taxiNums2.add(str.substring(1));
			} else if (str.length() == 8) {
				taxiNums2.add(str.substring(1, 2) + str.substring(3));
			}
		}
		try {
			if (type.equals("day")) {
				dates = CalendarUtil.getDays(startDate, endDate); // x轴为日

				// 为天的话
				for (String date : dates) {
					String dbname = FarmConstant.METER + date.substring(2, 4) + date.substring(5, 7);
					List<Meter> meters = addressDao.getMeterByTaxiNums(taxiNums2, date + " 00:00:00",
							date + " 23:59:29", dbname);
					MyPassVol m = new MyPassVol(date, meters.size());
					ms.add(m);
				}
			} else {
				dates = CalendarUtil.getMonths(startDate, endDate); // x轴为月

				// 为月的话
				for (String date : dates) {
					String dbname = FarmConstant.METER + date.substring(2, 4) + date.substring(5, 7);
					String n = null;
					switch (date.substring(5, 7)) {
					case "01":
					case "03":
					case "05":
					case "07":
					case "08":
					case "10":
					case "12":
						n = "-31";
						break;
					case "04":
					case "06":
					case "09":
					case "11":
						n = "-30";
						break;
					default:
						n = "-28";
						break;
					}

					List<Meter> meters = addressDao.getMeterByTaxiNums(taxiNums2, date + "-01 00:00:00",
							date + n + " 23:59:29", dbname);
					MyPassVol m = new MyPassVol(date, meters.size());
					ms.add(m);
				}
			}
		} catch (ParseException e) {
			e.printStackTrace();
			logger.error("获取运费信息出现异常" + e.getMessage());
		}

		return ms;
	}

	@Override
	public OnlineRate getOnlineRateOf0() {
		// 需要返回的值：当前在线率、 24小时在线率、 重车/电召/暂停
		OnlineRate online = new OnlineRate();
		// 首先去车辆表获取车辆数目
		logger.info("在线率： 获取所有车辆");

		Integer total = addressDao.getOnlineFromOnlineTemp();
		System.out.println("测试的分母巴拉巴拉"+total);
		// 计算当前在线的车辆数和二十四小时内在线的车辆数
		Integer curr = addressDao.getOnlineNum(DateUtil.countDate(FarmConstant.ONLINE));
		logger.info("在线率： 获取当前在线车辆数" + curr);
		Integer hist = addressDao.getOnlineNum(DateUtil.countDate(60 * 60 * 24));
		logger.info("在线率： 获取一天内在线车辆数" + hist);
		// 在线数/车辆总数

		Integer currRare, histRare;
		if (total == 0) {
			currRare = histRare = 0;
		} else {
			currRare = curr * 10000 / total;
			histRare = hist * 10000 / total;
		}
		int operNum = 0; // 重车数量
		int empNum = 0; // 暂停数量
		int callNum = 0; // 电召数量
		// 查询计价器临时表的所有数据，并遍历,获取各种营运状态的车辆总数。
		List<MeterTemp> meterTemps = addressDao.getAllMeterTemp();
		for (MeterTemp m : meterTemps) {
			switch (m.getState()) {
			case 1:
				operNum++;
				break;
			case 2:
				callNum++;
				break;
			case 3:
				empNum++;
				break;
			default:
				break;
			}
		}
		// 开始向对象中赋值
		online.setCallNum(callNum);
		online.setEmpNum(empNum);
		online.setOperNum(operNum);
		online.setCurrOnline(currRare / 100 + "." + currRare % 100 + "%");
		online.setHistOnline(histRare / 100 + "." + histRare % 100 + "%");

		return online;
	}

	//获取车辆的总数
	@Override
	public int getOnlineAll(){
		Integer total = taxiDao.getAllTaxi().size();
		return total;
	}
	//获取当前车辆的数量
	public int getOnlineCurrent(){
		Integer curr = addressDao.getOnlineNum(DateUtil.countDate(FarmConstant.ONLINE));
		return curr;
	}
	@Override
	public OnlineRate getOnlineRateOf1() {
		// 需要返回的值：当前在线率、 24小时在线率、 重车/电召/暂停
		OnlineRate online = new OnlineRate();
		// 首先去车辆表获取车辆数目
		Integer total = taxiDao.getAllTaxi().size();
		List<MeterTemp> meterTemps = addressDao.getAllMeterTemp();
		// System.out.println("查询出的传感器实时表记录"+meterTemps);
		int curr = 0;
		int hist = 0;
		int operNum = 0; // 重车数量
		int empNum = 0; // 暂停数量
		int callNum = 0; // 电召数量
		for (MeterTemp m : meterTemps) {
			if (m.getTime().getTime() >= DateUtil.stringToDate(DateUtil.countDate(FarmConstant.ONLINE)).getTime()) {
				curr++;
				hist++;
			} else if (m.getTime().getTime() >= DateUtil.stringToDate(DateUtil.countDate(60 * 60 * 24)).getTime()) {
				hist++;
			}
			switch (m.getState()) {
			case 1:
				operNum++;
				break;
			case 2:
				callNum++;
				break;
			case 3:
				empNum++;
				break;
			default:
				break;
			}
		}
		// System.out.println(curr);
		Integer currRare = curr * 10000 / total;
		Integer histRare = hist * 10000 / total;
		// 开始向对象中赋值
		online.setCallNum(callNum);
		online.setEmpNum(empNum);
		online.setOperNum(operNum);
		online.setCurrOnline(currRare / 100 + "." + currRare % 100 + "%");
		online.setHistOnline(histRare / 100 + "." + histRare % 100 + "%");
		return online;
	}

	// 排序类
	class ComparatorResultType implements Comparator {

		public int compare(Object arg0, Object arg1) {
			Address taxi0 = (Address) arg0;
			Address taxi1 = (Address) arg1;

			// 首先比较主指标，如果主指标相同，则比较次指标

			int flag = taxi0.getTaxiNum().compareTo(taxi1.getTaxiNum());
			if (flag == 0) {
				return taxi0.getTaxiNum().compareTo(taxi1.getTaxiNum());
			} else {
				return flag;
			}
		}
	}

	@Override
	public List<Alarm> getAllAlarmType(Date startDate, Date endDate, String alarmType) {
		return addressDao.getAllAlarmType(startDate, endDate, alarmType);
	}
	@Override
	public List<Alarm> getDealAlarmByIsu(String alarmType,String isu) {
		return addressDao.getDealAlarmByIsu(alarmType,isu);
	}

	@Override
	public Taxi getTaxiByIsu(String isu) {
		return taxiDao.getTaxiByIsu(isu);
	}

	@Override
	public Taxi getTaxisByTaxiNum(String taxiNum) {
		return taxiDao.getTaxiByTaxi(taxiNum);
	}
}
