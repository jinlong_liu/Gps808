<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">



<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<!-- <base target="_blank"> -->
<link href="css/plugins/jsTree/style.min.css" rel="stylesheet">
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-9">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>
							营运信息
							<!-- <small>分类，查找</small> -->
						</h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
							</a>
						</div>
					</div>
					<div class="ibox-content" style="height: 781px;">
					<!-- <div class="pull-right">
							<button type="button" class="btn btn-primary" id="demo1">导出表格</button>
					</div> -->
					 
						<table
							class="table table-striped table-bordered table-hover dataTables-example">
							<thead>
								<tr>
									<th>车牌号</th>
									<th>资格证号</th>
									<th>上车时间</th>
									<th>下车时间</th>
									<th>空驶里程</th>
									<th>该客次里程</th>
									<th>等待时间</th>
									<th>单价</th>
									<th>租金</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${meter}" var="meter">
									<tr>
										<td>${meter.taxiNum}</td>
										<td>${meter.quaNum}</td>
										<td>${meter.upTime}</td>
										<td>${meter.downTime}</td>
										<td>${meter.emptymil}</td>
										<td>${meter.mileage}</td>
										<td>${meter.waitTime}</td>
										<td>${meter.price}</td>
										<td>${meter.money}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<!-- 日期+精确查询+导出表格开始 -->
				<form action="checkMeterByTaxi" id="getOneForm">
					<div class="ibox-title">
						<h5 class="text-center">查询营运信息</h5>
					</div>
					<div class="ibox-content" style="height: 780px;">
						<button id="getOneBtn" type="button"
							class="btn btn-primary btn-sm-2">查询</button>
						<button type="button" class="btn btn-primary " id="demo1">导出表格</button>
						<input type="hidden" id="taxiNums" name="taxiNums"
							value="${meterTaxiNum}" placeholder="显示树形菜单选中的节点"> 
							<br> 
							<br> 开始时间：
					 <input
							type="text" name="startDate" id="startDate" class="form-control"
							value="${meterStartDate }" class="Wdate"
							onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'endDate\')||\'new Date()\'}',readOnly:true})">
						<br> <br> 结束时间： <input type="text" name="endDate"
							id="endDate" placeholder="" value="${meterEndDate }"  class="form-control"
							class="Wdate"
							onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'startDate\')}',maxDate:'#F{\'new Date()\'}',dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true})">
						<br>
							<h5>请选择车辆</h5>
							  <br>  
						<div id="jstree1" style="overflow:auto;height:300px;width:250px; ">
							<ul>
								<li class="jstree-open">所有公司
									<ul>
										<c:forEach items="${comInfo2s}" var="comInfo2s">
											<li id="${comInfo2s.comId}">${comInfo2s.cname }
												<ul>
													<c:forEach items="${comInfo2s.taxis}" var="taxis">
														<li id="${taxis.taxiNum}" data-jstree='{"type":"car"}'>
															${taxis.taxiNum }</li>
													</c:forEach>
												</ul>
											</li>
										</c:forEach>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</form>

				<!-- 导出表格表单 -->
				 <form action="getMeterExcel" id="meterPoi" method="post">
					<input type="hidden" name="taxiNum" id="taxiNumPoi"> <input
						type="hidden" name="startDate" id="startDatePoi"> <input
						type="hidden" name="endDate" id="endDatePoi">
				</form>  
				<!-- 日期+精确查询+导出表格结束 -->
			</div>
			
		</div>

	</div>
	<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/content.min.js?v=1.0.0"></script>
	<script src="js/plugins/jsTree/jstree.min.js"></script>
	<script>
		$(document)
				.ready(
						function() {

							//选中树形节点框制空
							$("#taxiNums").val("");
							//加载树形菜单
							//加载jstree
							$("#jstree1").jstree(
									{
										"core" : {
											"check_callback" : true
										},
										"plugins" : [ "types", "dnd", "checkbox", "sort",
												"wholerow", "search", "unique" ],
										"types" : {
											"car" : {
												"icon" : "fa fa-car"
											},
											"default" : {
												"icon" : "fa fa-folder"
											}
										}
									});

							//动态获取选中节点的id
							$("#jstree1").on('changed.jstree', function(e, data) {
								r = [];
								var i, j;
								for (i = 0, j = data.selected.length; i < j; i++) {
									var node = data.instance.get_node(data.selected[i]);
									if (data.instance.is_leaf(node)) {
										r.push(node.id);
									}
								}
								$("#taxiNums").val(r);
							}).on("search.jstree", function (e, data) {
								if(data.nodes.length) {
									var matchingNodes = data.nodes; // change
									$(this).find(".jstree-node").hide().filter('.jstree-last').filter(function() { return this.nextSibling; }).removeClass('jstree-last');
									data.nodes.parentsUntil(".jstree").addBack().show()
										.filter(".jstree-children").each(function () { $(this).children(".jstree-node:visible").eq(-1).addClass("jstree-last"); });
									// nodes need to be in an expanded state
									matchingNodes.find(".jstree-node").show(); // change
								}
							}).on("clear_search.jstree", function (e, data) {
								if(data.nodes.length) {
									$(this).find(".jstree-node").css("display","").filter('.jstree-last').filter(function() { return this.nextSibling; }).removeClass('jstree-last');
								}
							});
							//------------------------------------
							
							//jsTree的模糊查询
							var to = false;
							$("#txtIndustryArea").keyup(function() {
								if (to) {
									clearTimeout(to);
								}
								to = setTimeout(function() {
									var v = $("#txtIndustryArea").val();
									var temp = $("#jstree1").is(":hidden");
									if (temp == true) {
										$("#jstree1").show();
									}
									$("#jstree1").jstree(true).search(v);
								}, 250);
							});	

							//点击"精确查找"
							$("#getOneBtn").click(function() {
								var startTime = $("#startDate").val();
								var endTime = $("#endDate").val();
								var taxiNum = $("#taxiNums").val();
								var endYear = null;
								//开始时间的年份
								var startYear = startTime.substr(0, 4);
								//结束时间的年份
								if (endTime == "") {
									endYear = new Date().getFullYear();
								} else {
									endYear = endTime.substr(0, 4);
								}

								if (startTime == "") {
									swal({
										title : "请输入开始时间",
										type : "warning"
									});
								} else if (endTime == "") {
									swal({
										title : "请输入结束时间",
										type : "warning"
									});
								} else if (endYear != startYear) {
									swal({
										title : "不支持跨年查询，请重新选择开始时间或结束时间",
										type : "warning"
									});
								} else if (taxiNum == "") {
									swal({
										title : "请选择车辆",
										type : "warning"
									});
								} else {
									$("#getOneForm").submit();
								}
							});

							//点击"导出表格"按钮
							$("#demo1").click(
									function() {
										var a = '<%=(String)request.getParameter("taxiNums")%>';
										var startTime = $("#startDate").val();
										var endTime = $("#endDate").val();
										var nowTime = $("#endDate").val();//页面显示的结束时间
										var taxiNum = a;
										if (nowTime == "" || nowTime == null) {
											nowTime = "现在时刻";
										}
										if (startTime == "" || taxiNum == "") {
											swal({
												title : "导出表格时，开始时间和车牌号不能为空",
												type : "warning"
											});
										} else {
											swal({
												title : "",
												text : "您确定导出\n" + startTime
														+ " 到 " + nowTime
														+ " 期间，\n 车牌号为"
														+ taxiNum + "计价器的数据吗?",
												type : "warning",
												showCancelButton : true,
												cancelButtonText : "取消"
											}, function() {
												//赋值
												$("#startDatePoi").val(
														startTime);
												$("#endDatePoi").val(endTime);
												$("#taxiNumPoi").val(taxiNum);
												//提交表单
												$("#meterPoi").submit();
											});
										}

									});

							//显示dataTables自带的功能：每页显示数;查询;分页
							$(".dataTables-example").dataTable();
							var oTable = $("#editable").dataTable();
							oTable
									.$("td")
									.editable(
											"../example_ajax.php",
											{
												"callback" : function(sValue, y) {
													var aPos = oTable
															.fnGetPosition(this);
													oTable.fnUpdate(sValue,
															aPos[0], aPos[1])
												},
												"submitdata" : function(value,
														settings) {
													return {
														"row_id" : this.parentNode
																.getAttribute("id"),
														"column" : oTable
																.fnGetPosition(this)[2]
													}
												},
												"width" : "90%",
												"height" : "100%"
											})
						});
		function fnClickAddRow() {
			$("#editable").dataTable()
					.fnAddData(
							[ "Custom row", "New row", "New row", "New row",
									"New row" ])
		};
	</script>
</body>

</html>