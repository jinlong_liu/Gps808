package com.hx.gps.entities;

import com.mongodb.BasicDBObject;

public class adsCustomer {
	// 客户编码
	private Integer adsCustomerId;
	// 客户名称
	private String adsCustomerName;
	// 客户电话
	private String adsCustomerPhone;
	public int getAdsCustomerId() {
		return adsCustomerId;
	}
	public void setAdsCustomerId(Integer adsCustomerId) {
		this.adsCustomerId = adsCustomerId;
	}
	public String getAdsCustomerName() {
		return adsCustomerName;
	}
	public void setAdsCustomerName(String adsCustomerName) {
		this.adsCustomerName = adsCustomerName;
	}
	public String getAdsCustomerPhone() {
		return adsCustomerPhone;
	}
	public void setAdsCustomerPhone(String adsCustomerPhone) {
		this.adsCustomerPhone = adsCustomerPhone;
	}
	public adsCustomer() {
	}
	//带参构造，便于传递参数。
	public adsCustomer(Integer adsCustomerId, String adsCustomerName, String adsCustomerPhone) {
		super();
		this.adsCustomerId = adsCustomerId;
		this.adsCustomerName = adsCustomerName;
		this.adsCustomerPhone = adsCustomerPhone;
	}



	public adsCustomer(BasicDBObject adsCustomer) {
		super();
		this.adsCustomerId = adsCustomer.getInt("adsCustomerId");
		this.adsCustomerName =adsCustomer.getString("adsCustomerName");
		this.adsCustomerPhone =adsCustomer.getString("adsCustomerPhone");
	}
	@Override
	public String toString() {
		return "adsCustomer [adsCustomerId=" + adsCustomerId + ", adsCustomerName=" + adsCustomerName
				+ ", adsCustomerPhone=" + adsCustomerPhone + "]";
	}
}
