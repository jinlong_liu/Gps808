package com.hx.gps.service;

import java.text.SimpleDateFormat;
import java.util.List;

import com.hx.gps.entities.Accident;
import com.hx.gps.entities.Accidenttype;
import com.hx.gps.entities.Blacklist;
import com.hx.gps.entities.Breakrule;
import com.hx.gps.entities.Check;
import com.hx.gps.entities.Company;
import com.hx.gps.entities.Driver;
import com.hx.gps.entities.Fare;
import com.hx.gps.entities.Insure;
import com.hx.gps.entities.IsuCache;
import com.hx.gps.entities.Power;
import com.hx.gps.entities.Role;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.Taxitype;
import com.hx.gps.entities.User;
import com.hx.gps.entities.Yearinspect;
import com.hx.gps.entities.tool.DatatablesQuery;
import com.hx.gps.entities.tool.DatatablesView;
import com.hx.gps.entities.tool.DriverVO;
import com.hx.gps.entities.tool.InfoQuery;
import com.hx.gps.entities.tool.InfoQuery2;
import com.hx.gps.entities.tool.MeterState;
import com.hx.gps.entities.tool.TerminalState;
import com.hx.gps.entities.tool.videoState;

//信息管理
public interface InfoService {
	// 查询所有用户
	List<User> getAllUser();

	// 新增用户
	boolean saveUser(User user);

	// 更新用户
	boolean updateUser(User user, String old);

	// 更新用户状态
	boolean updateState(User user);

	// 删除用户
	void deleteUser(User user);

	// 查询所有角色
	List<Role> getAllRoleByPower();

	// 新增角色
	void saveRole(Role role);

	// 更新角色
	void updateRole(Role role);

	// 删除角色
	void deleteRole(Role role);

	// 新增权限
	void addPower(Power power);

	// 删除权限
	void deletePowerByID(Integer powerID);

	// 查询权限
	Power getPowerByID(Integer powerID);

	// 查询公司通过用户名
	Company getComByUser(String name);

	// 查询所有公司
	List<Company> getAllCompany();

	// 保存公司信息
	void saveCompany(Company company);

	// 更新公司信息
	void updateCompany(Company company);

	// 删除公司信息
	public void deleteCompany(Company company) throws Exception;

	/**
	 * 车辆信息管理相关
	 * 
	 * @param infoQuery
	 * @return
	 */
	// 多条件查询车辆信息
	List<Taxi> getTaxi(InfoQuery infoQuery);

	// 多条件查询车辆数量
	int getTaxiNumber(InfoQuery infoQuery);

	// 新增终端号（Mongo）
	boolean saveTaxiId(Taxi taxi);

	// 新增车辆
	String saveTaxi(Taxi taxi);

	// 批量新增车辆
	List<Integer> uploadTaxi(String xlsName, SimpleDateFormat fmt, int rowCount, int success, int retry)
			throws Exception;

	// 更新车辆
	void updateTaxi(Taxi taxi);

	// 删除车辆
	void deleteTaxi(Taxi taxi);

	// 定时任务
	void timeTask();

	// 更新车辆手机号
	void updateIsu(String isu, String isu2);

	// 根据公司id 获取车辆信息
	String[] getTaxiByCom(int id);

	// 删除mongo手机号
	void deleteIsu(String Isu);

	// 插入缓存表
	void addToIsucache(Taxi taxi, int type);

	void addToIsucache(Taxi taxi, String isu2, int type);

	// 查询缓存表
	List<IsuCache> getAllIsuCache();

	// 查询所有车辆类型
	List<Taxitype> getAllTaxiType();

	// 新增车辆类型
	boolean saveTaxiType(Taxitype taxitype);

	// 更新车辆类型
	void updateTaxiType(Taxitype taxitype);

	// 删除车辆类型
	void deleteTaxiType(Taxitype taxitype);

	// 多条件查询从业人员
	List<Driver> getDriver(InfoQuery infoQuery);
	
	// 后台分页式多条件查询从业人员
	DatatablesView<DriverVO> listDrivers(DatatablesQuery datatablesQuery, InfoQuery infoQuery);

	// 新增从业人员
	void saveDriver(Driver driver);

	// 更
	void updateDriver(Driver driver);

	// 删
	void deleteDriver(Driver driver);

	// 根据TaxiNum找到相应的taxiId
	int getTaxiIdBytaxiNum(InfoQuery infoQuery);

	// 查询所有的车
	List<Taxi> getAllTaxi();

	// 多条件查询保险信息
	List<Insure> getInsure(InfoQuery infoQuery);

	// 新增保险信息
	void saveInsure(Insure insure);

	// 更 保险信息
	void updateInsure(Insure insure);

	// 删 保险信息
	void deleteInsure(Insure insure);

	// 查询黑名单
	List<Blacklist> getBlacklist(InfoQuery2 infoQuery);

	// 保存黑名单
	void saveBlacklist(Blacklist blacklist);

	// 更新黑名单
	void updateBlacklist(Blacklist blacklist);

	// 删除黑名单
	void deleteBlacklist(Blacklist blacklist);

	/**
	 * 稽查信息
	 */
	// 查
	List<Check> getCheck(InfoQuery2 infoQuery);

	// 增
	void saveCheck(Check check);

	// 更
	void updateCheck(Check check);

	// 删
	void deleteCheck(Check check);

	/**
	 * 运价信息
	 */
	List<Fare> getFare(String type);

	void saveFare(Fare fare);

	void updateFare(Fare fare);

	void deleteFare(Fare fare);

	/**
	 * 违章信息
	 */
	List<Breakrule> getBreakrule(InfoQuery2 infoQuery2);

	void saveBreakrule(Breakrule breakrule);

	void updateBreakrule(Breakrule breakrule);

	void deleteBreakrule(Breakrule breakrule);

	/**
	 * 年审管理
	 */
	List<Yearinspect> getYearinspect(InfoQuery2 infoQuery2);

	void saveYearinspect(Yearinspect yearinspect);

	void updateYearinspect(Yearinspect yearinspect);

	void deleteYearinspect(Yearinspect yearinspect);

	// 获取设备状态
	List<videoState> getIsuVideoState(String isuNum);

	// 获取权限下的低级别用户
	List<User> getUserByCom(Company company);

	// 查询所有事故信息
	List<Accident> listAccidents(InfoQuery query);

	//
	void saveAccident(Accident accident) throws Exception;

	// 删除事故
	void deleteAccident(Accident accident);

	// 获取所有事故类型
	List<Accidenttype> getAccidentType();

	/**
<<<<<<< .mine
	 * @param accidentType
	 *            新增事故类型
=======
	 * @return 事故类型管理
>>>>>>> .r367
	 */
	List<Accidenttype> getAllAccidentType();

	/**
	 * @param accidenttype
	 * @return
	 */
	boolean saveAccidentType(Accidenttype accidenttype);

	/**
	 * @param accidenttype
	 */
	void updateAccidentType(Accidenttype accidenttype);

	/**
	 * @param accidenttype
	 */
	void deleteAccidentType(Accidenttype accidenttype);

	/**
	 * @param accident
	 */
	void updateAccident(Accident accident);
	//获取计价器实时状态
	List<MeterState> getMeterState(String taxiNum);
	//获取终端实时状态
	List<TerminalState> getTerminalState(String taxiNum);
	//批量上传及在线编辑车辆信息
	List<Taxi> uploadAndEditTaxi(String string, SimpleDateFormat fmt, int rowCount) throws Exception;
	//批量保存车辆
	String saveEditTaxis(List<Taxi> taxis);
	//验证公司是否存在
	boolean haveCompany(Company company);
	//获取毕节旗下所有公司
	List<Company> getAllCompanyBj();
	//审核从业人员
	void ApproalDriverById(Integer id, String state);
}
