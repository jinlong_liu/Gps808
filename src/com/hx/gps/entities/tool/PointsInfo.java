package com.hx.gps.entities.tool;

import java.util.ArrayList;
import java.util.List;

import com.mongodb.BasicDBObject;

/**
 * @author 作者 dream:
 * @version 创建时间：2017年6月13日 下午6:12:11 天道丶放飞梦想
 */
public class PointsInfo {
	// id
	private Integer serial;
	// 点总数
	private Integer pointsNum;
	// 点集合
	private List<Point> point = new ArrayList<>();

	//
	public Integer getPointsNum() {
		return pointsNum;
	}

	public void setPointsNum(Integer pointsNum) {
		this.pointsNum = pointsNum;
	}

	public List<Point> getPoint() {
		return point;
	}

	public void setPoint(List<Point> point) {
		this.point = point;
	}

	public Integer getSerial() {
		return serial;
	}

	public void setSerial(Integer serial) {
		this.serial = serial;
	}

	// 构造
	public PointsInfo() {
		super();
	}

	public PointsInfo(BasicDBObject finds) {
		// 获取到各参数
		BasicDBObject points = new BasicDBObject();
		List<Point> pointAll = new ArrayList<>();
		int serial = finds.getInt("serial");
		this.serial = serial;

		int number = finds.getInt("pointsNum");
		this.pointsNum = number;

		// 获取每个点
		if (finds.containsField("points")) {
			points = (BasicDBObject) finds.get("points");
			for (int i = 0; i < number; i++) {
				// 取到键值对
				String tempPoint = points.getString("point" + i);
				// 拆分成点
				String[] details = tempPoint.split(",");
				// 转换格式
				double lat = Double.parseDouble(details[0]);
				double lon = Double.parseDouble(details[1]);
				// 填充到集合中
				Point point = new Point(lon, lat);
				pointAll.add(point);
			}
			this.point = pointAll;
		}

	}

	// toString 要重写成json格式
	@Override
	public String toString() {
		return "PointsInfo [serial=" + serial + ", pointsNum=" + pointsNum + ", point=" + point + "]";
	}

}
