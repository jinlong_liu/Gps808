package com.hx.gps.entities;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Graph entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "graph" )
public class Graph implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer type;
	private Integer pointNum;
	private String circle;
	private Double radius;
	private String oneP;
	private String twoP;
	private String threeP;
	private String fourP;
	private String fiveP;
	private String sixP;
	private Set<Preinfo> preinfos = new HashSet<Preinfo>(0);

	// Constructors

	/** default constructor */
	public Graph() {
	}

	/** minimal constructor */
	public Graph(Integer type) {
		this.type = type;
	}

	/** full constructor */
	public Graph(Integer type, Integer pointNum, String circle, Double radius,
			String oneP, String twoP, String threeP, String fourP,
			String fiveP, String sixP, Set<Preinfo> preinfos) {
		this.type = type;
		this.pointNum = pointNum;
		this.circle = circle;
		this.radius = radius;
		this.oneP = oneP;
		this.twoP = twoP;
		this.threeP = threeP;
		this.fourP = fourP;
		this.fiveP = fiveP;
		this.sixP = sixP;
		this.preinfos = preinfos;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "type", nullable = false)
	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "pointNum")
	public Integer getPointNum() {
		return this.pointNum;
	}

	public void setPointNum(Integer pointNum) {
		this.pointNum = pointNum;
	}

	@Column(name = "circle", length = 30)
	public String getCircle() {
		return this.circle;
	}

	public void setCircle(String circle) {
		this.circle = circle;
	}

	@Column(name = "radius", precision = 22, scale = 0)
	public Double getRadius() {
		return this.radius;
	}

	public void setRadius(Double radius) {
		this.radius = radius;
	}

	@Column(name = "oneP", length = 30)
	public String getOneP() {
		return this.oneP;
	}

	public void setOneP(String oneP) {
		this.oneP = oneP;
	}

	@Column(name = "twoP", length = 30)
	public String getTwoP() {
		return this.twoP;
	}

	public void setTwoP(String twoP) {
		this.twoP = twoP;
	}

	@Column(name = "threeP", length = 30)
	public String getThreeP() {
		return this.threeP;
	}

	public void setThreeP(String threeP) {
		this.threeP = threeP;
	}

	@Column(name = "fourP", length = 30)
	public String getFourP() {
		return this.fourP;
	}

	public void setFourP(String fourP) {
		this.fourP = fourP;
	}

	@Column(name = "fiveP", length = 30)
	public String getFiveP() {
		return this.fiveP;
	}

	public void setFiveP(String fiveP) {
		this.fiveP = fiveP;
	}

	@Column(name = "sixP", length = 30)
	public String getSixP() {
		return this.sixP;
	}

	public void setSixP(String sixP) {
		this.sixP = sixP;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "graph")
	public Set<Preinfo> getPreinfos() {
		return this.preinfos;
	}

	public void setPreinfos(Set<Preinfo> preinfos) {
		this.preinfos = preinfos;
	}

	@Override
	public String toString() {
		return "Graph [id=" + id + ", type=" + type + ", pointNum=" + pointNum
				+ ", circle=" + circle + ", radius=" + radius + ", oneP="
				+ oneP + ", twoP=" + twoP + ", threeP=" + threeP + ", fourP="
				+ fourP + ", fiveP=" + fiveP + ", sixP=" + sixP + "]";
	}

}