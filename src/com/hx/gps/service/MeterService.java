package com.hx.gps.service;

import java.util.List;

import com.hx.gps.entities.tool.Meter;

/**
 * 营运信息表service
 * @author 高杨
 *
 */
public interface MeterService {

	/**
	 * 根据时间 车牌号 租金获取营运信息
	 * 
	 * @param fare
	 * @param startDate
	 * @param endDate
	 * @param taxiNum
	 * @return
	 * @author 高杨
	 */
	List<Meter> getMeterByFare(String fare, String startDate, String endDate,
			String taxiNum);

	/**
	 * 根据时间 车牌号 浮动租金获取营运信息
	 * @param floatingFare1
	 * @param floatingFare2
	 * @param startDate
	 * @param endDate
	 * @param taxiNum
	 * @return
	 * @author 高杨
	 */
	List<Meter> getMeterByFloatingFare(String floatingFare1,
			String floatingFare2, String startDate, String endDate,
			String taxiNum);

}
