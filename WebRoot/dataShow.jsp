<%--
  Created by IntelliJ IDEA.
  User: 刘子龙
  Date: 2020/11/25
  Time: 18:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>车辆综合管控平台</title>
    <meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
    <meta name="description" content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/style2.0.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script>
        $(function () {
            $('.myscroll').myScroll({
                speed: 60, //数值越大，速度越慢
                rowHeight: 38 //li的高度
            });
        })

    </script>
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/style.min.css?v=4.0.0" rel="stylesheet">
    <base target="_blank">

</head>

<body>
<!--头部-->
<header id="header">
    <div class="header">
        <h1 class="">车辆综合管控平台</h1>
    </div>
</header>

<!--内容-->
<section id="data_content">

    <div class="data_info">
        <div class="info_1 fl">
            <div class="text_1">
                <div class="fl">
                    <img src="img/info_1.png" alt="">
                    <div class="fl count">
                        <p>车辆总数</p>
                        <p class="conuts" id="carNums"></p>
                    </div>
                </div>
                <div class="fl">
                    <img src="img/info_2.png" alt="">
                    <div class="fl count">
                        <p>在线车辆数</p>
                        <p class="conuts"id="onlineCar"></p>
                    </div>
                </div>
                <div class="fl">
                    <img src="img/info_3.png" alt="">
                    <div class="fl count">
                        <p>在线率</p>
                        <p class="conuts">0.00%</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="info_2 fl">
            <div class="text_2">
              <div class="fl">
                  <img src="img/info_4.png" alt="">
               <div class="fl count">
                 <p>今日报警总数</p>
                 <p class="conuts" id="alarmNum"></p>
               </div>
            </div>
            <div class="fl">
                 <img src="img/info_5.png" alt="">
               <div class="fl count">
                 <p>今日未处理报警总数</p>
                 <p class="conuts" id="alarmNums"></p>
               </div>
             </div>

            </div>
        </div>
        <div class="info_3 fr">
            <div class="text_2">
                                <div class="fl">
                                    <img src="img/info_6.png" alt="">
                                    <div class="fl count">
                                        <p>紧急报警数</p>
                                        <p class="conuts">1</p>
                                    </div>
                                </div>
                                <div class="fl">
                                    <img src="img/info_7.png" alt="">
                                    <div class="fl count">
                                        <p>磁盘报警数</p>
                                        <p class="conuts">2</p>
                                    </div>
                                </div>

            </div>
        </div>
    </div>

    <div class="data_content">
        <!--左边-->
        <div class="data_left fl">
            <div class="left_top">
                <!--/*左边第一部分*/-->
                <div class="info public">
                    <div class="min-title">
                        <span>车辆总数</span>
                    </div>
                    <div id="char4" style="width:100%;height: 100%;"></div>
                    <div class="boxfoot"></div>
                </div>
                <!--左边第二部分-->
                <div class="top_bottom public">
                    <div class="min-title">
                        <span>报警类型</span>
                    </div>
                    <div id="echarts_1" style="width:100%;height: 100%;"></div>
                    <div class="boxfoot"></div>
                </div>
                <!--左边第三部分-->
                <div class="left_bottom public">
                    <div class="min-title">
                        <span>车辆状态</span>
                    </div>
                    <div id="chart_2" style="width:100%;height: 100%;"></div>
                    <div class="boxfoot"></div>
                </div>
            </div>
        </div>
        <!--中间-->
        <div class="data_c fl">
            <div class="data_c_1">


                <div id="contPar" class="contPar">
                    <div id="page1"  style="z-index:1">
                        <div class="title0">车辆综合管控大数据平台</div>
                        <div id="drag-container" style="z-index: 1">
                            <div id="spin-container">

                                <img alt="" class="infoBtn" src="picture/page1_0.png">
                                <img alt="" class="" src="picture/page1_1.png">
                                <img alt="" class="" src="picture/page1_2.png">
                                <img alt="" class="" src="picture/page1_1.png">
                                <img alt="" class="" src="picture/page1_2.png">

                            </div>
                            <div id="ground"></div>
                        </div>

                        <img alt="" class="img3 png" src="img/newdz.png" >
                    </div>
                </div>


            </div>
            <div class="data_c_2 public">

                <div id="echart4" style="width: 100%;height: 100%;padding-top: 10px;"></div>
                <div class="boxfoot"></div>
            </div>
        </div>
        <!--右边-->
        <div class="data_right fr">
            <div class="right_top">
                <!--/*右边第一部分*/-->
                <div class="right_info public">
                    <div class="min-title">
                        <span>报警日志</span>
                    </div>
                    <div class="echart wenzi">
<%--                        <div class="gun">--%>
<%--                            <span>小型车</span>--%>
<%--                            <span>中型车</span>--%>
<%--                            <span>大型车</span>--%>
<%--                        </div>--%>
                        <div id="FontScroll" class="myscroll">
                            <ul>
                                <li>
                                    <div class="fontInner clearfix">
                                        <span >磁盘已满</span>
<%--                                        <span>65万</span>--%>
<%--                                        <span>182万</span>--%>
                                    </div>
                                </li>
                                <li>
                                    <div class="fontInner clearfix">
                                        <span >GNSS天线未接或被剪断</span>
<%--                                        <span>65万</span>--%>
<%--                                        <span>182万</span>--%>
                                    </div>
                                </li>
                                <li>
                                    <div class="fontInner clearfix">
                                        <span >未检测到磁盘</span>
<%--                                        <span>65万</span>--%>
<%--                                        <span>182万</span>--%>
                                    </div>
                                </li>
                                <li>
                                    <div class="fontInner clearfix">
                                        <span >紧急报警</span>
<%--                                        <span>65万</span>--%>
<%--                                        <span>182万</span>--%>
                                    </div>
                                </li>
                                <li>
                                    <div class="fontInner clearfix">
                                        <span >终端主电源欠压</span>
<%--                                        <span>65万</span>--%>
<%--                                        <span>182万</span>--%>
                                    </div>
                                </li>
                                <li>
                                    <div class="fontInner clearfix">
                                        <span >磁盘数据错误</span>
<%--                                        <span>65万</span>--%>
<%--                                        <span>182万</span>--%>
                                    </div>
                                </li><li>
                                <div class="fontInner clearfix">
                                    <span >终端主电源掉电</span>
<%--                                    <span>65万</span>--%>
<%--                                    <span>182万</span>--%>
                                </div>
                            </li>
                                <li>
                                    <div class="fontInner clearfix">
                                        <span >摄像头故障</span>
<%--                                        <span>65万</span>--%>
<%--                                        <span>182万</span>--%>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="boxfoot"></div>
                </div>
                <!--右边第二部分-->
                <div class="right_content public">
                    <div class="min-title">
                        <span>抓拍车辆</span>
                    </div>
                    <div id="chart_3" style="width:100%;height: 100%;"></div>
                    <div class="boxfoot"></div>
                </div>
                <!--右边第三部分-->
                <div class="right_bottom public">
                    <div class="min-title">
                        <span>车辆类型</span>
                    </div>
                    <div id="echarts_2" style="width:100%;height: 100%;padding-top: 10px;"></div>
                    <div class="boxfoot"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="js/script.js"></script>
<script src="https://www.jq22.com/jquery/jquery-1.10.2.js"></script>
<script src="https://www.jq22.com/jquery/echarts-4.2.1.min.js"></script>
<script src="js/echart.js"></script>
<script src="js/fontscroll.js"></script>
<script>
    $(document).ready(function(){
        getAlarmNums();
        getAlarmNum();
        getOnlineAll();
        getOnlineCurrent();
    });
    function getAlarmNums() {
        $.ajax({
            type: "post",
            url: "getAlarmNums",
            async:true, // 异步请求
            cache:false, // 设置为 false 将不缓存此页面
            dataType: 'json', // 返回对象
            success: function(data) {
                console.log("ok"+data);
                $("#alarmNums").html(data);

            },
            error: function(data) {
                // 请求失败函数
                console.log(data);
            }
        })
    }
    function getAlarmNum() {
        $.ajax({
            type: "post",
            url: "getAlarmNum",
            async:true, // 异步请求
            cache:false, // 设置为 false 将不缓存此页面
            dataType: 'json', // 返回对象
            success: function(data) {
                console.log("ok"+data);
                $("#alarmNum").html(data);

            },
            error: function(data) {
                // 请求失败函数
                console.log(data);
            }
        })
    }

    function getOnlineAll() {
        $.ajax({
            type: "post",
            url: "getOnlineAll",
            async:true, // 异步请求
            cache:false, // 设置为 false 将不缓存此页面
            dataType: 'json', // 返回对象
            success: function(data) {
                console.log("ok"+data);
                $("#carNums").html(data);

            },
            error: function(data) {
                // 请求失败函数
                console.log(data);
            }
        })
    }
    function getOnlineCurrent() {
        $.ajax({
            type: "post",
            url: "getOnlineCurrent",
            async:true, // 异步请求
            cache:false, // 设置为 false 将不缓存此页面
            dataType: 'json', // 返回对象
            success: function(data) {
                console.log("ok"+data);
                $("#onlineCar").html(data);

            },
            error: function(data) {
                // 请求失败函数
                console.log(data);
            }
        })
    }

</script>
</body>

</html>