package com.hx.gps.dao;

import java.util.List;

import com.hx.gps.entities.Graph;
import com.hx.gps.entities.Preinfo;
import com.hx.gps.entities.tool.PointsInfo;

public interface MapDao {
	
	//添加地图信息,并返回插入的id
	public Graph addGraph(Graph graph);
	
	//添加预设信息
	public void addPreInfo(Preinfo preinfo);
	//删除预设信息
	public void deletePreInfo(Preinfo preinfo);
	//查询所有的预设信息
	public List<Preinfo> findAllPreinfoByDate();
	//根据id查询预设信息
	public Preinfo findPreInfoById(Integer id);
	/**
	 * 下发区域相关
	 * @return
	 */
	//查找
	public List<PointsInfo> findAllPreinfo2();
	//删除
	public boolean deletePreInfo2(Integer serial);
	//添加
	public void addPreInfo2(PointsInfo pointsInfo);
	
}
