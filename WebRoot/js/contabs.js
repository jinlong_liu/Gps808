$(function() {
    function f(l) {
        var k = 0;
        $(l).each(function() {
            k += $(this).outerWidth(true)   
        });
        return k
    }
    function g(n) {
        var o = f($(n).prevAll()),		//$(n).prevAll()  获取当前元素前面的同胞元素
        q = f($(n).nextAll());
        var l = f($(".content-tabs").children().not(".J_menuTabs"));
        var k = $(".content-tabs").outerWidth(true) - l;
        var p = 0;
        if ($(".page-tabs-content").outerWidth() < k) {
            p = 0       
        } else {
            if (q <= (k - $(n).outerWidth(true) - $(n).next().outerWidth(true))) {
                if ((k - $(n).next().outerWidth(true)) > q) {
                    p = o;
                    var m = n;
                    while ((p - $(m).outerWidth()) > ($(".page-tabs-content").outerWidth() - k)) {
                        p -= $(m).prev().outerWidth();
                        m = $(m).prev()                   
                    }         
                }
            } else {
                if (o > (k - $(n).outerWidth(true) - $(n).prev().outerWidth(true))) {
                    p = o - $(n).prev().outerWidth(true)               
                }             
            } 
        }
	    $(".page-tabs-content").animate({
	        marginLeft: 0 - p + "px" 
	    },"fast")   
    }
    
    /**
     * 向左切换
     */
    function toLeft() {
    	var targetSize = $(".page-tabs-content").children(".active").prevAll().size() + 1;//获取当前选项卡的位置
    	var targetId = $(".page-tabs-content").children(".active").prevAll().eq(0).data("id")//获取目标选项卡的data-id
    	if(targetSize == 1){
    		return false;
    	}else{
            $(".page-tabs-content").children('[data-id="' + targetId +'"]').each(function(){
            	//显示当前J_iframe元素、并隐藏其他J_iframe元素
            	$('.J_iframe[data-id="' + $(this).data("id") + '"]').show().siblings(".J_iframe").hide();
            	//为当前选项卡添加焦点类，并移除其他的焦点
                $(this).addClass("active").siblings(".J_menuTab").removeClass("active")
            })
    	}
    }
    /**
     * 向右切换
     */
    function toRight() {
    	var totalSize = $(".page-tabs-content").children("[data-id]").size();	//获取总的选项卡的个数(从1开始)
    	var targetSize = $(".page-tabs-content").children(".active").prevAll().size() + 1;//获取当前选项卡的位置
    	var targetId = $(".page-tabs-content").children(".active").next().data("id")//获取目标选项卡的data-id
    	if(targetSize == totalSize){
    		return false;
    	}else{
            $(".page-tabs-content").children('[data-id="' + targetId +'"]').each(function(){
            	$('.J_iframe[data-id="' + $(this).data("id") + '"]').show().siblings(".J_iframe").hide();
                $(this).addClass("active").siblings(".J_menuTab").removeClass("active")
            })
    	}
    }
    
    //跳转到视频监控界面
    function toNewTest(){
		markerClusterer.clearMarkers();
		var taxiNum = $("#show1").val();
		if (taxiNum == "") {
			swal({
				title : "请先选择车辆",
				type : "warning"
			});
		}else if (taxiNum.length > 7) {
			swal({
				title : "只允许选择一辆车",
				type : "warning"
			});
			return;
		} else {
			
			var o = "getTreeOfTempNew",
    			l = "视频监控",
    			m = 4,							//m应为要打开的界面在左侧列表的排序(从0开始)，视频监控为4
    			k = true;
			//window.location.href = "getTreeOfTemp3?taxiNum=" + taxiNum;//传到后台controller处理
			//判断在选项卡中是否已有该选项
			$(window.parent.document).find(".J_menuTab").each(function() {
	            if ($(this).data("id") == o) {			//判断是否有该选项
	            	//当前选项卡已存在
	                if (!$(this).hasClass("active")) {	//判断该选项卡是否为当前焦点选项	
	                	//当前选项不是焦点选项
	                    $(this).addClass("active").siblings(".J_menuTab").removeClass("active");
	                    g(this);
	                    $(window.parent.document).find(".J_mainContent .J_iframe").each(function() {
	                        if ($(this).data("id") == o) {
	                            $(this).show().siblings(".J_iframe").hide();
	                            return false                  
	                        }                    
	                    })
	                }
	                //当前选项存在且是焦点选项
	                k = false;
	                return false
	            }        
	        });
	    	if (k) {
	            var p = '<a href="javascript:;" class="active J_menuTab" data-id="' + o + '">' + l + ' <i class="fa fa-times-circle"></i></a>';           
	            $(window.parent.document).find(".J_menuTab").removeClass("active");          
	            var n = '<iframe class="J_iframe" name="iframe' + m + '" width="100%" height="100%" src="' + o + '" frameborder="0" data-id="' + o + '" seamless></iframe>';           
	            $(window.parent.document).find(".J_mainContent").find("iframe.J_iframe").hide().parents(".J_mainContent").append(n);            
	            $(window.parent.document).find(".J_menuTabs .page-tabs-content").append(p);
	            g($(window.parent.document).find(".J_menuTab.active"))        
	        }
	        if(!k){
	        	$(window.parent.document).find(".J_mainContent .J_iframe").each(function(){
	                if($(this).css('display') == 'inline'){
	                    $(this).attr('src', $(this).attr('src'));              
	                }; 
	            })        
	        }
	        //向后台发送车牌号
	        $.post("/GpsManage/getTreeOfTempNew","taxiNum=" + taxiNum,function(data){
	        	console.log(JSON.stringify(data));
	        });
	        return false  
		}
    }
    /**
     * 跳转到车辆实时监控界面
     */
    function toTempMoni(){
    	var o = "getTreeOfTemp",
    		l = "车辆实时监控",
    		m = 0,							//m应为要打开的界面在左侧列表的顺序(从0开始)，车辆实时监控为4
    		k = true;
			//判断在选项卡中是否已有该选项
    		$(window.parent.document).find(".J_menuTab").each(function() {
	            if ($(this).data("id") == o) {		//判断是否有该选项
	            	//当前选项存在
	                if (!$(this).hasClass("active")) {	//判断该选项卡是否为当前焦点选项	
	                	//当前选项不是焦点选项
	                    $(this).addClass("active").siblings(".J_menuTab").removeClass("active");
	                    g(this);
	                    $(window.parent.document).find(".J_mainContent .J_iframe").each(function() {
	                        if ($(this).data("id") == o) {
	                            $(this).show().siblings(".J_iframe").hide();
	                            return false                        
	                        }                    
	                    })             
	                }
	                //当前选项存在且是焦点选项
	                k = false;
	                return false
	            }        
	        });
	    	if (k) {     		
	            var p = '<a href="javascript:;" class="active J_menuTab" data-id="' + o + '">' + l + ' <i class="fa fa-times-circle"></i></a>';           
	            $(window.parent.document).find(".J_menuTab").removeClass("active");          
	            var n = '<iframe class="J_iframe" name="iframe' + m + '" width="100%" height="100%" src="' + o + '" frameborder="0" data-id="' + o + '" seamless></iframe>';           
	            $(window.parent.document).find(".J_mainContent").find("iframe.J_iframe").hide().parents(".J_mainContent").append(n);            
	            $(window.parent.document).find(".J_menuTabs .page-tabs-content").append(p);
	            g($(".J_menuTab.active"))        
	        }
	        if(!k){
	        	$(window.parent.document).find(".J_mainContent .J_iframe").each(function(){
	                if($(this).css('display') == 'inline'){
	                    $(this).attr('src', $(this).attr('src'));              
	                }; 
	            })        
	        }
	        return false
    }
    
    /**
     * 新增选项卡
     */
    function c() {   
        var o = $(this).attr("href"),
        	m = $(this).data("index"),				//m应为要打开的界面在左侧列表的顺序(从0开始)
        	l = $.trim($(this).text()),
        	k = true;
        if (o == undefined || $.trim(o).length == 0) {
            return false     
        }
        $(".J_menuTab").each(function() {
            if ($(this).data("id") == o) {
                if (!$(this).hasClass("active")) {
                    $(this).addClass("active").siblings(".J_menuTab").removeClass("active");
                    g(this);
                    $(".J_mainContent .J_iframe").each(function() {
                        if ($(this).data("id") == o) {
                            $(this).show().siblings(".J_iframe").hide();
                            return false                        
                        }                    
                    })
                }
                k = false;
                return false           
            }        
        });
        if (k) {        
            var p = '<a href="javascript:;" class="active J_menuTab" data-id="' + o + '">' + l + ' <i class="fa fa-times-circle"></i></a>';           
            $(".J_menuTab").removeClass("active");          
            var n = '<iframe class="J_iframe" name="iframe' + m + '" width="100%" height="100%" src="' + o + '" frameborder="0" data-id="' + o + '" seamless></iframe>';           
            $(".J_mainContent").find("iframe.J_iframe").hide().parents(".J_mainContent").append(n);            
            $(".J_menuTabs .page-tabs-content").append(p);
            g($(".J_menuTab.active"))        
        }
        if(!k){
            $(".J_mainContent .J_iframe").each(function(){
                if($(this).css('display') == 'inline'){
                    $(this).attr('src', $(this).attr('src'));              
                }; 
            })     
        }
        return false    
    }
    
    function h() {
        var m = $(this).parents(".J_menuTab").data("id");
        var l = $(this).parents(".J_menuTab").width();
        if ($(this).parents(".J_menuTab").hasClass("active")) {
            if ($(this).parents(".J_menuTab").next(".J_menuTab").size()) { 
                var k = $(this).parents(".J_menuTab").next(".J_menuTab:eq(0)").data("id");
                $(this).parents(".J_menuTab").next(".J_menuTab:eq(0)").addClass("active");
                $(".J_mainContent .J_iframe").each(function() {
                    if ($(this).data("id") == k) {
                        $(this).show().siblings(".J_iframe").hide();
                        return false                     
                    }                
                });
                var n = parseInt($(".page-tabs-content").css("margin-left"));
                if (n < 0) {
                    $(".page-tabs-content").animate({marginLeft: (n + l) + "px"},"fast")                
                }
                $(this).parents(".J_menuTab").remove();
                $(".J_mainContent .J_iframe").each(function() {
                    if ($(this).data("id") == m) {
                        $(this).remove();
                        return false                  
                    }                
                })             
            }
            if ($(this).parents(".J_menuTab").prev(".J_menuTab").size()) {
                var k = $(this).parents(".J_menuTab").prev(".J_menuTab:last").data("id");
                $(this).parents(".J_menuTab").prev(".J_menuTab:last").addClass("active");
                $(".J_mainContent .J_iframe").each(function() {
                    if ($(this).data("id") == k) {
                        $(this).show().siblings(".J_iframe").hide();
                        return false                    
                    }                
                });
                $(this).parents(".J_menuTab").remove();
                $(".J_mainContent .J_iframe").each(function() {
                    if ($(this).data("id") == m) {
                        $(this).remove();
                        return false
                    }
                })
            }
        } else {
            $(this).parents(".J_menuTab").remove();
            $(".J_mainContent .J_iframe").each(function() {
                if ($(this).data("id") == m) {
                    $(this).remove();
                    return false
                }
            });
            g($(".J_menuTab.active"))
        }
        return false
    }
    
    function i() {
        $(".page-tabs-content").children("[data-id]").not(":first").not(".active").each(function() { 
            $('.J_iframe[data-id="' + $(this).data("id") + '"]').remove();
            $(this).remove()        
        });
        $(".page-tabs-content").css("margin-left", "0")    
    }
    
    function j() {
    	g($(".J_menuTab.active"))  
    }
    
    function e() {
        if (!$(this).hasClass("active")) {
            var k = $(this).data("id");
            $(".J_mainContent .J_iframe").each(function() {
                if ($(this).data("id") == k) {
                    $(this).show().siblings(".J_iframe").hide();
                    return false             
                }            
            });
            $(this).addClass("active").siblings(".J_menuTab").removeClass("active");
            g(this)
        }    
    }
    
    function d() { 
        var l = $('.J_iframe[data-id="' + $(this).data("id") + '"]');
        var k = l.attr("src")    
    }
    
    
    $(".J_menuItem").each(function(k) {
        if (!$(this).attr("data-index")) {
            $(this).attr("data-index", k)      
        }
    });
    $(".J_toTempMoni").on("click", toTempMoni);			//跳转到车辆实时监控界面
    $(".J_toNewTest").on("click", toNewTest);			//跳转到视频监控界面
    $(".J_tabShowActive").on("click", j);				//定位当前选项卡
    $(".J_menuTabs").on("click", ".J_menuTab i", h);	//
    $(".J_menuTabs").on("click", ".J_menuTab", e);		//切换到选择的页面
    $(".J_menuTabs").on("dblclick", ".J_menuTab", d);	//双击选项卡重新加载该页面
    $(".J_menuItem").on("click", c);					//新建选项卡
    $(".J_tabLeft").on("click", toLeft);				//向左切换选项卡
    $(".J_tabRight").on("click", toRight);				//向右切换选项卡
    $(".J_tabCloseOther").on("click", i);				//关闭其他选项卡(即没有获得焦点的选项卡)
    $(".J_tabCloseAll").on("click", function() {		//关闭所有的选项卡 
        $(".page-tabs-content").children("[data-id]").not(":first").each(function() {
            $('.J_iframe[data-id="' + $(this).data("id") + '"]').remove();
            $(this).remove()        
        });
        $(".page-tabs-content").children("[data-id]:first").each(function() { 
            $('.J_iframe[data-id="' + $(this).data("id") + '"]').show();
            $(this).addClass("active")         
        });
        $(".page-tabs-content").css("margin-left", "0")     
    });
    $('.J_tabGo').on("click", function(){				//前进
        $(".J_mainContent .J_iframe").each(function(){
            if($(this).css('display') == 'inline'){
                $(this)[0].contentWindow.history.forward();            
            };         
        })     
    });
    $('.J_tabBack').on("click", function(){				//后退()
        $(".J_mainContent .J_iframe").each(function(){
            if($(this).css('display') == 'inline'){
                $(this)[0].contentWindow.history.back();             
            };         
        })    
    });
    $('.J_tabFresh').on("click", function(){ 			//刷新
        $(".J_mainContent .J_iframe").each(function(){
            if($(this).css('display') == 'inline'){
                $(this)[0].contentWindow.location.reload();            
            };         
        })   
    });
    
    
});