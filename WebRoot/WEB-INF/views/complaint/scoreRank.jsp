<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">
<!-- gy 添加表单验证 -->
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />

<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
<style type="text/css">
.form-control-gy {
	width: 30%;
	position:relative;
	left:650px;
	display: inline;
}
</style>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">

		
<!-- 点击添加按钮弹出来的form表单 -->
					<div class="modal inmodal fade" id="myModal5" tabindex="-1"
						role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<form id="addScorerank" class="form-horizontal" action="addOrUpdateScorerank" method="post">
								    <fieldset>
								        <div class="modal-body">
								            <div class="ibox-title">
								                <h5>添加分数等级对应信息<span style="color:red">（*为必填项） </span></h5>
								            </div>
								            <div class="ibox-content">
								                <div class="row">
														<!-- 分数上线 -->
											<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red">* </span>分数上限: </label>
												<div class="col-sm-7">
													<input type="text" class="form-control " name="top" autocomplete="off" />
												</div>
											</div>					
<!-- 								                    <div class="col-sm-6"> -->
<!-- 								                        <div class="form-group"> -->
<!-- 								                            <label class="col-sm-4 control-label">分数上限: </label> -->
<!-- 								                            <input class="form-control" type="text" name="top"/> -->
<!-- 								                        </div> -->
<!-- 								                    </div> -->
														<!-- 分数下限 -->
											<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red">* </span>分数下限: </label>
												<div class="col-sm-7">
													<input type="text" class="form-control" name="low"  autocomplete="off"/>
												</div>
											</div>	
<!-- 								                    <div class="col-sm-6"> -->
<!-- 								                        <div class="form-group"> -->
<!-- 								                            <label class="col-sm-4 control-label">分数下限:</label> -->
<!-- 								                            <input class="form-control" type="text" name="low"/> -->
<!-- 								                        </div> -->
<!-- 								                    </div> -->
														<!-- 分数下限 -->
											<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red">* </span>对应等级: </label>
												<div class="col-sm-7">
													<input type="text" class="form-control" name="rank" autocomplete="off" />
												</div>
											</div>									
<!-- 								                    <div class="col-sm-6"> -->
<!-- 								                        <div class="form-group"> -->
<!-- 								                            <label class="col-sm-4 control-label">对应等级:</label> -->
<!-- 								                            <input class="form-control" type="text" name="rank"/> -->
<!-- 								                        </div> -->
<!-- 								                    </div> -->
								
								    </div>
								    
								        <div class="modal-footer">
								            <button type="button" class="btn btn-white"
								                    data-dismiss="modal">关闭
								            </button>
								            <button type="submit" class="btn btn-primary">提交</button>
								        </div>
								        </div>
								        </div>
								        
								    </fieldset>
								</form>
							</div>
						</div>
					</div>

					<!-- 点击"修改"按钮弹出来的form表单 -->
					<div class="modal inmodal fade" id="myModal1" tabindex="-1"
						role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<form id="updateScorerank"  class="form-horizontal" action="addOrUpdateScorerank" method="post">
								    <fieldset>
								        <div class="modal-body">
								            <div class="ibox-title">
								                <h5>修改分数等级对应信息<span style="color:red">（* 为必填项）</span></h5>
								            </div>
								            <div class="ibox-content">
								                <input type="hidden" name="id" id="id">
								                <div class="row">
								
								                    
								                        <div class="form-group">
								                            <label class="col-sm-3 control-label"><span style="color:red">* </span>分数上限: </label>
								                           <div class="col-sm-7">
								                            <input class="form-control" type="text" name="top" id="top"/>
								                        </div>
								                    </div>
								                   
								                        <div class="form-group">
								                            <label class="col-sm-3 control-label"><span style="color:red">* </span>分数下限:</label>
								                             <div class="col-sm-7">
								                            <input class="form-control" type="text" name="low" id="low"/>
								                        </div>
								                    </div>
								                </div>
								                <div class="row">
								
								                    
								                        <div class="form-group">
								                            <label class="col-sm-3 control-label"><span style="color:red">* </span>对应等级:</label>
								                            <div class="col-sm-7">
								                            <input class="form-control" type="text" name="rank" id="rank"/>
								                        </div>
								                    </div>
								                </div>
								            </div>
								        </div>
								
								        <div class="modal-footer">
								            <button type="button" class="btn btn-white"
								                    data-dismiss="modal">关闭
								            </button>
								            <button type="submit" class="btn btn-primary">提交</button>
								        </div>
								    </fieldset>
								</form>
							</div>
						</div>
					</div>
		<div class="row">

			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>分数等级设置表</h5>
				</div>
				<div class="ibox-content" style="height: 780px">
				
						
					<table id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
						<thead>
						<tr><td>	<button type="button" class="btn btn-primary" data-toggle="modal"
						data-target="#myModal5">添&nbsp;&nbsp;&nbsp;加</button>&nbsp;</td></tr>
							<tr>
								<th>分数上限</th>
								<th>分数下限</th>
								<th>对应等级</th>
								<th>操作</th>
								
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${scoreRank}" var="scoreRank">
								<tr id="${scoreRank.id}">
									<td>${scoreRank.top}</td>
									<td>${scoreRank.low}</td>
									<td>${scoreRank.rank}</td>
									<td><button id="btn-update" type="button"
											class="btn btn-success btn-sm" data-toggle="modal"
										data-target="#myModal1"
											onclick="alterScore('${scoreRank.id}','${scoreRank.low}','${scoreRank.top}','${scoreRank.rank}')">更改</button>
										<button type="button" class="btn btn-danger btn-sm"
											onclick="submit('${scoreRank.id}')">删除</button></td>
								</tr>
							</c:forEach>
						</tbody>
				<tfoot>
							<tr>
								<td colspan="4">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
				<form id="deleteForm" action="delectScorerank" method="post">
					<input type="hidden" id="deleteId" name="id">
				</form>
			</div>
		</div>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>
	<script>
	//加载footable的功能
		$(document).ready(function() {
		$('#table1').DataTable({
			"pagingType" : "full_numbers"
		});
		//执行结果
		var result='<%=request.getAttribute("result")%>';
	if(result != null && result != "null"){
		if (result.indexOf("成功")>0) {
		swal({
			title : result,
			type : "success"
		});
	}else{
		swal({
			title :result,
			type : "warning"
		});
	}
	}
	});
	
	
		$(document).ready(function () {
	        $('#addScorerank').bootstrapValidator({
	            message: '这个值无效',
	            feedbackIcons: {
	                valid: 'glyphicon glyphicon-ok',
	                invalid: 'glyphicon glyphicon-remove',
	                validating: 'glyphicon glyphicon-refresh'
	            },
	            /*生效规则：字段值一旦变化就触发验证*/
	            live: 'enabled',
	            /*当表单验证不通过时，该按钮为disabled*/
	            submitButtons: 'button[type="submit"]',
	            fields: {
	
	                top: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写分数上限"
	                        },
	
	                        between: {
	                            min: 0,
	                            max: 100,
	                            message: '请输入0-100之间的整数'
	                        },
	                        numeric : {
								message : '请输入数字'
							}
	                    }
	                }, low: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写分数下限"
	                        },
	
	                        between: {
	                            min: 0,
	                            max: 100,
	                            message: '请输入0-100之间的整数'
	                        },
	                        numeric : {
								message : '请输入数字'
							}
	                    }
	                }, rank: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写等级名称"
	                        }
	                    }
	                }
	            }
	        });
	    });
	$(document).ready(function () {
	        $('#updateScorerank').bootstrapValidator({
	            message: '这个值无效',
	            feedbackIcons: {
	                valid: 'glyphicon glyphicon-ok',
	                invalid: 'glyphicon glyphicon-remove',
	                validating: 'glyphicon glyphicon-refresh'
	            },
	            /*生效规则：字段值一旦变化就触发验证*/
	            live: 'enabled',
	            /*当表单验证不通过时，该按钮为disabled*/
	            submitButtons: 'button[type="submit"]',
	            fields: {
	
	                top: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写分数上限"
	                        },
	
	                        between: {
	                            min: 0,
	                            max: 100,
	                            message: '请输入0-100之间的整数'
	                        },
	                        numeric : {
								message : '请输入数字'
							}
	                    }
	                }, low: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写分数下限"
	                        },
	
	                        between: {
	                            min: 0,
	                            max: 100,
	                            message: '请输入0-100之间的整数'
	                        },
	                        numeric : {
								message : '请输入数字'
							}
	                    }
	                }, rank: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写等级名称"
	                        }
	                    }
	                }
	            }
	        });
	    });
	
	//查看默认处理
		function alterScore(id,low,top,rank) {
			$("#id").val(id);
			$("#low").val(low);
			$("#top").val(top);
			$("#rank").val(rank);
		};
		
		function delect(id){
			$("#"+id).hide();
			var args = {
					"id":id,
				};
				var url="delectScorerank";
			$.post(url, args);
			 
		}
		
		function submit(id) {
			swal({
				title : "您确定执行该操作吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			},function() {
				  $("#deleteId").val(id);
					//提交表单
				  $("#deleteForm").submit();  
			});
		}
	</script>
</body>

</html>