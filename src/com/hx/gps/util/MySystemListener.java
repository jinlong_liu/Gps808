package com.hx.gps.util;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.bugull.mongo.BuguConnection;

public class MySystemListener implements ServletContextListener {
	@Override
	public void contextInitialized(ServletContextEvent event) {
		// 连接数据库
		// System.out.println("我先开始的");
		BuguConnection conn = BuguConnection.getInstance();
		//conn.connect("localhost", 60006, FarmConstant.MONGODB);
		conn.connect(FarmConstant.MONGOIP, FarmConstant.MONGOPORT, FarmConstant.MONGODB);
		// 建立定时任务 
		// Timer time=new Timer();
		// time.schedule(new TimeUtil(), 1000,5000);
		// ScheduledExecutorService service =
		// Executors.newSingleThreadScheduledExecutor();

		// 第二个参数为首次执行的延时时间，第三个参数为定时执行的间隔时间
		// service.scheduleAtFixedRate(new TimeUtil(), 1, 5, TimeUnit.SECONDS);

	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		// 关闭数据库连接
		BuguConnection.getInstance().close();
	}
}
