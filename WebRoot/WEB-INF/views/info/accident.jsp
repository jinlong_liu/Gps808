<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<script src="js/jquery.js"></script>

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">

<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>

<!-- gy 添加表单验证 -->
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />
<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">
		<!-- 查询列表 -->
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>输入条件查询事故信息</h5>
					<!-- 事故编号、事故类别、责任划分、车号、驾驶员姓名、事故时间、事故地点、事故概况、人员伤亡、损失金额、处罚金额、驾驶员电话、登记人、领导批复 -->
				</div>
				<div class="ibox-content">
					<form action="listAccidents" class="form-horizontal" method="post">
						<div class="row">
							<div class="form-group">
								<div class="col-sm-6">
									<!-- <label class="col-sm-2 control-label">事故编号：</label>
									<div style="padding-left: 1px;" class="col-sm-2">
										<select id="accidentId" name="addidentId"
										class="form-control js-example-basic-single">
										</select>
									</div> -->
									<label class="col-sm-2 control-label">车牌号：</label>
									<div style="padding-left: 1px;" class="col-sm-2">
										<input type="text" id="taxiNum" name="taxiNum"
											class="form-control" />
									</div>
									<!-- <label class="col-sm-2 control-label">:事故类型</label>
								 	<div style="padding-left: 1px;" class="col-sm-2">
										<input type="text" id="localSequence" class="form-control " />
								</div>	 -->

										<label class="col-sm-2 control-label"></label>
									<div style="padding-left: 1px; width: auto;"
										class="col-sm-1 form-inline">
										<button type="submit" class="btn btn-primary">查询</button>
										<button id="btn_addInsure" type="button"
											class="btn btn-primary" data-toggle="modal"
											data-target="#myModal5">添加</button>
									</div>
								</div>
							</div>
						</div>
						<!-- <div class="row">
				    <div class="form-group">
						<div class="col-sm-12">
							<label class="col-sm-2 control-label">车牌号：</label>
								<div style="padding-left: 1px;" class="col-sm-2">
									<input type="text" id="plate_number" class="form-control " placeholder="车牌号"/>
								</div>
							<label class="col-sm-2 control-label">驾驶员姓名：</label>
								<div style="padding-left: 1px;" class="col-sm-2">
									<input type="text" id="money" class="form-control " placeholder="驾驶员姓名"/>
								</div>		
							<label class="col-sm-2 control-label">人员伤亡：</label>
								<div style="padding-left: 1px;" class="col-sm-2">
									<input type="text" id="money" class="form-control " placeholder="人员伤亡"/>
								</div>																			
						</div>
					</div>
				</div>
				<div class="row">
				    <div class="form-group">
						<div class="col-sm-12">
							<label class="col-sm-2 control-label">事故地点：</label>
								<div style="padding-left: 1px;" class="col-sm-2">
									<input type="text" id="plate_number" class="form-control " placeholder="事故地点"/>
								</div>
							<label class="col-sm-2 control-label">事故概况：</label>
								<div style="padding-left: 1px;" class="col-sm-2">
									<input type="text" id="money" class="form-control " placeholder="事故概况"/>
								</div>		
							<label class="col-sm-2 control-label">事故时间：</label>
								<div style="padding-left: 1px; width: auto;" class="col-sm-1 form-inline">
									<input id="dateStart" type="text" readonly="readonly" class="form-control" name="dateStart"
											onfocus="WdatePicker({lang:'zh-cn',skin:'whyGreen',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'dateEnd\')||\'new Date()\'}',autoPickDate:true})" />
								</div>																			
						</div>
					</div>
				</div>
				<div class="row">
				    <div class="form-group">
						<div class="col-sm-12">
							<label class="col-sm-2 control-label">损失金额：</label>
								<div style="padding-left: 1px;" class="col-sm-2">
									<input type="text" id="plate_number" class="form-control " placeholder="损失金额"/>
								</div>
							<label class="col-sm-2 control-label">处罚金额：</label>
								<div style="padding-left: 1px;" class="col-sm-2">
									<input type="text" id="money" class="form-control " placeholder="处罚金额"/>
								</div>		
							<label class="col-sm-2 control-label">驾驶员电话：</label>
								<div style="padding-left: 1px;" class="col-sm-2">
									<input type="text" id="money" class="form-control " placeholder="驾驶员电话"/>
								</div>																				
						</div>
					</div>
				</div>-->
						<div class="row">
							<div class="form-group">
								<div class="col-sm-12">
									<!--	<label class="col-sm-2 control-label">登记人：</label>
								<div style="padding-left: 1px;" class="col-sm-2">
									<input type="text" id="plate_number" class="form-control " placeholder="登记人"/>
								</div>
							<label class="col-sm-2 control-label">领导批复：</label>
								<div style="padding-left: 1px;" class="col-sm-2">
									<input type="text" id="money" class="form-control " placeholder="领导批复"/>
								</div>		-->
								 
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!--gy修改后  点击添加按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal5" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form id="save" class="form-horizontal" action="saveAccident"
						method="post">
						<!-- 事故编号、事故类别、责任划分、车号、驾驶员姓名、事故时间、事故地点、事故概况、人员伤亡、损失金额、处罚金额、驾驶员电话、登记人、领导批复 -->
						<div class="modal-body">
							<div class="ibox-title">
								<h5>
									新增事故信息<span style="color: red">（* 为必填项）</span>
								</h5>
							</div>
							<div class="ibox-content">
								<!-- 第一行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 事故编号 -->
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>事故编号 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="accidentCode">
											</div>
										</div>
									</div>
									<!-- 事故类别 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span
												style="color: red">* </span>事故类别 ：</label>
											<div class="col-sm-7">
												<select class="form-control" id="addtype" name="accidenttype.id">
													<c:forEach items="${sessionScope.taxitype}" var="types">
														<option value="${types.id}">${types.accidentType}</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
								</div>

								<!-- 第二行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 责任划分 -->
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>责任划分 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="duty">
											</div>
										</div>
									</div>
									<!-- 车号 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span
												style="color: red">* </span>车牌号 ：</label>
											<div class="col-sm-7">
												<select class="form-control js-example-basic-single"
													name="taxiNum" id="taxiSelect" onchange="changeTaxi()">
													<option>请选择车牌号</option>
													<c:forEach items="${sessionScope.taxi}" var="taxis">
														<option value="${taxis.id}">${taxis.taxiNum}</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
								</div>
								<!-- 第三行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 驾驶员姓名 -->
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>驾驶员姓名 ：</label>
											<div class="col-sm-8">
												<select class="form-control js-example-basic-single"
													name="driver.id" id="selectDriver">
												</select>
											</div>
										</div>
									</div>
									<!-- 事故时间 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span
												style="color: red">* </span>事故时间 ：</label>
											<div class="col-sm-7">
												<input name="accidentDate" class="form-control" type="text"
													class="Wdate" value=""
													onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(save, name.valueOf())}})">
											</div>
										</div>
									</div>
								</div>
								<!-- 第四行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 事故地点 -->
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>事故地点 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="accidentPlace">
											</div>
										</div>
									</div>
									<!-- 事故概况 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span
												style="color: red">* </span>事故概况 ：</label>
											<div class="col-sm-7">
												<input name="accidentDsc" class="form-control" type="text">
											</div>
										</div>
									</div>
								</div>
								<!-- 第五行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 人员伤亡 -->
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>人员伤亡 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="isLoss">
											</div>
										</div>
									</div>
									<!-- 损失金额 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span
												style="color: red">* </span>损失金额 ：</label>
											<div class="col-sm-7">
												<input name="moneyLoss" class="form-control" type="text">
											</div>
										</div>
									</div>
								</div>
								<!-- 第六行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 处罚金额 -->
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>处罚金额 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="moneyProg">
											</div>
										</div>
									</div>
									<!-- 驾驶员电话 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span
												style="color: red">* </span>驾驶员电话 ：</label>
											<div class="col-sm-7">
												<input name="driverPhone" class="form-control" type="text">
											</div>
										</div>
									</div>
								</div>
								<!-- 第七行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 登记人 -->
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>登记人 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="checkName">
											</div>
										</div>
									</div>
									<!-- 领导批复 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span
												style="color: red">* </span>领导批复 ：</label>
											<div class="col-sm-7">
												<input name="bossReply" class="form-control" type="text">
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-white"
										data-dismiss="modal">关闭</button>
									<button type="submit" id="addButton" class="btn btn-primary">保存</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>



		<!-- 数据展示 -->
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-title">
					<div>
						<h5>&nbsp;&nbsp;&nbsp;事故信息表</h5>
					</div>
				</div>
				<div class="ibox-content">
					<!-- 事故编号、事故类别、责任划分、车号、驾驶员姓名、事故时间、事故地点、事故概况、人员伤亡、损失金额、处罚金额、驾驶员电话、登记人、领导批复 -->
					<table id="table1" width="100%"
						style="table-layout: fixed; background: #FFFFFF"
						class="footable table table-stripped" data-page-size="10"
						data-filter=#filter>
						<thead>
							<tr>
								<th>事故编号</th>
								<th>事故类别</th>
								<th>责任划分</th>
								<th>车号</th>
								<th>驾驶员姓名</th>
								<!-- 5 -->

								<th>事故时间</th>
								<th>事故地点</th>
								<th>事故概况</th>
								<th>人员伤亡</th>
								<th>损失金额</th>
								<th>处罚金额</th>
								<th>驾驶员电话</th>
								<th>登记人</th>
								<th>领导批复</th>
								<!-- 10 -->
								<th>操作</th>
							</tr>
						</thead>
						<tbody>

							<c:forEach items="${accidents}" var="a">
								<tr>
									<td>${a.accidentCode}</td>
									<td>${a.accidenttype.accidentType}</td>
									<td>${a.duty}</td>
									<td>${a.taxiNum}</td>
									<td>${a.driver.dname }</td>
									<!-- 5 -->
									<td>${a.accidentDate }</td>
									<td>${a.accidentPlace }</td>
									<td>${a.accidentDsc }</td>
									<td>${a.isLoss }</td>
									<td>${a.moneyLoss }</td>
									<!-- 10 -->
									<td>${a.moneyProg}</td>
									<td>${a.driverPhone}</td>
									<td>${a.checkName }</td>
									<td>${a.bossReply }</td>

									<td>
										<button id="btn-update" type="button"
											class="btn btn-success btn-sm" data-toggle="modal"
											data-target="#myModal1"
											onclick="update('${a.id }','${a.accidentCode}','${a.accidenttype.id}','${a.accidenttype.accidentType}'
											,'${a.duty}','${a.taxiNum}','${a.driver.dname }','${a.accidentDate }','${a.accidentPlace }'
											,'${a.accidentDsc }','${a.isLoss }','${a.moneyLoss }','${a.moneyProg}','${a.driverPhone}'
											,'${a.checkName }','${a.bossReply }')">修改</button>
										
										<button type="button" class="btn btn-danger btn-sm"
											onclick="delete1('${a.id}','${a.accidentCode}')">删除</button>
									</td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="14">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		<!-- 删除表格 -->
		<form id="deleteForm" action="deleteAccident" method="post">
			<input type="hidden" id="accidentId" name="id">
		</form>

		<!-- 点击"修改"按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal1" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form  id="update" class="form-horizontal" action="updateAccident" method="post">
							<input id="id" name="id" type="hidden">
							<div class="modal-body">
								<div class="ibox-title">
									<h5>修改事故信息（*为必填项）</h5>
								</div>
								<div class="ibox-content">
								<!-- 第一行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 事故编号 -->
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>事故编号 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="accidentCode2" name="accidentCode">
											</div>
										</div>
									</div>
									<!-- 事故类别 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span
												style="color: red">* </span>事故类别 ：</label>
											<div class="col-sm-7">
												<select class="form-control" id="accidenttype2" name="accidenttype.id">
													<c:forEach items="${sessionScope.taxitype}" var="types">
														<option value="${types.id}">${types.accidentType}</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
								</div>

								<!-- 第二行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 责任划分 -->
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>责任划分 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="duty2" name="duty">
											</div>
										</div>
									</div>
									<!-- 车号 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span
												style="color: red">* </span>车牌号 ：</label>
											<div class="col-sm-7">
												<select class="form-control js-example-basic-single"
													name="taxiNum" id="taxiSelect2" onchange="changeTaxi2()">
													<option>请选择车牌号</option>
													<c:forEach items="${sessionScope.taxi}" var="taxis">
														<option value="${taxis.id}">${taxis.taxiNum}</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
								</div>
								<!-- 第三行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 驾驶员姓名 -->
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>驾驶员姓名 ：</label>
											<div class="col-sm-8">
												<select class="form-control js-example-basic-single"
													 name="driver.id" id="selectDriver2">
												</select>
											</div>
										</div>
									</div>
									<!-- 事故时间 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span
												style="color: red">* </span>事故时间 ：</label>
											<div class="col-sm-7">
												<input id="accidentDate2" name="accidentDate" class="form-control" type="text"
													class="Wdate" value=""
													onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss'})">
											</div>
										</div>
									</div>
								</div>
								<!-- 第四行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 事故地点 -->
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>事故地点 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="accidentPlace2" name="accidentPlace">
											</div>
										</div>
									</div>
									<!-- 事故概况 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span
												style="color: red">* </span>事故概况 ：</label>
											<div class="col-sm-7">
												<input id="accidentDsc2" name="accidentDsc" class="form-control" type="text">
											</div>
										</div>
									</div>
								</div>
								<!-- 第五行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 人员伤亡 -->
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>人员伤亡 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="isLoss2" name="isLoss">
											</div>
										</div>
									</div>
									<!-- 损失金额 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span
												style="color: red">* </span>损失金额 ：</label>
											<div class="col-sm-7">
												<input id="moneyLoss2" name="moneyLoss" class="form-control" type="text">
											</div>
										</div>
									</div>
								</div>
								<!-- 第六行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 处罚金额 -->
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>处罚金额 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="moneyProg2" name="moneyProg">
											</div>
										</div>
									</div>
									<!-- 驾驶员电话 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span
												style="color: red">* </span>驾驶员电话 ：</label>
											<div class="col-sm-7">
												<input id="driverPhone2" name="driverPhone" class="form-control" type="text">
											</div>
										</div>
									</div>
								</div>
								<!-- 第七行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 登记人 -->
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>登记人 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="checkName2" name="checkName">
											</div>
										</div>
									</div>
									<!-- 领导批复 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span
												style="color: red">* </span>领导批复 ：</label>
											<div class="col-sm-7">
												<input id="bossReply2" name="bossReply" class="form-control" type="text">
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-white"
										data-dismiss="modal">关闭</button>
									<button type="submit" class="btn btn-primary">保存</button>
								</div>
							</div>
							</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<!-- 	<script src="js/jquery.min.js?v=2.1.4"></script> -->
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script>
	//加载footable的功能
	$(document).ready(function() {
		$('#table1').DataTable({
			"pagingType" : "full_numbers"
		});

		$("<option value=''>请选择司机</option>")
		.appendTo(selectDriver);
		
		$("<option value=''>请选择司机</option>")
		.appendTo(selectDriver2);

	});
	</script>
	<script>

	//动态加载司机下拉框（添加)
			function changeTaxi(){
	   			var id = $('#taxiSelect').val();
				if (id != "") {
					var driver = $('#selectDriver');
					$.post("getDriverByTaxi", {
						id : id,
					}, function(data) {
						if (data.length != 0) {
							//返回的数据不为空 
							var newdata = eval("(" + data + ")");
							driver.html("");//避免选择框中的值持续叠加
							$("<option value=''>请选择司机</option>")
									.appendTo(driver);

							for (var i = 0; i < data.length; i++) {
								$(
										"<option value ='"+newdata[i].driId +
										"'> "
												+ newdata[i].driName
												+ "</option>").appendTo(
										driver);
							};
						};
					});
				};
			};
		//动态加载司机下拉框(修改)
			function changeTaxi2(){
	   			var id = $('#taxiSelect2').val();
				if (id != "") {
					var driver = $('#selectDriver2');
					$.post("getDriverByTaxi", {
						id : id,
					}, function(data) {
						if (data.length != 0) {
							//返回的数据不为空 
							var newdata = eval("(" + data + ")");
							driver.html("");//避免选择框中的值持续叠加
							$("<option value=''>请选择司机</option>")
									.appendTo(driver);

							for (var i = 0; i < data.length; i++) {
								$(
										"<option value ='"+newdata[i].driId +
										"'> "
												+ newdata[i].driName
												+ "</option>").appendTo(
										driver);
							};
						};
					});
				};
			};
		//点击"修改"按钮
		function update(id, accidentCode, accidentId, accidentType, duty, taxiSelect, selectDriver, accidentDate, 
				accidentPlace, accidentDsc, isLoss, moneyLoss, moneyProg, driverPhone, checkName, bossReply){
			$("#id").val(id);
			$("#accidentCode2").val(accidentCode);
			$("#accidenttype2").val(accidentType);
			$("#duty2").val(duty);
			$("#taxiSelect2").val(taxiSelect);
			$("#accidentDate2").val(accidentDate);
			$("#accidentPlace2").val(accidentPlace);
			$("#accidentDsc2").val(accidentDsc);
			$("#isLoss2").val(isLoss);
			$("#moneyLoss2").val(moneyLoss);
			$("#moneyProg2").val(moneyProg);
			$("#driverPhone2").val(driverPhone);
			$("#checkName2").val(checkName);
			$("#bossReply2").val(bossReply);
			
			
		
			$.post("getDriverByTaxi", {
				id : taxiSelect,
			}, function(data) {
				if (data.length != 0) {
					//返回的数据不为空 
					var newdata = eval("(" + data + ")");
					//alert(JSON.stringify(newdata));
					$("#selectDriver2").html(""); //避免选择框中的值持续叠加
					$("<option value=''>"+selectDriver+"</option>").appendTo($("#selectDriver2"));

					for (var i = 0; i < data.length; i++) {
						//alert(JSON.stringify(newdata[i].driName));
						//alert(selectDriver);
						if(JSON.stringify(newdata[i].driName)==selectDriver){
							continue;
						}
						$("<option value ='"+newdata[i].driId +"'> "+ newdata[i].driName+ "</option>").appendTo($("#selectDriver2"));
					};
				};
			});
			
			$("#accidenttype2").html(""); //避免选择框中的值持续叠加
			//$("<option> " + accidentType + " </option>").appendTo($("#accidenttype2"));
			$(
					"<option value ='"+accidentId +
					"'> "
							+ accidentType
							+ "</option>").appendTo(
									$("#accidenttype2"));
			
		};

		//点击"删除"按钮
		function delete1(id, name) {
			swal({
				title: "您确定删除 编号为" + name + " 的数据吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
				$("#accidentId").val(id);
				//提交表单
				$("#deleteForm").submit();
			});
		}
		//加载完之后显示执行状态
		$(document).ready(function(){
			var result='<%=request.getAttribute("result")%>';
			if (result != null && result != "null") {
				if (result.indexOf("成功") > 0) {

					swal({
						title : result,
						type : "success",
					});
				} else {
					swal({
						title : result,
						type : "error",
					});
				};
			};
		});
	</script>

	<script type="text/javascript">
		/*添加保险界面的表单验证*/
	 	$('#btn_addInsure').click(function() {
		 $('#save').bootstrapValidator({
		 feedbackIcons : { 
		 valid : 'glyphicon glyphicon-ok',
		 invalid : 'glyphicon glyphicon-remove',
		 validating : 'glyphicon glyphicon-refresh'
		 },
		 live : 'enabled',
		 submitButtons : 'button[type="submit"]',
		 fields : {
		 accidentCode : {
		 message : 'The username is not valid',
		 validators : {
		 notEmpty : {
		 message : '事故编号不能为空'
		 }
		 }
		 },
		 roleid : {
		 validators : {
		 notEmpty : {
		 message : '车牌号不能为空'
		 }
		 }
		 },
		 duty : {
		 validators : {
		 notEmpty : {
		 message : '责任划分不能为空'
		 }
		 }
		 },
		 'driver.id' : {
		 validators : {
		 notEmpty : {
		 message : '请先选择车牌号'
		 },
		 }
		 },
		 accidentDate : {
		 validators : {
		 notEmpty : {
		 message : '事故时间不能为空'
		 },
		 }
		 },
		 accidentPlace : {
		 validators : {
		 notEmpty : {
		 message : '事故地点不能为空'
		 }
		 }
		 },
		 accidentDsc : {
		 validators : {
		 notEmpty : {
		 message : '事故概况不能为空'
		 }
		 }
		 },
		 isLoss : {
		 validators : {
		 notEmpty : {
		 message : '人员伤亡不能为空'
		 }
		 }
		 },
		 moneyLoss : {
		 validators : {
		 notEmpty : {
		 message : '损失金额不能为空'
		 }
		 }
		 },
		 moneyProg : {
		 validators : {
		 notEmpty : {
		 message : '处罚金额不能为空'
		 }
		 }
		 },
		 driverPhone : {
		 validators : {
		 notEmpty : {
		 message : '驾驶员电话不能为空'
		 }
		 }
		 },
		 checkName : {
		 validators : {
		 notEmpty : {
		 message : '登记人不能为空'
		 }
		 }
		 },
		 bossReply : {
		 validators : {
		 notEmpty : {
		 message : '领导批复不能为空'
		 }
		 }
		 },
		 }
		 });
		 });  
		
	 	/*修改保险界面的表单验证*/
	 	$(document).ready(function() {
		 $('#update').bootstrapValidator({
		 feedbackIcons : { 
		 valid : 'glyphicon glyphicon-ok',
		 invalid : 'glyphicon glyphicon-remove',
		 validating : 'glyphicon glyphicon-refresh'
		 },
		 live : 'enabled',
		 submitButtons : 'button[type="submit"]',
		 fields : {
		 accidentCode : {
		 message : 'The username is not valid',
		 validators : {
		 notEmpty : {
		 message : '事故编号不能为空'
		 }
		 }
		 },
		 'accidenttype.id' : {
		 validators : {
		 notEmpty : {
		 message : '事故类型不能为空'
		 }
		 }
		 },
		 'driver.id' : {
		 validators : {
		 notEmpty : {
		 message : '驾驶员不能为空'
		 }
		 }
		 },
		 duty : {
		 validators : {
		 notEmpty : {
		 message : '责任划分不能为空'
		 }
		 }
		 },
		 accidentDate : {
		 validators : {
		 notEmpty : {
		 message : '事故时间不能为空'
		 },
		 }
		 },
		 accidentPlace : {
		 validators : {
		 notEmpty : {
		 message : '事故地点不能为空'
		 }
		 }
		 },
		 accidentDsc : {
		 validators : {
		 notEmpty : {
		 message : '事故概况不能为空'
		 }
		 }
		 },
		 isLoss : {
		 validators : {
		 notEmpty : {
		 message : '人员伤亡不能为空'
		 }
		 }
		 },
		 moneyLoss : {
		 validators : {
		 notEmpty : {
		 message : '损失金额不能为空'
		 }
		 }
		 },
		 moneyProg : {
		 validators : {
		 notEmpty : {
		 message : '处罚金额不能为空'
		 }
		 }
		 },
		 driverPhone : {
		 validators : {
		 notEmpty : {
		 message : '驾驶员电话不能为空'
		 }
		 }
		 },
		 checkName : {
		 validators : {
		 notEmpty : {
		 message : '登记人不能为空'
		 }
		 }
		 },
		 bossReply : {
		 validators : {
		 notEmpty : {
		 message : '领导批复不能为空'
		 }
		 }
		 },
		 }
		 });
		 });
		
	 	/*日历刷新验证*/
		function refreshValidator(id, name) {
			$(id).data('bootstrapValidator').updateStatus(name,
					'NOT_VALIDATED', null).validateField(name);
		}
	</script>
	<script type="text/javascript">
	$("#addButton").click(function(){
		var obj = document.getElementById("taxiSelect"); 
		var index = obj.selectedIndex; 
		var taxi=$("#taxiSelect").val();
		var driver=$("#selectDriver").val();
		var type=$("#addtype").val();
 		if(index==0){
 			swal({
				title : taxi,
				type : "warn",
			});
 		}
	})
	
	</script>
</body>

</html>