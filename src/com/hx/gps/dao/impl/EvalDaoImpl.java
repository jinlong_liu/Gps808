package com.hx.gps.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.hx.gps.dao.EvalDao;
import com.hx.gps.entities.tool.Evaluate;
import com.hx.gps.util.DBUtil;
import com.hx.gps.util.FarmConstant;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
@Repository
public class EvalDaoImpl implements EvalDao {

	@Override
	public List<Evaluate> getEvalByDate(String dbname) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB,dbname);
		BasicDBObject finds=new BasicDBObject();
		List<Evaluate> evaluates=new ArrayList<>();
		
		DBCursor cursor=coll.find();
		while(cursor.hasNext()){
			finds = (BasicDBObject)cursor.next();
			Evaluate eval=new Evaluate(finds);
			evaluates.add(eval);
		}
		return evaluates;
	}

}
