package com.hx.gps.handler;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

public class CustomExceptionResolver implements HandlerExceptionResolver {

	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {
		ModelAndView modelAndView = new ModelAndView();
		if (ex instanceof UnauthenticatedException) {// 未认证的异常
			try {
				response.getWriter().write("请先登录");
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else if (ex instanceof UnauthorizedException) {// 未授权异常

			try {
				response.getWriter().write("您无权操作");
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {// 其他未知异常,跳转到500界面

			modelAndView.setViewName("500");

		}
		return modelAndView;
	}

}
