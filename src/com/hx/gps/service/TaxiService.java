package com.hx.gps.service;

import java.util.List;
import java.util.Map;

import com.hx.gps.entities.Driver;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.tool.Address;
import com.hx.gps.entities.tool.TaxiInfo2;

public interface TaxiService {

	// 获取所有车辆信息
	public List<Taxi> getAllTaxi();

	// 根据车牌号查询司机信息
	public List<Driver> getDriverOfTaxi(Integer id);

	// 根据车牌号查询相应的isuNum
	public Taxi getIsuByTaxi(String taxiNum);

	// 根据id查询手机号
	public String getIsuByid(Integer id);

	/**
	 * 根据终端号设置车辆在线、定位的状态
	 * @param taxiInfo2
	 * @param isuNum
	 * @author 高杨
	 * @return 
	 */
	public boolean setTaxiStateByIsuNum(TaxiInfo2 taxiInfo2, String isuNum);

	//根据公司列表获取车所有辆的在线状态
	public List<Address> getOnlineCarsByCompys(Integer type , List<Integer> companyIDs);
	
	public List<Address> getOfflineCarsByCompys(Integer type ,List<Integer> companyIDs);
	//根据公司id获取在线车辆数
	public Integer getOnlineNumByCom(int id);
	// 根据终端号获取车牌号
	public String getTaxiNumByIsuNum(String isuNum);
	//统一赋值 设备状态
	public int setTaxiStateByIsuNums(Map<String, TaxiInfo2> stateMap);
}
