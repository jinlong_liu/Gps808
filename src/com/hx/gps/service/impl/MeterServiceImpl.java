package com.hx.gps.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hx.gps.dao.MeterDao;
import com.hx.gps.entities.tool.Meter;
import com.hx.gps.service.MeterService;
import com.hx.gps.util.FarmConstant;

@Service
public class MeterServiceImpl implements MeterService {
	@Autowired
	private MeterDao meterDao;

	@Override
	public List<Meter> getMeterByFare(String fare, String startDate,
			String endDate, String taxiNum) {
		List<Meter> meters = null;
		String dbname = null;

		// 1.根据时间选择相应的历史表进行查询 获得开始年份、开始月份、结束月份
		String years = startDate.substring(2, 4);
		String start = startDate.substring(5, 7);
		String end = startDate.substring(5, 7);

		// 2.查询数据
		if (start.equals(end)) {
			// 若在同一月份
			dbname = FarmConstant.METER + years + start;
			meters = meterDao.getMetersByFare(startDate, endDate, taxiNum,
					fare, dbname);
			// System.out.println("addresses.size():"+addresses.size());
		} else {
			// 跨两个月份，且不跨年
			dbname = FarmConstant.METER + years + start;
			meters = meterDao.getMetersByFare(startDate, endDate, taxiNum,
					fare, dbname);

			dbname = FarmConstant.METER + years + end;
			List<Meter> meters2 = meterDao.getMetersByFare(startDate, endDate,
					taxiNum, fare, dbname);

			if (meters2 != null) {
				meters.addAll(meters2);
			}
		}
		return meters;
	}

	@Override
	public List<Meter> getMeterByFloatingFare(String floatingFare1,
			String floatingFare2, String startDate, String endDate,
			String taxiNum) {

		List<Meter> meters = null;
		String dbname = null;

		// 1.根据时间选择相应的历史表进行查询 获得开始年份、开始月份、结束月份
		String years = startDate.substring(2, 4);
		String start = startDate.substring(5, 7);
		String end = startDate.substring(5, 7);

		// 2.查询数据
		if (start.equals(end)) {
			// 若在同一月份
			dbname = FarmConstant.METER + years + start;
			meters = meterDao.getMetersByFloatingFare(floatingFare1,
					floatingFare2, startDate, endDate, taxiNum, dbname);
			// System.out.println("addresses.size():"+addresses.size());
		} else {
			// 跨两个月份，且不跨年
			dbname = FarmConstant.METER + years + start;
			meters = meterDao.getMetersByFloatingFare(floatingFare1,
					floatingFare2, startDate, endDate, taxiNum, dbname);

			dbname = FarmConstant.METER + years + end;
			List<Meter> meters2 = meterDao.getMetersByFloatingFare(
					floatingFare1, floatingFare2, startDate, endDate, taxiNum,
					dbname);

			if (meters2 != null) {
				meters.addAll(meters2);
			}
		}
		return meters;
	}

}
