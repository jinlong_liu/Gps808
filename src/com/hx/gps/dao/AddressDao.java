package com.hx.gps.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.hx.gps.entities.tool.Address;
import com.hx.gps.entities.tool.Alarm;
import com.hx.gps.entities.tool.AreaAddress;
import com.hx.gps.entities.tool.Blur;
import com.hx.gps.entities.tool.InfoQuery;
import com.hx.gps.entities.tool.Meter;
import com.hx.gps.entities.tool.MeterTemp;
import com.mongodb.DBObject;

public interface AddressDao {
	//查找实施位置表判断车辆是否在线(返回值为在线/离线)
	public String getOnlineOfAddress(String isuNum);
	//查询单位时间内，指定汽车的所有位置信息，按照时间排序
	public List<Address> findAddressByDate(String startDate,String endDate,String taxiNum,String dbname);
	//查询指定时间内，指定汽车的所有位置信息
	public List<DBObject> findAddressByTime(String startDate,String endDate,String dbname);
	//根据实时表来判断公司的车辆在线率
	public int screenOnlineOfAddress(List<String> isuNums,
				Date stratTime);
	//查找车辆实时表中的所有记录
	public List<Address> findAllTaxi();

	//根据终端号、开始时间和结束时间以及车辆状态查询出所有符合条件的记录
	public List<Address> findAddressByisu(String isuNum,String startDate,String endDate,String dbname);

	//添加报警消息
	public void addAlarm(String alarmType,int preinfoId,String isuNum);

	//查询历史表中某辆汽车小于指定时间的最后一条记录
	public Address getLastByDate(String isuNum,String date,String dbname);

	//查询所有报警信息
	public List<Alarm> getAllAlarm();

	//查看所有未提示信息
	public List<Alarm> getAlarmByDealState();

	//查询指定终端号的实时状态
	public Address findAddressByIsuNum(String isuNum);

	//根据传入的isuNum的数组，筛选出在线的车辆信息
	public List<Address> screenOnline(List<String> isuNums,Date stratTime);

	//对于计价器实时表
	public int screenOnlineOfMeter(List<String> isuNums,Date stratTime);

	//对于终端心跳，使用isuNum及时间信息，筛选出在线的车辆信息
	public Integer screenOnlineOfHeart(List<String> isuNums,Date stratTime);

	//通过车牌号集合 及开始时间、结束时间查询出符合条件的计价器表下车记录
	public List<Meter> getMeterByTaxiNums(List<String> taxiNums,String startDate,String endDate,String dbname);

	//根据一个车牌号及开始时间、结束时间，查询计价器下车记录
	public List<Meter> getMeterByTaxiNum(String taxiNum,String startDate,String endDate,String dbname);


	//根据终端号，时间段，处理状态等查看报警消息
	public List<Alarm> getAlarmByTaxi(String isuNum,String startDate,String endDate,Integer dealState);

	//wk后台分页，根据终端号，时间段，处理状态等查看报警消息
	public Blur getAlarmByTaxi1(String isuNum,InfoQuery infoQuery);

	//根据isuNum，查询计价器实时表中的记录
	public MeterTemp getTempMeterByIsuNum(String isuNum);

	//查询计价器实时表中的所有记录
	public List<MeterTemp> getAllMeterTemp();

	//通过车辆的终端号isuNum查看是否在线,返回值为：在线/离线(根据终端心跳)
	public String getOnlineOfHeart(String isuNum);

	//通过车辆的终端号查看是否在线，返回值为：在线/离线（根据计价器实时表）
	public String getOnlineOfMeter(String isuNum);

	//对于终端心跳表，根据时间，查询在线的车辆个数(根据终端心跳)
	public Integer getOnlineNum(String time);
	//查询平台下发区域之后的进出报警信息
	public AreaAddress getAreaAlarm(String isuNum, String time);

	public Integer getOnlineFromOnlineTemp();
	public List<Address> findAddressByIsuNum(List<String> isuNums);
	//在mongo查询所有终端号号和车牌号
	public Map<String, String> getIsuTaxis();

	public List<Alarm> getAllAlarmType(Date startDate, Date endDate, String alarmType);

	//首页报警提醒
	public List<Alarm> getAlarmByDealState1();
	//获取今日报警总数
	public List<Alarm> getAlarmNum();
	//获取今日未处理报警总数
	public List<Alarm> getAlarmNums();
	//根据isu查询报警信息
	List<Alarm> getDealAlarmByIsu(String alarmType, String isu);
}
