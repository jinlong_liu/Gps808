<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>视频监控测试页面</title>
<meta charset="utf-8">
<!--设置浏览器可以等比例缩放 -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--设置主地址栏颜色  -->
<meta name="theme-color" content="#000000">
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">
<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link rel="stylesheet" href="./assets/css/investigator.css">
<link rel="stylesheet" href="./assets/css/player.css">
<link href="./static/css/main.6d39a0f9.css" rel="stylesheet">
<link href="css/plugins/jsTree/style.min.css" rel="stylesheet">
<style>
/* 解决不能全屏视频的问题 */
/* .vedio1 video {
	position: absolute;
	height: 100%;
	width: 100%;
}
:-webkit-full-screen video {
  width: 100%;
  height: 100%;
} */
body{
	background:#ffffff;
}
.jstree-open>.jstree-anchor>.fa-folder:before {
	content: "\f07c"
}

.jstree-default .jstree-icon.none {
	width: 0
}

.row{
	margin-left:0;
	margin-right:0;
}
.ibox{
	margin-bottom:0;
}
.col-sm-6{
	padding-left:1px;
	padding-right:1px;
	margin:0;
}
</style>
<link href="css/video.css" rel="stylesheet">
</head>
<body>
	<!-- 视频监控 -->
	<div class="row" style="float:left;width:80%;height:100%">
		<div class="col-sm-2"></div>
		<div class="col-sm-12" id="playvedio">
			<!--选择回放模式:直接跳转内嵌页面 -->
			<br />
			<input type="button"
					style="transition: color .3s ease-in-out,background .3s ease-in-out;background-color: #04ecbd; color:black;border:1px solid #fff;font-weight: bold; font-size: 16px;  width: 45%;margin-left:2%; margin-top: -15px; margin-right:5%;height: 35px"
					class="btn J_toTempMoni" value="点击返回  [车辆监控] 页面" onmouseover="mOver(this)" onmouseout="mOut(this)">
					<!-- 通过 contab.js中的J_toTempMoni方法跳转-->
			<shiro:lacksPermission name="history:admin">
				<input type="button"
					style="transition: color .3s ease-in-out,background .3s ease-in-out;background-color: #04ecbd; color:black;border:1px solid #fff;font-weight: bold; font-size: 16px;  width: 45%; margin-top: -15px; height: 35px"
					class="btn " value="点击进入  [视频回放] 页面" onclick="reshow()" onmouseover="mOver(this)" onmouseout="mOut(this)">
			</shiro:lacksPermission>
	
			<br />
			<!-- 上侧视频1 视频2-->
			<div class="row" style="margin-top:1%;">
				<!-- video1 -->
				<div class="col-sm-6">
					<div class="ibox float-e-margins te">
						<div id="vedio1"
							style="width: 100%; position: relative; padding-top: 56.25%;">
							<video
								style="position: absolute; top: 0; left: 0; width: 100%; height: 100%"
								 id="my_video_1" class="video-js vjs-default-skin " controls
								preload='auto' data-setup='{}'>
								<source id="v1" src="${vedio1} " type="application/x-mpegURL">
							</video>
						</div>
					</div>
				</div>
				<!-- video2 -->
				<div class="col-sm-6">
					<div class="ibox float-e-margins te">
						<div id="vedio2"
							style="width: 100%; position: relative; padding-top: 56.25%;">
							<!--   <a class="J_menuItem" onclick="vedio1()">  -->
							<video
								style="position: absolute; top: 0; left: 0; width: 100%; height: 100%"
								id="my_video_2" class="video-js vjs-default-skin" controls
								preload="auto" data-setup='{}'>
								<!--<source src="./src/z.m3u8" type="application/x-mpegURL">-->
								<source id="v2" src="${vedio2}" type="application/x-mpegURL">
							</video>
							<!-- </a> -->
						</div>
					</div>
				</div>
			</div>
			<!-- 下侧视频3 视频4 -->
			<div class="row" style="margin-top:0.2%;">
				<!-- video3 -->
				<div class="col-sm-6">
					<div class="ibox float-e-margins te">
						<div id="vedio3"
							style="width: 100%; position: relative; padding-top: 56.25%; height: 0;">
							<!--   <a class="J_menuItem" onclick="vedio1()">  -->
							<video
								style="position: absolute; top: 0; left: 0; width: 100%; height: 100%"
								id="my_video_3" class="video-js vjs-default-skin" controls
								preload="auto" data-setup='{}'>
								<!--<source src="./src/z.m3u8" type="application/x-mpegURL">-->
								<source id="v3" src="${vedio3} " type="application/x-mpegURL">
							</video>
							<!-- </a> -->
						</div>
					</div>
				</div>
				<!-- video4 -->
				<div class="col-sm-6">
					<div class="ibox float-e-margins te">
						<div id="vedio4"
							style="width: 100%; position: relative; padding-top: 56.25%; height: 0;">
							<!--   <a class="J_menuItem" onclick="vedio1()">  -->
							<video
								style="position: absolute; top: 0; left: 0; width: 100%; height: 100%"
								id="my_video_4" class="video-js vjs-default-skin" controls
								preload="auto" data-setup='{}'>
								<!--<source src="./src/z.m3u8" type="application/x-mpegURL">-->
								<source id="v4" src="${vedio4} " type="application/x-mpegURL">
							</video>
							<!-- </a> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 车辆列表 -->
	<div style="float:left;width:20%;height:100%">
		<!-- 车辆列表 -->
		<div id="carList">
			<input type="hidden" id="timer">
			<div class="float-e-margins">
				<div class="ibox-content">
					<label class="control-label">
						<h4 style="color:black">车辆列表</h4>
					</label>
					<button type="button" class="btn btn-primary btn-sm-2"
							onclick="startMonitor()">开始监控</button>
					&nbsp;&nbsp;&nbsp;
					<input type="text" placeholder="请输入要监控的车辆" class="form-control form-control-sm" id="txtIndustryArea"> 
					<br> 
					<input type="hidden" id="show1">
					<!-- 用来显示选中的车辆，即节点 -->
					<div id="jstree1" class="trdemo"
						style="overflow-x: auto; overflow-y: auto; width:100%;height: 100%;">
					</div>
				</div>
			</div>
		</div>
	</div>
		<!-- 右键,jQuery的包请不要冲突-->
	<script src="js/jquery.min.js"></script>
	<script src="js/jquery.contextmenu.r2.js"></script>
	<script type="text/javascript"
		src="http://api.map.baidu.com/api?v=2.0&ak=xj2Gd1nMEwDHFcGeQvr0WORGk0t1Dlhw"></script>
	<script type="text/javascript"
		src="http://api.map.baidu.com/library/TextIconOverlay/1.2/src/TextIconOverlay_min.js"></script>
	<script type="text/javascript" src="js/MakerClusterer.js "></script>
	<!-- 加载鼠标绘制工具 -->
	<script type="text/javascript"
		src="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.js"></script>
	<link rel="stylesheet"
		href="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.css" />
	<!-- 加载检索信息窗口 -->
	<script type="text/javascript"
		src="http://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.js"></script>
	<link rel="stylesheet"
		href="http://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.css" />
	<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
	<script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>
	<script src="js/plugins/gritter/jquery.gritter.min.js"></script>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script type="text/javascript" src="js/contabs.js"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script src="js/plugins/jsTree/jstree.min.js"></script>
	<script src="js/video.js"></script>
	<script src="js/videojs-live.js"></script>
<script type="text/javascript">
function reshow(){
	var an = '<%=request.getAttribute("vedio0")%>';
	/*先指定回放车辆  */
	if (an == null || an == "" || an == "null") {
		alert("请先指定回放车辆");
	} else {
		// 指定父窗口跳转地址。
		parent.location.href = an;
	}
}
function mOver(obj){
	obj.style.color = "#ffffff";
	obj.style.background = "black";
}
function mOut(obj){
	obj.style.color = "black";
	obj.style.background = "#04ecbd";
}

function startMonitor(){
	var taxiNum = $("#show1").val();
	if (taxiNum == "") {
		swal({
			title : "请先选择车辆",
			type : "warning"
		});
	} else if (taxiNum.length > 7) {
		swal({
			title : "只允许选择一辆车",
			type : "warning"
		});
		return;
	} else {
		window.location.href = "getTreeOfTemp3?taxiNum=" + taxiNum;//传到后台controller处理
	}
}
</script>
<script>
	//设置定位间隔
	$("#interval").change(function() {
		var interval = $("#interval").val();
		var timer = $("#timer").val();
		if (interval<5||interval>60) {
			$("#interval").val("10");
			interval = 10;
		}
		window.clearInterval(timer);
		var timer = setInterval('chlink()', interval * 1000);
		//注意:执行的函数需要加引号,否则会报错的 
		$("#timer").val(timer);
	});

	$("#miletable").hide();
	$("#show1").val("");

	function pollingTree() {
		$
				.ajax({
					type : "POST",
					url : "/GpsManage/getTreeOfTempJson",
					dataType : "json",
					success : function(data) {
						sessionStorage.setItem("treelist", JSON
								.stringify(data));
						$
								.each(
										data.children,
										function(n, value1) {
											$
													.each(
															value1.children,
															function(n,
																	value2) {
																if (value2.onlineState === "在线") {
																	if (value2.positioningState === "定位") {
																		value2.text = value2.text
																				+ " [<span class=\"green\">定位</span>]"
																	} else {
																		value2.text = value2.text
																				+ " [<span class=\"red\">未定位</span>]"
																	}
																} else {
																	if (value2.positioningState === "定位") {
																		value2.text = value2.text
																				+ " [<span class=\"green\">定位</span>]"
																	} else {
																		value2.text = value2.text
																				+ " [<span class=\"red\">----</span>]"
																	}
																}
															});
										});
						var tree = $("#jstree1")
						tree.jstree(true).settings.core.data = data;
						tree.jstree(true).refresh(true);
					}
				});
	}

	//实现jQuery轮询树的状态
	$(document).ready(function() {
		setInterval(pollingTree, 10 * 1000);
		$("#timetools").hide();
	});
	//加载jstree
	$("#jstree1")
			.jstree(
					{
						"core" : {
							"check_callback" : true,
							"data" : function(node, cb) {
								$
										.ajax({
											type : "POST",
											dataType : "json",
											url : "/GpsManage/getTreeOfTempJson",
											success : function(data) {
												$
														.each(
																data.children,
																function(n,
																		value1) {
																	$
																			.each(
																					value1.children,
																					function(
																							n,
																							value2) {
																						if (value2.onlineState === "在线") {
																							if (value2.positioningState === "定位") {
																								value2.text = value2.text
																										+ " [<span class=\"green\">定位</span>]"
																							} else {
																								value2.text = value2.text
																										+ " [<span class=\"red\">未定位</span>]"
																							}
																						} else {
																							if (value2.positioningState === "定位") {
																								value2.text = value2.text
																										+ " [<span class=\"green\">定位</span>]"
																							} else {
																								value2.text = value2.text
																										+ " [<span class=\"red\">----</span>]"
																							}
																						}
																					});
																});
												cb.call(this, data);
											}
										});
							}
						},
						"plugins" : [ "types", "dnd", "checkbox", "sort",
								"search", "unique" ],
						"types" : {
							"car" : {
								"icon" : "fa fa-car"
							},
							"default" : {
								"icon" : "fa fa-folder"
							}
						}
					});

	//获取选择节点的id
	$("#jstree1").on('changed.jstree', function(e, data) {
		r = [];
		var i, j;
		for (i = 0, j = data.selected.length; i < j; i++) {
			var node = data.instance.get_node(data.selected[i]);
			if (data.instance.is_leaf(node)) {
				r.push(node.id);
			}
		}
		$("#show1").val(r);
	}).on(
			"search.jstree",
			function(e, data) {
				if (data.nodes.length) {
					var matchingNodes = data.nodes; // change
					$(this).find(".jstree-node").hide().filter(
							'.jstree-last').filter(function() {
						return this.nextSibling;
					}).removeClass('jstree-last');
					data.nodes.parentsUntil(".jstree")

					.addBack().show().filter(".jstree-children").each(
							function() {
								$(this).children(".jstree-node:visible")
										.eq(-1).addClass("jstree-last");
							});
					// nodes need to be in an expanded state
					matchingNodes.find(".jstree-node").show(); // change
				}
			}).on(
			"clear_search.jstree",
			function(e, data) {
				if (data.nodes.length) {
					$(this).find(".jstree-node").css("display", "").filter(
							'.jstree-last').filter(function() {
						return this.nextSibling;
					}).removeClass('jstree-last');
				}
			});

	//jsTree的模糊查询
	var to = false;
	$("#txtIndustryArea").keyup(function() {
		if (to) {
			clearTimeout(to);
		}
		to = setTimeout(function() {
			var v = $("#txtIndustryArea").val();
			var temp = $("#jstree1").is(":hidden");
			if (temp == true) {
				$("#jstree1").show();
			}
			$("#jstree1").jstree(true).search(v);
		}, 250);
	});

	//百度地图开始
	var x = 120.506862;
	var y = 36.173942;
	var ggPoint = new BMap.Point(x, y);
	var tempmarker = [];
	//地图初始化
	var bm = new BMap.Map("map");
	bm.centerAndZoom("青岛恒星", 13);
	bm.addControl(new BMap.NavigationControl());
	var markerClusterer = new BMapLib.MarkerClusterer(bm, {
		isAverangeCenter : false
	});
	bm.enableScrollWheelZoom();
	//添加控件
	var mapType1 = new BMap.MapTypeControl({
		mapTypes : [ BMAP_NORMAL_MAP, BMAP_HYBRID_MAP ]
	});
	var mapType2 = new BMap.MapTypeControl({
		anchor : BMAP_ANCHOR_TOP_LEFT
	});

	var overView = new BMap.OverviewMapControl();
	var overViewOpen = new BMap.OverviewMapControl({
		isOpen : true,
		anchor : BMAP_ANCHOR_BOTTOM_RIGHT
	});
	//添加地图类型和缩略图

	bm.addControl(mapType1); //2D图，卫星图
	bm.addControl(mapType2); //左上角，默认地图控件
	bm.setCurrentCity("青岛"); //由于有3D图，需要设置城市哦
	bm.addControl(overView); //添加默认缩略地图控件
	bm.addControl(overViewOpen); //右下角，打开

	var checkedIds = "";
	//添加信息窗口
	var opts = {
		width : 200, // 信息窗口宽度
		height : 200, // 信息窗口高度
		title : "车辆信息窗口", // 信息窗口标题
		enableMessage : true
	//设置允许信息窗发送短息
	};

	var timer = setInterval('chlink()', 10000); //注意:执行的函数需要加引号,否则会报错的 
	$("#timer").val(timer);
	function trclick(trNode) {
		var longtitude;
		var latitude;
		var taxiNumber = trNode.firstChild.innerHTML;
		longtitude = trNode.cells[3].innerText;
		latitude = trNode.cells[4].innerText;
		var temPoint = new BMap.Point(longtitude, latitude);
		//alert(taxiNumber);
		bm.centerAndZoom(temPoint, 15);
		//进行车辆详情框的数据刷新
		details(taxiNumber, longtitude, latitude);

	}
	function chlink() {
		//准备marker数组

		var state = $("#select").val();
		var isuNums = $("#show1").val();
		if (state == "实时监控中" && isuNums != "") {
			$("#templist").show();
			var args = {
				"isuNums" : isuNums
			};
			var url = "monitoring";
			$
					.post(
							url,
							args,
							function(data) {

								var points = [];
								var no_zero_list = [];

								var trStr = "";
								//alert(data);

								var d = eval("(" + data + ")");

								for (var i = 0; i < d.length; i++) {
									//排除经纬度为0的点
									if (d[i].longitude != 0
											&& d[i].latitude != 0) {
										no_zero_list.push(d[i])
									}
									if (d[i].longitude != 0
											&& d[i].latitude != 0) {

										var ggPoint = new BMap.Point(
												d[i].longitude,
												d[i].latitude);
										points.push(ggPoint);
									}
									var direction = "";
									if (d[i].direction == 0) {
										direction = "↑";
									} else if (d[i].direction > 0
											&& d[i].direction < 90) {
										direction = "↗";

									} else if (d[i].direction == 90) {
										direction = "→";
									} else if (d[i].direction > 90
											&& d[i].direction < 180) {

										direction = " ↘ ";
									} else if (d[i].direction == 180) {
										d[i].direction = "↓";
									} else if (d[i].direction > 180
											&& d[i].direction < 270) {

										direction = "↙ ";
									} else if (d[i].direction == 270) {
										direction = "←";

									} else if (d[i].direction > 270
											&& d[i].direction < 360) {

										direction = "↖";
									}

									trStr += "<tr onclick=trclick(this)>"
											+ "<td>"
											+ d[i].taxiNum
											+ "</td>"
											+ "<td>"
											+ d[i].speed
											+ "</td>"
											+ "<td>"
											+ direction
											+ "</td>"
											+ "<td>"
											+ d[i].longitude
											+ "</td>"
											+ "<td>"
											+ d[i].latitude
											+ "</td>"
											+ "<td>"
											+ d[i].state
											+ "</td>"
											+ "<td>"
											+ d[i].locateState
											+ "</td>"
											+ "<td>"
											+ d[i].time
											+ "</td>" + "</tr>";
								}
								//显示实时监控列表
								document.getElementById("tbody").innerHTML = trStr;
								/* //改变图标
												var myIcon = new BMap.Icon("car1.png",
													new BMap.Size(50, 40));
											marker = new BMap.Marker(data.points[i], {
												icon : myIcon
											});
											bm.addOverlay(marker2); */
								//循环外
								//markerCluster.clearMarkers() ;
								var mks = [];
								for (var i = 0; i < d.length; i++) {
									console.log("第" + i + "个点");
									var myIcon;
									if (no_zero_list[i].state == '在线') {

										if (no_zero_list[i].locateState == '不定位') {
											myIcon = new BMap.Icon(
													"img/car_red.png",
													new BMap.Size(21, 39));
										} else {
											if (no_zero_list[i].operateState == '载客') {
												//img
												myIcon = new BMap.Icon(
														"img/car_blue.png",
														new BMap.Size(21,
																39));
											} else {
												//img
												myIcon = new BMap.Icon(
														"img/car_green.png",
														new BMap.Size(21,
																39));
											}
										}

									} else {
										myIcon = new BMap.Icon(
												"img/car_gray.png",
												new BMap.Size(21, 39));
									}

									var marker = new BMap.Marker(
											new BMap.Point(d[i].longitude,
													d[i].latitude), {
												icon : myIcon
											});
									//bm.addOverlay(marker);
									//markersA.push(new BMap.Marker());
									//添加车牌号及在线信息
									var k = i;
									// for (var k = 0; k <= i; k++) {
									// if (i == k) {
									var label = new BMap.Label(
											"<b class='islabel'>"
													+ no_zero_list[k].taxiNum
													+ "/"
													+ no_zero_list[k].state
													+ "</b>", {
												offset : new BMap.Size(-25,
														-18)
											});
									marker
											.setRotation(no_zero_list[k].direction);
									//var label = new BMap.Label("我是文字标注哦",{offset:new BMap.Size(20,-10)});
									marker.setLabel(label);
									mks.push(marker);
									console.log("添加了marker");
									var direction2 = "";
									if (no_zero_list[k].direction == 0) {
										direction2 = "↑";
									} else if (no_zero_list[k].direction > 0
											&& no_zero_list[k].direction < 90) {
										direction2 = "↗";

									} else if (no_zero_list[k].direction == 90) {
										direction2 = "→";
									} else if (no_zero_list[k].direction > 90
											&& no_zero_list[k].direction < 180) {

										direction2 = " ↘ ";
									} else if (no_zero_list[k].direction == 180) {
										direction2 = "↓";
									} else if (no_zero_list[k].direction > 180
											&& no_zero_list[k].direction < 270) {

										direction2 = "↙ ";
									} else if (no_zero_list[k].direction == 270) {
										direction2 = "←";

									} else if (no_zero_list[k].direction > 270
											&& no_zero_list[k].direction < 360) {

										direction2 = "↖";
									}

									var content = "车牌号： "
											+ no_zero_list[k].taxiNum
											+ "<br>" + "在线状态: "
											+ no_zero_list[k].state
											+ "<br>" + "定位状态: "
											+ no_zero_list[k].locateState
											+ "<br>" + "营运状态： "
											+ no_zero_list[k].operateState
											+ "<br>" + "当前方向:" + direction2
											+ "<br>";
									addClickHandler(content, marker);
									// }
									// }

									// bm.setCenter(data.points[i]);//调整地图的中心坐标
									//添加监听事件

								}
								//赋值
								//清空数组
								markerClusterer.clearMarkers();
								//markerClusterer.removeMarkers(tempmarker);
								tempmarker = mks.concat();
								console.log("待清除marker" + tempmarker);
								markerClusterer.addMarkers(mks);
								//回调函数结束												

								setTimeout(function() {
									/*   bm.addEventListener("zoomend",
										function() {
											var DiTu = this.getZoom();
											$('b.islabel').parent()[DiTu >= 1 ? 'hide'
													: 'show']();
										});   */
									/*
									//bm.clearOverlays();
									//markerCluster.clearMarkers();	
									//bm.setMapStatus(MapStatusUpdateFactory.newMapStatus(bm.getMapStatus()));
									var interval = $('interval').val();
									//console.log("又开始删除marker");
									//console.log("待删除数组的 长度为:"+tempmarker.length);
									console.log("当前marker集合"
									+ markerClusterer);
									var convertor = new BMap.Convertor();
									convertor.translate(points, 1, 5,
									translateCallback)  */
									var interval = $('interval').val();
									//chlink();
								}, interval);

							});

		} else {
			// $("#templist").hide();
		}
	}

	//checkbox改变时
	$("input[name='chbox']").change(
			function() {
				var oneches = document.getElementsByName("chbox");
				for (var i = 0; i < oneches.length; i++) {
					if (oneches[i].checked == true) {
						//避免重复累计id （不含该id时进行累加）
						if (checkedIds.indexOf(oneches[i].value) == -1) {
							checkedIds = checkedIds + oneches[i].value
									+ ",";
						}
					}
					if (oneches[i].checked == false) {
						//取消复选框时 含有该id时将id从全局变量中去除
						if (checkedIds.indexOf(oneches[i].value) != -1) {
							checkedIds = checkedIds.replace(
									(oneches[i].value + ","), "");
						}
					}
				}
				//保存选中的数据到文本框
				$("#show1").val(checkedIds);

			});
	var tempState = true;
	//开始进行汽车的实时监控、monitoring

	$("#select").click(function() {
		if ($("#select").val() === "开始监控") {

			//bm.clearOverlays();
			markerClusterer.clearMarkers();
			var taxiNums = $("#show1").val();
			if (taxiNums == "") {
				swal({
					title : "请先选择车辆",
					type : "warning"
				});
			} else {
				tempState = false;
				//$("#state").val("实时监控中");
				$("#select").removeClass("btn-success");
				$("#select").addClass("btn-danger");
				$("#select").val("实时监控中");
				markerClusterer.clearMarkers();
				chlink();
			}
		} else {
			//$("#state").val("监控未开启");
			$("#select").removeClass("btn-danger");
			$("#select").addClass("btn-success");
			$("#select").val("开始监控");
			markerClusterer.clearMarkers();
			//bm.clearOverlays();
			$("#tbody").html("");
			tempState = true;
		}
	});

	//右键功能
	$(".trdemo").contextMenu(
			'myMenu1',
			{
				bindings : {
					'selSetIsu' : function(t) {//查询设置终端参数
						alert('该功能正在拼命开发中…');
					},
					'setEvent' : function(t) {//设置事件
						alert('该功能正在拼命开发中…');
					},
					'setInfo' : function(t) {//设置信息菜单
						alert('该功能正在拼命开发中…');
					},
					'sendText' : function(t) {//下发文本消息
						//获取文本框中的值
						var taxiNums = $("#show1").val();
						var taxinum;
						taxinum = taxiNums.split(",");
						if (taxiNums == "") {
							swal({
								title : "请先选择车辆",
								type : "warning"
							});
							return;
						} else if (taxiNums.length == 1) {
							swal({
								title : "该公司暂无车辆或车牌号格式错误",
								type : "warning"
							});
							return;

						} else if (taxiNums.length < 7) {
							swal({
								title : "车牌号长度有误",
								type : "warning"
							});
							return;
						} else if (taxiNums.length > 7) {
							//遍历数组
							for (var i = 0; i < taxinum.length; i++) {
								var taxi = taxinum[i];
								if (taxi.length != 7) {
									swal({
										title : "部分车辆车牌号格式不对或选定的公司没有车辆",
										type : "warning"
									});
									return;
								}

							}
						}
						console.log("跳转页面")
						window.location.href = "enterSendText?taxiNum="
								+ taxiNums;

					},
					'setPower' : function(t) {
						//远程开关机
						var taxiNums = $("#show1").val();
						var taxinum;
						taxinum = taxiNums.split(",");
						if (taxiNums == "") {
							swal({
								title : "请先选择车辆",
								type : "warning"
							});
							return;
						} else if (taxiNums.length == 1) {
							swal({
								title : "该公司暂无车辆或车牌号格式错误",
								type : "warning"
							});
							return;
						} else if (taxiNums.length < 7) {
							swal({
								title : "车牌号格式有误",
								type : "warning"
							});
							return;
						} else if (taxiNums.length > 7) {
							//遍历数组
							for (var i = 0; i < taxinum.length; i++) {
								var taxi = taxinum[i];
								if (taxi.length != 7) {
									swal({
										title : "部分车辆车牌号格式不对或选定的公司没有车辆",
										type : "warning"
									});
									return;
								}

							}
						}

						window.location.href = "enterSetPower?taxiNum="
								+ taxiNums;

					},
					'setPrice' : function(t) {//远程调价
						var taxiNums = $("#show1").val();
						var taxinum;
						taxinum = taxiNums.split(",");
						if (taxiNums == "") {
							swal({
								title : "请先选择车辆",
								type : "warning"
							});
							return;
						} else if (taxiNums.length == 1) {
							swal({
								title : "该公司暂无车辆或车牌号格式错误",
								type : "warning"
							});
							return;
						} else if (taxiNums.length < 7) {
							swal({
								title : "车牌号格式有误",
								type : "warning"
							});
							return;
						} else if (taxiNums.length > 7) {
							//遍历数组
							for (var i = 0; i < taxinum.length; i++) {
								var taxi = taxinum[i];
								if (taxi.length != 7) {
									swal({
										title : "部分车辆车牌号格式不对或选定的公司没有车辆",
										type : "warning"
									});
									return;
								}

							}
						}

						window.location.href = "enterSetPrice?taxiNum="
								+ taxiNums;
					},
					'infoSer' : function(t) {//信息服务
						alert('该功能正在拼命开发中…');
					},
					'setRelay' : function(t) { //断油断电
						var taxiNums = $("#show1").val();
						var taxinum;
						taxinum = taxiNums.split(",");
						if (taxiNums == "") {
							swal({
								title : "请先选择车辆",
								type : "warning"
							});
							return;
						} else if (taxiNums.length == 1) {
							swal({
								title : "该公司暂无车辆或车牌号格式错误",
								type : "warning"
							});
							return;
						} else if (taxiNums.length < 7) {
							swal({
								title : "车牌号格式有误",
								type : "warning"
							});
							return;
						} else if (taxiNums.length > 7) {
							//遍历数组
							for (var i = 0; i < taxinum.length; i++) {
								var taxi = taxinum[i];
								if (taxi.length != 7) {
									swal({
										title : "部分车辆车牌号格式不对或选定的公司没有车辆",
										type : "warning"
									});
									return;
								}

							}
						}

						window.location.href = "enterSetRelay?taxiNum="
								+ taxiNums;

					},
					'sendQuiz' : function(t) {//下发提问
						alert('该功能正在拼命开发中…');
					},
					'setArea' : function(t) { //下发区域
						var taxiNums = $("#show1").val();
						var taxinum;
						taxinum = taxiNums.split(",");
						if (taxiNums == "") {
							swal({
								title : "请先选择车辆",
								type : "warning"
							});
							return;
						} else if (taxiNums.length == 1) {
							swal({
								title : "该公司暂无车辆或车牌号格式错误",
								type : "warning"
							});
							return;
						} else if (taxiNums.length < 7) {
							swal({
								title : "车牌号格式有误",
								type : "warning"
							});
							return;
						} else if (taxiNums.length > 7) {
							//遍历数组
							for (var i = 0; i < taxinum.length; i++) {
								var taxi = taxinum[i];
								if (taxi.length != 7) {
									swal({
										title : "部分车辆车牌号格式不对或选定的公司没有车辆",
										type : "warning"
									});
									return;
								}

							}
						}

						window.location.href = "enterSetArea?taxiNum="
								+ taxiNums;

					},
					'deleteArea' : function(t) { //下发区域
						var taxiNums = $("#show1").val();
						var taxinum;
						taxinum = taxiNums.split(",");
						if (taxiNums == "") {
							swal({
								title : "请先选择车辆",
								type : "warning"
							});
							return;
						} else if (taxiNums.length == 1) {
							swal({
								title : "该公司暂无车辆或车牌号格式错误",
								type : "warning"
							});
							return;
						} else if (taxiNums.length < 7) {
							swal({
								title : "车牌号格式有误",
								type : "warning"
							});
							return;
						} else if (taxiNums.length > 7) {
							//遍历数组
							for (var i = 0; i < taxinum.length; i++) {
								var taxi = taxinum[i];
								if (taxi.length != 7) {
									swal({
										title : "部分车辆车牌号格式不对或选定的公司没有车辆",
										type : "warning"
									});
									return;
								}

							}
						}

						window.location.href = "enterDeleteArea?taxiNum="
								+ taxiNums;

					},
					'phoneCall' : function(t) {//电话回拨
						var taxiNums = $("#show1").val();
						if (taxiNums == "") {
							swal({
								title : "请先选择车辆",
								type : "warning"
							});
						} else if (taxiNums.length == 1) {
							swal({
								title : "该公司暂无车辆或车牌号格式错误",
								type : "warning"
							});

						} else if (taxiNums.length < 7) {
							swal({
								title : "车牌号格式不对",
								type : "warning"
							});

						} else if (taxiNums.length > 7) {
							swal({
								title : "只能选择一辆车",
								type : "warning"
							});
						} else {
							window.location.href = "intoBackdail?taxiNum="
									+ taxiNums;
						}
					},
					'sendPhoneText' : function(t) {//下发电话本
						alert('该功能正在拼命开发中…');
					},
					'addressTrack' : function(t) {//临时位置追踪
						alert('该功能正在拼命开发中…');
					},
					'carCall' : function(t) {//车辆点名
						alert('该功能正在拼命开发中…');
					},
					'takePic' : function(t) {//拍照
						var taxiNums = $("#show1").val();
						var taxinum;
						taxinum = taxiNums.split(",");
						if (taxiNums == "") {
							swal({
								title : "请先选择车辆",
								type : "warning"
							});
						} else if (taxiNums.length == 1) {
							swal({
								title : "该公司暂无车辆或车牌号格式错误",
								type : "warning"
							});

						} else if (taxiNums.length < 7) {
							swal({
								title : "车牌号格式有误",
								type : "warning"
							});

						} else if (taxiNums.length > 7) {
							//遍历数组
							for (var i = 0; i < taxinum.length; i++) {
								var taxi = taxinum[i];
								if (taxi.length != 7) {
									swal({
										title : "部分车辆车牌号格式不对或选定的公司没有车辆",
										type : "warning"
									});
									return;
								}

							}
							window.location.href = "enterTakePic?taxiNum="
									+ taxiNums;
						}

						else {
							window.location.href = "enterTakePic?taxiNum="
									+ taxiNums;
						}

					}
				}
			});

	//实时定位方法结束，下面为添加信息框调用的方法
	function addClickHandler(content, marker) {
		marker.addEventListener("click", function(e) {
			openInfo(content, e);
		});
	}
	function openInfo(content, e) {
		var p = e.target;
		var point = new BMap.Point(p.getPosition().lng, p.getPosition().lat);
		var infoWindow = new BMap.InfoWindow(content, opts); // 创建信息窗口对象 
		bm.openInfoWindow(infoWindow, point); //开启信息窗口
	}

	/** 选择框开始 **/
	/* $(document).ready(function() { 
		

		
	}); */
	function details(number, lon, lat) {
		var url = "getTaxiDetails";
		var args = {
			"taxiNum" : number
		};
		$.post(url, args, function(data) {
			var taxi = eval("(" + data + ")");
			//alert(taxi.taxiNum);
			$("#taxiNumber").text(taxi.taxiNum);
			$("#gps").text(taxi.gpsState);
			$("#annta").text(taxi.anntaState);
			$("#energy").text(taxi.energyState);
			$("#phone").text(taxi.phoneState);
			$("#acc").text(taxi.accState);
			$("#lat").text(taxi.latState);
			$("#lon").text(taxi.lonState);
			$("#meter").text(taxi.meterState);
			$("#alarm").text(taxi.alarmState);
			$("#key").text(taxi.keyState);
			$("#oil").text(taxi.oilState);
			$("#eng").text(taxi.engState);
			$("#door").text(taxi.doorState);
			$("#direction").text(taxi.directionState);
			//$('.details tr').eq(2).find("td").html(taxi.taxiNum);
			//处理
			var gc = new BMap.Geocoder();
			var point = new BMap.Point(lon, lat);
			gc.getLocation(point, function(rs) {
				var addComp = rs.addressComponents;
				$("#addnow").text(
						addComp.province + ", " + addComp.city + ", "
								+ addComp.district + ", " + addComp.street
								+ ", " + addComp.streetNumber);
			});
		});
	}
	//隐藏视频窗口,当点击显示的时候再打开。
	var a = 0;//计数器
	function showdiv() {
		var taxiNums = $("#show1").val();
		//控制地图大小
		if (($('#middleMap').hasClass('col-sm-10'))) {
			$('#middleMap').removeClass('col-sm-10');
			$('#middleMap').addClass('col-sm-8');
			//显示时间选择轴
			$("#timetools").show();
			document.getElementById('playvedio').style.display = document
					.getElementById('playvedio').style.display == "none" ? "block"
					: "none";
		} else {
			if (taxiNums !== "") {
				//检验手机号
				if (taxiNums.length == 1) {
					swal({
						title : "该公司暂无车辆或车牌号格式错误",
						type : "warning"
					});
					return;
				} else if (taxiNums.length < 7) {
					swal({
						title : "车牌号格式有误",
						type : "warning"
					});
					return;
				} else if (taxiNums.length > 7) {
					swal({
						title : "只允许选择一辆车",
						type : "warning"
					});
					return;
				} else {
					//后台取指定设备的视频连接
					$("#vedioNums").val(taxiNums);
					$("#vedioForm").submit();
					//window.location.href = "getTreeOfTemp3?taxiNum="
					//+ taxiNums;
				}

			} else {
				document.getElementById('playvedio').style.display = document
						.getElementById('playvedio').style.display == "none" ? "block"
						: "none";
				$('#middleMap').removeClass('col-sm-8');
				$('#middleMap').addClass('col-sm-10');
			}
			//隐藏时间轴
			$("#timetools").hide();
		}
		//隐藏或显示视频窗口

	}
	//	document.getElementById("EleId").style.display = "none";
	//	 document.getElementById("EleId").style.display = "inline";
	//点击视频监控按钮打开视频监控界面 通道1 通道2 通道3 通道4
	$("#playVedio").click(function() {
		if ($("#select").val() === "开始监控") {
			var taxiNum = $("#show1").val();
			if (taxiNum == "") {
				swal({
					title : "请先选择车辆",
					type : "warning"
				});
			}else if (taxiNums.length > 7) {
				swal({
					title : "只允许选择一辆车",
					type : "warning"
				});
				return;
			} else {
				window.location.href = "getTreeOfTemp3?taxiNum=" + taxiNum;//传到后台controller处理
			}
		}
	});
</script>

</body>

</html>