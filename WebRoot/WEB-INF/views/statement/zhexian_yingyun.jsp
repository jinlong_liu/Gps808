<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>营运分析</title>
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="css/plugins/jsTree/style.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<!-- <base target="_blank"> -->
<style>
.jstree-open>.jstree-anchor>.fa-folder:before {
	content: "\f07c"
}

.jstree-default .jstree-icon.none {
	width: 0
}
</style>
</head>

<body class="gray-bg">
	<!-- <div class="row  border-bottom white-bg dashboard-header">
		<div class="col-sm-12">
			
		</div>

	</div> -->
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-9">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>单车营运信息折线图（金额/载客里程/空驶里程）</h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
							</a>
						</div>
					</div>
					<div class="ibox-content" id="echartsShow" style="height: 781px;padding-top: 150px">
						<!-- <div class="echarts" id="echarts-line-chart0"></div>
						<div class="echarts" id="echarts-line-chart1"></div> -->
					</div>
				</div>


			</div>

			<!--右侧公司选择栏开始  -->
			<div class="col-sm-3">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>查询条件</h5>
					</div>
  					<div class="ibox-content" style="height: 780px;">
						<button type="button" class="btn btn-primary btn-sm-2"
							onclick="select2()">查询</button>
						<br> 开始时间： <input class="form-control" type="text" name="startDate" id="startDate"
							value="${meterStartDate}" class="Wdate"
							onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss'})">
						<br>结束时间： <input class="form-control" type="text" name="endDate"
							id="endDate" placeholder="" value="${meterEndDate}" class="Wdate"
							onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'startDate\')}',dateFmt:'yyyy-MM-dd HH:mm:ss'})">
						<br> <input type="hidden" name="taxiNums" id="taxiNums">
						<!-- <br> <input type="button" value="查询" onclick="select2()"> -->
						<hr>
						<h4>请选择车辆</h4>
							<input type="text" class="form-control form-control-sm"
							id="txtIndustryArea">
							<br>
						<div id="jstree1" style="overflow:auto;height:400px;width:250px;">
							<ul>
								<li class="jstree-open">所有公司
									<ul>
										<c:forEach items="${comInfo2s}" var="comInfo2s">
											<li id="${comInfo2s.comId}">${comInfo2s.cname }
												<ul>
													<c:forEach items="${comInfo2s.taxis}" var="taxis">
														<li id="${taxis.taxiNum}" data-jstree='{"type":"car"}'>
															${taxis.taxiNum }</li>
													</c:forEach>
												</ul>
											</li>
										</c:forEach>
									</ul>
								</li>
							</ul>
						</div>

					</div>
				</div>
			</div>
		</div>

	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/echarts/echarts-all.js"></script>
	<script src="js/content.min.js?v=1.0.0"></script>
	<!-- 	<script src="js/demo/echarts-demo.min.js"></script> -->
	<script src="js/demo/echarts-demo2.min.js"></script>
	<script src="js/plugins/jsTree/jstree.min.js"></script>
	<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
	<!-- <script type="text/javascript"
		src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script> -->
	<script type="text/javascript">
		function select2() {
			var startDate = $("#startDate").val();
			var endDate = $("#endDate").val();
			var taxiNums = $("#taxiNums").val();
			if (startDate == "") {
				swal({
					title : "开始时间不能为空！",
					type : "warning"
				});
				return;
			}
			if (endDate == "") {
				swal({
					title : "结束时间不能为空！",
					type : "warning"
				});
				return;
			}

			if (taxiNums == "") {
				swal({
					title : "请选择车辆！",
					type : "warning"
				});
				return;
			}
			var args = {
				"startDate" : startDate,
				"endDate" : endDate,
				"taxiNums" : taxiNums
			};
			var url = "getOperateByAjax";
			$
					.post(
							url,
							args,
							function(data) {
								if (data.length != 0) {
									var newdata = eval("(" + data + ")");
									var length = 20;//每个图显示的车辆数
									var totalLength = newdata.length;//数据总长度
									//图形个数，取整
									var chartsNum = parseInt(newdata.length
											/ length) + 1;  

									//建立每个图
									for (var i = 0; i < chartsNum; i++) {
										var xdata = null;
										var money = null;
										var fullmil = null;
										var emptymli = null;
										//最后一个图时，数组长度
										if (i == chartsNum - 1) {
											xdata = new Array((totalLength - i
													* length));//x轴数据，各个车牌号
											money = new Array((totalLength - i
													* length));//金额数据
											fullmil = new Array(
													(totalLength - i * length));//载客里程数据
											emptymli = new Array(
													(totalLength - i * length));//空驶里程数据
										} else {
											xdata = new Array(length);//x轴数据，各个车牌号
											money = new Array(length);//金额数据
											fullmil = new Array(length);//载客里程数据
											emptymli = new Array(length);//空驶里程数据
										}

										//组装数据
										for (var j = 0; j < length; j++) {
											xdata[j] = newdata[j + i * length].taxiNum;
											money[j] = newdata[j + i * length].money;
											fullmil[j] = newdata[j + i * length].fullmil;
											emptymli[j] = newdata[j + i
													* length].emptymli;
											//到达最大长度时，跳出循环
											if (j + i * length == newdata.length - 1) {
												break;
											}
										}

										//图形位置和数据展示

										$(
												"<div class='echarts' id='echarts-line-chart"+i+"'></div>")
												.appendTo($("#echartsShow"));
										//$("<div class='echarts' id='echarts-line-chart1'></div>").appendTo($("#echartsShow"));
										var e = null;

										if (i == 0) {
											e = echarts
													.init(document
															.getElementById("echarts-line-chart0"));
										}
										if (i == 1) {
											e = echarts
													.init(document
															.getElementById("echarts-line-chart1"));
										}

										var a = {
											title : {
												text : "营运数据"
											},
											tooltip : {
												trigger : "axis"
											},
											legend : {
												data : [ "金额", "载客里程", "空驶里程" ]
											},
											grid : {
												x : 40,
												x2 : 40,
												y2 : 24
											},
											calculable : true,
											//x轴			
											xAxis : [ {
												type : "category",
												boundaryGap : 1,
												data : xdata
											} ],
											yAxis : [ {
												type : "value",
												axisLabel : {
													formatter : "{value} "
												}
											} ],

											series : [ {
												name : "金额",
												type : "line",
												data : money,
												itemStyle : {
													normal : {
														label : {
															show : true
														}
													}
												},
											}, {
												name : "载客里程",
												type : "line",
												data : fullmil,
												itemStyle : {
													normal : {
														label : {
															show : true
														}
													}
												},
											}, {
												name : "空驶里程",
												type : "line",
												data : emptymli,
												itemStyle : {
													normal : {
														label : {
															show : true
														}
													}
												},
											} ]
										};
										e.setOption(a), $(window).resize(
												e.resize);
									}

									/* 
									var xdata = new Array(newdata.length);//x轴数据，各个车牌号
									var money = new Array(newdata.length);//金额数据
									var fullmil = new Array(newdata.length);//载客里程数据
									var emptymli = new Array(newdata.length);//空驶里程数据
									
									
									//组装数据
									for (var i = 0; i < newdata.length; i++) {
										xdata[i] = newdata[i].taxiNum;
										money[i] = newdata[i].money;
										fullmil[i] = newdata[i].fullmil;
										emptymli[i] = newdata[i].emptymli;
									}
									
									//e：图形位置；a：图形数据
									var e = echarts.init(document
											.getElementById("echarts-line-chart")); 
									var e2 = echarts.init(document
											.getElementById("echarts-line-chart2")); 		
											
									var	a = {
										title : {
											text : "营运数据"
										},
										tooltip : {
											trigger : "axis"
										},
										legend : {
											data : [ "金额", "载客里程", "空驶里程" ]
										},
										grid : {
											x : 40,
											x2 : 40,
											y2 : 24
										},
										calculable : true,
										//x轴			
										xAxis : [ {
											type : "category",
											boundaryGap : 1,
											data : xdata
										} ],
										yAxis : [ {
											type : "value",
											axisLabel : {
												formatter : "{value} "
											}
										} ],

										series : [ {
											name : "金额",
											type : "line",
											data : money,
											itemStyle : {
												normal : {
													label : {
														show : true
													}
												}
											},
										}, {
											name : "载客里程",
											type : "line",
											data : fullmil,
											itemStyle : {
												normal : {
													label : {
														show : true
													}
												}
											},
										}, {
											name : "空驶里程",
											type : "line",
											data : emptymli,
											itemStyle : {
												normal : {
													label : {
														show : true
													}
												}
											},
										} ]
									};
									var	b = {
										title : {
											text : "营运数据"
										},
										tooltip : {
											trigger : "axis"
										},
										legend : {
											data : [ "金额", "载客里程", "空驶里程" ]
										},
										grid : {
											x : 40,
											x2 : 40,
											y2 : 24
										},
										calculable : true,
										//x轴			
										xAxis : [ {
											type : "category",
											boundaryGap : 1,
											data : xdata
										} ],
										yAxis : [ {
											type : "value",
											axisLabel : {
												formatter : "{value} "
											}
										} ],

										series : [ {
											name : "金额",
											type : "line",
											data : money,
											itemStyle : {
												normal : {
													label : {
														show : true
													}
												}
											},
										}, {
											name : "载客里程",
											type : "line",
											data : fullmil,
											itemStyle : {
												normal : {
													label : {
														show : true
													}
												}
											},
										}, {
											name : "空驶里程",
											type : "line",
											data : emptymli,
											itemStyle : {
												normal : {
													label : {
														show : true
													}
												}
											},
										} ]
									};
									
									e.setOption(a),e2.setOption(b),$(window).resize(e.resize); */
								}
							});
		}

		//选择公司开始
		$(document).ready(function() {
			/* $("#miletable").hide(); */
			$("#taxiNums").val("");

			$("#jstree1").jstree({
				"core" : {
					"check_callback" : true
				},
				"plugins" : [ "types", "dnd", "checkbox", "sort",
					 "search", "unique" ],
				"types" : {
					"car" : {
						"icon" : "fa fa-car"
					},
					"default" : {
						"icon" : "fa fa-folder"
					}
				}
			});

			//获取选择节点的id
			$("#jstree1").on('changed.jstree', function(e, data) {
				r = [];
				var i, j;
				for (i = 0, j = data.selected.length; i < j; i++) {
					var node = data.instance.get_node(data.selected[i]);
					if (data.instance.is_leaf(node)) {
						r.push(node.id);
					}
				}
				$("#taxiNums").val(r);
			}).on(
					"search.jstree",
					function(e, data) {
						if (data.nodes.length) {
							var matchingNodes = data.nodes; // change
							$(this).find(".jstree-node").hide().filter(
									'.jstree-last').filter(function() {
								return this.nextSibling;
							}).removeClass('jstree-last');
							data.nodes
									.parentsUntil(".jstree")
									.addBack()
									.show()
									.filter(".jstree-children")
									.each(
											function() {
												$(this)
														.children(
																".jstree-node:visible")
														.eq(-1)
														.addClass(
																"jstree-last");
											});
							// nodes need to be in an expanded state
							matchingNodes.find(".jstree-node").show(); // change
						}
					}).on(
					"clear_search.jstree",
					function(e, data) {
						if (data.nodes.length) {
							$(this).find(".jstree-node").css("display", "")
									.filter('.jstree-last').filter(
											function() {
												return this.nextSibling;
											}).removeClass('jstree-last');
						}
					});
			
			$("#txtIndustryArea").keyup(function() {
			
					var v = $("#txtIndustryArea").val();
				
					$("#jstree1").jstree(true).search(v);
			});
		});
	</script>
</body>

</html>