package com.hx.gps.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

import com.hx.gps.util.*;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hx.gps.dao.AddressDao;
import com.hx.gps.dao.TaxiDao;
import com.hx.gps.entities.tool.Address;
import com.hx.gps.entities.tool.Alarm;
import com.hx.gps.entities.tool.Alarm2;
import com.hx.gps.entities.tool.AreaAddress;
import com.hx.gps.entities.tool.Blur;
import com.hx.gps.entities.tool.Gps;
import com.hx.gps.entities.tool.InfoQuery;
import com.hx.gps.entities.tool.Meter;
import com.hx.gps.entities.tool.MeterTemp;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.QueryOperators;

@Repository
public class AddressDaoImpl implements AddressDao {
	@Autowired
	private DateUtil dateUtil;
	@Autowired
	private TaxiDao taxiDao;
	@Autowired
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public List<Address> findAddressByDate(String startDate, String endDate, String isuNum, String dbname) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, dbname);
		BasicDBObject finds = new BasicDBObject();
		BasicDBObject sortBy = new BasicDBObject();
		List<Address> addresses = new ArrayList<>();

		sortBy.put("time", 1);
		finds.put("isuNum", isuNum);
		finds.put("time", new BasicDBObject("$lte", DateUtil.stringToDate(endDate)).append("$gte",
				DateUtil.stringToDate(startDate)));// 小于等于
													// 结束时间
		DBCursor cursor = coll.find(finds).sort(sortBy);
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			Address address = new Address(finds);
			addresses.add(address);
		}
		System.out.println("查询到轨迹" + addresses);
		return addresses;
	}

	@Override
	public Map<String, String> getIsuTaxis() {
		Map<String, String> map = new ConcurrentHashMap<>();
		// DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB,
		// FarmConstant.CJTAXINUM);
		// DBCursor cursor = coll.find();
		// BasicDBObject finds = new BasicDBObject();
		// while (cursor.hasNext()) {
		// finds = (BasicDBObject) cursor.next();
		// if (finds.containsField("isuNum")) {
		// if (finds.containsField("taxiNum"))
		// map.put(finds.getString("taxiNum"), finds.getString("isuNum"));
		// }
		// }
		String hql = "SELECT isuNum,taxiNum FROM Taxi t";
		Query query = getSession().createQuery(hql);
		// 默认查询出来的list里存放的是一个Object数组，还需要转换成对应的javaBean。
		List<Object[]> links = query.list();
		for (Object[] link : links) {
			String isuNum = (String) link[0];
			String taxiNum = (String) link[1];
			map.put(taxiNum, isuNum);
		}

		return map;
	}



	@Override
	public List<Address> findAddressByIsuNum(List<String> isuNums) {
		List<Address> addresses = new ArrayList<>();
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ADDRESSTRMP);
		BasicDBObject finds = new BasicDBObject();

		finds.append("isuNum", new BasicDBObject(QueryOperators.IN, isuNums));
		DBCursor db = coll.find(finds);
		while (db.hasNext()) {
			BasicDBObject object = (BasicDBObject) db.next();
			if (object != null) {
				addresses.add(new Address(object));
			}
		}
		return addresses;
	}

	@Override
	public List<Address> findAllTaxi() {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ADDRESSTRMP);
		BasicDBObject finds = new BasicDBObject();
		List<Address> addresses = new ArrayList<>();

		DBCursor cursor = coll.find();
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			addresses.add(new Address(finds));
		}
		return addresses;
	}

	@Override
	public List<Address> findAddressByisu(String isuNum, String startDate, String endDate, String dbname) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, dbname);
		BasicDBObject finds = new BasicDBObject();
		List<Address> addresses = new ArrayList<>();

		// taxiState:1空车2重车3不使用
		// 使用808 协议后修改该项，不再使用车辆状态来选择车辆
		/*
		 * try { if(taxiState.equals("1")||taxiState.equals("2")){
		 * finds.put("state",taxiState); } } catch (Exception e) { }
		 */

		finds.put("isuNum", isuNum);
		finds.put("time", new BasicDBObject("$lte", DateUtil.stringToDate(endDate)).append("$gte",
				DateUtil.stringToDate(startDate)));
		DBCursor cursor = coll.find(finds);
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			addresses.add(new Address(finds));
		}
		return addresses;
	}

	@Override
	public List<DBObject> findAddressByTime(String startDate, String endDate, String dbname) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, dbname);
		BasicDBObject finds = new BasicDBObject();
		List<Address> addresses = new ArrayList<>();

		// taxiState:1空车2重车3不使用
		// 使用808 协议后修改该项，不再使用车辆状态来选择车辆
		/*
		 * try { if(taxiState.equals("1")||taxiState.equals("2")){
		 * finds.put("state",taxiState); } } catch (Exception e) { }
		 */
		Long time1 = System.currentTimeMillis();
		finds.put("time", new BasicDBObject("$lte", DateUtil.stringToDate(endDate)).append("$gte",
				DateUtil.stringToDate(startDate)));
		BasicDBObject key = new BasicDBObject();
		key.append("longitude", 1).append("latitude", 1).append("isuNum", 1);
		List<DBObject> coursors = coll.find(finds).toArray();
		Long time2 = System.currentTimeMillis();
		System.err.println("查询所有符合条件的点占用时间" + (time2 - time1) + "查询结果数" + coursors.size());
		// while (cursor.hasNext()) {
		// finds = (BasicDBObject) cursor.next();
		// addresses.add(new Address(finds));
		// }
		Long time3 = System.currentTimeMillis();
		System.err.println("查询点转换成实体类" + (time3 - time2));
		return coursors;
	}

	@Override
	public void addAlarm(String alarmType, int preinfoId, String isuNum) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ALARM);
		BasicDBObject finds = new BasicDBObject();
		if (isuNum != null) {
			finds.put("isuNum", isuNum);
		}
		finds.put("alarmType", alarmType);
		finds.put("time", new Date());
		finds.put("dealState", "未处理");// 处理状态
		finds.put("remindState", "未报警");// 报警状态状态
		finds.put("preinfoId", preinfoId);
		coll.save(finds);

	}

	@Override
	public Address getLastByDate(String isuNum, String date, String dbname) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, dbname);
		BasicDBObject finds = new BasicDBObject();
		BasicDBObject sortBy = new BasicDBObject();
		Address address = null;
		// System.out.println("设备编号："+isuNum+" ,日期："+date+",数据库名称："+dbname);
		sortBy.put("currentTime", -1);
		finds.put("isuNum", isuNum);
		finds.put("time", new BasicDBObject("$lte", DateUtil.stringToDate(date)));
		DBCursor cursor = coll.find(finds).sort(sortBy).skip(0).limit(1);
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			address = new Address(finds);
		}
		return address;
	}

	@Override
	public AreaAddress getAreaAlarm(String isuNum, String time) {
		// 数据集查询参数
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "areaAlarm");
		// 查询条件
		BasicDBObject finds = new BasicDBObject();
		// 排序
		BasicDBObject sortBy = new BasicDBObject();
		AreaAddress address = null;
		// 按时间降序获取最大的一条
		// sortBy.put("time", -1);
		finds.put("isuNum", isuNum);
		// 找到小于等于该时间段
		finds.put("time", dateUtil.dateToISODate(time));
		// 唯一性查询
		DBCursor cursor = coll.find(finds).limit(1);
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			address = new AreaAddress(finds);
			// 轨迹纠偏
			Gps gps = PositionUtil.gcj02_To_Bd09(address.getLatitude(), address.getLongitude());
			address.setLatitude(gps.getWgLat());
			address.setLongitude(gps.getWgLon());
		}
		// 填充车牌号,数据库中不保存。
		if (address != null&&taxiDao.getTaxiByIsu(isuNum)!=null) {
			address.setTaxiNum(taxiDao.getTaxiByIsu(isuNum).getTaxiNum());
		}
		// 查询结果返回
		return address;
	}

	@Override
	public List<Alarm> getAllAlarm() {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ALARM);
		BasicDBObject finds = new BasicDBObject();
		DBCursor cursor = coll.find();
		List<Alarm> list = new ArrayList<>();
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			list.add(new Alarm(finds));
		}
		return list;
	}

	@Override
	public List<Alarm> getAlarmByDealState() {
		try {
			DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ALARM);
			BasicDBObject finds = new BasicDBObject();
			ArrayList<String> list1 = new ArrayList<>();
			list1.add("紧急报警");
			list1.add("磁盘已满");
			list1.add("未检测到磁盘");
			list1.add("磁盘数据错误");
			finds.put("dealState", "未处理");
			// finds.put("alarmType",new BasicDBObject("$ne","终端主电源欠压"));
			finds.put("alarmType", new BasicDBObject("$in", list1));
			DBCursor cursor = coll.find(finds);
			List<Alarm> list = new ArrayList<>();
			int nums=0;
			while (cursor.hasNext()) {
				finds = (BasicDBObject) cursor.next();
				list.add(new Alarm(finds));
				nums++;
				if (nums > 10000) {
					return list;
				}
			}
			return list;
		} catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Alarm> getDealAlarmByIsu(String alarmType, String isu) {
		try {

			DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ALARM);
			BasicDBObject finds = new BasicDBObject();
			finds.put("alarmType", alarmType);
			finds.put("isuNum", isu);
			finds.put("dealState", "未处理");
			DBCursor cursor = coll.find(finds);
			List<Alarm> list = new ArrayList<>();
			int nums=0;
			while (cursor.hasNext()) {
				finds = (BasicDBObject) cursor.next();
				list.add(new Alarm(finds));
				nums++;
				if (nums > 10000) {
					return list;
				}
			}
			return list;
		} catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Address findAddressByIsuNum(String isuNum) {
		System.out.println("===========findAddressByIsuNum=============");
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ADDRESSTRMP);
		BasicDBObject finds = new BasicDBObject();

		finds.put("isuNum", isuNum);
		BasicDBObject finds2 = (BasicDBObject) coll.findOne(finds);
		if (finds2 != null) {
			return (new Address(finds2));
		} else {
			return null;
		}
	}

	@Override
	public List<Address> screenOnline(List<String> isuNums, Date stratTime) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ADDRESSTRMP);
		BasicDBObject finds = new BasicDBObject();
		List<Address> addresses = new ArrayList<>();
		finds.put("time", new BasicDBObject("$gte", stratTime));// 大于等于开始时间
		finds.append("isuNum", new BasicDBObject(QueryOperators.IN, isuNums));
		DBCursor cursor = coll.find(finds);
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			addresses.add(new Address(finds));
		}
		return addresses;
	}

	@Override
	public List<Meter> getMeterByTaxiNums(List<String> taxiNums, String startDate, String endDate, String dbname) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, dbname);
		BasicDBObject finds = new BasicDBObject();
		List<Meter> operates = new ArrayList<>();
		finds.put("upTime", new BasicDBObject("$gte", DateUtil.stringToDate(startDate)));
		finds.put("downTime", new BasicDBObject("$lte", DateUtil.stringToDate(endDate)));// 小于等于
																							// 结束时间//大于等于开始时间
		finds.append("taxiNum", new BasicDBObject(QueryOperators.IN, taxiNums));
		DBCursor cursor = coll.find(finds);
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			operates.add(new Meter(finds));
		}
		return operates;
	}

	@Override
	public List<Alarm> getAlarmByTaxi(String isuNum, String startDate, String endDate, Integer alarmType) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ALARM);
		BasicDBObject finds = new BasicDBObject();
		if (alarmType == 1) {
			finds.put("alarmType", "紧急报警");
		} else if (alarmType == 2) {
			finds.put("alarmType", "聚集报警");
		} else if (alarmType == 3) {
			finds.put("alarmType", "超速报警");
		} else if (alarmType == 4) {
			finds.put("alarmType", "进出区域");
		} else if (alarmType == 5) {
			finds.put("alarmType", "疲劳驾驶报警");
		}
		if (isuNum != null) {
			finds.put("isuNum", isuNum);
		}
		finds.put("time", new BasicDBObject("$lte", DateUtil.stringToDate(endDate)).append("$gte",
				DateUtil.stringToDate(startDate)));

		DBCursor cursor = coll.find(finds);
		List<Alarm> list = new ArrayList<>();
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			list.add(new Alarm(finds));
		}
		return list;
	}

	// wk后台分页，根据终端号，时间段，处理状态等查看报警消息
	@Override
	public Blur getAlarmByTaxi1(String isuNum, InfoQuery infoQuery) {
		Blur blur = new Blur();
		String endDate = infoQuery.getEndDate();
		String startDate = infoQuery.getStartDate();
		// System.out.println("【报警日志进入dao】");
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB,FarmConstant.ALARM);
		BasicDBObject finds = new BasicDBObject();
		if (infoQuery.getAlarmType() == 1) {
			finds.put("alarmType", "紧急报警");
		} else if (infoQuery.getAlarmType() == 2) {
			finds.put("alarmType", "疲劳驾驶");
		} else if (infoQuery.getAlarmType() == 3) {
			finds.put("alarmType", "超速报警");
		} else if (infoQuery.getAlarmType() == 4) {
			finds.put("alarmType", "GNSS模块发生故障");
		} else if (infoQuery.getAlarmType() == 5) {
			finds.put("alarmType", "GNSS天线未接或被剪断");
		}else if (infoQuery.getAlarmType() == 6) {
			finds.put("alarmType", "GNSS天线短路");
		} else if (infoQuery.getAlarmType() == 7) {
			finds.put("alarmType", "终端主电源欠压");
		} else if (infoQuery.getAlarmType() == 8) {
			finds.put("alarmType", "终端主电源掉电");
		} else if (infoQuery.getAlarmType() == 9) {
			finds.put("alarmType", "终端LED或显示器故障");
		}else if (infoQuery.getAlarmType() == 10) {
			finds.put("alarmType", "TTS模块故障");
		} else if (infoQuery.getAlarmType() == 11) {
			finds.put("alarmType", "摄像头故障");
		} else if (infoQuery.getAlarmType() == 12) {
			finds.put("alarmType", "磁盘数据错误");
		} else if (infoQuery.getAlarmType() == 13) {
			finds.put("alarmType", "未检测到磁盘");
		}else if (infoQuery.getAlarmType() == 14) {
			finds.put("alarmType", "磁盘已满");
		} else if (infoQuery.getAlarmType() == 15) {
			finds.put("alarmType", "当天累计驾驶超时");
		} else if (infoQuery.getAlarmType() == 16) {
			finds.put("alarmType", "超时停车");
		} else if (infoQuery.getAlarmType() == 17) {
			finds.put("alarmType", "进出区域");
		} else if (infoQuery.getAlarmType() == 18) {
			finds.put("alarmType", "进出路线");
		} else if (infoQuery.getAlarmType() == 19) {
			finds.put("alarmType", "路段行驶时间不足");
		} else if (infoQuery.getAlarmType() == 20) {
			finds.put("alarmType", "路线偏移报警");
		} else if (infoQuery.getAlarmType() == 21) {
			finds.put("alarmType", "车辆VSS故障");
		} else if (infoQuery.getAlarmType() == 22) {
			finds.put("alarmType", "车辆油量异常");
		} else if (infoQuery.getAlarmType() == 23) {
			finds.put("alarmType", "车辆被盗");
		} else if (infoQuery.getAlarmType() == 24) {
			finds.put("alarmType", "车辆非法点火");
		} else if (infoQuery.getAlarmType() == 25) {
			finds.put("alarmType", "车辆非法位移");
		} else if (infoQuery.getAlarmType() == 26) {
			finds.put("alarmType", "预警");
		}
		if (isuNum != null) {
			finds.put("isuNum", isuNum);
		}
		if (infoQuery.getEndDate().equals(null)) {
			infoQuery.setEndDate("");
		}
		// 模糊查询
		if (infoQuery.getSearch() != null) {
			// System.out.println("【报警日志开始模糊mongodb】");
			Pattern pattern = Pattern.compile("^.*" + infoQuery.getSearch() + ".*$", Pattern.CASE_INSENSITIVE);
			BasicDBList values = new BasicDBList();
			values.add(new BasicDBObject("dealState", pattern));
			values.add(new BasicDBObject("alarmType", pattern));

			finds.put("time", new BasicDBObject("$lte", DateUtil.stringToDate(endDate)).append("$gte",
					DateUtil.stringToDate(startDate)));

			finds.put("$or", values);

			Integer size = coll.find(finds).count();

			DBCursor cursor = coll.find(finds).skip((infoQuery.getOffset())).limit(infoQuery.getLimit());
			// System.out.println("【报警日志模糊分页查询】："+cursor.toString());
			List<Alarm2> list = new ArrayList<>();
			while (cursor.hasNext()) {
				finds = (BasicDBObject) cursor.next();
				list.add(new Alarm2(finds));
			}
			// System.out.println("【dao报警日志后台模糊分页alarms】"+list);
			blur.setListAlarm(list);
			blur.setSize(size);
			return blur;
		} else {
			finds.put("time", new BasicDBObject("$lte", DateUtil.stringToDate(endDate)).append("$gte",
					DateUtil.stringToDate(startDate)));

			// System.out.println("【报警日志开始查mongodb】");
			Integer size = coll.find(finds).count();
			DBCursor cursor = coll.find(finds).skip((infoQuery.getOffset())).limit(infoQuery.getLimit());
			// System.out.println("【报警日志分页查询】："+cursor.toString());
			List<Alarm2> list = new ArrayList<>();
			while (cursor.hasNext()) {
				finds = (BasicDBObject) cursor.next();
				list.add(new Alarm2(finds));
			}
			// System.out.println("【dao报警日志后台分页alarms】"+list);
			blur.setListAlarm(list);
			blur.setSize(size);
			return blur;
		}

	}

	@Override
	public List<Meter> getMeterByTaxiNum(String taxiNum, String startDate, String endDate, String dbname) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, dbname);
		BasicDBObject finds = new BasicDBObject();
		List<Meter> operates = new ArrayList<>();
		finds.put("upTime", new BasicDBObject("$gte", DateUtil.stringToDate(startDate)));
		finds.put("downTime", new BasicDBObject("$lte", DateUtil.stringToDate(endDate)));// 小于等于
																							// 结束时间//大于等于开始时间
		finds.put("taxiNum", taxiNum);
		DBCursor cursor = coll.find(finds);
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			operates.add(new Meter(finds));
		}
		return operates;
	}

	@Override
	public MeterTemp getTempMeterByIsuNum(String isuNum) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.METERTEMP);
		BasicDBObject finds = new BasicDBObject();
		finds.put("isuNum", isuNum);
		finds = (BasicDBObject) coll.findOne(finds);
		MeterTemp meter = new MeterTemp();
		if (finds != null) {
			meter = new MeterTemp(finds);
		}
		return meter;
	}

	@Override
	public List<MeterTemp> getAllMeterTemp() {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.METERTEMP);
		BasicDBObject finds = new BasicDBObject();
		List<MeterTemp> meterTemps = new ArrayList<>();
		DBCursor cursor = coll.find();
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			meterTemps.add(new MeterTemp(finds));
		}
		return meterTemps;
	}

	// 单车在线，通过终端心跳。
	@Override
	public String getOnlineOfHeart(String isuNum) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ISUHEART);
		BasicDBObject finds = new BasicDBObject();
		BasicDBObject sortBy = new BasicDBObject();
		boolean b = false;
		String result = "状态异常";
		finds.put("isuNum", isuNum);
		sortBy.put("time", -1);
		System.out.println("isuNum" + isuNum);
		DBCursor cursor = coll.find(finds).sort(sortBy).skip(0).limit(1);
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
		}
		/*
		 * finds=(BasicDBObject) coll.findOne(finds);
		 * System.out.println("finds"+finds);
		 */
		Date time = finds.getDate("time");
		try {
			b = (time.getTime() >= DateUtil.stringToDate(DateUtil.countDate(FarmConstant.ONLINE)).getTime());
			if (b) {
				result = "在线";
			} else {
				result = "离线";
			}
		} catch (Exception e) {
			System.out.println("获取在线状态时出现异常" + e.toString());
		}
		return result;

	}

	@Override
	public Integer getOnlineNum(String time) {
		// 获取在线车辆 在线状态判断由心跳表修改为实时位置信息表。
		// DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB,
		// FarmConstant.ISUHEART);
		//
		// DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB,
		// FarmConstant.ADDRESSTRMP);
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.onlineTemp);
		BasicDBObject finds = new BasicDBObject();
		Integer n = 0;
		finds.put("time", new BasicDBObject("$gte", DateUtil.stringToDate(time)));
		System.out.println("界面在线率计算：判断时间" + DateUtil.stringToDate(time));
		DBCursor cursor = coll.find(finds);
		System.out.println(cursor.count());
		while (cursor.hasNext()) {

			finds = (BasicDBObject) cursor.next();
			n++;
		}
		return n;
	}

	@Override
	public int screenOnlineOfMeter(List<String> isuNums, Date stratTime) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.METERTEMP);
		BasicDBObject finds = new BasicDBObject();
		List<MeterTemp> meters = new ArrayList<>();
		finds.put("time", new BasicDBObject("$gte", stratTime));// 大于等于开始时间
		finds.append("isuNum", new BasicDBObject(QueryOperators.IN, isuNums));
		DBCursor cursor = coll.find(finds);

		return cursor.size();
	}

	@Override
	public int screenOnlineOfAddress(List<String> isuNums, Date stratTime) {
		// DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB,
		// FarmConstant.ADDRESSTRMP);
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.onlineTemp);

		Integer n = 0;
		BasicDBObject finds = new BasicDBObject();
		List<Address> addresses = new ArrayList<>();
		finds.put("time", new BasicDBObject("$gte", stratTime));// 大于等于开始时间
		finds.append("isuNum", new BasicDBObject(QueryOperators.IN, isuNums));
		DBCursor cursor = coll.find(finds);
		/*
		 * while (cursor.hasNext()) { finds = (BasicDBObject) cursor.next();
		 * addresses.add(new Address(finds)); }
		 */
		return cursor.size();
	}

	@Override
	public Integer screenOnlineOfHeart(List<String> isuNums, Date stratTime) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ISUHEART);
		Integer n = 0;
		BasicDBObject finds = new BasicDBObject();
		// List<Address> addresses=new ArrayList<>();
		finds.put("time", new BasicDBObject("$gte", stratTime));// 大于等于开始时间
		finds.append("isuNum", new BasicDBObject(QueryOperators.IN, isuNums));
		DBCursor cursor = coll.find(finds);
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			// addresses.add(new Address(finds));
			n++;
		}
		return n;
	}

	@Override
	public String getOnlineOfMeter(String isuNum) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.METERTEMP);
		BasicDBObject finds = new BasicDBObject();
		BasicDBObject sortBy = new BasicDBObject();
		boolean b = false;
		String result = "状态异常";
		finds.put("isuNum", isuNum);
		sortBy.put("time", -1);

		DBCursor cursor = coll.find(finds).sort(sortBy).skip(0).limit(1);// 鉴于计价器实时表只含有最新消息，所以取消排序？
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
		}
		Date time = finds.getDate("time");
		try {
			b = (time.getTime() >= DateUtil.stringToDate(DateUtil.countDate(FarmConstant.ONLINE)).getTime());
			if (b) {
				result = "在线";
			} else {
				result = "离线";
			}
		} catch (Exception e) {
			System.out.println("获取在线状态时出现异常");
		}

		return result;
	}

	// 该方法修改为根据onlineTemp表来判断
	@Override
	public String getOnlineOfAddress(String isuNum) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.onlineTemp);
		BasicDBObject finds = new BasicDBObject();
		BasicDBObject sortBy = new BasicDBObject();
		boolean b = false;
		String result = "状态异常";
		finds.put("isuNum", isuNum);
		// sortBy.put("time", -1);

		DBCursor cursor = coll.find(finds).skip(0).limit(1);// 鉴于计价器实时表只含有最新消息，所以取消排序？
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
		}
		Date time = finds.getDate("time");
		try {
			b = (time.getTime() >= DateUtil.stringToDate(DateUtil.countDate(FarmConstant.ONLINE)).getTime());
			if (b) {
				result = "在线";
			} else {
				result = "离线";
			}
		} catch (Exception e) {
			System.out.println("获取在线状态时出现异常");

		}
		return result;
	}

	// 查询onlinetemp的总记录数得到在线车辆
	@Override
	public Integer getOnlineFromOnlineTemp() {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ISU);
		BasicDBObject finds = new BasicDBObject();
		int result = coll.find(finds).count();

		return result;
	}

	@Override
	public List<Alarm> getAllAlarmType(Date startDate, Date endDate, String alarmType){
		String[] allTypes = {"紧急报警", "疲劳驾驶", "超速报警", "GNSS模块发生故障", "GNSS天线未接或被剪断", "GNSS天线短路"
				, "终端主电源欠压", "终端主电源掉电", "终端LED或显示器故障", "TTS模块故障", "摄像头故障", "磁盘数据错误", "未检测到磁盘", "磁盘已满",
				"当天累计驾驶超时", "超时停车", "进出区域", "进出路线", "路段行驶时间不足", "路线偏移报警", "车辆VSS故障", "车辆油量异常"
				, "车辆被盗", "车辆非法点火", "车辆非法位移", "预警"
		};
		List<Alarm> resultList = new ArrayList<>();
		for (String type : allTypes) {
			DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ALARM);
			DBCursor dbCursor = coll.find(new BasicDBObject("alarmType", type)).limit(1).sort(new BasicDBObject("time", -1));
			if (dbCursor.toArray().size() != 0) {
				DBObject dbObject = dbCursor.toArray().get(0);
				Alarm alarm = new Alarm();
				alarm.setAlarmType(dbObject.get("alarmType").toString());
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date time = (Date) dbObject.get("time");
				String format1 = format.format(time);
				alarm.setTime(format1);
				alarm.setDealState(dbObject.get("dealState").toString());
				resultList.add(alarm);
			}
		}
		resultList.sort(new Comparator<Alarm>() {
			@Override
			public int compare(Alarm o1, Alarm o2) {
				try {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date date1 = format.parse(o1.getTime());
					Date date2 = format.parse(o2.getTime());
					if (date1.getTime() > date2.getTime()) {
						return -1;
					} else {
						return 1;
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return 0;
			}
		});
		return resultList;
	}

	@Override
	public List<Alarm> getAlarmByDealState1() {
		try {
			DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ALARM);
			BasicDBObject finds = new BasicDBObject();
			ArrayList<String> list1 = new ArrayList<>();
			list1.add("紧急报警");
			list1.add("磁盘已满");
			list1.add("未检测到磁盘");
			list1.add("磁盘数据错误");
			finds.put("dealState", "未处理");
			// finds.put("alarmType",new BasicDBObject("$ne","终端主电源欠压"));
			finds.put("alarmType", new BasicDBObject("$in", list1));
			//获取今日凌晨的时间戳
			Date endDate = new Date();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String format = simpleDateFormat.format(endDate);
			Date parse = simpleDateFormat.parse(format);

			System.out.println(parse);
			finds.put("time",new BasicDBObject("$gte",parse));
			DBCursor cursor = coll.find(finds);
			List<Alarm> list = new ArrayList<>();
			int nums=0;
			while (cursor.hasNext()) {
				finds = (BasicDBObject) cursor.next();
				list.add(new Alarm(finds));
				nums++;
				if(nums>10000){
					return list;
				}
			}
			System.out.println(list.size());
			return list;
		} catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Alarm> getAlarmNums() {
		try {
			DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ALARM);
			BasicDBObject finds = new BasicDBObject();
			finds.put("dealState", "未处理");
			// finds.put("alarmType",new BasicDBObject("$ne","终端主电源欠压"));

			//获取今日凌晨的时间戳
			Date endDate = new Date();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String format = simpleDateFormat.format(endDate);
			Date parse = simpleDateFormat.parse(format);

			finds.put("time",new BasicDBObject("$gte",parse));
			DBCursor cursor = coll.find(finds);
			List<Alarm> list = new ArrayList<>();
			int nums=0;
			while (cursor.hasNext()) {
				finds = (BasicDBObject) cursor.next();
				list.add(new Alarm(finds));
				nums++;
				if(nums>10000){
					return list;
				}
			}
			System.out.println(list.size());
			return list;
		} catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Alarm> getAlarmNum() {
		try {
			DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ALARM);
			BasicDBObject finds = new BasicDBObject();
			// finds.put("alarmType",new BasicDBObject("$ne","终端主电源欠压"));

			//获取今日凌晨的时间戳
			Date endDate = new Date();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String format = simpleDateFormat.format(endDate);
			Date parse = simpleDateFormat.parse(format);

			finds.put("time",new BasicDBObject("$gte",parse));
			DBCursor cursor = coll.find(finds);
			List<Alarm> list = new ArrayList<>();
			int nums=0;
			while (cursor.hasNext()) {
				finds = (BasicDBObject) cursor.next();
				list.add(new Alarm(finds));
				nums++;
				if(nums>10000){
					return list;
				}
			}
			System.out.println(list.size());
			return list;
		} catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) {
		String[] allTypes = {"紧急报警", "疲劳驾驶", "超速报警", "GNSS模块发生故障", "GNSS天线未接或被剪断", "GNSS天线短路"
				, "终端主电源欠压", "终端主电源掉电", "终端LED或显示器故障", "TTS模块故障", "摄像头故障", "磁盘数据错误", "未检测到磁盘", "磁盘已满",
				"当天累计驾驶超时", "超时停车", "进出区域", "进出路线", "路段行驶时间不足", "路线偏移报警", "车辆VSS故障", "车辆油量异常"
				, "车辆被盗", "车辆非法点火", "车辆非法位移", "预警"
		};
		List<Alarm> resultList = new ArrayList<>();
		for (String type : allTypes) {
			DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ALARM);
			DBCursor dbCursor = coll.find(new BasicDBObject("alarmType", type)).limit(1).sort(new BasicDBObject("time", -1));
			if (dbCursor.toArray().size() != 0) {
				DBObject dbObject = dbCursor.toArray().get(0);
				Alarm alarm = new Alarm();
				alarm.setAlarmType(dbObject.get("alarmType").toString());
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date time = (Date) dbObject.get("time");
				String format1 = format.format(time);
				alarm.setTime(format1);
				alarm.setDealState(dbObject.get("dealState").toString());
				resultList.add(alarm);
			}
		}
		resultList.sort(new Comparator<Alarm>() {
			@Override
			public int compare(Alarm o1, Alarm o2) {
				try {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date date1 = format.parse(o1.getTime());
					Date date2 = format.parse(o2.getTime());
					if (date1.getTime() > date2.getTime()) {
						return -1;
					} else {
						return 1;
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return 0;
			}
		});
		System.out.println(resultList);
	}
}
