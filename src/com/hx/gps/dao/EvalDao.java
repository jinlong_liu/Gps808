package com.hx.gps.dao;

import java.util.List;

import com.hx.gps.entities.tool.Evaluate;

public interface EvalDao {
	
	//查看指定月份的所有评价信息
	public List<Evaluate> getEvalByDate(String dbname);
	
	

}
