<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
<meta charset="utf-8" />
<title>路书</title>
<style type="text/css">
body,html {
	width: 100%;
	height: 100%;
	margin: 0;
	font-family: "微软雅黑";
}

#map_canvas {
	width: 100%;
	height: 500px;
}

#result {
	width: 100%
}
</style>
<script
	src="http://api.map.baidu.com/api?v=2.0&ak=xj2Gd1nMEwDHFcGeQvr0WORGk0t1Dlhw"></script>
<!-- <script type="text/javascript"
	src="http://api.map.baidu.com/library/LuShu/1.2/src/LuShu_min.js"></script> -->
	<script type="text/javascript" src="js/lushu.js"></script>
</head>
<body>
	<div id="map_canvas" style="height:90%;"></div>
	<div id="result"></div>
	<button id="run">开始</button>
	<button id="stop">停止</button>
	<button id="pause">暂停</button>
	<button id="hide">隐藏信息窗口</button>
	<button id="show">展示信息窗口</button>
	<a href="trackBack.jsp"><button>返回</button></a>
	
	<script>
		var map = new BMap.Map('map_canvas');
		map.enableScrollWheelZoom();
		map.centerAndZoom(new BMap.Point(120.490373,36.126672), 13);
		var lushu;
		// 实例化一个驾车导航用来生成路线
		var drv = new BMap.DrivingRoute(
				'青岛',
				{
					onSearchComplete : function(res) {
						var arrPois = [];
						<c:forEach var="obj" items="${address}">
						var point = new BMap.Point(${obj.longitude},
								${obj.latitude});
						arrPois = arrPois.concat(point);
						</c:forEach>
						/* alert(arrPoi.length); */
						//开始坐标转换
						translateCallback = function(data) {
						if (data.status === 0) {
							var points = [];
							for (var i = 0; i < data.points.length; i++) {
								//遍历获取到转换后的坐标，并且放入到一个新的数组中
								points = points.concat(data.points[i]);
							}
							//开始进行路书操作
							//开始对点进行操作
						map.addOverlay(new BMap.Polyline(points, {
							strokeColor : 'blue'
						}));//向地图中添加导航线
						map.setViewport(points);//设置视图端口

						lushu = new BMapLib.LuShu(
								map,
								points,
								{
									defaultContent : "",//默认为空，小汽车上的文本框，可展示车牌号等信息
									autoView : true,//是否开启自动视野调整，如果开启那么路书在运动过程中会根据视野自动调整，，即地图视角会因小汽车移动到视线外而自动调整视野
									icon : new BMap.Icon(
											'http://developer.baidu.com/map/jsdemo/img/car.png',
											new BMap.Size(52, 26), {
												anchor : new BMap.Size(27, 13)
											}),//添加的小汽车图标
									speed : 300, //小汽车在地图上的行驶速度
									enableRotation : true,//是否设置marker随着道路的走向进行旋转
									landmarkPois : [ //删除该部分会导致停止之后 无法再次开始
											{
												lng : 116.314782,
												lat : 39.913508,
												html : '加油站',
												pauseTime : 2
											},
											{
												lng : 116.315391,
												lat : 39.964429,
												html : '高速公路收费<div><img src="http://map.baidu.com/img/logo-map.gif"/></div>',
												pauseTime : 3
											},
											{
												lng : 116.381476,
												lat : 39.974073,
												html : '肯德基早餐<div><img src="http://ishouji.baidu.com/resource/images/map/show_pic04.gif"/></div>',
												pauseTime : 2
											} ]
								});
							
						}
					}
					setTimeout(function() {
						//bm.clearOverlays();//在车辆实时监控中用于清除上次标注的图标
						var convertor = new BMap.Convertor();
						convertor.translate(arrPois, 1, 5, translateCallback)
					},0);
						
						
								//对点的操作到此结束
						//}
					}
				});
		//drv.search('天安门', '百度大厦');
		//drv.search('青岛科技大学','维客广场','青岛恒星科技学院');
		//drv.search('青岛恒星科技学院','维客广场');
		drv.search('维客广场', '汽车北站');
		//绑定事件
		$("run").onclick = function() {
			lushu.start();
			$("kaishi").onclick();
		}
		$("stop").onclick = function() {
			lushu.stop();
		}
		$("pause").onclick = function() {
			lushu.pause();
			$("kaishi").onclick();
		}
		$("hide").onclick = function() {
			lushu.hideInfoWindow();
		}
		$("show").onclick = function() {
			lushu.showInfoWindow();
		}
		$("kaishi").onclick = function() {

		}
		function $(element) {
			return document.getElementById(element);
		}
	</script>
</body>
</html>
