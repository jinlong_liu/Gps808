package com.hx.gps.dao;

import java.util.List;

import com.hx.gps.entities.Company;
import com.hx.gps.entities.Driver;
import com.hx.gps.entities.IsuCache;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.tool.Address;
import com.hx.gps.entities.tool.Blur;
import com.hx.gps.entities.tool.ComInfo2;
import com.hx.gps.entities.tool.DatatablesQuery;
import com.hx.gps.entities.tool.MeterTemp;
import com.hx.gps.entities.tool.TaxiDetails;

public interface TaxiDao {
	/**
	 * WK新增 查询车辆记录数
	 */
	public Integer getTaxiSize(DatatablesQuery datatablesQuery, String[] taxiNums);
	/**
	 * WK新增 根据车牌号查询相应的车辆表记录（包含终端号）后台分页、轮询相关
	 */
	public Taxi getTaxiByTaxi2(DatatablesQuery datatablesQuery, String taxiNum);
	/**
	 * WK新增 首页车辆列表模糊查询添加
	 */
	MeterTemp getTempMeterByIsuNum(String isuNum);
	/**
	 * WK新增 首页模糊查询车辆列表
	 */
	public Blur getTaxiList(DatatablesQuery datatablesQuery,
			String[] taxiNums);
	/**
	 * wk新增 查询所有终端号
	 */
	public String[] getAllIsuNums();
	/**
	 * wk新增 根据车牌号查终端号
	 */
	String getIsuByTaxiNum(String taxiNum);
	
	/**
	 * wk新增 根据车牌号查终端号方式2 车牌号参数形式不同 2不带汉字
	 */
	String getIsuByTaxiNum2(String taxiNum);
	
	//根据车牌号查询相应的车辆表记录（包含终端号）
	public Taxi getTaxiByTaxi(String taxiNum);
	
	//根据车辆终端号来查询相应的车辆信息
	public Taxi getTaxiByIsu(String isuNum);
	
	//根据条件对车辆进行筛选,条件：车牌号（taxiNum），车辆类型（taxiType），车辆颜色（taxiColor）,所属公司（company）
	public List<Taxi> selectTaxi(String taxiNum,String taxiType,String taxiColor,String company);
	
	//添加公司信息
	public void addCompany(Company company);
	
	//添加司机信息
	public void addDriver(Driver driver);
	
	//查询所有的车辆信息
	public List<Taxi> getAllTaxi();
	
	//根据车牌号查询司机信息
	public List<Driver> getDriverByTaxi(String taxiNum);
	
	//查出司机的所有信息
	public List<Driver> getAllDriver();
	
	//通过公司id，查询出所选公司的车辆的isuNum终端号
	public List<String> getIsuNumsByPany(List<Integer> ids);
	
	//通过公司id，查询出所选公司的车辆的车牌号的集合
	public List<String> getTaxiNumsByPany(List<Integer> companyId);
	
	//通过车牌号的集合，查出相关公司信息
	public List<ComInfo2> getPanyBytaxiNums(List<String> taxiNums);
	
	//通过汽车id查找司机
	public List<Driver> getDriverByTaxiId(Integer id);
	
	//通过车牌号，查询公司信息
	public Company getCompanyBytaxi(String taxiNum);
	//查询缓存表全部记录
	public List<IsuCache> getAllIsuCache();
	//将失败数据插入缓存表
	public void addIsuCache(Taxi taxi,int type);
	public void addIsuCache(Taxi taxi,String isu2,int type);
	//删除当前缓存表记录
	public void deleteIsuCacheById(IsuCache isuCache);
	//查询全部插入操作
	public List<IsuCache>getIsuCacheForSave();
	//查询全部更新操作
	public List<IsuCache>getIsuCacheForUpdate();
	//查询全部删除操作
	public List<IsuCache>getIsuCacheForDelete();
	//插入mongodb
	public void addIsu(String Isu);
	//更新mongodb
	public void updateIsu(String isu1,String isu2);
	//删除mongodb
	public void deleteIsu(String isu);
	//查询mongodb
	public boolean  findIsu(String isu);
	//通过id查询手机号
	public String getIsuById(Integer id);
	//获取车辆详细状态
	public TaxiDetails getDetailsByIsu(String isu);
	
}



