<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<style type="text/css">
body,html {
	width: 100%;
	height: 100%;
	margin: 0;
	font-family: "微软雅黑";
}

#allmap {
	width: 100%;
	height: 500px;
	overflow: hidden;
}

#result {
	width: 100%;
	font-size: 12px;
}

dl,dt,dd,ul,li {
	margin: 0;
	padding: 0;
	list-style: none;
}

p {
	font-size: 12px;
}

dt {
	font-size: 14px;
	font-family: "微软雅黑";
	font-weight: bold;
	border-bottom: 1px dotted #000;
	padding: 5px 0 5px 5px;
	margin: 5px 0;
}

dd {
	padding: 5px 0 0 5px;
}

li {
	line-height: 28px;
}
</style>
<script type="text/javascript"
	src="http://api.map.baidu.com/api?v=2.0&ak=xj2Gd1nMEwDHFcGeQvr0WORGk0t1Dlhw"></script>
<!--加载鼠标绘制工具-->
<script type="text/javascript"
	src="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.js"></script>
<link rel="stylesheet"
	href="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.css" />
<!--加载检索信息窗口-->
<script type="text/javascript"
	src="http://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.js"></script>
<link rel="stylesheet"
	href="http://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
<title>鼠标绘制工具</title>
</head>
<body>
	<div id="allmap"
		style="height:90%;overflow:hidden;zoom:1;position:relative;">
		<div id="map"
			style="height:100%;-webkit-transition: all 0.5s ease-in-out;transition: all 0.5s ease-in-out;"></div>
	</div>
	<div id="result">
		<input type="button" value="获取绘制的覆盖物个数"
			onclick="alert(overlays.length)" /> <input type="button"
			value="清除所有覆盖物" onclick="clearAll()" /> <input type="button"
			value="隐藏工具栏" onclick="getOverlays()" />
		<form action="putInformation" method="post">
			<input type="hidden" id="state" name="state"> <input
				type="hidden" id="point1" name="point1"> <input
				type="hidden" id="point2" name="point2"> <input
				type="hidden" id="point3" name="point3"> <input
				type="hidden" id="point4" name="point4"> <input
				type="hidden" id="point5" name="point5"> <input
				type="hidden" id="point6" name="point6"> 
				开始时间： <input
				type="text" name="startDate" id="startDate"
				value="${meterStartDate }" class="Wdate"
				onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'endDate\')||\'new Date()\'}',readOnly:true})">
			结束时间： <input type="text" name="endDate" id="endDate"
				placeholder="默认为当前时间" value="${meterEndDate }" class="Wdate"
				onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'startDate\')}',maxDate:'#F{\'new Date()\'}',dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true})">
			<br> <input type="submit" value="提交">
		</form>

	</div>
	<script type="text/javascript">
		// 百度地图API功能
		var map = new BMap.Map('map');
		var poi = new BMap.Point(116.307852, 40.057031);
		map.centerAndZoom(poi, 12);
		map.enableScrollWheelZoom(); //添加滚轮缩放地图功能
		//通过ip定位城市
		function myFun(result) {
			var cityName = result.name;
			map.setCenter(cityName);
			//alert("当前定位城市:" + cityName);
		}
		var myCity = new BMap.LocalCity();
		myCity.get(myFun);

		//添加在地图上添加覆盖物的控件
		/* var overlays = [];
		var overlaycomplete = function(e) {
			overlays.push(e.overlay);
		}; */
		var overlays = [];
		var overlaycomplete = function(e) {
			//clearAll();
			overlays.push(e.overlay);
		}
		var styleOptions = {
			strokeColor : "red", //边线颜色。
			fillColor : "silver", //填充颜色。当参数为空时，圆形将没有填充效果。
			strokeWeight : 3, //边线的宽度，以像素为单位。
			strokeOpacity : 0.3, //边线透明度，取值范围0 - 1。
			fillOpacity : 0.6, //填充的透明度，取值范围0 - 1。
			strokeStyle : 'solid' //边线的样式，solid或dashed。
		}
		//实例化鼠标绘制工具
		var drawingManager = new BMapLib.DrawingManager(map, {
			isOpen : false, //是否开启绘制模式
			enableDrawingTool : true, //是否显示工具栏
			//enableCalculate:true, //开启面积计算
			drawingToolOptions : {
				anchor : BMAP_ANCHOR_TOP_RIGHT, //位置
				offset : new BMap.Size(5, 5), //偏离值
			},
			//对于各个图形的样式
			circleOptions : styleOptions, //圆的样式
			polylineOptions : styleOptions, //线的样式 *
			polygonOptions : styleOptions, //多边形的样式
			rectangleOptions : styleOptions
		//矩形的样式
		});
		drawingManager.addEventListener('overlaycomplete',function(e) {	//鼠标绘制完成后执行该事件
			clearAll();
			overlaycomplete(e);//先清除以前所有的再添加刚画的
			if(e.drawingMode=="circle"){
				var r=e.overlay.getRadius();
				var longitude=e.overlay.getCenter().lng;
				var latitude=e.overlay.getCenter().lat;
				$("#state").val("1"); 
				$("#point1").val(longitude+","+latitude);
				$("#point2").val(r);
						
			}else if(e.drawingMode=="rectangle"){
				$("#state").val("2"); 
				$("#point1").val(e.overlay.getBounds().getNorthEast().lng+","+e.overlay.getBounds().getNorthEast().lat);
				$("#point2").val(e.overlay.getBounds().getSouthWest().lng+","+e.overlay.getBounds().getSouthWest().lat);
			}else if(e.drawingMode=="polygon"){
				var points=[];
				points=e.overlay.getPath();
				alert(points.length);
				if(points.length>6){
					alert("多边形不能超过六个顶点");
				}else{
					$("#state").val("3"); 
					for(var i=0;i<points.length;i++){
						$("#point"+(i+1)).val(points[i].lng+","+points[i].lat);
					}
				}
			}else{
				alert("不能识别该覆盖物");
			}
		});
		function clearAll() {
			for (var i = 0; i < overlays.length; i++) {
				map.removeOverlay(overlays[i]);
				//map.clearOverlays();
			}
			overlays.length = 0;
		}
		
	</script>
</body>
</html>
