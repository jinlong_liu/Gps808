package com.hx.gps.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hx.gps.dao.AssessDao;
import com.hx.gps.dao.EvalDao;
import com.hx.gps.dao.TaxiDao;
import com.hx.gps.entities.Assarch;
import com.hx.gps.entities.Asscycle;
import com.hx.gps.entities.Complaints;
import com.hx.gps.entities.Comptype;
import com.hx.gps.entities.Deduct;
import com.hx.gps.entities.Driver;
import com.hx.gps.entities.Evalscore;
import com.hx.gps.entities.Scorerank;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.tool.EvalVector;
import com.hx.gps.entities.tool.Evaluate;
import com.hx.gps.service.AssessService;
import com.hx.gps.util.DateUtil;
import com.hx.gps.util.FarmConstant;

@Service
public class AssessServiceImpl implements AssessService {
	@Autowired
	private AssessDao assessDao;
	@Autowired
	private TaxiDao taxiDao;
	@Autowired
	private EvalDao evalDao;
	@Autowired
	private DateUtil dateUtil;
	private static Log logger = LogFactory.getLog(AssessServiceImpl.class);

	@Override
	public String addComplaint(Complaints complaint, Integer driver, Integer taxi, Integer reportType) {
		Taxi t = new Taxi();
		Driver d = new Driver();
		Comptype c = new Comptype();

		t.setId(taxi);
		d.setId(driver);
		c.setId(reportType);
		complaint.setTaxi(t);
		complaint.setDriver(d);
		complaint.setComptype(c);
		complaint.setType(1); // 添加投诉处理状态,1表示初始状态，0表示审核未通过，直接结束
		// 根据id是否存在，判断执行更新操作还是保存操作
		if (complaint.getId() == null) { // 执行保存操作
			if (assessDao.addComplaint(complaint)) {
				logger.info("添加投诉信息" + complaint.toString());
				return "投诉信息保存成功";
			} else {
				logger.info("投诉信息保存失败" + complaint.toString());
				return "投诉信息保存失败";
			}

		} else { // 执行更新操作
			Complaints c1 = assessDao.getCompById(complaint.getId());
			if (c1 != null && c1.getType() == complaint.getType()) {
				assessDao.updateComplaint(complaint);
				logger.info("更新投诉信息，id为" + complaint.getId());
				return "更新操作执行成功";
			} else {
				logger.info("更新投诉信息失败" + "id" + complaint.getId());
				return "更新操作执行失败，投诉状态已被改变";
			}
		}
	}
@Override
public List<Complaints> getAllCompliant() {
	List<Complaints> complaints=new ArrayList<>();
	complaints=assessDao.getAllComplaints();
	return complaints;
}
	@Override
	public List<Comptype> getCompType() {
		return assessDao.getCompType();
	}

	@Override
	public List<Complaints> getCompByType(Integer type) {

		return assessDao.getCompByType(type);
	}

	@Override
	public boolean alterTypeOfAssess(Integer compId, Integer type, Integer initType) {
		// 首先判断该投诉是否还为初始状态，即在dao中根据id查出投诉信息，在service层中判断
		Complaints comp = assessDao.getCompById(compId);
		if (comp.getType() != initType) {
			return false;
		} else {
			// 修改状态
			assessDao.updateTypeForComp(compId, type);
			// System.out.println("正在修改状态");
			logger.info("修改投诉状态" + "对应id" + compId + "状态改为" + type);
			return true;
		}
	}

	@Override
	public boolean addDeduct(Deduct deduct) {
		//分数处理
		deduct.setState(0);
		//Integer id=assessDao.addDeduct(deduct);
		//获取投诉对象，指定扣分的对象。
		Complaints complaint = assessDao.getCompById(deduct.getCompId());
		complaint.setDeduct(deduct);
		//删除多余的当前无效的扣分对象。
		//System.out.println("删除处理方案,对应投诉id："+deduct.getCompId());
		//assessDao.deleteDeduct(deduct.getCompId());
		//将二者关联起来
		 System.out.println("关联处理方案");
		assessDao.connectComp(complaint);
		return true;
	}

	@Override
	public boolean approvalComp(Integer compId, Integer state, String apprOpinion) {
		boolean result = true;
		if (state == 4) {
			// 审批通过,首先修改投诉信息状态，然后进行扣分,扣分即把对应的扣分记录的state修改为1
			if (alterTypeOfAssess(compId, 4, 3)) {
				//System.out.println("开始进行扣分");
				logger.info("开始进行扣分");
				assessDao.updateDeduct(compId);
				logger.info("投诉审批通过，公司id" + compId + "已扣1分");
			} else {
				result = false;
			}
		} else {
			// 审批未通过
			if (alterTypeOfAssess(compId, 5, 3)) {
				Complaints c = assessDao.getCompById(compId);
				c.setApprOpinion(apprOpinion);
				assessDao.updateComplaint(c);
				logger.info("投诉审批取消：公司id" + compId + "操作还原");
			} else {
				result = false;
			}
		}
		return result;
	}

	@Override
	public void addOrUpdateCompType(Comptype compType) {
		assessDao.addOrUpdateCompType(compType);
		logger.info("更新或新增评价类型" + compType.toString());
	}

	@Override
	public boolean deleteCompType(Integer id) {
		return assessDao.deleteCompType(id);
	}

	@Override
	public List<Asscycle> getAsscycle() {
		return assessDao.getAsscycle();
	}

	@Override
	public void updateAssCycle(Asscycle asscycle) {
		assessDao.updateAsscycle(asscycle);
		logger.info("更新考核信息" + asscycle.toString());

	}

	@Override
	public List<Scorerank> getScorerank() {
		return assessDao.getScorerank();
	}

	@Override
	public void addOrUpScorerank(Scorerank scorerank) {
		assessDao.addOrUpdateScorerank(scorerank);
		logger.info("新增分数等级对应记录" + scorerank.toString());

	}

	@Override
	public void deleteScorerank(Integer id) {
		assessDao.deleteScorerank(id);
		logger.info("删除分数等级对应记录的id" + id);

	}

	@Override
	public List<Evalscore> getEvalscore() {
		return assessDao.getEvalscore();
	}

	@Override
	public void addOrUpdateEval(Evalscore evalscore) {
		assessDao.addOrUpdateEval(evalscore);
		logger.info("新增评价分数对应信息" + evalscore.toString());

	}

	@Override
	public void delectEval(Integer id) {
		assessDao.deleteEvalScore(id);
		logger.info("删除评价分数对应信息（id为）" + id);

	}

	@Override
	public void deleteAsscycleById(Integer id) {
		assessDao.deleteAsscycleById(id);
	}

	private List<EvalVector> getAssessByDate(String date) {
		String dbname = FarmConstant.Eval + date;
		List<EvalVector> evals = new ArrayList<>();
		// 得到上个月的结束时间和这个月的结束时间
		Date lastDate = dateUtil.getLastDate(date, 1);
		Date currDate = dateUtil.getLastDate(date, 0);

		// 首先查出所有司机的信息
		List<Driver> driver = taxiDao.getAllDriver();
		logger.info("【当前考核】司机信息："+driver);
		// 根据传入的年月，查出当月的所有评价信息
		List<Evaluate> evaluates = evalDao.getEvalByDate(dbname);
		logger.info("【当前考核】评价信息："+evaluates);
		// 查出评价分数对应表和分数等级对应表
		List<Evalscore> evalscores = assessDao.getEvalscore();
		List<Scorerank> scoreranks = assessDao.getScorerank();
		logger.info("【当前考核】分数对应等级："+evalscores);
		// 查出该月内的投诉处理信息
		List<Deduct> deducts = assessDao.getDeductByDate(currDate, lastDate);
		logger.info("【当前考核】投诉信息："+deducts);
		// 查询考核周期信息表，得到基础分数
		Asscycle asscycle = assessDao.getAsscycleByName("月度考核");
		if (assessDao.getAsscycleByName("月度考核") == null) {
			asscycle = new Asscycle(100, 100, 100, 60, 60, 60, "默认考核", 100, 100, 100);
		}
		logger.info("【当前考核】初始分数："+asscycle.toString());
		
		String[] quaNums = assessDao.getAllQuaNums();
		// 添加好评率及评价得分
		for (Evaluate evaluate : evaluates) {
			if(evaluate.getQuaNum()==null||evaluate.getQuaNum()==""){
				continue;
			}
			for(int i = 0; i < quaNums.length; i++) {
				System.out.println("Service比较资格证号"+evaluate.getQuaNum()+"="+quaNums[i]);
				if(evaluate.getQuaNum().equals(quaNums[i])) {
					EvalVector eval = new EvalVector();
					// 得到好评率
					int goodRate = (evaluate.getGood() + evaluate.getUnvalued()) * 100 / evaluate.getTotal();
					eval.setQueNum(evaluate.getQuaNum());
					eval.setGoodRate(goodRate);
					// 根据好评率，得出评价分数
					for (Evalscore evalscore : evalscores) {
						if (goodRate >= evalscore.getLow() && goodRate <= evalscore.getTop()) {
							eval.setAssScore(evalscore.getScore());
							break;
						}
					}
					evals.add(eval);
					logger.info("【当前考核】考核结果："+evals);
					continue;
				}
			}		
		}
		// 完善司机信息
		for (EvalVector e : evals) {
			for (Driver d : driver) {
				//System.out.println("e:" + e.getQueNum());
				logger.info("e:" + e.getQueNum());
				//System.out.println("d:" + d.getQuaNum());
				logger.info("d:" + d.getQuaNum());
				if (e.getQueNum().equals(d.getQuaNum())) {
					
					if (d.getTaxi() != null) {
						e.setTaxiNum(d.getTaxi().getTaxiNum());
					}
					e.setDname(d.getDname());
					e.setSex(d.getSex());
					e.setDriNum(d.getDriNum());
					
					if (d.getCompany() != null) {
						e.setCompany(d.getCompany().getName());
					}
					
					e.setPhone(d.getPhone());
					e.setCurrAssessName("月度考核");
					e.setDriId(d.getId());
					driver.remove(d);
					break;
				}
			}
			logger.info("【当前考核】完善司机信息："+e.toString());
		}
		// 计算并添加投诉得分
		for (Deduct deduct : deducts) {
			for (EvalVector e : evals) {
				if (deduct.getDriverId() == e.getDriId()) {
					e.setComplaintScore(e.getComplaintScore() + deduct.getScorer());
					break;
				}
			}
		}

		// 最后计算总分数并得出对应等级(已关联初始分数)
		for (EvalVector e : evals) {
			e.setCurrInitScore(asscycle.getInitScorer());
			int n = asscycle.getInitScorer() + e.getAssScore() + e.getComplaintScore();
			if (asscycle.getToper() != null && n > asscycle.getToper()) {
				n = asscycle.getToper();
			}
			if (asscycle.getLower() != null && n < asscycle.getLower()) {
				n = asscycle.getLower();
			}
			e.setCurrScore(n);
			for (Scorerank scorerank : scoreranks) {
				if (scorerank.getLow() != null && n >= scorerank.getLow() && scorerank.getTop() != null
						&& n <= scorerank.getTop()) {
					e.setCurrRank(scorerank.getRank());
				}
			}
		}
		return evals;
	}

	@Override
	public List<EvalVector> queryCurrAssess() {
		// 首先获取当前月份的记录
		String date = dateUtil.getNewDate();
		List<EvalVector> evals = getAssessByDate(date);
		// 查询数据库中是否有上个月的记录
		int date2 = Integer.parseInt(date);
		if (date2 % 10 == 1) {
			date2 -= 89;
		} else {
			date2 -= 1;
		}
		String lastTime = String.valueOf(date2);
		List<Assarch> assars = assessDao.getAssarsByDate(lastTime);
		// 对集合长度进行判断，若没有则查询并插入
		if (assars.size() != 0) {
			// 当数据库存储有上月数据时
			for (EvalVector eval : evals) {
				for (Assarch assarch : assars) {
					if (eval.getDriId() == assarch.getDriver().getId()) {
						eval.setLastInitScore(assarch.getInitScore());
						eval.setLastScore(assarch.getCurrScore());
						eval.setLastRank(assarch.getCurrRank());
						assars.remove(assarch);
						break;
					}
				}
			}
		} else {
			// 当数据库中不含有上月数据时
			List<EvalVector> lastEvals = getAssessByDate(lastTime);
			List<Assarch> asses = new ArrayList<>();
			if (lastEvals.size() != 0) {
				// 进行合并并存储
				for (EvalVector eval : evals) {
					for (EvalVector lastEval : lastEvals) {
						eval.setLastInitScore(lastEval.getCurrInitScore());
						eval.setLastScore(lastEval.getCurrScore());
						eval.setCurrRank(lastEval.getCurrRank());
						// 添加存储
						Assarch ass = new Assarch();
						ass.setArchDate(new Date());
						ass.setAssDate(lastTime);
						ass.setCurrRank(lastEval.getCurrRank());
						ass.setCurrScore(lastEval.getCurrScore());
						ass.setInitScore(lastEval.getCurrInitScore());
						Driver d = new Driver();
						d.setId(lastEval.getDriId());
						ass.setDriver(d);
						asses.add(ass);
						// 在集合中删除已对应的信息
						lastEvals.remove(lastEval);
						break;
					}
				}
				// 向数据库保存上个月的信息
				assessDao.addAssarch(asses);
				logger.info("保存上月考核记录" + asses);
			}
		}

		return evals;                
	}

	@Override
	public boolean deleteComById(Integer id) {
		logger.info("删除未审核的投诉记录");
		return assessDao.daleteComById(id);
	}

}
