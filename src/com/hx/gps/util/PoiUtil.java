package com.hx.gps.util;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.hx.gps.entities.tool.Alarm;
import com.hx.gps.entities.tool.ComInfo2;
import com.hx.gps.entities.tool.Gps;
import com.hx.gps.entities.tool.Meter;
import com.hx.gps.entities.tool.MyPassVol;
import com.hx.gps.entities.tool.OrderVector;
import com.hx.gps.entities.tool.Point;
import com.hx.gps.entities.tool.ReveInfo;
import com.hx.gps.entities.tool.TaxiInfo2;

public class PoiUtil {

	/**
	 * 传入数据导出excel表格，数据多时分为多个sheet 表头需要手动输入！这个可以从页面传上来表头信息！
	 * 根据泛型将实体类所有的！所有的！所有的！属性对应的值，存到相应的列---有时候不需要所有的属性，还不如手工输入。
	 * 
	 * @param lists
	 */
	public static void getSensorExcel(List<OrderVector> lists, String startDate, String endDate,
			HttpServletResponse response) throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		int sheetCount = FarmConstant.MAXROWS; // 每个sheet中的最大行数
		// 截取年月日
		String startTime = startDate.substring(0, 10);
		String endTime = endDate.substring(0, 10);
		String headerStr = "从" + startTime + "至" + endTime + "的命令日志"; // Excel的名称
																		// "车号"+"期间"+"传感器or计价器"
		// 车牌号、终端号、添加时间、命令序号、命令类型、标识、发送人、内容、状态、发送次数
		String[] tableHeader = { "车牌号", "终端号", "添加时间", "命令序号", "命令类型", "标识", "发送人", "内容", "状态", "发送次数" }; // 手动设置sheet表头信息
		int cellNumber = tableHeader.length; // 表的总列数

		HSSFWorkbook workbook = new HSSFWorkbook(); // Excel的工作簿
		HSSFSheet sheet = null; // Excel的sheet

		/**
		 * 设置表头和表体
		 */
		int p = 1; // 当前sheet的当前行
		int w = 1; // 用来区别当前sheet的名字
		sheet = workbook.createSheet("sheet" + w);// 创建第一个sheet
		PoiUtil.initSheetHeader(workbook, sheet, tableHeader);// 创建并格式化表头

		// 创建多个sheet，并放入数据
		for (OrderVector s : lists) {
			if (p <= sheetCount) {
				PoiUtil.setCellValue(p, cellNumber, sheet, s);// 设置当前行的每一列的数据
				p++;
			} else {
				w++;
				sheet = workbook.createSheet("sheet" + w);// 创建新的sheet
				// 创建并格式化表头
				PoiUtil.initSheetHeader(workbook, sheet, tableHeader);
				// 把当前s加入到新的sheet第一行
				PoiUtil.setCellValue(p = 1, cellNumber, sheet, s);
				p = 2;
			}
		}

		/**
		 * 输出xls表
		 */
		// HttpServletResponse response = null;//创建一个HttpServletResponse对象
		OutputStream out = null;// 创建一个输出流对象
		try {
			// response =
			// ServletActionContext.getResponse();//初始化HttpServletResponse对象
			out = response.getOutputStream();
			headerStr = new String(headerStr.getBytes("gb2312"), "ISO8859-1");// headerString为中文时转码
			response.setHeader("Content-disposition", "attachment; filename=" + headerStr + ".xls");// filename是下载的xls的名，建议最好用英文
			response.setContentType("application/msexcel;charset=UTF-8");// 设置类型
			response.setHeader("Pragma", "No-cache");// 设置头
			response.setHeader("Cache-Control", "no-cache");// 设置头
			response.setDateHeader("Expires", 0);// 设置日期头
			workbook.write(out);
			out.flush();
			workbook.write(out);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void initSheetHeader(HSSFWorkbook workbook, HSSFSheet sheet, String[] tableHeader) {
		HSSFCell cell = null; // Excel的列
		HSSFRow row = null; // Excel的行
		HSSFFont font = workbook.createFont(); // 设置字体
		HSSFCellStyle style = workbook.createCellStyle(); // 设置表头的类型
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		HSSFCellStyle style1 = workbook.createCellStyle(); // 设置数据类型
		style1.setAlignment(HSSFCellStyle.ALIGN_CENTER);

		int cellNumber = tableHeader.length;
		row = sheet.createRow(0);
		row.setHeight((short) 400);
		for (int k = 0; k < cellNumber; k++) {
			cell = row.createCell(k);// 创建第0行第k列
			cell.setCellValue(tableHeader[k]);// 设置第0行第k列的值，把表头信息加进表里
			sheet.setColumnWidth(k, 8000);// 设置列的宽度
			font.setColor(HSSFFont.COLOR_NORMAL); // 设置单元格字体的颜色.
			font.setFontHeight((short) 350); // 设置单元字体高度
			style1.setFont(font);// 设置字体风格
			cell.setCellStyle(style1);
		}
	}

	/**
	 * 手动输入每列的数据
	 */
	private static void setCellValue(int p, int cellNumber, HSSFSheet sheet, OrderVector s)
			throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {
		HSSFRow row = null;// Excel的行
		HSSFCell cell = null;// Excel的列
		row = sheet.createRow(p);
		row.setHeight((short) 400);// 设置行高
		for (int j = 0; j < cellNumber; j++) {

			cell = row.createCell(j);
			cell.setCellValue(false);
			// "开始时间","结束时间","脉冲","K值","公里数"
			// 车牌号、终端号、添加时间、命令序号、命令类型、标识、发送人、内容、状态、发送次数
			if (j == 0) {
				cell.setCellValue(s.getTaxiNum());
			} else if (j == 1) {
				cell.setCellValue(s.getIsuNum());
			} else if (j == 2) {
				cell.setCellValue(s.getTime());
			} else if (j == 3) {
				cell.setCellValue(s.getSerialNum());
			} else if (j == 4) {
				cell.setCellValue(s.getOrderType());
			} else if (j == 5) {
				cell.setCellValue(s.getIdentify());
			} else if (j == 6) {
				cell.setCellValue(s.getPeople());
			} else if (j == 7) {
				cell.setCellValue(s.getContext());
			} else if (j == 8) {
				cell.setCellValue(s.getState());
			} else if (j == 8) {
				cell.setCellValue(s.getSendTime());
			}
		}
	}

	/***
	 * 报警日志表导出表格
	 * 
	 */
	public static void getExcelOfAlarm(List<Alarm> lists, String startDate, String endDate,
			HttpServletResponse response) throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		int sheetCount = FarmConstant.MAXROWS; // 每个sheet中的最大行数
		// 截取年月日
		String startTime = startDate.substring(0, 10);
		String endTime = endDate.substring(0, 10);
		String headerStr = "从" + startTime + "至" + endTime + "的命令日志"; // Excel的名称
																		// "车号"+"期间"+"传感器or计价器"
		// 车牌号、终端号、报警类型、报警时间、处理状态、载客状态
		String[] tableHeader = { "车牌号", "终端号", "报警类型", "报警时间", "处理状态", "载客状态" }; // 手动设置sheet表头信息
		int cellNumber = tableHeader.length; // 表的总列数

		HSSFWorkbook workbook = new HSSFWorkbook(); // Excel的工作簿
		HSSFSheet sheet = null; // Excel的sheet

		/**
		 * 设置表头和表体
		 */
		int p = 1; // 当前sheet的当前行
		int w = 1; // 用来区别当前sheet的名字
		sheet = workbook.createSheet("sheet" + w);// 创建第一个sheet
		PoiUtil.initSheetHeaderOfAlarm(workbook, sheet, tableHeader);// 创建并格式化表头

		// 创建多个sheet，并放入数据
		for (Alarm s : lists) {
			if (p <= sheetCount) {
				PoiUtil.setCellValueOfAlarm(p, cellNumber, sheet, s);// 设置当前行的每一列的数据
				p++;
			} else {
				w++;
				sheet = workbook.createSheet("sheet" + w);// 创建新的sheet
				// 创建并格式化表头
				PoiUtil.initSheetHeaderOfAlarm(workbook, sheet, tableHeader);
				// 把当前s加入到新的sheet第一行
				PoiUtil.setCellValueOfAlarm(p = 1, cellNumber, sheet, s);
				p = 2;
			}
		}

		/**
		 * 输出xls表
		 */
		// HttpServletResponse response = null;//创建一个HttpServletResponse对象
		OutputStream out = null;// 创建一个输出流对象
		try {
			// response =
			// ServletActionContext.getResponse();//初始化HttpServletResponse对象
			out = response.getOutputStream();
			headerStr = new String(headerStr.getBytes("gb2312"), "ISO8859-1");// headerString为中文时转码
			response.setHeader("Content-disposition", "attachment; filename=" + headerStr + ".xls");// filename是下载的xls的名，建议最好用英文
			response.setContentType("application/msexcel;charset=UTF-8");// 设置类型
			response.setHeader("Pragma", "No-cache");// 设置头
			response.setHeader("Cache-Control", "no-cache");// 设置头
			response.setDateHeader("Expires", 0);// 设置日期头
			workbook.write(out);
			out.flush();
			workbook.write(out);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void initSheetHeaderOfAlarm(HSSFWorkbook workbook, HSSFSheet sheet, String[] tableHeader) {
		HSSFCell cell = null; // Excel的列
		HSSFRow row = null; // Excel的行
		HSSFFont font = workbook.createFont(); // 设置字体
		HSSFCellStyle style = workbook.createCellStyle(); // 设置表头的类型
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		HSSFCellStyle style1 = workbook.createCellStyle(); // 设置数据类型
		style1.setAlignment(HSSFCellStyle.ALIGN_CENTER);

		int cellNumber = tableHeader.length;
		row = sheet.createRow(0);
		row.setHeight((short) 400);
		for (int k = 0; k < cellNumber; k++) {
			cell = row.createCell(k);// 创建第0行第k列
			cell.setCellValue(tableHeader[k]);// 设置第0行第k列的值，把表头信息加进表里
			sheet.setColumnWidth(k, 8000);// 设置列的宽度
			font.setColor(HSSFFont.COLOR_NORMAL); // 设置单元格字体的颜色.
			font.setFontHeight((short) 350); // 设置单元字体高度
			style1.setFont(font);// 设置字体风格
			cell.setCellStyle(style1);
		}
	}

	// 手动输入每列的数据
	private static void setCellValueOfAlarm(int p, int cellNumber, HSSFSheet sheet, Alarm s)
			throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {
		HSSFRow row = null;// Excel的行
		HSSFCell cell = null;// Excel的列
		row = sheet.createRow(p);
		row.setHeight((short) 400);// 设置行高
		for (int j = 0; j < cellNumber; j++) {

			cell = row.createCell(j);
			cell.setCellValue(false);

			// 车牌号、终端号、报警类型、报警时间、处理状态、载客状态
			if (j == 0) {
				cell.setCellValue(s.getTaxiNum());
			} else if (j == 1) {
				cell.setCellValue(s.getIsuNum());
			} else if (j == 2) {
				cell.setCellValue(s.getAlarmType());
			} else if (j == 3) {
				cell.setCellValue(s.getTime());
			} else if (j == 4) {
				cell.setCellValue(s.getDealState());
			} else if (j == 5) {
				cell.setCellValue(s.getPgsstate());
			}
		}
	}

	/***
	 * 导出未处理的报警信息
	 * 
	 */
	public static void getExcelOfDealAlarm(List<Alarm> lists, HttpServletResponse response)
			throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {
		int sheetCount = FarmConstant.MAXROWS; // 每个sheet中的最大行数
		String time = DateUtil.dateToString(new Date()).substring(0, 10);
		String headerStr = time + "未处理的命令日志"; // Excel的名称 "车号"+"期间"+"传感器or计价器"
		// 车牌号、终端号、报警类型、报警时间、处理状态、载客状态
		String[] tableHeader = { "车牌号", "终端号", "报警类型", "报警时间", "处理状态", "载客状态" }; // 手动设置sheet表头信息
		int cellNumber = tableHeader.length; // 表的总列数

		HSSFWorkbook workbook = new HSSFWorkbook(); // Excel的工作簿
		HSSFSheet sheet = null; // Excel的sheet

		/**
		 * 设置表头和表体
		 */
		int p = 1; // 当前sheet的当前行
		int w = 1; // 用来区别当前sheet的名字
		sheet = workbook.createSheet("sheet" + w);// 创建第一个sheet
		PoiUtil.initSheetHeaderOfDeal(workbook, sheet, tableHeader);// 创建并格式化表头

		// 创建多个sheet，并放入数据
		for (Alarm s : lists) {
			if (p <= sheetCount) {
				PoiUtil.setCellValueOfDeal(p, cellNumber, sheet, s);// 设置当前行的每一列的数据
				p++;
			} else {
				w++;
				sheet = workbook.createSheet("sheet" + w);// 创建新的sheet
				// 创建并格式化表头
				PoiUtil.initSheetHeaderOfDeal(workbook, sheet, tableHeader);
				// 把当前s加入到新的sheet第一行
				PoiUtil.setCellValueOfDeal(p = 1, cellNumber, sheet, s);
				p = 2;
			}
		}

		/**
		 * 输出xls表
		 */
		// HttpServletResponse response = null;//创建一个HttpServletResponse对象
		OutputStream out = null;// 创建一个输出流对象
		try {
			// response =
			// ServletActionContext.getResponse();//初始化HttpServletResponse对象
			out = response.getOutputStream();
			headerStr = new String(headerStr.getBytes("gb2312"), "ISO8859-1");// headerString为中文时转码
			response.setHeader("Content-disposition", "attachment; filename=" + headerStr + ".xls");// filename是下载的xls的名，建议最好用英文
			response.setContentType("application/msexcel;charset=UTF-8");// 设置类型
			response.setHeader("Pragma", "No-cache");// 设置头
			response.setHeader("Cache-Control", "no-cache");// 设置头
			response.setDateHeader("Expires", 0);// 设置日期头
			workbook.write(out);
			out.flush();
			workbook.write(out);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void initSheetHeaderOfDeal(HSSFWorkbook workbook, HSSFSheet sheet, String[] tableHeader) {
		HSSFCell cell = null; // Excel的列
		HSSFRow row = null; // Excel的行
		HSSFFont font = workbook.createFont(); // 设置字体
		HSSFCellStyle style = workbook.createCellStyle(); // 设置表头的类型
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		HSSFCellStyle style1 = workbook.createCellStyle(); // 设置数据类型
		style1.setAlignment(HSSFCellStyle.ALIGN_CENTER);

		int cellNumber = tableHeader.length;
		row = sheet.createRow(0);
		row.setHeight((short) 400);
		for (int k = 0; k < cellNumber; k++) {
			cell = row.createCell(k);// 创建第0行第k列
			cell.setCellValue(tableHeader[k]);// 设置第0行第k列的值，把表头信息加进表里
			sheet.setColumnWidth(k, 8000);// 设置列的宽度
			font.setColor(HSSFFont.COLOR_NORMAL); // 设置单元格字体的颜色.
			font.setFontHeight((short) 350); // 设置单元字体高度
			style1.setFont(font);// 设置字体风格
			cell.setCellStyle(style1);
		}
	}

	// 手动输入每列的数据
	private static void setCellValueOfDeal(int p, int cellNumber, HSSFSheet sheet, Alarm s)
			throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {
		HSSFRow row = null;// Excel的行
		HSSFCell cell = null;// Excel的列
		row = sheet.createRow(p);
		row.setHeight((short) 400);// 设置行高
		for (int j = 0; j < cellNumber; j++) {

			cell = row.createCell(j);
			cell.setCellValue(false);

			// 车牌号、终端号、报警类型、报警时间、处理状态、载客状态
			if (j == 0) {
				cell.setCellValue(s.getTaxiNum());
			} else if (j == 1) {
				cell.setCellValue(s.getIsuNum());
			} else if (j == 2) {
				cell.setCellValue(s.getAlarmType());
			} else if (j == 3) {
				cell.setCellValue(s.getTime());
			} else if (j == 4) {
				cell.setCellValue(s.getDealState());
			} else if (j == 5) {
				cell.setCellValue(s.getPgsstate());
			}
		}
	}

	/**
	 * 手动输入营运数据
	 * 
	 * 
	 */
	private static void setCellValueOfMeter(int p, int cellNumber, HSSFSheet sheet, Meter s)
			throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {
		HSSFRow row = null;// Excel的行
		HSSFCell cell = null;// Excel的列
		row = sheet.createRow(p);
		row.setHeight((short) 400);// 设置行高
		for (int j = 0; j < cellNumber; j++) {

			cell = row.createCell(j);
			cell.setCellValue(false);
			DecimalFormat df = new DecimalFormat("#.0");
			// 车牌号，终端号,资格证号,上车时间,下车时间,空驶里程,该客次计程,等待时间,单价,租金,合租合乘
			if (j == 0) {
				cell.setCellValue(s.getTaxiNum());
			} else if (j == 1) {
				cell.setCellValue(s.getIsuNum());
			} else if (j == 2) {
				cell.setCellValue(s.getQuaNum());
			} else if (j == 3) {
				cell.setCellValue(s.getUpTime());
			} else if (j == 4) {
				cell.setCellValue(s.getDownTime());
			} else if (j == 5) {
				cell.setCellValue(df.format(s.getEmptymil()));
			} else if (j == 6) {
				cell.setCellValue(df.format(s.getMileage()));
			} else if (j == 7) {
				cell.setCellValue(s.getWaitTime());
			} else if (j == 8) {
				cell.setCellValue(df.format(s.getPrice()));
			} else if (j == 9) {
				cell.setCellValue(df.format(s.getMoney()));
			} else if (j == 10) {
				cell.setCellValue(s.getEvaluate());
			}
		}
	}

	public static void getExcelOfMeter(List<Meter> meters, HttpServletResponse response, String startDate,
			String endDate) throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		int sheetCount = FarmConstant.MAXROWS; // 每个sheet中的最大行数
		// 截取年月日
		String startTime = startDate.substring(0, 10);
		String endTime = endDate.substring(0, 10);
		String headerStr = "从" + startTime + "至" + endTime + "的营运数据"; // Excel的名称
																		// "车号"+"期间"+"传感器or计价器"
		// 车牌号、终端号、报警类型、报警时间、处理状态、载客状态
		String[] tableHeader = { "车牌号", "终端号", "资格证号", "上车时间", "下车时间", "空驶里程", "该客次计程", "等待时间", "单价", "租金", "是否合租合乘" }; // 手动设置sheet表头信息
		int cellNumber = tableHeader.length; // 表的总列数

		HSSFWorkbook workbook = new HSSFWorkbook(); // Excel的工作簿
		HSSFSheet sheet = null; // Excel的sheet

		/**
		 * 设置表头和表体
		 */
		int p = 1; // 当前sheet的当前行
		int w = 1; // 用来区别当前sheet的名字
		sheet = workbook.createSheet("sheet" + w);// 创建第一个sheet
		PoiUtil.initSheetHeaderOfAlarm(workbook, sheet, tableHeader);// 创建并格式化表头

		// 创建多个sheet，并放入数据
		for (Meter s : meters) {
			if (p <= sheetCount) {
				PoiUtil.setCellValueOfMeter(p, cellNumber, sheet, s);// 设置当前行的每一列的数据
				p++;
			} else {
				w++;
				sheet = workbook.createSheet("sheet" + w);// 创建新的sheet
				// 创建并格式化表头
				PoiUtil.initSheetHeaderOfAlarm(workbook, sheet, tableHeader);
				// 把当前s加入到新的sheet第一行
				PoiUtil.setCellValueOfMeter(p = 1, cellNumber, sheet, s);
				p = 2;
			}
		}

		/**
		 * 输出xls表
		 */
		// HttpServletResponse response = null;//创建一个HttpServletResponse对象
		OutputStream out = null;// 创建一个输出流对象
		try {
			// response =
			// ServletActionContext.getResponse();//初始化HttpServletResponse对象
			out = response.getOutputStream();
			headerStr = new String(headerStr.getBytes("gb2312"), "ISO8859-1");// headerString为中文时转码
			response.setHeader("Content-disposition", "attachment; filename=" + headerStr + ".xls");// filename是下载的xls的名，建议最好用英文
			response.setContentType("application/msexcel;charset=UTF-8");// 设置类型
			response.setHeader("Pragma", "No-cache");// 设置头
			response.setHeader("Cache-Control", "no-cache");// 设置头
			response.setDateHeader("Expires", 0);// 设置日期头
			workbook.write(out);
			out.flush();
			workbook.write(out);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void getExcelOfRevenue(List<ComInfo2> infos, HttpServletResponse response, String startDate,
			String endDate) throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		int sheetCount = FarmConstant.MAXROWS; // 每个sheet中的最大行数
		// 截取年月日
		String startTime = startDate.substring(0, 10);
		String endTime = endDate.substring(0, 10);
		String headerStr = "从" + startTime + "至" + endTime + "的营收报表数据"; // Excel的名称
		// "车号"+"期间"+"传感器or计价器"
		// 公司名称 车牌号、对应日期，年月日、 营收次数、空驶里程 载客里程 租金 合租次数

		String[] tableHeader = { "公司名称", "车牌号", "日期", "营收次数", "空驶里程", "载客里程", "租金", "租金", "合租次数" }; // 手动设置sheet表头信息
		int cellNumber = tableHeader.length; // 表的总列数

		HSSFWorkbook workbook = new HSSFWorkbook(); // Excel的工作簿
		HSSFSheet sheet = null; // Excel的sheet

		/**
		 * 设置表头和表体
		 */
		int p = 1; // 当前sheet的当前行
		int w = 1; // 用来区别当前sheet的名字
		sheet = workbook.createSheet("sheet" + w);// 创建第一个sheet
		PoiUtil.initSheetHeaderOfAlarm(workbook, sheet, tableHeader);// 创建并格式化表头

		// 创建多个sheet，并放入数据 : 公司 公司.taxis taxi。营运汇总。
		for (ComInfo2 com : infos) {
			// 首先遍历出公司
			for (TaxiInfo2 taxi : com.getTaxis()) {
				// 然后遍历出车辆
				// 然后遍历每辆车的营收数据，按时间分类
				for (ReveInfo reveInfo : taxi.getRevenues()) {
					// 将每辆车的营运数据汇总到表格中去
					if (p <= sheetCount) {
						PoiUtil.setCellValueOfRevenue(p, cellNumber, sheet, reveInfo);// 设置当前行的每一列的数据
						p++;
					} else {
						w++;
						// 创建新的sheet
						sheet = workbook.createSheet("sheet" + w);
						// 创建并格式化表头
						PoiUtil.initSheetHeaderOfAlarm(workbook, sheet, tableHeader);
						// 把当前s加入到新的sheet第一行
						PoiUtil.setCellValueOfRevenue(p, cellNumber, sheet, reveInfo);
						p = 2;
					}

				}

			}

		}

		/**
		 * 输出xls表
		 */
		// HttpServletResponse response = null;//创建一个HttpServletResponse对象
		OutputStream out = null;// 创建一个输出流对象
		try {
			// response =
			// ServletActionContext.getResponse();//初始化HttpServletResponse对象
			out = response.getOutputStream();
			headerStr = new String(headerStr.getBytes("gb2312"), "ISO8859-1");// headerString为中文时转码
			response.setHeader("Content-disposition", "attachment; filename=" + headerStr + ".xls");// filename是下载的xls的名，建议最好用英文
			response.setContentType("application/msexcel;charset=UTF-8");// 设置类型
			response.setHeader("Pragma", "No-cache");// 设置头
			response.setHeader("Cache-Control", "no-cache");// 设置头
			response.setDateHeader("Expires", 0);// 设置日期头
			workbook.write(out);
			out.flush();
			workbook.write(out);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void setCellValueOfRevenue(int p, int cellNumber, HSSFSheet sheet, ReveInfo reveInfo)
			throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {
		HSSFRow row = null;// Excel的行
		HSSFCell cell = null;// Excel的列
		row = sheet.createRow(p);
		row.setHeight((short) 400);// 设置行高
		for (int j = 0; j < cellNumber; j++) {

			cell = row.createCell(j);
			cell.setCellValue(false);

			// "公司名称", "车牌号", "日期","营收次数", "空驶里程" ,"载客里程","租金","合租次数"
			if (j == 0) {
				cell.setCellValue(reveInfo.getCompany());
			} else if (j == 1) {
				cell.setCellValue(reveInfo.getTaxiNum());
			} else if (j == 2) {
				cell.setCellValue(reveInfo.getDate());
			} else if (j == 3) {
				cell.setCellValue(reveInfo.getNumber());
			} else if (j == 4) {
				cell.setCellValue(reveInfo.getEmptyMile());
			} else if (j == 5) {
				cell.setCellValue(reveInfo.getMileUtil());
			} else if (j == 6) {
				cell.setCellValue(reveInfo.getMoney());
			} else if (j == 7) {
				cell.setCellValue(reveInfo.getRoommate());
			}
		}
	}

	public static void getExcelOfKeYun(List<MyPassVol> passVols, HttpServletResponse response, String type,
			String startDate, String endDate) throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		int sheetCount = FarmConstant.MAXROWS; // 每个sheet中的最大行数
		// 截取年月日
		String startTime = startDate.substring(0, 10);
		String endTime = endDate.substring(0, 10);
		String headerStr = " ";
		if (type.equalsIgnoreCase("day")) {
			headerStr = "从" + startTime + "至" + endTime + "每天的客运数据"; // Excel的名称
		} else {
			headerStr = "从" + startTime + "至" + endTime + "每月的客运数据"; // Excel的名称
		}
		//
		// 日期，客运量

		String[] tableHeader = { "日期", "客运量" }; // 手动设置sheet表头信息
		int cellNumber = tableHeader.length; // 表的总列数

		HSSFWorkbook workbook = new HSSFWorkbook(); // Excel的工作簿
		HSSFSheet sheet = null; // Excel的sheet

		/**
		 * 设置表头和表体
		 */
		int p = 1; // 当前sheet的当前行
		int w = 1; // 用来区别当前sheet的名字
		sheet = workbook.createSheet("sheet" + w);// 创建第一个sheet
		PoiUtil.initSheetHeaderOfAlarm(workbook, sheet, tableHeader);// 创建并格式化表头

		// 创建多个sheet，并放入数据 : 公司 公司.taxis taxi。营运汇总。
		for (MyPassVol passVol : passVols) {
			// 将每天或者每月的客运总量存到表格中。
			if (p <= sheetCount) {
				PoiUtil.setCellValueOfKeYun(p, cellNumber, sheet, passVol);// 设置当前行的每一列的数据
				p++;
			} else {
				w++;
				// 创建新的sheet
				sheet = workbook.createSheet("sheet" + w);
				// 创建并格式化表头
				PoiUtil.initSheetHeaderOfAlarm(workbook, sheet, tableHeader);
				// 把当前s加入到新的sheet第一行
				PoiUtil.setCellValueOfKeYun(p, cellNumber, sheet, passVol);
				p = 2;
			}

		}

		/**
		 * 输出xls表
		 */
		// HttpServletResponse response = null;//创建一个HttpServletResponse对象
		OutputStream out = null;// 创建一个输出流对象
		try {
			// response =
			// ServletActionContext.getResponse();//初始化HttpServletResponse对象
			out = response.getOutputStream();
			headerStr = new String(headerStr.getBytes("gb2312"), "ISO8859-1");// headerString为中文时转码
			response.setHeader("Content-disposition", "attachment; filename=" + headerStr + ".xls");// filename是下载的xls的名，建议最好用英文
			response.setContentType("application/msexcel;charset=UTF-8");// 设置类型
			response.setHeader("Pragma", "No-cache");// 设置头
			response.setHeader("Cache-Control", "no-cache");// 设置头
			response.setDateHeader("Expires", 0);// 设置日期头
			workbook.write(out);
			out.flush();
			workbook.write(out);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * 
	 * 手动输入客运数据
	 * 
	 */
	private static void setCellValueOfKeYun(int p, int cellNumber, HSSFSheet sheet, MyPassVol passVol)
			throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {
		HSSFRow row = null;// Excel的行
		HSSFCell cell = null;// Excel的列
		row = sheet.createRow(p);
		row.setHeight((short) 400);// 设置行高
		for (int j = 0; j < cellNumber; j++) {
			cell = row.createCell(j);
			cell.setCellValue(false);

			// 日期，客运量
			if (j == 0) {
				cell.setCellValue(passVol.getDate());
			} else if (j == 1) {
				cell.setCellValue(passVol.getPassVol());
			}
		}

	}

	/**
	 * 生成导出附件中文名。应对导出文件中文乱码
	 * <p>
	 * response.addHeader("Content-Disposition", "attachment; filename=" +
	 * cnName);
	 * 
	 * @param cnName
	 * @param defaultName
	 * @return
	 */
	public static String genAttachmentFileName(String cnName, String defaultName) {
		try {
			// fileName = URLEncoder.encode(fileName, "UTF-8");
			cnName = new String(cnName.getBytes("gb2312"), "ISO8859-1");
			/*
			 * if (fileName.length() > 150) { fileName = new String(
			 * fileName.getBytes("gb2312"), "ISO8859-1" ); }
			 */
		} catch (Exception e) {
			cnName = defaultName;
		}
		return cnName;

	}

	/**
	 * 输出坐标点
	 * 
	 * @throws JSONException
	 */
	public static void getExcelOfPoint(JSONArray exportPoint, HttpServletResponse response)
			throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, JSONException {
		int sheetCount = FarmConstant.MAXROWS; // 每个sheet中的最大行数
		String headerStr = "导出经纬度"; // Excel的名称
		// 车牌号、终端号、报警类型、报警时间、处理状态、载客状态
		String[] tableHeader = { "经度", "纬度" }; // 手动设置sheet表头信息
		int cellNumber = tableHeader.length; // 表的总列数

		HSSFWorkbook workbook = new HSSFWorkbook(); // Excel的工作簿
		HSSFSheet sheet = null; // Excel的sheet

		/**
		 * 设置表头和表体
		 */
		int p = 1; // 当前sheet的当前行
		int w = 1; // 用来区别当前sheet的名字
		sheet = workbook.createSheet("sheet" + w);// 创建第一个sheet
		PoiUtil.initSheetHeaderOfPoint(workbook, sheet, tableHeader);// 创建并格式化表头

		// 创建多个sheet，并放入数据
		for (int i = 0; i < exportPoint.length(); i++) {
			JSONObject s = exportPoint.getJSONObject(i);
			if (p <= sheetCount) {
				PoiUtil.setCellValueOfPoint(p, cellNumber, sheet, s);// 设置当前行的每一列的数据
				p++;
			} else {
				w++;
				sheet = workbook.createSheet("sheet" + w);// 创建新的sheet
				// 创建并格式化表头
				PoiUtil.initSheetHeaderOfPoint(workbook, sheet, tableHeader);
				// 把当前s加入到新的sheet第一行
				PoiUtil.setCellValueOfPoint(p = 1, cellNumber, sheet, s);
				p = 2;
			}
		}

		/**
		 * 输出xls表
		 */
		// HttpServletResponse response = null;//创建一个HttpServletResponse对象
		OutputStream out = null;// 创建一个输出流对象
		try {
			// response =
			// ServletActionContext.getResponse();//初始化HttpServletResponse对象
			out = response.getOutputStream();
			headerStr = new String(headerStr.getBytes("gb2312"), "ISO8859-1");// headerString为中文时转码
			response.setHeader("Content-disposition", "attachment; filename=" + headerStr + ".xls");// filename是下载的xls的名，建议最好用英文
			response.setContentType("application/msexcel;charset=UTF-8");// 设置类型
			response.setHeader("Pragma", "No-cache");// 设置头
			response.setHeader("Cache-Control", "no-cache");// 设置头
			response.setDateHeader("Expires", 0);// 设置日期头
			workbook.write(out);
			out.flush();
			workbook.write(out);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("输出表格");
	}

	private static void setCellValueOfPoint(int p, int cellNumber, HSSFSheet sheet, JSONObject s)
			throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, JSONException {
		HSSFRow row = null;// Excel的行
		HSSFCell cell = null;// Excel的列
		row = sheet.createRow(p);
		row.setHeight((short) 400);// 设置行高
		for (int j = 0; j < cellNumber; j++) {
			Gps gps = PositionUtil.bd09_To_Gps84(Double.parseDouble(s.get("latitude").toString()),
					Double.parseDouble(s.get("longitude").toString()));
			Point pt = new Point(gps.getWgLon(), gps.getWgLat());
			cell = row.createCell(j);
			cell.setCellValue(false);
			DecimalFormat df = new DecimalFormat("#.0");
			// 经度、纬度
			if (j == 0) {
				cell.setCellValue(pt.getLongitude());
			} else if (j == 1) {
				cell.setCellValue(pt.getLatitude());
			}
		}
		System.out.println("设置表格");
	}

	private static void initSheetHeaderOfPoint(HSSFWorkbook workbook, HSSFSheet sheet, String[] tableHeader) {
		HSSFCell cell = null; // Excel的列
		HSSFRow row = null; // Excel的行
		HSSFFont font = workbook.createFont(); // 设置字体
		HSSFCellStyle style = workbook.createCellStyle(); // 设置表头的类型
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		HSSFCellStyle style1 = workbook.createCellStyle(); // 设置数据类型
		style1.setAlignment(HSSFCellStyle.ALIGN_CENTER);

		int cellNumber = tableHeader.length;
		row = sheet.createRow(0);
		row.setHeight((short) 400);
		for (int k = 0; k < cellNumber; k++) {
			cell = row.createCell(k);// 创建第0行第k列
			cell.setCellValue(tableHeader[k]);// 设置第0行第k列的值，把表头信息加进表里
			sheet.setColumnWidth(k, 8000);// 设置列的宽度
			font.setColor(HSSFFont.COLOR_NORMAL); // 设置单元格字体的颜色.
			font.setFontHeight((short) 350); // 设置单元字体高度
			style1.setFont(font);// 设置字体风格
			cell.setCellStyle(style1);
		}
		System.out.println("初始化表格");
	}

}
