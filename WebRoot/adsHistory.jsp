<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<script src="js/jquery.js"></script>

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>

 
<link rel="stylesheet" href="css/myhx.css" />
</head>

<body class="gray-bg">
	<div class="wrapper animated ">
 <!--查询条件  -->
 <div class="row">
			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>输入条件查询发送记录</h5>
				</div>
				<div class="ibox-content">
					<form action="getHistoryByTime" class="form-horizontal" method="post">
						<div class="row">
							<div class="form-group">
							
								<label class="col-sm-1 control-label">起始时间：</label>
								<div class="col-sm-2">
									 <input type="text" class="form-control" style="margin-top: 7px;" name="startDate" id="startDate"
									value="${startDate}" class="Wdate  form-control"
									onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true})">
								</div>
								
								<label class="col-sm-1 control-label">结束时间：</label>
								<div class="col-sm-2">
							     <input type="text" class="form-control" style="margin-top: 7px;" name="endDate" id="endDate"
									 value="${endDate}" class="Wdate form-control "
									onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'startDate\')}',dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true})">
								</div>
						 
								<div class="col-sm-3">
									<button type="submit" id="selectAds" class="btn btn-primary">查询</button>
		 

								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- 数据展示 -->
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-title">
					<h4>历史发送记录</h4>
				</div>
			<!-- 	<div class="row">
				<input type="radio" id="Public" name="condition" onclick="show1()">
				 <input type="radio" id="normal" name="condition" onclick="show2()">
				<input type="radio" id="instruct" name="condition" onclick="show3()" >
				</div> -->
				<div class="ibox-content">
 

					<table id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
						<thead>
							<tr>
								<th>发送类型</th>
								<th>信息号</th>
								<th>发送时间</th>
								<th>操作人</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<!--遍历  -->
							<c:forEach items="${waitAdvertises}" var="history">
								<tr>
									<td>${history.type}</td><c:if test="${history.type eq '广告下发' }">
										<td><a style="color: dark" href="intoAdsSend?playId=${history.playId}">
												${history.playId}</a></td></c:if><c:if test="${history.type eq '公益广告' }">
										 <td><a  style="color: red" href="intoPubSend?playId=${history.playId}">
												${history.playId}</a></td></c:if><c:if test="${history.type eq '指令下发' }">
										<td><a style="color: blue"  href="intoInsSend?playId=${history.playId}">
												${history.playId}</a></td></c:if>
									<td>${history.endTime}</td>
									<td>${history.adsName}</td>
									<td><button id="btn-update" type="button"
											class="btn btn-success btn-sm"  onclick="findDetails3('${history.type}','${history.playId}','${history.endTime}','${history.adsName}')"  >查看详情</button>
		                  </td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="5">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
				 
			</div>
		</div>
		
		
		<div class="row" id="templist">
			<div class="col-sm-12" >
				<div class="float-e-margins">
					<div class="ibox-title">
						<h4>发送详情表</h4>
 
					</div>

					<div class="ibox-content" style="height:400px; overflow: auto;">
 
						<table id="table2"  width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped"  
							 >
							<thead>
								<tr>
									<th>车牌号</th>
									<th>终端号</th>
									<th>信息号</th>
									<th>下发类型</th>
									<th>处理状态</th>
									<th>失败次数</th>
									<th>结束时间</th>
									<th>操作人</th>
								</tr>
							</thead>
							<tbody id="tbody">
							 <c:forEach items="${details}" var="a">
							 <tr>
							<td>${a.taxiNum}</td>
							<td>${a.isuNum}</td>
							<td>${a.playId}</td>
							<td>${a.type}</td>
							<td>${a.state}</td>
							<td>${a.failNum}</td>
							<td>${a.endTime}</td>
							<td>${a.adsName}</td>
							</tr>
							</c:forEach> 
							</tbody>
							 <tfoot>
							<tr>
								<td colspan="8">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
						</table>
					</div>
				</div>
			</div>
		 <form action="findDetailsTime" method="post" id="find2">
			<input type="hidden" id="type1" name="type">
			<input type="hidden" id="playId1" name="playId">
			<input type="hidden" id="adsName1" name="adsName">
			<input type="hidden" id="endTime1" name="endTime">
			<input type="hidden" id="startTime1" name="startDate">
			<input type="hidden" id="endTime2" name="endDate">
		</form>  
		</div>
		
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<!-- 	<script src="js/jquery.min.js?v=2.1.4"></script> -->
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script>
 
	//加载footable的功能
				$(document).ready(function() {
					 
		$('#table1').DataTable({
			"pagingType" : "full_numbers"
		});
	 	$('#table2').DataTable({
			"pagingType" : "full_numbers"
		}); 
	});
		function findDetails(type,id,time,user){
			//根据id和type 确认发送的哪一条。 根据时间确定发送的是哪一次。
			var url = "findDetailsById";
			$.post(url, {
				"type" : type,
				"playId" :id,
				"endTime":time,
				"adsName":user
			}, function(data) {
				$('#templist').show();
			var d=eval("(" + data + ")");
			var trStr = "";
				/* swal({
					title : data,
					type : "success"
				}); */
				//alert(d);
			 //拼接历史表记录
			 for (var i = 0; i < d.length; i++)
			 {
				 trStr += "<tr>"
						+ "<td>"
						+ d[i].taxiNum
						+ "</td>"
						+ "<td>"
						+ d[i].isuNum
						+ "</td>"
						+ "<td>"
						+ d[i].playId
						+ "</td>"
						+ "<td>"
						+ d[i].type
						+ "</td>"
						+ "<td>"
						+ d[i].state
						+ "</td>"
						+ "<td>"
						+ d[i].failNum
						+ "</td>"
						+ "<td>"
						+ d[i].endTime
						+ "</td>"
						+"<td>"
						+d[i].adsName
						+"</td>"
						+ "</tr>";
			}
	 
			document.getElementById("tbody").innerHTML = trStr;
			  
			});
			
		}
	</script>
	<script>
	 function findDetails3(type,id,time,user){
			console.log("等待");
			 $("#type1").val(type);
			$("#playId1").val(id);
			$("#adsName1").val(user);
			$("#endTime1").val(time);
			$("#startTime1").val( $("#startDate").val());
			$("#endTime2").val($("#endDate").val());
			console.log("tijiao");
			//提交
			$("#find2").submit();  
			
		}
	</script>
	<script>
	//点击"精确查找"
	$("#selectAds").click(function() {
		var startTime = $("#startDate").val();
		var endTime = $("#endDate").val();
		//开始时间的年份
		var startYear = startTime.substr(0, 4);
		//结束时间的年份
         if (endYear != startYear) {
			swal({
				title : "不支持跨年查询，请重新选择开始时间或结束时间",
				type : "warning"
			});
		}  else {
			$("#selectAds").submit();
		}
	});
	</script>
	<script  type="text/javascript">
function cleanWhitespace(oEelement)//定义一个删除空白节点的函数
{
 for(var i=0;i<oEelement.childNodes.length;i++){
  var node=oEelement.childNodes[i];
  if(node.nodeType==3 && /^\s+/.test(node.nodeValue)){ 

      node.parentNode.removeChild(node);//删除节点
}
  }
}
</script>
</body>

</html>