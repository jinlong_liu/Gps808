<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">
<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">

<script type="text/javascript"
	src="http://api.map.baidu.com/api?v=2.0&ak=xj2Gd1nMEwDHFcGeQvr0WORGk0t1Dlhw"></script>
<!--加载鼠标绘制工具-->
<script type="text/javascript"
	src="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.js"></script>
<link rel="stylesheet"
	href="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.css" />
<!--加载检索信息窗口-->
<script type="text/javascript"
	src="http://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.js"></script>
<link rel="stylesheet"
	href="http://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
</head>

<body class="gray-bg">
	<input type="hidden" id="latitude" value="0" />
	<input type="hidden" id="longtitude" value="0" />
	<!--查询测试  -->
	<!-- 	<div style="width: 730px; margin: auto;">
		要查询的地址：<input id="text_" type="text" value="青岛"
			style="margin-right: 100px;" /> 查询结果(经纬度)：<input id="result"
			type="text" /> <input type="button" value="查询"
			onclick="searchByStationName();" /> <input type="hidden"
			id="latitude" value="0" /> <input type="hidden" id="longtitude"
			value="0" /> <input type="button" value="添加"
			onclick="addPointByFind();" />
			<input type="button" value="清除"
			onclick="deletePointAll();" />
		<div id="container"
			style="position: absolute; margin-top: 30px; width: 730px; height: 590px; top: 50; border: 1px solid gray; overflow: hidden;">
		</div>
	</div>  
	查询测试结束  -->

	<div class="wrapper animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<!-- 地图模块结束 -->
					<!-- 查询模块 -->
					<div class="ibox-title">
						<h5>提示：输入消息越准确，查询速度越快</h5>
					</div>
					<div class="ibox-content" style="padding: 15px 35px 20px">
						<form action="" id="">
							<input type="hidden" id="type" name="type"> <input
								type="hidden" id="pointNum" name="pointNum"> <input
								type="hidden" id="circle" name="circle"> <input
								type="hidden" id="radius" name="radius"> <input
								type="hidden" id="oneP" name="oneP"> <input
								type="hidden" id="twoP" name="twoP"> <input
								type="hidden" id="threeP" name="threeP"> <input
								type="hidden" id="fourP" name="fourP"> <input
								type="hidden" id="fiveP" name="fiveP"> <input
								type="hidden" id="sixP" name="sixP">
							<div class="row">

								<div class="col-sm-2">
									<label class=" control-label">开始时间： </label> <input
										class="form-control" type="text" name="startDate"
										id="startDate" value="${sensorStartDate}" class="Wdate"
										onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'endDate\',{H:-4})}',maxDate:'#F{$dp.$D(\'endDate\')||\'new Date()\'}',readOnly:true})">
								</div>
								<!-- placeholder="默认为当前时间" -->
								<div class="col-sm-2">
									<label class=" control-label">结束时间：</label> <input
										class="form-control" type="text" name="endDate" id="endDate"
										value="${sensorEndDate}" class="Wdate" value=""
										onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'startDate\')}',maxDate:'#F{$dp.$D(\'startDate\',{H:4})}',dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true})">
								</div>

								<div class="col-sm-1">
									<label class=" control-label">车&nbsp;&nbsp;牌&nbsp;&nbsp;号：</label>
									<input class="form-control" placeholder="比如B00001" type="text" autocomplete="off"
										id="taxiNum" name="taxiNum" value="" />
								</div>

								<div class="col-sm-1">
									<label class=" control-label">车型：</label> <select
										class="form-control" id="taxiType">
										<option></option>
										<c:forEach items="${taxiType}" var="type">
											<option value="${type.type}">${type.type}</option>
										</c:forEach>
									</select>
								</div>
								<div class="col-sm-1">
									<label class=" control-label">车辆颜色：</label> <input
										class="form-control" type="text" id="taxiColor"
										name="taxiColor" value="" />
								</div>
								<!-- 			<div class="col-sm-1">
									 <label class=" control-label">所属公司：</label> <input
										class="form-control" type="text" id="companyName"
										name="companyName" value="" />
									<input class="form-control" type="hidden" id="companyName"
										name="companyName" value="" />
								</div> -->
								<div class="col-sm-2">
									<label class=" control-label">输入途经点：</label> <input id="text_"
										type="text" class="form-control" value="青岛"
										style="margin-right: 100px;" />
								</div>
								<div class="col-sm-1">
									<br> <input type="button" value="定位" 
										class="btn btn-primary" style="margin-top: 5px;width:90px"
										onclick="searchByStationName();" />
								</div>
								<div class="col-sm-1">
									<br>
									<button id="start" type="button" class="btn btn-primary "
										style="margin-top: 4px">开始查询</button>
								</div>
								<div class="col-sm-1">
									<br> <input type="button" value="清空途经点"
										class="btn btn-danger" style="margin-top: 4px"
										onclick="deletePointAll();" />
								</div>
							</div>

						</form>
						<div class="row">
							<div class="col-sm-1">
								<!-- <label class="control-label"> 途经点一:</label>  -->
								<input id="add1" type="hidden" class="form-control"
									disabled="disabled" />
							</div>
							<div class="col-sm-1">
								<!-- <label class="control-label"> 途经点二:</label>  -->
								<input id="add2" type="hidden" class="form-control"
									disabled="disabled" />
							</div>
							<div class="col-sm-1">
								<!-- <label class="control-label"> 途经点三:</label>  -->
								<input id="add3" type="hidden" class="form-control"
									disabled="disabled" />
							</div>
							<div class="col-sm-1">
								<!-- <label class="control-label"> 途经点四:</label>  -->
								<input id="add4" type="hidden" class="form-control"
									disabled="disabled" />
							</div>
							<div class="col-sm-1">
								<!-- <label class="control-label"> 途经点五:</label> -->
								<input id="add5" type="hidden" class="form-control"
									disabled="disabled" />
							</div>
							<div class="col-sm-1">
								<!-- <label class="control-label"> 途经点六:</label>  -->
								<input id="add6" type="hidden" class="form-control"
									disabled="disabled" />
							</div>
							<div class="col-sm-1"></div>

						</div>

					</div>
					<!-- 地图模块 -->
					<div class="ibox-content">
						<div id="allmap">
							<div id="map" style="height: 600px"></div>
						</div>
					</div>


				</div>


				<div id="result">
					<div class="ibox-title">
						<h5>查询结果</h5>
					</div>
					<div class="ibox-content">
						<table id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
							<thead>
								<tr>
									<th id="tn">车牌号</th>
									<th>公司</th>
									<th>车主姓名</th>
									<th>车主手机号</th>
									<th>车型</th>
									<th>车色</th>
									<th>时间</th>
								</tr>
							</thead>
							<tbody id="tbody">

							</tbody>
							<tfoot>
								<tr>
									<td colspan="6">
										<ul class="pagination pull-right"></ul>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
					<!-- 查询结果模块 结束-->
				</div>


			</div>
		</div>

	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/content.min.js?v=1.0.0"></script>
	<script src="js/layer.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			document.getElementById('result').style.display = 'none';
		});
		$(document).ready(function() {
			$("#table1").footable();
		});
		// 百度地图API功能
		var map = new BMap.Map("map");
		var poi = new BMap.Point(120.506862, 36.173942);
		map.centerAndZoom("阜阳", 12);
		map.addControl(new BMap.NavigationControl());
		map.enableScrollWheelZoom(); //添加滚轮缩放地图功能
		//通过ip定位城市

		//添加在地图上添加覆盖物的控件
		/* var overlays = [];
		var overlaycomplete = function(e) {
			overlays.push(e.overlay);
		}; */
		var overlays = [];
		var overlaycomplete = function(e) {
			clearAll();
			overlays.push(e.overlay);
		}
		var styleOptions = {
			strokeColor : "red", //边线颜色。
			fillColor : "silver", //填充颜色。当参数为空时，圆形将没有填充效果。
			strokeWeight : 3, //边线的宽度，以像素为单位。
			strokeOpacity : 0.3, //边线透明度，取值范围0 - 1。
			fillOpacity : 0.5, //填充的透明度，取值范围0 - 1。
			strokeStyle : 'solid' //边线的样式，solid或dashed。
		}
		//实例化鼠标绘制工具
		var drawingManager = new BMapLib.DrawingManager(map, {
			isOpen : false, //是否开启绘制模式
			enableDrawingTool : true, //是否显示工具栏
			//enableCalculate:true, //开启面积计算
			drawingToolOptions : {
				anchor : BMAP_ANCHOR_TOP_RIGHT, //位置
				offset : new BMap.Size(5, 5), //偏离值
			},
			//对于各个图形的样式
			circleOptions : styleOptions, //圆的样式
			polylineOptions : styleOptions, //线的样式 *
			polygonOptions : styleOptions, //多边形的样式
			rectangleOptions : styleOptions
		//矩形的样式
		});
		//添加鼠标绘制工具监听事件，用于获取绘制结果
		//drawingManager.addEventListener('overlaycomplete', overlaycomplete);
		//map.addControl(drawingManager); 
		var countPoint = [ 1, 2, 3, 4, 5, 6 ];
		drawingManager
				.addEventListener(
						'overlaycomplete',
						function(e) { //鼠标绘制完成后执行该事件
							clearAll();
							overlaycomplete(e);//先清除以前所有的再添加刚画的
							//计数器 
							if (e.drawingMode == "circle") {
								var r = e.overlay.getRadius();
								var longitude = e.overlay.getCenter().lng;
								var latitude = e.overlay.getCenter().lat;
								$("#type").val("0");
								$("#circle").val(longitude + "," + latitude);
								$("#radius").val(r);

							} else if (e.drawingMode == "rectangle") {
								$("#type").val("1");
								$("#oneP")
										.val(
												e.overlay.getBounds()
														.getNorthEast().lng
														+ ","
														+ e.overlay.getBounds()
																.getNorthEast().lat);
								$("#twoP")
										.val(
												e.overlay.getBounds()
														.getSouthWest().lng
														+ ","
														+ e.overlay.getBounds()
																.getSouthWest().lat);
							} else if (e.drawingMode == "polygon") {
								var points = [];
								points = e.overlay.getPath();
								//alert(points.length);
								if (points.length > 6) {
									alert("多边形不能超过六个顶点");
								} else {
									$("#type").val("2");
									/* for (var i = 0; i < points.length; i++) {
										//alert(points[i].lng+","+points[i].lat);
										$("#point" + (i + 1)).val(points[i].lng + "," + points[i].lat);
									} */
									$("#pointNum").val(points.length);
									$("#oneP")
											.val(
													points[0].lng + ","
															+ points[0].lat);
									$("#twoP")
											.val(
													points[1].lng + ","
															+ points[1].lat);
									$("#threeP")
											.val(
													points[2].lng + ","
															+ points[2].lat);
									if (points.length > 3) {
										$("#fourP").val(
												points[3].lng + ","
														+ points[3].lat);
									}
									if (points.length > 4) {
										$("#fiveP").val(
												points[4].lng + ","
														+ points[4].lat);
									}
									if (points.length > 5) {
										$("#sixP").val(
												points[5].lng + ","
														+ points[5].lat);
									}
								}
							} else if (e.drawingMode == "marker") {
								if (countPoint.length == 0) {
									swal({
										title : "只允许添加6个点",
										type : "warning"
									})
									var marker = e.overlay;
									map.removeOverlay(marker);
								} else {
									var i = countPoint.shift();
									console.log("当前添加的为第" + i + "个点");
									console.log("开始记录marker");
									console.log("e的值" + e.overlay);
									console.log(e.overlay.point);
									console.log("预设数组元素为" + countPoint);
									/*-----------------标注右键删除删除的时候清空坐标点-------------------------*/
									var marker = e.overlay;
									var address;
									/*-----------------标注右键删除-------------------------*/
									var markerMenu = new BMap.ContextMenu();
									markerMenu.addItem(new BMap.MenuItem(
											'删除标注 ', function() {
												map.removeOverlay(marker);
												console.log("删除点开始");
												console.log("被删除点的下标为" + i);
												removePointByFind(i);
												//将删除后的下标添加到数组中供重用。 
												countPoint.unshift(i);
												countPoint.sort();
												console.log("预设数组的元素增加为"
														+ countPoint);
											}))
									marker.addContextMenu(markerMenu);
									/*---------------------------------------------------------------*/
									$("#longtitude").val(e.overlay.point.lng);
									$("#latitude").val(e.overlay.point.lat);
									console.log("开始记录点");
									addPointByFind();
									/*-------------------反编码获取地址信息--------------------------------------*/
									var gc = new BMap.Geocoder();
									var point = new BMap.Point($("#longtitude")
											.val(), $("#latitude").val());
									gc.getLocation(point, function(rs) {
										address = rs.addressComponents;
										console.log(address.province + ", "
												+ address.city + ", "
												+ address.district + ", "
												+ address.street + ", "
												+ address.streetNumber);
									});
									/*-----------------标注点击弹窗-------------------------*/
									marker
											.addEventListener(
													"click",
													function(e) {
														var sContent = "<form method='post' action=''>"
																+ "<table>"
																+ "<tr>"
																+ "<td><b>点序号：</b>"
																+ " <label style='margin-top:10px;width:200px'>"
																+ i
																+ "</label>"
																+ "</td>"
																+ "</tr>"
																+ "<tr>"
																+ "<td><b>名称：</b>"
																+ "<input type='text' name='Name' value='"+address.city+","+address.district+","+address.street+","+address.streetNumber+"' style='margin-top:10px;width:200px'/>"
																+ "</td>"
																+ "</tr>"
																+ "<tr>"
																+ "<td><b>坐标：</b>"
																+ "<input type='text' name='lng' value='" + e.point.lng + "' style='width:80px;'/>"
																+ "<span>-</span>"
																+ "<input type='text' name='lat' value='" + e.point.lat + "' style='width:80px;'/>"
																+ "</td>"
																+ "</tr>"
																+ "<tr >"

																+ "</tr>"
																+ "</table>"
																+ "</form>";
														var opts = {
															enableMessage : false
														};
														var infoWindow = new BMap.InfoWindow(
																sContent, opts);
														this
																.openInfoWindow(infoWindow);
													});

									/*-----------------标注点击弹窗-------------------------*/

								}
							}
						});

		function clearAll() {
			var i1 = 0;
			var i2 = 0;
			for (var i = 0; i < overlays.length; i++) {
				if (overlays[i].toString() == "[object Marker]") {
					i1++;
				}
				i2++;
			}
			if (i1 != i2) {
				for (var j = 0; j < overlays.length; j++) {
					map.removeOverlay(overlays[j]);
				}
				overlays.length = 0;
			}
			//map.clearOverlays();
		}

		$("#start")
				.click(
						function() {
							//获取值
							var circle = $("#circle").val();
							var radius = $("#radius").val();
							var type = $("#type").val();
							var oneP = $("#oneP").val();
							var twoP = $("#twoP").val();
							var threeP = $("#threeP").val();
							var fourP = $("#fourP").val();
							var fiveP = $("#fiveP").val();
							var sixP = $("#sixP").val();
							var startDate = $("#startDate").val();
							var endDate = $("#endDate").val();
							var taxiNum = $("#taxiNum").val();
							var taxiType = $("#taxiType").val();
							var taxiColor = $("#taxiColor").val();
							var companyName = $("#companyName").val();
							var pointNum = $("#pointNum").val();
							if (type == "") {
								swal({
									title : "请在地图上标明地理范围",
									type : "warning"
								});
								return;
							}
							if (startDate == "" || endDate == "") {
								swal({
									title : "请选择要查询的时间段",
									type : "warning"
								});
								return;
							}
							//开始进行操作
							ii = layer.load(2, {
								shade : [ 0.3, '#393D49' ]
							});
							//遮罩层的添加
							/* 	var args = {
									"type" : type,
									"circle" : circle,
									"radius" : radius,
									"pointNum" : pointNum,
									"oneP" : oneP,
									"twoP" : twoP,
									"threeP" : threeP,
									"fourP" : fourP,
									"fiveP" : fiveP,
									"sixP" : sixP,
									"startDate" : startDate,
									"endDate" : endDate,
									"taxiNum" : taxiNum,
									"taxiType" : taxiType,
									"taxiColor" : taxiColor,
									"companyName" : companyName
								//  "taxiState" : taxiState  
								}; */
							var str = "type=" + type + "&circle=" + circle
									+ "&radius=" + radius + "&pointNum="
									+ pointNum + "&oneP=" + oneP + "&twoP="
									+ twoP + "&threeP=" + threeP + "&fourP="
									+ fourP + "&fiveP=" + fiveP + "&sixP="
									+ sixP + "&startDate=" + startDate
									+ "&endDate=" + endDate + "&taxiNum="
									+ taxiNum + "&taxiType=" + taxiType
									+ "&taxiColor=" + taxiColor
									+ "&companyName=" + companyName;

							//首先清空上次的查询记录 
							document.getElementById("tbody").innerHTML = "";
							/* 	var timeout=setTimeout(function(){
									swal({
										title : "查询超时",
										type : "warning",
										animation : "slide-from-left"
									});
									 
									  },
									  60000  //超时时间，考虑到网络问题，5秒还是比较合理的(目前为1分钟)
									); 
							 */

							jQuery
									.ajax({
										type : "post",
										url : "selectCar",
										data : str,
										timeout : 600000,
										success : function(data) {
											document.getElementById('result').style.display = 'block';
											var d = eval("(" + data + ")");

											if (d == "" || d == null) {
												layer.close(ii);
												swal({
													title : "未查找到相应记录",
													type : "error",
													animation : "slide-from-top"
												});
											} else {
												layer.close(ii);
												var trStr = "";
												for (var i = 0; i < d.length; i++) {
													trStr += "<tr>" + "<td>"
															+ d[i].taxiNum
															+ "</td>" + "<td>"
															+ d[i].company.name
															+ "</td>" + "<td>"
															+ d[i].masterName
															+ "</td>" + "<td>"
															+ d[i].masterPhone
															+ "</td>" + "<td>"
															+ d[i].taxiType
															+ "</td>" + "<td>"
															+ d[i].taxiColor
															+ "</td>" + "<td>"
															+ d[i].addDate
															+ "</td>" + "</tr>";
												}
												$('table tbody').html(trStr).trigger('footable_redraw');
											/* 	document
														.getElementById("tbody").innerHTML = trStr;
												$("#tn").click(); */
												/* swal({
													title : "查找到相关车辆，请在查询结果里查看详情",
													type : "sueecss"
												}); */
											}

										},
										//出现错误后执行:
										error : function(data, e) {
											document.getElementById('result').style.display = 'block';
											//关闭遮罩
											layer.close(ii);
											swal({
												title : "查询失败或超时",
												type : "error"
											});
										}
									});

						});
	</script>
	<script type="text/javascript">
		//输入点测试：
		var localSearch = new BMap.LocalSearch(map);
		localSearch.enableAutoViewport();
		function searchByStationName() {
			map.clearOverlays();
			//清空原来的标注
			var keyword = document.getElementById("text_").value;
			localSearch.setSearchCompleteCallback(function(searchResult) {
				var poi = searchResult.getPoi(0);
				map.centerAndZoom(poi.point, 13);
			});
			localSearch.search(keyword);
		}
		var number = 0;
		var lastlai = 0;
		var lastlon = 0;
		var contain = [];
		function removePointByFind(i) {
			//标记为空点
			contain.unshift(i);
			contain.sort();
			console.log("空闲点为：" + contain);
			//alert(i);
			//开始删除对应点
			number = number - 1;
			switch (i) {
			case 1:
				$("#oneP").val("");
				$("#add1").val("");
				$("#pointNum").val(number - 1);
				break;
			case 2:
				$("#twoP").val("");
				$("#add2").val("");
				$("#pointNum").val(number - 1);
				break;
			case 3:
				$("#threeP").val("");
				$("#add3").val("");
				$("#pointNum").val(number - 1);
				break;
			case 4:
				$("#fourP").val("");
				$("#add4").val("");
				$("#pointNum").val(number - 1);
				break;
			case 5:
				$("#fiveP").val("");
				$("#add5").val("");
				$("#pointNum").val(number - 1);
				break;
			case 6:
				$("#sixP").val("");
				$("#add6").val("");
				$("#pointNum").val(number - 1);
				break;
			}
			swal({
				title : "删除成功",
				type : "success"
			});
		}
		function addPointByFind() {
			swal({
				title : "",
				text : "确认将该点设为途经点吗",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function(isConfirm) {
				if (isConfirm) {
					//判断是否选择点了    
					if ($("#longtitude").val() !== "0") {
						//记录当前经纬度，判断是否为重复添加
						if (lastlai !== $("#longtitude").val()
								|| lastlon !== $("#latitude").val()) {
							number += 1;
							//设置半径为400米;
							$("#radius").val(parseFloat(100.0123));
							//设置类型为途经点
							$("#type").val("6");
							if (contain.length > 0) {
								var poi = contain.shift();
								//来找到指定点并添加数据
								console.log(poi + "空闲点数组取数据");
								switch (poi) {
								case 1:
									$("#type").val("6");
									$("#oneP").val(
											$("#longtitude").val() + ","
													+ $("#latitude").val());
									$("#add1").val($("#oneP").val());
									$("#pointNum").val(number);

									break;
								case 2:
									$("#type").val("6");
									$("#twoP").val(
											$("#longtitude").val() + ","
													+ $("#latitude").val());
									$("#add2").val($("#twoP").val());
									$("#pointNum").val(number);
									break;
								case 3:
									$("#threeP").val(
											$("#longtitude").val() + ","
													+ $("#latitude").val());
									$("#add3").val($("#threeP").val());
									$("#pointNum").val(number);
									break;
								case 4:
									$("#fourP").val(
											$("#longtitude").val() + ","
													+ $("#latitude").val());
									$("#add4").val($("#fourP").val());
									$("#pointNum").val(number);
									break;
								case 5:
									$("#fiveP").val(
											$("#longtitude").val() + ","
													+ $("#latitude").val());
									$("#add5").val($("#fiveP").val());
									$("#pointNum").val(number);
									break;
								case 6:
									$("#sixP").val(
											$("#longtitude").val() + ","
													+ $("#latitude").val());
									$("#add6").val($("#sixP").val());
									$("#pointNum").val(number);
									break;
								}

							} else {
								switch (number) {
								case 1:
									$("#type").val("6");
									$("#oneP").val(
											$("#longtitude").val() + ","
													+ $("#latitude").val());
									$("#add1").val($("#oneP").val());
									document.getElementById("add1").name = $(
											"#longtitude").val();
									$("#pointNum").val(number);

									break;
								case 2:
									$("#type").val("6");
									$("#twoP").val(
											$("#longtitude").val() + ","
													+ $("#latitude").val());
									$("#add2").val($("#twoP").val());
									$("#pointNum").val(number);
									break;
								case 3:
									$("#threeP").val(
											$("#longtitude").val() + ","
													+ $("#latitude").val());
									$("#add3").val($("#threeP").val());
									$("#pointNum").val(number);
									break;
								case 4:
									$("#fourP").val(
											$("#longtitude").val() + ","
													+ $("#latitude").val());
									$("#add4").val($("#fourP").val());
									$("#pointNum").val(number);
									break;
								case 5:
									$("#fiveP").val(
											$("#longtitude").val() + ","
													+ $("#latitude").val());
									$("#add5").val($("#fiveP").val());
									$("#pointNum").val(number);
									break;
								case 6:
									$("#sixP").val(
											$("#longtitude").val() + ","
													+ $("#latitude").val());
									$("#add6").val($("#sixP").val());
									$("#pointNum").val(number);
									break;
								default:
									swal({
										title : "只允许添加6个点",
										type : "warning"
									});

									break;
								}
							}
							//alert("添加成功");
							//保存完当前点之后保存其经纬度 便于下次判断是否重复
							lastlai = $("#longtitude").val();
							lastlon = $("#latitude").val();
						} else {
							/* swal({
								title : "请勿重复添加",
								type : "warning"
							}); */
							//alert("请勿重复添加");
						}
					} else {
						//	alert("请先设置 点坐标 ");
					}
				} else {
					//进行标记点的删除
					map.removeOverlay(overlays[overlays.length - 1]);
				}
			})

		}

		//右键添加获取标注坐标并监听事件
		map.addEventListener("rightclick", function(e) {
			rightclickPoint = {
				lng : e.point.lng,
				lat : e.point.lat
			};

			var markerMenu = new BMap.ContextMenu();
			markerMenu.addItem(new BMap.MenuItem('删除标注 ', function() {
				map.removeOverlay(marker);
			}))
			marker.addContextMenu(markerMenu);
		});
		// 然后点击菜单项的时候，在

		function deletePointAll() {
			swal({
				title : "",
				text : "确认清空所有点？",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
				//计数器归0
				number = 0;
				//点数据清空
				$("#oneP").val("");
				$("#twoP").val("");
				$("#threeP").val("");
				$("#fourP").val("");
				$("#fiveP").val("");
				$("#sixP").val("");
				$("#add1").val("");
				$("#add2").val("");
				$("#add3").val("");
				$("#add4").val("");
				$("#add5").val("");
				$("#add6").val("");
				contain.splice(0, contain.length);
				countPoint = [ 1, 2, 3, 4, 5, 6 ];
				//类型和点数量不操作，下次自动更改值。
				//标注清空，覆盖物清空。
				map.clearOverlays();
			});

			//实例化鼠标绘制工具
		    drawingManager = new BMapLib.DrawingManager(map, {
				isOpen : false, //是否开启绘制模式
				enableDrawingTool : true, //是否显示工具栏
				//enableCalculate:true, //开启面积计算
				drawingToolOptions : {
					anchor : BMAP_ANCHOR_TOP_RIGHT, //位置
					offset : new BMap.Size(5, 5), //偏离值
				},
				//对于各个图形的样式
				circleOptions : styleOptions, //圆的样式
				polylineOptions : styleOptions, //线的样式 *
				polygonOptions : styleOptions, //多边形的样式
				rectangleOptions : styleOptions
			//矩形的样式
			});
			
			
		    countPoint = [ 1, 2, 3, 4, 5, 6 ];
			drawingManager
					.addEventListener(
							'overlaycomplete',
							function(e) { //鼠标绘制完成后执行该事件
								clearAll();
								overlaycomplete(e);//先清除以前所有的再添加刚画的
								//计数器 
								if (e.drawingMode == "circle") {
									var r = e.overlay.getRadius();
									var longitude = e.overlay.getCenter().lng;
									var latitude = e.overlay.getCenter().lat;
									$("#type").val("0");
									$("#circle").val(longitude + "," + latitude);
									$("#radius").val(r);

								} else if (e.drawingMode == "rectangle") {
									$("#type").val("1");
									$("#oneP")
											.val(
													e.overlay.getBounds()
															.getNorthEast().lng
															+ ","
															+ e.overlay.getBounds()
																	.getNorthEast().lat);
									$("#twoP")
											.val(
													e.overlay.getBounds()
															.getSouthWest().lng
															+ ","
															+ e.overlay.getBounds()
																	.getSouthWest().lat);
								} else if (e.drawingMode == "polygon") {
									var points = [];
									points = e.overlay.getPath();
									//alert(points.length);
									if (points.length > 6) {
										alert("多边形不能超过六个顶点");
									} else {
										$("#type").val("2");
										/* for (var i = 0; i < points.length; i++) {
											//alert(points[i].lng+","+points[i].lat);
											$("#point" + (i + 1)).val(points[i].lng + "," + points[i].lat);
										} */
										$("#pointNum").val(points.length);
										$("#oneP")
												.val(
														points[0].lng + ","
																+ points[0].lat);
										$("#twoP")
												.val(
														points[1].lng + ","
																+ points[1].lat);
										$("#threeP")
												.val(
														points[2].lng + ","
																+ points[2].lat);
										if (points.length > 3) {
											$("#fourP").val(
													points[3].lng + ","
															+ points[3].lat);
										}
										if (points.length > 4) {
											$("#fiveP").val(
													points[4].lng + ","
															+ points[4].lat);
										}
										if (points.length > 5) {
											$("#sixP").val(
													points[5].lng + ","
															+ points[5].lat);
										}
									}
								} else if (e.drawingMode == "marker") {
									if (countPoint.length == 0) {
										swal({
											title : "只允许添加6个点",
											type : "warning"
										})
										var marker = e.overlay;
										map.removeOverlay(marker);
									} else {
										var i = countPoint.shift();
										console.log("当前添加的为第" + i + "个点");
										console.log("开始记录marker");
										console.log("e的值" + e.overlay);
										console.log(e.overlay.point);
										console.log("预设数组元素为" + countPoint);
										/*-----------------标注右键删除删除的时候清空坐标点-------------------------*/
										var marker = e.overlay;
										var address;
										/*-----------------标注右键删除-------------------------*/
										var markerMenu = new BMap.ContextMenu();
										markerMenu.addItem(new BMap.MenuItem(
												'删除标注 ', function() {
													map.removeOverlay(marker);
													console.log("删除点开始");
													console.log("被删除点的下标为" + i);
													removePointByFind(i);
													//将删除后的下标添加到数组中供重用。 
													countPoint.unshift(i);
													countPoint.sort();
													console.log("预设数组的元素增加为"
															+ countPoint);
												}))
										marker.addContextMenu(markerMenu);
										/*---------------------------------------------------------------*/
										$("#longtitude").val(e.overlay.point.lng);
										$("#latitude").val(e.overlay.point.lat);
										console.log("开始记录点");
										addPointByFind();
										/*-------------------反编码获取地址信息--------------------------------------*/
										var gc = new BMap.Geocoder();
										var point = new BMap.Point($("#longtitude")
												.val(), $("#latitude").val());
										gc.getLocation(point, function(rs) {
											address = rs.addressComponents;
											console.log(address.province + ", "
													+ address.city + ", "
													+ address.district + ", "
													+ address.street + ", "
													+ address.streetNumber);
										});
										/*-----------------标注点击弹窗-------------------------*/
										marker
												.addEventListener(
														"click",
														function(e) {
															var sContent = "<form method='post' action=''>"
																	+ "<table>"
																	+ "<tr>"
																	+ "<td><b>点序号：</b>"
																	+ " <label style='margin-top:10px;width:200px'>"
																	+ i
																	+ "</label>"
																	+ "</td>"
																	+ "</tr>"
																	+ "<tr>"
																	+ "<td><b>名称：</b>"
																	+ "<input type='text' name='Name' value='"+address.city+","+address.district+","+address.street+","+address.streetNumber+"' style='margin-top:10px;width:200px'/>"
																	+ "</td>"
																	+ "</tr>"
																	+ "<tr>"
																	+ "<td><b>坐标：</b>"
																	+ "<input type='text' name='lng' value='" + e.point.lng + "' style='width:80px;'/>"
																	+ "<span>-</span>"
																	+ "<input type='text' name='lat' value='" + e.point.lat + "' style='width:80px;'/>"
																	+ "</td>"
																	+ "</tr>"
																	+ "<tr >"

																	+ "</tr>"
																	+ "</table>"
																	+ "</form>";
															var opts = {
																enableMessage : false
															};
															var infoWindow = new BMap.InfoWindow(
																	sContent, opts);
															this
																	.openInfoWindow(infoWindow);
														});

										/*-----------------标注点击弹窗-------------------------*/

									}
								}
							});
		}
	</script>
	<script type="text/javascript">
		function timeal() {
			alert("d");

		}
	</script>
	<!-- <script type="text/javascript"
		src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script> -->
</body>

</html>