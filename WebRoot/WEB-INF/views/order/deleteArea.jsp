<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>下发区域-多边形</title>
<link rel="shortcut icon" href="favicon.ico">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- <base target="_blank"> -->

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>删除区域</h5>
				</div>
				<div class="ibox-content" style=" height: 756px">
					<form class="form-horizontal" action="deleteArea" method="post" id="getOneForm">
						<div class="form-group">
							<label class="col-sm-3 control-label">车牌号：</label>
							<div class="col-sm-3">
								<input class="form-control" type="text" name="taxiNums" id="taxiNums"
									value="${taxiNum}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">待删除区域ID：</label>
							<div class="col-sm-3">
								<select class="form-control"  name="areaId" id="areaId">
								<option value="001">全部删除</option>
								<c:forEach items="${preinfo}" var="preinfo">
								<option value="${preinfo.id}">${preinfo.name}</option>
								</c:forEach>
								</select>
							</div>
						</div>

					</form>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-4">
							<button class="btn  btn-primary" type="button" id="getOneBtn">发&nbsp;&nbsp;&nbsp;送</button>&nbsp;&nbsp;
							<a href="getTreeOfTemp"><button type="button"
									class="btn">返&nbsp;&nbsp;&nbsp;回</button></a>
						</div>
					</div>
					<br>
				</div>
			</div>
		</div>
	</div>

	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/content.min.js?v=1.0.0"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		var result='<%=request.getAttribute("result")%>';
			if(result != null && result != "null"){
				if (result.indexOf("成功")>0) {
				swal({
					title : result,
					type : "success"
				});
			}else{
				swal({
					title :result,
					type : "error"
				});
			}
			}

	});
	
			 
		//点击"发送"按钮
		$("#getOneBtn").click(function() {
			var taxiNums = $("#taxiNums").val();

			if (taxiNums == "") {
				swal({
					title : "没有选中任何车辆,请点击'返回'按钮返回",
					type : "warning"
				});
			} else {
				 $("#getOneForm").submit();
			}
		});
	</script>
</body>

</html>