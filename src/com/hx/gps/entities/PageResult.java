package com.hx.gps.entities;

import net.sf.json.JSONArray;

public class PageResult {
    
    private Integer sEcho;//总页数
    private Integer iTotalRecords;//实际的行数 记录数
    private Integer iTotalDisplayRecords;//过滤之后，实际的行数。
    private String sColumns;//可选，以逗号分隔的列名，;
    private JSONArray aaData; //实际数组
    
    public PageResult() {
        super();
    }

    public Integer getSEcho() {
        return sEcho;
    }

    public void setSEcho(Integer sEcho) {
        this.sEcho = sEcho;
    }

    public Integer getITotalRecords() {
        return iTotalRecords;
    }

    public void setITotalRecords(Integer iTotalRecords) {
        this.iTotalRecords = iTotalRecords;
    }

    public Integer getITotalDisplayRecords() {
        return iTotalDisplayRecords;
    }

    public void setITotalDisplayRecords(Integer iTotalDisplayRecords) {
        this.iTotalDisplayRecords = iTotalDisplayRecords;
    }

    public String getSColumns() {
        return sColumns;
    }

    public void setSColumns(String sColumns) {
        this.sColumns = sColumns;
    }

    public JSONArray getAaData() {
        return aaData;
    }

    public void setAaData(JSONArray aaData) {
        this.aaData = aaData;
    }
    
    
    

}