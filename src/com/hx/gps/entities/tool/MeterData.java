package com.hx.gps.entities.tool;

import java.util.Date;

import com.hx.gps.util.DateUtil;
import com.mongodb.BasicDBObject;

/**
 * 计价器透传记录
 * 
 * @author 作者 dream:
 * @version 创建时间：2019年6月22日 下午4:09:12 天道丶放飞梦想
 */
public class MeterData {
	private String isuNum;
	private String taxiNum;
	private String type;
	private String orderType;
	private Integer length;
	private String data;
	private String date;

	public MeterData() {
	}

	public MeterData(String isuNum, String taxiNum, String type, String orderType, Integer length, String data,
			String date) {
		super();
		this.isuNum = isuNum;
		this.taxiNum = taxiNum;
		this.type = type;
		this.orderType = orderType;
		this.length = length;
		this.data = data;
		this.date = date;
	}

	public MeterData(BasicDBObject finds) {
		super();
		this.isuNum = finds.getString("isuNum");
		if (finds.getInt("type") == 0) {
			this.type = "上行透传";
		} else {
			this.type = "下行透传";
		}
		this.length = finds.getInt("length");
		this.data = finds.getString("data");
		this.orderType = finds.getString("orderType");
		this.date = DateUtil.dateToString(finds.getDate("date"));
	}

	public String getIsuNum() {
		return isuNum;
	}

	public void setIsuNum(String isuNum) {
		this.isuNum = isuNum;
	}

	public String getTaxiNum() {
		return taxiNum;
	}

	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "{isuNum:'" + isuNum + "', taxiNum:'" + taxiNum + "', type:'" + type + "', orderType:'" + orderType
				+ "', length:'" + length + "', data:'" + data + "', date:'" + date + "}";
	}

}
