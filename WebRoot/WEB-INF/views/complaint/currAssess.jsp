<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">

<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">

	
<!-- 点击"修改"按钮弹出来的form表单 -->
					<div class="modal inmodal fade" id="myModal1" tabindex="-1"
						role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<form action="updateAsscycle" method="post">
									<div class="modal-body">
										<div class="ibox-title">
											<h5>修改当前考核<span style="color:red">（*为必填项）</span></h5>
										</div>
										<div class="ibox-content">
											<div class="row">
												<div class="col-sm-6">
													<input type="text" name="id" id="id">
												</div>
												<div class="col-sm-6">
													<input type="text" name="name" id="name">
												</div>
												<div class="col-sm-6">
													<span style="color:red">* </span>司机初始分数：<input type="text" name="initScorer" id="initScorer">
												</div>
												<div class="col-sm-6">
													<span style="color:red">* </span>司机分数下限：<input type="text" name="lower" id="lower">
												</div>
												<div class="col-sm-6">
													<span style="color:red">* </span>司机分数上限：<input type="text" name="toper" id="toper">
												</div>
												
												<div class="col-sm-6">
													<span style="color:red">* </span>汽车初始分数：<input type="text" name="initScorCar" id="initScorCar">
												</div>
												<div class="col-sm-6">
													<span style="color:red">* </span>汽车分数上限：<input type="text" name="lowCar" id="lowCar">
												</div>
												<div class="col-sm-6">
												<span style="color:red">* </span>司机分数下限：<input type="text" name="topCar" id="topCar">
												</div>
												
												<div class="col-sm-6">
													<span style="color:red">* </span>公司初始分数：<input type="text" name="initScorPany" id="initScorPany">
												</div>
												<div class="col-sm-6">
													<span style="color:red">* </span>公司分数下限：<input type="text" name="lowPany" id="lowPany">
												</div>
												<div class="col-sm-6">
													<span style="color:red">* </span>公司分数上限：<input type="text" name="topPany" id="topPany">
												</div>
												
											</div>
											<br>
										</div>
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-white"
											data-dismiss="modal">取消</button>
											<button type="submit" class="btn btn-primary">提交</button>
									</div>
								</form>
							</div>
						</div>
					</div>

		<div class="row">

			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>当前考核:</h5>
				</div>
				<div class="ibox-content" style="height: 780px">
		
					<table id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
						<thead>
							<tr>
								<th width="5%">姓名</th>
								<th width="5%">性别</th>
								<th width="9%">电话</th>
								<th>资格证号</th>
								<th>驾驶证号</th>
								<th width="8%">对应出租车</th>
								<th>所属公司</th>
								<th width="10%">当前评价名称</th>
								<th width="9%">当期初始分数</th>
								<th>当期得分</th>
								<th>当期排名</th>
								<th width="9%">上期初始分数</th>
								<th>上期得分</th>
								<th>上期排名</th>
								
							</tr>
						</thead>
						<tbody>
						<c:if test=""></c:if>
							<c:forEach items="${evals}" var="evals">
								<tr id="${evals.driId}">
									<td>${evals.dname}</td>
									<td>${evals.sex}</td>
									<td>${evals.phone}</td>
									<td>${evals.queNum}</td>
									<td>${evals.driNum}</td>
									<td>${evals.taxiNum}</td>
									<td>${evals.company}</td>
									<td>${evals.currAssessName}</td>
									<td>${evals.currInitScore}</td>
									<td>${evals.currScore}</td>
									<td>${evals.currRank}</td>
									<td>${evals.lastInitScore}</td>
									<td>${evals.lastScore}</td>
									<td>${evals.lastRank}</td>
									<%-- <td><button id="btn-update" type="button"
											class="btn btn-success btn-sm" data-toggle="modal"
										data-target="#myModal1"
											onclick="alterScore('${asscycle.id}','${asscycle.name}','${asscycle.initScorer}','${asscycle.lower}',
											'${asscycle.toper}','${asscycle.initScorCar}','${asscycle.lowCar}','${asscycle.topCar}','${asscycle.initScorPany}',
											'${asscycle.lowPany}','${asscycle.topPany}')">更改</button>
									</td> --%>	
								</tr>
							</c:forEach>
						</tbody>
			<tfoot>
							<tr>
								<td colspan="14">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script>
	//加载footable的功能
$(document).ready(function() {
	$('#table1').DataTable({
		"pagingType" : "full_numbers"
	});
});
	</script>
</body>

</html>