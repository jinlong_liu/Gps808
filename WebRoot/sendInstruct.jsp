<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title></title>
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="css/plugins/jsTree/style.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<!-- <base target="_blank"> -->
<style>
.jstree-open>.jstree-anchor>.fa-folder:before {
	content: "\f07c"
}

.jstree-default .jstree-icon.none {
	width: 0
}

.red{

    color:#fda6a6;
}

.green{
color: #2b6901 ;
font-weight:900;
}
.black{

    color:#000000;

}
textarea {
	resize: none;
}
</style>
</head>

<body class="gray-bg">

	<div class="wrapper animated fadeInRight">

			<div class="row">
			<div class="col-sm-9">
				
					<table id="table1" 
 
							style="table-layout: fixed; background: #FFFFFF; font-size: 18px;width:100%;height:821px"
 
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
							<tbody>
                                   
                                       <tr>
                                            <th width="20%"></th>
                                       		<th>请选择下发指令</th>
                                       		<th>操作</th>
                                       </tr>
                                         <tr>
                                            <td></td>
											<td>开启广告屏</td>
											<td><input value="0" id="instructType0" type="radio" style="width:20px;height:20px"
													name="instructType">&nbsp;&nbsp;
											</td>
                                         </tr>
                                         <tr>
                                        	 <td></td>
											 <td>关闭广告屏</td>
							   				 <td><input value="1" id="instructType1" name="instructType"  style="width:20px;height:20px"
								type="radio"></td>
										</tr>

    
										<tr>
											<td></td>
											<td>亮度固定5</td>
											<td> <input value="2" id="instructType2" name="instructType" style="width:20px;height:20px"
								type="radio"></td>
										</tr>
						
				                       <tr>
				                       		<td></td>
											<td>亮度固定14</td>
											<td> <input value="3" id="instructType3" name="instructType" style="width:20px;height:20px"
								type="radio"> 
											</td>
									  </tr>
						              
						              <tr>
					     			 <td></td>
								        <td>自定义亮度级别
								

									   <div class="row" id="playIdLight">
							 			<br>
													<label class="col-sm-4 control-label">请选择亮度:</label> 
												<div class="col-sm-3">
														<select name="LightId" id="LightId" class="form-control">
														<!-- 播放项id，目前设置成静态的 -->
															<OPTION value=1>1</OPTION>
															<OPTION value=2>2</OPTION>
															<OPTION value=3>3</OPTION>
															<OPTION value=4>4</OPTION>
															<OPTION value=5>5</OPTION>
															<OPTION value=6>6</OPTION>
															<OPTION value=7>7</OPTION>
															<OPTION value=8>8</OPTION>
															<OPTION value=9>9</OPTION>
															<OPTION value=10>10</OPTION>
															<OPTION value=11>11</OPTION>
															<OPTION value=12>12</OPTION>
															<OPTION value=13>13</OPTION>
															<OPTION value=14>14</OPTION>
															<OPTION value=15>15</OPTION>
														</select>
													</div>
											</div></td>
	 									<td> <input value="8" id="instructType8" name="instructType" style="width:20px;height:20px"
								type="radio"> 
								       </td>
											
									</tr>
						              
									  <tr>
									  <td></td>
									  <td>时间校正指令</td>
									  <td> <input value="4" id="instructType4" name="instructType" style="width:20px;height:20px"
								type="radio"> </td>
										</tr>
						
					   				  <tr>
										<td></td>
										<td>删除所有广告</td>
										<td> <input value="5" id="instructType5" name="instructType" style="width:20px;height:20px"
								type="radio"> 
										</td>
									</tr>
					     			 <tr>
					     			 <td></td>
								        <td>删除指定广告
								

									   <div class="row" id="playIdSelect">
							 	<br>
													<label class="col-sm-4 control-label">请选择广告id:</label> 
												<div class="col-sm-3">
														<select name="playId" id="selectId" class="form-control">
														<!-- 播放项id，目前设置成静态的 -->
															<OPTION value=0>0</OPTION>
															<OPTION value=1>1</OPTION>
															<OPTION value=2>2</OPTION>
															<OPTION value=3>3</OPTION>
															<OPTION value=4>4</OPTION>
															<OPTION value=5>5</OPTION>
															<OPTION value=6>6</OPTION>
															<OPTION value=7>7</OPTION>
															<OPTION value=8>8</OPTION>
															<OPTION value=9>9</OPTION>
															<OPTION value=10>10</OPTION>
															<OPTION value=11>11</OPTION>
															<OPTION value=12>12</OPTION>
															<OPTION value=13>13</OPTION>
															<OPTION value=14>14</OPTION>
															<OPTION value=15>15</OPTION>
															<OPTION value=16>16</OPTION>
															<OPTION value=17>17</OPTION>
															<OPTION value=18>18</OPTION>
															<OPTION value=19>19</OPTION>
															<OPTION value=20>20</OPTION>
															<OPTION value=21>21</OPTION>
															<OPTION value=22>22</OPTION>
															<OPTION value=23>23</OPTION>
															<OPTION value=24>24</OPTION>
															<!-- <OPTION value=25>25</OPTION>
															<OPTION value=26>26</OPTION>
															<OPTION value=27>27</OPTION>
															<OPTION value=28>28</OPTION>
															<OPTION value=29>29</OPTION>
															<OPTION value=30>30</OPTION>
															<OPTION value=31>31</OPTION>
															<OPTION value=32>32</OPTION>
															<OPTION value=33>33</OPTION>
															<OPTION value=34>34</OPTION>
															<OPTION value=35>35</OPTION>
															<OPTION value=36>36</OPTION>
															<OPTION value=37>37</OPTION>
															<OPTION value=38>38</OPTION>
															<OPTION value=39>39</OPTION>
															<OPTION value=40>40</OPTION>
															<OPTION value=41>41</OPTION>
															<OPTION value=42>42</OPTION>
															<OPTION value=43>43</OPTION>
															<OPTION value=44>44</OPTION>
															<OPTION value=45>45</OPTION>
															<OPTION value=46>46</OPTION>
															<OPTION value=47>47</OPTION>
															<OPTION value=48>48</OPTION>
															<OPTION value=49>49</OPTION>
															<OPTION value=50>50</OPTION>
															<OPTION value=51>51</OPTION>
															<OPTION value=52>52</OPTION>
															<OPTION value=53>53</OPTION>
															<OPTION value=54>54</OPTION>
															<OPTION value=55>55</OPTION>
															<OPTION value=56>56</OPTION>
															<OPTION value=57>57</OPTION>
															<OPTION value=58>58</OPTION>
															<OPTION value=59>59</OPTION>
															<OPTION value=60>60</OPTION>
															<OPTION value=61>61</OPTION>
															<OPTION value=62>62</OPTION>
															<OPTION value=63>63</OPTION>
															<OPTION value=64>64</OPTION>
															<OPTION value=65>65</OPTION>
															<OPTION value=66>66</OPTION>
															<OPTION value=67>67</OPTION>
															<OPTION value=68>68</OPTION>
															<OPTION value=69>69</OPTION>
															<OPTION value=70>70</OPTION>
															<OPTION value=71>71</OPTION>
															<OPTION value=72>72</OPTION>
															<OPTION value=73>73</OPTION>
															<OPTION value=74>74</OPTION>
															<OPTION value=75>75</OPTION>
															<OPTION value=76>76</OPTION>
															<OPTION value=77>77</OPTION>
															<OPTION value=78>78</OPTION>
															<OPTION value=79>79</OPTION>
															<OPTION value=80>80</OPTION>
															<OPTION value=81>81</OPTION>
															<OPTION value=82>82</OPTION>
															<OPTION value=83>83</OPTION>
															<OPTION value=84>84</OPTION>
															<OPTION value=85>85</OPTION>
															<OPTION value=86>86</OPTION>
															<OPTION value=87>87</OPTION>
															<OPTION value=88>88</OPTION>
															<OPTION value=89>89</OPTION>
															<OPTION value=90>90</OPTION>
															<OPTION value=91>91</OPTION>
															<OPTION value=92>92</OPTION>
															<OPTION value=93>93</OPTION>
															<OPTION value=94>94</OPTION>
															<OPTION value=95>95</OPTION>
															<OPTION value=96>96</OPTION>
															<OPTION value=97>97</OPTION>
															<OPTION value=98>98</OPTION>
															<OPTION value=99>99</OPTION>
															<OPTION value=100>100</OPTION>
															<OPTION value=101>101</OPTION>
															<OPTION value=102>102</OPTION>
															<OPTION value=103>103</OPTION>
															<OPTION value=104>104</OPTION>
															<OPTION value=105>105</OPTION>
															<OPTION value=106>106</OPTION>
															<OPTION value=107>107</OPTION>
															<OPTION value=108>108</OPTION>
															<OPTION value=109>109</OPTION>
															<OPTION value=110>110</OPTION>
															<OPTION value=111>111</OPTION>
															<OPTION value=112>112</OPTION>
															<OPTION value=113>113</OPTION>
															<OPTION value=114>114</OPTION>
															<OPTION value=115>115</OPTION>
															<OPTION value=116>116</OPTION>
															<OPTION value=117>117</OPTION>
															<OPTION value=118>118</OPTION>
															<OPTION value=119>119</OPTION>
															<OPTION value=120>120</OPTION>
															<OPTION value=121>121</OPTION>
															<OPTION value=122>122</OPTION>
															<OPTION value=123>123</OPTION>
															<OPTION value=124>124</OPTION>
															<OPTION value=125>125</OPTION>
															<OPTION value=126>126</OPTION>
															<OPTION value=127>127</OPTION>
															<OPTION value=128>128</OPTION>
															<OPTION value=129>129</OPTION>
															<OPTION value=130>130</OPTION>
															<OPTION value=131>131</OPTION>
															<OPTION value=132>132</OPTION>
															<OPTION value=133>133</OPTION>
															<OPTION value=134>134</OPTION>
															<OPTION value=135>135</OPTION>
															<OPTION value=136>136</OPTION>
															<OPTION value=137>137</OPTION>
															<OPTION value=138>138</OPTION>
															<OPTION value=139>139</OPTION>
															<OPTION value=140>140</OPTION>
															<OPTION value=141>141</OPTION>
															<OPTION value=142>142</OPTION>
															<OPTION value=143>143</OPTION>
															<OPTION value=144>144</OPTION>
															<OPTION value=145>145</OPTION>
															<OPTION value=146>146</OPTION>
															<OPTION value=147>147</OPTION>
															<OPTION value=148>148</OPTION>
															<OPTION value=149>149</OPTION>
															<OPTION value=150>150</OPTION>
															<OPTION value=151>151</OPTION>
															<OPTION value=152>152</OPTION>
															<OPTION value=153>153</OPTION>
															<OPTION value=154>154</OPTION>
															<OPTION value=155>155</OPTION>
															<OPTION value=156>156</OPTION>
															<OPTION value=157>157</OPTION>
															<OPTION value=158>158</OPTION>
															<OPTION value=159>159</OPTION>
															<OPTION value=160>160</OPTION>
															<OPTION value=161>161</OPTION>
															<OPTION value=162>162</OPTION>
															<OPTION value=163>163</OPTION>
															<OPTION value=164>164</OPTION>
															<OPTION value=165>165</OPTION>
															<OPTION value=166>166</OPTION>
															<OPTION value=167>167</OPTION>
															<OPTION value=168>168</OPTION>
															<OPTION value=169>169</OPTION>
															<OPTION value=170>170</OPTION>
															<OPTION value=171>171</OPTION>
															<OPTION value=172>172</OPTION>
															<OPTION value=173>173</OPTION>
															<OPTION value=174>174</OPTION>
															<OPTION value=175>175</OPTION>
															<OPTION value=176>176</OPTION>
															<OPTION value=177>177</OPTION>
															<OPTION value=178>178</OPTION>
															<OPTION value=179>179</OPTION>
															<OPTION value=180>180</OPTION>
															<OPTION value=181>181</OPTION>
															<OPTION value=182>182</OPTION>
															<OPTION value=183>183</OPTION>
															<OPTION value=184>184</OPTION>
															<OPTION value=185>185</OPTION>
															<OPTION value=186>186</OPTION>
															<OPTION value=187>187</OPTION>
															<OPTION value=188>188</OPTION>
															<OPTION value=189>189</OPTION>
															<OPTION value=190>190</OPTION>
															<OPTION value=191>191</OPTION>
															<OPTION value=192>192</OPTION>
															<OPTION value=193>193</OPTION>
															<OPTION value=194>194</OPTION>
															<OPTION value=195>195</OPTION>
															<OPTION value=196>196</OPTION>
															<OPTION value=197>197</OPTION>
															<OPTION value=198>198</OPTION>
															<OPTION value=199>199</OPTION>
															<OPTION value=200>200</OPTION>
															<OPTION value=201>201</OPTION>
															<OPTION value=202>202</OPTION>
															<OPTION value=203>203</OPTION>
															<OPTION value=204>204</OPTION>
															<OPTION value=205>205</OPTION>
															<OPTION value=206>206</OPTION>
															<OPTION value=207>207</OPTION>
															<OPTION value=208>208</OPTION>
															<OPTION value=209>209</OPTION>
															<OPTION value=210>210</OPTION>
															<OPTION value=211>211</OPTION>
															<OPTION value=212>212</OPTION>
															<OPTION value=213>213</OPTION>
															<OPTION value=214>214</OPTION>
															<OPTION value=215>215</OPTION>
															<OPTION value=216>216</OPTION>
															<OPTION value=217>217</OPTION>
															<OPTION value=218>218</OPTION>
															<OPTION value=219>219</OPTION>
															<OPTION value=220>220</OPTION>
															<OPTION value=221>221</OPTION>
															<OPTION value=222>222</OPTION>
															<OPTION value=223>223</OPTION>
															<OPTION value=224>224</OPTION>
															<OPTION value=225>225</OPTION>
															<OPTION value=226>226</OPTION>
															<OPTION value=227>227</OPTION>
															<OPTION value=228>228</OPTION>
															<OPTION value=229>229</OPTION>
															<OPTION value=230>230</OPTION>
															<OPTION value=231>231</OPTION>
															<OPTION value=232>232</OPTION>
															<OPTION value=233>233</OPTION>
															<OPTION value=234>234</OPTION>
															<OPTION value=235>235</OPTION>
															<OPTION value=236>236</OPTION>
															<OPTION value=237>237</OPTION>
															<OPTION value=238>238</OPTION>
															<OPTION value=239>239</OPTION>
															<OPTION value=240>240</OPTION>
															<OPTION value=241>241</OPTION>
															<OPTION value=242>242</OPTION>
															<OPTION value=243>243</OPTION>
															<OPTION value=244>244</OPTION>
															<OPTION value=245>245</OPTION>
															<OPTION value=246>246</OPTION>
															<OPTION value=247>247</OPTION>
															<OPTION value=248>248</OPTION>
															<OPTION value=249>249</OPTION>
															<OPTION value=250>250</OPTION>
															<OPTION value=251>251</OPTION>
															<OPTION value=252>252</OPTION>
															<OPTION value=253>253</OPTION>
															<OPTION value=254>254</OPTION> -->
														</select>
													</div>
											</div></td>
	 									<td> <input value="6" id="instructType6" name="instructType" style="width:20px;height:20px"
								type="radio"> 
								       </td>
											
									</tr>
									<!-- <tr>
										<td></td>
								        <td>广告屏重置</td>
										<td> <input value="7" id="instructType7" name="instructType" style="width:20px;height:20px"
								        type="radio"> 
								        </td>
									</tr> -->
							</tbody>
						</table>
				</div>

			<!--右侧公司选择栏开始  -->
			<div class="col-sm-3">
				<div class="float-e-margins">

					<div class="ibox-content"  style="height: 820px">
						<button type="button" class="btn btn-primary btn-sm"
							onclick="select()">下发</button>

						<br> <input type="hidden" name="taxiNums" id="taxiNums">
						<input type="hidden" name="instructId" id="instructId">
						 
						<hr>
						<h4>请选择车辆</h4>
						<div id="jstree1" class="trdemo"
							style=" overflow-x: auto; overflow-y: auto; height: 700px;">
							<ul>
							</ul>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- 删除表格 -->
		<form id="deleteForm" action="deleteAdsById" method="post">
			<input type="hidden" id="deleteId" name="id">
		</form>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/echarts/echarts-all.js"></script>
	<script src="js/content.min.js?v=1.0.0"></script>
	<script src="js/demo/echarts-demo2.min.js"></script>
	<script src="js/plugins/jsTree/jstree.min.js"></script>
	<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
 
	<script type="text/javascript">
		//选择公司开始
		$(document).ready(function() {
			/* $("#miletable").hide(); */
			$("#taxiNums").val("");
			// 异步刷新 【车辆列表】
			function pollingTree(){
		  	      var newdata = sessionStorage.getItem("treelist");
		  	  if(newdata!=null){
		  	    var tree= $("#jstree1")
         	   tree.jstree(true).settings.core.data = JSON.parse(newdata);
         	   tree.jstree(true).refresh(true);
		  	  }
			}
			//实现jQuery轮询树的状态
			$(document).ready(function(){
				setInterval(pollingTree, 10*1000);  
				});
			$("#jstree1").jstree({
				"core" : {
					"check_callback" : true,
					'data' : function (node, cb) {
						if(sessionStorage.getItem("treelist")==null){
						    $.ajax({
						    	type:"POST",
						    	dataType:"json",
						    	url : "/GpsManage/getTreeOfTempJson",
						    	success: function(data) {
						    		cb.call(this,data);
						   		}
							});
						}else{
							var newdata = sessionStorage.getItem("treelist");
							cb.call(this,JSON.parse(newdata));
						}
					}
				},
				"plugins" : [ "types", "dnd", "checkbox" ],
				"types" : {
					"car" : {
						"icon" : "fa fa-car"
					},
					"default" : {
						"icon" : "fa fa-folder"
					}
				}
			});
			//页面加载的时候选中转发过来的id
			var idSelect='<%=request.getAttribute("selected")%>';
			
			$(document).ready(function(){
				if(idSelect!=null){ 
				$("input[name=instructType][value="+idSelect+"]").attr("checked", 'checked');
				/* swal({
					title : "指定信息号已被选中",
					type : "success"
				}); */
				}
				})
			//获取选择节点的id
			$("#jstree1").on('changed.jstree', function(e, data) {
				r = [];
				var i, j;
				for (i = 0, j = data.selected.length; i < j; i++) {
					var node = data.instance.get_node(data.selected[i]);
					if (data.instance.is_leaf(node)) {
						r.push(node.id);
					}
				}
				$("#taxiNums").val(r);
			});
		});
	</script>
	<script>
		var trs = document.getElementById('table1').getElementsByTagName('tr');
		window.onload = function() {
			for (var i = 0; i < trs.length; i++) {
				trs[i].onmousedown = function() {
					tronmousedown(this);
				}
			}
		}
		function tronmousedown(obj) {
			for (var o = 0; o < trs.length; o++) {
				if (trs[o] == obj) {
					trs[o].style.backgroundColor = '#DFEBF2';
				} else {
					trs[o].style.backgroundColor = '';
				}
			}
		}
 
		</script>
 
	<script>
		function select() {
			//获取选中的车辆
			var taxiNums = $("#taxiNums").val();
			//选中的广告id
			var playId=$('#selectId option:selected').val(); 
			var taxinum;
			taxinum=taxiNums.split(",");
			//记录长度
			var len=taxiNums.length;
			var len2=taxinum.length;
			console.log(len);
			//选中的指令
			var instructId = $("#instructId").val();
			if(instructId=='8'){
				playId=$('#LightId option:selected').val(); 
			}
	         if (instructId == "") {	
				swal({
					title : "请选择要下发的指令",
					type : "warning"
				});
				return;
			} else if (taxiNums == "") {
				swal({
					title : "请先选择车辆",
					type : "warning"
				});
				return;
			}else if(len==1){
				swal({
					title : "该公司暂无车辆或车牌号格式有误",
					type : "warning"
				});
				return;
			}
			else if(len<7){
				swal({
					title : "车牌号长度有误",
					type : "warning"
				});
				return;
			}   
				var url = "SendInstructByAjax";
				$.post(url, {
					"taxiNums" : taxiNums,
					"instructId" : instructId,
					"playId":playId
					 
				}, function(data) {
					swal({
						title : data,
						type : "success"
					});
		 
				});

			 
				 
		}
	</script>


	<script>
	//点击"删除"按钮
	function delete1(id,name) {
		swal({
			title : "",
			text : "您确定删除名为" + name + " 的广告数据吗?",
			type : "warning",
			showCancelButton : true,
			cancelButtonText : "取消"
		}, function() {
			$("#deleteId").val(id);
			//提交表单
			$("#deleteForm").submit();
		})
	}
	//页面加载
	$(document).ready(function(){
			var result='<%=request.getAttribute("state")%>';
			if(result != null && result != "null"){
				if (result.indexOf("成功")>0) {
				swal({
					title : result,
					type : "success"
				});
			}else{
				swal({
					title :result,
					type : "warning"
				});
			}
			}

		});
	//默认隐藏 
	$("#playIdSelect").hide();
	$("#playIdLight").hide();
	//点击type6
		  $(function () {
                  $("input:radio[name='instructType']").click(function () {
                	  
			  		 var val=$(this).val();
			  		 $("#instructId").val(val);
                      if ($(this).val()==6) {
                         // alert("选中了");
                          $("#playIdSelect").show();
                      }else{
                    	  $("#playIdSelect").hide();
                      }
					  if ($(this).val()==8) {
		                         // alert("选中了");
		                 $("#playIdLight").show();
		              }else{
		                 $("#playIdLight").hide();
		              }
               
                  });
              });
 	
		//默认隐藏 
 
	</script>

</body>

</html>