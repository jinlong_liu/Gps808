package com.hx.gps.entities.tool;

public class OperateTest {
	private String taxiNum;		//车牌号
	private String company;		//公司名称
	private Integer number;		//客次数
	private float money;		//金额
	private float fullmil;		//载客里程
	private float emptymli;		//空驶里程
	private String waitTime;	//等待时间
	
	public String getTaxiNum() {
		return taxiNum;
	}
	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}
	public float getMoney() {
		return money;
	}
	public void setMoney(float money) {
		this.money = money;
	}
	public float getFullmil() {
		return fullmil;
	}
	public void setFullmil(float fullmil) {
		this.fullmil = fullmil;
	}
	public float getEmptymli() {
		return emptymli;
	}
	public void setEmptymli(float emptymli) {
		this.emptymli = emptymli;
	}
	
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public String getWaitTime() {
		return waitTime;
	}
	public void setWaitTime(String waitTime) {
		this.waitTime = waitTime;
	}
	@Override
	public String toString() {
		return "Operate [taxiNum=" + taxiNum + ", money=" + money
				+ ", fullmil=" + fullmil + ", emptymli=" + emptymli + "]";
	}
	public OperateTest(String taxiNum, float money, float fullmil, float emptymli) {
		super();
		this.taxiNum = taxiNum;
		this.money = money;
		this.fullmil = fullmil;
		this.emptymli = emptymli;
	}
	public OperateTest() {
		super();
	}

}
