package com.hx.gps.entities.tool;

public class PassVol {
	private String type;//日或者月
	private int passNum;//客运次数
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getPassNum() {
		return passNum;
	}
	public void setPassNum(int passNum) {
		this.passNum = passNum;
	}
	
	public PassVol(String type, int passNum) {
		super();
		this.type = type;
		this.passNum = passNum;
	}
	public PassVol() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "PassVol [type=" + type + ", passNum=" + passNum + "]";
	}
	
}
