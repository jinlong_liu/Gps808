<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">

<script type="text/javascript"
	src="http://api.map.baidu.com/api?v=2.0&ak=xj2Gd1nMEwDHFcGeQvr0WORGk0t1Dlhw"></script>
<!-- 加载鼠标绘制工具 -->
<script type="text/javascript"
	src="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.js"></script>
<link rel="stylesheet"
	href="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.css" />
<!-- 加载检索信息窗口 -->
<script type="text/javascript"
	src="http://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.js"></script>
<link rel="stylesheet"
	href="http://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>

<!-- gy 添加表单验证 -->
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />
<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>
</head>

<body class="gray-bg">
	<div class="wrapper animated ">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-content">
					<button type="button" class="btn btn-primary" data-toggle="modal"
						data-target="#myModal5">添加新的预设信息</button>
					<div class="modal inmodal fade" id="myModal5" tabindex="-1"
						role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<!-- <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
										<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
									</button>
									<h4 class="modal-title">窗口标题</h4>
									<small class="font-bold">这里可以显示副标题。 
								</div> -->
								<form action="addPreInfo" id="addPreInfoForm" class="form-horizontal"  method="post">
									<div class="modal-body">
										<div class="ibox-title">
											<h5>添加新的预设信息<span style="color:red">（* 为必填项）</span></h5>
										</div>
										<div class="ibox-content">
											<!-- 限时 -->
											
											  <div class="row">
												<div class="col-sm-3">
													<input type="checkbox" name="timeLimit" value="1" /> 限时 
												</div>
												<div class="col-sm-3">
													<input type="checkbox" name="rateLimit" value="1" /> 限速 
												</div>
												<!-- 聚集报警 -->
												<div class="col-sm-3">
													<input type="checkbox" name="gatherAlarm" value="1" />
													聚集报警 
												</div>
												</div>
												
											    <!-- 进出 -->
											     <div class="row">
												<div class="col-sm-3">
													<input type="checkbox" name="inToPlatform" value="1" />
													进区域报警给平台 
												</div>
												<div class="col-sm-3">
													<input type="checkbox" name="outToPlatform" value="1" />
													进区域报警给平台 
												</div>
												
											   
												<div class="col-sm-3">
													<input type="checkbox" name="inToDriver" value="1" />
													进区域报警给驾驶员 
												</div>
												<div class="col-sm-3">
													<input type="checkbox" name="outToDriver" value="1" />
													出区域报警给驾驶员 
												</div>
												</div>
												<br>
												
									
											
											<!-- 限速 -->
											<div class="row">
											<div class="col-sm-6">
											<div class="form-group">
												<label class="col-sm-5 control-label"><span style="color:red">*  </span>最大速度 （km/s）：</label>
												<div class="col-sm-7">
												<input type="text" name="maxSpeed" class="form-control" >
												</div>
												</div>
											</div>
												
												<div class="col-sm-6">
												<div class="form-group">
												<label class="col-sm-5 control-label"><span style="color:red">*  </span>开始时间 ：</label>
												<div class="col-sm-7">
												<input type="text" name="startDate" class="form-control"  id="startDate"
														value="${sensorStartDate}" class="Wdate"
														onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'endDate\')||\'new Date()\'}',onpicked:function(dp){refreshValidator(addPreInfoForm, name.valueOf())},readOnly:true})">
												</div>
												</div>
												</div>
												</div>
												
												<div class="row">
												<div class="col-sm-6">
												<div class="form-group">
												<label class="col-sm-5 control-label"><span style="color:red">*  </span>持续时间 （秒）：</label>
												<div class="col-sm-7">
												<input type="text" name="continueTime" class="form-control" >
												</div>
												</div>
												</div>
												<div class="col-sm-6">
												<div class="form-group">
												<label class="col-sm-5 control-label"><span style="color:red">*  </span>结束时间 ： </label>
												<div class="col-sm-7">
												<input type="text" name="endDate" class="form-control"  id="endDate"
														value="${sensorEndDate}"
														class="Wdate"
														onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'startDate\')}',maxDate:'#F{\'new Date()\'}',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(addPreInfoForm, name.valueOf())},readOnly:true})">
												</div>
												</div>
												</div>
												</div>
												
											<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
												<label class="col-sm-5 control-label"><span style="color:red">*  </span>车辆上限 ：</label>
												<div class="col-sm-7">
													<input type="text" name="carUpper" class="form-control" >
												</div>
												</div>
												</div>
											  <div class="col-sm-6">
												<div class="form-group">
												<label class="col-sm-5 control-label"><span style="color:red">*  </span>名称 ： </label>
												<div class="col-sm-7">
												  <input type="text" class="form-control"  name="name" class="form-control" ></div>
												 </div>
												  </div>
									 
												 </div>
												 <div class="row">
												<div class="col-sm-6">
												<div class="form-group">
												<label class="col-sm-5 control-label"><span style="color:red">*  </span>编号 ：</label>
												<div class="col-sm-7">
												   <input type="text" class="form-control" name="num" class="form-control"></div>
												 </div>
												 </div>
											
												</div>  
												</div>   
										<div class="ibox-title">
											<h5>预设地图区域</h5>
											<div class="ibox-tools">
												<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
												</a>
											</div>
										</div>
										<div class="ibox-content">
											<div id="allmap">
												<div id="map" style="height:300px"></div>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<input type="hidden" id="type" name="type"> <input
											type="hidden" id="pointNum" name="pointNum"> <input
											type="hidden" id="circle" name="circle"> <input
											type="hidden" id="radius" name="radius"> <input
											type="hidden" id="oneP" name="oneP"> <input
											type="hidden" id="twoP" name="twoP"> <input
											type="hidden" id="threeP" name="threeP"> <input
											type="hidden" id="fourP" name="fourP"> <input
											type="hidden" id="fiveP" name="fiveP"> <input
											type="hidden" id="sixP" name="sixP">
										<button id="btn_addPreInfo" type="button" class="btn btn-white"
											data-dismiss="modal">关闭</button>
										<button id="submit_preinfo" type="submit" class="btn btn-primary" >保存</button>
									 
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>预设信息表</h5>
				</div>
 
				<div class="ibox-content" style="height: 710px">
					<table id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
 
						<thead>
							<tr>
								<th>名称</th>
								<th>编号</th>
								<th>类型</th>
								<th>触发聚集报警</th>
								<th>允许车辆上限</th>
								<th>当前车辆总数</th>
								<th>是否限时</th>
								<th>开始时间</th>
								<th>结束时间</th>
								<th>是否限速</th>
								<th>最高时速</th>
								<th>超速持续时间</th>
								<th>进区域报警给驾驶员</th>
								<th>出区域报警给驾驶员</th>
								<th>进区域报警给平台</th>
								<th>出区域报警给平台</th>
								<th>操作人</th>
								<th>添加时间</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${preinfo}" var="preinfo">
								<tr>
									<td>${preinfo.name}</td>
									<td>${preinfo.num}</td>
									<td>
										<c:if test="${preinfo.graph.type==0}">圆形</c:if>
										<c:if test="${preinfo.graph.type==1}">矩形</c:if>
										<c:if test="${preinfo.graph.type==2}">多边形</c:if>
									</td>
									<td>${preinfo.gatherAlarm}</td>
									<td>${preinfo.carUpper}</td>
									<td><%-- ${preinfo.carNum} --%>0</td>
									<td>${preinfo.timeLimit}</td>
									<td>${preinfo.startTime}</td>
									<td>${preinfo.endTime}</td>
									<td>${preinfo.rateLimit}</td>
									<td>${preinfo.maxSpeed}</td>
									<td>${preinfo.continueTime}</td>
									<td>${preinfo.inToDriver}</td>
									<td>${preinfo.outToDriver}</td>
									<td>${preinfo.inToPlatform}</td>
									<td>${preinfo.outToPlatform}</td>
									<td>${preinfo.people}</td>
									<td>${preinfo.time}</td>
									<td>
									            
												<button type="button" class="btn btn-danger btn-sm"
													onclick="delete1('${preinfo.id}','${preinfo.name}')">删除</button>
										
										</td>
								</tr>
							</c:forEach>
							
						</tbody>
						<tfoot>
							<tr>
								<td colspan="18">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
			<!-- 删除表格 -->
	  	<form id="deleteForm" action="deletePreInfo" method="post">
			<input type="hidden" id="deleteId" name="id">
		</form>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>

	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script>
	
	$(document).ready(function() {
		$('#table1').DataTable({
			"pagingType" : "full_numbers"
		});
	});
	

	//点击"删除"按钮
	function delete1(id, name) {
		swal({
			title : "您确定删除 " + name + " 的数据吗?",
			type : "warning",
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "确认",
			showCancelButton : true,
			cancelButtonText : "取消"
		}, function() {
			$("#deleteId").val(id);
			//提交表单
			$("#deleteForm").submit();
	   	});
	}
    //页面加载完成后执行,获取执行状态
	$(document).ready(function(){
		console.log("获取执行状态");	
		var result = '<%=request.getAttribute("result")%>';
		if(result != null && result != "null"){
			if (result="操作执行成功！") {
			swal({
				title : result,
				type : "success"
			});
		}else{
			swal({
				title :result,
				type : "error"
			});
		}
		}
	});
				
		// 百度地图API功能
		var map = new BMap.Map('map');
		var poi = new BMap.Point(116.307852, 40.057031);
		map.centerAndZoom("铜仁", 12);
		map.addControl(new BMap.NavigationControl());
		//map.enableScrollWheelZoom(); //添加滚轮缩放地图功能
		//添加在地图上添加覆盖物的控件
		/* var overlays = [];
		var overlaycomplete = function(e) {
			overlays.push(e.overlay);
		}; */
		var overlays = [];
		var overlaycomplete = function(e) {
			//clearAll();
			overlays.push(e.overlay);
		}
		var styleOptions = {
			strokeColor : "red", //边线颜色。
			fillColor : "silver", //填充颜色。当参数为空时，圆形将没有填充效果。
			strokeWeight : 3, //边线的宽度，以像素为单位。
			strokeOpacity : 0.3, //边线透明度，取值范围0 - 1。
			fillOpacity : 0.6, //填充的透明度，取值范围0 - 1。
			strokeStyle : 'solid' //边线的样式，solid或dashed。
		}
		//实例化鼠标绘制工具
		var drawingManager = new BMapLib.DrawingManager(map, {
			isOpen : false, //是否开启绘制模式
			enableDrawingTool : true, //是否显示工具栏
			//enableCalculate:true, //开启面积计算
			drawingToolOptions : {
				anchor : BMAP_ANCHOR_TOP_RIGHT, //位置
				offset : new BMap.Size(5, 5), //偏离值
		        drawingModes : [   //绘制类型
				BMAP_DRAWING_CIRCLE,
				BMAP_DRAWING_POLYLINE,
				BMAP_DRAWING_POLYGON,
				BMAP_DRAWING_RECTANGLE 
         ]
			},
			//对于各个图形的样式
			circleOptions : styleOptions, //圆的样式
			polylineOptions : styleOptions, //线的样式 *
			polygonOptions : styleOptions, //多边形的样式
			rectangleOptions : styleOptions
		//矩形的样式
		});
		drawingManager.addEventListener('overlaycomplete', function(e) { //鼠标绘制完成后执行该事件
			clearAll();
			overlaycomplete(e);//先清除以前所有的再添加刚画的
			if (e.drawingMode == "circle") {
				var r = e.overlay.getRadius();
				var longitude = e.overlay.getCenter().lng;
				var latitude = e.overlay.getCenter().lat;
				$("#type").val("0");
				$("#circle").val(longitude + "," + latitude);
				$("#radius").val(r);

			} else if (e.drawingMode == "rectangle") {
				$("#type").val("1");
				$("#oneP").val(
						e.overlay.getBounds().getNorthEast().lng + ","
								+ e.overlay.getBounds().getNorthEast().lat);
				$("#twoP").val(
						e.overlay.getBounds().getSouthWest().lng + ","
								+ e.overlay.getBounds().getSouthWest().lat);
			} else if (e.drawingMode == "polygon") {
				var points = [];
				points = e.overlay.getPath();
				if (points.length > 6) {
					alert("多边形不能超过六个顶点");
				} else {
					$("#type").val("2");
					$("#pointNum").val(points.length);
					$("#oneP").val(points[0].lng + "," + points[0].lat);
					$("#twoP").val(points[1].lng + "," + points[1].lat);
					$("#threeP").val(points[2].lng + "," + points[2].lat);
					if (points.length > 3) {
						$("#fourP").val(points[3].lng + "," + points[3].lat);
					}
					if (points.length > 4) {
						$("#fiveP").val(points[4].lng + "," + points[4].lat);
					}
					if (points.length > 5) {
						$("#sixP").val(points[5].lng + "," + points[5].lat);
					}
				}
			} else {
				alert("不能识别该覆盖物");
			}
		});
		function clearAll() {
			for (var i = 0; i < overlays.length; i++) {
				map.removeOverlay(overlays[i]);
				//map.clearOverlays();
			}
			overlays.length = 0;
		}
		
		
		$("#submit_preinfo").click(function(){
			if($("#type").val()==""){
				swal({
					title : "请在地图上标明地理范围",
					type : "warning"
				});
				return false;
			}
		});
	</script>
	
	<script type="text/javascript">
	$(document).ready(function formValidator() {
			$('#addPreInfoForm').bootstrapValidator({
				feedbackIcons : {/*输入框不同状态，显示图片的样式*/
					valid : 'glyphicon glyphicon-ok',
					invalid : 'glyphicon glyphicon-remove',
					validating : 'glyphicon glyphicon-refresh'
				},
				/*生效规则：字段值一旦变化就触发验证*/
				live : 'enabled',
				/*当表单验证不通过时，该按钮为disabled*/
				submitButtons : 'button[type="submit"]',
				/*验证*/
				fields : {
					maxSpeed : {
						message : '',
						validators : {
							notEmpty : {/*非空提示*/
								message : '最大速度不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					startDate : {
						message : '',
						validators : {
							notEmpty : {
								message : '开始时间不能为空'
							}
						}
					},
					continueTime : {
						validators : {
							notEmpty : {
								message : '持续时间不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					endDate : {
						validators : {
							notEmpty : {
								message : '结束时间不能为空'
							}
						}
					},carUpper : {
						validators : {
							notEmpty : {
								message : '车辆上限不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},name : {
						validators : {
							notEmpty : {
								message : '名称不能为空'
							}
						}
					},num : {
						validators : {
							notEmpty : {
								message : '编号不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					}
					

				}
			});
		});
		/*日历刷新验证*/
		function refreshValidator(id, name) {
			$(id).data('bootstrapValidator').updateStatus(name,
					'NOT_VALIDATED', null).validateField(name);
		}
		</script>
</body>

</html>