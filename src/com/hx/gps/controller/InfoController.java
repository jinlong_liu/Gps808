package com.hx.gps.controller;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate4.HibernateOptimisticLockingFailureException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hx.gps.entities.Accident;
import com.hx.gps.entities.Accidenttype;
import com.hx.gps.entities.Blacklist;
import com.hx.gps.entities.Breakrule;
import com.hx.gps.entities.Check;
import com.hx.gps.entities.Company;
import com.hx.gps.entities.Driver;
import com.hx.gps.entities.Fare;
import com.hx.gps.entities.Insure;
import com.hx.gps.entities.IsuCache;
import com.hx.gps.entities.PageBean;
import com.hx.gps.entities.Role;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.Taxitype;
import com.hx.gps.entities.User;
import com.hx.gps.entities.Yearinspect;
import com.hx.gps.entities.tool.ComInfo;
import com.hx.gps.entities.tool.ComInfo2;
import com.hx.gps.entities.tool.DatatablesQuery;
import com.hx.gps.entities.tool.DatatablesView;
import com.hx.gps.entities.tool.DriverVO;
import com.hx.gps.entities.tool.InfoQuery;
import com.hx.gps.entities.tool.InfoQuery2;
import com.hx.gps.entities.tool.TaxiInfo;
import com.hx.gps.entities.tool.TaxiInfo2;
import com.hx.gps.entities.tool.Taxis;
import com.hx.gps.entities.tool.TimeTest;
import com.hx.gps.service.AddressService;
import com.hx.gps.service.InfoService;
import com.hx.gps.service.TaxiService;
import com.hx.gps.util.FarmConstant;

import net.coobird.thumbnailator.Thumbnails;

//信息管理
@Controller
public class InfoController {
	@Autowired
	private InfoService infoService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private TaxiService taxiService;
	// 日志记录
	private static Log logger = LogFactory.getLog(InfoController.class);
	/*
	 * @InitBinder public void initBinder(WebDataBinder binder) {
	 * SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"
	 * );
	 * 
	 * 
	 * dateFormat.setLenient(false);显示时间信息。并进行格式化。
	 * 
	 * 
	 * binder.registerCustomEditor(Timestamp.class, new CDateEditor(dateFormat,
	 * true)); // 参数2是否允许为空 }
	 */

	// 查询所有的用户,进入"用户管理"页面
	@RequestMapping("/getAllUser")
	public String getAllUser(Map<String, Object> map) {
		// 查询所有公司
		// List<Company> companies = infoService.getAllCompany();
		// -------------- ---------------------
		Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
			// 超级用户权限
			List<User> users = infoService.getAllUser();
			List<User> users2 = new ArrayList<>();
			// 洗掉内部用户
			String role = (String) subject.getSession().getAttribute("rolename");
			logger.info("当前用户权限" + role);
			if (!role.equals("开发人员")) {
				for (User user : users) {
					// 非内部用户洗掉洗掉数据。
					if (!user.getRole().getRemark().equals("开发人员")) {
						users2.add(user);
					}
				}
				map.put("users", users2);
			} else if (subject.isPermitted("bjcompy:get")) {
				companies = infoService.getAllCompanyBj();
				for (Company com : companies) {
					users2.addAll(infoService.getUserByCom(com));
				}
				map.put("users", users2);
			} else {
				map.put("users", users);
			}
		} else {
			// 当前公司管理员
			String name = (String) subject.getPrincipal();
			// int id=(int)session.getAttribute("compyid");
			Company company = infoService.getComByUser(name);
			// 当前公司管理员和用户，包括自己。
			List<User> users = infoService.getUserByCom(company);
			map.put("users", users);
			if (company != null) {
				companies.add(company);
			}
		}

		// ----------------------------------------------
		map.put("companies", companies);
		// 查询所有的角色
		List<Role> roles = infoService.getAllRoleByPower();
		map.put("roles", roles);
		return "WEB-INF/views/info/user2";
	}

	// 添加新的用户
	@RequestMapping("/saveUser")
	public String saveUser(User user, RedirectAttributes attr) {
		user.setState(0);// 使用状态
		user.setPeople(SecurityUtils.getSubject().getPrincipal().toString());// 上传人员，以后改为从session中获取
		user.setTime(new Timestamp(new Date().getTime()));// 上传时间,date转为Timestamp
		String result = null;
		if (infoService.saveUser(user)) {
			logger.info("【用户管理】添加" + user.toString());
			result = "操作成功";
		} else {
			result = "操作失败";
		}
		attr.addFlashAttribute("result", result);
		return "redirect:/getAllUser";

		/*
		 * try { infoService.saveUser(user);
		 * logger.info("【用户管理】添加"+user.toString()); result="操作成功"; } catch
		 * (Exception e) { result="操作失败";
		 * logger.error("【用户管理】添加出现异常"+e.getMessage());
		 * attr.addFlashAttribute("result", result); return
		 * "redirect:/getAllUser";
		 * 
		 * 
		 * 
		 * 
		 * } attr.addFlashAttribute("result", result); return
		 * "redirect:/getAllUser";
		 */
	}

	// ajax 修改用户状态
	@RequestMapping("/updateState")
	public void updateState(int id, String name, HttpServletResponse response) {
		User user = new User();
		user.setId(id);
		user.setName(name);
		user.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
		String result = null;
		if (infoService.updateState(user)) {
			result = "状态变更成功";
		} else {
			result = "状态变更失败";
		}
		// return "redirect:/getAllUser";
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().print(result);
		} catch (IOException e) {
			logger.error("更新用户状态异常" + e.getMessage());
			e.printStackTrace();
		}
	}

	// 更新用户
	@RequestMapping("/updateUser")
	public String updateUser(User user, String old, RedirectAttributes attr) {
		user.setState(0);// 使用状态
		user.setPeople(SecurityUtils.getSubject().getPrincipal().toString());// 上传人员，以后改为从session中获取
		user.setTime(new Timestamp(new Date().getTime()));// 上传时间,date转为Timestamp
		String result = null;
		if (infoService.updateUser(user, old)) {
			// 修改用户 。
			logger.info("【用户管理】更新" + user.toString());
			result = "操作执行成功";
			attr.addFlashAttribute("result", result);
		} else {
			result = "操作失败,原密码错误";
			attr.addFlashAttribute("result", result);
		}

		return "redirect:/getAllUser";
	}

	// 删除用户deleteUser
	@RequestMapping("/deleteUser")
	public String deleteUser(Integer id, RedirectAttributes attr) {
		User user = new User();
		user.setId(id);
		String result = null;
		try {
			infoService.deleteUser(user);
			logger.info("【用户管理】删除" + user.toString());
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			attr.addFlashAttribute("result", result);
			logger.error("【用户管理】删除出现异常" + e.getMessage());
			return "redirect:/getAllUser";
		}
		attr.addFlashAttribute("result", result);
		return "redirect:/getAllUser";
	}

	// 查询所有的角色,进入"角色管理"页面
	@RequestMapping("/getAllRole")
	public String getAllRole(Map<String, Object> map) {
		// 查询所有公司
		List<Company> companies = infoService.getAllCompany();
		map.put("companies", companies);
		// 查询所有的角色
		List<Role> roles = infoService.getAllRoleByPower();
		map.put("roles", roles);
		return "WEB-INF/views/info/role2";
	}

	// 新增安全事故
	@RequestMapping("/saveAccident")
	public String saveAccident(Accident accident, HttpServletRequest request) {
		String result = null;
		try {
			infoService.saveAccident(accident);
			logger.info("【安全事故管理】添加" + accident.toString());
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			request.setAttribute("result", result);
			logger.error("【安全事故管理】添加出现异常" + e.getMessage());
			return "forward:/listAccidents";
		}
		request.setAttribute("result", result);
		return "forward:/listAccidents";
	}

	// 新增角色saveRole
	@RequestMapping("/saveRole")
	public String saveRole(Role role, RedirectAttributes attr) {
		String result = null;
		try {
			infoService.saveRole(role);
			logger.info("【角色管理】添加" + role.toString());
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			attr.addFlashAttribute("result", result);
			logger.error("【角色管理】添加出现异常" + e.getMessage());
			return "redirect:/getAllRole";
		}
		attr.addFlashAttribute("result", result);
		return "redirect:/getAllRole";
	}

	// 更新角色
	@RequestMapping("/updateRole")
	public String updateRole(Role role, RedirectAttributes attr) {
		String result = null;
		try {
			infoService.updateRole(role);
			logger.info("【角色管理】更新" + role.toString());
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			attr.addFlashAttribute("result", result);
			logger.error("【更新角色】更新出现异常" + e.getMessage());
			return "redirect:/getAllRole";
		}
		attr.addFlashAttribute("result", result);
		return "redirect:/getAllRole";
	}

	// 删除角色
	@RequestMapping("/deleteRole")
	public String deleteRole(Role role, RedirectAttributes attr) {
		String result = null;
		try {
			infoService.deleteRole(role);
			logger.info("【角色管理】删除" + role.toString());
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			attr.addFlashAttribute("result", result);
			logger.error("【角色管理】删除出现异常" + e.getMessage());
			return "redirect:/getAllRole";
		}
		attr.addFlashAttribute("result", result);
		return "redirect:/getAllRole";
	}

	// 查询所有的公司信息,进入"公司信息"页面
	@RequestMapping("/getAllCompany")
	public String getAllCompany(Map<String, Object> map) {
		// List<Company> companies = infoService.getAllCompany();
		// --------------按权限显示车辆---------------------
		Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
		} else if (subject.isPermitted("bjcompy:get")) {
			companies = infoService.getAllCompanyBj();
		} else {
			String name = (String) subject.getPrincipal();

			// int id=(int)session.getAttribute("compyid");
			Company company = infoService.getComByUser(name);
			if (company != null) {
				companies.add(company);
			}
		}

		// --------------------------------------
		// companies.remove(0);//不在页面显示"所有公司"
		map.put("companies", companies);
		return "WEB-INF/views/info/company2";
	}

	// ajax 查询公司信息。
	@RequestMapping("/getAllCompanyAjax")
	public void getAllCompanyAjax(HttpServletResponse response) {
		// 查询所有公司
		// List<Company> companies = infoService.getAllCompany();
		// --------------按权限显示车辆---------------------
		Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
		} else if (subject.isPermitted("bjcompy:get")) {
			companies = infoService.getAllCompanyBj();
		} else {
			String name = (String) subject.getPrincipal();

			// int id=(int)session.getAttribute("compyid");
			Company company = infoService.getComByUser(name);
			if (company != null) {
				companies.add(company);
			}
		}

		// --------------------------------------
		// 转换成jason格式
		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().print(mapper.writeValueAsString(companies));
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("实时监控出现异常" + e.getMessage());
		}
	}

	// 新增公司信息
	@RequestMapping("/saveCompany")
	public String saveCompany(Company company, HttpServletRequest request) {
		// logger.info("saveCompany: " + company);
		String result = null;
		company.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
		company.setTime(new Timestamp(new Date().getTime()));
		try {
			if (infoService.haveCompany(company)) {
				result = "当前公司已存在，勿重复添加";
			} else {
				infoService.saveCompany(company);

				result = "操作执行成功";
			}
		} catch (Exception e) {
			result = "操作失败";
			request.setAttribute("result", result);
			logger.error("【新增公司】异常" + e.getMessage());
			return "forward:/getAllCompany";

		}
		request.setAttribute("result", result);
		return "forward:/getAllCompany";
	}

	// 更新公司信息
	@RequestMapping("/updateCompany")
	public String updateCompany(Company company, HttpServletRequest request) {
		// HttpSession session=request.getSession();
		company.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
		company.setTime(new Timestamp(new Date().getTime()));
		String str = null;
		try {
			infoService.updateCompany(company);
			str = "操作执行成功";
		} catch (HibernateOptimisticLockingFailureException e) {
			logger.error("更新公司信息" + company.getName() + "失败" + "该公司已被删除");
			str = "您要操作的对象已被删除";
			request.setAttribute("result", str);
			return "forward:/getAllCompany";
		} catch (Exception e) {
			logger.error("更新公司信息" + company.getName() + "失败" + "捕获到未知异常" + e.getMessage());
			str = "出现未知错误";
			request.setAttribute("result", str);
			return "forward:/getAllCompany";
		}
		request.setAttribute("result", str);
		return "forward:/getAllCompany";
	}

	// 删除公司信息
	@RequestMapping("/deleteCompany")
	public String deleteCompany(Company company, HttpServletRequest request, HttpServletResponse response) {
		String result = null;
		try {
			infoService.deleteCompany(company);
			result = "操作执行成功";
			logger.info("【公司管理】删除" + company.toString());
		} catch (HibernateOptimisticLockingFailureException e) {
			logger.error("删除公司信息" + company.getName() + "失败，操作前已被删除");
			result = "您要操作的对象已被删除";
			request.setAttribute("result", result);
			return "forward:/getAllCompany";
		} catch (Exception e) {
			logger.error("删除公司信息" + company.getName() + "失败,捕获到未知异常：" + e.getMessage());
			result = "出现未知错误";
			request.setAttribute("result", result);
			return "forward:/getAllCompany";
		}
		// request.setAttribute("result",result);
		// ObjectMapper mapper = new ObjectMapper();
		/*
		 * ajax版本 response.setContentType("text/html;charset=UTF-8");
		 * response.setCharacterEncoding("UTF-8"); try {
		 * response.getWriter().print(result); } catch (IOException e) {
		 * logger.error("删除公司" + company.getName() + "失败，捕获到异常" +
		 * e.getMessage()); e.printStackTrace(); }
		 */
		request.setAttribute("result", result);
		return "forward:/getAllCompany";
	}

	// 删除安全事故信息
	@RequestMapping("/deleteAccident")
	public String deleteAccident(Accident accident, HttpServletRequest request, HttpServletResponse response) {
		String result = null;
		try {
			infoService.deleteAccident(accident);
			result = "操作执行成功";
			logger.info("【安全事故管理】删除" + accident.toString());
		} catch (HibernateOptimisticLockingFailureException e) {
			logger.error("删除事故信息" + accident.toString() + "失败，操作前已被删除");
			result = "您要操作的对象已被删除";
			request.setAttribute("result", result);
			return "forward:/enterAccidents";
		} catch (Exception e) {
			logger.error("删除事故信息" + accident.toString() + "失败,捕获到未知异常：" + e.getMessage());
			result = "出现未知错误";
			request.setAttribute("result", result);
			return "forward:/listAccidents";
		}
		request.setAttribute("result", result);
		return "forward:/listAccidents";
	}

	// 进入"车辆管理界面"
	@RequestMapping("/enterTaxi")
	public String enterTaxi(Map<String, Object> map) {
		// 查出所有公司,显示在页面下拉框

		Subject subject = SecurityUtils.getSubject();
		Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
		} else if (subject.isPermitted("bjcompy:get")) {
			companies = infoService.getAllCompanyBj();
		} else {

			// int id=(int)session.getAttribute("compyid");
			String name = (String) subject.getPrincipal();
			Company company = infoService.getComByUser(name);
			if (company != null) {
				companies.add(company);
			}

		}

		// List<Company> companies = infoService.getAllCompany();
		// companies.remove(0);//不在页面显示"所有公司"
		map.put("companies", companies);

		// 查询所有的车型，显示到"新增车辆"弹框的下拉框里
		List<Taxitype> taxitypes = infoService.getAllTaxiType();
		map.put("taxitypes", taxitypes);
		return "WEB-INF/views/info/taxi2";
	}

	// 进入事故管理界面
	// 进入"车辆管理界面"
	@RequestMapping("/enterAccident")
	public String enterAccident(Map<String, Object> map, HttpServletRequest request) {
		// 查出所有公司,显示在页面下拉框

		Subject subject = SecurityUtils.getSubject();
		Session session = subject.getSession();
		HttpSession httpSession = request.getSession();
		// 查出所有车辆
		List<Taxi> taxis = new ArrayList<Taxi>();
		if (subject.isPermitted("allcompy:get")) {
			taxis = infoService.getAllTaxi();
		} else if (subject.isPermitted("bjcompy:get")) {
			List<Company> companies = new ArrayList<>();
			companies = infoService.getAllCompanyBj();
			if (companies != null) {
				for (Company company : companies) {
					InfoQuery infoQuery = new InfoQuery();
					infoQuery.setComId(company.getId());
					taxis.addAll(infoService.getTaxi(infoQuery));
				}
			}
		} else {
			String name = (String) subject.getPrincipal();
			Company company = infoService.getComByUser(name);
			if (company != null) {
				InfoQuery infoQuery = new InfoQuery();
				infoQuery.setComId(company.getId());
				taxis = infoService.getTaxi(infoQuery);
			}
		}
		if (taxis.size() > 0) {
		} else {
			taxis.add(new Taxi("未查询到车辆信息", "大众"));
		}
		// 查询事故类型
		map.put("taxis", taxis);
		if (infoService.getAccidentType() != null) {
			List<Accidenttype> accidentTypes = infoService.getAccidentType();
			map.put("taxitypes", accidentTypes);
			httpSession.setAttribute("taxitype", accidentTypes);
		}
		httpSession.setAttribute("taxi", taxis);
		return "WEB-INF/views/info/accident";
	}

	// 查询事故信息

	@RequestMapping("/listAccidents")
	public String listAccidents(Map<String, Object> map, InfoQuery query) {
		// 根据TaxiNum找到相应的taxiId
		if (!("").equals(query.getTaxiNum())) {
			Integer taxiId = infoService.getTaxiIdBytaxiNum(query);
			query.setTaxiId(taxiId);
		}
		// 查询所有事故
		List<Accident> accidents = infoService.listAccidents(query);
		map.put("accidents", accidents);
		return "WEB-INF/views/info/accident";
	}

	// 查询车辆
	@RequestMapping("/getTaxi")
	public String getTaxi(Map<String, Object> map, InfoQuery infoQuery) {
		// 查询时判断是否为普通用户，如果是则无论选择不选择公司都显示自己公司的信息。
		Subject subject = SecurityUtils.getSubject();
		List<Taxi> taxis = new ArrayList<>();
		// Session session = subject.getSession();
		// List<Company> companies=new ArrayList<Company>();
		if (subject.isPermitted("bjcompy:get")) {
			List<Company> companies = new ArrayList<>();
			companies = infoService.getAllCompanyBj();
			if (companies != null) {
				for (Company company : companies) {
					infoQuery.setComId(company.getId());
					taxis.addAll(infoService.getTaxi(infoQuery));
				}
			}
		} else if (subject.isPermitted("allcompy:get")) {
			infoQuery.setComId(0);
			taxis = infoService.getTaxi(infoQuery);
		} else {
			String name = (String) subject.getPrincipal();
			Company company = infoService.getComByUser(name);
			if (company != null) {
				infoQuery.setComId(company.getId());
				taxis = infoService.getTaxi(infoQuery);
			}
		}
 
		map.put("taxis", taxis);

		// 查出所有公司,显示在页面下拉框
		// List<Company> companies = infoService.getAllCompany();
		// --------------------按权限显示车辆---------------------
		// Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
		} else if (subject.isPermitted("bjcompy:get")) {
			companies = infoService.getAllCompanyBj();
		} else {
			String name = (String) subject.getPrincipal();

			// int id=(int)session.getAttribute("compyid");
			Company company = infoService.getComByUser(name);
			if (company != null) {
				companies.add(company);
			}
		}

		// --------------------------------------
		// companies.remove(0);//不在页面显示"所有公司"
		map.put("companies", companies);

		// 查询所有的车型，显示到"新增车辆"弹框的下拉框里
		List<Taxitype> taxitypes = infoService.getAllTaxiType();
		map.put("taxitypes", taxitypes);

		// 查询框的数据回显
		map.put("taxiNumSelect", infoQuery.getTaxiNum());
		map.put("masterNameSelect", infoQuery.getMasterName());
		map.put("isuNumSelect", infoQuery.getIsuNum());
		map.put("isuPhoneSelect", infoQuery.getIsuPhone());

		return "WEB-INF/views/info/taxi2";
	}

	/**
	 * 查询车辆
	 * 
	 * 
	 * @author 高杨
	 * @date 2018年4月26日 下午3:21:41
	 * @param infoQuery
	 * @return
	 */
	@RequestMapping("/queryTaxi")
	@ResponseBody
	public PageBean<Taxi> queryTaxi(InfoQuery infoQuery) {
		System.out.println("----------------------------------------------------");
		System.out.println(infoQuery);
		// 查询时判断是否为普通用户，如果是则无论选择不选择公司都显示自己公司的信息。
		List<Taxi> taxis = infoService.getTaxi(infoQuery);
		PageBean<Taxi> pageBean = new PageBean<Taxi>();
		pageBean.setTotal(infoService.getTaxiNumber(infoQuery));
		pageBean.setRows(taxis);
		return pageBean;
	}

	// 进入"车辆缓存表界面"
	@RequestMapping("/enterIsuCache")
	public ModelAndView enterIsuCache() {
		ModelAndView mav = new ModelAndView();
		List<IsuCache> data = infoService.getAllIsuCache();
		mav.addObject("datas", data);
		mav.setViewName("WEB-INF/views/info/isuCache");
		return mav;
	}

	// 新增车辆
	@RequestMapping("/saveTaxi")
	public String saveTaxi(Taxi taxi, HttpServletRequest request) {
		String result = null;

		// 操作人员
		taxi.setOperationer(SecurityUtils.getSubject().getPrincipal().toString());
		// taxi.setAddDate(new Timestamp(new Date().getTime()));
		try {
			result = infoService.saveTaxi(taxi);
		} catch (Exception e) {
			result = "操作失败";
			request.setAttribute("result", result);
			logger.error("【车辆管理】新增出现异常" + e.getMessage());
			return "forward:/getTaxi?&comId=0" + "&isuPhone=" + "";
		}
		request.setAttribute("result", result);
		// return "forward:/enterTaxi";
		return "forward:/getTaxi?&comId=0" + "&isuPhone=" + "";
	}

	// 批量上传车辆
	@RequestMapping(value = "/uploadTaxi")
	public String upload(@RequestParam(value = "file", required = false) MultipartFile file, HttpServletRequest request,
			ModelMap model) {
		// 上传部分
		String path = request.getSession().getServletContext().getRealPath("upload");
		String fileName = file.getOriginalFilename();
		File targetFile = new File(path, fileName);
		if (!targetFile.exists()) {
			targetFile.mkdirs();
		}
		// 保存至服务端
		try {
			file.transferTo(targetFile);
			logger.info("保存批量数据到服务端");
		} catch (Exception e) {
			logger.error("批量文件保存异常" + file.getName() + e.getMessage());
			e.printStackTrace();
		}
		model.addAttribute("fileUrl", request.getContextPath() + "/upload/" + fileName);
		// 处理数据
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		int rowCount = 0;
		int success = 0;
		int retry = 0;
		try {
			// 修改转发路径
			List<Integer> results = infoService.uploadTaxi(path + "/" + fileName, fmt, rowCount, success, retry);
			rowCount = results.get(0);
			success = results.get(1);
			retry = results.get(2);
			logger.info("批量导入完毕！总数：" + (rowCount - 2) + " 成功个数：" + success + " 失败个数：" + (rowCount - 2 - success)
					+ "重试个数：" + retry);
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("result", "上传失败！");
			logger.error("批量导入失败！" + "出现异常" + e.getMessage());
			return "forward:/enterTaxi";
		}
		request.setAttribute("result", "导入完毕！总数：" + (rowCount - 2) + " 成功个数：" + success + " 失败个数："
				+ (rowCount - 2 - success) + "重试个数：" + retry);
		return "forward:/enterTaxi";
	}

	// （莱芜特有，带在线编辑功能）
	@RequestMapping(value = "/uploadAndEditTaxi")
	public String uploadAndEditTaxi(@RequestParam(value = "file", required = false) MultipartFile file,
			HttpServletRequest request, ModelMap model) {
		FarmConstant.taxis.clear();
		List<Taxi> taxis = new ArrayList<>();
		// 上传部分
		String path = request.getSession().getServletContext().getRealPath("upload");
		String fileName = file.getOriginalFilename();
		File targetFile = new File(path, fileName);
		if (!targetFile.exists()) {
			targetFile.mkdirs();
		}
		// 保存至服务端
		try {
			file.transferTo(targetFile);
			logger.info("保存批量数据到文件" + file.getName() + ",大小：" + file.getSize());
		} catch (Exception e) {
			logger.error("批量文件保存异常" + file.getName() + e.getMessage());
			e.printStackTrace();
			return "forward:/editTaxi";
		}
		model.addAttribute("fileUrl", request.getContextPath() + "/upload/" + fileName);
		// 处理数据
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		int rowCount = 0;
		try {
			// 加载到本地，编辑车辆信息
			taxis = infoService.uploadAndEditTaxi(path + "/" + fileName, fmt, rowCount);
			logger.info("【批量导入】加载完毕" + fileName);
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("result", "导入失败！请检查文件格式！");
			logger.error("批量导入失败！" + "出现异常" + e);
			return "forward:/editTaxi";
		}
		request.setAttribute("taxiList", taxis);
		return "forward:/editTaxi";
	}

	// 在线编辑界面
	@RequestMapping(value = "/editTaxi")
	public String editTaxi() {

		return "WEB-INF/views/info/editTaxi";
	}

	// 在线编辑界面(重试)
	@RequestMapping(value = "/reEditTaxi")
	public String reEditTaxi(HttpServletRequest request) {
		request.setAttribute("taxiList", FarmConstant.taxis);
		return "WEB-INF/views/info/editTaxi";
	}

	// 批量保存车辆
	@RequestMapping("/saveEditTaxis")
	@ResponseBody
	public List<String> saveEditTaxis(Taxis taxi, HttpServletRequest request) {
		System.err.println("开始保存" + taxi.getTaxis().size());
		List<String> str = new ArrayList<>();
		String result = null;
		// taxi.setAddDate(new Timestamp(new Date().getTime()));
		try {
			result = infoService.saveEditTaxis(taxi.getTaxis());
			str.add(result);
		} catch (Exception e) {
			result = "操作失败";
			return str;
		}
		request.setAttribute("result", result);
		return str;

	}

	// 更新车辆
	@RequestMapping("/updateTaxi")
	public String updateTaxi(Taxi taxi, HttpServletRequest request) {
		int state = 0;
		int type = 2;
		// 比较两个手机号
		String isu = taxi.getIsuNum();// 新
		String isu2 = taxiService.getIsuByid(taxi.getId());// 旧
		taxi.setOperationer(SecurityUtils.getSubject().getPrincipal().toString());
		taxi.setAddDate(new Timestamp(new Date().getTime()));
		String result = null;
		try {
			infoService.updateTaxi(taxi);
			result = "操作执行成功";
			state = 1;
		} catch (HibernateOptimisticLockingFailureException e) {
			result = "您要操作的对象已被删除";
			logger.error("更新车辆信息失败，车辆已被删除");
			request.setAttribute("result", result);
			return "forward:/getTaxi?&comId=0" + "&isuPhone=" + "";
		}
		// catch(ConstraintViolationException e) {
		// result="您要更新的手机号已存在";//hibenate对jdbc的异常做了转换。
		// }
		catch (DataAccessException e) {
			result = "您要更新的手机号已被使用";
			logger.error("更新车辆信息失败，终端号已被使用");
			request.setAttribute("result", result);
			return "forward:/getTaxi?&comId=0" + "&isuPhone=" + "";
		} catch (Exception e) {
			logger.error("更新车辆失败,捕获到异常" + e.getMessage());
			result = "出现未知错误，请重新打开本界面";
			request.setAttribute("result", result);
			return "forward:/getTaxi?&comId=0" + "&isuPhone=" + "";
		}
		// 如果是更新手机号，则同步mongo。
		if (state == 1) {
			if (isu != isu2) {
				try {
					infoService.updateIsu(isu, isu2);
					logger.info("更新车辆" + isu2 + "的终端号为：" + isu);
				} catch (Exception e) {
					result = "操作失败，已加入缓存表";
					logger.error("更新车辆" + isu2 + "终端号失败,已加入缓存表");
					infoService.addToIsucache(taxi, isu2, type);
					request.setAttribute("result", result);
					return "forward:/getTaxi?&comId=0" + "&isuPhone=" + "";
				}
			}
		}
		request.setAttribute("result", result);

		// 查询修改的这一条记录
		return "forward:/getTaxi?&comId=0" + "&isuPhone=" + ""
		/*
		 * +"&masterName="+"" +"&isuNum="+""
		 */;
	}

	// 删除车辆
	/*
	 * @RequestMapping("/deleteTaxi") public String deleteTaxi(Taxi
	 * taxi,HttpServletRequest request){
	 * 
	 * 
	 * String result=null; try { infoService.deleteTaxi(taxi); result="操作执行成功";
	 * } catch (HibernateOptimisticLockingFailureException e) {
	 * result="您要操作的对象已被删除"; } catch (Exception e) { result="出现未知错误"; }
	 * request.setAttribute("result",result); return "forward:/enterTaxi"; }
	 */
	@RequestMapping("/deleteTaxi")
	public String deleteTaxi(Taxi taxi, @RequestParam("taxiNumDelete") String taxiNumDelete, RedirectAttributes attr) {
		// logger.info("进来啦");
		int type = 3;
		int state = 0;
		String result = null;
		String isu;
		try {
			isu = taxiService.getIsuByid(taxi.getId());
		} catch (Exception e1) {
			result = "该对象已被删除";
			logger.error("删除车辆失败， 该车辆已不存在");
			attr.addFlashAttribute("result", result);
			return "redirect:/getTaxi?taxiNum=" + taxiNumDelete + "&masterName=" + "&isuNum=" + "&comId=0"
					+ "&isuPhone=" + "";
		}
		// 补上手机号
		taxi.setIsuNum(isu);
		try {
			infoService.deleteTaxi(taxi);
			result = "操作执行成功";
			state = 1;
		} catch (HibernateOptimisticLockingFailureException e) {
			logger.error("删除车辆失败，终端号" + isu + "该车辆已不存在");
			result = "您要操作的对象已被删除";
			attr.addFlashAttribute("result", result);
			return "redirect:/getTaxi?taxiNum=" + taxiNumDelete + "&masterName=" + "&isuNum=" + "&comId=0"
					+ "&isuPhone=" + "";
		} catch (Exception e) {
			logger.error("删除车辆失败，终端号：" + isu + "捕获到异常" + e.getMessage());
			result = "出现未知错误";
			attr.addFlashAttribute("result", result);
			return "redirect:/getTaxi?taxiNum=" + taxiNumDelete + "&masterName=" + "&isuNum=" + "&comId=0"
					+ "&isuPhone=" + "";
		}
		// request.setAttribute("result",result);
		// ObjectMapper mapper = new ObjectMapper();
		if (state == 1) {
			try {
				infoService.deleteIsu(isu);
				logger.info("删除车辆成功，终端号：" + isu);
			} catch (Exception e) {
				result = "删除失败，已加入缓存表自动重试中..";
				attr.addFlashAttribute("result", result);
				taxi.setAddDate(new Timestamp(new Date().getTime()));
				infoService.addToIsucache(taxi, type);
				return "redirect:/getTaxi?taxiNum=" + taxiNumDelete + "&masterName=" + "&isuNum=" + "&comId=0"
						+ "&isuPhone=" + "";
			}
		}
		/*
		 * ajax版本 response.setContentType("text/html;charset=UTF-8");
		 * response.setCharacterEncoding("UTF-8"); try {
		 * response.getWriter().print(result); } catch (IOException e) {
		 * e.printStackTrace(); logger.error("执行删除车辆响应异常" + e.getMessage()); }
		 */
		attr.addFlashAttribute("result", result);
		return "redirect:/getTaxi?taxiNum=" + taxiNumDelete + "&masterName=" + "&isuNum=" + "&comId=0" + "&isuPhone="
				+ "";
	}

	// 查询指定公司的所有车辆
	@RequestMapping("/getTaxiByCom")
	public void getTaxiByCom(int id, HttpServletResponse response) {
		// 获取前台的公司id。

		// 准备数组接收终端号
		String[] isu = infoService.getTaxiByCom(id);

		// 调用service获取所有车辆
		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		// 格式转换。
		try {
			response.getWriter().print(mapper.writeValueAsString(isu));
		} catch (IOException e) {
			logger.error("查询指定公司车辆响应异常" + e.getMessage());
		}

	}

	// 查询所有车辆类型，并进入"车辆类型"页面
	@RequestMapping("/getAllTaxiType")
	public String getAllTaxiType(Map<String, Object> map) {
		List<Taxitype> taxitypes = infoService.getAllTaxiType();
		map.put("taxitypes", taxitypes);
		return "WEB-INF/views/info/taxiType";
	}

	// 新增车辆类型
	@RequestMapping("/saveTaxiType")
	public String saveTaxiType(Taxitype taxitype, HttpServletRequest request) {
		taxitype.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
		taxitype.setTime(new Timestamp(new Date().getTime()));
		String result = null;
		boolean b = infoService.saveTaxiType(taxitype);
		if (b) {
			logger.info("新增车辆类型" + taxitype.toString());
			result = "操作执行成功";
		} else {
			result = "操作失败";
			logger.error("【车辆类型】新增失败");
		}
		request.setAttribute("result", result);
		return "forward:/getAllTaxiType";
	}

	// 更新车辆类型
	@RequestMapping("/updateTaxiType")
	public String updateTaxiType(Taxitype taxitype, HttpServletRequest request) {
		// 添加用户登录后该值由session中取出
		taxitype.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
		taxitype.setTime(new Timestamp(new Date().getTime()));
		String result = null;
		try {
			infoService.updateTaxiType(taxitype);
			logger.info("【车辆类型】更新" + taxitype.toString());
			result = "操作执行成功 ";
		} catch (HibernateOptimisticLockingFailureException e) {
			logger.error("车辆类型已被删除，无法更新" + taxitype.toString());
			result = "您要操作的对象早已被删除";
			request.setAttribute("result", result);
			return "forward:/getAllTaxiType";
		} catch (Exception e) {
			logger.error("更新车辆类型出现异常" + e.getMessage());
			result = "出现未知错误";
			request.setAttribute("result", result);
			return "forward:/getAllTaxiType";
		}
		request.setAttribute("result", result);
		return "forward:/getAllTaxiType";
	}

	// 删除车辆类型
	@RequestMapping("/deleteTaxiType")
	public String deleteTaxiType(Taxitype taxitype, HttpServletRequest request) {
		String result = null;
		try {
			infoService.deleteTaxiType(taxitype);
			logger.info("删除车辆类型" + taxitype.toString());
			result = "操作执行成功";
		} catch (HibernateOptimisticLockingFailureException e) {
			logger.error("删除车辆类型失败，已不存在" + taxitype.toString());
			result = "您要操作的对象已被删除";
			request.setAttribute("result", result);
			return "forward:/getAllTaxiType";
		} catch (Exception e) {
			logger.error("执行删除车辆类型出现异常" + e.getMessage());
			result = "出现未知错误";
			request.setAttribute("result", result);
			return "forward:/getAllTaxiType";
		}
		request.setAttribute("result", result);
		return "forward:/getAllTaxiType";
	}

	// 进入"从业人员"界面
	@RequestMapping("/enterDriver")
	public String enterDriver(Map<String, Object> map) {
		// 查询所有的车牌号，显示在下拉框里
		List<Taxi> taxis = infoService.getAllTaxi();
		map.put("taxis", taxis);

		// 查出所有公司,显示在页面下拉框
		// List<Company> companies = infoService.getAllCompany();
		// --------------按权限显示车辆---------------------
		Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
		} else if (subject.isPermitted("bjcompy:get")) {
			companies = infoService.getAllCompanyBj();
		} else {
			String name = (String) subject.getPrincipal();

			// int id=(int)session.getAttribute("compyid");
			Company company = infoService.getComByUser(name);
			if (company != null) {
				companies.add(company);

			}
		}

		// --------------------------------------
		// companies.remove(0);//不在页面显示"所有公司"
		map.put("companies", companies);

		return "WEB-INF/views/info/driver";
	}

	// 查询"从业人员"
	@RequestMapping("/getDriver")
	public String getDriver(Map<String, Object> map, InfoQuery infoQuery) {

		// 根据TaxiNum找到相应的taxiId
		int taxiId = 0;
		if (!infoQuery.getTaxiNum().equals("")) {
			taxiId = infoService.getTaxiIdBytaxiNum(infoQuery);
		}
		infoQuery.setTaxiId(taxiId);

		// 按条件查询出从业人员
		List<Driver> drivers = infoService.getDriver(infoQuery);
		map.put("drivers", drivers);

		// 查询所有的车牌号，显示在下拉框里
		List<Taxi> taxis = infoService.getAllTaxi();
		map.put("taxis", taxis);

		// 查出所有公司,显示在页面下拉框
		// List<Company> companies = infoService.getAllCompany();
		// --------------按权限显示车辆---------------------
		Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
		} else if (subject.isPermitted("bjcompy:get")) {
			companies = infoService.getAllCompanyBj();
		} else {
			String name = (String) subject.getPrincipal();

			// int id=(int)session.getAttribute("compyid");
			Company company = infoService.getComByUser(name);
			if (company != null) {
				companies.add(company);

			}
		}

		// --------------------------------------
		// companies.remove(0);//不在页面显示"所有公司"
		map.put("companies", companies);

		// 数据回显
		map.put("taxiNumSelect", infoQuery.getTaxiNum());
		map.put("quaNumSelect", infoQuery.getQuaNum());
		map.put("dnameSelect", infoQuery.getDname());
		return "WEB-INF/views/info/driver";
	}

	/**
	 * @Function: InfoController.java
	 * @Description: 后台分页查询从业人员
	 *
	 * @param:分页条件，车牌号，姓名，证件号
	 * @return：分页包装类结果
	 * @throws：异常描述
	 *
	 * @version: v1.0.0
	 * @author: AN
	 * @date: 2018年5月7日 上午11:21:44
	 *
	 */
	@RequestMapping("/listDrivers")
	public @ResponseBody DatatablesView<DriverVO> listDrivers(DatatablesQuery datatablesQuery, String taxiNum,
			String dname, String quaNum) {

		// System.out.println(datatablesQuery.getDraw());
		// System.out.println(datatablesQuery.getStart());
		// System.out.println(datatablesQuery.getLength());
		// System.out.println(datatablesQuery.getOrderColumn());
		// System.out.println(datatablesQuery.getOrderType());
		// System.out.println(datatablesQuery.getSearch());
		try {
			System.out.println(taxiNum);
			System.out.println(dname);
			System.out.println(quaNum);
			InfoQuery infoQuery = new InfoQuery();
			if (taxiNum != null) {
				infoQuery.setTaxiNum(taxiNum);
			} else {
				infoQuery.setTaxiNum("");

			}
			if (dname != null) {
				infoQuery.setDname(dname);
			} else {
				infoQuery.setDname("");

			}
			if (quaNum != null) {
				infoQuery.setQuaNum(quaNum);
			} else {
				infoQuery.setQuaNum("");

			}

			if (datatablesQuery.getOrderColumn() == null) {
				datatablesQuery.setOrderColumn("");

			}
			if (datatablesQuery.getSearch() == null) {
				datatablesQuery.setSearch("");
			}
			//毕节公司显示的时候需要审核
			Subject subject = SecurityUtils.getSubject();
			if (!subject.isPermitted("allcompy:get")&&!subject.isPermitted("bjcompy:get")){
				String name = (String) subject.getPrincipal();
				Company company = infoService.getComByUser(name);
				if(company.getRemark().equals("bj")){
					//需要审核
					infoQuery.setState("1");
				}
			}
			DatatablesView<DriverVO> datatablesView = infoService.listDrivers(datatablesQuery, infoQuery);
			System.out.println("ddd"+datatablesView.toString());
			return datatablesView;
		} catch (Exception e) {
			return null;
		}

	}

	/**
	 * 照片处理
	 * 
	 * @author 高杨
	 * @date 2018年3月14日 上午9:24:40
	 * @param photo
	 *            照片文件
	 * @param request
	 * @param size
	 *            转换的照片尺寸
	 * @param prefix
	 *            照片名称前缀
	 * @return
	 */
	private String photoHandler(MultipartFile photo, HttpServletRequest request, int size, String prefix) {
		String newName = "";
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		String formatTime = df.format(new Date());

		String photoName = formatTime + "." + photo.getOriginalFilename().split("\\.")[1];
		String path = request.getSession().getServletContext().getRealPath("/uploadPhotos");
		// System.out.println("照片路径：" + path);
		File dir = new File(path, photoName);
		if (!dir.exists()) {
			dir.mkdirs();
		}

		// MultipartFile自带的解析方法
		try {
			newName = prefix + photoName;
			photo.transferTo(dir);
			Thumbnails.of(path + "/" + photoName).size(size, size).toFile(path + "/" + newName);

			// product.setProductImage("new" + photoName);
			// productService.insertProduct(product);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/uploadPhotos/" + newName;
	}

	// 新增从业人员
	// @RequestMapping("/addDriver")
	// public @ResponseBody String saveDriver(MultipartFile photo, Driver
	// driver,
	// @RequestParam("taxiNumSave") String taxiNumSave, RedirectAttributes attr,
	// HttpServletRequest request) {
	// String mainPhoto = photoHandler(photo, request, 220, "new");

	/**
	 * 添加司机信息-带照片
	 * 
	 * @return Map<String,Object>
	 * @author 高杨
	 * @date 2018年10月12日上午9:23:59
	 */
	@RequestMapping("/addDriver")
	public @ResponseBody Map<String, Object> saveDriver(MultipartFile photo, Driver driver,
			HttpServletRequest request) {
		Map<String, Object> result = new HashMap<String, Object>();
		System.out.println("照片名称：" + photo.getOriginalFilename());
		String photoAddr = photoHandler(photo, request, 220, "new");
//=======
//	public @ResponseBody String saveDriver(Driver driver, @RequestParam("taxiNumSave") String taxiNumSave,
//			RedirectAttributes attr) {
//
//>>>>>>> .merge-right.r708
		/*
		 * 老版本
		 * driver.setPeople(SecurityUtils.getSubject().getPrincipal().toString()
		 * ); driver.setTime(new Timestamp(new Date().getTime()));
		 * infoService.saveDriver(driver); // return "redirect:/enterDriver";
		 * return "redirect:/getDriver?taxiNum=" + taxiNumSave + "&quaNum=" +
		 * "&dname=";
		 */
		driver.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
		driver.setTime(new Timestamp(new Date().getTime()));
		driver.setPhotoAddr(photoAddr);
		//毕节自己公司的需要审核
		Subject subject = SecurityUtils.getSubject();
		//先判断是否有管理权限
		driver.setState(true);
		if (!subject.isPermitted("allcompy:get")&&!subject.isPermitted("bjcompy:get")){
			String name = (String) subject.getPrincipal();
			Company company = infoService.getComByUser(name);
			if(company.getRemark().equals("bj")){
				//需要审核
				driver.setState(false);
			}
		}
		try {
			infoService.saveDriver(driver);
			logger.info("【从业人员】新增" + driver.toString());
			result.put("result", "success");
		} catch (Exception e) {
			result.put("result", "error");
			// attr.addFlashAttribute("result", result);
//			result = "error";
//			// attr.addFlashAttribute("result", result);
//>>>>>>> .merge-right.r708
			logger.error("【从业人员】新增出现异常" + e.getMessage());
			// return "redirect:/getDriver?taxiNum=" + taxiNumSave + "&quaNum="
			// + "&dname=";

		}
		// attr.addFlashAttribute("result", result);
		// return "redirect:/getDriver?taxiNum=" + taxiNumSave + "&quaNum=" +
		// "&dname=";

		return result;

	}

	// 更新从业人员
	@RequestMapping("/updateDriver")
	public String updateDriver(Driver driver, @RequestParam("taxiNumUpdate") String taxiNumUpdate,
			RedirectAttributes attr) {
		String result = null;
		driver.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
		driver.setTime(new Timestamp(new Date().getTime()));
		try {
			infoService.updateDriver(driver);
			logger.info("【从业人员】更新" + driver.toString());
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			attr.addFlashAttribute("result", result);
			logger.error("【从业人员】更新出现异常" + e.getMessage());
			return "redirect:/getDriver?taxiNum=" + taxiNumUpdate + "&quaNum=" + "&dname=";
		}
		attr.addFlashAttribute("result", result);
		// return "redirect:/enterDriver";
		return "redirect:/getDriver?taxiNum=" + taxiNumUpdate + "&quaNum=" + "&dname=";
	}

	@RequestMapping("/updateDriverById")
	public @ResponseBody Map<String, Object> updateDriverById(MultipartFile photo, Driver driver,
			HttpServletRequest request) {
		Map<String, Object> result = new HashMap<String, Object>();
		System.out.println("照片名称：" + photo.getOriginalFilename());
		String photoAddr = photoHandler(photo, request, 220, "new");

		System.out.println(driver.toString());
		driver.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
		driver.setTime(new Timestamp(new Date().getTime()));
		driver.setPhotoAddr(photoAddr);
		try {
			infoService.updateDriver(driver);
			logger.info("【从业人员】更新" + driver.toString());
			result.put("result", "success");
		} catch (Exception e) {
			result.put("result", "error");
			logger.error("【从业人员】更新出现异常" + e.getMessage());
		}
		return result;
	}

	// 删除从业人员
	@RequestMapping("/deleteDriver")
	public String deleteDriver(Driver driver, @RequestParam("taxiNumDelete") String taxiNumDelete,
			RedirectAttributes attr) {
		String result = null;
		try {
			infoService.deleteDriver(driver);
			logger.info("删除从业人员" + driver.toString());
			result = "操作执行成功";
		} catch (HibernateOptimisticLockingFailureException e) {
			logger.error("删除从业人员失败，已不存在" + driver.toString());
			result = "您要操作的对象已被删除";
			attr.addFlashAttribute("result", result);
			return "redirect:/getDriver?taxiNum=" + taxiNumDelete + "&quaNum=" + "&dname=";
		} catch (Exception e) {
			logger.error("执行删除从业人员出现异常" + e.getMessage());
			result = "相关投诉尚未处理，删除失败";
			attr.addFlashAttribute("result", result);
			return "redirect:/getDriver?taxiNum=" + taxiNumDelete + "&quaNum=" + "&dname=";
		}

		// request.setAttribute("result",result);
		// ObjectMapper mapper = new ObjectMapper();
		attr.addFlashAttribute("result", result);
		return "redirect:/getDriver?taxiNum=" + taxiNumDelete + "&quaNum=" + "&dname=";
	}

	// 删除从业人员 huihui
	@RequestMapping("/deleteDriverById")
	public @ResponseBody String deleteDriverById(Driver driver) {
		String result = null;
		try {
			System.out.println("接受到的信息：" + driver.toString());
			infoService.deleteDriver(driver);
			logger.info("删除从业人员" + driver.toString());
			result = "success";
		} catch (HibernateOptimisticLockingFailureException e) {
			logger.error("删除从业人员失败，已不存在" + driver.toString());
			result = "nouser";
		} catch (Exception e) {
			logger.error("执行删除从业人员出现异常" + e.getMessage());
			result = "error";
		}
		System.out.println(result);
		return result;
	}

	// ajax获取所有的车辆(id和名称)
	@RequestMapping("/getAllTaxiByAjax")
	public void getAllTaxiByAjax(HttpServletResponse response) {
		Subject subject = SecurityUtils.getSubject();
		List<Taxi> taxis = new ArrayList<>();
		InfoQuery info = new InfoQuery();
		if (subject.isPermitted("allcompy:get")) {
			taxis = infoService.getAllTaxi();
		} else if (subject.isPermitted("bjcompy:get")) {
			taxis.clear();
			List<Company> companies = new ArrayList<>();
			companies = infoService.getAllCompanyBj();
			if (companies != null) {
				for (Company company : companies) {
					info.setComId(company.getId());
					taxis.addAll(infoService.getTaxi(info));
				}
			}
		} else {
			String name = (String) subject.getPrincipal();
			Company company = infoService.getComByUser(name);
			if (company != null) {
				info.setComId(company.getId());
				taxis = infoService.getTaxi(info);
			}

		}
		List<TaxiInfo> taxiInfos = new ArrayList<>();
		for (Taxi t : taxis) {
			taxiInfos.add(new TaxiInfo(t.getId(), t.getTaxiNum()));
		}

		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().print(mapper.writeValueAsString(taxiInfos));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// ajax获取所有的公司(id和名称)
	@RequestMapping("/getAllComByAjax")
	public void getAllComByAjax(HttpServletResponse response) {
		// List<Company> companies = infoService.getAllCompany();
		// companies.remove(0);//不在页面显示"所有公司"
		// --------------按权限显示车辆---------------------
		Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
		} else if (subject.isPermitted("bjcompy:get")) {
			companies = infoService.getAllCompanyBj();
		} else {
			String name = (String) subject.getPrincipal();

			// int id=(int)session.getAttribute("compyid");
			Company company = infoService.getComByUser(name);
			if (company != null) {

				companies.add(company);
			}
		}

		// --------------------------------------

		List<ComInfo> comInfos = new ArrayList<>();
		for (Company c : companies) {
			comInfos.add(new ComInfo(c.getId(), c.getName()));
		}

		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().print(mapper.writeValueAsString(comInfos));
		} catch (IOException e) {
			logger.info("获取公司车辆出现异常" + e.getMessage());
		}
	}

	// ajax获取所有的车型
	@RequestMapping("/getAllTaxiTypeByAjax")
	public void getAllTaxiTypeByAjax(HttpServletResponse response) {
		// List<Company> companies = infoService.getAllCompany();
		// //companies.remove(0);//不在页面显示"所有公司"
		//
		// List<ComInfo> comInfos = new ArrayList<>();
		// for (Company c:companies) {
		// comInfos.add(new ComInfo(c.getId(), c.getName()));
		// }

		List<Taxitype> taxitypes = infoService.getAllTaxiType();

		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().print(mapper.writeValueAsString(taxitypes));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// ajax获取所有的车型，返回json
	@ResponseBody
	@RequestMapping("/getAllTaxiTypeByAjaxWithJson")
	public List<Taxitype> getAllTaxiTypeByAjaxWithJson(HttpServletResponse response) {
		try {
			List<Taxitype> taxitypes = infoService.getAllTaxiType();
			return taxitypes;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// ajax获取所有的公司(全部!)
	@RequestMapping("/getAllCom2ByAjax")
	public void getAllCom2ByAjax(HttpServletResponse response) {
		// --------------按权限显示车辆---------------------
		Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
		} else if (subject.isPermitted("bjcompy:get")) {
			companies = infoService.getAllCompanyBj();
		} else {
			String name = (String) subject.getPrincipal();

			// int id=(int)session.getAttribute("compyid");
			Company company = infoService.getComByUser(name);
			if (company != null) {

				companies.add(company);
			}
		}

		// --------------------------------------
		// List<Company> companies = infoService.getAllCompany();

		List<ComInfo2> comInfo2s = new ArrayList<>();
		for (Company c : companies) {
			Set<Taxi> taxis = c.getTaxis();
			Set<TaxiInfo2> taxis2 = new HashSet<>();

			Iterator<Taxi> it = taxis.iterator();
			while (it.hasNext()) {
				Taxi next = it.next();
				TaxiInfo2 t2 = new TaxiInfo2(next.getId(), next.getTaxiNum());
				taxis2.add(t2);
			}
			comInfo2s.add(new ComInfo2(c.getId(), c.getName(), taxis2));
		}

		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().print(mapper.writeValueAsString(comInfo2s));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// 进入"保险管理"界面
	@RequestMapping("/enterInsure")
	public String enterInsure(Map<String, Object> map) {
		return "WEB-INF/views/info/insure";
	}

	// 查询"保险信息"
	@RequestMapping("/getInsure")
	public String getInsure(Map<String, Object> map, InfoQuery infoQuery) {

		// 按条件查询出保险信息
		List<Insure> insures = infoService.getInsure(infoQuery);
		map.put("insures", insures);

		return "WEB-INF/views/info/insure";
	}

	// 新增保险信息
	@RequestMapping("/saveInsure")
	public String saveInsure(Insure insure, RedirectAttributes attr) {
		String result = null;
		insure.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
		insure.setTime(new Timestamp(new Date().getTime()));
		try {
			infoService.saveInsure(insure);
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			logger.error("【保险管理】新增出现异常" + e.getMessage());
			attr.addFlashAttribute("result", result);
			return "redirect:/enterInsure";
		}
		attr.addFlashAttribute("result", result);
		return "redirect:/enterInsure";
	}

	// 更新保险信息
	@RequestMapping("/updateInsure")
	public String updateInsure(Insure insure, RedirectAttributes attr) {
		String result = null;
		insure.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
		insure.setTime(new Timestamp(new Date().getTime()));
		try {
			infoService.updateInsure(insure);
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			logger.error("【保险管理】更新出现异常" + e.getMessage());
			attr.addFlashAttribute("result", result);
			return "redirect:/enterInsure";

		}
		attr.addFlashAttribute("result", result);
		return "redirect:/enterInsure";
	}

	// 更新安全事故
	@RequestMapping("/updateAccident")
	public String updateAccident(Accident insure, RedirectAttributes attr) {
		String result = null;
		try {
			infoService.updateAccident(insure);
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			logger.error("【安全管理】更新出现异常" + e.getMessage());
			attr.addFlashAttribute("result", result);
			return "redirect:/listAccidents";

		}
		attr.addFlashAttribute("result", result);
		return "redirect:/listAccidents2";
	}

	@RequestMapping("/listAccidents2")
	public String listAccidents2(Map<String, Object> map, InfoQuery query) {
		// 查询所有事故
		List<Accident> accidents = infoService.listAccidents(query);
		map.put("accidents", accidents);
		return "WEB-INF/views/info/accident";
	}

	// 删除保险信息
	@RequestMapping("/deleteInsure")
	public String deleteInsure(Insure insure, RedirectAttributes attr) {
		/*
		 * infoService.deleteInsure(insure); return "redirect:/enterInsure";
		 */
		String result = null;
		try {
			infoService.deleteInsure(insure);
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			logger.error("【保险管理】删除出现异常" + e.getMessage());
			attr.addFlashAttribute("result", result);
			return "redirect:/enterInsure";
		}
		attr.addFlashAttribute("result", result);
		return "redirect:/enterInsure";
	}

	// 进入"黑名单管理"界面
	@RequestMapping("/enterBlacklist")
	public String enterBlacklist(HttpServletRequest request) {

		Subject subject = SecurityUtils.getSubject();
		InfoQuery infoQuery = new InfoQuery();
		// 获取车辆信息
		List<Taxi> taxis = new ArrayList<>();
		if (subject.isPermitted("allcompy:get")) {
			taxis = infoService.getAllTaxi();
		} else if (subject.isPermitted("bjcompy:get")) {
			taxis.clear();
			List<Company> companies = new ArrayList<>();
			companies = infoService.getAllCompanyBj();
			if (companies != null) {
				for (Company company : companies) {
					InfoQuery info = new InfoQuery();
					info.setComId(company.getId());
					taxis.addAll(infoService.getTaxi(infoQuery));
				}
			}
		} else {
			String name = (String) subject.getPrincipal();
			Company company = infoService.getComByUser(name);
			if (company != null) {
				infoQuery.setComId(company.getId());
				taxis = infoService.getTaxi(infoQuery);
			}

		}
		if (taxis.size() > 0) {

			HttpSession session = request.getSession();
			// 获取所有车辆
			session.setAttribute("taxis", taxis);
		}
		return "WEB-INF/views/info/blacklist";
	}

	// 查询黑名单
	@RequestMapping("/getBlacklist")
	public String getBlacklist(Map<String, Object> map, InfoQuery2 infoQuery) {

		// 按条件查询出黑名单车辆
		List<Blacklist> blacklists = infoService.getBlacklist(infoQuery);
		map.put("blacklists", blacklists);

		return "WEB-INF/views/info/blacklist";
	}

	// 新增黑名单
	@RequestMapping("/saveBlacklist")
	public String saveBlacklist(Blacklist blacklist, RedirectAttributes attr) {
		String result = null;
		// backedList.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
		blacklist.setTime(new Timestamp(new Date().getTime()));
		try {
			infoService.saveBlacklist(blacklist);
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			logger.error("【黑名单】新增出现异常" + e.getMessage());
			attr.addFlashAttribute("result", result);
			return "redirect:/enterBlacklist";
		}
		attr.addFlashAttribute("result", result);
		return "redirect:/enterBlacklist";
	}

	// 更新黑名单信息
	@RequestMapping("/updateBlacklist")
	public String updateBlacklist(Blacklist blacklist, RedirectAttributes attr) {
		String result = null;
		// insure.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
		try {
			blacklist.setTime(new Timestamp(new Date().getTime()));
			infoService.updateBlacklist(blacklist);
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			logger.error("【黑名单】更新出现异常" + e.getMessage());
			attr.addFlashAttribute("result", result);
			return "redirect:/enterBlacklist";
		}
		attr.addFlashAttribute("result", result);
		return "redirect:/enterBlacklist";
	}

	// 删除稽查信息
	@RequestMapping("/deleteBlacklist")
	public String deleteBlacklist(Blacklist blacklist, RedirectAttributes attr) {
		/*
		 * infoService.deleteBlacklist(blacklist); return
		 * "redirect:/enterBlacklist";
		 */
		String result = null;
		try {
			infoService.deleteBlacklist(blacklist);
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			logger.error("【黑名单】删除出现异常" + e.getMessage());
			attr.addFlashAttribute("result", result);
			return "redirect:/enterBlacklist";

		}
		attr.addFlashAttribute("result", result);
		return "redirect:/enterBlacklist";
	}

	// 进入"稽查信息管理"界面
	@RequestMapping("/enterCheck")
	public String enterCheck(Map<String, Object> map) {
		return "WEB-INF/views/info/check";
	}

	// 查询稽查信息
	@RequestMapping("/getCheck")
	public String getCheck(Map<String, Object> map, InfoQuery2 infoQuery) {

		// 按条件查询出从业人员
		List<Check> checks = infoService.getCheck(infoQuery);
		map.put("checks", checks);
		return "WEB-INF/views/info/check";
	}

	// 新增稽查信息
	@RequestMapping("/saveCheck")
	public String saveCheck(Check check, RedirectAttributes attr) {
		String result = null;
		if (check.getState() == null) {
			check.setState(0);
		}
		try {
			infoService.saveCheck(check);
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			logger.error("【稽查管理】新增出现异常" + e.getMessage());
			attr.addFlashAttribute("result", result);
			return "redirect:/enterCheck";

		}
		attr.addFlashAttribute("result", result);
		return "redirect:/enterCheck";
	}

	// 更新稽查信息
	@RequestMapping("/updateCheck")
	public String updateCheck(Check check, RedirectAttributes attr) {
		String result = null;
		if (check.getState() == null) {
			check.setState(0);
		}
		try {
			infoService.updateCheck(check);
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			logger.error("【稽查管理】更新出现异常" + e.getMessage());
			attr.addFlashAttribute("result", result);
			return "redirect:/enterCheck";
		}
		attr.addFlashAttribute("result", result);
		return "redirect:/enterCheck";
	}

	// 删除稽查信息
	@RequestMapping("/deleteCheck")
	public String deleteCheck(Check check, RedirectAttributes attr) {
		String result = null;
		try {
			infoService.deleteCheck(check);
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			logger.error("【稽查管理】更新出现异常" + e.getMessage());
			attr.addFlashAttribute("result", result);
			return "redirect:/enterCheck";

		}
		attr.addFlashAttribute("result", result);
		return "redirect:/enterCheck";
	}

	// 进入"运价信息管理"界面
	@RequestMapping("/enterFare")
	public String enterFare(Map<String, Object> map) {
		return "WEB-INF/views/info/fare";
	}

	// 查询运价信息
	@RequestMapping("/getFare")
	public String getFare(Map<String, Object> map, @RequestParam String type) {
		List<Fare> fares = new ArrayList<>();
		// 按条件查询出运价信息
		if (infoService.getFare(type).size() > 0) {
			fares = infoService.getFare(type);
		}
		map.put("fares", fares);
		return "WEB-INF/views/info/fare";
	}

	// 新增运价信息
	@RequestMapping("/saveFare")
	public String saveFare(Fare fare, RedirectAttributes attr) {
		String result = null;
		try {
			infoService.saveFare(fare);
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			logger.error("【运价管理】新增出现异常" + e.getMessage());
			attr.addFlashAttribute("result", result);
			return "redirect:/enterFare";

		}
		attr.addFlashAttribute("result", result);
		return "redirect:/enterFare";
	}

	// 更新运价信息
	@RequestMapping("/updateFare")
	public String updateFare(Fare fare, RedirectAttributes attr) {
		String result = null;
		try {
			infoService.updateFare(fare);
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			logger.error("【运价管理】更新出现异常" + e.getMessage());
			attr.addFlashAttribute("result", result);
			return "redirect:/enterFare";

		}
		attr.addFlashAttribute("result", result);
		return "redirect:/enterFare";
	}

	// 删除运价信息
	@RequestMapping("/deleteFare")
	public String deleteFare(Fare fare, RedirectAttributes attr) {
		String result = null;
		try {
			infoService.deleteFare(fare);
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			logger.error("【运价管理】删除出现异常" + e.getMessage());
			attr.addFlashAttribute("result", result);
			return "redirect:/enterFare";
		}
		attr.addFlashAttribute("result", result);
		return "redirect:/enterFare";
	}

	// 进入"违章管理"界面
	@RequestMapping("/enterBreakrule")
	public String enterBreakrule(Map<String, Object> map) {
		return "WEB-INF/views/info/breakRule";
	}

	// 查询违章信息
	@RequestMapping("/getBreakrule")
	public String getBreakrule(Map<String, Object> map, InfoQuery2 infoQuery2) {

		// 按条件查询出违章信息
		List<Breakrule> breakrules = infoService.getBreakrule(infoQuery2);
		map.put("breakrules", breakrules);
		return "WEB-INF/views/info/breakRule";
	}

	// 新增违章信息
	@RequestMapping("/saveBreakrule")
	public String saveBreakrule(Breakrule breakrule, RedirectAttributes attr) {
		String result = null;
		if (breakrule.getState() == null) {
			breakrule.setState(0);
		}
		try {
			infoService.saveBreakrule(breakrule);
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			logger.error("【违章管理】新增出现异常" + e.getMessage());
			attr.addFlashAttribute("result", result);
			return "redirect:/enterBreakrule";
		}
		attr.addFlashAttribute("result", result);
		return "redirect:/enterBreakrule";
	}

	// 更新违章信息
	@RequestMapping("/updateBreakrule")
	public String updateBreakrule(Breakrule breakrule, RedirectAttributes attr) {
		String result = null;
		if (breakrule.getState() == null) {
			breakrule.setState(0);
		}
		try {
			infoService.updateBreakrule(breakrule);
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			logger.error("【违章管理】更新出现异常" + e.getMessage());
			attr.addFlashAttribute("result", result);
			return "redirect:/enterBreakrule";

		}
		attr.addFlashAttribute("result", result);
		return "redirect:/enterBreakrule";
	}

	// 删除违章信息
	@RequestMapping("/deleteBreakrule")
	public String deleteBreakrule(Breakrule breakrule, RedirectAttributes attr) {
		String result = null;
		try {
			infoService.deleteBreakrule(breakrule);
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			logger.error("【违章管理】删除出现异常" + e.getMessage());
			attr.addFlashAttribute("result", result);
			return "redirect:/enterBreakrule";

		}
		attr.addFlashAttribute("result", result);
		return "redirect:/enterBreakrule";
	}

	// 进入"年审管理"界面
	@RequestMapping("/enterYearinspect")
	public String enterYearinspect(Map<String, Object> map) {
		// 查出所有公司,显示在页面下拉框
		// List<Company> companies = infoService.getAllCompany();
		// --------------按权限显示车辆---------------------
		Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
		} else if (subject.isPermitted("bjcompy:get")) {
			companies = infoService.getAllCompanyBj();
		} else {
			String name = (String) subject.getPrincipal();

			// int id=(int)session.getAttribute("compyid");
			Company company = infoService.getComByUser(name);
			if (company != null) {

				companies.add(company);
			}
		}

		// --------------------------------------
		// companies.remove(0);//不在页面显示"所有公司"
		map.put("companies", companies);

		return "WEB-INF/views/info/yearinspect";
	}

	// 查询年审信息
	@RequestMapping("/getYearinspect")
	public String getYearinspect(Map<String, Object> map, InfoQuery2 infoQuery2) {

		// 按条件查询出年审信息
		List<Yearinspect> yearinspects = infoService.getYearinspect(infoQuery2);
		map.put("yearinspects", yearinspects);

		// 查出所有公司,显示在页面下拉框
		// --------------按权限显示车辆---------------------
		Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
		} else if (subject.isPermitted("bjcompy:get")) {
			companies = infoService.getAllCompanyBj();
		} else {
			String name = (String) subject.getPrincipal();

			// int id=(int)session.getAttribute("compyid");
			Company company = infoService.getComByUser(name);
			if (company != null) {

				companies.add(company);
			}
		}

		// --------------------------------------
		// List<Company> companies = infoService.getAllCompany();
		// companies.remove(0);//不在页面显示"所有公司"
		map.put("companies", companies);
		return "WEB-INF/views/info/yearinspect";
	}

	// 新增年审信息
	@RequestMapping("/saveYearinspect")
	public String saveYearinspect(Yearinspect yearinspect, RedirectAttributes attr) {
		String result = null;
		try {
			infoService.saveYearinspect(yearinspect);
			result = "操作执行成功";
			logger.info("【年审管理】新增：" + yearinspect.toString());
		} catch (Exception e) {
			result = "操作失败";
			logger.error("【年审管理】新增出现异常" + e.getMessage());
			attr.addFlashAttribute("result", result);
			return "redirect:/enterYearinspect";
		}
		attr.addFlashAttribute("result", result);
		return "redirect:/enterYearinspect";
	}

	// 更新年审信息
	@RequestMapping("/updateYearinspect")
	public String updateYearinspect(Yearinspect yearinspect, RedirectAttributes attr) {
		String result = null;
		try {
			infoService.updateYearinspect(yearinspect);
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			logger.error("【年审管理】更新出现异常" + e.getMessage());
			attr.addFlashAttribute("result", result);
			return "redirect:/enterYearinspect";

		}
		attr.addFlashAttribute("result", result);
		return "redirect:/enterYearinspect";
	}

	// 删除年审信息
	@RequestMapping("/deleteYearinspect")
	public String deleteYearinspect(Yearinspect yearinspect, RedirectAttributes attr) {
		String result = null;
		try {
			infoService.deleteYearinspect(yearinspect);
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			logger.error("【年审管理】删除出现异常" + e.getMessage());
			attr.addFlashAttribute("result", result);
			return "redirect:/enterYearinspect";

		}
		attr.addFlashAttribute("result", result);
		return "redirect:/enterYearinspect";
	}

	// 进入树形菜单
	@RequestMapping("/getTree")
	public String getTree(Map<String, Object> map) {
		// --------------按权限显示车辆---------------------
		Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
		} else if (subject.isPermitted("bjcompy:get")) {
			companies = infoService.getAllCompanyBj();
		} else {
			String name = (String) subject.getPrincipal();

			// int id=(int)session.getAttribute("compyid");
			Company company = infoService.getComByUser(name);
			if (company != null) {

				companies.add(company);
			}
		}

		// --------------------------------------

		// List<Company> companies = infoService.getAllCompany();

		List<ComInfo2> comInfo2s = new ArrayList<>();
		for (Company c : companies) {
			Set<Taxi> taxis = c.getTaxis();
			Set<TaxiInfo2> taxis2 = new HashSet<>();

			Iterator<Taxi> it = taxis.iterator();
			while (it.hasNext()) {
				Taxi next = it.next();
				TaxiInfo2 t2 = new TaxiInfo2(next.getId(), next.getTaxiNum());
				taxis2.add(t2);
			}
			comInfo2s.add(new ComInfo2(c.getId(), c.getName(), taxis2));
		}

		map.put("comInfo2s", comInfo2s);
		return "jstree";
	}

	// 测试时间格式
	@RequestMapping("/saveTime")
	public String saveTime(TimeTest test) {
		logger.info("test:" + test);
		return "timeTest";
	}

	// ajax获取车辆id
	@RequestMapping("/ifTaxiNumByAjax")
	public void ifTaxiNumExistByAjax(HttpServletResponse response, HttpServletRequest request) {
		String taxiNum = request.getParameter("taxiNum");
		InfoQuery infoQuery = new InfoQuery();
		infoQuery.setTaxiNum(taxiNum);
		int taxiId = 0;
		if (!taxiNum.equals("")) {
			taxiId = infoService.getTaxiIdBytaxiNum(infoQuery);
		}

		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().print(mapper.writeValueAsString(taxiId));
		} catch (IOException e) {
			logger.error("【ajax获取车辆id】出现异常" + e.getMessage());
		}
	}

	// 根据车牌号获取车辆终端
	@RequestMapping("/getIsuNumBycar")
	public void getIsuNumBycar(HttpServletResponse response, HttpServletRequest request) {
		// 打印出车牌号
		String taxiNum = request.getParameter("taxiNum");
		// 拼接查询条件
		InfoQuery infoQuery = new InfoQuery();
		infoQuery.setTaxiNum(taxiNum);
		String isu = null;
		// 判断是否为空
		if (!taxiNum.equals("") && taxiNum != null) {
			if (taxiService.getIsuByTaxi(taxiNum) != null) {
				isu = taxiService.getIsuByTaxi(taxiNum).getIsuNum();
			}
		}
		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().print(mapper.writeValueAsString(isu));
		} catch (IOException e) {
			logger.error("【ajax获取车辆终端号】出现异常" + e.getMessage());
		}
	}

	/**
	 * 导出所有的出租车数据到excel
	 * 
	 * @author 高杨
	 * @date 2018年10月31日 下午7:26:19
	 * @param @return
	 * @return String
	 * @throws IOException
	 */
	@RequestMapping("/exportTaxiData")
	public void exportTaxiData(HttpServletResponse response) throws IOException {
		// 1.获取所有的车辆数据
		List<Taxi> taxis = taxiService.getAllTaxi();

		// 2.通过POI导出到excel
		// [1] 创建一个HSSFWorkbook，对应一个Excel文件
		HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
		HSSFFont font = hssfWorkbook.createFont();
		font.setFontHeightInPoints((short) 14);
		font.setFontName("宋体");
		// [2] 在workbook中添加一个sheet,对应Excel文件中的sheet
		HSSFSheet sheet = hssfWorkbook.createSheet("车辆信息");
		// [3] 在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制
		HSSFRow row = sheet.createRow(0);
		// [4] 创建单元格，并设置值表头 设置表头居中， 边框
		HSSFCellStyle style = hssfWorkbook.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN); //下边框    
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);//左边框    
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);//上边框    
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);//右边框 
		style.setFont(font);
		
		//设置时间格式
		CreationHelper helper = hssfWorkbook.getCreationHelper();
		HSSFCellStyle style2 = hssfWorkbook.createCellStyle();
		style2.setDataFormat(helper.createDataFormat().getFormat("yyyy-MM-dd HH:mm:ss"));
		style2.setFont(font);
		// [5] 声明列对象
		HSSFCell cell = null;

		// [6] 设置表头
		String title[] = { "序号", "公司", "车型", "车辆颜色", "车牌号", "终端号", "车架号", "车主姓名", "车主身份证号", "车主手机", "注册日期", "运营开始日期",
				"运营结束日期", "保险单号", "保险期限", "燃料类型", "购置金额", "年审日期", "下次年审时间", "运输证发证机构", "行驶证号", "道路运输证号", "车辆尺寸", "发动机号",
				"运营状态" };
		int[] size = { 9, 20, 20, 10, 20, 20, 20, 10, 20, 20, 30, 30, 30, 20, 30, 10, 10, 30, 30, 20, 20, 20, 30, 20,
				10 };// 写完车主手机
		for (int i = 0; i < title.length; i++) {
			cell = row.createCell(i);
			cell.setCellValue(title[i]);
			cell.setCellStyle(style);
			sheet.setColumnWidth(i, 252 * size[i] + 323);
		}

		// [7]设置数据
		int count = 1;
		for (Taxi taxi : taxis) {
			// sheet.autoSizeColumn(count);
			row = sheet.createRow(count);
			row.createCell(0).setCellValue(count);
			String companyName = taxi.getCompany().getName();
			if (companyName != null && companyName != "") {
				row.createCell(1).setCellValue(companyName);
				row.setRowStyle(style);
			}
			String taxiType = taxi.getTaxiType();
			if (taxiType != null && taxiType != "") {
				row.createCell(2).setCellValue(taxiType);
				row.setRowStyle(style);
			}
			String taxiColor = taxi.getTaxiColor();
			if (taxiColor != null && taxiColor != "") {
				row.createCell(3).setCellValue(taxiColor);
				row.setRowStyle(style);
			}
			String taxiNum = taxi.getTaxiNum();
			if (taxiNum != null && taxiNum != "") {
				row.createCell(4).setCellValue(taxiNum);
				row.setRowStyle(style);
			}
			String isuNum = taxi.getIsuNum();
			if (isuNum != null && isuNum != "") {
				row.createCell(5).setCellValue(isuNum);
				row.setRowStyle(style);
			}
			String taxiFrame = taxi.getTaxiFrame();
			if (taxiFrame != null && taxiFrame != "") {
				row.createCell(6).setCellValue(taxiFrame);
				row.setRowStyle(style);
			}
			String masterName = taxi.getMasterName();
			if (masterName != null && masterName != "") {
				row.createCell(7).setCellValue(masterName);
				row.setRowStyle(style);
			}
			String masterIdNum = taxi.getMasterIdNum();
			if (masterIdNum != null && masterIdNum != "") {
				row.createCell(8).setCellValue(masterIdNum);
				row.setRowStyle(style);
			}
			String masterPhone = taxi.getMasterPhone();
			if (masterPhone != null && masterPhone != "") {
				row.createCell(9).setCellValue(masterPhone);
				row.setRowStyle(style);
			}

			Timestamp registerDate = taxi.getRegisterDate();
			if (registerDate != null) {
				HSSFCell cell2 = row.createCell(10);
				cell2.setCellStyle(style2);
				cell2.setCellValue(registerDate);
			}
			// row.createCell(10).setCellType(Cell);.setCellValue(taxi.getRegisterDate());
			Timestamp operStartDate = taxi.getOperStartDate();
			if (operStartDate != null) {
				HSSFCell cell3 = row.createCell(11);
				cell3.setCellStyle(style2);
				cell3.setCellValue(operStartDate);
			}
			// .setCellValue(taxi.getOperStartDate());
			Timestamp operEndDate = taxi.getOperEndDate();
			if (operEndDate != null) {
				HSSFCell cell4 = row.createCell(12);
				cell4.setCellStyle(style2);
				cell4.setCellValue(operEndDate);
			}
			String insuranceNum = taxi.getInsuranceNum();
			if (insuranceNum != null && insuranceNum != "") {
				row.createCell(13).setCellValue(insuranceNum);
				row.setRowStyle(style);
			}
			Timestamp insuranceTime = taxi.getInsuranceTime();
			if (insuranceTime != null) {
				HSSFCell cell7 = row.createCell(14);
				cell7.setCellStyle(style2);
				cell7.setCellValue(insuranceTime);
			}
			String fuelType = taxi.getFuelType();
			if (fuelType != null && fuelType != "") {
				row.createCell(15).setCellValue(fuelType);
				row.setRowStyle(style);
			}
			Integer purAmount = taxi.getPurAmount();
			if (purAmount != null) {
				row.createCell(16).setCellValue(purAmount);
				row.setRowStyle(style);
			}
			Timestamp auditDate = taxi.getAuditDate();
			if (auditDate != null) {
				HSSFCell cell5 = row.createCell(17);
				cell5.setCellStyle(style2);
				cell5.setCellValue(auditDate);
			}
			Timestamp nextAuditDate = taxi.getNextAuditDate();
			if (nextAuditDate != null) {
				HSSFCell cell6 = row.createCell(18);
				cell6.setCellStyle(style2);
				cell6.setCellValue(nextAuditDate);
			}
			String tranAgency = taxi.getTranAgency();
			if (tranAgency != null && tranAgency != "") {
				row.createCell(19).setCellValue(tranAgency);
				row.setRowStyle(style);
			}
			String drivIdNum = taxi.getDrivIdNum();
			if (drivIdNum != null && drivIdNum != "") {
				row.createCell(20).setCellValue(drivIdNum);
				row.setRowStyle(style);
			}
			String tranIdNum = taxi.getTranIdNum();
			if (tranIdNum != null && tranIdNum != "") {
				row.createCell(21).setCellValue(tranIdNum);
				row.setRowStyle(style);
			}
			String carSize = taxi.getCarSize();
			if (carSize != null && carSize != "") {
				row.createCell(22).setCellValue(carSize);
				row.setRowStyle(style);
			}
			String engineNum = taxi.getEngineNum();
			if (engineNum != null && engineNum != "") {
				row.createCell(23).setCellValue(engineNum);
				row.setRowStyle(style);
			}
			String operationStatus = taxi.getOperationStatus();
			if (operationStatus != null && operationStatus != "") {
				row.createCell(24).setCellValue(operationStatus);
				row.setRowStyle(style);
			}
			count++;
		}

		// [8]设置文件名称
		String fileName = new String("车辆信息.xls".getBytes(), "ISO8859-1");
		response.setContentType("application/octet-stream;charset=ISO8859-1");
		response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
		response.addHeader("Pargam", "no-cache");
		response.addHeader("Cache-Control", "no-cache");
		OutputStream os = response.getOutputStream();
		hssfWorkbook.write(os);
	}
	
		//审核
		@RequestMapping(value="/ApproalDriverById")
		@ResponseBody
		public String ApproalDriverById(InfoQuery info) {
			String result = null;
			try {
				infoService.ApproalDriverById(info.getId(),info.getState());
				result = "success";
			} catch (Exception e) {
				result = "false";
				logger.error("【审核司机】新增出现异常" + e.getMessage());
				return result;
			}
			return result;
		}
}
