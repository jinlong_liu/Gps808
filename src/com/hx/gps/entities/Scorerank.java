package com.hx.gps.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Scorerank entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "scorerank" )
public class Scorerank implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer low;
	private String rank;
	private Integer top;

	// Constructors

	/** default constructor */
	public Scorerank() {
	}

	/** full constructor */
	public Scorerank(Integer low, String rank, Integer top) {
		this.low = low;
		this.rank = rank;
		this.top = top;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "low")
	public Integer getLow() {
		return this.low;
	}

	public void setLow(Integer low) {
		this.low = low;
	}

	@Column(name = "rank", length = 30)
	public String getRank() {
		return this.rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	@Column(name = "top")
	public Integer getTop() {
		return this.top;
	}

	public void setTop(Integer top) {
		this.top = top;
	}

	@Override
	public String toString() {
		return "Scorerank [id=" + id + ", low=" + low + ", rank=" + rank
				+ ", top=" + top + "]";
	}

}