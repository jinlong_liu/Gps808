package com.hx.gps.entities.tool;

import com.mongodb.BasicDBObject;

public class Evaluate {
	
	private String isuNum;//终端编号
	private	String quaNum;//司机资格证号
	private int good;	 //好评个数
	private int general;	 //中评个数
	private int bad;		 //差评个数
	private int unvalued; //未评价个数
	private int total;	 //总评价个数（包含未评价）
	public String getIsuNum() {
		return isuNum;
	}
	public void setIsuNum(String isuNum) {
		this.isuNum = isuNum;
	}
	
	public String getQuaNum() {
		return quaNum;
	}
	public void setQuaNum(String quaNum) {
		this.quaNum = quaNum;
	}
	public int getGood() {
		return good;
	}
	public void setGood(int good) {
		this.good = good;
	}
	public int getGeneral() {
		return general;
	}
	public void setGeneral(int general) {
		this.general = general;
	}
	public int getBad() {
		return bad;
	}
	public void setBad(int bad) {
		this.bad = bad;
	}
	public int getUnvalued() {
		return unvalued;
	}
	public void setUnvalued(int unvalued) {
		this.unvalued = unvalued;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public Evaluate() {
		super();
	}
	public Evaluate(BasicDBObject finds) {
		super();
		this.isuNum = finds.getString("isuNum");
		this.good = finds.getInt("good");
		this.general = finds.getInt("general");
		this.bad = finds.getInt("bad");
		this.unvalued = finds.getInt("unvalued");
		this.total = finds.getInt("total");
		this.quaNum=finds.getString("quaNum");
	}
	@Override
	public String toString() {
		return "Evaluate [isuNum=" + isuNum + ", quaNum=" + quaNum + ", good="
				+ good + ", general=" + general + ", bad=" + bad
				+ ", unvalued=" + unvalued + ", total=" + total + "]";
	}
	

}
