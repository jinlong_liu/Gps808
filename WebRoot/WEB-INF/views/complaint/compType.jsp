<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<!-- gy 添加表单验证 -->
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
<style type="text/css">
.form-control-gy {
	width: 30%;
	position:relative;
	left:650px;
	display: inline;
}
</style>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">

					<!-- 点击添加按钮弹出来的form表单 -->
					<div class="modal inmodal fade" id="myModal5" tabindex="-1"
						role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<form action="addCompType" class="form-horizontal" id="addCompTypeForm" method="post">
								    <fieldset>
								        <div class="modal-body">
								            <div class="ibox-title">
								                <h5>添加投诉类型<span style="color:red">（*为必填项）</span></h5>
								            </div>
								            <div class="ibox-content">
								                <div class="row">
								                
								                <!-- 投诉类型 -->
											<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red"> * </span>投诉类型 :</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" name="type"/>
												</div>
											</div>
<!-- 								                    <div class="col-sm-6"> -->
<!-- 								                        <div class="form-group"> -->
<!-- 								                            <label class="col-sm-3 control-label"> 投诉类型: </label> -->
<!-- 								                            <input class="form-control" type="text" name="type"/> -->
<!-- 								                        </div> -->
<!-- 								                    </div> -->
                                            <!-- 司機得分 -->
                                            <div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red"> * </span>司机得分 :</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" name="scorer"/>
												</div>
											</div>
<!-- 								                    <div class="col-sm-6"> -->
<!-- 								                        <div class="form-group"> -->
<!-- 								                            <label class="col-sm-3 control-label">司机得分:</label> -->
<!-- 								                            <input class="form-control" type="text" name="scorer"/> -->
<!-- 								                        </div> -->
<!-- 								                    </div> -->
													<!-- 車輛得分 -->
												<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red"> * </span>车辆得分:</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" name="scoreTaxi"/>
												</div>
											</div>
<!-- 								                </div> -->
<!-- 								                <div class="row"> -->
<!-- 								                    <div class="col-sm-6"> -->
<!-- 								                        <div class="form-group"> -->
<!-- 								                            <label class="col-sm-3 control-label">车辆得分:</label> -->
<!-- 								                            <input class="form-control" type="text" name="scoreTaxi"/></div> -->
<!-- 								                    </div> -->
													<!-- 公司得分 -->
												<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red"> * </span>公司得分:</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" name="scorePany"/>
												</div>
<!-- 											</div> -->
<!-- 								                    <div class="col-sm-6"> -->
<!-- 								                        <div class="form-group"> -->
<!-- 								                            <label class="col-sm-3 control-label">公司得分:</label> -->
<!-- 								                            <input class="form-control" type="text" name="scorePany"/></div> -->
<!-- 								                    </div> -->
								        </div>
								                </div>
								                <div class="modal-footer">
								            <button type="button" class="btn btn-white"
								                    data-dismiss="modal">关闭
								            </button>
								            <button type="submit" class="btn btn-primary">提交</button>
								            </div>
								        </div>
								
								        </div>
								    </fieldset>
								</form>
							</div>
						</div>
					</div>
					
					
					<!-- 点击"修改"按钮弹出来的form表单 -->
					<div class="modal inmodal fade" id="myModal1" tabindex="-1"
						role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<form action="addCompType" class="form-horizontal" id="modifyCompTypeForm" method="post">
								    <fieldset>
								        <div class="modal-body">
								            <div class="ibox-title">
								                <h5>修改投诉类型<span style="color:red">（*为必填项）</span></h5>
								            </div>
								            <div class="ibox-content">
								      
								                <input type="hidden" name="id" id="id">
								          
								                <div class="row">
								                        <div class="form-group">
								                            <label class="col-sm-3 control-label"><span style="color:red">* </span> 投诉类型: </label>
								                            <div class="col-sm-6">
								                            <input class="form-control" type="text" name="type"
																   id="type" autocomplete="off" />
								                            </div>
								                        </div>
								                        <div class="form-group">
								                            <label class="col-sm-3 control-label"><span style="color:red">* </span>司机得分:</label>
								                            <div class="col-sm-6">
								                            <input class="form-control" type="text" name="scorer"
																   id="scorer" autocomplete="off"/>
								                            </div>
								                        </div>
								              
								                        <div class="form-group">
								                            <label class="col-sm-3 control-label"><span style="color:red">* </span>车辆得分:</label>
								                            <div class="col-sm-6">
								                            <input class="form-control" type="text" name="scoreTaxi"
																   id="scoreTaxi" autocomplete="off"/></div>
								                            </div>
								                        <div class="form-group">
								                            <label class="col-sm-3 control-label"><span style="color:red">* </span>公司得分:</label>
								                            <div class="col-sm-6">
								                            <input class="form-control" type="text" name="scorePany"
																   id="scorePany" autocomplete="off"/></div>
								                            </div>
								                </div>
								                 <div class="modal-footer">
								            <button type="button" class="btn btn-white"
								                    data-dismiss="modal">关闭
								            </button>
								            <button type="submit" class="btn btn-primary">提交</button>
								        </div>
								            </div>
								       
								        </div>
								
								       
								    </fieldset>
								</form>
							</div>
						</div>
					</div>
		<div class="row">

			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>投诉类型表</h5>
	
				</div>
				<div class="ibox-content" style="height: 780px">
					<div class="row">
								
					<table  id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF;margin:10px 10px 10px"
							class="footable table table-stripped " data-page-size="10"
							data-filter=#filter>
							
					<thead>
					<tr><td>	<button type="button" class="btn btn-primary" data-toggle="modal"
						data-target="#myModal5">添&nbsp;&nbsp;&nbsp;加</button>&nbsp;</td></tr>
							<tr>
								<th>投诉类型</th>
								<th>司机得分</th>
								<th>车辆得分</th>
								<th>公司得分</th>
								<th>操作</th>

							</tr>
					
						</thead>
						<tbody>
							<c:forEach items="${compType}" var="compType">
								<tr id="${compType.id}">
									<td>${compType.type}</td>
									<td>${compType.scorer}</td>
									<td>${compType.scoreTaxi}</td>
									<td>${compType.scorePany}</td>
									<td><button id="btn-update" type="button"
											class="btn btn-success btn-sm" data-toggle="modal"
											data-target="#myModal1"
											onclick="alterScore('${compType.id}','${compType.type}','${compType.scorer}','${compType.scoreTaxi}','${compType.scorePany}')">更改</button>
										<button type="button" class="btn btn-danger btn-sm"
											onclick="delect('${compType.id}')">删除</button></td>
								</tr>
							</c:forEach>
						</tbody>
				<tfoot>
							<tr>
								<td colspan="5">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
				<form id="deleteForm" action="delectCompType" method="post">
					<input type="hidden" id="deleteId" name="id">
				</form>
			</div>
		</div>
	</div>
	</div>
	<script src="js/jquery.js"></script>
	<script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
	<script>
	//加载footable的功能
	$(document).ready(function() {
		$('#table1').DataTable({
			"pagingType" : "full_numbers"
		});

		var result='<%=request.getAttribute("result")%>';
		if (result != null && result != "null") {
			if (result.indexOf("成功") > 0) {
				swal({
					title : result,
					type : "success"
				});
			} else {
				swal({
					title : result,
					type : "warning"
				});
			}
		}

	});
		$(document).ready(function () {
	        $('#addCompTypeForm').bootstrapValidator({
	            message: '这个值无效',
	            feedbackIcons: {
	                valid: 'glyphicon glyphicon-ok',
	                invalid: 'glyphicon glyphicon-remove',
	                validating: 'glyphicon glyphicon-refresh'
	            },
	            /*生效规则：字段值一旦变化就触发验证*/
	            live: 'enabled',
	            /*当表单验证不通过时，该按钮为disabled*/
	            submitButtons: 'button[type="submit"]',
	            fields: {
	                type: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写投诉类型"
	                        },
	                    }
	                },
	                scorer: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写司机得分"
	                        },
	
	                        between: {
	                            min: 0,
	                            max: 100,
	                            message: '请输入0-100之间的整数'
	                        },
	                        numeric : {
								message : '请输入数字'
							}
	                    }
	                }, scoreTaxi: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写车辆得分"
	                        },
	
	                        between: {
	                            min: 0,
	                            max: 100,
	                            message: '请输入0-100之间的整数'
	                        },
	                        numeric : {
								message : '请输入数字'
							}
	                    }
	                }, scorePany: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写公司得分"
	                        },
	
	                        between: {
	                            min: 0,
	                            max: 100,
	                            message: '请输入0-100之间的整数'
	                        },
	                        numeric : {
								message : '请输入数字'
							}
	                    }
	                }
	            }
	        });
	    });
		$(document).ready(function () {
	        $('#modifyCompTypeForm').bootstrapValidator({
	            message: '这个值无效',
	            feedbackIcons: {
	                valid: 'glyphicon glyphicon-ok',
	                invalid: 'glyphicon glyphicon-remove',
	                validating: 'glyphicon glyphicon-refresh'
	            },
	            /*生效规则：字段值一旦变化就触发验证*/
	            live: 'enabled',
	            /*当表单验证不通过时，该按钮为disabled*/
	            submitButtons: 'button[type="submit"]',
	            fields: {
	                type: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写投诉类型"
	                        },
	                    }
	                },
	                scorer: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写司机得分"
	                        },numeric : {
								message : '请输入数字'
							},
                            between: {
	                            min: 0,
	                            max: 100,
	                            message: '请输入0-100之间的整数'
	                        },
	                        numeric : {
								message : '请输入数字'
							}
	                    }
	                }, scoreTaxi: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写车辆得分"
	                        },
	
	                        between: {
	                            min: 0,
	                            max: 100,
	                            message: '请输入0-100之间的整数'
	                        },
	                        numeric : {
								message : '请输入数字'
							}
	                    }
	                }, scorePany: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写公司得分"
	                        },
	
	                        between: {
	                            min: 0,
	                            max: 100,
	                            message: '请输入0-100之间的整数'
	                        },
	                        numeric : {
								message : '请输入数字'
							}
	                    }
	                }
	            }
	        });
	    });
		//查看默认处理
		function alterScore(id, type, scorer, scoreTaxi, scorePany) {
			$("#id").val(id);
			$("#type").val(type);
			$("#scorer").val(scorer);
			$("#scoreTaxi").val(scoreTaxi);
			$("#scorePany").val(scorePany);
		};

		function getVal(state) {
			var id = $("#complaintId").val();
			var apprOpinion = $("#apprOpinion").val();
			deal(id, state, apprOpinion);
		}
	 	//删除操作
		function delect(id) {
			swal({
				title: "您确定执行该操作吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			},function(){
		/* 	$("#" + id).hide();
			var args = {
				"id" : id,
			};
			var url = "delectCompType";
			$.post(url, args, function(data) {
				var a = String(data);
				if(a=="操作执行成功"){
					  swal({
			                title: a,
			                type: "success"
			            },function(){
							window.location.reload();});
				}else{
					swal({
		                title: a,
		                type: "warning"
		            });
					 $("#"+id).show();
				} 
			} */
				 $("#deleteId").val(id);
					//提交表单
				  $("#deleteForm").submit(); 
			});
			 
		}

		function submit(id, state) {
			swal({
				title : "您确定执行该操作吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
				//deal(id, state, null);
				  $("#deleteId").val(id);
					//提交表单
				  $("#deleteForm").submit();  
			});
		}
	</script>
</body>

</html>