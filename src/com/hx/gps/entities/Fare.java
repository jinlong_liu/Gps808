package com.hx.gps.entities;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Fare entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "fare" )
public class Fare implements java.io.Serializable {

	// Fields

	private Integer id;
	private String type;
	private Timestamp startTime;
	private Timestamp endTime;
	private Float dayStart;
	private Float nightStart;
	private Float fareLeave;
	private Float dayPrice;
	private Float nightPrice;
	private Float addPrice;
	private Float wait;
	private Float haulback;
	private Float lowVelocity;

	// Constructors

	/** default constructor */
	public Fare() {
	}

	/** full constructor */
	public Fare(String type, Timestamp startTime, Timestamp endTime,
			Float dayStart, Float nightStart, Float fareLeave, Float dayPrice,
			Float nightPrice, Float addPrice, Float wait, Float haulback,
			Float lowVelocity) {
		this.type = type;
		this.startTime = startTime;
		this.endTime = endTime;
		this.dayStart = dayStart;
		this.nightStart = nightStart;
		this.fareLeave = fareLeave;
		this.dayPrice = dayPrice;
		this.nightPrice = nightPrice;
		this.addPrice = addPrice;
		this.wait = wait;
		this.haulback = haulback;
		this.lowVelocity = lowVelocity;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "type")
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "startTime", length = 19)
	public Timestamp getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	@Column(name = "endTime", length = 19)
	public Timestamp getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	@Column(name = "dayStart", precision = 11)
	public Float getDayStart() {
		return this.dayStart;
	}

	public void setDayStart(Float dayStart) {
		this.dayStart = dayStart;
	}

	@Column(name = "nightStart", precision = 11)
	public Float getNightStart() {
		return this.nightStart;
	}

	public void setNightStart(Float nightStart) {
		this.nightStart = nightStart;
	}

	@Column(name = "fareLeave", precision = 11)
	public Float getFareLeave() {
		return this.fareLeave;
	}

	public void setFareLeave(Float fareLeave) {
		this.fareLeave = fareLeave;
	}

	@Column(name = "dayPrice", precision = 11)
	public Float getDayPrice() {
		return this.dayPrice;
	}

	public void setDayPrice(Float dayPrice) {
		this.dayPrice = dayPrice;
	}

	@Column(name = "nightPrice", precision = 11)
	public Float getNightPrice() {
		return this.nightPrice;
	}

	public void setNightPrice(Float nightPrice) {
		this.nightPrice = nightPrice;
	}

	@Column(name = "addPrice", precision = 11)
	public Float getAddPrice() {
		return this.addPrice;
	}

	public void setAddPrice(Float addPrice) {
		this.addPrice = addPrice;
	}

	@Column(name = "wait", precision = 11)
	public Float getWait() {
		return this.wait;
	}

	public void setWait(Float wait) {
		this.wait = wait;
	}

	@Column(name = "haulback", precision = 11)
	public Float getHaulback() {
		return this.haulback;
	}

	public void setHaulback(Float haulback) {
		this.haulback = haulback;
	}

	@Column(name = "lowVelocity", precision = 11)
	public Float getLowVelocity() {
		return this.lowVelocity;
	}

	public void setLowVelocity(Float lowVelocity) {
		this.lowVelocity = lowVelocity;
	}

}