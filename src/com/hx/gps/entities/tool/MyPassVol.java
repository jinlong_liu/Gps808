package com.hx.gps.entities.tool;

public class MyPassVol {
	private String date;
	private int passVol;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getPassVol() {
		return passVol;
	}
	public void setPassVol(int passVol) {
		this.passVol = passVol;
	}
	public MyPassVol() {
		super();
	}
	public MyPassVol(String date, int passVol) {
		super();
		this.date = date;
		this.passVol = passVol;
	}
	@Override
	public String toString() {
		return "MyPassVol [date=" + date + ", passVol=" + passVol + "]";
	}
	
}
