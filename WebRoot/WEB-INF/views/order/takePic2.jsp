<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>H+ 后台主题UI框架 - 基本表单</title>
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link rel="shortcut icon" href="favicon.ico">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- <base target="_blank"> -->

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>车辆拍照设置</h5>
				</div>
				<div class="ibox-content" style=" height: 756px">
					<form class="form-horizontal" action="saveTakePic" method="post" id="getOneForm">
						<div class="form-group">
							<label class="col-sm-3 control-label">车牌号：</label>
							<div class="col-sm-3">
								<input class="form-control" type="text" name="taxiNums" id="taxiNums"
									value="${taxiNum}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">通道：</label>
							<div class="col-sm-3">
								<input class="form-control" type="text" name="gallery" value="1">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">分辨率：</label>
							<div class="col-sm-8">
								<div class="col-sm-3">
									<input type="radio" name="resolution" value="1" checked>
									320*240
								</div>
								<div class="col-sm-3">
									<input type="radio" name="resolution" value="2">
									640*480
								</div>
								<div class="col-sm-3">
									<input type="radio" name="resolution" value="3">
									800*600
								</div>
								<div class="col-sm-3">
									<input type="radio" name="resolution" value="4">
									1024*768
								</div>
								<br>
								<div class="col-sm-3">
									<input type="radio" name="resolution" value="5">
									176*144[Qcif]
								</div>
								<div class="col-sm-3">
									<input type="radio" name="resolution" value="6">
									352*288[Cif]
								</div>
								<div class="col-sm-3">
									<input type="radio" name="resolution" value="7">
									704*288[HALF D1]
								</div>
								<div class="col-sm-3">
									<input type="radio" name="resolution" value="8">
									701*576[D1]
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">图片质量：</label>
							<div class="col-sm-3">
								<input class="form-control" type="text" name="picQuality"
									value="10" id="quality">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">亮度：</label>
							<div class="col-sm-3">
								<input class="form-control" type="text" name="luminance"
									value="120">
							</div>

						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">对比度：</label>
							<div class="col-sm-3">
								<input class="form-control" type="text" name="contrast"
									value="65">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">饱和度：</label>
							<div class="col-sm-3">
								<input class="form-control" type="text" name="saturability"
									value="65">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label">色度：</label>
							<div class="col-sm-3">
								<input class="form-control" type="text" name="chroma"
									value="120">
							</div>
						</div>
					</form>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-4">
							<button class="btn  btn-primary" type="button" id="getOneBtn">发&nbsp;&nbsp;&nbsp;送</button>&nbsp;&nbsp;
							<a href="getDealAlarm"><button type="button"
									class="btn">返&nbsp;&nbsp;&nbsp;回</button></a>
						</div>
					</div>
					<br>
				</div>
			</div>
		</div>
	</div>

	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/content.min.js?v=1.0.0"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		var result='<%=request.getAttribute("result")%>';
			if(result != null && result != "null"){
				if (result.indexOf("成功")>0) {
				swal({
					title : result,
					type : "success"
				});
			}else{
				swal({
					title :result,
					type : "error"
				});
			}
			}

	});
	
	//图像质量改变:
	$("#quality").change(function() {
	var num = $("#quality").val();
	if (num<1) {
		$("#quality").val("1");
	}else if(num>10){
		$("#quality").val("10");
	}
});
		//点击"发送"按钮
		$("#getOneBtn").click(function() {
			var taxiNums = $("#taxiNums").val();

			if (taxiNums == "") {
				swal({
					title : "没有选中任何车辆,请点击'返回'按钮返回",
					type : "warning"
				});
			} else {
				/* swal({
					title : "发送成功,请前往命令日志页面查看",
					type : "warning"
				}, function() {
				}); */
				 $("#getOneForm").submit();
			}
		});
	</script>
</body>

</html>