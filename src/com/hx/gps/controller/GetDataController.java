package com.hx.gps.controller;

import com.hx.gps.dao.SerialDao;
import com.hx.gps.entities.tool.PointsInfo;
import com.hx.gps.service.AddressService;
import com.hx.gps.service.SystemService;
import com.hx.gps.service.impl.AddressServiceImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
public class GetDataController {
    // 操作日志类
    private static Log logger = LogFactory.getLog(GetDataController.class);
    @Autowired
    SystemService systemService;
    @Autowired
    private AddressService addressService;
    // 获取当日报警总数
    @ResponseBody
    @RequestMapping("/getAlarmNum")
    public int getAlarmNum() {
        return systemService.getAlarmNum();
    }
    //获取未处理报警总数
    @ResponseBody
    @RequestMapping("/getAlarmNums")
    public int getAlarmNums() {
        return systemService.getAlarmNums();
    }
    //获取车辆总数\
    @ResponseBody
    @RequestMapping("/getOnlineAll")
    public int getOnlineAll(){
        return addressService.getOnlineAll();
    }
    //获取当前的车辆的数量
    @ResponseBody
    @RequestMapping("/getOnlineCurrent")
    public int getOnlineCurrent(){
        return addressService.getOnlineCurrent();
    }
}
