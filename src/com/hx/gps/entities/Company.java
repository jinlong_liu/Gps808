package com.hx.gps.entities;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Company entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "company" )
public class Company implements java.io.Serializable {

	// Fields

	private Integer id;
	private String name;
	private String cphone;
	private String address;
	private String principal;
	private String pphone;
	private String lagal;
	private String blNum;
	private String ccode;
	private Integer refund;
	private String etype;
	private String remark;
	private String people;
	private Timestamp time;
	private Set<Taxi> taxis = new HashSet<Taxi>(0);
	private Set<Role> roles = new HashSet<Role>(0);
	private Set<User> users = new HashSet<User>(0);
	private Set<Driver> drivers = new HashSet<Driver>(0);

	// Constructors

	/** default constructor */
	public Company() {
	}

	/** full constructor */
	public Company(String name, String cphone, String address,
			String principal, String pphone, String lagal, String blNum,
			String ccode, Integer refund, String etype, String remark,
			String people, Timestamp time, Set<Taxi> taxis, Set<Role> roles,
			Set<User> users, Set<Driver> drivers) {
		this.name = name;
		this.cphone = cphone;
		this.address = address;
		this.principal = principal;
		this.pphone = pphone;
		this.lagal = lagal;
		this.blNum = blNum;
		this.ccode = ccode;
		this.refund = refund;
		this.etype = etype;
		this.remark = remark;
		this.people = people;
		this.time = time;
		this.taxis = taxis;
		this.roles = roles;
		this.users = users;
		this.drivers = drivers;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "cphone")
	public String getCphone() {
		return this.cphone;
	}

	public void setCphone(String cphone) {
		this.cphone = cphone;
	}

	@Column(name = "address")
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "principal")
	public String getPrincipal() {
		return this.principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	@Column(name = "pphone")
	public String getPphone() {
		return this.pphone;
	}

	public void setPphone(String pphone) {
		this.pphone = pphone;
	}

	@Column(name = "lagal")
	public String getLagal() {
		return this.lagal;
	}

	public void setLagal(String lagal) {
		this.lagal = lagal;
	}

	@Column(name = "blNum")
	public String getBlNum() {
		return this.blNum;
	}

	public void setBlNum(String blNum) {
		this.blNum = blNum;
	}

	@Column(name = "ccode")
	public String getCcode() {
		return this.ccode;
	}

	public void setCcode(String ccode) {
		this.ccode = ccode;
	}

	@Column(name = "refund")
	public Integer getRefund() {
		return this.refund;
	}

	public void setRefund(Integer refund) {
		this.refund = refund;
	}

	@Column(name = "etype")
	public String getEtype() {
		return this.etype;
	}

	public void setEtype(String etype) {
		this.etype = etype;
	}

	@Column(name = "remark")
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "people")
	public String getPeople() {
		return this.people;
	}

	public void setPeople(String people) {
		this.people = people;
	}

	@Column(name = "time", length = 19)
	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "company")
	public Set<Taxi> getTaxis() {
		return this.taxis;
	}

	public void setTaxis(Set<Taxi> taxis) {
		this.taxis = taxis;
	}

	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "company")
	public Set<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "company")
	public Set<User> getUsers() {
		return this.users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "company")
	public Set<Driver> getDrivers() {
		return this.drivers;
	}

	public void setDrivers(Set<Driver> drivers) {
		this.drivers = drivers;
	}

	/*@Override
	public String toString() {
		return "Company [id=" + id + ", name=" + name + ", taxis=" + taxis
				+ "]";
	}*/
	
}