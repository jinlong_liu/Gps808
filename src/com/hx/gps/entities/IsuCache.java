package com.hx.gps.entities;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * IsuCache entity. @author MyEclipse Persistence Tools
 */
@SuppressWarnings("serial")
@Entity 
@Table(name = "IsuCache" )
public class IsuCache implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */

	private Integer id;
	private String isu;
	private String isu2;
	private int type;
	private Timestamp date;


	// Constructors

	/** default constructor */
	public IsuCache() {
	}

	/**
	 * 有参构造
	 * @param id
	 * @param isu
	 * @param isu2
	 * @param type
	 * @param date
	 */

	public IsuCache(Integer id, String isu, String isu2, int type,
			Timestamp date) {
		 
		this.id = id;
		this.isu = isu;
		this.isu2 = isu2;
		this.type = type;
		this.date = date;
	}



	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "isu")

	public String getIsu() {
		return isu;
	}
	
	public void setIsu(String isu) {
		this.isu = isu;
	}


	@Column(name = "isu2")
	public String getIsu2() {
		return isu2;
	}
	
	public void setIsu2(String isu2) {
		this.isu2 = isu2;
	}

 @Column(name="type")
 public int getType() {
	 return type;
 }
 
 public void setType(int type) {
	 this.type = type;
 }

 @Column(name="date")
 public Timestamp getDate() {
	 return date;
 }
 
 public void setDate(Timestamp date) {
	 this.date = date;
 }

@Override
public String toString() {
	return "IsuCache [id=" + id + ", isu=" + isu + ", isu2=" + isu2 + ", type="
			+ type + ", date=" + date + "]";
}
 
 

}