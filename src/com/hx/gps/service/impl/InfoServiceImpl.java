package com.hx.gps.service.impl;

import java.beans.beancontext.BeanContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Array;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

//import org.apache.catalina.core.ApplicationContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hx.gps.auth.CustomRealm;
import com.hx.gps.dao.InfoDao;
import com.hx.gps.dao.TaxiDao;
import com.hx.gps.dao.UserDao;
import com.hx.gps.entities.Accident;
import com.hx.gps.entities.Accidenttype;
import com.hx.gps.entities.Blacklist;
import com.hx.gps.entities.Breakrule;
import com.hx.gps.entities.Check;
import com.hx.gps.entities.Company;
import com.hx.gps.entities.Complaints;
import com.hx.gps.entities.Driver;
import com.hx.gps.entities.Fare;
import com.hx.gps.entities.Insure;
import com.hx.gps.entities.IsuCache;
import com.hx.gps.entities.Power;
import com.hx.gps.entities.Role;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.Taxitype;
import com.hx.gps.entities.User;
import com.hx.gps.entities.Yearinspect;
import com.hx.gps.entities.tool.DatatablesQuery;
import com.hx.gps.entities.tool.DatatablesView;
import com.hx.gps.entities.tool.DriverVO;
import com.hx.gps.entities.tool.InfoQuery;
import com.hx.gps.entities.tool.InfoQuery2;
import com.hx.gps.entities.tool.MeterState;
import com.hx.gps.entities.tool.TerminalState;
import com.hx.gps.entities.tool.videoState;
import com.hx.gps.service.InfoService;
import com.hx.gps.util.FarmConstant;

@Service
public class InfoServiceImpl implements InfoService {
	@Autowired
	private InfoDao infoDao;
	@Autowired
	private TaxiDao taxiDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private CustomRealm customRealm;

	// 日志记录
	private static Log logger = LogFactory.getLog(InfoServiceImpl.class);

	@Override
	public List<User> getAllUser() {
		return infoDao.getAllUser();
	}

	@Override
	public List<User> getUserByCom(Company company) {
		return infoDao.getUserByCom(company);
	}

	@Override
	public Company getComByUser(String name) {

		return infoDao.getComByUser(name);
	}

	@Override
	public List<Role> getAllRoleByPower() {

		return infoDao.getAllRole();
	}

	@Override
	public List<Accident> listAccidents(InfoQuery query) {
		return infoDao.listAccidents(query);
	}

	@Override
	public List<Company> getAllCompany() {

		return infoDao.getAllCompany();
	}

	@Override
	public List<Taxi> getTaxi(InfoQuery infoQuery) {
		List<Taxi> taxis = infoDao.getTaxi(infoQuery);

		// System.out.println(taxis);

		// 保留
		List<Taxi> result = new ArrayList<>();

		// for (int i = 0; i < taxis.size(); i++) {
		// if (taxiDao.findIsu(taxis.get(i).getIsuNum())) { //
		// 判断本条信息是否可以在MongoDB里找到
		// result.add(taxis.get(i)); // 添加至结果集
		// }
		// }
		// taxis.clear();
		return taxis;
	}

	@Override
	public String[] getTaxiByCom(int id) {
		// 获取公司的所有终端
		String[] isu = infoDao.getTaxiByCompany(id);
		if (isu.length > 0) {
			return isu;
		} else {
			return null;
		}
	}

	@Override
	public boolean saveUser(User user) {
		if (userDao.findUserByName(user.getName()) != null) {
			logger.info("【添加用户】错误：用户名已存在" + user.toString());
			return false;
		} else {
			infoDao.saveUser(user);
			logger.info("以上为添加新用户" + user.toString());
			return true;
		}

	}

	// 保存安全事故
	@Override
	public void saveAccident(Accident accident) throws Exception {
		infoDao.saveAccident(accident);
		logger.info("新增安全事故" + accident.toString());
	}

	// 更新安全事故
	@Override
	public void updateAccident(Accident accident) {
		infoDao.updateAccident(accident);
		logger.info("更新安全事故" + accident.toString());
	}

	@Override
	public boolean updateState(User user) {
		User old = userDao.findUserById(user.getId());
		int Sta = old.getState();

		if (Sta == 0) {
			old.setState(1);
			old.setPeople(user.getPeople());
			infoDao.updateState(old);
			logger.info("【更新用户状态】原状态为使用中");
			// 更改用户状态需要清除shiro的缓存
			customRealm.clearCached();
			return true;
		} else if (Sta == 1) {
			old.setState(0);
			old.setPeople(user.getPeople());
			infoDao.updateState(old);
			logger.info("【更新用户状态】原状态为禁用");
			// 更改用户状态需要清除shiro的缓存
			customRealm.clearCached();
			return true;
		}

		return false;
	}

	@Override
	public boolean updateUser(User user, String old) {
		User found = userDao.findUserByName(user.getName());
		// if (old.equals("")) {
		// user.setPassword(found.getPassword());
		// infoDao.updateUser(user);
		// logger.info("更新用户信息:" + user.getName() + "更新后信息" + user.toString());
		// return true;
		// } else {
		if (found != null) {
			if (found.getPassword().equals(old)) {
				found.setPassword(user.getPassword());
				if (user.getCompany() != null) {
					found.setCompany(user.getCompany());
				}
				found.setTime(user.getTime());
				found.setPeople(user.getPeople());
				infoDao.updateUser(found);
				logger.info("更新用户信息:" + user.getName() + "更新后信息" + user.toString());
				// 更改用户状态需要清除shiro的缓存
				customRealm.clearCached();
				return true;
			} else {
				logger.info("更新用户信息失败，原密码错误。更新用户：" + user.getName());
				return false;
			}
		} else {
			return false;
		}
	}

	// }

	@Override
	public void timeTask() {
		int type = 0;
		List<IsuCache> isu = taxiDao.getAllIsuCache();
		if (isu.size() == 0) {
			logger.info("缓存为空：");
			return;
		}
		for (IsuCache isuCache : isu) {
			logger.info("开始同步缓存数据：" + isu);
			// System.out.println("执行轮询任务中=====");
			type = isuCache.getType();
			try {
				if (type == 1) {
					taxiDao.addIsu(isuCache.getIsu());
					logger.info("同步新车辆" + isuCache.getIsu() + "到mongodb");
					// System.out.println("执行插入中");
					// System.out.println(isuCache.getIsu());
				} else if (type == 2) {
					taxiDao.updateIsu(isuCache.getIsu2(), isuCache.getIsu());
					logger.info("同步更新原车辆" + isuCache.getIsu2() + "到mogodb，更新后终端号" + isuCache.getIsu());
					// System.out.println("执行更新中");
				} else if (type == 3) {
					taxiDao.deleteIsu(isuCache.getIsu());
					logger.info("同步删除车辆" + isuCache.getIsu() + "到mongodb");
					// System.out.println("执行删除中"+isuCache.getIsu());
				} else {
					logger.info("同步类型有误，无法解析");
				}
				// 执行成功跳到这一步，删除原有记录。
				taxiDao.deleteIsuCacheById(isuCache);
				logger.info("操作成功，对应缓存表记录已删除");
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("重试失败..." + isuCache.toString());
				logger.error("同步数据中出现未知错误" + e.getMessage());
			}

		}
	}

	@Override
	public void deleteUser(User user) {
		infoDao.deleteUser(user);
		// 删除用户需要清除shiro的缓存
		customRealm.clearCached();
		logger.info("删除用户:" + user.toString());
	}
	@Override
	public List<Company> getAllCompanyBj() {
		return infoDao.getAllCompanyBj();
	}
	@Override
	public List<Accidenttype> getAccidentType() {
		return infoDao.getAccidentType();
	}

	@Override
	public boolean saveAccidentType(Accidenttype accidentType) {
		return infoDao.saveAccidentType(accidentType);
	}

	@Override
	public void deleteAccident(Accident accident) {
		infoDao.deleteAccident(accident);
		logger.info("dao删除事故信息:" + accident.toString());
	}

	@Override
	public boolean haveCompany(Company company) {
		return infoDao.haveCompany(company);
	}

	@Override
	public void saveRole(Role role) {
		infoDao.saveRole(role);
		logger.info("新增角色" + role.toString());
	}

	@Override
	public void updateRole(Role role) {
		infoDao.updateRole(role);
		customRealm.clearCached();
		logger.info("更新角色：" + role.getName() + "更新后信息：" + role.toString());
	}

	@Override
	public void deleteRole(Role role) {
		infoDao.deleteRole(role);
		customRealm.clearCached();
		logger.info("删除角色:" + role.getName());
	}

	@Override
	public void saveCompany(Company company) {
		infoDao.saveCompany(company);
		logger.info(" 新增公司信息" + company.getName());
	}

	@Override
	public void updateCompany(Company company) {
		infoDao.updateCompany(company);
		logger.info("更新公司信息：" + company.getName() + "操作成功");
	}

	@Override
	public void deleteCompany(Company company) throws Exception {
		infoDao.deleteCompany(company);
		logger.info("删除公司信息" + company.getName());
	}

	@Override
	public List<Taxitype> getAllTaxiType() {
		return infoDao.getAllTaxiType();
	}

	@Override
	public boolean saveTaxiType(Taxitype taxitype) {
		return infoDao.saveTaxiType(taxitype);
	}

	@Override
	public void updateTaxiType(Taxitype taxitype) {
		infoDao.updateTaxiType(taxitype);
	}

	@Override
	public void deleteTaxiType(Taxitype taxitype) {
		infoDao.deleteTaxiType(taxitype);
	}

	@Override
	public boolean saveTaxiId(Taxi taxi) {
		try {
			taxiDao.addIsu(taxi.getIsuNum());
			if (FarmConstant.isCjAds) {
				infoDao.addCjTaxiNum(taxi);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			// 失败后存入缓存表
			taxiDao.addIsuCache(taxi, 1);
			logger.error("保存车辆到mongo出现异常，已加入缓存表" + e.getMessage());
			return false;
		}
	}

	@Override
	public String saveTaxi(Taxi taxi) {
		String result = null;
		// 首先进行重复性验证
		// System.out.println("-------------" + taxi.getIsuNum());
		if (taxiDao.getTaxiByIsu(taxi.getIsuNum()) != null) {
			result = "终端号已存在";
		} else if (taxiDao.getTaxiByTaxi(taxi.getTaxiNum()) != null) {
			result = "该车辆已存在";
		} else if (infoDao.saveTaxi(taxi)) {
			// 是否为川基广告
			if (saveTaxiId(taxi)) {
				logger.info("新增车辆信息：" + taxi.getIsuNum() + "操作成功");
				result = "插入成功";
			} else {
				logger.info("像mongodb新增车辆失败" + taxi.getIsuNum() + "已加入缓存表");
				result = "插入失败，已加入缓存表等待自动添加。。";
			}
		} else {
			logger.info("新增车辆信息：" + taxi.getIsuNum() + "操作失败");
			result = "插入失败";
		}

		return result;
	}

	@Override
	public List<Integer> uploadTaxi(String xlsName, SimpleDateFormat fmt, int rowCount, int success, int retry)
			throws Exception {
		//TODO 需要调整新的excel文件，才能做解析，先注释掉
		 
		 
		// 同时支持Excel 2003、2007
		File excelFile = new File(xlsName); // 创建文件对象
		FileInputStream is = new FileInputStream(excelFile); // 文件流
		Workbook workbook = WorkbookFactory.create(is); // 这种方式 Excel
														// 2003/2007/2010
														// 都是可以处理的
		int sheetCount = workbook.getNumberOfSheets(); // Sheet的数量
		// 遍历每个Sheet
		for (int s = 0; s < sheetCount; s++) {
			Sheet sheet = workbook.getSheetAt(s);
			rowCount = sheet.getPhysicalNumberOfRows(); // 获取总行数
			int skip = 0;
			// 遍历每一行
			for (int r = 0; r < rowCount; r++) {
				if (skip < 2) { // 跳过前两列
					skip++;
					continue;
				}
				Row row = sheet.getRow(r);
				int cellCount = row.getPhysicalNumberOfCells(); // 获取总列数

				List<Object> taxiTemp = new ArrayList<>();

				// 遍历每一列
				for (int c = 0; c < cellCount; c++) {
					Cell cell = row.getCell(c);
					int cellType = cell.getCellType();
					String cellValue = null;
					switch (cellType) {
					case Cell.CELL_TYPE_STRING: // 文本
						cellValue = cell.getStringCellValue();
						if (cellValue.equals("\\N") || cellValue.equals("\\n")) {
							cellValue = "";
						}
						break;

					case Cell.CELL_TYPE_NUMERIC: // 数字、日期
						if (DateUtil.isCellDateFormatted(cell)) {
							cellValue = fmt.format(cell.getDateCellValue());
						} // 日期型
						else {
							cellValue = String.valueOf(cell.getNumericCellValue());
						} // 数字
						break;
					case Cell.CELL_TYPE_BOOLEAN: // 布尔型
						cellValue = String.valueOf(cell.getBooleanCellValue());
						break;
					case Cell.CELL_TYPE_BLANK: // 空白
						cellValue = cell.getStringCellValue();
						break;
					case Cell.CELL_TYPE_ERROR: // 错误
						cellValue = "错误";
						break;
					case Cell.CELL_TYPE_FORMULA: // 公式
						cellValue = "错误";
						break;
					default:
						cellValue = "错误";
					}
					taxiTemp.add(cellValue);
				}
				Company company = new Company();
				if (((String) taxiTemp.get(2)).equals("")) {
					company.setId(1);
				} else {
					if (((String) taxiTemp.get(2)).indexOf(".") == -1) {
						company.setId(Integer.valueOf((String) taxiTemp.get(2)));
					} else {
						company.setId(Integer.valueOf(
								((String) taxiTemp.get(2)).substring(0, ((String) taxiTemp.get(2)).length() - 2)));
					}
				}
				Timestamp timestamp = null;
				Taxi taxi = new Taxi(null,company, (String) taxiTemp.get(1), (String) taxiTemp.get(3),
						(String) taxiTemp.get(4), (String) taxiTemp.get(5), (String) taxiTemp.get(6),
						(String) taxiTemp.get(7),  (String) taxiTemp.get(9),
						(String) taxiTemp.get(10), (String) taxiTemp.get(11), timestamp, timestamp, timestamp,
						  0, (String) taxiTemp.get(17), timestamp, (String) taxiTemp.get(19),
						(String) taxiTemp.get(20), (String) taxiTemp.get(22), (String) taxiTemp.get(23),
						timestamp, timestamp, (String) taxiTemp.get(26), (String) taxiTemp.get(27),
						(String) taxiTemp.get(28), (String) taxiTemp.get(29), (String) taxiTemp.get(30),
						new HashSet<Complaints>(0), new HashSet<Driver>(0),null,null,null,null,null);
				if (!((String) taxiTemp.get(12)).equals("")) {
					taxi.setRegisterDate(Timestamp.valueOf((String) taxiTemp.get(12)));
				}
				if (!((String) taxiTemp.get(13)).equals("")) {
					taxi.setOperStartDate(Timestamp.valueOf((String) taxiTemp.get(13)));
				}
				if (!((String) taxiTemp.get(14)).equals("")) {
					taxi.setOperEndDate(Timestamp.valueOf((String) taxiTemp.get(14)));
				}
//				if (!((String) taxiTemp.get(15)).equals("")) {
//					taxi.setPurDate(Timestamp.valueOf((String) taxiTemp.get(15)));
//				}
				if (!((String) taxiTemp.get(16)).equals("")) {
					taxi.setPurAmount(Integer.valueOf((String) taxiTemp.get(16)));
				}
				if (!((String) taxiTemp.get(18)).equals("")) {
					taxi.setAddDate(Timestamp.valueOf((String) taxiTemp.get(18)));
				}
//				if (!((String) taxiTemp.get(21)).equals("")) {
//					taxi.setByCarDate(Timestamp.valueOf((String) taxiTemp.get(21)));
//				}
				if (!((String) taxiTemp.get(24)).equals("")) {
					taxi.setAuditDate(Timestamp.valueOf((String) taxiTemp.get(24)));
				}
				if (!((String) taxiTemp.get(25)).equals("")) {
					taxi.setNextAuditDate(Timestamp.valueOf((String) taxiTemp.get(25)));
				}
				if (taxi.getIsuNum().equals("")) {
					continue;
				} else if (taxiDao.getTaxiByIsu(taxi.getIsuNum()) != null) {
					continue;
				} else if (taxiDao.getTaxiByTaxi(taxi.getTaxiNum()) != null) {
					continue;
				} else if (infoDao.saveTaxi(taxi)) {
					if (saveTaxiId(taxi)) {
						logger.info("批量操作：新增车辆" + taxi.getIsuNum());
						success++;
					} else {
						logger.info("批量操作：失败车辆" + taxi.getIsuNum());
						retry++;
					}

				} else {
					logger.info("批量操作：添加车辆失败" + taxi.getIsuNum());
				}

			}
		}
		List<Integer> results = new ArrayList<>();
		results.add(rowCount);
		results.add(success);
		results.add(retry);
		return results;
	};

	@Override
	public String saveEditTaxis(List<Taxi> taxis) {
		int i = 0;
		if (taxis.size() > 0) {
			// 清缓存
			FarmConstant.taxis.clear();
			//
			for (Taxi taxi : taxis) {
				// 操作人员
				Company company = new Company();
				company.setId(1);
				taxi.setCompany(company);
				taxi.setOperationer(SecurityUtils.getSubject().getPrincipal().toString());
				if (taxi.getIsuNum().equals("")) {
					i++;
					continue;
				} else if (taxiDao.getTaxiByIsu(taxi.getIsuNum()) != null) {
					i++;
					continue;
				} else if (taxiDao.getTaxiByTaxi(taxi.getTaxiNum()) != null) {
					i++;
					continue;
				} else if (infoDao.saveTaxi(taxi)) {
					if (saveTaxiId(taxi)) {
						logger.info("批量操作：新增车辆" + taxi.getIsuNum());
					} else {
						logger.info("批量操作：失败车辆" + taxi.getIsuNum());
						FarmConstant.taxis.add(taxi);
						i++;
						continue;
					}

				} else {
					logger.info("批量操作：添加车辆失败" + taxi.getIsuNum());
					continue;
				}

			}
		}
		if (i > 0) {
			return "失败" + i + "辆车，是否重试？";
		}
		return "成功";
	}

	// 莱芜特有，在线编辑
	@Override
	public List<Taxi> uploadAndEditTaxi(String xlsName, SimpleDateFormat fmt, int rowCount) throws Exception {
		 
		 
		
		// 同时支持Excel 2003、2007
		File excelFile = new File(xlsName); // 创建文件对象
		FileInputStream is = new FileInputStream(excelFile); // 文件流
		Workbook workbook = WorkbookFactory.create(is); // 这种方式 Excel
														// 2003/2007/2010
														// 都是可以处理的
		int sheetCount = workbook.getNumberOfSheets(); // Sheet的数量
		// 遍历每个Sheet
		for (int s = 0; s < sheetCount; s++) {
			Sheet sheet = workbook.getSheetAt(s);
			rowCount = sheet.getPhysicalNumberOfRows(); // 获取总行数
			int skip = 0;
			// 遍历每一行
			for (int r = 0; r < rowCount; r++) {
				if (skip < 2) { // 跳过前两列
					skip++;
					continue;
				}
				Row row = sheet.getRow(r);
				int cellCount = row.getPhysicalNumberOfCells(); // 获取总列数

				List<Object> taxiTemp = new ArrayList<>();

				// 遍历每一列
				for (int c = 0; c < cellCount; c++) {
					Cell cell = row.getCell(c);
					int cellType = cell.getCellType();
					String cellValue = null;
					switch (cellType) {
					case Cell.CELL_TYPE_STRING: // 文本
						cellValue = cell.getStringCellValue();
						if (cellValue.equals("\\N") || cellValue.equals("\\n")) {
							cellValue = "";
						}
						break;

					case Cell.CELL_TYPE_NUMERIC: // 数字、日期
						if (DateUtil.isCellDateFormatted(cell)) {
							cellValue = fmt.format(cell.getDateCellValue());
						} // 日期型
						else {
							cellValue = String.valueOf(cell.getNumericCellValue());
						} // 数字
						break;
					case Cell.CELL_TYPE_BOOLEAN: // 布尔型
						cellValue = String.valueOf(cell.getBooleanCellValue());
						break;
					case Cell.CELL_TYPE_BLANK: // 空白
						cellValue = cell.getStringCellValue();
						break;
					case Cell.CELL_TYPE_ERROR: // 错误
						cellValue = "错误";
						break;
					case Cell.CELL_TYPE_FORMULA: // 公式
						cellValue = "错误";
						break;
					default:
						cellValue = "错误";
					}
					taxiTemp.add(cellValue);
				}
				Company company = new Company();
				if (infoDao.getAllCompany() != null) {
					company = infoDao.getAllCompany().get(0);
				} else {
					return null;
				}
				System.err.println("公司：" + company.toString());
				// 文档中去掉公司的判断，改为自动获取。
				// if (((String) taxiTemp.get(2)).equals("")) {
				// } else {
				// if (((String) taxiTemp.get(2)).indexOf(".") == -1) {
				// company.setId(Integer.valueOf((String) taxiTemp.get(2)));
				// } else {
				// company.setId(Integer.valueOf(
				// ((String) taxiTemp.get(2)).substring(0, ((String)
				// taxiTemp.get(2)).length() - 2)));
				// }
				// }
				Timestamp timestamp = null;
				Taxi taxi = new Taxi(null,company, (String) taxiTemp.get(1), (String) taxiTemp.get(3),
						(String) taxiTemp.get(4), (String) taxiTemp.get(5), (String) taxiTemp.get(6),
						(String) taxiTemp.get(7),  (String) taxiTemp.get(9),
						(String) taxiTemp.get(10), (String) taxiTemp.get(11), timestamp, timestamp, timestamp,
						  0, (String) taxiTemp.get(17), timestamp, (String) taxiTemp.get(19),
						(String) taxiTemp.get(20), (String) taxiTemp.get(22), (String) taxiTemp.get(23),
						timestamp, timestamp, (String) taxiTemp.get(26), (String) taxiTemp.get(27),
						(String) taxiTemp.get(28), (String) taxiTemp.get(29), (String) taxiTemp.get(30),
						new HashSet<Complaints>(0), new HashSet<Driver>(0),null,null,null,null,null);
				if (company != null) {
					taxi.setCompany(company);
				}
				if (!((String) taxiTemp.get(12)).equals("")) {
					taxi.setRegisterDate(Timestamp.valueOf((String) taxiTemp.get(12)));
				}
				if (!((String) taxiTemp.get(13)).equals("")) {
					taxi.setOperStartDate(Timestamp.valueOf((String) taxiTemp.get(13)));
				}
				if (!((String) taxiTemp.get(14)).equals("")) {
					taxi.setOperEndDate(Timestamp.valueOf((String) taxiTemp.get(14)));
				}
//				if (!((String) taxiTemp.get(15)).equals("")) {
//					taxi.setPurDate(Timestamp.valueOf((String) taxiTemp.get(15)));
//				}
				if (!((String) taxiTemp.get(16)).equals("")) {
					taxi.setPurAmount(Integer.valueOf((String) taxiTemp.get(16)));
				}
				if (!((String) taxiTemp.get(18)).equals("")) {
					taxi.setAddDate(Timestamp.valueOf((String) taxiTemp.get(18)));
				}
//				if (!((String) taxiTemp.get(21)).equals("")) {
//					taxi.setByCarDate(Timestamp.valueOf((String) taxiTemp.get(21)));
//				}
				if (!((String) taxiTemp.get(24)).equals("")) {
					taxi.setAuditDate(Timestamp.valueOf((String) taxiTemp.get(24)));
				}
				if (!((String) taxiTemp.get(25)).equals("")) {
					taxi.setNextAuditDate(Timestamp.valueOf((String) taxiTemp.get(25)));
				}
				// 填充完数据后添加至集合
				FarmConstant.taxis.add(taxi);
			}
		}
	 
		return FarmConstant.taxis;
	}

	@Override
	public void updateTaxi(Taxi taxi) {
		infoDao.updateTaxi(taxi);
		logger.info("更新车辆信息成功" + taxi.toString());
	}

	@Override
	public void deleteTaxi(Taxi taxi) {
		infoDao.deleteTaxi(taxi);

	}

	@Override
	public void deleteIsu(String Isu) {
		taxiDao.deleteIsu(Isu);
		logger.info("待删除手机号：" + Isu);
	}

	@Override
	public void addToIsucache(Taxi taxi, int type) {
		taxiDao.addIsuCache(taxi, type);
		logger.info("缓存表新增记录" + taxi.toString() + "类型" + type);
	}

	@Override
	public List<Driver> getDriver(InfoQuery infoQuery) {
		return infoDao.getDriver(infoQuery);
	}

	@Override
	public DatatablesView<DriverVO> listDrivers(DatatablesQuery datatablesQuery, InfoQuery infoQuery) {
		List<Driver> drivers = infoDao.listDrivers(datatablesQuery, infoQuery);
		Integer size = infoDao.getDriversSize(datatablesQuery, infoQuery);
		System.out.println(drivers.toString());
		List<DriverVO> lists = new ArrayList<>();
		for (Driver driver : drivers) {
			lists.add(new DriverVO(driver));
			System.out.println("封装后的内容：" + driver.toString());
		}

		DatatablesView<DriverVO> datatablesView = new DatatablesView<>();
		datatablesView.setData(lists);
		datatablesView.setDraw(datatablesQuery.getDraw());
		datatablesView.setRecordsFiltered(size);
		datatablesView.setRecordsTotal(size);
		return datatablesView;
	}

	@Override
	public void saveDriver(Driver driver) {
		infoDao.saveDriver(driver);
		logger.info("新增驾驶员" + driver.getDriNum());
	}

	@Override
	public void updateDriver(Driver driver) {
		infoDao.updateDriver(driver);
		logger.info("更新驾驶员信息" + driver.toString());
	}

	@Override
	public void deleteDriver(Driver driver) {
		infoDao.deleteDriver(driver);
	}

	@Override
	public int getTaxiIdBytaxiNum(InfoQuery infoQuery) {
		return infoDao.getTaxiIdBytaxiNum(infoQuery);
	}

	@Override
	public List<Taxi> getAllTaxi() {
		return infoDao.getAllTaxi();
	}

	@Override
	public List<Insure> getInsure(InfoQuery infoQuery) {
		return infoDao.getInsure(infoQuery);
	}

	@Override
	public void saveInsure(Insure insure) {
		infoDao.saveInsure(insure);
		logger.info("新增保险信息" + insure.toString());
	}

	@Override
	public void updateInsure(Insure insure) {
		infoDao.updateInsure(insure);
		logger.info("更新保险信息" + insure.toString());
	}

	@Override
	public void deleteInsure(Insure insure) {
		infoDao.deleteInsure(insure);
		logger.info("删除保险信息" + insure.toString());
	}

	@Override
	public List<Blacklist> getBlacklist(InfoQuery2 infoQuery) {

		return infoDao.getBlacklist(infoQuery);

	}

	@Override
	public void saveBlacklist(Blacklist blacklist) {
		infoDao.saveBlacklist(blacklist);
		logger.info("新增黑名单信息： " + blacklist.getType() + blacklist.getTaxiNum());
	}

	@Override
	public void updateBlacklist(Blacklist blacklist) {
		infoDao.updateBlacklist(blacklist);
		logger.info("更新黑名单信息" + blacklist.getRemark() + blacklist.getTaxiNum());
	}

	@Override
	public void deleteBlacklist(Blacklist blacklist) {
		infoDao.deleteBlacklist(blacklist);
		logger.info("删除黑名单信息" + blacklist.getRemark() + blacklist.getTaxiNum());
	}

	@Override
	public List<Check> getCheck(InfoQuery2 infoQuery) {
		return infoDao.getCheck(infoQuery);
	}

	@Override
	public void saveCheck(Check check) {
		infoDao.saveCheck(check);
		logger.info("新增稽查信息：" + check.toString());
	}

	@Override
	public void updateCheck(Check check) {
		infoDao.updateCheck(check);
		logger.info("更新稽查信息" + check.toString());
	}

	@Override
	public void deleteCheck(Check check) {
		infoDao.deleteCheck(check);
		logger.info("删除稽查信息" + check.toString());
	}

	@Override
	public List<Fare> getFare(String type) {
		return infoDao.getFare(type);
	}

	@Override
	public void saveFare(Fare fare) {
		infoDao.saveFare(fare);
		logger.info("新增运价信息" + fare.toString());
	}

	@Override
	public void updateFare(Fare fare) {
		infoDao.updateFare(fare);
		logger.info("更新运价信息" + fare.toString());
	}

	@Override
	public void deleteFare(Fare fare) {
		infoDao.deleteFare(fare);
		logger.info("删除运价信息" + fare.toString());
	}

	@Override
	public List<Yearinspect> getYearinspect(InfoQuery2 infoQuery2) {
		return infoDao.getYearinspect(infoQuery2);
	}

	@Override
	public void saveYearinspect(Yearinspect yearinspect) {
		infoDao.saveYearinspect(yearinspect);
		logger.info("新增年审信息" + yearinspect.toString());
	}

	@Override
	public void updateYearinspect(Yearinspect yearinspect) {
		infoDao.updateYearinspect(yearinspect);
		logger.info("更新年审信息" + yearinspect.toString());
	}

	@Override
	public void deleteYearinspect(Yearinspect yearinspect) {
		infoDao.deleteYearinspect(yearinspect);
		logger.info("新增年审信息" + yearinspect.toString());
	}

	@Override
	public List<Breakrule> getBreakrule(InfoQuery2 infoQuery2) {
		return infoDao.getBreakrule(infoQuery2);
	}

	@Override
	public void saveBreakrule(Breakrule breakrule) {
		infoDao.saveBreakrule(breakrule);
		logger.info("新增违章信息" + breakrule.toString());
	}

	@Override
	public void updateBreakrule(Breakrule breakrule) {
		infoDao.updateBreakrule(breakrule);
		logger.info("更新违章信息" + breakrule.toString());
	}

	@Override
	public void deleteBreakrule(Breakrule breakrule) {
		infoDao.deleteBreakrule(breakrule);
		logger.info("删除违章信息" + breakrule.toString());
	}

	@Override
	public void updateIsu(String isu, String isu2) {
		taxiDao.updateIsu(isu, isu2);

	}

	@Override
	public void addToIsucache(Taxi taxi, String isu2, int type) {
		taxiDao.addIsuCache(taxi, isu2, type);
		logger.info("缓存表新增更新记录" + "终端号" + isu2 + "新终端号" + taxi.getIsuNum());
	}

	@Override
	public List<IsuCache> getAllIsuCache() {
		return taxiDao.getAllIsuCache();
	}

	@Override
	public void addPower(Power power) {
		infoDao.addPower(power);

	}

	@Override
	public void deletePowerByID(Integer powerID) {
		infoDao.deletePowerByID(powerID);

	}

	@Override
	public Power getPowerByID(Integer powerID) {

		return infoDao.getPowerByID(powerID);
	}

	@Override
	public List<videoState> getIsuVideoState(String isuNum) {
		// 判断终端号是否存在
		Taxi taxi = taxiDao.getTaxiByTaxi(isuNum);
		String isu = null;
		if (taxi != null) {
			isu = taxi.getIsuNum();
			List<videoState> videoStates = infoDao.getIsuVideoState(isu, isuNum);
			return videoStates;
		} else {
			return null;
		}
		// 调用dao，获取最新记录

	}

	@Override
	public int getTaxiNumber(InfoQuery infoQuery) {
		return infoDao.getTaxiNumber(infoQuery);
	}

	@Override
	public List<Accidenttype> getAllAccidentType() {
		return infoDao.getAllAccidentType();
	}

	@Override
	public void updateAccidentType(Accidenttype accidenttype) {
		infoDao.updateAccidentType(accidenttype);
	}

	@Override
	public void deleteAccidentType(Accidenttype accidenttype) {
		infoDao.deleteAccidentType(accidenttype);
	}

	@Override
	public List<MeterState> getMeterState(String taxiNum) {
		// 不加判断条件，不判断公司
		List<MeterState> meterStates = infoDao.getMeterStates(taxiNum);
		return meterStates;
	}

	@Override
	public List<TerminalState> getTerminalState(String taxiNum) {

		// 车牌号转终端号
		String[] choose = taxiNum.split(",");
		List<String> taxiNums = new ArrayList<>();
		for (int i = 0; i < choose.length; i++) {
			if (choose[i].length() == 7)
				taxiNums.add(choose[i].substring(1));
			else if (choose[i].length() == 8)
				taxiNums.add(choose[i].substring(1, 2) + choose[i].substring(3));
		}
		List<String> isuNums = new ArrayList<>();
		// System.out.println("查询到的车辆终端状态taxiNums：" + taxiNums.toString());
		String a = null;
		for (String taxiNum1 : taxiNums) {
			a = taxiDao.getIsuByTaxiNum2(taxiNum1);
			if (a != null) {
				isuNums.add(a);
			}
		}
		// System.out.println("查询到的车辆终端状态isuNums：" + isuNums.toString());

		// 查询状态信息
		List<TerminalState> terminalStates = infoDao.getTerminalStates(isuNums);
		List<String> arrayList=Arrays.asList(choose);
		ArrayList<String> results = new ArrayList<String>(arrayList);
		// 添加车牌号
		if(terminalStates!=null&&terminalStates.size()>0){
			for (TerminalState terminalState : terminalStates) {
				Taxi t = taxiDao.getTaxiByIsu(terminalState.getIsuNum());
				terminalState.setTaxiNum(t.getTaxiNum());
				results.remove(t.getTaxiNum());
			}
		} 
		//填充从未上线的终端
		if(results.size()>0){
			for (int i = 0; i < results.size(); i++) {
				TerminalState terminalState=new TerminalState();
				terminalState.setTaxiNum(results.get(i));
				terminalState.setIsuNum(taxiDao.getIsuByTaxiNum(results.get(i)));
				terminalState.setTime("从未上线");
				terminalState.setState("从未上线");
			}
		}
		return terminalStates;

	}
	@Override
	public void ApproalDriverById(Integer id ,String state) {
		if(state.equalsIgnoreCase("1")){
			infoDao.ApproalDriverById(id,true);
		}else{
			infoDao.ApproalDriverById(id,false);
		}
	}
}
