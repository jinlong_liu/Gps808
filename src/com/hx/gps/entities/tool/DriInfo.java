package com.hx.gps.entities.tool;

public class DriInfo {
	private int driId;//司机id
	private String driName;//司机姓名
	public int getDriId() {
		return driId;
	}
	public void setDriId(int driId) {
		this.driId = driId;
	}
	public String getDriName() {
		return driName;
	}
	public void setDriName(String driName) {
		this.driName = driName;
	}
	
	public DriInfo() {
		super();
	}
	public DriInfo(int driId, String driName) {
		super();
		this.driId = driId;
		this.driName = driName;
	}
	@Override
	public String toString() {
		return "DriInfo [driId=" + driId + ", driName=" + driName + "]";
	}
	
}
