package com.hx.gps.dao;

import com.hx.gps.util.DBUtil;
import com.hx.gps.util.FarmConstant;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

public class SerialDao {
	public static int getSerialNum(){
		int serialNum = 0;
		DBCollection serialDao = DBUtil.getDBCollection(FarmConstant.MONGODB, 
				"serial");
		//查询
		DBObject document = serialDao.findOne();  
		if (document==null) {
			serialDao.insert(new BasicDBObject("serialNum",0));
		}else{
			serialNum = (int)document.get("serialNum");
			//更新
			if(serialNum==65535){
				serialDao.update(new BasicDBObject("serialNum",serialNum), new BasicDBObject("serialNum",0));
			}else{
				serialDao.update(new BasicDBObject("serialNum",serialNum), new BasicDBObject("serialNum",serialNum+1));
			}
		}  
		return serialNum;
	}
}
