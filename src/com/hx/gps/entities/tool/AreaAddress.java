package com.hx.gps.entities.tool;

import com.hx.gps.util.DateUtil;
import com.mongodb.BasicDBObject;

/**
 * @author 作者 dream:
 * @version 创建时间：2017年8月5日 下午2:53:31 天道丶放飞梦想
 */
// 进出区域专属工具类
public class AreaAddress {
	private String isuNum;
	private String taxiNum; // 数据库中不包含车牌号，添加该属性只是为了方便数据的传输
	private String state; // 数据库中不包含，用于显示车辆的在线状态
	private String locateState; // 数据库中不包含，表示定位状态
	private String operateState;// 数据库中不包含，运营状态
	private int serialNum;
	private String alarm;
	private double longitude;
	private double latitude;
	private float speed;
	private int direction;
	private String time;
	// 区域报警信息
	private String addType; // 进出区域/路线报警位置类型
	private int areaId; // 进出区域/路段id
	private String aresDirection;// 进出区域/路线区域方向

	public String getIsuNum() {
		return isuNum;
	}

	public void setIsuNum(String isuNum) {
		this.isuNum = isuNum;
	}

	public String getTaxiNum() {
		return taxiNum;
	}

	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getLocateState() {
		return locateState;
	}

	public void setLocateState(String locateState) {
		this.locateState = locateState;
	}

	public String getOperateState() {
		return operateState;
	}

	public void setOperateState(String operateState) {
		this.operateState = operateState;
	}

	public int getSerialNum() {
		return serialNum;
	}

	public void setSerialNum(int serialNum) {
		this.serialNum = serialNum;
	}

	public String getAlarm() {
		return alarm;
	}

	public void setAlarm(String alarm) {
		this.alarm = alarm;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getAddType() {
		return addType;
	}

	public void setAddType(String addType) {
		this.addType = addType;
	}

	public int getAreaId() {
		return areaId;
	}

	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}

	public String getAresDirection() {
		return aresDirection;
	}

	public void setAresDirection(String aresDirection) {
		this.aresDirection = aresDirection;
	}

	public AreaAddress() {
	}

	/*
	 * //普通位置信息 public AreaAddress(BasicDBObject finds) { super(); this.isuNum =
	 * finds.getString("isuNum"); this.serialNum = finds.getInt("serialNum");
	 * this.alarm = finds.getString("alarm"); this.state =
	 * finds.getString("state"); this.latitude = finds.getDouble("latitude");
	 * this.longitude = finds.getDouble("longitude"); this.speed =
	 * (float)finds.getDouble("speed"); this.direction =
	 * finds.getInt("direction"); this.time =
	 * DateUtil.dateToString(finds.getDate("time")); }
	 */
	// 区域报警信息
	public AreaAddress(BasicDBObject finds) {
		super();
		this.isuNum = finds.getString("isuNum");
		this.serialNum = finds.getInt("serialNum");
		this.alarm = finds.getString("alarm");
		this.state = finds.getString("state");
		this.latitude = finds.getDouble("latitude");
		this.longitude = finds.getDouble("longitude");
		this.speed = (float) finds.getDouble("speed");
		this.direction = finds.getInt("direction");
		this.time = DateUtil.dateToString(finds.getDate("time"));
		// 设置区域类型
		switch (finds.getInt("addType")) {
		case 0:
			this.setAddType("圆形");
			break;
		case 1:
			this.setAddType("矩形");
			break;
		case 2:
			this.setAddType("线段");
			break;

		case 3:
			this.setAddType("多边形");
			break;
		// 设置默认值
		default:
			this.setAddType("多边形");
			break;
		}
		// 设置区域id
		this.setAreaId(finds.getInt("areaId"));
		// 设置区域方向 aresDirection
		
		switch (finds.getInt("aresDirection")) {
		case 0:
			this.setAresDirection("进区域");
			break;
		case 1:
			this.setAresDirection("出区域");
			break;
		case 2:
			this.setAresDirection("在边界上");
			break;
		// 设置默认值
		default:
			this.setAresDirection("未知");
			break;
		}

	}

	// toString
	@Override
	public String toString() {
		return "AreaAddress [isuNum=" + isuNum + ", taxiNum=" + taxiNum + ", state=" + state + ", locateState="
				+ locateState + ", operateState=" + operateState + ", serialNum=" + serialNum + ", alarm=" + alarm
				+ ", longitude=" + longitude + ", latitude=" + latitude + ", speed=" + speed + ", direction="
				+ direction + ", time=" + time + ", addType=" + addType + ", areaId=" + areaId + ", aresDirection="
				+ aresDirection + "]";
	}

}
