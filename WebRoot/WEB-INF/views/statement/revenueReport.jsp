<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>H+ 后台主题UI框架 - 百度ECHarts</title>
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="css/plugins/jsTree/style.min.css" rel="stylesheet">
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">

<!-- <link rel="shortcut icon" href="favicon.ico">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="css/plugins/jsTree/style.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet"> -->
<!-- <base target="_blank"> -->

<style>
.jstree-open>.jstree-anchor>.fa-folder:before {
	content: "\f07c"
}

.jstree-default .jstree-icon.none {
	width: 0
}
</style>

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-9">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>查看营收信息</h5>
					 
					</div>
					<div class="ibox-content" style="height:780px;  overflow-y: auto;">
						<div class="pull-right">
							<form action="revenueReport" class="form-inline" method="post" id="revenueForm">
							开始时间：&nbsp;&nbsp; <input type="text" class="form-control" name="startDate" id="startDate"
							value="${startDate}" class="Wdate"
							onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true})">
						&nbsp;&nbsp;
						结束时间：&nbsp;&nbsp; <input type="text" name="endDate" id="endDate"
							  value="${endDate}" class="form-control" class="Wdate"
							onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'startDate\')}',dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true})">
							&nbsp;&nbsp;
							
							<input type="hidden" id="taxiNums" name="taxiNums">
							<input type="button" class="btn btn-primary btn-sm-2" value="查看" onclick="submitForm()">
							&nbsp;&nbsp;
							<input type="button" class="btn btn-primary" id="demo1" value="导出表格">  
							</form>
						</div> 
						
						<div id="miletable">
						 
							<table class="footable table table-stripped" data-page-size="6"
						data-filter=#filter>
						 <c:if test="${not empty compInfo}">
						<center><h2>营收汇总表</h2></center>
						</c:if> 
						<tbody>
							<c:forEach items="${compInfo}" var="compInfo">
								<tr id="${compInfo.comId}">
									<td ><h3><b>公司名称：</b></h3></td>
									<td ><h3><b>${compInfo.cname}</b></h3></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									</tr>
									<c:forEach items="${compInfo.taxis}" var="taxis">
										<tr id="${taxis.taxiId}">
										<td>车牌号：</td>
										<td>${taxis.taxiNum}</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										</tr>
										<c:if test="${not empty taxis.revenues}">
										<tr>
											<td>营收日期</td>
											<td>营收次数</td>
											<td>空驶里程</td>
											<td>载客里程</td>
											<td>租金</td>
											<td>合租次数</td>
										</tr>
										</c:if>
											<c:forEach items="${taxis.revenues}" var="revenues">
											<tr>
											<td width="200px">${revenues.date}</td>
											<td>${revenues.number}</td>
											<td>${revenues.emptyMile}</td>
											<td>${revenues.mileUtil}</td>
											<td>${revenues.money}</td>
											<td>${revenues.roommate}</td>
											</tr>
											</c:forEach>
									</c:forEach>
								
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="6">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
						</div>
					</div>
				</div>
			</div>
			<!--右侧公司选择栏开始  -->
			<div class="col-sm-3" >
				<div class="ibox float-e-margins">
				<div class="ibox-title">
						<h5>公司选择</h5>
					</div>
					<div class="ibox-content" style="height:780px; ">
						<input style="width:65%" type="text" class="form-control form-control-sm" id="txtIndustryArea">
						 <br> 
						 
						<div id="jstree1" class="trdemo" style="overflow-y: auto; height: 500px;">
							<ul>
								<li class="jstree-open trdemo">所有公司
									<ul>
										<c:forEach items="${comInfo2s}" var="comInfo2s">
											<li class="trdemo" id="${comInfo2s.comId}">${comInfo2s.cname }
												<ul>
													<c:forEach items="${comInfo2s.taxis}" var="taxis">
														<li class="trdemo" id="${taxis.taxiNum}"
															data-jstree='{"type":"car"}'>
															${taxis.taxiNum }
														</li>
													</c:forEach>
												</ul>
											</li>
										</c:forEach>
									</ul>
								</li>
							</ul>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- 导出表格表单 -->
		 <form action="getRevenueExcel" id="revenuePoi" method="post">
			<input type="hidden" name="taxiNum" id="taxiNumPoi"> <input
				type="hidden" name="startDate" id="startDatePoi"> <input
				type="hidden" name="endDate" id="endDatePoi">
		</form> 
	</div>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/echarts/echarts-all.js"></script>
	<script src="js/content.min.js?v=1.0.0"></script>
	<script src="js/demo/echarts-demo2.min.js"></script>
	<script src="js/plugins/jsTree/jstree.min.js"></script>
	<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
	<script type="text/javascript"
		src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>	
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script><!-- 包含部分提示弹出框的样式 -->
	
	<script type="text/javascript">
		function submitForm(){
			var startDate=$("#startDate").val();
			var endDate=$("#endDate").val();
			var taxiNums=$("#taxiNums").val();
			if(startDate==""||endDate==""){
				swal({
					title :"时间不能为空",
					type : "warning"
				});
			}else if(taxiNums==""){
				swal({
					title :"请选择要查询的车辆",
					type : "warning"
				});
			}else{
				$("#revenueForm").submit();
			}
		}
		//开始查询数据
		/* function check(){
			var companys=$("#taxiNums").val();
			var startDate=$("#startDate").val();
			var endDate=$("#endDate").val();
			//alert(type+companys);
			if(companys!=""){
				var args = {
					"companys" : companys,
					"startDate" : startDate,
					"endDate" : endDate
				};
				var url = "mileageUtil";
				$.post(url,args,function(data){
					var newdata = eval("(" + data + ")");
					//alert(data);
					document.getElementById('totalMileage').innerHTML=newdata.totalMileage;
					document.getElementById('mileageUtil').innerHTML=newdata.mileageUtil;
					document.getElementById('avgmileage1').innerHTML=newdata.totalMileage/newdata.taxiNumber/newdata.dayNumber;	//单车日均营运
					document.getElementById('avgmileage2').innerHTML=newdata.mileageUtil/newdata.taxiNumber/newdata.dayNumber;	//单车日均载客
					document.getElementById('rate').innerHTML=newdata.rate+"%";
					document.getElementById('taxiNumber').innerHTML="该表统计了"+newdata.taxiNumber+"辆汽车在"+newdata.dayNumber+"天内的行程记录";
					//$("#totalMileage").val(newdata.totalMileage);
				});
				$("#miletable").show();
			}
			
			
		} */
		
		//选择公司开始
		$(document).ready(function() {
			/* $("#miletable").hide(); */
			$("#taxiNums").val("");
			
			$("#jstree1").jstree({
				"core" : {
					"check_callback" : true
				},
				"plugins" : [ "types", "dnd", "checkbox", "search" ],
				"types" : {
					"car" : {
						"icon" : "fa fa-car"
					},
					"default" : {
						"icon" : "fa fa-folder"
					}
				}
			});
			
			//动态获取选中节点的id
			$("#jstree1").on('changed.jstree', function(e, data) {
				r = [];
				var i, j;
				for (i = 0, j = data.selected.length; i < j; i++) {
					var node = data.instance.get_node(data.selected[i]);
					if (data.instance.is_leaf(node)) {
						r.push(node.id);
					}
				}
				$("#taxiNums").val(r);
			}).on("search.jstree", function (e, data) {
				if(data.nodes.length) {
					var matchingNodes = data.nodes; // change
					$(this).find(".jstree-node").hide().filter('.jstree-last').filter(function() { return this.nextSibling; }).removeClass('jstree-last');
					data.nodes.parentsUntil(".jstree").addBack().show()
						.filter(".jstree-children").each(function () { $(this).children(".jstree-node:visible").eq(-1).addClass("jstree-last"); });
					// nodes need to be in an expanded state
					matchingNodes.find(".jstree-node").show(); // change
				}
			}).on("clear_search.jstree", function (e, data) {
				if(data.nodes.length) {
					$(this).find(".jstree-node").css("display","").filter('.jstree-last').filter(function() { return this.nextSibling; }).removeClass('jstree-last');
				}
			});
			
			//jsTree的模糊查询
			var to = false;
			$("#txtIndustryArea").keyup(function() {
				if (to) {
					clearTimeout(to);
				}
				to = setTimeout(function() {
					var v = $("#txtIndustryArea").val();
					var temp = $("#jstree1").is(":hidden");
					if (temp == true) {
						$("#jstree1").show();
					}
					$("#jstree1").jstree(true).search(v);
				}, 250);
			});
		});
		//点击"导出表格"按钮
		$("#demo1").click(
				function() {
					var a = '<%=(String)request.getParameter("taxiNums")%>';
					var startTime = $("#startDate").val();
					var endTime = $("#endDate").val();
					var nowTime = $("#endDate").val();//页面显示的结束时间
					var taxiNum = a;
					if (nowTime == "" || nowTime == null) {
						nowTime = "现在时刻";
					}
					if (startTime == "" || taxiNum == "") {
						swal({
							title : "导出表格时，开始时间和车牌号不能为空",
							type : "warning"
						});
					} else {
						swal({
							title : "",
							text : "您确定导出当前的表格数据吗?",
							type : "warning",
							showCancelButton : true,
							cancelButtonText : "取消"
						}, function() {
							//赋值
							$("#startDatePoi").val(
									startTime);
							$("#endDatePoi").val(endTime);
							$("#taxiNumPoi").val(taxiNum);
							//提交表单
							$("#revenuePoi").submit();
						});
					}

				});

	</script>
</body>

</html>