package com.hx.gps.entities.tool;

import java.util.Date;

import com.hx.gps.util.DateUtil;
import com.mongodb.BasicDBObject;

public class OrderVector {
	// 添加车牌号
	private String taxiNum;

	// 添加时间（包含时分秒）
	private String time;
	// 命令序号（流水号，每日更新）
	private int serialNum;
	// 终端号（isu）
	private String isuNum;
	// 命令类型
	private String orderType;
	// 标识（命令类型）
	private String identify;
	// 发送人
	private String people;
	// 内容
	private String context;
	// 状态
	private String state;
	// 发送次数
	private int sendTime;

	public String getTaxiNum() {
		return taxiNum;
	}

	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getSerialNum() {
		return serialNum;
	}

	public void setSerialNum(int serialNum) {
		this.serialNum = serialNum;
	}

	public String getIsuNum() {
		return isuNum;
	}

	public void setIsuNum(String isuNum) {
		this.isuNum = isuNum;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getIdentify() {
		return identify;
	}

	public void setIdentify(String identify) {
		this.identify = identify;
	}

	public String getPeople() {
		return people;
	}

	public void setPeople(String people) {
		this.people = people;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getSendTime() {
		return sendTime;
	}

	public void setSendTime(int sendTime) {
		this.sendTime = sendTime;
	}

	public OrderVector() {
		super();
	}

	public OrderVector(BasicDBObject finds) {
		super();
		this.time ="未获取到..";
		if (finds.containsField("time")) {
			if (finds.getDate("time") != null) {
				this.time = DateUtil.dateToString(finds.getDate("time"));
			}
		}
		this.serialNum = finds.getInt("serialNum");
		this.isuNum = finds.getString("isuNum");
		this.orderType = finds.getString("orderType");
		this.identify = finds.getString("identify");
		this.people = finds.getString("people");
		this.context = finds.getString("context");
		this.state = changeState(finds.getInt("state"));
		this.sendTime = finds.getInt("sendTime");
	}

	private String changeState(Integer i) {
		String str = null;
		switch (i) {
		case 0:
			str = "等待发送";
			break;
		case 1:
			str = "发送成功";
			break;
		case 2:
			str = "发送失败";
			break;
		case 3:
			str = "发送中或消息有误";
			break;
		case 4:
			str = "离线";
			break;
		case 5:
			str = "执行失败";
			break;
		case 6:
			str = "Acc未关闭";
			break;
		case 7:
			str = "Gps速度不为0";
			break;
		default:
			str = "状态异常";
			break;
		}
		return str;
	}

	@Override
	public String toString() {
		return "OrderVector [taxiNum=" + taxiNum + ", time=" + time + ", serialNum=" + serialNum + ", isuNum=" + isuNum
				+ ", orderType=" + orderType + ", identify=" + identify + ", people=" + people + ", context=" + context
				+ ", state=" + state + ", sendTime=" + sendTime + "]";
	}

}
