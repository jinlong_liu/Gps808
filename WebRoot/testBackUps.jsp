<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <title>视频监控页面</title>
    <meta charset="utf-8">
    <!--设置浏览器可以等比例缩放 -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--设置主地址栏颜色  -->
    <meta name="theme-color" content="#000000">
    <meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
    <meta name="description"
          content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">
    <link href="css/plugins/footable/footable.core.css" rel="stylesheet">
    <link rel="shortcut icon" href="favicon.ico">
    <link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <style>
        /* 解决不能全屏视频的问题 */
        /* .vedio1 video {
            position: absolute;
            height: 100%;
            width: 100%;
        }
        :-webkit-full-screen video {
          width: 100%;
          height: 100%;
        } */
    </style>
    <link href="css/video.css" rel="stylesheet">
    <base target="_blank">
</head>
<body>
<div class="col-sm-2" id="playvedio" >
    <!--选择回放模式:直接跳转内嵌页面 -->
    <br />
    <shiro:lacksPermission name="history:admin">
        <input type="button"
               style="background-color: #2ae0c8; font-weight: bold; font-size: 16px; color: white; width: 100%; margin-top: -15px; height: 30px"
               class="btn " value="点击进入  [视频回放] 页面" onclick="reshow()">
    </shiro:lacksPermission>
    <br />
    <!-- 最右侧视频1 视频2-->
    <div class="ibox float-e-margins te">
        <div id="vedio1"
             style="width: 100%; position: relative; padding-top: 52.25%; height: 0;">
            <!--   <a class="J_menuItem" onclick="vedio1()">  -->
            <video
                    style="position: absolute; top: 0; left: 0; width: 100%; height: 100%"
                    id="my_video_1" class="video-js vjs-default-skin " controls
                    preload='auto' data-setup='{}'>
                <source id="v1" src="${vedio1}" type="application/x-mpegURL">
                <!--http://040201389516.xstrive.com:8080/newhls/record/record0/index.m3u8  -->
                <!--"http://ivi.bupt.edu.cn/hls/cctv6hd.m3u8"  -->
            </video>
            <!-- </a> -->
        </div>
    </div>
    <br />
    <div class="ibox float-e-margins te">
        <div id="vedio2"
             style="width: 100%; position: relative; padding-top: 56.25%; height: 0;">
            <!--   <a class="J_menuItem" onclick="vedio1()">  -->
            <video
                    style="position: absolute; top: 0; left: 0; width: 100%; height: 100%"
                    id="my_video_2" class="video-js vjs-default-skin" controls
                    preload="auto" data-setup='{}'>
                <!--<source src="./src/z.m3u8" type="application/x-mpegURL">-->
                <source id="v2" src="${vedio2}" type="application/x-mpegURL">
            </video>
            <!-- </a> -->
        </div>
    </div>
    <br />
    <div class="ibox float-e-margins te">
        <div id="vedio3"
             style="width: 100%; position: relative; padding-top: 56.25%; height: 0;">
            <!--   <a class="J_menuItem" onclick="vedio1()">  -->
            <video
                    style="position: absolute; top: 0; left: 0; width: 100%; height: 100%"
                    id="my_video_3" class="video-js vjs-default-skin" controls
                    preload="auto" data-setup='{}'>
                <!--<source src="./src/z.m3u8" type="application/x-mpegURL">-->
                <source id="v3" src="${vedio3}" type="application/x-mpegURL">
            </video>
            <!-- </a> -->
        </div>
    </div>
    <br />
    <div class="ibox float-e-margins te">
        <div id="vedio4"
             style="width: 100%; position: relative; padding-top: 56.25%; height: 0;">
            <!--   <a class="J_menuItem" onclick="vedio1()">  -->
            <video
                    style="position: absolute; top: 0; left: 0; width: 100%; height: 100%"
                    id="my_video_4" class="video-js vjs-default-skin" controls
                    preload="auto" data-setup='{}'>
                <!--<source src="./src/z.m3u8" type="application/x-mpegURL">-->
                <source id="v4" src="${vedio4}" type="application/x-mpegURL">
            </video>
            <!-- </a> -->
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="js/plugins/gritter/jquery.gritter.min.js"></script>
<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="js/bootstrap.min.js?v=3.3.5"></script>
<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="js/plugins/footable/footable.all.min.js"></script>
<script src="js/plugins/jsTree/jstree.min.js"></script>
<script src="js/video.js"></script>
<script src="js/videojs-live.js"></script>
<script type="text/javascript">

    function reshow(){
        /*
        var url = "getIsuNumBycar";
        var args = {
        "taxiNum" : taxiNums
        };
          $.post(url, args, function(data) {
            //后台查车辆的终端号。
         var a=eval('('+data+')');
             if(a!==null){
                 //后期根据车牌号拼接字符串，目前测试直接输入默认地址就可以。
            // window.location.href="http://"+序列号+".xstrive.com:8080";
                 window.location.href="http://defaultRRR.xstrive.com:8080";
             }else{
                 swal({
                        title : "车牌号格式有误",
                        type : "warning"
                    });
                 return;
             }
            //return;
        }); */
        //实现跳转
        //window.location.href = "http://defaultRRR.xstrive.com:8080";
        var an = '<%=request.getAttribute("vedio0")%>';
        /*先指定回放车辆  */
        if (an == null || an == "" || an == "null") {
            alert("请先指定回放车辆");
        } else {
            // 指定父窗口跳转地址。
            var storage=window.localStorage;
            storage.setItem("url",an);
            storage.setItem("isu",an.substr(7,12));
            // window.top.href="timeTest.jsp";
            parent.open("timeTest.jsp");
            //  parent.location.href = an;
        }
    }
</script>
</body>

</html>
