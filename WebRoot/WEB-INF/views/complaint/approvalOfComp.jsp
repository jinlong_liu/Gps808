<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">

<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">


<!-- 点击"修改"按钮弹出来的form表单 -->
					<div class="modal inmodal fade" id="myModal1" tabindex="-1"
						role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<form action="approvalComp" method="post">
									<div class="modal-body">
										<div class="ibox-title">
											<h5>添加审批不通过原因</h5>
										</div>
										<div class="ibox-content">
											<div class="row">
												<div class="col-sm-6">
													<input type="hidden" name="complaintId" id="complaintId">
													<input type="text" class="form-control" name="apprOpinion" id="apprOpinion">
												</div>
											</div>
											<br>
										</div>
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-white"
											data-dismiss="modal">取消</button>
											<button type="button" class="btn btn-primary"
											data-dismiss="modal" onclick="getVal(0)">提交</button>
										<!-- <button type="submit" class="btn btn-primary">保存</button> -->
									</div>
								</form>
							</div>
						</div>
					</div>
		<div class="row">

			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>投诉审批表</h5>
				</div>
				<div class="ibox-content" style="height: 780px">
					<table  id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
						<thead>
							<tr>
								<th>公司名称</th>
								<th>车牌号</th>
								<th>司机姓名</th>
								<th>投诉类型</th>
								<th>投诉事由</th>
								<th>投诉内容</th>
								<th>上车地点</th>
								<th>下车地点</th>
								<th>司机得分</th>
								<th>车辆得分</th>
								<th>公司得分</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${complaint}" var="complaint">
								<tr id="${complaint.id}">
									<td>${complaint.taxi.company.name}</td>
									<td>${complaint.taxi.taxiNum}</td>
									<td>${complaint.driver.dname}</td>
									<td>${complaint.comptype.type}</td>
									<td>${complaint.reportReason}</td>
									<td>${complaint.reportContent}</td>
									<td>${complaint.startPlace}</td>
									<td>${complaint.endPlace}</td>
									<td>${complaint.deduct.scorer}</td>
									<td>${complaint.deduct.scoreTaxi}</td>
									<td>${complaint.deduct.scorePany}</td>
									<td>
											<button type="button" class="btn btn-success btn-sm"
											onclick="submit('${complaint.id}',1)">批准</button>
									<button id="btn-update" type="button"
											class="btn btn-danger btn-sm" data-toggle="modal"
										data-target="#myModal1"
											onclick="alterScore('${complaint.id}')">不批准</button>
								</td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="12">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
				<form id="deleteForm" action="approvalComp" method="post">
					<input type="hidden" id="approvalId" name="id">
					<input type="hidden" id="approvalState" name="state">
					<input type="hidden" id="approvalOpinion" name="apprOpinion">
				</form>
			</div>
		</div>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script>
	$(document).ready(function() {
		$('#table1').DataTable({
			"pagingType" : "full_numbers"
		});
		var result='<%=request.getAttribute("result")%>';
		if(result != null && result != "null"){
			if (result.indexOf("成功")>0) {
			swal({
				title : result,
				type : "success"
			});
		}else{
			swal({
				title :result,
				type : "warning"
			});
		}
		}

	});
	 
		function alterScore(complaintId) {
			$("#complaintId").val(complaintId);
		};
	//不批准之后执行的方法：	
		function getVal(state){
			var id=$("#complaintId").val();
			var apprOpinion=$("#apprOpinion").val();
		//	deal(id,state,apprOpinion);
			$("#approvalId").val(id);
			$("#approvalState").val(state);
			$("#approvalOpinion").val(apprOpinion);
			//提交表单
			$("#deleteForm").submit();
		}
		
		function deal(id,state,apprOpinion){
			$("#"+id).hide();
			var args = {
					"id":id,
					"state":state,
					"apprOpinion":apprOpinion,
				};
				var url="approvalComp";
			$.post(url, args, function(data) {
				var a  = String(data);
				 if(a=="操作执行成功"){
					  swal({
			                title: data,
			                type: "success"
			            });
				}else{
					  swal({
			                title: data,
			                type: "warning"
			            });
					$("#"+id).show();
				} 
				 
			});
		}
		
		function submit(id,state) {
			swal({
				title : "您确定执行该操作吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			},function() {
			//deal(id,state,null);
			 	// delect(id);
				$("#approvalId").val(id);
				$("#approvalState").val(state);
				$("#approvalOpinion").val("");
				 // 提交表单
				$("#deleteForm").submit();
			});
		}
	</script>
</body>

</html>