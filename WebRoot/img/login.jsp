<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>


<title>莱芜出租车智能服务平台</title>
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link rel="shortcut icon" href="favicon.ico">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">

<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min_login.css?v=4.0.0" rel="stylesheet">
<!-- 打开一个新的界面 -->
<!--  <base target="_blank"> -->
<!--[if lt IE 8]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
<!-- 让登录界面跳出当前界面 -->
<script>
	if (window.top !== window.self) {
		window.top.location = window.location;
	}
</script>
</head>

<body class="gray-bg">
<div class="wenzi">

<div class="carbody" >
	<div class="car">
		<div class="body">
			<div class="mirror-wrap">
				<div class="mirror-inner">
					<div class="mirror">
						<div class="shine"></div>
					</div>
				</div>
			</div>
			<div class="middle">
				<div class="top">
					<div class="line"></div>
				</div>
				<div class="bottom">
					<div class="lights">
						<div class="line"></div>
					</div>
				</div>
			</div>
			<div class="bumper">
				<div class="top"></div>
				<div class="middle" data-numb="LW"></div>
				<div class="bottom"></div>
			</div>
		</div>
		<div class="tyres">
			<div class="tyre back"></div>
			<div class="tyre front"></div>
		</div>
	</div>
	<div class="road-wrap">
		<div class="road">
			<div class="lane-wrap">
				<div class="lane">
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
				</div>
			</div>
		</div>
	</div>
  </div>	

 <span>莱</span><span>芜</span><span>出</span><span>租</span><span>车</span><span>智</span><span>能</span><span>终
</span><span>端</span><span>服</span><span>务</span><span>平</span><span>台</span></div>
	<div class="middle-box text-center loginscreen  animated fadeInDown">
		<div>
			<div>
				<h1 class="logo-name">LW</h1>
			</div>
			<c:if test="${page eq 'page' }"><h3>欢迎使用</h3></c:if>
			<c:if test="${page eq 'error' }"><h3 style="color:red">用户名或密码错误</h3></c:if>
			<c:if test="${state eq 'forbid' }"><h3 style="color:red">该用户已被禁用</h3></c:if>
			
			<!-- class="m-t"  role="form" -->
			<form action="login" method="post">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="用户名"
						required="" name="account" autocomplete="off">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" placeholder="密码"
						required="" onfocus="this.type='password'" name="password" autocomplete="off">
				</div>
				<button type="submit" class="btn btn-primary block full-width m-b">登
					录</button>

				<!-- 
                <p class="text-muted text-center"> <a href="login.html#"><small>忘记密码了？</small></a> | <a href="register.html">注册一个新账号</a>
                </p> -->

			</form>
		</div>
	</div>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script type="text/javascript"
		src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
</body>

</html>
