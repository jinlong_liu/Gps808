<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<script src="js/jquery.js"></script>

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">

<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>

<!-- gy 添加表单验证 -->
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />
<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>
<script src=" js/select2.js"></script>

<!-- fileinput -->
<link
	href="${pageContext.request.contextPath}/css/fileinput/fileinput.css"
	rel="stylesheet">
<script
	src="${pageContext.request.contextPath}/js/fileinput/fileinput.js"></script>
<script src="${pageContext.request.contextPath}/js/fileinput/zh.js"></script>
<link href=" css/select2.css" rel="stylesheet" />

<style>
/*防止select2不会自动失去焦点*/
.select2-container {
	z-index: 16000 !important;
}

.select2-drop-mask {
	z-index: 15990 !important;
}

.select2-drop-active {
	z-index: 15995 !important;
}
/*select2在Bootstrap的modal中默认被遮盖，现在强制显示在最前*/
.select2-drop {
	z-index: 10050 !important;
}

.select2-search-choice-close {
	margin-top: 0 !important;
	right: 2px !important;
	min-height: 10px;
}

.select2-search-choice-close:before {
	color: black !important;
}
</style>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">
		<!-- 查询列表 -->
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>输入条件查询从业人员信息</h5>
				</div>

				<div class="ibox-content">
					<form action="getDriver" class="form-horizontal" method="post"
						id="selectForm">
						<div class="row">
							<div class="form-group">
								<label class="col-sm-1 control-label">车牌号</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" name="taxiNum"
										placeholder="请输入7位的车牌号" id="taxiNumSelect" autocomplete="off"
										value="${taxiNumSelect}">
								</div>

								<label class="col-sm-1 control-label">资格证号</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" name="quaNum" autocomplete="off"
										value="${quaNumSelect}" id="quaNumSelect">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group">
								<label class="col-sm-1 control-label">姓名</label>
								<div class="col-sm-4">
									<input type="text" class="form-control"
										id="tables.fnDraw(false);//刷新保持分页状态" name="dname" autocomplete="off"
										value="${dnameSelect}">
								</div>
								<label class="col-sm-1 control-label"></label>
								<div class="col-sm-5">
									<button type="button" class="btn btn-primary"
										onclick="delAll()">清空查询条件</button>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<button id="selectBtn" type="button" class="btn btn-primary"
										onclick="listDrivers()">查询</button>
									&nbsp;&nbsp;&nbsp;&nbsp;

									<shiro:hasPermission name="staff:admin">
										<button id="btn_addDriver" type="button"
											class="btn btn-primary" data-toggle="modal"
											data-target="#myModal5">添加</button>
									</shiro:hasPermission>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!--gy修改后 点击添加按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal5" role="dialog"
			aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form action="saveDriver" class="form-horizontal" method="post"
						id="saveForm">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>
									新增从业人员信息<span style="color: red">（* 为必填项）</span>
								</h5>
							</div>
							<div class="ibox-content">
								<!-- 第一行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 公司 -->
											<label class="col-sm-6 control-label"> <span
												style="color: red">* </span>公司 ：
											</label>
											<div class="col-sm-6">
												<select class="form-control" name="company.id" id="saveCom">
													<c:forEach items="${companies}" var="companies">
														<option value="${companies.id }">${companies.name }</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
									<!-- 车牌号 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"> <span
												style="color: red">* </span>车牌号 ：
											</label>
											<div class="col-sm-7">
												<select class="form-control js-example-basic-single"
													name="taxi.id" id="saveTaxi">
													<c:forEach items="${taxis}" var="taxis">
														<option value="${taxis.id}">${taxis.taxiNum }</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
								</div>
								<!-- 第二行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 姓 名 -->
											<label class="col-sm-6 control-label"> <span
												style="color: red">* </span>姓 名 ：
											</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" name="dname" autocomplete="off"
													id="saveName">
											</div>
										</div>
									</div>

									<!-- 年 龄 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"> <span
												style="color: red">* </span>年龄 ：
											</label>
											<div class="col-sm-7">
												<input type="text" name="age" class="form-control" autocomplete="off">
											</div>
										</div>
									</div>
								</div>
								<!-- 第三行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 性 别 -->
											<label class="col-sm-6 control-label"> <span
												style="color: red">* </span>性别 ：
											</label>
											<div class="col-sm-6">
												<!-- 													<input type="text"  class="form-control" name="sex"> -->
												<select name="sex" class="form-control">
													<option value="男">男</option>
													<option value="女">女</option>
												</select>
											</div>
										</div>
									</div>
									<!-- 联 系 电 话 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"> <span
												style="color: red">* </span>联系电话 ：
											</label>
											<div class="col-sm-7">
												<input type="text" name="phone" class="form-control" autocomplete="off">
											</div>
										</div>
									</div>
								</div>

								<!-- 第四行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 身 份 证 号 -->
											<label class="col-sm-6 control-label"> <span
												style="color: red">* </span>身份证号 ：
											</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" name="identityCard" autocomplete="off">
											</div>
										</div>
									</div>
									<!-- 准 驾 车 型  -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"> <span
												style="color: red">* </span>准驾车型 ：
											</label>
											<div class="col-sm-7">
												<input type="text" name="qdType" class="form-control" autocomplete="off">
											</div>
										</div>
									</div>
								</div>

								<!-- 第五行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 从业资格证发放机构  -->
											<label class="col-sm-6 control-label"> <span
												style="color: red">* </span>从业资格证发放机构 ：
											</label>
											<div class="col-sm-6">
												<input type="text" name="occSendIns" class="form-control" autocomplete="off">
											</div>
										</div>
									</div>
									<!-- 驾 驶 证 号 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"> <span
												style="color: red">* </span>驾驶证号 ：
											</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="driNum" autocomplete="off">
											</div>
										</div>
									</div>
								</div>

								<!-- 第六行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 从业资格证起始日期 -->
											<label class="col-sm-6 control-label"> <span
												style="color: red">* </span>从业资格证起始日期 ：
											</label>
											<div class="col-sm-6">
												<input type="text" name="occStartTime" id="startDate"
													class="Wdate form-control" readonly="readonly"
													onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd '+'00:00:00',maxDate:'#F{$dp.$D(\'endDate\')||\'new Date()\'}',onpicked:function(dp){refreshValidator(saveForm, name.valueOf())}})">
											</div>
										</div>
									</div>
									<!-- 评 定 等 级 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"> <span
												style="color: red">* </span>评定等级 ：
											</label>
											<div class="col-sm-7">
												<select class="form-control" name="evaGrade">
													<option>一星</option>
													<option>二星</option>
													<option>三星</option>
													<option>四星</option>
													<option>五星</option>
												</select>
											</div>
										</div>
									</div>
								</div>

								<!-- 第七行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 从业资格证终止日期   -->
											<label class="col-sm-6 control-label"> <span
												style="color: red">* </span>从业资格证终止日期 ：
											</label>
											<div class="col-sm-6">
												<input type="text" name="occEndTime" id="endDate"
													class="Wdate form-control" readonly="readonly"
													onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd '+'00:00:00',maxDate:'#F{$dp.$D(\'endDate\')||\'new Date()\'}',onpicked:function(dp){refreshValidator(saveForm, name.valueOf())}})">
											</div>
										</div>
									</div>
									<!-- 监督单位    -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"> <span
												style="color: red">* </span>监督单位 ：
											</label>
											<div class="col-sm-7">
												<input type="text" name="moniUnit" class="form-control" autocomplete="off">
											</div>
										</div>
									</div>
								</div>
								<!-- 第八行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 监 督 电 话-->
											<label class="col-sm-6 control-label"> <span
												style="color: red">* </span>监督电话 ：
											</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" name="moniPhone" autocomplete="off">
											</div>
										</div>
									</div>
									<!-- 监督单位    -->
									<!-- 										<div class="col-sm-6"> -->
									<!-- 											<div class="form-group"> -->
									<!-- 												<label class="col-sm-4 control-label">监督单位 </label> -->
									<!-- 												<div class="col-sm-7"> -->
									<!-- 													<input type="text" name="moniUnit" class="form-control"> -->
									<!-- 												</div> -->
									<!-- 											</div> -->
									<!-- 										</div> -->
									<!-- 									</div> -->


									<!-- 									<div class="row"> -->
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 资 格 证 号 -->
											<label class="col-sm-4 control-label"> <span
												style="color: red">* </span>资格证号 ：
											</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="quaNum" autocomplete="off">
											</div>
										</div>
									</div>
								</div>
								<!-- 第九行 -->
								<div class="row">
									<!-- 公 司 电 话   -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-6 control-label"> <span
												style="color: red">* </span>公司电话 ：
											</label>
											<div class="col-sm-6">
												<input type="text" name="comPhone" class="form-control" autocomplete="off">
											</div>
										</div>
									</div>


									<!-- 									<div class="row"> -->
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 家 庭 地 址 -->
											<label class="col-sm-4 control-label"> <span
												style="color: red">* </span>家庭地址 ：
											</label>
											<!-- 											<div class="col-sm-7"> -->
											<!-- 												<textarea style="width: 100%; height: 60px; resize: none;" -->
											<!-- 													name="home" class="form-control" aria-required="true"></textarea> -->
											<!-- 											</div> -->
											<div class="col-sm-7">
												<input type="text" name="home" class="form-control" autocomplete="off">
											</div>
										</div>
									</div>
								</div>
								<!-- 第十行 -->
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="control-label col-sm-3"> <span
												style="color: red">* </span>司机照片：
											</label>
											<div class="col-md-8">
												<input type="file" name="photo" id="photo">
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-danger"
										data-dismiss="modal">关闭</button>
									<!-- <button type="button" class="btn btn-primary" id="saveBtn">保存</button> -->
									<button id="btn_add_save" type="button" class="btn btn-primary">保存</button>
								</div>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>

		<!-- 从业人员信息展示 安仲辉修改 -->
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-title">
					<div>
						<h5>&nbsp;&nbsp;&nbsp;从业人员信息表</h5>
					</div>
				</div>
				<div class="ibox-content">
					<table id="table_drivers" width="100%"
						style="table-layout: fixed; background: #FFFFFF; display: none;"
						class="footable table table-stripped" data-page-size="10"
						data-filter=#filter>
						<thead>
							<tr>
								<th width="70">公司名称</th>
								<th width="70">车牌号</th>
								<th width="70">驾驶员姓名</th>
								<th width="70">性别</th>
								<th width="70">年龄</th>
								<th width="70">联系电话</th>
								<th width="70">评价等级</th>
								<th width="70">监督单位</th>
								<th width="70">监督电话</th>
								<th width="120">操作</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>

		<!-- 数据展示 -->
		<%-- 		<c:if test="${drivers!=null}">
			<div class="row">
				<div class="col-sm-12">
					<div class="ibox-title">
						<div>
							<h5>&nbsp;&nbsp;&nbsp;从业人员信息表</h5>
						</div>
					</div>
					<div class="ibox-content" style="height: 640px">
						<table id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>

							<thead>
								<tr>
									<th width="70">公司名称</th>
									<th width="70">车牌号</th>
									<th width="70">驾驶员姓名</th>
									<th width="70">性别</th>
									<th width="70">年龄</th>

									<th width="70">联系电话</th>

									<th width="70">评价等级</th>


									<th width="70">监督单位</th>
									<th width="70">监督电话</th>

									<th width="120">操作</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${drivers}" var="drivers">
									<tr id="${drivers.id}">

										<td>${drivers.company.name }</td>
										<td>${drivers.taxi.taxiNum }</td>
										<td>${drivers.dname }</td>
										<td>${drivers.sex }</td>
										<td>${drivers.age }</td>

										<td>${drivers.phone }</td>

										<td>${drivers.evaGrade }</td>

										<td>${drivers.moniUnit }</td>
										<td>${drivers.moniPhone }</td>
										<td>
											<button id="btn-update" type="button"
												class="btn btn-success btn-sm" data-toggle="modal"
												data-target="#myModal2"
												onclick="check2('${drivers.company.id }','${drivers.company.name }','${drivers.taxi.id }','${drivers.taxi.taxiNum }'
									,'${drivers.dname }','${drivers.sex }','${drivers.age }'
									,'${drivers.qdType }','${drivers.quaNum }','${drivers.phone }','${drivers.identityCard }','${drivers.driNum }'
                                    ,'${drivers.home }','${drivers.occSendIns }','${drivers.occStartTime }'
                                    ,'${drivers.occEndTime }','${drivers.evaGrade }'
                                    ,'${drivers.moniUnit }','${drivers.moniPhone }','${drivers.comPhone }')">详情</button>

											<shiro:hasPermission name="staff:admin">
												<button id="btn-update" type="button"
													class="btn btn-success btn-sm" data-toggle="modal"
													data-target="#myModal1"
													onclick="update('${drivers.id}','${drivers.company.id }','${drivers.company.name }','${drivers.taxi.id }','${drivers.taxi.taxiNum }'
									,'${drivers.dname }','${drivers.sex }','${drivers.age }'
									,'${drivers.qdType }','${drivers.quaNum }','${drivers.phone }','${drivers.identityCard }','${drivers.driNum }'
                                    ,'${drivers.home }','${drivers.occSendIns }','${drivers.occStartTime}'
                                    ,'${drivers.occEndTime }','${drivers.evaGrade }'
                                    ,'${drivers.moniUnit }','${drivers.moniPhone }','${drivers.comPhone }');editDriver()">修改</button>
												<button type="button" class="btn btn-danger btn-sm"
													onclick="delete1('${drivers.id}','${drivers.dname}')">删除</button>
											</shiro:hasPermission>
										</td>
									</tr>
								</c:forEach>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="10">
										<ul class="pagination pull-right"></ul>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</c:if> --%>
		<!-- 删除表格 -->
		<form id="deleteForm" action="deleteDriver" method="post">
			<input type="hidden" id="deleteId" name="id"> <input
				type="hidden" name="taxiNumDelete" id="taxiNumDelete">
		</form>

		<!-- 点击"修改"按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal1" role="dialog"
			aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form action="updateDriver" class="form-horizontal" method="post"
						id="updateForm">
						<input id="id" name="id" type="hidden"> <input
							type="hidden" name="taxiNumUpdate" id="taxiNumUpdate">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>
									修改司机信息<span style="color: red">（* 为必填项）</span>
								</h5>
							</div>
							<div class="ibox-content">

								<!-- 第一行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-6 control-label"><span
												style="color: red">* </span>姓 名 ：</label>
											<div class="col-sm-6">
												<input class="form-control" type="hidden" name="id"
													id="updateId"> <input class="form-control"
													type="text" name="dname" id="updateName">
											</div>
										</div>
									</div>

									<!-- 车 牌 号 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>车牌号 ：</label>
											<div class="col-sm-7">
												<select name="taxi.id" id="taxiSelect"
													class="form-control js-example-basic-single"></select>
											</div>
										</div>
									</div>
								</div>
								<!-- 第二行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 公 司  -->
											<label class="col-sm-6 control-label"><span
												style="color: red">* </span>公司 ：</label>
											<div class="col-sm-6">
												<select name="company.id" id="comSelect"
													class="form-control"></select>
											</div>
										</div>
									</div>
									<!-- 性 别 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>性别 ：</label>
											<div class="col-sm-7">
												<!-- 												<input class="form-control" type="text" name="sex" id="sex"> -->
												<select name="sex" class="form-control" id="sex">
													<option value="男">男</option>
													<option value="女">女</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<!-- 第三行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 年 龄  -->
											<label class="col-sm-6 control-label"><span
												style="color: red">* </span>年龄 ： </label>
											<div class="col-sm-6">
												<input type="text" class="form-control" name="age" id="age">
											</div>
										</div>
									</div>
									<!-- 联 系 电 话 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>联系电话 ：</label>
											<div class="col-sm-7">
												<input class="form-control" type="text" name="phone"
													id="phone">
											</div>
										</div>
									</div>
								</div>
								<!-- 第四行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 身 份 证 号  -->
											<label class="col-sm-6 control-label"><span
												style="color: red">* </span>身份证号 ：</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" name="identityCard"
													id="identityCard">
											</div>
										</div>
									</div>
									<!-- 驾 驶 证 号 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>驾驶证号 ：</label>
											<div class="col-sm-7">
												<input class="form-control" type="text" name="driNum"
													id="driNum">
											</div>
										</div>
									</div>
								</div>
								<!-- 第五行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">

											<!-- 从业资格证起始日期 -->
											<label class="col-sm-6 control-label"><span
												style="color: red">* </span>从业资格证起始日期 ：</label>
											<div class="col-sm-6">
												<input class="form-control" type="text" name="occStartTime"
													id="occStartTime" readonly="readonly"
													onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd '+'00:00:00',maxDate:'#F{$dp.$D(\'endDate\')||\'new Date()\'}',onpicked:function(dp){refreshValidator(updateForm, name.valueOf())}})">
												<!-- 													onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd '+'00:00:00.0',maxDate:'#F{$dp.$D(\'occEndTime\')||\'new Date()\'}'})"> -->
												<!-- 											,onpicked:function(dp){refreshValidator(updateForm, name.valueOf())} -->
											</div>
										</div>
									</div>
									<!-- 准 驾 车 型 -->
									<div class="col-sm-6">
										<div class="form-group">

											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>准驾车型 ：</label>
											<div class="col-sm-7">
												<input class="form-control" type="text" name="qdType"
													id="qdType">
											</div>
										</div>
									</div>
								</div>
								<!-- 第六行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 从业资格证发放机构  -->
											<label class="col-sm-6 control-label"><span
												style="color: red">* </span>从业资格证发放机构 ：</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" name="occSendIns"
													id="occSendIns">
											</div>
										</div>
									</div>
									<!-- 评 定 等 级 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>评定等级 ：</label>
											<div class="col-sm-7">
												<select name="evaGrade" id="evaGrade" class="form-control">
												</select>
											</div>
										</div>
									</div>

								</div>
								<!-- 第七行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 从业资格证终止日期  -->
											<label class="col-sm-6 control-label"><span
												style="color: red">* </span>从业资格证终止日期 ：</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" name="occEndTime"
													id="occEndTime" readonly="readonly"
													onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'occStartTime\')}',maxDate:'#F{\'new Date()\'}',dateFmt:'yyyy-MM-dd '+'00:00:00.0',onpicked:function(dp){refreshValidator(updateForm, name.valueOf())}})">
											</div>
										</div>
									</div>
									<!-- 监 督 单 位 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>监督单位 ：</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="moniUnit"
													id="moniUnit">
											</div>
										</div>
									</div>
								</div>
								<!-- 第八行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 监 督 电 话  -->
											<label class="col-sm-6 control-label"><span
												style="color: red">* </span>监督电话 ：</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" name="moniPhone"
													id="moniPhone">
											</div>
										</div>
									</div>
									<!-- 资 格 证 号 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>资格证号 ：</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="quaNum"
													id="quaNum">
											</div>
										</div>
									</div>
								</div>
								<!-- 第九行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 公 司 电 话 -->
											<label class="col-sm-6 control-label"><span
												style="color: red">* </span>公司电话 ：</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" name="comPhone"
													id="comPhone">
											</div>
										</div>
									</div>

									<!-- 家 庭 地 址 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color: red">* </span>家庭地址 ：</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="home"
													id="home">
											</div>
										</div>
									</div>
								</div>
								<!-- 第十行 -->
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="control-label col-sm-3"> <span
												style="color: red">* </span>司机照片：
											</label>
											<div class="col-md-8">
												<input type="file" name="photo" id="editPhoto"><!--  <input
													type="hidden" name="state" id="up_state"> -->
												<!-- 												<input type="hidden" name="photoAddr" id="up_addr"> 
 -->
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-danger"
										data-dismiss="modal">关闭</button>
									<button id="btn_save_update" type="button"
										class="btn btn-primary" id="">保存</button>
								</div>
							</div>


						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- 点击"查看详情"按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal2" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-body">
						<div class="ibox-title">
							<h5>查看司机详情</h5>
						</div>
						<div class="ibox-content " style="line-height: 35px">
							<div class="row">
								<div class="col-sm-4 b-r">
									<img id="driver-photo">
								</div>
								<div class="col-sm-8">
									<label class="col-sm-2 control-label"
										style="text-align: right;">姓名：</label>
									<div class="col-sm-4">
										<div class="form-group">
											<input id="dname2" type="text" class="form-control"
												readonly="readonly" />
										</div>
									</div>

									<label class="col-sm-2 control-label"
										style="text-align: right;">车牌号：</label>
									<div class="col-sm-4" style="padding-left: -1px;">
										<div class="form-group">
											<input id="taxiNum2" type="text" class="form-control"
												readonly="readonly" />
										</div>
									</div>

									<br> <label class="col-sm-2 control-label"
										style="text-align: right;">性别 ：</label>
									<div class="col-sm-4" style="padding-left: -1px;">
										<div class="form-group">
											<input id="sex2" type="text" class="form-control"
												readonly="readonly" />
										</div>
									</div>

									<label class="col-sm-2 control-label"
										style="text-align: right;">年龄：</label>
									<div class="col-sm-4" style="padding-left: -1px;">
										<div class="form-group">
											<input id="age2" type="text" class="form-control"
												readonly="readonly" />
										</div>
									</div>

									<br> <label class="col-sm-2 control-label"
										style="text-align: right;">电话：</label>
									<div class="col-sm-4" style="padding-left: -1px;">
										<div class="form-group">
											<input id="phone2" type="text" class="form-control"
												readonly="readonly" />
										</div>
									</div>

									<label class="col-sm-2 control-label"
										style="text-align: right;">公 司 ：</label>
									<div class="col-sm-4" style="padding-left: -5px;">
										<div class="form-group">
											<input id="company2" type="text" class="form-control"
												readonly="readonly" />
										</div>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-sm-12">
									<label class="col-sm-3 control-label"
										style="text-align: right;">身份证号 ：</label>
									<div class="col-sm-3">
										<div class="form-group">
											<input type="text" class="form-control" id="identityCard2"
												readonly="readonly">
										</div>
									</div>
									<label class="col-sm-3 control-label"
										style="text-align: right;">驾驶证号 ：</label>
									<div class="col-sm-3">
										<div class="form-group">
											<input type="text" class="form-control" id="driNum2"
												readonly="readonly">
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<label class="col-sm-3 control-label"
										style="text-align: right;">准驾车型 ：</label>
									<div class="col-sm-3">
										<div class="form-group">
											<input type="text" class="form-control" id="qdType2"
												readonly="readonly">
										</div>
									</div>
									<label class="col-sm-3 control-label"
										style="text-align: right;">从业资格证起始日期：</label>
									<div class="col-sm-3">
										<div class="form-group">
											<input type="text" class="form-control" id="occStartTime2"
												readonly="readonly">
										</div>
									</div>
								</div>

								<div class="col-sm-12">
									<label class="col-sm-3 control-label"
										style="text-align: right;">从业资格证终止日期 ：</label>
									<div class="col-sm-3">
										<div class="form-group">
											<input type="text" class="form-control" id="occEndTime2"
												readonly="readonly">
										</div>
									</div>
									<label class="col-sm-3 control-label"
										style="text-align: right;">评定等级：</label>
									<div class="col-sm-3">
										<div class="form-group">
											<input type="text" class="form-control" id="evaGrade2"
												readonly="readonly">
										</div>
									</div>
								</div>

								<div class="col-sm-12">
									<label class="col-sm-3 control-label"
										style="text-align: right;">监督单位：</label>
									<div class="col-sm-3">
										<div class="form-group">
											<input type="text" class="form-control" id="moniUnit2"
												readonly="readonly">
										</div>
									</div>
									<label class="col-sm-3 control-label"
										style="text-align: right;">监督电话：</label>
									<div class="col-sm-3">
										<div class="form-group">
											<input type="text" class="form-control" id="moniPhone2"
												readonly="readonly">
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<label class="col-sm-3 control-label"
										style="text-align: right;">资格证号：</label>
									<div class="col-sm-3">
										<div class="form-group">
											<input type="text" class="form-control" id="quaNum2"
												readonly="readonly">
										</div>
									</div>
									<label class="col-sm-3 control-label"
										style="text-align: right;">家庭地址：</label>
									<div class="col-sm-3">
										<div class="form-group">
											<input type="text" class="form-control" id="home2"
												readonly="readonly">
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<label class="col-sm-3 control-label"
										style="text-align: right;">公司电话：</label>
									<div class="col-sm-3">
										<div class="form-group">
											<input type="text" class="form-control" id="comPhone2"
												readonly="readonly">
										</div>
									</div>
									<label class="col-sm-3 control-label"
										style="text-align: right;">从业资格证发放机构：</label>
									<div class="col-sm-3">
										<div class="form-group">
											<input type="text" class="form-control" id="occSendIns2"
												readonly="readonly">
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-danger"
									data-dismiss="modal">关闭</button>
							</div>
						</div>

					</div>
				</div>


			</div>
		</div>

		<!--点击审核状态  -->
		<div class="modal inmodal fade" id="myModal3" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-body">
						<div class="ibox-title">
							<h5>从业人员审核</h5>
						</div>
						<div class="ibox-content " style="line-height: 35px">
							<div class="row">
								<div class="col-sm-12">
									<label class="col-sm-3 control-label"
										style="text-align: right;">当前审核状态：</label>
									<div class="col-sm-3">
										<div class="form-group">
											<input id="app_state" type="text" class="form-control"
												readonly="readonly" /> <input id="app_id" type="hidden" />
										</div>
									</div>
									<label class="col-sm-2 control-label"
										style="text-align: right;">操作：</label>
									<div class="col-sm-2">
										<button id="app_pass" type="button" class="btn btn-success">通过</button>
									</div>
									<div class="col-sm-2">
										<button id="app_down" type="button" class="btn btn-danger">撤销</button>
									</div>
								</div>
							</div>
							<br>


						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal">关闭</button>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<!-- 	</div> -->
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<!-- <script src="js/jquery.min.js?v=2.1.4"></script> -->
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script>
		//加载下拉框
		$(".js-example-basic-single").select2({
			width : "100%"
		});
		$.fn.modal.Constructor.prototype.enforceFocus = function() {};
	
		//加载footable的功能
		/* $(document).ready(function() {
			$('#table1').DataTable({
				"pagingType" : "full_numbers"
			});
		}); */
	
		//点击查看详情按钮
		/* function check2(comId, comName, taxiId, taxiNum, dname, sex, age,
				qdType, quaNum, phone, identityCard, driNum, home, occSendIns,
				occStartTime, occEndTime, evaGrade, moniUnit, moniPhone,
				comPhone) {
		
			$("#dname2").text(dname);
			$("#company2").text(comName);
			$("#taxiNum2").text(taxiNum);
			$("#sex2").text(sex);
			$("#age2").text(age);
		
			$("#qdType2").text(qdType);
			$("#quaNum2").text(quaNum);
			$("#phone2").text(phone);
			$("#identityCard2").text(identityCard);
			$("#driNum2").text(driNum);
		
			$("#home2").text(home);
			$("#occSendIns2").text(occSendIns);
			$("#occStartTime2").text(occStartTime);
		
			$("#occEndTime2").text(occEndTime);
		
			$("#evaGrade2").text(evaGrade);
			$("#moniUnit2").text(moniUnit);
			$("#moniPhone2").text(moniPhone);
			$("#comPhone2").text(comPhone);
		} */
	
		//点击"修改"按钮
		/* function update(id, comId, comName, taxiId, taxiNum, dname, sex, age,
				qdType, quaNum, phone, identityCard, driNum, home, occSendIns,
				occStartTime, occEndTime, evaGrade, moniUnit, moniPhone,
				comPhone) {
			$("#id").val(id);
			$("#taxiNumUpdate").val(taxiNum);
			//修改车牌号下拉框的值
			$.post("getAllTaxiByAjax", function(data) {
				if (data.length != 0) {
					//返回的数据不为空 
					var newdata = eval("(" + data + ")");
					$("#taxiSelect").html("");//避免选择框中的值持续叠加
					$("<option value='"+taxiId+"'>" + taxiNum + "</option>")
							.appendTo($("#taxiSelect"));
		
					for (var i = 0; i < data.length; i++) {
						if (newdata[i].taxiNum != taxiNum) {
							$(
									"<option value ='"+newdata[i].taxiId +
											"'> "
											+ newdata[i].taxiNum + "</option>")
									.appendTo($("#taxiSelect"));
						}
					}
				}
			});
		
			//修改公司 下拉框的值
			$.post("getAllComByAjax", function(data) {
				if (data.length != 0) {//返回的数据不为空 
					var newdata = eval("(" + data + ")");
					$("#comSelect").html("");//避免选择框中的值持续叠加
					$("<option value='"+comId+"'>" + comName + "</option>")
							.appendTo($("#comSelect"));//数据回显
		
					for (var i = 0; i < data.length; i++) {//数据库所有数据
						if (newdata[i].cname != comName) {
							$(
									"<option value ='"+newdata[i].comId +"'> "
											+ newdata[i].cname + "</option>")
									.appendTo($("#comSelect"));
						}
					}
				}
			});
		
			$("#updateName").val(dname);
			$("#sex").val(sex);
			$("#age").val(age);
		
			$("#qdType").val(qdType);
			$("#quaNum").val(quaNum);
			$("#phone").val(phone);
			$("#identityCard").val(identityCard);
			$("#driNum").val(driNum);
		
			$("#home").val(home);
			$("#occSendIns").val(occSendIns);
			$("#occStartTime").val(occStartTime);
			$("#occEndTime").val(occEndTime);
			//	承包人信息暂时不显示
			//评定等级数据回显
			$("#evaGrade").html("");//避免选择框中的值持续叠加
			$("<option>" + evaGrade + "</option>").appendTo($("#evaGrade"));
			if (evaGrade != "一星") {
				$("<option>" + "一星" + "</option>").appendTo($("#evaGrade"));
			}
			if (evaGrade != "二星") {
				$("<option>" + "二星" + "</option>").appendTo($("#evaGrade"));
			}
			if (evaGrade != "三星") {
				$("<option>" + "三星" + "</option>").appendTo($("#evaGrade"));
			}
			if (evaGrade != "四星") {
				$("<option>" + "四星" + "</option>").appendTo($("#evaGrade"));
			}
			if (evaGrade != "五星") {
				$("<option>" + "五星" + "</option>").appendTo($("#evaGrade"));
			}
		
			$("#moniUnit").val(moniUnit);
			$("#moniPhone").val(moniPhone);
			$("#comPhone").val(comPhone);
		}; */
	
		//点击"删除"按钮
		/* function delete1(id, name) {
			var s1 = $("#taxiSelect").val();
			$("#taxiNumDelete").val(s1);
			swal({
				title : "您确定删除 " + name + " 的数据吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
				$("#deleteId").val(id);
				//提交表单
				$("#deleteForm").submit();
			});
		} */
		//------------------改-------------------
		// 			v	var url = "deleteDriver";
		// 				$.post(url, {
		// 					"id" : id
		// 				}, function(date) {
		// 					if(date.indexof("成功")>-1){
		// 						swal({
		// 							title : date,
		// 							type : "success"
		// 						});
		// 					}else{
		// 						swal({
		// 							title : date,
		// 							type : "warning"
		// 						});
		//				}
	
		// 					if (date != "出现未知错误") {
		// 						$("#" + id).hide();
		// 					}
		// 				});
		// 			})
		// 		}
	
		//点击“查询”按钮
		/* $("#selectBtn").click(function() {
			var taxiNumSelect = $("#taxiNumSelect").val();
			if (taxiNumSelect == "") {
				$("#selectForm").submit();
			} else {
				//先判断车牌号长度是否为6
				if (taxiNumSelect.length != 6) {
					swal({
						title : "请输入6位的车牌号！",
						type : "warning"
					});
				} else {//通过ajax，查看该车牌号是否存在
					$.post("ifTaxiNumByAjax", {
						"taxiNum" : taxiNumSelect
					}, function(data) {
						var newdata = eval("(" + data + ")");
						if (newdata == 0) {
							swal({
								title : "输入的车牌号无相关数据，请检查是否输入正确！",
								type : "warning"
							});
						} else {
							$("#selectForm").submit();
						}
					});
				}
			}
		}); */
	
		/* //添加-点击保存“添加”的按钮
		$("#saveBtn").click(function() {
			//赋值车牌号文本框，用来回显该新增的数据
			var s1 = $("#saveTaxi option:checked").text();
			$("#taxiNumSave").val(s1);
		
			//清空错误提示框的值
			$("#taxiErr").empty();
			$("#comErr").empty();
		
			var saveName = $("#saveName").val();
			var saveTaxi = $("#saveTaxi").val();
			var saveCom = $("#saveCom").val();
			//alert("saveName:" + saveName)
			if (saveName == "") {
				$("#saveName").focus();
				swal({
					title : "从业人员姓名不能为空！",
					type : "warning"
				});
			} else if (saveTaxi == "") {
				$("#taxiErr").append("请选择!");
				swal({
					title : "车牌号不能为空！",
					type : "warning"
				});
			} else if (saveCom == "") {
				$("#comErr").append("请选择!");
				swal({
					title : "公司不能为空！",
					type : "warning"
				});
			} else {
				$("#saveForm").submit();
			}
		}); */
	
		/* //修改-点击保存"添加"的按钮
		$("#updateSaveBtn").click(function() {
		
			var s1 = $("#taxiSelect").val();
		
			$("#taxiNumUpdate").val(s1);
			var dName = $("#updateName").val();
			if ( dName == "" ||dName == null) {
				$("#dName").focus();
				swal({
					title : "姓名不能为空！",
					type : "warning"
				});
			} else {
				$("#updateForm").submit();
			}
		}); */
	
		//清空查询框
		function delAll() {
			$("#taxiNumSelect").val("");
			$("#quaNumSelect").val("");
			$("#dnameSelect").val("");
		}
		;
		$(document).ready(function() {
			$("#photo").fileinput({
				language : 'zh',
				autoReplace : false,
				maxFileCount : 1,
				allowedFileExtensions : [ "jpg", "png", "gif" ],
				browseClass : "btn btn-primary", //按钮样式 
				showUpload : false, //是否显示上传按钮
			});
			$("#editPhoto").fileinput({
				language : 'zh',
				autoReplace : false,
				maxFileCount : 1,
				allowedFileExtensions : [ "jpg", "png", "gif" ],
				browseClass : "btn btn-primary", //按钮样式 
				showUpload : false, //是否显示上传按钮
			});
	
			var result = '<%=request.getAttribute("result")%>
		';
			if (result != null && result != "null") {
				if (result.indexOf("成功") > 0) {

					swal({
						title : result,
						type : "success"
					});
				} else {
					swal({
						title : result,
						type : "error"
					});
				}
			}
		});
	</script>

	<script type="text/javascript">
		var saveForm;
		/*添加从业人员界面的表单验证*/
		$('#btn_addDriver')
				.click(
						function() {
							saveForm = $('#saveForm')
									.bootstrapValidator(
											{
												feedbackIcons : { /*输入框不同状态，显示图片的样式*/
													valid : 'glyphicon glyphicon-ok',
													invalid : 'glyphicon glyphicon-remove',
													validating : 'glyphicon glyphicon-refresh'
												},
												/*生效规则：字段值一旦变化就触发验证*/
												live : 'enabled',
												/*当表单验证不通过时，该按钮为disabled*/
												submitButtons : 'button[type="submit"]',
												/*验证*/
												fields : {
													dname : { /*键名username和input name值对应*/
														message : 'The username is not valid',
														validators : {
															notEmpty : { /*非空提示*/
																message : '姓名不能为空'
															}
														}
													},
													age : {
														validators : {
															notEmpty : {
																message : '年龄不能为空'
															},
															numeric : {
																message : '请输入数字'
															}
														}
													},
													phone : {
														validators : {
															notEmpty : {
																message : '年龄不能为空'
															},
															numeric : {
																message : '请输入数字'
															}
														}
													},
													identityCard : {
														validators : {
															notEmpty : {
																message : '身份证号码不能为空'
															},
															regexp : { /* 只需加此键值对，包含正则表达式，和提示 */
																regexp : /^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/,
																message : '请输入正确格式的身份证号'
															}
														}
													},
													driNum : {
														validators : {
															notEmpty : {
																message : '驾驶证号码不能为空'
															}
														}
													},
													qdType : {
														validators : {
															notEmpty : {
																message : '准驾车型不能为空'
															}
														}
													},
													occSendIns : {
														validators : {
															notEmpty : {
																message : '从业资格证发放机构不能为空'
															}
														}
													},
													moniUnit : {
														validators : {
															notEmpty : {
																message : '监督单位不能为空'
															}
														}
													},
													moniPhone : {
														validators : {
															notEmpty : {
																message : '监督电话不能为空'
															},
															numeric : {
																message : '请输入数字'
															}
														}
													},
													quaNum : {
														validators : {
															notEmpty : {
																message : '资格证号码不能为空'
															}
														}
													},
													home : {
														validators : {
															notEmpty : {
																message : '家庭住址不能为空'
															}
														}
													},
													comPhone : {
														validators : {
															notEmpty : {
																message : '公司电话不能为空'
															},
															numeric : {
																message : '请输入数字'
															}
														}
													},
													occStartTime : {
														validators : {
															notEmpty : {
																message : '从业资格证起始日期不能为空'
															}
														}
													},
													occEndTime : {
														validators : {
															notEmpty : {
																message : '从业资格证终止日期不能为空'
															}
														}
													},
													photo : {
														validators : {
															notEmpty : {
																message : '司机照片不能为空'
															}
														}
													}
												}
											});
						});

		var updateForm = $('#updateForm');
		/*修改从业人员界面的表单验证*/
		function editDriver() {
			updateForm
					.bootstrapValidator({
						feedbackIcons : { /*输入框不同状态，显示图片的样式*/
							valid : 'glyphicon glyphicon-ok',
							invalid : 'glyphicon glyphicon-remove',
							validating : 'glyphicon glyphicon-refresh'
						},
						/*生效规则：字段值一旦变化就触发验证*/
						live : 'enabled',
						/*当表单验证不通过时，该按钮为disabled*/
						submitButtons : 'button[type="submit"]',
						/*验证*/
						fields : {
							dname : { /*键名username和input name值对应*/
								message : 'The username is not valid',
								validators : {
									notEmpty : { /*非空提示*/
										message : '姓名不能为空'
									}
								}
							},
							age : {
								message : '密码无效',
								validators : {
									notEmpty : {
										message : '年龄不能为空'
									},
									numeric : {
										message : '请输入数字'
									}
								}
							},
							phone : {
								validators : {
									notEmpty : {
										message : '年龄不能为空'
									},
									numeric : {
										message : '请输入数字'
									}
								}
							},
							identityCard : {
								validators : {
									notEmpty : {
										message : '身份证号码不能为空'
									},
									regexp : { /* 只需加此键值对，包含正则表达式，和提示 */
										regexp : /^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/,
										message : '请输入正确格式的身份证号'
									}
								}
							},
							driNum : {
								validators : {
									notEmpty : {
										message : '驾驶证号码不能为空'
									}
								}
							},
							qdType : {
								validators : {
									notEmpty : {
										message : '准驾车型不能为空'
									}
								}
							},
							occSendIns : {
								validators : {
									notEmpty : {
										message : '从业资格证发放机构不能为空'
									}
								}
							},
							moniUnit : {
								validators : {
									notEmpty : {
										message : '监督单位不能为空'
									}
								}
							},
							moniPhone : {
								validators : {
									notEmpty : {
										message : '监督电话不能为空'
									},
									numeric : {
										message : '请输入数字'
									}
								}
							},
							quaNum : {
								validators : {
									notEmpty : {
										message : '资格证号码不能为空'
									}
								}
							},
							home : {
								validators : {
									notEmpty : {
										message : '家庭住址不能为空'
									}
								}
							},
							comPhone : {
								validators : {
									notEmpty : {
										message : '公司电话不能为空'
									},
									numeric : {
										message : '请输入数字'
									}
								}
							},
							occStartTime : {
								validators : {
									notEmpty : {
										message : '从业资格证起始日期不能为空'
									}
								}
							},
							occEndTime : {
								validators : {
									notEmpty : {
										message : '从业资格证终止日期不能为空'
									}
								}
							},
							photo : {
								validators : {
									notEmpty : {
										message : '司机照片不能为空'
									}
								}
							}
						}
					});
		};
		/*日历刷新验证*/
		function refreshValidator(id, name) {
			$(id).data('bootstrapValidator').updateStatus(name,
					'NOT_VALIDATED', null).validateField(name);
		}
	</script>

	<!-- 修改从业人员为后台分页 安仲辉 -->
	<script type="text/javascript">
		var tables;
		//添加、修改异步提交地址
		function listDrivers() {
			var taxiNumSelect = $("#taxiNumSelect").val();
			if (taxiNumSelect != "" && taxiNumSelect.length != 7) {
				swal({
					title : "请输入七位的车牌号！",
					type : "warning"
				});
				return;
			}

			$("#table_drivers").show();
			//避免多次点击查询报错
			if (tables != null) {
				console.log("销毁datatable");
				tables.fnClearTable(); //清空数据
				tables.fnDestroy(); //销毁datatable
			}
			tables = $("#table_drivers")
					.dataTable(
							{
								serverSide : true, //分页，取数据等等的都放到服务端去
								processing : true, //载入数据的时候是否显示“载入中”
								pageLength : 10, //首次加载的数据条数
								ordering : true, //排序操作在服务端进行，所以可以关了。
								pagingType : "full_numbers",
								autoWidth : false,
								stateSave : true, //保持翻页状态，和comTable.fnDraw(false);结合使用
								searching : true, //禁用datatables搜索
								ajax : {
									type : "post",
									url : "listDrivers",
									dataSrc : "data",
									data : function(d) {
										var param = {};
										param.draw = d.draw; //拉取次数
										param.start = d.start; //起始页
										param.length = d.length; //长度
										param.orderColumn = d.columns[d.order["0"].column].data; //排序的列名
										param.orderType = d.order["0"].dir; //排序规则
										param.search = d.search.value; //搜索框
										param.taxiNum = $('#taxiNumSelect')
												.val(); //车牌号
										param.dname = $('#dnameSelect').val(); //姓名
										param.quaNum = $('#quaNumSelect').val(); //证件号
										return param; //自定义需要传递的参数。
									},
								},
								columns : [ //对应上面thead里面的序列
										{
											"data" : 'company',
											defaultContent : "-",
											"render" : function(data, type,
													full, callback) {
												if (data != null && data != "") {
													return data.name;
												}
											}
										},
										{
											"data" : 'taxi',
											defaultContent : "-",
											"render" : function(data, type,
													full, callback) {
												if (data != null && data != "") {
													return data.taxiNum;
												}
											}
										}, {
											"data" : 'dname'
										}, {
											"data" : 'sex'
										}, {
											"data" : 'age'
										}, {
											"data" : 'phone'
										}, {
											"data" : 'evaGrade'
										}, {
											"data" : 'moniUnit'
										}, {
											"data" : 'moniPhone'
										}, ],
								//操作按钮
								columnDefs : [ {
									targets : 9,
									defaultContent : "<button id='btn-detail' type=\"button\"" +
									"class=\"btn btn-success btn-sm\" data-toggle=\"modal\">详情</button>"
											+ "<shiro:hasPermission name='staff:admin'>"
											+ "<button id=\"btn-update\" type=\"button\""
											+ "class=\"btn btn-success btn-sm\" data-toggle=\"modal\""
											+ "data-target=\"#myModal1\" >修改</button>"
											+ "<button id=\"btn-delete\" type=\"button\" class=\"btn btn-danger btn-sm\">删除</button>"
											+ "</shiro:hasPermission>"
											+ "<shiro:hasPermission name='staff:approal'>"
											+ "<button id=\"btn-approal\" type=\"button\""
											+ "class=\"btn btn-success btn-sm\" data-toggle=\"modal\""
											+ "data-target=\"#myModal3\">审核状态 </button>"
											+ "</shiro:hasPermission>"
								} ],
								//在每次table被draw完后回调函数,添加按钮监听
								fnDrawCallback : function(data) {
									//alert(ShowTheObject(data));
									addBtnDetail();
									addBtnDelete();
									addBtnApproal();
									addBtnUpdate();
								}
							});
		}

		function ShowTheObject(obj) {
			var des = "";
			for ( var name in obj) {
				des += name + ":" + obj[name] + ";";
			}

			return des;
		}

		function addBtnDetail() {
			$("#table_drivers tbody")
					.on(
							"click",
							"#btn-detail",
							function() {
								var dataRow = tables.api().row(
										$(this).parents("tr")).data();
								console.log(dataRow);
								$("#dname2").val(dataRow.dname);
								if (dataRow.company != null) {
									$("#company2").val(dataRow.company.name);
								} else {
									$("#company2").val("-");
								}
								if (dataRow.taxi != null) {
									$("#taxiNum2").val(dataRow.taxi.taxiNum);
								} else {
									$("#taxiNum2").val("-");
								}
								$("#sex2").val(dataRow.sex);
								$("#age2").val(dataRow.age);
								$("#qdType2").val(dataRow.qdType);
								$("#quaNum2").val(dataRow.quaNum);
								$("#phone2").val(dataRow.phone);
								$("#identityCard2").val(dataRow.identityCard);
								$("#driNum2").val(dataRow.driNum);
								$("#home2").val(dataRow.home);
								$("#occSendIns2").val(dataRow.occSendIns);
								$("#occStartTime2").val(
										getNowFormatDate(new Date(
												dataRow.occStartTime)));
								$("#occEndTime2").val(
										getNowFormatDate(new Date(
												dataRow.occEndTime)));
								$("#evaGrade2").val(dataRow.evaGrade);
								$("#moniUnit2").val(dataRow.moniUnit);
								$("#moniPhone2").val(dataRow.moniPhone);
								$("#comPhone2").val(dataRow.comPhone);
								$("#driver-photo").attr(
										"src",
										"${pageContext.request.contextPath}"
												+ dataRow.photoAddr);
								$('#myModal2').modal('show');
							});
		}

		function addBtnUpdate() {
			$("#table_drivers tbody")
					.on(
							"click",
							"#btn-update",
							function() {
								editDriver();
								var dataRow = tables.api().row(
										$(this).parents("tr")).data();
								$("#id").val(dataRow.id);

								var taxi = dataRow.taxi;
								var taxiNum;
								var company = dataRow.company;
								var companyName;
								if (taxi != null) {
									$("#taxiNumUpdate").val(
											dataRow.taxi.taxiNum);
									taxiNum = dataRow.taxi.taxiNum;
								} else {
									taxiNum = "";
								}
								if (company != null) {
									$("#taxiNumUpdate").val(
											dataRow.company.name);
									companyName = dataRow.company.name;
								} else {
									companyName = "";
								}

								//修改车牌号下拉框的值
								$
										.post(
												"getAllTaxiByAjax",
												function(data) {
													if (data.length != 0) {
														//返回的数据不为空 
														var newdata = eval("("
																+ data + ")");
														$("#taxiSelect").html(
																""); //避免选择框中的值持续叠加
														if (taxi != null) {
															$(
																	"<option value='" + taxi.id + "'>"
																			+ taxiNum
																			+ "</option>")
																	.appendTo(
																			$("#taxiSelect"));
														}

														for (var i = 0; i < data.length; i++) {
															if (newdata[i].taxiNum != taxiNum) {
																$(
																		"<option value ='" + newdata[i].taxiId +
													"'> "
																				+ newdata[i].taxiNum
																				+ "</option>")
																		.appendTo(
																				$("#taxiSelect"));
															}
														}
													}
												});

								//修改公司 下拉框的值
								$
										.post(
												"getAllComByAjax",
												function(data) {
													if (data.length != 0) { //返回的数据不为空 
														var newdata = eval("("
																+ data + ")");
														$("#comSelect")
																.html(""); //避免选择框中的值持续叠加
														if (company != null) {
															$(
																	"<option value='" + company.id + "'>"
																			+ companyName
																			+ "</option>")
																	.appendTo(
																			$("#comSelect")); //数据回显
														}

														for (var i = 0; i < data.length; i++) { //数据库所有数据
															if (newdata[i].cname != companyName) {
																$(
																		"<option value ='" + newdata[i].comId + "'> "
																				+ newdata[i].cname
																				+ "</option>")
																		.appendTo(
																				$("#comSelect"));
															}
														}
													}
												});

								$("#updateId").val(dataRow.id);
								$("#updateName").val(dataRow.dname);
								$("#sex").val(dataRow.sex);
								$("#age").val(dataRow.age);

								$("#qdType").val(dataRow.qdType);
								$("#quaNum").val(dataRow.quaNum);
								$("#phone").val(dataRow.phone);
								$("#identityCard").val(dataRow.identityCard);
								$("#driNum").val(dataRow.driNum);
								$("#home").val(dataRow.home);
								$("#occSendIns").val(dataRow.occSendIns);
								$("#occStartTime").val(
										getNowFormatDate(new Date(
												dataRow.occStartTime)));
								$("#occEndTime").val(
										getNowFormatDate(new Date(
												dataRow.occEndTime)));
								//	承包人信息暂时不显示
								//评定等级数据回显
								$("#evaGrade").html(""); //避免选择框中的值持续叠加
								$("<option>" + dataRow.evaGrade + "</option>")
										.appendTo($("#evaGrade"));
								if (dataRow.evaGrade != "一星") {
									$("<option>" + "一星" + "</option>")
											.appendTo($("#evaGrade"));
								}
								if (dataRow.evaGrade != "二星") {
									$("<option>" + "二星" + "</option>")
											.appendTo($("#evaGrade"));
								}
								if (dataRow.evaGrade != "三星") {
									$("<option>" + "三星" + "</option>")
											.appendTo($("#evaGrade"));
								}
								if (dataRow.evaGrade != "四星") {
									$("<option>" + "四星" + "</option>")
											.appendTo($("#evaGrade"));
								}
								if (dataRow.evaGrade != "五星") {
									$("<option>" + "五星" + "</option>")
											.appendTo($("#evaGrade"));
								}

								$("#moniUnit").val(dataRow.moniUnit);
								$("#moniPhone").val(dataRow.moniPhone);
								$("#comPhone").val(dataRow.comPhone);
							});
		}

		function addBtnDelete() {
			$("#table_drivers tbody").on("click", "#btn-delete", function() {
				var dataRow = tables.api().row($(this).parents("tr")).data();
				var s1 = $("#taxiSelect").val();
				$("#taxiNumDelete").val(s1);
				swal({
					title : "您确定删除 " + dataRow.dname + " 的数据吗?",
					type : "warning",
					showCancelButton : true,
					cancelButtonText : "取消"
				}, function() {
					//提交表单
					$.ajax({
						url : "deleteDriverById",
						type : "POST",
						data : "id=" + dataRow.id,
						success : function(data) {
							$.ajax({
								url : "deleteDriverById",
								type : "post",
								data : "id:" + dataRow.id,
								success : function(data) {
									if (data == "success") {
										swal({
											title : "操作执行成功",
											type : "success"
										});

										//刷新表单
										tables.fnDraw(true); //刷新保持分页状态
									} else if (data == "nouser") {
										swal({
											title : "您要操作的对象已被删除",
											type : "warning"
										});
									} else {
										swal({
											title : "相关投诉尚未处理，删除失败",
											type : "error"
										});
									}
								}
							});
						}
					});
				});
			});
		}

		function addBtnApproal() {
			$("#table_drivers tbody").on("click", "#btn-approal", function() {
				var dataRow = tables.api().row($(this).parents("tr")).data();
				$("#app_id").val(dataRow.id);
				var s1 = $("#taxiSelect").val();
				if(dataRow.state==true){
					 $("#app_state").val("审核通过");
				}else{
		        	$("#app_state").val("未审核或审核不通过");
				}
			});
		}
		
		$("#app_pass").click(function(){
			swal({
				title : "您确定通过 当前司机 的审核吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
				var id=$("#app_id").val();
				$.post("ApproalDriverById",
						  {
						    "id":id,
						    "state":"1"
						  },
						  function(data){
							  if (data == "success") {
									swal({
										title : "操作执行成功",
										type : "success"
									});
									//刷新表单
									tables.fnDraw(true);  //刷新保持分页状态
								} else if (data == "nouser") {
									swal({
										title : "您要操作的对象已被删除",
										type : "warning"
									});
								} else {
									swal({
										title : "当前司机已通过审核",
										type : "warning"
									});
								}
						  });
				
				//提交表单
			});
			
		})
		
		$("#app_down").click(function(){
			swal({
				title : "您确定取消当前司机 的审核吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
				var id=$("#app_id").val();
				$.post("ApproalDriverById",
						  {
						    "id":id,
						    "state":"0"
						  },
						  function(data){
							  if (data == "success") {
									swal({
										title : "操作执行成功",
										type : "success"
									});
									//刷新表单
									tables.fnDraw(true);  //刷新保持分页状态
								} else if (data == "nouser") {
									swal({
										title : "您要操作的对象已被删除",
										type : "warning"
									});
								} else {
									swal({
										title : "当前司机已撤销审核",
										type : "warning"
									});
								}
						  });
				
				//提交表单
				});
			
		});
		$("#btn_save_update")
				.click(
						function() {

							//进行表单验证
							var bv = updateForm.data('bootstrapValidator');
							bv.validate();
							//如果表单校验成功，提交数据
							if (bv.isValid()) {
								//$("#updateForm").serialize()
								/* alert($("#updateForm").serialize());
								alert($("#updateForm").serializeArray());
								alert( JSON.stringify($("#updateForm").serialize())); */
								/* var arr = $("#updateForm").serialize().split("&");
								var param = new Object();
								for(var i=0; i<arr.length; i++){
									var temp = arr[i].split("=");
									param[temp[0]] = temp[1];
								} 
								alert(ShowTheObject(arr));
								 */
								// 				var param = "id=" + $("#updateId").val() + "&dname="
								// 				+ $("#updateName").val() + //姓名
								// 				"&taxi.id=" + $("#taxiSelect").val() + //+ //车牌号id 
								// 				"&company.id=" + $("#comSelect").val() + //公司id
								// 				"&sex=" + $("#sex").val() + "&age="
								// 				+ $("#age").val() + "&phone=" + $("#phone").val() + //联系电话
								// 				"&identityCard=" + $("#identityCard").val() + //身份证号
								// 				"&driNum=" + $("#driNum").val() + //驾驶证号
								// 				"&occStartTime=" + $("#occStartTime").val() + //从业资格证起始日期
								// 				"&qdType=" + $("#qdType").val() + //准驾车型
								// 				"&occSendIns=" + $("#occSendIns").val() + //从业资格证发放机构
								// 				"&evaGrade=" + $("#evaGrade").val() + //评定等级
								// 				"&occEndTime=" + $("#occEndTime").val() + //从业资格证终止日期
								// 				"&moniUnit=" + $("#moniUnit").val() + //监督单位
								// 				"&moniPhone=" + $("#moniPhone").val() + //监督电话
								// 				"&quaNum=" + $("#quaNum").val() + //资格证号
								// 				"&comPhone=" + $("#comPhone").val() + //公司电话
								// 				"&home=" + $("#home").val(); //家庭住址 
								// 				$.ajax({
								// 					url : "updateDriverById",
								// 					type : "POST",
								// 					data : param,
								// 					success : function(data) {
								// 						if (data == "success") {
								// 							swal({
								// 								title : "操作执行成功",
								// 								type : "success"
								// 							});
								// 							//还未测试
								// 							tables.fnDraw(true); //刷新保持分页状态
								// 							//1.添加成功后，隐藏表单
								// 							$('#myModal1').modal('hide');
								// 							//2.销毁
								// 							updateForm.data('bootstrapValidator').destroy();
								// 						} else {
								// 							swal({
								// 								title : "操作失败",
								// 								type : "error"
								// 							});
								// 						}
								// 					}
								// 				});
								var params = new FormData($("#updateForm")[0]);
								$
										.ajax({
											url : "${pageContext.request.contextPath}/updateDriverById",
											type : "POST",
											data : params,
											dataType : 'json',
											contentType : false, //必须
											processData : false, //必须
											success : function(data) {
												if (data.result == "success") {
													swal({
														title : "操作执行成功",
														type : "success"
													});

													//还未测试
													tables.fnDraw(true); //刷新保持分页状态

													//1.添加成功后，隐藏表单
													$('#myModal1')
															.modal('hide');
													//2.销毁
													updateForm
															.data(
																	'bootstrapValidator')
															.destroy();

												} else {
													swal({
														title : "操作失败",
														type : "error"
													});
												}
											}
										});
							}
						});

		function getNowFormatDate(date) {
			var seperator1 = "-";
			var seperator2 = ":";
			var month = date.getMonth() + 1;
			var strDate = date.getDate();
			var currentdate = date.getFullYear() + seperator1 + month
					+ seperator1 + strDate + " 00:00:00.0";
			return currentdate;
		}

		$("#btn_add_save")
				.click(
						function() {
							// 			var param = $("#saveForm").serialize();
							var param = new FormData($("#saveForm")[0]);
							var bv = saveForm.data('bootstrapValidator');
							bv.validate();
							//如果表单校验成功，提交数据
							if (bv.isValid()) {
								$
										.ajax({
											url : "${pageContext.request.contextPath}/addDriver",
											type : "POST",
											data : param,
											dataType : 'json',
											contentType : false, //必须
											processData : false, //必须
											success : function(data) {
												if (data.result == "success") {
													swal({
														title : "操作执行成功",
														type : "success"
													});

													//1.添加成功后，隐藏表单
													$('#myModal5')
															.modal('hide');
													//2.刷新数据
													if (tables != null) {
														tables.fnDraw(true); //刷新保持分页状态
													} else {
														listDrivers();
													}
													//3.销毁
													saveForm
															.data(
																	'bootstrapValidator')
															.destroy();
													//4.清空form
													$(':input', '#saveForm')
															.not(
																	':button,:submit,:reset,:hidden') //将myform表单中input元素type为button、submit、reset、hidden排除
															.val('') //将input元素的value设为空值
															.removeAttr(
																	'checked')
															.removeAttr(
																	'checked') // 如果任何radio/checkbox/select inputs有checked or selected 属性，将其移除
												} else {
													swal({
														title : "操作失败",
														type : "error"
													});
												}
											}
										});
							}
						});
	</script>
</body>

</html>

<!-- 解决select2在bootstrap的modal中默认不显示的问题 -->
