package com.hx.gps.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Role entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "role" )
public class Role implements java.io.Serializable {

	// Fields

	private Integer id;
	private Company company;
	private String name;
	private String remark;
	private Set<User> users = new HashSet<User>(0);

	// 20170318 fmy添加权限属性

	private Set<Power> powers;

	// Constructors

	/** default constructor */
	public Role() {
	}

	/** full constructor */
	public Role(Company company, String name, String remark, Set<User> users) {
		this.company = company;
		this.name = name;
		this.remark = remark;
		this.users = users;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "comId")
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "remark")
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "role")
	public Set<User> getUsers() {
		return this.users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}


	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "role_power", joinColumns = { @JoinColumn(name = "roleId") }, // JoinColumns定义本方在中间表的主键映射
			inverseJoinColumns = { @JoinColumn(name = "powerId") })
	public Set<Power> getPowers() {
		return powers;
	}

	public void setPowers(Set<Power> powers) {
		this.powers = powers;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Role [id=");
		builder.append(id);
		builder.append(", company=");
		builder.append(company);
		builder.append(", name=");
		builder.append(name);
		builder.append(", remark=");
		builder.append(remark);
		builder.append(", users=");
		builder.append(users);
		builder.append(", powers=");
		builder.append(powers);
		builder.append("]");
		return builder.toString();
	}

}