package com.hx.gps.entities.tool;

import com.hx.gps.util.DateUtil;
import com.mongodb.BasicDBObject;

public class Alarm2 {
	private String taxiNum; // 数据表中无该属性，添加车牌号只是为了便于向前台传输
	private String isuNum; // 报警信息的唯一标示，终端手机号
	private String alarmType; // 报警类型
	private String time; // 报警时间
	private String dealState; // 处理状态
	private String pgsstate; // 车辆的载客状态，但808协议暂时未启用该属性
	/* private String remindState; //提示状态 */
	private String alarmIdentify; // 报警标识

	public String getAlarmIdentify() {
		return alarmIdentify;
	}

	public void setAlarmIdentify(String alarmIdentify) {
		this.alarmIdentify = alarmIdentify;
	}

	public String getTaxiNum() {
		return taxiNum;
	}

	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}

	public String getIsuNum() {
		return isuNum;
	}

	public void setIsuNum(String isuNum) {
		this.isuNum = isuNum;
	}

	public String getAlarmType() {
		return alarmType;
	}

	public void setAlarmType(String alarmType) {
		this.alarmType = alarmType;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getDealState() {
		return dealState;
	}

	public void setDealState(String dealState) {
		this.dealState = dealState;
	}

	public String getPgsstate() {
		return pgsstate;
	}

	public void setPgsstate(String pgsstate) {
		this.pgsstate = pgsstate;
	}

	@Override
	public String toString() {
		return "Alarm [taxiNum=" + taxiNum + ", isuNum=" + isuNum + ", alarmType=" + alarmType + ", time=" + time
				+ ", dealState=" + dealState + ", pgsstate=" + pgsstate + ", alarmIdentify=" + alarmIdentify
				+ "]";
	}

	public Alarm2() {
		super();
	}

	public Alarm2(BasicDBObject finds) {
		super();
		this.isuNum = finds.getString("isuNum");
		this.alarmType = finds.getString("alarmType");
		this.time = DateUtil.dateToString(finds.getDate("time"));
		if (finds.getString("alarmIdentify") != "" && finds.getString("alarmIdentify") != null) {
			this.alarmIdentify = finds.getString("alarmIdentify");
		}
		this.dealState = finds.getString("dealState");
	}
}


