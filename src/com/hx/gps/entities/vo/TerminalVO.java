package com.hx.gps.entities.vo;

import com.hx.gps.entities.Taxi;

import java.util.List;

/**
 * @Author Keyon
 * @Date 2019/11/30 15:16
 * @Desc 终端升级页面 VO
 */
public class TerminalVO {

    Integer taxiId; //id

    String companyName; //公司ID

    String taxiNum; //车牌号

    String isuNum; //终端号

    String version; //当前版本

    String isuDate; //版本获取时间

    public Integer getTaxiId() {
        return taxiId;
    }

    public void setTaxiId(Integer taxiId) {
        this.taxiId = taxiId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTaxiNum() {
        return taxiNum;
    }

    public void setTaxiNum(String taxiNum) {
        this.taxiNum = taxiNum;
    }

    public String getIsuNum() {
        return isuNum;
    }

    public void setIsuNum(String isuNum) {
        this.isuNum = isuNum;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getIsuDate() {
        return isuDate;
    }

    public void setIsuDate(String isuDate) {
        this.isuDate = isuDate;
    }

    @Override
    public String toString() {
        return "TerminalVO{" +
                "taxiId=" + taxiId +
                ", companyName='" + companyName + '\'' +
                ", taxiNum='" + taxiNum + '\'' +
                ", isuNum='" + isuNum + '\'' +
                ", version='" + version + '\'' +
                ", isuDate='" + isuDate + '\'' +
                '}';
    }
}
