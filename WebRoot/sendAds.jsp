<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title></title>
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="css/plugins/jsTree/style.min.css" rel="stylesheet">

<!--  添加表单验证 -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />
<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>
<!-- <base target="_blank"> -->
<style>
.jstree-open>.jstree-anchor>.fa-folder:before {
	content: "\f07c"
}

.jstree-default .jstree-icon.none {
	width: 0
}


.red{

    color:#fda6a6;
}

.green{
color: #2b6901 ;
font-weight:900;
}
.black{

    color:#000000;

}
 
</style>
</head>

<body class="gray-bg">

	<div class="wrapper animated fadeInRight">
		<!-- 修改弹出框 -->
		<div class="modal inmodal fade" id="myModal1" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form action="updateAds" method="post"  class="form-horizontal"  id="submitForm">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>修改广告内容</h5>
							</div>
							<div class="ibox-content"  >
								<input type="hidden" name="playId" id="playId">
								<div class="row">
									<div class="form-group">
									<label class="col-sm-3  control-label">播放速度:</label>
									<div class="col-sm-5">
										 <input name="playRate" id="playRate"
											class="form-control" type="text" placeholder="范围：(0-15)">
									</div>
                                 </div>
								<div class="form-group">
									<label class="col-sm-3 control-label">停留：</label>
									<div class="col-sm-5">
										 <input name="playStop" id="playStop" class="form-control"
											type="text" placeholder="范围：(0-60)">

									</div>
								</div>
								</div>

					
								<div class="row">
							    	<div class="form-group">
										 <label class="col-sm-3 control-label">广告内容：</label>
											<div class="col-sm-5">

											还可以输入<span style="font-family: Georgia; font-size: 20px;"
											id="wordCheck">100</span>个汉字
					                      
					                     
					               
					         

						    <textarea style="height: 250px;resize: none;" name="adsContext" id="adsContext"
							class="form-control" type="text" placeholder="请输入广告内容"
							required="" aria-required="true"
							onKeyUp="javascript:checkWord(this);"
							onMouseDown="javascript:checkWord(this);"
							style="overflow-y:scroll"></textarea>
					
                        </div>


				</div>	
 
								</div>
								<div class="row">
								<div class="form-group">
								<label class="col-sm-3 control-label">广告名称：</label>
									<div class="col-sm-5">
										 <input name="adsName" id="adsName" class="form-control"
											placeholder="请输入广告名称" type="text">

									</div>
									</div>

                                <div class="form-group">
                                <label class="col-sm-3 control-label">	广告类型： </label>
									<div class="col-sm-5">
									<select class="form-control" name="adsType" id="adsType"
											placeholder="请选择广告类型">
											<option value="0">普通</option>
											<option value="1">紧急</option>
											<option value="2">报警</option>
										</select>
									</div>
									</div>
								</div>
								<div class="row">
								<div class="form-group">
								 <label class="col-sm-3 control-label">	客户： </label>
									<div class="col-sm-5">
									<select class="form-control" name="adsCustomer"
											id="adsCustomer">

											<c:forEach items="${adsCustomers}" var="adsCustomers">
												<option value="${adsCustomers.adsCustomerName}">
													${adsCustomers.adsCustomerName}</option>
											</c:forEach>

										</select>
									</div>
 								</div>

									<%-- 	<div class="col-sm-6">
										开始时间： <input name="startYMD" class="form-control" type="text"
											id="startYMD" value="${sensorStartDate}" class="Wdate"
											onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd',maxDate:'#F{$dp.$D(\'endYMD\')||\'new Date()\'}'})">

									</div> --%>

									<!-- </div>
								<div class="row"> -->
									<%-- <div class="col-sm-6">
										结束时间： <input name="endYMD" class="form-control" type="text"
											id="endYMD" value="${sensorEndDate}" class="Wdate" value=""
											onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'startYMD\')}'})">

									</div> --%>




									<%-- 
									<div class="col-sm-6">
										播出时间段： <select name="period" id="period" class=" form-control">
											<option value="">请选择时段</option>
											<c:forEach items="${adsTime}" var="ads">
												<option value="${ads.adsTimeId}">${ads.adsTimeName}</option>
											</c:forEach>

										</select>

									</div> --%>
									<!-- </div> -->
									<!-- <div class="row"> -->
								<div class="form-group">
								 <label class="col-sm-3 control-label">	设置字体： </label>
									<div class="col-sm-5">
										 <select class="form-control" name="playFontType"
											id="playFontType">
											<option value="0" selected="selected">宋体</option>

										</select>
									</div>
								</div>
							</div>
								<div class="row">
									<div class="form-group">
									 <label class="col-sm-3 control-label">	字体大小： </label>
									<div class="col-sm-5">
										<select class="form-control" name="playFontSize"
											id="playFontSize">
											<option value="0">自适应</option>

										</select>
									</div>
								</div>
								<div class="form-group">
								 <label class="col-sm-3 control-label">	颜色： </label>
									<div class="col-sm-5">
										 <select class="form-control" name="playFontColor"
											id="playFontColor">
											<option value="7">单基色</option>

										</select>
									</div>
									</div>
								<div class="form-group">
								 <label class="col-sm-3 control-label">	设置时间或次数： </label>
									<div class="col-sm-5">
										 <input name="playNumber" id="playNumber"
											class="form-control" type="text">


									</div>
								</div>
								</div>
								<div class="row">
									<div class="form-group">
									 <label class="col-sm-3  control-label">	循环类型： </label>
									<div class="col-sm-5">
										 <input value="0" id="playType0" name="playType"
											type="radio">次数 <input value="1" id="playType1"
											name="playType" type="radio">时间
									</div>
									</div>



								</div>
								<div class="row">
									<!-- <div class="col-sm-6">
										是否为即时广告： <input value="1" name="immeAds" type="radio"
											id="immeAds">是 <input value="0" id="immeAds"
											name="immeAds" type="radio">否

									</div> -->


									<!-- <div class="col-sm-6">
										设置无广告段播出： <input value="1" name="noAdsTime" type="radio"
											id="noAdsTime">是 <input value="0" id="noAdsTime"
											name="noAdsTime" type="radio">否
									</div> -->
								</div>

							</div>
						</div>

						 
							<div class="modal-footer">
								<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
								<button type="submit" class="btn btn-primary">保存</button>
							</div>
					</form>

				</div>
			</div>
		</div>


		<!-- 修改结束 -->
		<div class="row">
			<div class="col-sm-9">
 
				<div class="ibox-content" style=" overflow-x: hidden; overflow-y: auto; height: 831px;">
 
					<!-- <div class="form-group">

						<input type="text" class="form-control form-control-gy"
							id="filter" placeholder="搜索">
					</div> -->
					<table  id="table1" width="100%"
						style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
						<thead>
							<tr>
								<th>广告名称</th>
								<th>客户</th>
								<th align="center">内容</th>
								<th>记录时间</th>
								<th>开始时间</th>
								<th>结束时间</th>
								<th>周期</th>
								<th>信息号</th>
								<th>操 作</th>
								 
								 
								<th> <input type="checkbox" id="selectAll">&nbsp;全选</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${advertises}" var="advertises">
								<tr id="${advertises.playId}">
									<td>${advertises.adsName}</td>
									<td>${advertises.adsCustomer}</td>
									<td>${advertises.adsContext}</td>	
									<td>${advertises.addTime}</td>
									<td>${advertises.dateStart}</td>
									<td>${advertises.dateEnd}</td>
									<td>${advertises.weekScorll}</td>
									<td>${advertises.playId}</td>

									<td><button id="btn-update" type="button"
											class="btn btn-success btn-sm" data-toggle="modal"
											data-target="#myModal1"
											onclick="change('${advertises.playId}','${advertises.adsName}','${advertises.adsContext}','${advertises.adsCustomer}','${advertises.playRate}','${advertises.playStop}','${advertises.playMode}','${advertises.playType}','${advertises.playNumber}','${advertises.dateStart}','${advertises.dateEnd}','${advertises.playFontType}','${advertises.playFontSize}','${advertises.playFontColor}')">修改</button> 
								 
										<button id="btn-delete" type="button"
											class="btn btn-danger btn-sm"
											onclick="delete1('${advertises.playId}','${advertises.adsName}')">删除
										</button>
									</td>
									<td><input type="checkbox" name="selectTr"
										value="${advertises.playId}"></td>
								</tr>
							</c:forEach>
						</tbody>
						 	 <tfoot>
							<tr>
								<td colspan="10">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>

			</div>

			<!--右侧公司选择栏开始  -->
			<div class="col-sm-3 " >
				<!-- <div class="ibox-title"> -->
				<input type="hidden" id="timer">
				<div class=" float-e-margins">
 
					<div class="ibox-content" style="height: 830px">
 
					<button type="button" class="btn btn-primary btn-sm-2"
							onclick="select()">下发</button>
						<input type="hidden" name="taxiNums" id="taxiNums">
						<input type="hidden" name="adsPlayId" id="adsPlayId">
						<label class="control-label">
						<h4>请选择车辆</h4>
						</label>&nbsp;&nbsp;&nbsp;

						<input type="text" class="form-control form-control-sm"
							id="txtIndustryArea"> <br> <input type="hidden"
							id="show1">
						<!-- 用来显示选中的车辆，即节点 -->
						<div id="jstree1" class="trdemo"
							style=" overflow-x: auto; overflow-y: auto; height: 500px;">
							<%-- <ul>
								<li class="jstree-open trdemo">所有公司   [${totality}]
									<ul>
										<c:forEach items="${comInfo2s}" var="comInfo2s">
											<li class="trdemo" id="${comInfo2s.comId}">${comInfo2s.cname } [${comInfo2s.count }]
												<ul>
													<c:forEach items="${comInfo2s.taxis}" var="taxis">
														<li class="trdemo" id="${taxis.taxiNum}"
															data-jstree='{"type":"car"}'>${taxis.taxiNum}
															[<span class="${taxis.onlineState=='离线'?'red':'green' }"> ${taxis.onlineState}</span> ] 
														</li>
													</c:forEach>
												</ul>
											</li>
										</c:forEach>
									</ul>
								</li>
							</ul> --%>
						</div>

					</div>
				</div>
			</div>
			<%-- <div class="col-sm-3">
				<div class="float-e-margins">

					<div class="ibox-content">
						<button type="button" class="btn btn-primary btn-sm"
							onclick="select()">下发</button>

						<br> <input type="hidden" name="taxiNums" id="taxiNums">
						<input type="hidden" name="adsPlayId" id="adsPlayId">
						<hr>
						<h4>请选择车辆</h4>
						<div id="jstree1">
							<ul>
								<li class="jstree-open">所有公司
									<ul>
										<c:forEach items="${comInfo2s}" var="comInfo2s">
											<li id="${comInfo2s.comId}">${comInfo2s.cname }
												<ul>
													<c:forEach items="${comInfo2s.taxis}" var="taxis">
														<li id="${taxis.taxiNum}" data-jstree='{"type":"car"}'>
															${taxis.taxiNum }</li>
													</c:forEach>
												</ul>
											</li>
										</c:forEach>
									</ul>
								</li>
							</ul>
						</div>

					</div>
				</div>
			</div> --%>
		</div>
		<!-- 删除表格 -->
		<form id="deleteForm" action="deleteAdsById" method="post">
			<input type="hidden" id="deleteId" name="id">
		</form>
	</div>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/content.min.js?v=1.0.0"></script>
	<script src="js/demo/echarts-demo2.min.js"></script>
	<script src="js/plugins/jsTree/jstree.min.js"></script>
	<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script type="text/javascript">
		//选择公司开始
			/* $("#miletable").hide(); */
			$("#taxiNums").val("");
			function pollingTree(){
		  	      var newdata = sessionStorage.getItem("treelist");
		  	  if(newdata!=null){
		  	    var tree= $("#jstree1")
           	   tree.jstree(true).settings.core.data = JSON.parse(newdata);
           	   tree.jstree(true).refresh(true);
		  	  }
			}
			//页面加载的时候选中转发过来的id
			var idSelect='<%=request.getAttribute("selected")%>';
			
			$(document).ready(function(){
				if(idSelect!=null){ 
				$("input[name=selectTr][value="+idSelect+"]").attr("checked", 'checked');
				/* swal({
					title : "指定信息号已被选中",
					type : "success"
				}); */
				}
				})
			//实现jQuery轮询树的状态
			$(document).ready(function(){
				$('#table1').DataTable({
					"pagingType" : "full_numbers"
				});
				setInterval(pollingTree, 10*1000);  
				});
			$("#jstree1").jstree({
				"core" : {
					"check_callback" : true,
					'data' : function (node, cb) {
						if(sessionStorage.getItem("treelist")==null){
						    $.ajax({
						    	type:"POST",
						    	dataType:"json",
						    	url : "/GpsManage/getTreeOfTempJson",
						    	success: function(data) {
						    		cb.call(this,data);
						   		}
							});
						}else{
							var newdata = sessionStorage.getItem("treelist");
							cb.call(this,JSON.parse(newdata));
						}
					}
				},
				"plugins" : [ "types", "dnd", "checkbox", "sort",
					"search", "unique" ],
				"types" : {
					"car" : {
						"icon" : "fa fa-car"
					},
					"default" : {
						"icon" : "fa fa-folder"
					}
				}
			});

			//获取选择节点的id
			$("#jstree1").on('changed.jstree', function(e, data) {
				r = [];
				var i, j;
				for (i = 0, j = data.selected.length; i < j; i++) {
					var node = data.instance.get_node(data.selected[i]);
					if (data.instance.is_leaf(node)) {
						r.push(node.id);
					}
				}
				$("#taxiNums").val(r);
			}).on("search.jstree", function(e, data) {
						if (data.nodes.length) {
							var matchingNodes = data.nodes; // change
							$(this).find(".jstree-node").hide().filter(
									'.jstree-last').filter(function() {
								return this.nextSibling;
							}).removeClass('jstree-last');
							data.nodes.parentsUntil(".jstree")
									.addBack()
									.show()
									.filter(".jstree-children")
									.each(function() {
												$(this).children(".jstree-node:visible")
														.eq(-1)
														.addClass("jstree-last");
											});
							// nodes need to be in an expanded state
							matchingNodes.find(".jstree-node").show(); // change
						}
					}).on( "clear_search.jstree", function(e, data) {
						if (data.nodes.length) {
							$(this).find(".jstree-node").css("display", "")
									.filter('.jstree-last').filter( function() {
												return this.nextSibling;
							}).removeClass('jstree-last');
						}
					});
			
			//jsTree的模糊查询
			var to = false;
			$("#txtIndustryArea").keyup(function() {
				var v = $("#txtIndustryArea").val();
				$("#jstree1").jstree(true).search(v);
			});


		

	</script>
	<script>
		var trs = document.getElementById('table1').getElementsByTagName('tr');
		window.onload = function() {
			for (var i = 0; i < trs.length; i++) {
				trs[i].onmousedown = function() {
					tronmousedown(this);
				}
			}
		}
		function tronmousedown(obj) {
			for (var o = 0; o < trs.length; o++) {
				if (trs[o] == obj) {
					trs[o].style.backgroundColor = '#DFEBF2';
				} else {
					trs[o].style.backgroundColor = '';
				}
			}
		}

	 
	</script>
	<script type="text/javascript">
	$("#selectAll").click(function() {
    $("input[name='selectTr']").prop("checked", this.checked);
  });
  
  $("input[name='selectTr']").click(function() {
    var $subs = $("input[name='selectTr']");
    $("#selectAll").prop("checked" , $subs.length == $subs.filter(":checked").length ? true :false);
  });


</script>
	<script>
		function select() {
			//获取选中的车辆
			var taxiNums = $("#taxiNums").val();
			//选中的广告id
			var taxinum;
			taxinum=taxiNums.split(",");
			//记录长度
			var len=taxiNums.length;
			var len2=taxinum.length;
		 	obj = document.getElementsByName("selectTr");
			//数组接收  ，被选中的广告
		    check_val = [];
		    for(k in obj){
		        if(obj[k].checked)
		            check_val.push(obj[k].value);
		    }
			if (taxiNums == "") {
				swal({
					title : "请先选择车辆",
					type : "warning"
				});
				return;
			} else if (check_val == "") {
				swal({
					title : "请选择要下发的广告",
					type : "warning"
				});
				return;
			}else if(len==1){
				swal({
					title : "该公司暂无车辆或车牌号格式有误",
					type : "warning"
				});
				return;
			}else if(len<7){
				swal({
					title : "车牌号长度有误",
					type : "warning"
				});
				return;
			} 
				var url = "SendAdsByAjax";
				$.post(url, {
					"taxiNums" : taxiNums,
					"playIdArray" :check_val
				}, function(data) {
					swal({
						title : data,
						type : "success"
					});
				 
				});

			 

		}
	</script>
	<script>
		function change(playId, adsName, adsContext, adsCustomer, PlayRate,
				PlayStop, playMode, playType, playNumber, dateStart, dateEnd,
				playFontType, playFontSize, playFontColor) {

			$("#playId").val(playId);
			$("#adsName").val(adsName);
			$("#adsContext").val(adsContext);
			$("#adsCustomer").find("option[value=" + adsCustomer + "]").attr(
					"selected", true);
			$("#playRate").val(PlayRate);
			$("#playStop").val(PlayStop);
			$("#playMode").val(playMode);
			$("#playNumber").val(playNumber);
			/* 	$("#dateStart").val(dateStart);
			 $("#dateEnd").val(dateEnd); */
			$("#playFontType").val(playFontType);
			$("#playFontSize").val(playFontSize);
			$("#playFontColor").val(playFontColor);
			$("#playType" + playType).attr("checked", true);
		}
	</script>
	<script>
	//点击"删除"按钮
	function delete1(id,name) {
		swal({
			title : "",
			text : "您确定删除名为" + name + " 的广告数据吗?",
			type : "warning",
			showCancelButton : true,
			cancelButtonText : "取消"
		}, function() {
			$("#deleteId").val(id);
			//提交表单
			$("#deleteForm").submit();
		})
	}
	//页面加载
	$(document).ready(function(){
		var result='<%=request.getAttribute("state")%>';
			if(result != null && result != "null"){
				if (result.indexOf("成功")>0) {
				swal({
					title : result,
					type : "success",
				});
				 
			}else{
				swal({
					title :result,
					type : "error"
				});
			}
			}

	});
	</script>
<script type="text/javascript">
		var maxstrlen = 100;

		function Q(s) {
			return document.getElementById(s);
		}

		function checkWord(c) {

			len = maxstrlen;
			var str = c.value;
			myLen = getStrleng(str);
			var wck = Q("wordCheck");
			if (myLen > len * 2) {
				c.value = str.substring(0, i-1);
			} else {
				wck.innerHTML = Math.floor((len * 2 - myLen) / 2);
			}

		}

		function getStrleng(str) {

			myLen = 0;
			i = 0;
			for (; (i < str.length) && (myLen <= maxstrlen * 2); i++) {
				if (str.charCodeAt(i) > 0 && str.charCodeAt(i) < 128)
					myLen++;
				else
					myLen += 2;
			}
			return myLen;
		}
		$(document).ready(function formValidator() {
			$('#submitForm').bootstrapValidator({
				feedbackIcons : {/*输入框不同状态，显示图片的样式*/
					valid : 'glyphicon glyphicon-ok',
					invalid : 'glyphicon glyphicon-remove',
					validating : 'glyphicon glyphicon-refresh'
				},
				/*生效规则：字段值一旦变化就触发验证*/
				live : 'enabled',
				/*当表单验证不通过时，该按钮为disabled*/
				submitButtons : 'button[type="submit"]',
				/*验证*/
				fields : {
					playMode : {
						message : '',
						validators : {
							notEmpty : {/*非空提示*/
								message : '播放模式不能为空'
							}
						}
					},
					playRate : {
						message : '',
						validators : {
							notEmpty : {
								message : '播放速度不能为空'
							},
							between : {
								min : 0,
								max : 15,
								message : '请输入0-15之间的数'
							}
						}
					},
					playStop : {
						validators : {
							notEmpty : {
								message : '停留不能为空'
							},
							between : {
								min : 0,
								max : 60,
								message : '请输入0-60之间的数'
							}
						}
					},
					adsContext : {
						validators : {
							notEmpty : {
								message : '广告内容不能为空'
							}
						}
					},
					adsName : {
						validators : {
							notEmpty : {
								message : '广告名称不能为空'
							}
						}
					},
					adsType : {
						validators : {
							notEmpty : {
								message : '广告类型不能为空'
							}
						}
					},
					adsCustomer : {
						validators : {
							notEmpty : {
								message : '客户不能为空'
							}
						}
					},
					startYMD : {
						validators : {
							notEmpty : {
								message : '开始时间不能为空'
							}
						}
					},
					endYMD : {
						validators : {
							notEmpty : {
								message : '结束时间不能为空'
							}
						}
					},
					weekDay : {
						validators : {
							notEmpty : {
								message : '设置周期不能为空'
							}
						}
					},
					period : {
						validators : {
							notEmpty : {
								message : '播出时间段不能为空'
							}
						}
					},
					playId : {
						validators : {
							notEmpty : {
								message : '信息号不能为空'
							},
							between : {
								min : 0,
								max : 24,
								message : '请输入0-24之间的数'
							},
							remote : {
								url : 'hasPlayId',//验证地址
								message : '信息号已存在',
								delay : 800,
								type : 'POST'

							},
						}
					},
					playFontType : {
						validators : {
							notEmpty : {
								message : '设置字体不能为空'
							}
						}
					},
					playFontSize : {
						validators : {
							notEmpty : {
								message : '字体大小不能为空'
							}
						}
					},
					playFontColor : {
						validators : {
							notEmpty : {
								message : '字体颜色不能为空'
							}
						}
					},
					playNumber : {
						validators : {
							notEmpty : {
								message : '循环时间或次数不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					}
				}

			});
		});
		/*日历刷新验证*/
		function refreshValidator(id, name) {
			$(id).data('bootstrapValidator').updateStatus(name,
					'NOT_VALIDATED', null).validateField(name);
		}
</script>

</body>

</html>