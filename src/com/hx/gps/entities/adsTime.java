package com.hx.gps.entities;

import com.mongodb.BasicDBObject;

public class adsTime {
	private Integer adsTimeId;
	private String adsTimeName;
	private String adsTimeStart;
	private String adsTimeEnd;

	public int getAdsTimeId() {
		return adsTimeId;
	}

	public void setAdsTimeId(Integer adsTimeId) {
		this.adsTimeId = adsTimeId;
	}

	public String getAdsTimeName() {
		return adsTimeName;
	}

	public void setAdsTimeName(String adsTimeName) {
		this.adsTimeName = adsTimeName;
	}

	public String getAdsTimeStart() {
		return adsTimeStart;
	}

	public void setAdsTimeStart(String adsTimeStart) {
		this.adsTimeStart = adsTimeStart;
	}

	public String getAdsTimeEnd() {
		return adsTimeEnd;
	}

	public void setAdsTimeEnd(String adsTimeEnd) {
		this.adsTimeEnd = adsTimeEnd;
	}

	public adsTime() {
	}

	public adsTime(Integer adsTimeId, String adsTimeName, String adsTimeStart, String adsTimeEnd) {
		super();
		this.adsTimeId = adsTimeId;
		this.adsTimeName = adsTimeName;
		this.adsTimeStart = adsTimeStart;
		this.adsTimeEnd = adsTimeEnd;
	}

	public adsTime(BasicDBObject adsTimeOb) {
		super();
		this.adsTimeId = adsTimeOb.getInt("adsTimeId");
		this.adsTimeName =adsTimeOb.getString("adsTimeName");
		this.adsTimeStart = adsTimeOb.getString("adsTimeStart");
		this.adsTimeEnd = adsTimeOb.getString("adsTimeEnd");
	}

	@Override
	public String toString() {
		return "adsTime [adsTimeId=" + adsTimeId + ", adsTimeName=" + adsTimeName + ", adsTimeStart=" + adsTimeStart
				+ ", adsTimeEnd=" + adsTimeEnd + "]";
	}

}
