/**
 * 
 */
package com.hx.gps.dao;

import com.hx.gps.entities.User;

/**
 * @author Minazuki
 *
 */
public interface UserDao {
	
	//根据账号密码查找用户
	public User findUserByAccount(String account,String password);
	
	//根据用户名查找用户，注册时判断用户名是否唯一
	public User findUserByName(String name);
		
	//修改用户密码
	public void updatePass(User user,String newPassword);
 
	//根据id查找用户
	public User findUserById(Integer id);

}
