package com.hx.gps.test;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.hx.gps.entities.tool.*;
import org.junit.Test;

import com.hx.gps.dao.AddressDao;
import com.hx.gps.dao.impl.AddressDaoImpl;
import com.hx.gps.service.AddressService;
import com.hx.gps.service.impl.AddressServiceImpl;
import com.hx.gps.util.DBUtil;
import com.hx.gps.util.DateUtil;
import com.hx.gps.util.FarmConstant;
import com.hx.gps.util.PositionUtil;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.QueryOperators;

public class test {

	DateUtil dateUtil = new DateUtil();
	AddressService addressService = new AddressServiceImpl();
	AddressDao addressDao = new AddressDaoImpl();

	@Test
	public void luanmatest() {
		String str = null;
		String a = "璁剧疆overlay鐨刾osition";
		try {
			str = new String(a.getBytes("GBK"), "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		System.out.println(str);
	}
	@Test
	public void daoTest(){
		AddressDao dao=new AddressDaoImpl();
		Date date=new Date();
		List<Alarm> resultList = new ArrayList<>();
		for (int i=0;i<dao.getAllAlarmType(date,date,"3").size();i++){
		resultList=dao.getAllAlarmType(date,date,"3");}
	}
	// 向地址表中插入数据
	@Test
	public void test() {
		// Address address=new Address("B00001","1234",
		// "0","0",(double)120.401251,(double)36.11791,(float)40.2,0,new
		// Date());
		// 临时表：addressTemp 历史表：addressHistory1610
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ADDRESSHIS + "1611");
		BasicDBObject finds = new BasicDBObject();
		finds.put("isuNum", "018953299149");
		finds.put("serialNum", 1234);
		finds.put("alarm", "0");
		finds.put("state", "00000200");
		finds.put("longitude", (double) 120.401151);
		finds.put("latitude", (double) 36.11691);
		finds.put("height", (double) 36.11791);
		finds.put("speed", 100);
		finds.put("direction", 0);
		finds.put("time", new Date());
		coll.save(finds);
	}

	// 测试查询
	@Test
	public void test2() {
		/* String[] isuNums=new String[]{"100100053202"}; */
		// System.out.println(addressDao.screenOnline(isuNums));
		// AddressDao dao=new AddressDaoImpl();
		// System.out.println(dateUtil.getStringToDate("2016-10-8 1:41:28"));
		// System.out.println(dao.findAddressByDate("2016-10-07
		// 01:41:28","2016-10-10 12:41:28","B00001","addressHistory1610"));
	}

	@Test
	public void test3() {
		List<Point> points = new ArrayList<>();
		// 东北西南
		Point p1 = new Point(120.455746, 36.13691);
		Point p2 = new Point(120.361251, 36.09191);
		points.add(p1);
		points.add(p2);

		// addressService.lostAndFound(2, points,"2016-10-07
		// 01:41:28","2016-10-20 12:41:28", "B00001", null, null, null,"3");
	}

	// 向报警表中添加信息
	@Test
	public void test4() {
		DBCollection coll = DBUtil.getDBCollection("HxGps808Test", "alarmLog");
		BasicDBObject finds = new BasicDBObject();
		finds.put("isuNum", "018953299149");
		finds.put("alarmType", "紧急报警");
		finds.put("alarmIdentify", "00000000000000000000000000000001");
		finds.put("time", new Date());
		finds.put("pgsState", "空车");
		finds.put("dealState", "未处理");// 处理状态
		finds.put("preinfoId", 0);
		coll.save(finds);
	}

	// 日期和秒之间的相互转化
	@Test
	public void test5() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String timeStr = formatter.format((new Date()).getTime());
		System.out.println((new Date()).getTime());
		System.out.println(timeStr);
		try {
			Date date = formatter.parse(timeStr);
			System.out.println("date" + date);
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

	@Test
	public void test6() {
		DBCollection coll = DBUtil.getDBCollection("HXGPSTest", "evaluate1610");
		BasicDBObject finds = new BasicDBObject();
		finds.put("isuNum", "100100053201");
		finds.put("quaNum", "1122");
		finds.put("good", 3);
		finds.put("general", 1);
		finds.put("bad", 1);
		finds.put("unvalued", 5);
		finds.put("total", 10);
		coll.save(finds);
	}

	// 获取当月第一天
	@Test
	public void testForDate() {
		// 规定返回日期格式
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		Date theDate = calendar.getTime();
		GregorianCalendar gcLast = (GregorianCalendar) Calendar.getInstance();
		gcLast.setTime(theDate);
		// 设置为第一天
		gcLast.set(Calendar.DAY_OF_MONTH, 1);
		String day_first = sf.format(gcLast.getTime());
		// 打印本月第一天
		System.out.println(day_first);
	}

	// 获取当月最后一天
	@Test
	public void testForDatelast() {
		// 获取Calendar
		Calendar calendar = Calendar.getInstance();
		// 设置日期为本月最大日期
		calendar.set(Calendar.DATE, calendar.getActualMaximum(calendar.DATE));
		// 设置日期格式
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		String ss = sf.format(calendar.getTime());
		System.out.println(ss + " 23:59:59");
	}

	// 4、获取上个月的第一天
	@Test
	public void getBeforeFirstMonthdate() throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd ");
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 1);
		calendar.set(Calendar.MONTH, 1);
		calendar.set(Calendar.DAY_OF_MONTH, 0);
		System.out.println("上个月第一天：" + format.format(calendar.getTime()) + "23:59:59");
	}

	// 对mongodb使用子查询,可以使用
	@Test
	public void test7() {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "addressTemp");
		BasicDBObject finds = new BasicDBObject();
		List<Address> addresses = new ArrayList<>();
		finds.append(QueryOperators.OR, new BasicDBObject[] { new BasicDBObject("isuNum", "100100053208"),
				new BasicDBObject("isuNum", "100100053202") });

		DBCursor cursor = coll.find(finds);
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			addresses.add(new Address(finds));
		}
		System.out.println(addresses);
	}

	// mongodb的子查询方法2
	@Test
	public void test8() {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, "addressTemp");
		BasicDBObject finds = new BasicDBObject();
		List<Address> addresses = new ArrayList<>();
		List<String> str = new ArrayList<>();
		str.add("100100053202");
		str.add("100100053203");
		finds.put("time", new BasicDBObject("$lte", new Date()));
		// .append("$gte",dateUtil.getStringToDate(startDate))
		finds.append("isuNum", new BasicDBObject(QueryOperators.IN, str));
		DBCursor cursor = coll.find(finds);
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			addresses.add(new Address(finds));
		}
		System.out.println(addresses);
	}
	// 向计价器记录表中插入数据
	/*
	 * @Test public void insertOperate2(){ DBCollection coll =
	 * DBUtil.getDBCollection("HXGPSTest","operate1610");//插入10月份的数据
	 * BasicDBObject finds = new BasicDBObject(); finds.put("taxiNum","B00001");
	 * finds.put("quaNum","1111111"); finds.put("upTime",new Date());
	 * finds.put("downTime",new Date()); finds.put("emptymil",(float)123.5);
	 * finds.put("mileage",(float)60.5); finds.put("allTime","015522");
	 * finds.put("price",(float)2.50); finds.put("money",(float)(1.25));
	 * coll.save(finds); }
	 */

	// 向营运信息表插入数据
	@Test
	public void insertOperate() {
		DBCollection coll = DBUtil.getDBCollection("HXGPSTest", "operate1610");
		BasicDBObject finds = new BasicDBObject();
		finds.put("taxiNum", "B00005");
		finds.put("quaNum", "1111111");
		finds.put("upTime", new Date());
		finds.put("downTime", new Date());
		finds.put("emptymil", (float) 123.5);
		finds.put("mileage", (float) 60.5);
		finds.put("allTime", "015522");
		finds.put("price", (float) 2.50);
		finds.put("money", (float) (1.25));
		coll.save(finds);
	}

	// 添加命令日志信息
	@Test
	public void test101() {
		DBCollection coll = DBUtil.getDBCollection("HxGps808Test", FarmConstant.ORDERLOG);// 插入10月份的数据
		BasicDBObject finds = new BasicDBObject();
		finds.put("time", new Date());
		finds.put("serialNum", 5);
		finds.put("isuNum", "018953299149");
		finds.put("orderType", "文本消息下发");
		finds.put("identify", "终端显示器显示");
		finds.put("people", "007");
		finds.put("context", "015522");
		finds.put("state", 2);
		finds.put("sendTime", 10);
		coll.save(finds);
	}

	@Test
	public void test102() {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ORDERLOG);
		BasicDBObject finds = new BasicDBObject();
		List<OrderVector> list = new ArrayList<>();

		DBCursor cursor = coll.find();
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			list.add(new OrderVector(finds));
		}
		System.out.println(list);
	}

	// 添加终端心跳
	@Test
	public void test103() {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ISUHEART);// 插入10月份的数据
		BasicDBObject finds = new BasicDBObject();
		finds.put("time", new Date());
		finds.put("isuNum", "018953299149");
		coll.save(finds);
	}

	// 向计价器临时表中添加数据
	@Test
	public void test104() {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.METERTEMP);
		BasicDBObject finds = new BasicDBObject();
		finds.put("isuNum", "100100053204");// 018953299149
		finds.put("taxiNum", "C34M56");
		finds.put("quaNum", "12A4567");
		finds.put("state", 1);
		finds.put("time", new Date());
		coll.save(finds);
	}

	// 测试更新操作
	@Test
	public void test204() {
		DBCollection coll = DBUtil.getDBCollection("HxGps808Test", "alarmLog");
		BasicDBObject finds = new BasicDBObject();
		// finds.put("time",DateUtil.stringToDate("2016-11-9 16:36:53"));
		finds.put("dealState", "未处理");
		finds.put("alarmType", "紧急报警");
		finds.put("isuNum", "018953299149");
		// 查询出相应的记录
		// finds=(BasicDBObject) coll.findOne(finds);
		System.out.println(finds);
		BasicDBObject res = new BasicDBObject();
		BasicDBObject set = new BasicDBObject();
		set.put("dealState", "已处理");
		res.put("$set", set);
		coll.update(finds, res);

		// System.out.println("------------------------------");
		// DBCollection dbCol = db.getCollection(COLLECTION_NAME); //获取连接
		// DBCursor ret = dbCol.find(); //查找出所有文档
		// BasicDBObject doc = new BasicDBObject();
		// BasicDBObject res = new BasicDBObject();
		// res.put("age", 40);
		// System.out.println("将数据集中的所有文档的age修改成40！");
		// doc.put("$set", res);
		// dbCol.update(new BasicDBObject(),doc,false,true); //第一个参数应该为条件
		// System.out.println("更新数据完成！");
	}

	@Test
	public void testBD() {
		Point o = new Point(120.502177,36.185457);
		Point o2 = new Point(120.505689,36.182675);
		Point o3 = new Point(120.497577,36.183076);
		Point o4 = new Point(120.498943,36.18143);
		Gps gps = PositionUtil.bd09_To_Gps84(o.getLatitude(), o.getLongitude());
		Gps gps2 = PositionUtil.bd09_To_Gps84(o2.getLatitude(), o2.getLongitude());
		Gps gps3 = PositionUtil.bd09_To_Gps84(o3.getLatitude(), o3.getLongitude());
		Gps gps4 = PositionUtil.bd09_To_Gps84(o4.getLatitude(), o4.getLongitude());
		System.out.println("结果"+gps.toString());
		System.out.println("结果"+gps2.toString());
		System.out.println("结果"+gps3.toString());
		System.out.println("结果"+gps4.toString());
	}
}
