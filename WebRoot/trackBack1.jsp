<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title></title>
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">  
<link href="css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet" type="text/css">  
<script src="js/jquery.min.js"></script>  
<script src="js/bootstrap.min.js"></script>  
<script src="js/plugins/bootstrap-table/bootstrap-table.min.js"></script>  
<script src="js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>   
</head>
<body>  
<table class="table table-hover" id="cusTable"  
       data-pagination="true"  
       data-show-refresh="true"  
       data-show-toggle="true"  
       data-showColumns="true">  
       <thead>  
          <tr>                                                           
              <th  data-field="adsTimeId" data-sortable="true">  
                           销售记录id  
              </th>  
              <th data-field="adsTimeName" >  
                           销售单号  
              </th>  
               <th data-field="adsTimeStart" >  
                           销售单号  
              </th> 
               <th data-field="adsTimeEnd" >  
                           销售单号  
              </th> 
                
          </tr>  
       </thead>  
       <tbody>  
       </tbody>  
       
</table>  
       </body>
<script type="text/javascript">            
      function initTable() {  
        //先销毁表格  
        $('#cusTable').bootstrapTable('destroy');  
        //初始化表格,动态从服务器加载数据  
        $("#cusTable").bootstrapTable({  
            method: "get",  //使用get请求到服务器获取数据  
            url: "test",  //获取数据的Servlet地址  
            striped: true,  //表格显示条纹  
            pagination: true, //启动分页  
            pageSize: 1,  //每页显示的记录数  
            pageNumber:1, //当前第几页  
            pageList: [5, 10, 15, 20, 25],  //记录数可选列表  
            search: false,  //是否启用查询  
            showColumns: true,  //显示下拉框勾选要显示的列  
            showRefresh: true,  //显示刷新按钮  
            sidePagination: "server", //表示服务端请求  
            //设置为undefined可以获取pageNumber，pageSize，searchText，sortName，sortOrder  
            //设置为limit可以获取limit, offset, search, sort, order  
            queryParamsType : "undefined",   
            queryParams: function queryParams(params) {   //设置查询参数  
              var param = {    
                  pageNumber: params.pageNumber,    
                  pageSize: params.pageSize,  
                 // orderNum : $("#orderNum").val()  
              };    
              return param;                   
            },  
            columns: [
                      {
                          field: 'adsTimeId',
                          title: '序号',
                          width: 20,
                          align: 'center',
                          valign: 'middle',
                          sortable: false,
                      }, {
                          field: 'adsTimeName',
                          title: '**名称',
                          width: 200,
                          align: 'left',
                          valign: 'top',
                          sortable: false,
                      }, {
                          field: 'adsTimeStart',
                          title: '创建人',
                          width: 50,
                          align: 'left',
                          valign: 'top',
                          sortable: false
                      }, {
                          field: 'adsTimeEnd',
                          title: '创建时间',
                          width: 110,
                          align: 'middle',
                          valign: 'top',
                          sortable: false
                      }, {
                          field: 'operate',
                          title: '操作',
                          width: 100,
                          align: 'center',
                          valign: 'middle',
                          formatter: operateFormatter,
                          events: operateEvents
                      }],
            onLoadSuccess: function(){  //加载成功时执行  
              alert("加载成功");
            },  
            onLoadError: function(){  //加载失败时执行  
              layer.msg("加载数据失败", {time : 1500, icon : 2});  
            }  
          });  
      }  
  
      $(document).ready(function () {          
          //调用函数，初始化表格  
          initTable();  
  
          //当点击查询按钮的时候执行  
          $("#search").bind("click", initTable);  
      });  
</script>  
</html>