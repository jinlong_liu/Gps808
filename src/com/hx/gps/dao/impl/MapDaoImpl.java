package com.hx.gps.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hx.gps.dao.MapDao;
import com.hx.gps.entities.Graph;
import com.hx.gps.entities.Power;
import com.hx.gps.entities.Preinfo;
import com.hx.gps.entities.tool.Address;
import com.hx.gps.entities.tool.Point;
import com.hx.gps.entities.tool.PointsInfo;
import com.hx.gps.util.DBUtil;
import com.hx.gps.util.DateUtil;
import com.hx.gps.util.FarmConstant;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

@Repository
public class MapDaoImpl implements MapDao {
	@Autowired
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public Graph addGraph(Graph graph) {
		getSession().save(graph);
		return graph;
	}

	@Override
	public void deletePreInfo(Preinfo preinfo) {
		getSession().delete(preinfo);

	}

	@Override
	public void addPreInfo(Preinfo preinfo) {
		getSession().save(preinfo);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Preinfo> findAllPreinfoByDate() {
		Criteria criteria = getSession().createCriteria(Preinfo.class);
		List<Preinfo> p = criteria.list();
		// System.out.println(p);
		return p;
	}

	@Override
	public Preinfo findPreInfoById(Integer id) {
		Preinfo preinfo = null;
		if (id != null) {
			// 根据id来获取preinfo信息
			preinfo = (Preinfo) getSession().get(Preinfo.class, id);
		}

		return preinfo;
	}

	@Override
	public void addPreInfo2(PointsInfo pointsInfo) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.preInfo2);
		BasicDBObject finds = new BasicDBObject();

		// 添加信息
		finds.append("serial", pointsInfo.getSerial());
		// 首先进行查重的操作，由于这里通过serial来区分 故不再考虑重复。
		finds.append("pointsNum", pointsInfo.getPointsNum());
		List<Point> points = pointsInfo.getPoint();
		BasicDBObject db = new BasicDBObject();
		for (int i = 0; i < points.size(); i++) {
			Point point = points.get(i);
			double lat = point.getLatitude();
			double lng = point.getLongitude();
			db.append("point"+i, lat+","+lng);
		}
		finds.append("points",db);
		// 保存到数据库中
		try {
			coll.insert(finds);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public boolean deletePreInfo2(Integer serial) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.preInfo2);
		BasicDBObject finds = new BasicDBObject();
		finds.append("serial", serial);
		DBCursor cursor = coll.find(finds);
		if (cursor.hasNext()) {
			BasicDBObject dele = (BasicDBObject) cursor.next();
			// 删除该对象
			coll.remove(dele);
			return true;
		}
		// 无记录的话返回错误
		return false;
	}

	@Override
	public List<PointsInfo> findAllPreinfo2() {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.preInfo2);
		BasicDBObject finds = new BasicDBObject();
		List<PointsInfo> pointsInfos = new ArrayList<>();
		DBCursor cursor = coll.find();
		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			// 在构造器中传递参数
			  PointsInfo pointsInfo = new PointsInfo(finds);
			  pointsInfos.add(pointsInfo);
		}
		return pointsInfos;
	}
}
