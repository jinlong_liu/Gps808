<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">


<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">

<!-- 点击查看详情按钮弹出来的form表单 -->
					<div class="modal inmodal fade" id="myModal5" tabindex="-1"
						role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-body">
									<div class="ibox-title">
										<h5>照片详情</h5>
									</div>
								</div>
								<!-- <div style="width:500x;height:300px;"> -->
								<!-- <div style="width:200px;height:300px;"></div> -->
								<div style="text-align:center">
									<img id="picture2" src='' style="border:1px solid" />
									<!-- width:300px;height:300px; -->
								</div>
								<!-- </div> -->
								<div class="modal-footer">
									<button type="button" class="btn btn-white" onclick="increase()">放大图片</button>
									<button type="button" class="btn btn-white" onclick="shrink()">缩小图片</button>
									<!-- <button type="submit" class="btn btn-primary">保存</button> -->
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-white"
										data-dismiss="modal">关闭</button>
									<!-- <button type="submit" class="btn btn-primary">保存</button> -->
								</div>
							</div>
						</div>
					</div>

		<div class="row">
		<div class="col-sm-12">
				<!-- 日期+精确查询+导出表格开始 -->
				<!-- 	<div class="ibox float-e-margins"> -->
				<form action="getPictureByTaxi" class="form-inline" id="getOneForm" method="post">
					<div class="ibox-title">
						<h5 class="text-center">查询拍照记录</h5>
					</div>
					<div class="ibox-content" style="padding:15px 35px 20px">
						<div class="row">
							<div class="col-sm-3">
								<label class="control-label">开始时间： </label><input type="text" class="form-control" name="startDate" id="startDate"
									value="${sensorStartDate}" class="Wdate"
									onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true})">
							</div>
							<div class="col-sm-3">
								<label class="control-label">结束时间：</label> <input type="text" class="form-control" name="endDate" id="endDate"
									value="${sensorEndDate}" class="Wdate"
									onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'startDate\')}',dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true})">
							</div>
							<div class="col-sm-3">
								<label class="control-label">车牌号：</label><input placeholder="如B00001" type="text" class="form-control" id="taxiNum"
									name="taxiNum" value="${sensorTaxiNum}" />
							</div>
					
					
							<div class="col-sm-3">
								<button id="getOneBtn" type="button" class="btn btn-primary ">查询</button>
						
								<button type="button" class="btn btn-success demo1">导出</button>
							</div>
								</div>
					</div>
				</form>
			</div>
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>拍照信息</h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
							</a>
						</div>
					</div>
					<div class="ibox-content" style="height: 662px">
						<table
							class="table table-striped table-bordered table-hover dataTables-example">
							<thead>
								<tr>
									<th>车牌号</th>
									<th>终端号</th>
									<th>上传时间</th>
									<th>存储地址</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${picture}" var="picture">
									<tr>
										<td>${picture.taxiNum}</td>
										<td>${picture.isuNum}</td>
										<td>${picture.time}</td>
										<td>${picture.picAddress}</td>
										<td><input type="button" value="查看详情" data-toggle="modal"
											data-target="#myModal5"
											onclick="getPic('${picture.picAddress}')"></td>
									</tr>
								</c:forEach>
							</tbody>
							<tfoot>
							<tr>
								<td colspan="5">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
						</table>
					</div>
				</div>
			</div>
			
		</div>

	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/content.min.js?v=1.0.0"></script>
	<script>
		/* $("#picture2").bind("click", function() { */ //
		function increase(){
			var height = $("#picture2").height();
			if (height <= 600) {
				$("#picture2").height(height+200);
				//$(this).width(300);
			} else {
				swal({
					title : "已放大到最大限度",
					type : "warning"
				});
			}
		};
		function shrink(){
			var height = $("#picture2").height();
			if (height >= 176) {
				$("#picture2").height(height-200);
				//$(this).width(300);
			} else {
				swal({
					title : "无法再次缩小",
					type : "warning"
				});
			}
		}
		$(document)
				.ready(
						function() {
							//保留分页后的checkbox的值
							var checkedIds = "";
							$("input[name='chbox']")
									.change(
											function() {
												var oneches = document
														.getElementsByName("chbox");
												for (var i = 0; i < oneches.length; i++) {
													if (oneches[i].checked == true) {
														//避免重复累计id （不含该id时进行累加）
														if (checkedIds
																.indexOf(oneches[i].value) == -1) {
															checkedIds = checkedIds
																	+ oneches[i].value
																	+ ",";
														}
													}
													if (oneches[i].checked == false) {
														//取消复选框时 含有该id时将id从全局变量中去除
														if (checkedIds
																.indexOf(oneches[i].value) != -1) {
															checkedIds = checkedIds
																	.replace(
																			(oneches[i].value + ","),
																			"");
														}
													}
												}
												$("#manyIsu").val(checkedIds);
											});

							//点击"精确搜索"
							$("#getOneBtn").click(function() {
								var startTime = $("#startDate").val();
								var endTime = $("#endDate").val();
								var taxiNum = $("#taxiNum").val();
								var endYear = null;
								//开始时间的年份
								var startYear = startTime.substr(0, 4);
								//结束时间的年份
								 if (endTime == "") {
									endYear = new Date().getFullYear();
								} else {
									endYear = endTime.substr(0, 4);
								} 

								if (startTime == "") {
									swal({
										title : "开始时间不能为空",
										type : "warning"
									});
								} else if (endTime == ""){
									swal({
										title : "结束时间不能为空",
										type : "warning"
									});
								} else if (endYear != startYear) {
									swal({
										title : "不支持跨年查询，请重新选择开始时间或结束时间",
										type : "warning"
									});
								} else if (taxiNum == "") {
									swal({
										title : "请输入车牌号",
										type : "warning"
									});
								} else {
									$("#getOneForm").submit();
								}
							});

							//查看图片详情

							//点击"导出表格"按钮
							$(".demo1").click(
									function() {
										var startTime = $("#startDate").val();
										var endTime = $("#endDate").val();
										var nowTime = $("#endDate").val();//页面显示的结束时间
										var taxiNum = $("#taxiNum").val();
										if (nowTime == "" || nowTime == null) {
											nowTime = "现在时刻";
										}
										if (startTime == "" || taxiNum == "") {
											swal({
												title : "导出表格时，开始时间和车牌号不能为空",
												type : "warning"
											});
										} else {
											swal({
												title : "",
												text : "您确定导出\n" + startTime
														+ " 到 " + nowTime
														+ " 该期间，\n 车牌号为"
														+ taxiNum + "的数据吗?",
												type : "warning",
												showCancelButton : true,
												cancelButtonText : "取消"
											}, function() {
												//赋值
												$("#startDatePoi").val(
														startTime);
												$("#endDatePoi").val(endTime);
												$("#taxiNumPoi").val(taxiNum);
												//提交表单
												$("#sensorPoi").submit();
											})
										}
									});

							$(".dataTables-example").dataTable();
							var oTable = $("#editable").dataTable();
							oTable
									.$("td")
									.editable(
											"../example_ajax.php",
											{
												"callback" : function(sValue, y) {
													var aPos = oTable
															.fnGetPosition(this);
													oTable.fnUpdate(sValue,
															aPos[0], aPos[1])
												},
												"submitdata" : function(value,
														settings) {
													return {
														"row_id" : this.parentNode
																.getAttribute("id"),
														"column" : oTable
																.fnGetPosition(this)[2]
													}
												},
												"width" : "90%",
												"height" : "100%"
											})
						});

		/* function fnClickAddRow() {
			$("#editable").dataTable()
					.fnAddData(
							[ "Custom row", "New row", "New row", "New row",
									"New row" ])
		};*/
		function getPic(picAddress) {
		 
			//var url = "getPictureByAddr";
			
			/* $('#picture2').attr("src","getPictureByAddr?picAddress="+picAddress); */
			document.getElementById("picture2").src = "getPictureByAddr?picAddress="
					+ picAddress;
			/* args = {
				"picAddress" : picAddress
			}; */
			/* $.post(url, args, function(data) {
				//返回的数据不为空 
				//var newdata = eval("(" + data + ")");
				driver.html("");//避免选择框中的值持续叠加
				$("<img src='"+data+"'/>").appendTo(picture);
			}); */
		}
	</script>
</body>

</html>