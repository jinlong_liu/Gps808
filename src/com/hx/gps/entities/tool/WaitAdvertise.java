package com.hx.gps.entities.tool;

public class WaitAdvertise {

//终端号
private String isuNum;
//车牌号
private String taxiNum;
//播放项
private int playId;
//类型（广告，指令）
private String type;
//状态 发送中，未发送， 成功
private String state;
//失败次数
private int failNum;
//广告名称(这里代表操作人)
private String adsName;
//最后一次时间，成功的就为成功时间，失败的就为失败时间。
private String endTime;
public String getIsuNum() {
	return isuNum;
}
public void setIsuNum(String isuNum) {
	this.isuNum = isuNum;
}
public String getTaxiNum() {
	return taxiNum;
}
public void setTaxiNum(String taxiNum) {
	this.taxiNum = taxiNum;
}
public int getPlayId() {
	return playId;
}
public void setPlayId(int playId) {
	this.playId = playId;
}
public String getType() {
	return type;
}
public void setType(String type) {
	this.type = type;
}
public String getState() {
	return state;
}
public void setState(String state) {
	this.state = state;
}
public int getFailNum() {
	return failNum;
}
public void setFailNum(int failNum) {
	this.failNum = failNum;
}
public String getAdsName() {
	return adsName;
}
public void setAdsName(String adsName) {
	this.adsName = adsName;
}
public String getEndTime() {
	return endTime;
}
public void setEndTime(String endTime) {
	this.endTime = endTime;
}

//初始构造
public WaitAdvertise() {
	super();
}

//待发送详情。
public WaitAdvertise(String isuNum, String taxiNum, int playId, String type, String state, int failNum, String adsName,
		String endTime) {
	
	super();
	this.isuNum = isuNum;
	this.taxiNum = taxiNum;
	this.playId = playId;
	this.type = type;
	this.state = state;
	this.failNum = failNum;
	this.adsName = adsName;
	this.endTime = endTime;
}
// 带参Dbobject构造 。
@Override
public String toString() {
	return "WaitAdvertise [isuNum=" + isuNum + ", taxiNum=" + taxiNum + ", playId=" + playId + ", type=" + type
			+ ", state=" + state + ", failNum=" + failNum + ", adsName=" + adsName + ", endTime=" + endTime + "]";
}


}
