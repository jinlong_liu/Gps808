package com.hx.gps.entities.tool;

public class PictureInfo {
	//通道  分辨率
	private Integer gallery;
	private Integer resolution;
	//图片质量  亮度
	private Integer picQuality;
	private Integer luminance;
	//对比度  饱和度  色度
	private Integer contrast;
	private Integer saturability;
	private Integer chroma;
	public Integer getGallery() {
		return gallery;
	}
	public void setGallery(Integer gallery) {
		this.gallery = gallery;
	}
	public Integer getResolution() {
		return resolution;
	}
	public void setResolution(Integer resolution) {
		this.resolution = resolution;
	}
	public Integer getPicQuality() {
		return picQuality;
	}
	public void setPicQuality(Integer picQuality) {
		this.picQuality = picQuality;
	}
	public Integer getLuminance() {
		return luminance;
	}
	public void setLuminance(Integer luminance) {
		this.luminance = luminance;
	}
	public Integer getContrast() {
		return contrast;
	}
	public void setContrast(Integer contrast) {
		this.contrast = contrast;
	}
	public Integer getSaturability() {
		return saturability;
	}
	public void setSaturability(Integer saturability) {
		this.saturability = saturability;
	}
	public Integer getChroma() {
		return chroma;
	}
	public void setChroma(Integer chroma) {
		this.chroma = chroma;
	}
	@Override
	public String toString() {
		return "PictureInfo [gallery=" + gallery + ", resolution=" + resolution
				+ ", picQuality=" + picQuality + ", luminance=" + luminance
				+ ", contrast=" + contrast + ", saturability=" + saturability
				+ ", chroma=" + chroma + "]";
	}
	
	
}
