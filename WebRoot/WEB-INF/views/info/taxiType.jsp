<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">

<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>

<!--gy 自定义css -->
<link rel="stylesheet" href="css/myhx.css" />
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">

		<div class="row">

			<!-- 点击添加按钮弹出来的form表单 -->
			<div class="modal inmodal fade" id="myModal5" tabindex="-1"
				role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<form action="saveTaxiType" class="form-horizontal" method="post"
							id="form1">
							<div class="modal-body">
								<div class="ibox-title">
									<h5>新增车辆类型信息 <span style="color:red">（* 为必填项）</span></h5>
								</div>
								<div class="ibox-content">
									<div class="row">
										<div class="form-group">
											<!-- 车辆类型-->
											<label class="col-sm-3 control-label"><span style="color:red">*  </span>车辆类型 ：</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" name="type"
													id="type1">
											</div>
										</div>
									</div>
									<!-- 车辆类型新增长宽高 -->
									<div class="row">
										<div class="form-group">
											<!-- 车辆长度-->
											<label class="col-sm-3 control-label"><span style="color:red">*  </span>车辆长度(m)：</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" name="lengthOfCar"
													id="taxi_add_lengthOfCar">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<!-- 车辆宽度-->
											<label class="col-sm-3 control-label"><span style="color:red">*  </span>车辆宽度(m)：</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" name="widthOfCar"
													id="taxi_add_widthOfCar">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<!-- 车辆高度-->
											<label class="col-sm-3 control-label"><span style="color:red">*  </span>车辆高度(m)：</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" name="heightOfCar"
													id="taxi_add_heightOfCar">
											</div>
										</div>
									</div>
									<!-- 备注-->
									<div class="row">
										<div class="form-group">
											<label class="col-sm-3 control-label">备注：</label>
											<div class="col-sm-6">
												<textarea style="height:150px;resize: none;" name="remark"
													class="form-control" aria-required="true"></textarea>
											</div>
										</div>

									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-white"
											data-dismiss="modal">关闭</button>
										<button type="button" class="btn btn-primary"
											onclick="submitForm1()">保存</button>
									</div>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>

			<!-- 点击"修改"按钮弹出来的form表单 -->
			<div class="modal inmodal fade" id="myModal1" tabindex="-1"
				role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<form action="updateTaxiType" class="form-horizontal"
							method="post" id="form2">
							<input class="form-control" id="id2" name="id" type="hidden">
							<div class="modal-body">
								<div class="ibox-title">
									<h5>修改车辆类型<span style="color:red">（* 为必填项）</span></h5>
								</div>
								<div class="ibox-content">
									<div class="row">
										<div class="form-group">
											<label class="col-sm-3 control-label"><span style="color:red">*  </span>车辆类型 *：</label>
											<div class="col-sm-7">
												<input class="form-control" type="text" name="type" autocomplete="off"
													id="type2">
											</div>
										</div>
										<!-- 车辆类型新增长宽高 -->
									 
										<div class="form-group">
											<!-- 车辆长度-->
											<label class="col-sm-3 control-label"><span style="color:red">*  </span>车辆长度(m)：</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" name="lengthOfCar" autocomplete="off"
													id="taxi_update_lengthOfCar">
											</div>
										</div>
									 
										<div class="form-group">
											<!-- 车辆宽度-->
											<label class="col-sm-3 control-label"><span style="color:red">*  </span>车辆宽度(m)：</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" name="widthOfCar" autocomplete="off"
													id="taxi_update_widthOfCar">
											</div>
										</div>
								 
										<div class="form-group">
											<!-- 车辆高度-->
											<label class="col-sm-3 control-label"><span style="color:red">*  </span>车辆高度(m)：</label>
											<div class="col-sm-6">
												<input type="text" class="form-control" name="heightOfCar" autocomplete="off"
													id="taxi_update_heightOfCar">
											</div>
										</div>
									 
										<div class="form-group">
											<label class="col-sm-3 control-label">备注：</label>
											<div class="col-sm-7">
												<textarea style="height:150px;resize: none;" name="remark" id="remark2"
													class="form-control" aria-required="true"></textarea>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-white"
											data-dismiss="modal">关闭</button>
										<button type="button" class="btn btn-primary"
											onclick="submitForm2()">保存</button>
									</div>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>

		</div>

		<!-- 数据展示 -->
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>车辆类型表</h5>
				</div>
				<!--  gy 将添加按钮与搜索框放一块 -->
				<div class="ibox-content"  style="height: 775px">
					<table id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
						<thead>
						<tr>
						<td><button type="button" class="btn btn-primary btn-add1"
							data-toggle="modal" data-target="#myModal5">添&nbsp;&nbsp;&nbsp;加</button></td>
						</tr>
							<tr>
								<th>车辆类型</th>
								<th>车辆尺寸</th>
								<th>备注</th>
								<th>操作人</th>
								<th>添加时间</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${taxitypes}" var="taxitypes">
								<tr>
									<td>${taxitypes.type }</td>
									<td>${taxitypes.lengthOfCar}米*${taxitypes.widthOfCar}米*${taxitypes.heightOfCar}米</td>
									<td>${taxitypes.remark }</td>
									<td>${taxitypes.people}</td>
									<td>${taxitypes.time}</td>

									<td><button id="btn-update" type="button"
											class="btn btn-success btn-sm" data-toggle="modal"
											data-target="#myModal1"
											onclick="update('${taxitypes.id}','${taxitypes.type}','${taxitypes.remark }'
											,'${taxitypes.lengthOfCar}','${taxitypes.widthOfCar}','${taxitypes.heightOfCar}')">修改</button>
										<button type="button" class="btn btn-danger btn-sm"
											onclick="delete1('${taxitypes.id}','${taxitypes.type}')">删除</button></td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="5">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<!-- 删除表格 -->
			<form id="deleteForm" action="deleteTaxiType" method="post">
				<input type="hidden" id="deleteId" name="id">
			</form>
		</div>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script>
	//正则：验证正整数或者非负浮点数
	function regxCarSize(value) {
		//正浮点数
		var regxFloat = /^[1-9]\d*\.\d*|0\.\d*[1-9]\d*|0?\.0+|0$/;
		//正整数
		var regxNum = /^[0-9]*[1-9][0-9]*$/;
		if (regxFloat.test(value) || regxNum.test(value)) {
			return true;
		} else {
			return false;
		}
	}
	
		//加载footable的功能
	$(document).ready(function() {
		$('#table1').DataTable({
			"pagingType" : "full_numbers"
		});
	});

		//点击"修改"按钮
		function update(id, type, remark,lengthOfCar,widthOfCar,heightOfCar) {
			$("#id2").val(id);
			$("#type2").val(type);
			$("#remark2").val(remark);
			//修改模态框填充长宽高
			$("#taxi_update_lengthOfCar").val(lengthOfCar);
			$("#taxi_update_widthOfCar").val(widthOfCar);
			$("#taxi_update_heightOfCar").val(heightOfCar);
			 
		};
		
		//添加车辆类型验证
		function submitForm1(){
			var type=$("#type1").val();
			var lengthOfCar=$("#taxi_add_lengthOfCar").val();
			var widthOfCar=$("#taxi_add_widthOfCar").val();
			var heightOfCar=$("#taxi_add_heightOfCar").val();
			//var regx = /^[1-9]\d*\.\d*|0\.\d*[1-9]\d*|0?\.0+|0$ | ^[0-9]*[1-9][0-9]*$ /;
			if(type==""||type==" "){
				swal({
					title :"车辆类型不能为空",
					type : "warning"
				});
			}else if (!regxCarSize(lengthOfCar)||!regxCarSize(widthOfCar)||!regxCarSize(heightOfCar)) {
				swal({
					title :"车辆长宽高任一字段必须合法！",
					type : "warning"
				});
			}else{
				$("#form1").submit();
			}
		}
		
		//修改操作验证
		function submitForm2(){
			var type=$("#type2").val();
			var lengthOfCar=$("#taxi_update_lengthOfCar").val();
			var widthOfCar=$("#taxi_update_widthOfCar").val();
			var heightOfCar=$("#taxi_update_heightOfCar").val();
			//var regx = /(^[1-9]\d*\.\d*|0\.\d*[1-9]\d*|0?\.0+|0$/ ;
			if(type==""||type==" "){
				swal({
					title :"车辆类型不能为空",
					type : "warning"
				});
			}else if (!regxCarSize(lengthOfCar)||!regxCarSize(widthOfCar)||!regxCarSize(heightOfCar)) {
				swal({
					title :"车辆长宽高任一字段必须合法！",
					type : "warning"
				});
			}else{
				$("#form2").submit();
			}
		}

		//点击"删除"按钮
		function delete1(id, name) {
			swal({
				title : "您确定删除 " + name + " 的数据吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
				$("#deleteId").val(id);
				//提交表单
				$("#deleteForm").submit();
			})
		}
		
		//页面加载完成后执行,获取执行状态
		$(document).ready(function(){
			var result='<%=request.getAttribute("result")%>';
			if (result != "null") {
				if(result.indexOf("成功")>-1){
					swal({
						title : result,
						type : "success"
					});
				}else{
					swal({
						title : result,
						type : "error"
					});
				}	
			}
		});
	</script>
</body>

</html>