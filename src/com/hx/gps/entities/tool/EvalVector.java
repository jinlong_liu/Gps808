package com.hx.gps.entities.tool;

public class EvalVector {
	private int driId;//司机id
	private String taxiNum;
	private String isuNum;
 	private String dname;//司机姓名
 	private String sex;//性别
 	private	String queNum;//司机资格证号
 	private	String driNum;//驾驶证号
 	private String company;//所属公司
 	private String phone;//电话
 	private String currAssessName;//当期审核名称
 	private int currScore;//当期分数
 	private int currInitScore;//当期初始分数
 	private String currRank;//当期等级
 	private int lastScore;
 	private int lastInitScore;
 	private String lastRank;
 	private int yearScore;
 	private int yearInitScore;
 	private String yearRank;
 	private int	goodRate;//好评率
 	private int assScore;//评价得分
 	private int complaintScore;//投诉得分
 	
 	
	public int getDriId() {
		return driId;
	}
	public void setDriId(int driId) {
		this.driId = driId;
	}
	public String getTaxiNum() {
		return taxiNum;
	}
	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}
	public String getIsuNum() {
		return isuNum;
	}
	public void setIsuNum(String isuNum) {
		this.isuNum = isuNum;
	}
	public String getDname() {
		return dname;
	}
	public void setDname(String dname) {
		this.dname = dname;
	}
	
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getQueNum() {
		return queNum;
	}
	public void setQueNum(String queNum) {
		this.queNum = queNum;
	}
	public String getDriNum() {
		return driNum;
	}
	public void setDriNum(String driNum) {
		this.driNum = driNum;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCurrAssessName() {
		return currAssessName;
	}
	public void setCurrAssessName(String currAssessName) {
		this.currAssessName = currAssessName;
	}
	public int getCurrScore() {
		return currScore;
	}
	public void setCurrScore(int currScore) {
		this.currScore = currScore;
	}
	public int getCurrInitScore() {
		return currInitScore;
	}
	public void setCurrInitScore(int currInitScore) {
		this.currInitScore = currInitScore;
	}
	public String getCurrRank() {
		return currRank;
	}
	public void setCurrRank(String currRank) {
		this.currRank = currRank;
	}
	public int getLastScore() {
		return lastScore;
	}
	public void setLastScore(int lastScore) {
		this.lastScore = lastScore;
	}
	public int getLastInitScore() {
		return lastInitScore;
	}
	public void setLastInitScore(int lastInitScore) {
		this.lastInitScore = lastInitScore;
	}
	public String getLastRank() {
		return lastRank;
	}
	public void setLastRank(String lastRank) {
		this.lastRank = lastRank;
	}
	public int getYearScore() {
		return yearScore;
	}
	public void setYearScore(int yearScore) {
		this.yearScore = yearScore;
	}
	public int getYearInitScore() {
		return yearInitScore;
	}
	public void setYearInitScore(int yearInitScore) {
		this.yearInitScore = yearInitScore;
	}
	public String getYearRank() {
		return yearRank;
	}
	public void setYearRank(String yearRank) {
		this.yearRank = yearRank;
	}
	
	public int getGoodRate() {
		return goodRate;
	}
	public void setGoodRate(int goodRate) {
		this.goodRate = goodRate;
	}
	public int getAssScore() {
		return assScore;
	}
	public void setAssScore(int assScore) {
		this.assScore = assScore;
	}
	public int getComplaintScore() {
		return complaintScore;
	}
	public void setComplaintScore(int complaintScore) {
		this.complaintScore = complaintScore;
	}
	@Override
	public String toString() {
		return "EvalVector [driId=" + driId + ", taxiNum=" + taxiNum
				+ ", isuNum=" + isuNum + ", dname=" + dname + ", sex=" + sex
				+ ", queNum=" + queNum + ", driNum=" + driNum + ", company="
				+ company + ", phone=" + phone + ", currAssessName="
				+ currAssessName + ", currScore=" + currScore
				+ ", currInitScore=" + currInitScore + ", currRank=" + currRank
				+ ", lastScore=" + lastScore + ", lastInitScore="
				+ lastInitScore + ", lastRank=" + lastRank + ", yearScore="
				+ yearScore + ", yearInitScore=" + yearInitScore
				+ ", yearRank=" + yearRank + ", goodRate=" + goodRate
				+ ", assScore=" + assScore + ", complaintScore="
				+ complaintScore + "]";
	}
	
	
}
