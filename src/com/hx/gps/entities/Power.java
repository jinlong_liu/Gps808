package com.hx.gps.entities;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * Power entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "power" )
public class Power implements java.io.Serializable {

	// Fields

	private Integer id;
	private String name;
	private String res;

	// 20170318 fmy 添加角色属性,由role来维护关联关系

	private Set<Role> roles;
	
	
	// Constructors

	/** default constructor */
	public Power() {
	}

	/** full constructor */
	public Power(String name, String res) {
		this.name = name;
		this.res = res;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "res")
	public String getRes() {
		return this.res;
	}

	public void setRes(String res) {
		this.res = res;
	}


	@ManyToMany(mappedBy = "powers")
	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Power [id=");
		builder.append(id);
		builder.append(", name=");
		builder.append(name);
		builder.append(", res=");
		builder.append(res);
		builder.append("]");
		return builder.toString();
	}
	
	

}