﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!--设置浏览器可以等比例缩放 -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--设置主地址栏颜色  -->
    <meta name="theme-color" content="#000000">
    <meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
    <meta name="description"
          content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

    <link href="css/plugins/footable/footable.core.css" rel="stylesheet">
    <link rel="shortcut icon" href="favicon.ico">
    <link href="css/style.min.css?v=4.0.0" rel="stylesheet">
    <link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="css/plugins/jsTree/style.min.css" rel="stylesheet">
    <!--Live App 相关样式  -->
    <!-- 添加视频窗口样式 -->
    <!-- <link rel="stylesheet" href="plyr/dist/plyr.css"> -->
    <!-- 视频播放器样式 -->
    <link href="css/video.css" rel="stylesheet">
    <base target="_blank">
    <style>
        .jstree-open > .jstree-anchor > .fa-folder:before {
            content: "\f07c"
        }

        .jstree-default .jstree-icon.none {
            width: 0
        }

        .red {
            color: #fda6a6;
        }

        .green {
            color: #2b6901;
            font-weight: 900;
        }

        .black {
            color: #000000;
        }

        /* 解决不能全屏视频的问题 */
        .vedio1 video {
            position: absolute;
            height: 100%;
            width: 100%;
        }
        .left {
            float: left;
            width: 100%;
            height: 300px;
            margin-right: -300px;
        }
        .right {
            float: right;
            width: 300px;
            height: 300px;
        }
        .col-sm-6 {
            margin-left: -15px;
            margin-right: -15px;
            padding-left: 0px;
            padding-right: 0px;
        }
        .ibox-content{
            padding-left: 0px;
            padding-right: 0px;
        }
        #allmap label {
            max-width: none;
        }
    </style>
    <!-- Data Tables -->
    <link href="css/plugins/dataTables/dataTables.bootstrap.css"
          rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

</head>

<body class="gray-bg" style="zoom: 0.9;overflow-y: hidden;margin-left: 8px">
<div class="wrapper animated fadeInRight">
    <div class="row" style="background-color: #fcfbfb">
        <!--左侧车辆列表 -->
        <div class="col-sm-2" id="carList">
            <!-- <div class="ibox-title"> -->
            <input type="hidden" id="timer">
            <div class="float-e-margins">
                <div class="ibox-content" style="height: 635px">
                    <label class="control-label" style="display: none"><h4>车辆列表</h4></label>&nbsp;&nbsp;&nbsp;
                    <center><input type="text" class="form-control form-control-sm"
                                   id="txtIndustryArea" style="margin-top: -10px;width: 250px;"> <br> <input type="hidden" name="show1" id="show1"></center>
                    <!-- 用来显示选中的车辆，即节点 -->
                    <div id="jstree1" class="trdemo"
                         style="overflow-x: hidden; overflow-y: auto; height: 735px;">
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-top: 10px">
            <button class="btn" id="video-mode"><svg t="1606387915350" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2132" width="16" height="16"><path d="M512 1024a512 512 0 1 1 512-512 512 512 0 0 1-512 512z m0-965.485714a453.485714 453.485714 0 1 0 453.485714 453.485714A453.485714 453.485714 0 0 0 512 58.514286z" fill="" p-id="2133"></path><path d="M424.228571 737.28a73.142857 73.142857 0 0 1-36.571428-9.947429 72.118857 72.118857 0 0 1-36.571429-63.341714V360.009143a73.142857 73.142857 0 0 1 109.714286-63.341714l263.314286 151.990857a73.142857 73.142857 0 0 1 0 126.683428l-263.314286 151.990857a73.142857 73.142857 0 0 1-36.571429 9.947429z m0-392.045714a14.628571 14.628571 0 0 0-7.314285 2.048 14.628571 14.628571 0 0 0-7.314286 12.726857v303.981714a14.628571 14.628571 0 0 0 7.314286 12.726857 14.628571 14.628571 0 0 0 14.628571 0l263.314286-151.990857a14.628571 14.628571 0 0 0 0-25.453714l-263.314286-151.990857a14.628571 14.628571 0 0 0-7.314286-2.048z" fill="" p-id="2134"></path></svg>  视频模式</button>
            <button class="btn" id="map-mode"><svg t="1606387966398" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3101" width="16" height="16"><path d="M537.972364 826.228364L512 855.179636l-25.972364-28.951272a2274.606545 2274.606545 0 0 1-44.520727-52.084364 2336.116364 2336.116364 0 0 1-97.489454-125.742545 1516.218182 1516.218182 0 0 1-68.072728-102.516364C225.908364 462.731636 197.818182 392.913455 197.818182 337.454545 197.818182 163.933091 338.478545 23.272727 512 23.272727s314.181818 140.660364 314.181818 314.181818c0 55.458909-28.090182 125.277091-78.126545 208.430546a1516.218182 1516.218182 0 0 1-68.072728 102.516364 2336.116364 2336.116364 0 0 1-97.466181 125.742545 2274.606545 2274.606545 0 0 1-44.544 52.084364z m-9.216-96.628364a2267.461818 2267.461818 0 0 0 94.533818-121.949091 1447.726545 1447.726545 0 0 0 64.930909-97.745454c43.985455-73.076364 68.142545-133.166545 68.142545-172.45091C756.363636 202.496 646.958545 93.090909 512 93.090909S267.636364 202.496 267.636364 337.454545c0 39.284364 24.180364 99.374545 68.142545 172.45091a1447.726545 1447.726545 0 0 0 64.930909 97.745454A2267.461818 2267.461818 0 0 0 512 749.591273c5.352727-6.283636 10.938182-12.986182 16.756364-20.014546zM888.226909 605.090909H861.090909a34.909091 34.909091 0 0 1 0-69.818182h58.181818c17.687273 0 32.581818 13.265455 34.676364 30.836364l46.545454 395.636364A34.909091 34.909091 0 0 1 965.818182 1000.727273h-907.636364a34.909091 34.909091 0 0 1-34.676363-38.981818l46.545454-395.636364A34.909091 34.909091 0 0 1 104.727273 535.272727H162.909091a34.909091 34.909091 0 0 1 0 69.818182H135.773091L97.442909 930.909091h829.114182l-38.330182-325.818182zM512 453.818182a128 128 0 1 1 0-256 128 128 0 0 1 0 256z m0-69.818182a58.181818 58.181818 0 1 0 0-116.363636 58.181818 58.181818 0 0 0 0 116.363636z" p-id="3102"></path></svg>  地图模式</button>
        </div>
        <!-- 中间地图 -->
        <div class="col-sm-6" id="middleMap" style="padding-right: 0px;top: 0">
            <div id="videoDiv" style="top: 0;">
                <div class="ibox-content" style="max-height: 100%;width: 100%;margin-left: 15px;padding-top: 0px;">
                    <iframe id="playShow" name="playShow" src="test.jsp" allowfullscreen="true"
                            allowtransparency="true" width="100%" height="602px"
                            frameborder="no" border="0" marginwidth="0" marginheight="0"
                            scrolling="no"></iframe>
                </div>
            </div>
        </div>
        <div id="allmap" class="col-sm-4">
            <div id="map" style="height: 602px"></div>
        </div>

        <form action="getTreeOfTemp3" method="get" target="playShow"
              id="vedioForm">
            <input type="hidden" id="vedioNums" name="taxiNum">
        </form>
    </div>
    <!--右键菜单的源-->
    <shiro:lacksRole name="guest">
        <div class="contextMenu" id="myMenu1">
            <ul>
                <!-- <li id="selSetIsu">查询设置终端参数</li>
            <li id="setEvent">设置事件</li>
            <li id="setInfo">设置信息菜单</li> -->
                <li id="showVideo"><svg t="1605969736713" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="11682" width="16" height="16"><path d="M891.13 896H133.71c-38.18 0-69.13-30.95-69.13-69.13V389.13c0-38.18 30.95-69.13 69.13-69.13h757.42c38.18 0 69.13 30.95 69.13 69.13v437.73c0.01 38.18-30.95 69.14-69.13 69.14z" fill="#d4237a" p-id="11683" data-spm-anchor-id="a313x.7781069.0.i47" class="selected"></path><path d="M374.56 374.27c-12.69 12.69-33.27 12.69-45.96 0L194.24 239.92c-12.69-12.69-12.69-33.27 0-45.96 12.69-12.69 33.27-12.69 45.96 0l134.35 134.35c12.7 12.69 12.7 33.27 0.01 45.96zM649.64 374.27c-12.69-12.69-12.69-33.27 0-45.96l134.35-134.35c12.69-12.69 33.27-12.69 45.96 0 12.69 12.69 12.69 33.27 0 45.96L695.61 374.27c-12.7 12.69-33.27 12.69-45.97 0z" fill="#333333" p-id="11684"></path><path d="M622.58 608l-110.16 63.91-110.16 63.92V480.16l110.16 63.92z" fill="#FFFFFF" p-id="11685"></path></svg>实时视频</li>
                <li id="reTrack"><svg t="1605969826259" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="12255" width="16" height="16"><path d="M863.53 159.9m-96.04 0a96.04 96.04 0 1 0 192.08 0 96.04 96.04 0 1 0-192.08 0Z" fill="#333333" p-id="12256"></path><path d="M863.17 288.05c-70.6 0-127.83-57.23-127.83-127.83 0-38.21 16.77-72.49 43.34-95.92-3.44-0.18-6.91-0.28-10.4-0.28H256.19C150.13 64.02 64.16 150 64.16 256.05v511.94c0 106.06 85.98 192.03 192.03 192.03h512.08c106.06 0 192.03-85.98 192.03-192.03V256.05c0-4.12-0.15-8.21-0.4-12.26-23.44 27.1-58.08 44.26-96.73 44.26z" fill="#d4237a" p-id="12257" data-spm-anchor-id="a313x.7781069.0.i53" class="selected"></path><path d="M136.62 758.42c-12.51-12.51-12.51-32.79 0-45.3l182.1-182.1c12.51-12.51 32.79-12.51 45.3 0 12.51 12.51 12.51 32.79 0 45.3l-182.1 182.1c-12.51 12.51-32.8 12.51-45.3 0zM499.57 758.34c-12.51-12.51-12.51-32.79 0-45.3l341.86-341.86c12.51-12.51 32.79-12.51 45.3 0 12.51 12.51 12.51 32.79 0 45.3L544.87 758.34c-12.51 12.51-32.79 12.51-45.3 0z" fill="#FFFFFF" p-id="12258"></path><path d="M546.08 758.34c-12.51 12.51-32.79 12.51-45.3 0l-182.1-182.1c-12.51-12.51-12.51-32.79 0-45.3 12.51-12.51 32.79-12.51 45.3 0l182.1 182.1c12.51 12.51 12.51 32.79 0 45.3z" fill="#FFFFFF" p-id="12259"></path></svg>轨迹回放</li>
                <li id="sendText"><svg t="1605969349389" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3800" width="16" height="16"><path d="M664.5 927.9m-26.3 0a26.3 26.3 0 1 0 52.6 0 26.3 26.3 0 1 0-52.6 0Z" fill="#FFCD43" p-id="3801"></path><path d="M574.6 954.1c-0.1 0-0.1 0 0 0l-420.4-0.1c-23.8 0-46.2-9.3-63.1-26.1C74.3 911 65 888.6 65 864.8l0.2-669.2c0-49.2 40-89.2 89.2-89.2l399.2 0.1c14.5 0 26.3 11.8 26.3 26.3s-11.8 26.3-26.3 26.3l-399.2-0.1c-20.1 0-36.5 16.4-36.5 36.5l-0.2 669.2c0 9.8 3.8 18.9 10.7 25.8 6.9 6.9 16.1 10.7 25.8 10.7l420.4 0.1c14.5 0 26.3 11.8 26.3 26.3 0 14.7-11.8 26.5-26.3 26.5zM823.3 954.2h-66.4c-14.5 0-26.3-11.8-26.3-26.3s11.8-26.3 26.3-26.3h66.4c9.7 0 18.9-3.8 25.8-10.7 6.9-6.9 10.7-16.1 10.7-25.8l0.1-380.7c0-14.5 11.8-26.3 26.3-26.3s26.3 11.8 26.3 26.3V865c0 23.8-9.3 46.2-26.1 63.1-16.9 16.8-39.2 26.1-63.1 26.1z" p-id="3802"></path><path d="M492.7 686.9L351.6 696c-10.6 0.7-20.3-9-19.6-19.7l9.7-140.6 429.6-429.3c15.2-15.1 41.4-13.5 58.6 3.7l88.7 88.8c17.2 17.2 18.9 43.4 3.7 58.6L492.7 686.9z" fill="#FFCD43" p-id="3803"></path><path d="M350.5 722.4c-11.7 0-23.1-4.8-31.7-13.4-9.2-9.3-14-21.8-13.1-34.5l9.7-140.5c0.4-6.3 3.2-12.3 7.7-16.8L752.7 87.8c12.6-12.6 30.4-19.1 48.8-17.9 17.5 1.1 34.2 8.8 47 21.7l88.7 88.8c12.8 12.9 20.5 29.6 21.6 47 1.2 18.5-5.4 36.2-18 48.8L511.3 705.5c-4.5 4.5-10.5 7.2-16.9 7.7l-141.2 9.1c-0.9 0-1.8 0.1-2.7 0.1z m142.2-35.5zM367.3 547.4l-8.4 121.7 122.3-7.9 422.6-422.3c2.4-2.4 2.8-5.8 2.6-8.2-0.3-4.6-2.6-9.4-6.3-13.1l-88.7-88.8c-3.7-3.7-8.5-6-13.1-6.3-2.4-0.2-5.8 0.2-8.2 2.6L367.3 547.4z" p-id="3804"></path><path d="M354.562 533.215l37.275-37.254 133.039 133.114L487.6 666.33zM708.678 191.76l37.275-37.254L878.99 287.62l-37.274 37.253z" p-id="3805"></path></svg>下发文本</li>
                <!-- <li id="infoSer">信息服务</li>
             <li id="sendQuiz">下发提问</li> -->
                <li id="phoneCall"><svg t="1605969422702" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4051" width="16" height="16"><path d="M511.8 956.8c-58 0-114.5-11-168-32.7-55.4-22.5-105.1-55.5-147.7-98.1S120.5 733.7 98 678.3c-21.7-53.5-32.7-110-32.7-168s11-114.5 32.7-168c22.5-55.4 55.5-105.1 98.1-147.7 51.5-51.5 115.2-89.9 184.4-111.1 66.9-20.5 138.8-25 208-13 14.3 2.5 23.8 16.1 21.4 30.3-2.5 14.3-16 23.8-30.3 21.4-61-10.6-124.5-6.6-183.6 11.5-61 18.7-117.3 52.6-162.7 98.1-37.6 37.6-66.7 81.5-86.5 130.3-19.1 47.2-28.8 97.1-28.8 148.3s9.7 101.1 28.8 148.3c19.8 48.9 48.9 92.7 86.5 130.3s81.5 66.7 130.3 86.5c47.2 19.1 97.1 28.8 148.3 28.8s101.1-9.7 148.3-28.8c48.9-19.8 92.7-48.9 130.3-86.5s66.7-81.5 86.5-130.3c19.1-47.2 28.8-97.1 28.8-148.3s-9.7-101.1-28.8-148.3c-19.8-48.9-48.9-92.7-86.5-130.3-15.3-15.3-32-29.5-49.5-42-11.8-8.4-14.5-24.8-6-36.6 8.4-11.8 24.8-14.5 36.6-6 19.8 14.2 38.7 30.2 56 47.6C870 237.3 903 287 925.5 342.4c21.7 53.5 32.7 110 32.7 168s-11 114.5-32.7 168C903 733.7 870 783.4 827.4 826s-92.3 75.6-147.7 98.1c-53.5 21.7-110 32.7-167.9 32.7z" p-id="4052"></path><path d="M672.7 127m-26.2 0a26.2 26.2 0 1 0 52.4 0 26.2 26.2 0 1 0-52.4 0Z" p-id="4053"></path><path d="M645.6 735.2c11 0 18.3-9 18.3-19.5v-32.5c0-23.8-35.3-42.2-116-91.1-18.8-11.5-63.7-32.3-36.6-78.1 24-38 42.8-53.5 42.7-117.2 0.1-60.4-41.8-123.7-103.8-123.7-68 0-109.9 63.3-109.9 123.7 0 63.6 18.7 79.1 42.7 117.2 27.1 45.8-17.9 66.6-36.6 78.1-80.7 49-116 67.3-116 91.1v32.5c0 10.5 7.3 19.5 18.3 19.5h396.9zM762.6 440.4h-130c-14.5 0-26.2-11.7-26.2-26.2s11.7-26.2 26.2-26.2h130.1c14.5 0 26.2 11.7 26.2 26.2s-11.8 26.2-26.3 26.2z" fill="#FF5969" p-id="4054"></path><path d="M762.6 527.9H608.9c-14.5 0-26.2-11.7-26.2-26.2s11.7-26.2 26.2-26.2h153.7c14.5 0 26.2 11.7 26.2 26.2s-11.7 26.2-26.2 26.2zM762.6 617.7h-85.1c-14.5 0-26.2-11.7-26.2-26.2s11.7-26.2 26.2-26.2h85.1c14.5 0 26.2 11.7 26.2 26.2s-11.7 26.2-26.2 26.2z" fill="#FF5969" p-id="4055"></path></svg>电话回拨</li>
                <!-- <li id="sendPhoneText">下发电话本</li>
            <li id="addressTrack">临时位置追踪</li>
            <li id="carCall">车辆点名</li>-->
                <li id="setPrice"><svg t="1605969554173" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4777" width="16" height="16"><path d="M859.9 293.4m-26.2 0a26.2 26.2 0 1 0 52.4 0 26.2 26.2 0 1 0-52.4 0Z" fill="#46A8FF" p-id="4778"></path><path d="M163.4 638.8v266.9c0.3 20.5 22.7 33.2 40.8 23.7l138.3-71.7c13-6.7 28.5-6.5 41.2 0.7L494 920.5c12.6 7.1 28 7.5 40.9 0.9l126.3-64c13.1-6.6 28.7-6.2 41.4 1.2l117.9 68.8c17.3 10.1 39.8-1.8 40.1-21.8V638.8H163.4z" fill="#46A8FF" p-id="4779"></path><path d="M688.3 347.6h-350c-14.5 0-26.2-11.7-26.2-26.2s11.7-26.2 26.2-26.2h349.9c14.5 0 26.2 11.7 26.2 26.2 0.1 14.5-11.7 26.2-26.1 26.2zM688.3 506.3h-350c-14.5 0-26.2-11.7-26.2-26.2s11.7-26.2 26.2-26.2h349.9c14.5 0 26.2 11.7 26.2 26.2s-11.7 26.2-26.1 26.2zM688.3 665h-350c-14.5 0-26.2-11.7-26.2-26.2s11.7-26.2 26.2-26.2h349.9c14.5 0 26.2 11.7 26.2 26.2 0.1 14.4-11.7 26.2-26.1 26.2z" p-id="4780"></path><path d="M833 959c-8.9 0-17.7-2.3-25.7-7l-118.2-70.9c-4.8-2.8-10.9-3-16.1-0.3l-126.3 64c-20.8 10.5-45.3 10-65.6-1.5l-110.2-62.1c-5.1-2.9-11.2-3-16.3-0.3l-138.3 71.7c-16.9 8.8-36.8 8.1-53.1-1.7-16-9.6-25.8-26.4-26-45V126.2c0-33.7 27.5-61.1 61.1-61.2h627.3c33.7 0 61.2 27.5 61.2 61.2v71.6c0 14.5-11.7 26.2-26.2 26.2s-26.2-11.7-26.2-26.2v-71.6c0-4.8-4-8.8-8.8-8.8H198.4c-4.8 0-8.8 4-8.8 8.9v779.4c0.1 0 0.4 0.4 1 0.6 0.8 0.3 1.3 0 1.6-0.1l138.3-71.7c20.9-10.8 45.7-10.4 66.2 1.2l110.2 62.1c5 2.8 11 2.9 16.2 0.4l126.3-64c21.2-10.7 46-10 66.5 2l117.4 70.4c0.5-0.2 1.1-0.8 1.2-1.2V383.5c0-14.5 11.7-26.2 26.2-26.2s26.2 11.7 26.2 26.2v522.7c-0.3 18.9-10.9 36.5-27.7 46-8.3 4.5-17.3 6.8-26.2 6.8z" p-id="4781"></path></svg>远程调价</li>
                <li id="setPower"><svg t="1605969533400" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4533" width="16" height="16"><path d="M512 691.2c98 1 179.2-78.2 180.2-176.3 1-101-78.2-182.2-179.2-182.2-98-1-180.2 79.2-181.2 178.2 0 99.1 80.2 180.3 180.2 180.3z" fill="#46A8FF" p-id="4534"></path><path d="M513.8 717.5h-1.9c-55.3 0-107.1-21.6-146.1-60.7-38.9-39-60.3-90.8-60.3-145.8 0.5-55.1 22.6-106.6 62.1-145.4 39.4-38.7 91.3-59.5 145.7-59.2 55.3 0 107.1 21.5 145.7 60.4 39 39.3 60.1 92 59.6 148.3-1.2 112.1-92.8 202.4-204.8 202.4z m-1.8-52.6h1.8c83.2 0 151.2-67.1 152.1-150.2 0.4-42.1-15.3-81.5-44.3-110.8C593 375 554.4 359.1 513 359.1h-1.6c-40.1 0-78 15.6-107 44.1-29.5 28.9-45.9 67.3-46.3 108.1 0 40.7 16 79.3 44.9 108.4 29.1 29.2 67.8 45.2 109 45.2z" p-id="4535"></path><path d="M215.2 214.4m-26.3 0a26.3 26.3 0 1 0 52.6 0 26.3 26.3 0 1 0-52.6 0Z" fill="#46A8FF" p-id="4536"></path><path d="M511.9 959c-38.5 0-73-19.9-92.3-53.1L399 870.4c-12.2-21-37.1-31.3-60.6-25.1l-39.7 10.5c-37.2 9.8-75.6-0.5-102.8-27.7-27.2-27.2-37.5-65.6-27.7-102.8l10.5-39.7c6.2-23.5-4.1-48.4-25.1-60.7l-35.5-20.7C84.8 584.8 65 550.3 65 511.9s19.9-72.9 53.1-92.3l35.5-20.6c21-12.2 31.3-37.1 25.1-60.6l-5.9-22.1c-3.7-14 4.7-28.4 18.7-32.2 14.1-3.7 28.4 4.7 32.2 18.7l5.9 22.1c12.2 46.3-8.1 95.5-49.6 119.6l-35.5 20.6c-16.9 9.8-26.9 27.3-26.9 46.8 0 19.5 10.1 37 26.9 46.8l35.5 20.7c41.4 24.1 61.8 73.3 49.5 119.6L219 738.7c-5 18.8 0.3 38.3 14 52.1 13.8 13.8 33.3 19.1 52.1 14.1l39.7-10.5c46.3-12.2 95.5 8.1 119.6 49.6l20.6 35.5c9.8 16.9 27.3 26.9 46.8 26.9 19.5 0 37-10.1 46.8-26.9l20.7-35.5c24.1-41.4 73.3-61.8 119.6-49.5l39.7 10.5c18.9 5 38.3-0.3 52.1-14 13.8-13.8 19-33.3 14.1-52.1l-10.5-39.7c-12.2-46.3 8.1-95.5 49.6-119.6l35.5-20.6c16.9-9.8 26.9-27.3 26.9-46.8 0-19.5-10.1-37-26.9-46.8L844 444.7c-41.4-24.1-61.8-73.3-49.5-119.6l10.5-39.7c5-18.8-0.3-38.3-14-52.1-13.8-13.8-33.3-19.1-52.1-14.1l-39.7 10.5c-46.3 12.3-95.5-8.1-119.6-49.6L559 144.6c-9.8-16.9-27.3-26.9-46.8-26.9-19.5 0-37 10.1-46.8 26.9L444.7 180c-24.1 41.4-73.3 61.8-119.6 49.5l-20.4-5.4c-14-3.7-22.4-18.1-18.7-32.2 3.7-14 18.1-22.4 32.2-18.7l20.4 5.4c23.5 6.2 48.4-4.1 60.6-25.1l20.7-35.5c19.3-33.2 53.8-53 92.2-53 38.4 0 72.9 19.9 92.3 53.1l20.6 35.5c12.2 21 37.1 31.4 60.6 25.1l39.7-10.5c37.2-9.8 75.6 0.5 102.8 27.7 27.2 27.2 37.5 65.6 27.7 102.8l-10.5 39.7c-6.2 23.5 4.1 48.4 25.1 60.6l35.5 20.7C939.1 439 959 473.5 959 512s-19.9 72.9-53.1 92.3L870.4 625c-21 12.2-31.3 37.1-25.1 60.6l10.5 39.7c9.8 37.2-0.5 75.6-27.7 102.8-27.2 27.2-65.6 37.5-102.8 27.7l-39.7-10.5c-23.5-6.2-48.4 4.1-60.6 25.1l-20.7 35.5c-19.5 33.3-54 53.1-92.4 53.1z" p-id="4537"></path></svg>远程开关机</li>
                <li id="setArea"><svg t="1605969572637" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="5015" width="16" height="16"><path d="M836.4 959H187.7c-63.1 0-114.5-51.4-114.5-114.5V748c0-18.4 14.9-33.4 33.4-33.4s33.4 15 33.4 33.4v96.5c0 26.3 21.4 47.7 47.7 47.7h648.7c26.3 0 47.7-21.4 47.7-47.7V289.3c0-26.3-21.4-47.7-47.7-47.7h-105c-18.4 0-33.4-15-33.4-33.4s15-33.4 33.4-33.4h105c63.1 0 114.5 51.4 114.5 114.5v555.2c-0.1 63.1-51.4 114.5-114.5 114.5zM106.6 552.4c-18.4 0-33.4-15-33.4-33.4V289.3c0-63.1 51.4-114.5 114.5-114.5h101.2c18.4 0 33.4 14.9 33.4 33.4s-14.9 33.4-33.4 33.4H187.7c-26.3 0-47.7 21.4-47.7 47.7V519c0 18.4-15 33.4-33.4 33.4z" p-id="5016" fill="#d81e06"></path><path d="M506.5 770.2L644.6 531c5.3-9.2-1.3-20.8-12-20.8H356.4c-10.7 0-17.3 11.5-12 20.8l138.1 239.2c5.3 9.3 18.7 9.3 24 0z" fill="#d81e06" p-id="5017"></path><path d="M494.5 810.5c-16.8 0-32.5-9-40.9-23.6L315.5 547.8c-8.4-14.6-8.4-32.7 0-47.2 8.4-14.6 24.1-23.6 40.9-23.6h276.2c16.8 0 32.5 9 40.9 23.6 8.4 14.6 8.4 32.7 0 47.2L535.4 786.9c-8.4 14.6-24.1 23.6-40.9 23.6z m-16.9-57z m-87.3-209.8l104.2 180.5 104.2-180.5H390.3z" p-id="5018" fill="#d81e06"></path><path d="M494.5 538.4c-18.4 0-33.4-15-33.4-33.4V98.4c0-18.4 15-33.4 33.4-33.4s33.4 14.9 33.4 33.4V505c0 18.4-15 33.4-33.4 33.4z" p-id="5019" fill="#d81e06"></path><path d="M106.5 630.1m-33.4 0a33.4 33.4 0 1 0 66.8 0 33.4 33.4 0 1 0-66.8 0Z" p-id="5020" fill="#d81e06"></path></svg>区域下发</li>
                <li id="deleteArea"><svg t="1605969498069" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4289" width="16" height="16"><path d="M220.4 445.2l381.5 381.5-30 30c-69.3 69.3-181.6 69.3-250.9 0l-256-256 155.4-155.5z" fill="#49E8CE" p-id="4290"></path><path d="M446.5 935.8c-51.5 0-103-19.6-142.2-58.8L100.1 672.8c-19.3-19.3-29.9-44.9-29.9-72.2 0-27.3 10.6-52.9 29.9-72.2L510.6 118c39.8-39.8 104.5-39.8 144.3 0l274.3 274.3c39.8 39.8 39.8 104.5 0 144.3L588.7 877c-39.2 39.2-90.7 58.8-142.2 58.8z m136.2-795.2c-12.7 0-25.4 4.8-35.1 14.5L137.2 565.6c-9.4 9.4-14.5 21.8-14.5 35.1 0 13.2 5.2 25.7 14.5 35.1L341.4 840c58 58 152.3 58 210.3 0l340.5-340.5c9.4-9.4 14.5-21.8 14.5-35.1 0-13.2-5.2-25.7-14.5-35.1L617.8 155.1c-9.7-9.7-22.4-14.5-35.1-14.5z" p-id="4291"></path><path d="M599.8 855.1c-6.7 0-13.4-2.6-18.5-7.7l-271-271c-10.2-10.2-10.2-26.8 0-37.1 10.2-10.2 26.8-10.2 37.1 0l270.9 271.1c10.2 10.2 10.2 26.8 0 37.1-5.1 5-11.8 7.6-18.5 7.6z" p-id="4292"></path><path d="M257.400833 518.632843a22.8 22.8 0 1 0 32.243507-32.244632 22.8 22.8 0 1 0-32.243507 32.244632Z" p-id="4293"></path></svg>删除区域</li>
                <li id="takePic"><svg t="1605969249949" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3436" width="16" height="16"><path d="M91.5 424.3h841.3v247.9H91.5z" fill="#49E8CE" p-id="3437"></path><path d="M857.6 844.1H166.4C110.5 844.1 65 798.6 65 742.7V372c0-55.9 45.5-101.4 101.4-101.4h51.9c33.4 0 57.9-16.4 74.8-50.1 6.3-15.4 24.8-39.5 57.8-39.5h506.7c55.9 0 101.4 45.5 101.4 101.4v460.2c0 56-45.5 101.5-101.4 101.5z m-691.2-521c-26.9 0-48.8 21.9-48.8 48.8v370.7c0 26.9 21.9 48.8 48.8 48.8h691.2c26.9 0 48.8-21.9 48.8-48.8V282.5c0-26.9-21.9-48.8-48.8-48.8H350.9c-5.4 0-8.2 4.9-9.1 6.9l-0.5 1.7-0.5 0.6c-25.9 52.6-68.3 80.3-122.4 80.3h-52z" p-id="3438"></path><path d="M838 303.9m-35.9 0a35.9 35.9 0 1 0 71.8 0 35.9 35.9 0 1 0-71.8 0Z" p-id="3439"></path><path d="M578 318.9H464.6c-14.5 0-26.3-11.8-26.3-26.3s11.8-26.3 26.3-26.3H578c14.5 0 26.3 11.8 26.3 26.3s-11.8 26.3-26.3 26.3zM228.7 232.5H115.4c-14.5 0-26.3-11.8-26.3-26.3s11.8-26.3 26.3-26.3h113.3c14.5 0 26.3 11.8 26.3 26.3s-11.8 26.3-26.3 26.3z" p-id="3440"></path><path d="M521.3 548.2m-180.6 0a180.6 180.6 0 1 0 361.2 0 180.6 180.6 0 1 0-361.2 0Z" fill="#FFFFFF" p-id="3441"></path><path d="M521.3 755.1c-114.1 0-206.8-92.8-206.8-206.8s92.8-206.8 206.8-206.8 206.8 92.8 206.8 206.8-92.7 206.8-206.8 206.8z m0-361.1C436.2 394 367 463.2 367 548.2s69.2 154.3 154.3 154.3 154.3-69.2 154.3-154.3S606.4 394 521.3 394z" p-id="3442"></path><path d="M518.2 646.4c-54.4 0-98.7-44.3-98.7-98.7 0-54.4 44.3-98.7 98.7-98.7 39.8 0 75.5 23.7 90.9 60.3 5.7 13.4-0.6 28.8-14 34.5-13.4 5.6-28.8-0.6-34.5-14-7.2-17.1-23.9-28.2-42.5-28.2-25.4 0-46.1 20.7-46.1 46.1 0 25.4 20.7 46.1 46.1 46.1 14.5 0 26.3 11.8 26.3 26.3s-11.7 26.3-26.2 26.3z" p-id="3443"></path><path d="M575.9 584.4m-26.3 0a26.3 26.3 0 1 0 52.6 0 26.3 26.3 0 1 0-52.6 0Z" fill="#49E8CE" p-id="3444"></path></svg> 拍照</li>
                <li id="setRelay"><svg t="1605969643165" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="6698" width="16" height="16"><path d="M60.6 491.2c-3.2-3.9-1.6-9.8 3.1-11.5 12.3-4.5 31.5-11.4 45.3-16.4 10.8-3.9 22.7-3 32.9 2.5 20.1 10.8 54 29.2 70.8 38.8 6.7 3.8 14.7 4.5 22 1.8 32.4-11.8 123.8-45.1 145.4-53.1 4.1-1.5 5.7-6.4 3.3-10.1L185.9 138.9c-2.6-3.9-0.8-9.2 3.6-10.9 16.9-6.3 54.5-20.1 74.2-27.6 5.4-2.1 9-1.1 13.3 2.4 109.3 86.8 218.7 173.5 327.9 260.4 5.6 4.5 10 5 16.6 2.6 86.9-31.9 173.9-63.5 260.9-95.1 40.5-14.7 80.5 9.2 86.9 51.5 4.4 29-13.5 57.7-42.9 68.4-68.5 25-137 49.9-205.5 74.9L242.5 639.8c-34.4 12.6-60.7 3.2-82.7-26.3-16.6-22.5-78.7-97.6-99.2-122.3z m-6.6-8" fill="#FFCF41" p-id="6699"></path><path d="M246.7 785.2c-15.4 0-29.8-9.6-35.2-24.9-6.9-19.5 3.2-40.8 22.7-47.8l525.7-186.9c19.5-6.9 40.8 3.2 47.8 22.7 6.9 19.5-3.2 40.8-22.7 47.8L259.3 783c-4.2 1.5-8.4 2.2-12.6 2.2zM246.7 924.6c-15.2 0-29.6-9.4-35.1-24.5-7.1-19.4 2.8-40.9 22.2-48l268.7-98.5c19.4-7.1 40.9 2.8 48 22.2s-2.8 40.9-22.2 48l-268.7 98.5c-4.2 1.5-8.6 2.3-12.9 2.3z" fill="#232426" p-id="6700"></path></svg>断油断电</li>
                <li id="setControl"><svg t="1605969625144" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="6437" width="16" height="16"><path d="M277.1 410.9l-54.3 55c-5.8 5.9-13.7 9.2-21.9 9.2s-16.1-3.3-21.9-9.2l-95.3-96.5c-12.2-12.3-12.2-32.2 0-44.5l157.1-158.3c0.7-0.8 1.8-1.1 2.6-1.7 0.7-0.9 1-1.9 1.8-2.6C328.5 76.9 455.6 51 565.9 96.9h0.1l1.9 0.7c2.2 0.9 3.8 2.5 5.7 3.8 1.1 0.7 2.2 1.2 3.1 2 2.3 2.2 4.2 4.8 5.7 7.7 0.4 0.7 0.9 1.2 1.3 1.9 1.7 3.6 2.8 7.5 3 11.5v0.1c0.2 4.2-0.3 8.4-1.8 12.3h0.1c-0.2 0.5-0.1 1.1-0.4 1.7-1.3 2.9-3.1 5.6-5.2 7.9-0.2 0.2-0.3 0.6-0.6 0.9-2.3 2.6-5.1 4.8-8.2 6.4-0.5 0.2-0.9 0.6-1.3 0.9-3.2 1.5-6.6 2.4-10.1 2.7-0.5 0-1 0.3-1.6 0.3-38.2 2.5-74.6 17.4-103.6 42.4-4.2 3.7-8.6 7.2-12.6 11.2-17.2 17.4-31.7 37.3-43.1 58.9l-0.2 0.9c-2.9 5.8-3.1 12.6-0.5 18.6l514.3 508.1c5.9 5.9 9.2 13.8 9.2 22.1s-3.3 16.3-9.2 22.1l-76.8 77c-5.9 5.9-13.8 9.2-22.1 9.2h-0.1c-8.3 0-16.2-3.3-22.1-9.2L277.1 410.9z" fill="#FFCF41" p-id="6438"></path><path d="M291.9 949.5c-28 0-55.6-6.2-81-18-9.2-4.4-15.7-12.9-17.5-23-1.8-10 1.5-20.3 8.7-27.6l86.3-86.4-32.4-32.3-85.8 85.8c-7.3 7.3-17.7 10.6-27.8 8.8-10.2-1.9-18.7-8.6-22.9-18.1-32-71.4-16.8-155.2 38.2-210.8 50-49.2 123.1-67 190.1-46.1l234-233.9c-21-67-3.3-140.1 46.1-190 35.2-35.3 83-55.1 132.8-54.9 26.7 0 53.7 5.8 78.1 16.8 9.4 4.2 16.2 12.8 18 22.9 1.8 10.2-1.4 20.6-8.8 27.8l-87 87 32.4 32.4 87.6-87.6c7.2-7.2 17.5-10.5 27.5-8.7 10.1 1.7 18.6 8.3 23 17.5 33.9 71.8 19.2 157.2-36.7 213.6-35.1 35.3-82.9 55-132.6 54.9-20.3 0-40.7-3.4-60-9.8L469.6 702.1c22.2 67.1 5.4 142.1-45.1 192.6-35.1 35.2-82.9 55-132.6 54.8z" fill="#232426" p-id="6439"></path><path d="M286.3 885.3c35.3 2.1 69.8-11.1 94.7-36.1 37-37 46.8-94.1 24.2-142-5.6-11.9-3.1-26 6.2-35.3l260.5-260.5c9.3-9.3 23.3-11.8 35.3-6.3 47.8 22 104.2 12.4 142-24.2 25.3-25.3 37.8-60 36.1-94.7l-70.8 70.9c-5.9 5.9-13.8 9.2-22.1 9.2s-16.2-3.3-22.1-9.2L694 280.6c-12.2-12.2-12.2-32 0-44.2l69.3-69.3c-34.2-1-67.3 12.2-91.5 36.3-37 36.8-47 92.8-25.2 140.2 5.4 11.9 2.8 25.8-6.4 35L378.6 640c-9.2 9.1-23 11.6-34.9 6.4-47.5-21-103-11-140.2 25.2-24.3 24.2-37.5 57.4-36.5 91.7l68.3-68.3c5.8-5.9 13.8-9.2 22.1-9.2s16.2 3.3 22.1 9.2l76.5 76.5c5.9 5.8 9.2 13.8 9.2 22.1s-3.3 16.2-9.2 22.1l-69.7 69.6z" fill="#FFFFFF" p-id="6440"></path></svg>终端控制</li>
            </ul>
        </div>
    </shiro:lacksRole>

    <div class="row" id="templist">

        <div class="col-sm-12 leftmargin">
            <div class="float-e-margins">
                <div class="ibox-content" style="height: 300px; overflow: auto;">
                    <table id="table1" class="footable table table-stripped" data-page-size="10"data-filter=#filter style="cursor: pointer;">
                        <thead>
                        <tr>
                            <th>车牌号</th>
                            <th>速度</th>
                            <th>方向</th>
                            <th>经度</th>
                            <th>纬度</th>
                            <th>在线状态</th>
                            <th>定位状态</th>
                            <th>报警</th>
                            <th>时间</th>
                        </tr>
                        </thead>
                        <tbody id="tbody">

                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="12">
                                <ul class="pagination pull-right"></ul>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- 右键,jQuery的包请不要冲突-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.contextmenu.r2.js"></script>

<script type="text/javascript"
        src="http://api.map.baidu.com/api?v=2.0&ak=xj2Gd1nMEwDHFcGeQvr0WORGk0t1Dlhw"></script>
<script type="text/javascript"
        src="http://api.map.baidu.com/library/TextIconOverlay/1.2/src/TextIconOverlay_min.js"></script>
<script type="text/javascript" src="js/MakerClusterer.js "></script>
<!-- 加载鼠标绘制工具 -->
<script type="text/javascript"
        src="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.js"></script>
<link rel="stylesheet"
      href="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.css"/>
<!-- 加载检索信息窗口 -->
<script type="text/javascript"
        src="http://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.js"></script>
<link rel="stylesheet"
      href="http://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.css"/>
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>

<script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="js/plugins/gritter/jquery.gritter.min.js"></script>
<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
<!-- <script src="js/jquery.min.js?v=2.1.4"></script> -->
<script src="js/bootstrap.min.js?v=3.3.5"></script>
<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="js/plugins/footable/footable.all.min.js"></script>
<script src="js/plugins/jsTree/jstree.min.js"></script>
<!-- <script src="js/addInfoWindow.js"></script> -->
<!--本地 添加信息窗口js-->
<!-- <script src="http://libs.baidu.com/jquery/1.9.0/jquery.js"></script> -->
<!--添加信息窗口  -->
<!-- gy 添加视频播放器 -->
<!-- <script src="plyr/dist/plyr.js"></script> -->
<!-- hls播放器插件 -->
<script src="js/video.js"></script>
<script src="js/videojs-live.js"></script>
<script>
    $("#video-mode").click(function () {
        var taxiNums = $("#show1").val();
        if (taxiNums === "") {
            swal({
                title: "请选择一辆车",
                type: "warning"
            });
            if (($('#allmap').hasClass('col-sm-10'))) {
                $('#allmap').removeClass('col-sm-4');
                $('#middleMap').removeClass('col-sm-6');
                $('#allmap').addClass('col-sm-10');
            }
            return;
        }
        var nums = taxiNums.split(",");
        if (nums.length >= 2) {
            swal({
                title: "只能选择一辆车",
                type: "warning"
            });
            if (($('#allmap').hasClass('col-sm-4'))) {
                $('#allmap').removeClass('col-sm-4');
                $('#middleMap').removeClass('col-sm-6');
                $('#allmap').addClass('col-sm-10');
            }
            return;
        }
                if (($('#allmap').hasClass('col-sm-10'))) {
            $('#allmap').removeClass('col-sm-10');
            $('#middleMap').addClass('col-sm-6');
            $('#allmap').addClass('col-sm-4');
        }
        $("#videoDiv").css("display", "block");
        $("#vedioNums").val(taxiNums);
        $("#vedioForm").submit();
    });

    $("#map-mode").click(function () {
        if (($('#allmap').hasClass('col-sm-4'))) {
            $('#middleMap').removeClass('col-sm-6');
            $('#allmap').removeClass('col-sm-4');
            $('#allmap').addClass('col-sm-10');
        }
        $("#vedioNums").val("");
        $("#vedioForm").submit();
        $("#allmap").css("display", "block");
        $("#videoDiv").css("display", "none");
        markerClusterer.clearMarkers();
        var taxiNums = $("#show1").val();
        if (taxiNums == "") {
            swal({
                title : "请先选择车辆",
                type : "warning"
            });
        } else {
            tempState = false;
            //$("#state").val("实时监控中");
            markerClusterer.clearMarkers();
            chlink();
        }
    });
    //实现jQuery轮询树的状态
    $(document).ready(function () {
        setInterval(pollingTree, 10 * 1000);
    });
    //分页
    // $(document).ready(function () {
    //     $('#table1').DataTable({
    //         "pagingType": "full_numbers"
    //     });
    // });
    //设置定位间隔
    $("#interval").change(function () {
        var interval = $("#interval").val();
        var timer = $("#timer").val();
        if (interval < 5 || interval > 60) {
            $("#interval").val("10");
            interval = 10;
        }
        window.clearInterval(timer);
        var timer = setInterval('chlink()', interval * 1000);
        //注意:执行的函数需要加引号,否则会报错的
        $("#timer").val(timer);
    });

    $("#miletable").hide();
    $("#show1").val("");

    function pollingTree() {
        $
            .ajax({
                type: "POST",
                url: "/GpsManage/getTreeOfTempJson",
                dataType: "json",
                success: function (data) {
                    sessionStorage.setItem("treelist", JSON
                        .stringify(data));
                    $
                        .each(
                            data.children,
                            function (n, value1) {
                                $
                                    .each(
                                        value1.children,
                                        function (n,
                                                  value2) {
                                            if (value2.onlineState === "在线") {
                                                if (value2.positioningState === "定位") {
                                                    value2.text = value2.text
                                                        + " [<span class=\"green\">定位</span>]"
                                                } else {
                                                    value2.text = value2.text
                                                        + " [<span class=\"red\">未定位</span>]"
                                                }
                                            } else {
                                                if (value2.positioningState === "定位") {
                                                    value2.text = value2.text
                                                        + " [<span class=\"green\">定位</span>]"
                                                } else {
                                                    value2.text = value2.text
                                                        + " [<span class=\"red\">----</span>]"
                                                }
                                            }
                                        });
                            });
                    var tree = $("#jstree1")
                    tree.jstree(true).settings.core.data = data;
                    tree.jstree(true).refresh(true);
                }
            });
    }

    //加载jstree
    $("#jstree1")
        .jstree(
            {
                "core": {
                    "check_callback": true,
                    "data": function (node, cb) {
                        $
                            .ajax({
                                type: "POST",
                                dataType: "json",
                                url: "/GpsManage/getTreeOfTempJson",
                                success: function (data) {
                                    $
                                        .each(
                                            data.children,
                                            function (n,
                                                      value1) {
                                                $
                                                    .each(
                                                        value1.children,
                                                        function (
                                                            n,
                                                            value2) {
                                                            if (value2.onlineState === "在线") {
                                                                if (value2.positioningState === "定位") {
                                                                    value2.text = value2.text
                                                                        + " [<span class=\"green\">定位</span>]"
                                                                } else {
                                                                    value2.text = value2.text
                                                                        + " [<span class=\"red\">未定位</span>]"
                                                                }
                                                            } else {
                                                                if (value2.positioningState === "定位") {
                                                                    value2.text = value2.text
                                                                        + " [<span class=\"green\">定位</span>]"
                                                                } else {
                                                                    value2.text = value2.text
                                                                        + " [<span class=\"red\">----</span>]"
                                                                }
                                                            }
                                                        });
                                            });
                                    cb.call(this, data);
                                }
                            });
                    }
                },
                "plugins": ["types", "dnd", "checkbox", "sort",
                    "search", "unique"],
                "types": {
                    "car": {
                        "icon": "fa fa-car"
                    },
                    "default": {
                        "icon": "fa fa-folder"
                    }
                }
            });

    //获取选择节点的id
    $("#jstree1").on('changed.jstree', function (e, data) {
        r = [];
        var i, j;
        for (i = 0, j = data.selected.length; i < j; i++) {
            var node = data.instance.get_node(data.selected[i]);
            if (data.instance.is_leaf(node)) {
                r.push(node.id);
            }
        }
        $("#show1").val(r);
    }).on(
        "search.jstree",
        function (e, data) {
            if (data.nodes.length) {
                var matchingNodes = data.nodes; // change
                $(this).find(".jstree-node").hide().filter(
                    '.jstree-last').filter(function () {
                    return this.nextSibling;
                }).removeClass('jstree-last');
                data.nodes.parentsUntil(".jstree")

                    .addBack().show().filter(".jstree-children").each(
                    function () {
                        $(this).children(".jstree-node:visible")
                            .eq(-1).addClass("jstree-last");
                    });
                // nodes need to be in an expanded state
                matchingNodes.find(".jstree-node").show(); // change
            }
        }).on(
        "clear_search.jstree",
        function (e, data) {
            if (data.nodes.length) {
                $(this).find(".jstree-node").css("display", "").filter(
                    '.jstree-last').filter(function () {
                    return this.nextSibling;
                }).removeClass('jstree-last');
            }
        });

    //jsTree的模糊查询
    var to = false;
    $("#txtIndustryArea").keyup(function () {
        if (to) {
            clearTimeout(to);
        }
        to = setTimeout(function () {
            var v = $("#txtIndustryArea").val();
            var temp = $("#jstree1").is(":hidden");
            if (temp == true) {
                $("#jstree1").show();
            }
            $("#jstree1").jstree(true).search(v);
        }, 250);
    });

    //百度地图开始
    var x = 109.48;
    var y = 36.60;
    var ggPoint = new BMap.Point(x, y);
    var tempmarker = [];
    //地图初始化
    var bm = new BMap.Map("map");
    bm.centerAndZoom("铜仁", 13);
<%--    bm.addControl(new BMap.NavigationControl());--%>
    var markerClusterer = new BMapLib.MarkerClusterer(bm, {
        isAverangeCenter: false
    });
    bm.enableScrollWheelZoom();
    //添加控件
    var mapType1 = new BMap.MapTypeControl({
        mapTypes: [BMAP_NORMAL_MAP, BMAP_HYBRID_MAP]
    });
    var mapType2 = new BMap.MapTypeControl({
        anchor: BMAP_ANCHOR_TOP_LEFT
    });

    var overView = new BMap.OverviewMapControl();
    var overViewOpen = new BMap.OverviewMapControl({
        isOpen: true,
        anchor: BMAP_ANCHOR_BOTTOM_RIGHT
    });
    //添加地图类型和缩略图

<%--    bm.addControl(mapType1); //2D图，卫星图--%>
<%--    bm.addControl(mapType2); //左上角，默认地图控件--%>
    bm.setCurrentCity("铜仁"); //由于有3D图，需要设置城市哦
<%--    bm.addControl(overView); //添加默认缩略地图控件--%>
    bm.addControl(overViewOpen); //右下角，打开

    var checkedIds = "";
    //添加信息窗口
    var opts = {
        width: 320, // 信息窗口宽度
        height: 150, // 信息窗口高度
        title: "车辆信息窗口", // 信息窗口标题
        enableMessage: true
        //设置允许信息窗发送短息
    };

    var timer = setInterval('chlink()', 10000); //注意:执行的函数需要加引号,否则会报错的
    $("#timer").val(timer);

    function trclick(trNode) {
        var longtitude;
        var latitude;
        var taxiNumber = trNode.firstChild.innerHTML;
        longtitude = trNode.cells[3].innerText;
        latitude = trNode.cells[4].innerText;
        var temPoint = new BMap.Point(longtitude, latitude);
        //alert(taxiNumber);

        //进行车辆详情框的数据刷新
        details(taxiNumber, longtitude, latitude);
        bm.centerAndZoom(temPoint, 15);

    }

    function chlink() {
        //准备marker数组


        var isuNums = $("#show1").val();
        if (isuNums != "") {
            $("#templist").show();
            var args = {
                "isuNums": isuNums
            };
            var url = "monitoring";
            $
                .post(
                    url,
                    args,
                    function (data) {
                        var points = [];
                        var no_zero_list = [];

                        var trStr = "";
                        //alert(data);

                        var d = eval("(" + data + ")");

                        for (var i = 0; i < d.length; i++) {
                            //排除经纬度为0的点
                            if (d[i].longitude != 0
                                && d[i].latitude != 0) {
                                no_zero_list.push(d[i])
                            }
                            if (d[i].longitude != 0
                                && d[i].latitude != 0) {

                                var ggPoint = new BMap.Point(
                                    d[i].longitude,
                                    d[i].latitude);
                                points.push(ggPoint);
                            }
                            var direction = "";
                            if (d[i].direction == 0) {
                                direction = "↑";
                            } else if (d[i].direction > 0
                                && d[i].direction < 90) {
                                direction = "↗";

                            } else if (d[i].direction == 90) {
                                direction = "→";
                            } else if (d[i].direction > 90
                                && d[i].direction < 180) {

                                direction = " ↘ ";
                            } else if (d[i].direction == 180) {
                                d[i].direction = "↓";
                            } else if (d[i].direction > 180
                                && d[i].direction < 270) {

                                direction = "↙ ";
                            } else if (d[i].direction == 270) {
                                direction = "←";

                            } else if (d[i].direction > 270
                                && d[i].direction < 360) {

                                direction = "↖";
                            }

                            trStr += "<tr onclick=trclick(this)>"
                                + "<td>"
                                + d[i].taxiNum
                                + "</td>"
                                + "<td>"
                                + d[i].speed
                                + "</td>"
                                + "<td>"
                                + direction
                                + "</td>"
                                + "<td>"
                                + d[i].longitude
                                + "</td>"
                                + "<td>"
                                + d[i].latitude
                                + "</td>"
                                + "<td>"
                                + d[i].state
                                + "</td>"
                                + "<td>"
                                + d[i].locateState
                                + "</td>"
                                + "<td>"
                                + d[i].time
                                + "</td>" + "</tr>";
                        }
                        //显示实时监控列表
                        document.getElementById("tbody").innerHTML = trStr;
                        /* //改变图标
                                        var myIcon = new BMap.Icon("car1.png",
                                            new BMap.Size(50, 40));
                                    marker = new BMap.Marker(data.points[i], {
                                        icon : myIcon
                                    });
                                    bm.addOverlay(marker2); */
                        //循环外
                        //markerCluster.clearMarkers() ;
                        var mks = [];
                        for (var i = 0; i < d.length; i++) {
                            console.log("第" + i + "个点");
                            var myIcon;
                            if (no_zero_list[i].state == '在线') {

                                if (no_zero_list[i].locateState == '不定位') {
                                    myIcon = new BMap.Icon(
                                        "img/car_red.png",
                                        new BMap.Size(21, 39));
                                } else {
                                    if (no_zero_list[i].operateState == '载客') {
                                        //img
                                        myIcon = new BMap.Icon(
                                            "img/car_blue.png",
                                            new BMap.Size(21,
                                                39));
                                    } else {
                                        //img
                                        myIcon = new BMap.Icon(
                                            "img/car_green.png",
                                            new BMap.Size(21,
                                                39));
                                    }
                                }

                            } else {
                                myIcon = new BMap.Icon(
                                    "img/car_gray.png",
                                    new BMap.Size(21, 39));
                            }

                            var marker = new BMap.Marker(
                                new BMap.Point(d[i].longitude,
                                    d[i].latitude), {
                                    icon: myIcon
                                });
                            //bm.addOverlay(marker);
                            //markersA.push(new BMap.Marker());
                            //添加车牌号及在线信息
                            var k = i;
                            // for (var k = 0; k <= i; k++) {
                            // if (i == k) {
                            var label = new BMap.Label(
                                "<b class='islabel'>"
                                + no_zero_list[k].taxiNum
                                + "/"
                                + no_zero_list[k].state
                                + "</b>", {
                                    offset: new BMap.Size(-25,
                                        -18)
                                });
                            marker
                                .setRotation(no_zero_list[k].direction);
                            //var label = new BMap.Label("我是文字标注哦",{offset:new BMap.Size(20,-10)});
                            marker.setLabel(label);
                            mks.push(marker);
                            var direction2 = "";
                            if (no_zero_list[k].direction == 0) {
                                direction2 = "↑";
                            } else if (no_zero_list[k].direction > 0
                                && no_zero_list[k].direction < 90) {
                                direction2 = "↗";

                            } else if (no_zero_list[k].direction == 90) {
                                direction2 = "→";
                            } else if (no_zero_list[k].direction > 90
                                && no_zero_list[k].direction < 180) {

                                direction2 = " ↘ ";
                            } else if (no_zero_list[k].direction == 180) {
                                direction2 = "↓";
                            } else if (no_zero_list[k].direction > 180
                                && no_zero_list[k].direction < 270) {

                                direction2 = "↙ ";
                            } else if (no_zero_list[k].direction == 270) {
                                direction2 = "←";

                            } else if (no_zero_list[k].direction > 270
                                && no_zero_list[k].direction < 360) {

                                direction2 = "↖";
                            }

                            var content = "车牌号："
                                + no_zero_list[k].taxiNum
                                + "<br>" + "时间: "
                                +no_zero_list[k].time
                                + "    速度: "
                                +no_zero_list[k].speed
                                +"公里/时"
                                +"("
                                +direction2
                                +")"
                                + "<br>" + "位置: "
                                +"  经度: "
                                +no_zero_list[k].longitude
                                +" 纬度:"
                                +no_zero_list[k].latitude
                                + "<br>" + "状态: "
                                +no_zero_list[k].locateState
                                +"(定位状态) "
                                + no_zero_list[k].operateState
                                +"(营运状态)"
                                + no_zero_list[k].state
                                +"(在线状态)"
                                // + "<br>" + "定位状态: "
                                // + no_zero_list[k].locateState
                                // + "<br>" + "营运状态： "
                                // + no_zero_list[k].operateState
                                // + "<br>" + "当前方向:" + direction2
                                +"<br>"
                                +"<input type='button' id='carVideo' value='视频'/>"
                                +"<input type='button' id='carPlayback' value='回放'/>"
                                +"<input type='button' id='carTrack' value='轨迹'/>"
                                +"<input type='button' id='carText' value='文本'/>"
                                +"<input type='button' id='carFormat' value='格式化'/>"
                                + "<br>";
                            addClickHandler(content, marker);
                            // }
                            // }

                            // bm.setCenter(data.points[i]);//调整地图的中心坐标
                            //添加监听事件

                        }
                        //赋值
                        //清空数组
                        markerClusterer.clearMarkers();
                        //markerClusterer.removeMarkers(tempmarker);
                        tempmarker = mks.concat();
                        markerClusterer.addMarkers(mks);
                        //回调函数结束

                        setTimeout(function () {
                            /*   bm.addEventListener("zoomend",
                                function() {
                                    var DiTu = this.getZoom();
                                    $('b.islabel').parent()[DiTu >= 1 ? 'hide'
                                            : 'show']();
                                });   */
                            /*
                            //bm.clearOverlays();
                            //markerCluster.clearMarkers();
                            //bm.setMapStatus(MapStatusUpdateFactory.newMapStatus(bm.getMapStatus()));
                            var interval = $('interval').val();
                            //console.log("又开始删除marker");
                            //console.log("待删除数组的 长度为:"+tempmarker.length);
                            console.log("当前marker集合"
                            + markerClusterer);
                            var convertor = new BMap.Convertor();
                            convertor.translate(points, 1, 5,
                            translateCallback)  */
                            var interval = $('interval').val();
                            //chlink();
                        }, interval);

                    });

        } else {
            // $("#templist").hide();
        }
    }

    //checkbox改变时
    $("input[name='chbox']").change(
        function () {
            var oneches = document.getElementsByName("chbox");
            for (var i = 0; i < oneches.length; i++) {
                if (oneches[i].checked == true) {
                    //避免重复累计id （不含该id时进行累加）
                    if (checkedIds.indexOf(oneches[i].value) == -1) {
                        checkedIds = checkedIds + oneches[i].value
                            + ",";
                    }
                }
                if (oneches[i].checked == false) {
                    //取消复选框时 含有该id时将id从全局变量中去除
                    if (checkedIds.indexOf(oneches[i].value) != -1) {
                        checkedIds = checkedIds.replace(
                            (oneches[i].value + ","), "");
                    }
                }
            }
            //保存选中的数据到文本框
            $("#show1").val(checkedIds);

        });
    var tempState = true;
    //开始进行汽车的实时监控、monitoring



    //右键功能
    $(".trdemo").contextMenu(
        'myMenu1',
        {
            bindings: {
                'selSetIsu': function (t) {//查询设置终端参数
                    alert('该功能正在拼命开发中…');
                },
                'setEvent': function (t) {//设置事件
                    alert('该功能正在拼命开发中…');
                },
                'setInfo': function (t) {//设置信息菜单
                    alert('该功能正在拼命开发中…');
                },
                'setSpeed': function (t) {//设置超速参数
                    var taxiNums = $("#show1").val();
                    var taxinum;
                    taxinum = taxiNums.split(",");
                    if (taxiNums == "") {
                        swal({
                            title: "请先选择车辆",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length == 1) {
                        swal({
                            title: "该公司暂无车辆或车牌号格式错误",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length < 7) {
                        swal({
                            title: "车牌号格式有误",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length > 7) {
                        //遍历数组
                        for (var i = 0; i < taxinum.length; i++) {
                            var taxi = taxinum[i];
                            if (taxi.length != 7) {
                                swal({
                                    title: "部分车辆车牌号格式不对或选定的公司没有车辆",
                                    type: "warning"
                                });
                                return;
                            }

                        }
                    }

                    window.location.href = "enterSetSpeed?taxiNum="
                        + taxiNums;

                },
                'reTrack': function (t) {//轨迹回放
                    //获取文本框中的值
                    var taxiNums = $("#show1").val();
                    var taxinum;
                    taxinum = taxiNums.split(",");
                    if (taxiNums == "") {
                        swal({
                            title: "请先选择车辆",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length == 1) {
                        swal({
                            title: "该公司暂无车辆或车牌号格式错误",
                            type: "warning"
                        });
                        return;

                    } else if (taxiNums.length < 7) {
                        swal({
                            title: "车牌号长度有误",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length > 7) {
                        swal({
                            title: "请选择一辆车",
                            type: "warning"
                        });
                        return;
                    }
                    console.log("taxinum"+taxinum);
                    var url="trackBack.jsp?thisTaxiNum="+taxinum;
                    pageUp(url,"轨迹回放");
                },
                'showVideo': function (t) {//查看实时视频
                    //获取文本框中的值
                    var taxiNums = $("#show1").val();
                    var taxinum;
                    taxinum = taxiNums.split(",");
                    if (taxiNums == "") {
                        swal({
                            title: "请先选择车辆",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length == 1) {
                        swal({
                            title: "该公司暂无车辆或车牌号格式错误",
                            type: "warning"
                        });
                        return;

                    } else if (taxiNums.length < 7) {
                        swal({
                            title: "车牌号长度有误",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length > 7) {
                        swal({
                            title: "请选择一辆车",
                            type: "warning"
                        });
                        return;
                    }
                    console.log("taxinum"+taxinum);
                    $("#videoDiv").css("display", "block");
                    $("#vedioNums").val(taxinum);
                    $("#vedioForm").submit();
                },
                'sendText': function (t) {//下发文本消息
                    //获取文本框中的值
                    var taxiNums = $("#show1").val();
                    var taxinum;
                    taxinum = taxiNums.split(",");
                    if (taxiNums == "") {
                        swal({
                            title: "请先选择车辆",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length == 1) {
                        swal({
                            title: "该公司暂无车辆或车牌号格式错误",
                            type: "warning"
                        });
                        return;

                    } else if (taxiNums.length < 7) {
                        swal({
                            title: "车牌号长度有误",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length > 7) {
                        //遍历数组
                        for (var i = 0; i < taxinum.length; i++) {
                            var taxi = taxinum[i];
                            if (taxi.length != 7) {
                                swal({
                                    title: "部分车辆车牌号格式不对或选定的公司没有车辆",
                                    type: "warning"
                                });
                                return;
                            }

                        }
                    }
                    var url="enterSendText?taxiNum=" + taxiNums;
                    pageUp(url,"下发文本");

                },
                'setPower': function (t) {
                    //远程开关机
                    var taxiNums = $("#show1").val();
                    var taxinum;
                    taxinum = taxiNums.split(",");
                    if (taxiNums == "") {
                        swal({
                            title: "请先选择车辆",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length == 1) {
                        swal({
                            title: "该公司暂无车辆或车牌号格式错误",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length < 7) {
                        swal({
                            title: "车牌号格式有误",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length > 7) {
                        //遍历数组
                        for (var i = 0; i < taxinum.length; i++) {
                            var taxi = taxinum[i];
                            if (taxi.length != 7) {
                                swal({
                                    title: "部分车辆车牌号格式不对或选定的公司没有车辆",
                                    type: "warning"
                                });
                                return;
                            }

                        }
                    }

                    window.location.href = "enterSetPower?taxiNum="
                        + taxiNums;

                },
                'setPrice': function (t) {//远程调价
                    var taxiNums = $("#show1").val();
                    var taxinum;
                    taxinum = taxiNums.split(",");
                    if (taxiNums == "") {
                        swal({
                            title: "请先选择车辆",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length == 1) {
                        swal({
                            title: "该公司暂无车辆或车牌号格式错误",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length < 7) {
                        swal({
                            title: "车牌号格式有误",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length > 7) {
                        //遍历数组
                        for (var i = 0; i < taxinum.length; i++) {
                            var taxi = taxinum[i];
                            if (taxi.length != 7) {
                                swal({
                                    title: "部分车辆车牌号格式不对或选定的公司没有车辆",
                                    type: "warning"
                                });
                                return;
                            }

                        }
                    }

                    window.location.href = "enterSetPrice?taxiNum="
                        + taxiNums;
                },
                'infoSer': function (t) {//信息服务
                    alert('该功能正在拼命开发中…');
                },
                'setRelay': function (t) { //断油断电
                    var taxiNums = $("#show1").val();
                    var taxinum;
                    taxinum = taxiNums.split(",");
                    if (taxiNums == "") {
                        swal({
                            title: "请先选择车辆",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length == 1) {
                        swal({
                            title: "该公司暂无车辆或车牌号格式错误",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length < 7) {
                        swal({
                            title: "车牌号格式有误",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length > 7) {
                        //遍历数组
                        for (var i = 0; i < taxinum.length; i++) {
                            var taxi = taxinum[i];
                            if (taxi.length != 7) {
                                swal({
                                    title: "部分车辆车牌号格式不对或选定的公司没有车辆",
                                    type: "warning"
                                });
                                return;
                            }

                        }
                    }

                    window.location.href = "enterSetRelay?taxiNum="
                        + taxiNums;

                },
                'sendQuiz': function (t) {//下发提问
                    alert('该功能正在拼命开发中…');
                },
                'setArea': function (t) { //下发区域
                    var taxiNums = $("#show1").val();
                    var taxinum;
                    taxinum = taxiNums.split(",");
                    if (taxiNums == "") {
                        swal({
                            title: "请先选择车辆",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length == 1) {
                        swal({
                            title: "该公司暂无车辆或车牌号格式错误",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length < 7) {
                        swal({
                            title: "车牌号格式有误",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length > 7) {
                        //遍历数组
                        for (var i = 0; i < taxinum.length; i++) {
                            var taxi = taxinum[i];
                            if (taxi.length != 7) {
                                swal({
                                    title: "部分车辆车牌号格式不对或选定的公司没有车辆",
                                    type: "warning"
                                });
                                return;
                            }

                        }
                    }

                    window.location.href = "enterSetArea?taxiNum="
                        + taxiNums;

                },
                'deleteArea': function (t) { //下发区域
                    var taxiNums = $("#show1").val();
                    var taxinum;
                    taxinum = taxiNums.split(",");
                    if (taxiNums == "") {
                        swal({
                            title: "请先选择车辆",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length == 1) {
                        swal({
                            title: "该公司暂无车辆或车牌号格式错误",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length < 7) {
                        swal({
                            title: "车牌号格式有误",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length > 7) {
                        //遍历数组
                        for (var i = 0; i < taxinum.length; i++) {
                            var taxi = taxinum[i];
                            if (taxi.length != 7) {
                                swal({
                                    title: "部分车辆车牌号格式不对或选定的公司没有车辆",
                                    type: "warning"
                                });
                                return;
                            }

                        }
                    }

                    window.location.href = "enterDeleteArea?taxiNum="
                        + taxiNums;

                },
                'phoneCall': function (t) {//电话回拨
                    var taxiNums = $("#show1").val();
                    if (taxiNums == "") {
                        swal({
                            title: "请先选择车辆",
                            type: "warning"
                        });
                    } else if (taxiNums.length == 1) {
                        swal({
                            title: "该公司暂无车辆或车牌号格式错误",
                            type: "warning"
                        });

                    } else if (taxiNums.length < 7) {
                        swal({
                            title: "车牌号格式不对",
                            type: "warning"
                        });

                    } else if (taxiNums.length > 7) {
                        swal({
                            title: "只能选择一辆车",
                            type: "warning"
                        });
                    } else {
                        window.location.href = "intoBackdail?taxiNum="
                            + taxiNums;
                    }
                },
                'sendPhoneText': function (t) {//下发电话本
                    alert('该功能正在拼命开发中…');
                },
                'addressTrack': function (t) {//临时位置追踪
                    alert('该功能正在拼命开发中…');
                },
                'carCall': function (t) {//车辆点名
                    alert('该功能正在拼命开发中…');
                },
                'takePic': function (t) {//拍照
                    var taxiNums = $("#show1").val();
                    var taxinum;
                    taxinum = taxiNums.split(",");
                    if (taxiNums == "") {
                        swal({
                            title: "请先选择车辆",
                            type: "warning"
                        });
                    } else if (taxiNums.length == 1) {
                        swal({
                            title: "该公司暂无车辆或车牌号格式错误",
                            type: "warning"
                        });

                    } else if (taxiNums.length < 7) {
                        swal({
                            title: "车牌号格式有误",
                            type: "warning"
                        });

                    } else if (taxiNums.length > 7) {
                        //遍历数组
                        for (var i = 0; i < taxinum.length; i++) {
                            var taxi = taxinum[i];
                            if (taxi.length != 7) {
                                swal({
                                    title: "部分车辆车牌号格式不对或选定的公司没有车辆",
                                    type: "warning"
                                });
                                return;
                            }

                        }
                        window.location.href = "enterTakePic?taxiNum="
                            + taxiNums;
                    } else {
                        window.location.href = "enterTakePic?taxiNum="
                            + taxiNums;
                    }

                },
                'setControl': function (t) {//车辆点名
                    var taxiNums = $("#show1").val();
                    var taxinum;
                    taxinum = taxiNums.split(",");
                    if (taxiNums == "") {
                        swal({
                            title: "请先选择车辆",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length == 1) {
                        swal({
                            title: "该公司暂无车辆或车牌号格式错误",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length < 7) {
                        swal({
                            title: "车牌号格式有误",
                            type: "warning"
                        });
                        return;
                    } else if (taxiNums.length > 7) {
                        //遍历数组
                        for (var i = 0; i < taxinum.length; i++) {
                            var taxi = taxinum[i];
                            if (taxi.length != 7) {
                                swal({
                                    title: "部分车辆车牌号格式不对或选定的公司没有车辆",
                                    type: "warning"
                                });
                                return;
                            }

                        }
                    }

                    window.location.href = "enterSetControl?taxiNum="
                        + taxiNums;
                }
            }
        });

    //实时定位方法结束，下面为添加信息框调用的方法
    function addClickHandler(content, marker) {
        marker.addEventListener("click", function (e) {
            openInfo(content, e);
        });
    }
    function reshowcar(taxNum){
        $.ajax({
            url:"getReshow",    //请求的url地址
            dataType:"json",   //返回格式为json
            async:true,//请求是否异步，默认为异步，这也是ajax重要特性
            data:{"taxiNum":taxNum},    //参数值
            type:"GET",   //请求方式
            success:function(req){
                var storage=window.localStorage;
                var url = req.url;
                var isu = req.isu;
                storage.setItem("url",url);
                storage.setItem("isu",isu);
                // window.top.href="timeTest.jsp";
                parent.open("timeTest.jsp");
                //请求成功时处理
            },
        });
    }
    function openInfo(content, e) {
        var p = e.target;
        var point = new BMap.Point(p.getPosition().lng, p.getPosition().lat);
        var infoWindow = new BMap.InfoWindow(content, opts); // 创建信息窗口对象
        bm.openInfoWindow(infoWindow, point); //开启信息窗口
        //判断信息窗口的状态
        if(!infoWindow.isOpen()){
            carNum = content.toString().substring(4,11);
            //如果没有打开，监听打开事件，获取按钮，添加事件
            infoWindow.addEventListener("open", function () {
                //视频
                document.getElementById("carVideo").onclick = function (e) {
                    var taxiNums = $("#show1").val();
                    if (taxiNums === "") {
                        swal({
                            title: "请选择一辆车",
                            type: "warning"
                        });
                        return;
                    }
                    var nums = taxiNums.split(",");
                    if (nums.length >= 2) {
                        swal({
                            title: "只能选择一辆车",
                            type: "warning"
                        });
                        return;
                    }
                    $("#videoDiv").css("display", "block");
                    $("#vedioNums").val(taxiNums);
                    $("#vedioForm").submit();
                }
                //回放
                document.getElementById("carPlayback").onclick = function (e,value,row,index) {
                    reshowcar(carNum);
                }
                //轨迹
                document.getElementById("carTrack").onclick = function (e) {
                    var taxiNums = $("#show1").val();
                    var taxinum= taxiNums.split(",");
                    var url="trackBack.jsp?thisTaxiNum="+taxinum;
                    pageUp(url,"轨迹回放");
                }
                //文本
                document.getElementById("carText").onclick = function (e) {
                    var taxiNums = $("#show1").val();
                    var url="enterSendText?taxiNum=" + taxiNums;
                    pageUp(url,"下发文本");
                }
                //格式化
                document.getElementById("carFormat").onclick = function (e) {
                    var taxiNums = $("#show1").val();
                    var isuNum;
                    $.ajax({
                        type: "post",
                        url: "getTaxiByTaxiNum",
                        data: {
                            "taxiNum": taxiNums
                        },
                        async:true, // 异步请求
                        cache:false, // 设置为 false 将不缓存此页面
                        dataType: 'json', // 返回对象
                        success: function(data) {
                            isuNum=data.isuNum;
                        },
                        error: function(data) {
                            // 请求失败函数
                            console.log(data);
                        }
                    })
                    swal({
                        title: "您确定要格式化磁盘吗?",
                        type : "warning",
                        showCancelButton : true,
                        cancelButtonText : "取消"
                    }, function() {
                        window.location.href="formatDiscMain?isuNum="+isuNum;
                    });
                }
            })
        }else{
            //如果打开则直接获取按钮，添加部事件
            //视频
            document.getElementById("carVideo").onclick = function (e)  {
                var taxiNums = $("#show1").val();
                if (taxiNums === "") {
                    swal({
                        title: "请选择一辆车",
                        type: "warning"
                    });
                    return;
                }
                var nums = taxiNums.split(",");
                if (nums.length >= 2) {
                    swal({
                        title: "只能选择一辆车",
                        type: "warning"
                    });
                    return;
                }
                $("#allmap").css("display", "none");
                $("#videoDiv").css("display", "block");
                $("#vedioNums").val(taxiNums);
                $("#vedioForm").submit();
            }
            //回放
            document.getElementById("carPlayback").onclick = function (e,value,row,index) {
                reshowcar(carNum);
            }
            //轨迹
            document.getElementById("carTrack").onclick = function (e) {
                var taxiNums = $("#show1").val();
                var taxinum= taxiNums.split(",");
                var url="trackBack.jsp?thisTaxiNum="+taxinum;
                pageUp(url,"轨迹回放");
            }
            //文本
            document.getElementById("carText").onclick = function (e) {
                var taxiNums = $("#show1").val();
                var url="enterSendText?taxiNum=" + taxiNums;
                pageUp(url,"下发文本");
            }
            //格式化
            document.getElementById("carFormat").onclick = function (e) {
                var taxiNums = $("#show1").val();
                var isuNum;
                $.ajax({
                    type: "post",
                    url: "getTaxiByTaxiNum",
                    data: {
                        "taxiNum": taxiNums
                    },
                    async:true, // 异步请求
                    cache:false, // 设置为 false 将不缓存此页面
                    dataType: 'json', // 返回对象
                    success: function(data) {
                        isuNum=data.isuNum;
                    },
                    error: function(data) {
                        // 请求失败函数
                        console.log(data);
                    }
                })
                swal({
                    title: "您确定要格式化磁盘吗?",
                    type : "warning",
                    showCancelButton : true,
                    cancelButtonText : "取消"
                }, function() {
                    window.location.href="formatDiscMain?isuNum="+isuNum;
                });
            }
        }
    }
    //弹出一个新的tab
    function pageUp(url, title){
        var nav = $(window.parent.document).find('.J_menuTabs .page-tabs-content ');
        $(window.parent.document).find('.J_menuTabs .page-tabs-content ').find(".J_menuTab.active").removeClass("active");
        $(window.parent.document).find('.J_mainContent').find("iframe").css("display", "none");
        var iframe = '<iframe class="J_iframe" name="iframe10000" width="100%" height="100%" src="' + url + '" frameborder="0" data-id="' + url
            + '" seamless="" style="display: inline;"></iframe>';
        $(window.parent.document).find('.J_menuTabs .page-tabs-content ').append(
            ' <a href="javascript:;" class="J_menuTab active" data-id="'+url+'">' + title + ' <i class="fa fa-times-circle"></i></a>');
        $(window.parent.document).find('.J_mainContent').append(iframe);
    }



    function details(number, lon, lat) {
        var url = "getTaxiDetails";
        var args = {
            "taxiNum": number
        };
        $.post(url, args, function (data) {
            var taxi = eval("(" + data + ")");
            //alert(taxi.taxiNum);
            $("#taxiNumber").text(taxi.taxiNum);
            $("#gps").text(taxi.gpsState);
            $("#annta").text(taxi.anntaState);
            $("#energy").text(taxi.energyState);
            $("#phone").text(taxi.phoneState);
            $("#acc").text(taxi.accState);
            $("#lat").text(taxi.latState);
            $("#lon").text(taxi.lonState);
            $("#meter").text(taxi.meterState);
            $("#alarm").text(taxi.alarmState);
            $("#key").text(taxi.keyState);
            $("#oil").text(taxi.oilState);
            $("#eng").text(taxi.engState);
            $("#door").text(taxi.doorState);
            $("#direction").text(taxi.directionState);
            //$('.details tr').eq(2).find("td").html(taxi.taxiNum);
            //处理
            var gc = new BMap.Geocoder();
            var point = new BMap.Point(lon, lat);
            gc.getLocation(point, function (rs) {
                var addComp = rs.addressComponents;
                $("#addnow").text(
                    addComp.province + ", " + addComp.city + ", "
                    + addComp.district + ", " + addComp.street
                    + ", " + addComp.streetNumber);
            });
        });
    }

    //隐藏视频窗口,当点击显示的时候再打开。
    var a = 0;//计数器
    function showdiv() {
        var taxiNums = $("#show1").val();
        //控制地图大小
        if (($('#middleMap').hasClass('col-sm-10'))) {
            $('#middleMap').removeClass('col-sm-10');
            $('#middleMap').addClass('col-sm-8');
            //显示时间选择轴
            //$("#timetools").show();
            document.getElementById('playvedio').style.display = document
                .getElementById('playvedio').style.display == "none" ? "block"
                : "none";
        } else {
            if (taxiNums !== "") {
                //检验手机号
                if (taxiNums.length == 1) {
                    swal({
                        title: "该公司暂无车辆或车牌号格式错误",
                        type: "warning"
                    });
                    return;
                } else if (taxiNums.length < 7) {
                    swal({
                        title: "车牌号格式有误",
                        type: "warning"
                    });
                    return;
                } else if (taxiNums.length > 8) {
                    swal({
                        title: "只允许选择一辆车",
                        type: "warning"
                    });
                    return;
                } else {
                    //后台取指定设备的视频连接
                    $("#vedioNums").val(taxiNums);
                    $("#vedioForm").submit();
                    //window.location.href = "getTreeOfTemp3?taxiNum="
                    //+ taxiNums;
                }

            } else {
                document.getElementById('playvedio').style.display = document
                    .getElementById('playvedio').style.display == "none" ? "block"
                    : "none";
                $('#middleMap').removeClass('col-sm-8');
                $('#middleMap').addClass('col-sm-10');
            }
            //隐藏时间轴
            //	$("#timetools").hide();
        }
        //隐藏或显示视频窗口

    }

    //	document.getElementById("EleId").style.display = "none";
    //	 document.getElementById("EleId").style.display = "inline";
    //点击视频监控按钮 通道1 通道2 通道3 通道4
    $("#playVedio").click(function () {
        showdiv();
    });
</script>
<script>
    /* 		//视频监控相关(Live App)
     var ip = "505c456b.xstrive.top:8080";
     var host = window.location.host;
     ip = host;//
     //if ( host.indexOf('xstrive') == -1 ) {
     //	ip = host;
     //} */

    /* $("#vedio1").click(function(){
        window.location.href="www.baidu.com/";
    }) */
    // 通道一
    function vedio1() {
        //选择车牌号
        var taxiNums = $("#show1").val();
        //单一判断
        taxinum = taxiNums.split(",");
        if (taxiNums == "") {
            swal({
                title: "请先选择要监控的车辆",
                type: "warning"
            });
            return;
        } else if (taxiNums.length == 1) {
            swal({
                title: "该公司暂无车辆或车牌号格式错误",
                type: "warning"
            });
            return;
        } else if (taxiNums.length < 7) {
            swal({
                title: "车牌号格式有误",
                type: "warning"
            });
            return;
        } else if (taxiNums.length > 7) {
            //遍历数组
            swal({
                title: "只允许选中一辆车",
                type: "warning"
            });
            return;
        } else {
            //判断是否指定时间（目前先不判断）
            //ajax根据选中的车牌号获取指定ip的终端号(11位),拼接Url
            //后期根据车牌号来产生序列号进行跳转的时候使用到该方法。
            /*
            var url = "getIsuNumBycar";
            var args = {
            "taxiNum" : taxiNums
            };
              $.post(url, args, function(data) {
                //后台查车辆的终端号。
             var a=eval('('+data+')');
                 if(a!==null){
                     //后期根据车牌号拼接字符串，目前测试直接输入默认地址就可以。
                // window.location.href="http://"+序列号+".xstrive.com:8080";
                     window.location.href="http://defaultRRR.xstrive.com:8080";
                 }else{
                     swal({
                            title : "车牌号格式有误",
                            type : "warning"
                        });
                     return;
                 }

                //return;
            }); */
            // 实现source的加载
        }
    }

    //通道2
    function vedio2() {
        //选择车牌号
        var taxiNums = $("#show1").val();
        //单一判断
        taxinum = taxiNums.split(",");
        if (taxiNums == "") {
            swal({
                title: "请先选择要监控的车辆",
                type: "warning"
            });
            return;
        } else if (taxiNums.length == 1) {
            swal({
                title: "该公司暂无车辆或车牌号格式错误",
                type: "warning"
            });
            return;
        } else if (taxiNums.length < 7) {
            swal({
                title: "车牌号格式有误",
                type: "warning"
            });
            return;
        } else if (taxiNums.length > 7) {
            //遍历数组
            swal({
                title: "只允许选中一辆车",
                type: "warning"
            });
            return;
        } else {
            //判断是否指定时间（目前先不判断）
            //ajax根据选中的车牌号获取指定ip的终端号(11位),拼接Url
            //后期根据车牌号来产生序列号进行跳转的时候使用到该方法。
            /*
            var url = "getIsuNumBycar";
            var args = {
            "taxiNum" : taxiNums
            };
              $.post(url, args, function(data) {
                //后台查车辆的终端号。
             var a=eval('('+data+')');
                 if(a!==null){
                     //后期根据车牌号拼接字符串，目前测试直接输入默认地址就可以。
                // window.location.href="http://"+序列号+".xstrive.com:8080";
                     window.location.href="http://defaultRRR.xstrive.com:8080";
                 }else{
                     swal({
                            title : "车牌号格式有误",
                            type : "warning"
                        });
                     return;
                 }
                //return;
            }); */

            //实现跳转
            window.location.href = "http://defaultRRR.xstrive.com:8080";
        }
    }

    //通道3
    function vedio3() {
        //选择车牌号
        var taxiNums = $("#show1").val();
        //单一判断
        taxinum = taxiNums.split(",");
        if (taxiNums == "") {
            swal({
                title: "请先选择要监控的车辆",
                type: "warning"
            });
            return;
        } else if (taxiNums.length == 1) {
            swal({
                title: "该公司暂无车辆或车牌号格式错误",
                type: "warning"
            });
            return;
        } else if (taxiNums.length < 7) {
            swal({
                title: "车牌号格式有误",
                type: "warning"
            });
            return;
        } else if (taxiNums.length > 7) {
            //遍历数组
            swal({
                title: "只允许选中一辆车",
                type: "warning"
            });
            return;
        } else {
            //判断是否指定时间（目前先不判断）
            //ajax根据选中的车牌号获取指定ip的终端号(11位),拼接Url
            //后期根据车牌号来产生序列号进行跳转的时候使用到该方法。
            /*
            var url = "getIsuNumBycar";
            var args = {
            "taxiNum" : taxiNums
            };
              $.post(url, args, function(data) {
                //后台查车辆的终端号。
             var a=eval('('+data+')');
                 if(a!==null){
                     //后期根据车牌号拼接字符串，目前测试直接输入默认地址就可以。
                // window.location.href="http://"+序列号+".xstrive.com:8080";
                     window.location.href="http://defaultRRR.xstrive.com:8080";
                 }else{
                     swal({
                            title : "车牌号格式有误",
                            type : "warning"
                        });
                     return;
                 }

                //return;
            }); */

            //实现跳转
            window.location.href = "http://defaultRRR.xstrive.com:8080";
        }
    }

    //通道4
    function vedio4() {
        //选择车牌号
        var taxiNums = $("#show1").val();
        //单一判断
        taxinum = taxiNums.split(",");
        if (taxiNums == "") {
            swal({
                title: "请先选择要监控的车辆",
                type: "warning"
            });
            return;
        } else if (taxiNums.length == 1) {
            swal({
                title: "该公司暂无车辆或车牌号格式错误",
                type: "warning"
            });
            return;
        } else if (taxiNums.length < 7) {
            swal({
                title: "车牌号格式有误",
                type: "warning"
            });
            return;
        } else if (taxiNums.length > 7) {
            //遍历数组
            swal({
                title: "只允许选中一辆车",
                type: "warning"
            });
            return;
        } else {
            //判断是否指定时间（目前先不判断）
            //ajax根据选中的车牌号获取指定ip的终端号(11位),拼接Url
            //后期根据车牌号来产生序列号进行跳转的时候使用到该方法。
            /*
            var url = "getIsuNumBycar";
            var args = {
            "taxiNum" : taxiNums
            };
              $.post(url, args, function(data) {
                //后台查车辆的终端号。
             var a=eval('('+data+')');
                 if(a!==null){
                     //后期根据车牌号拼接字符串，目前测试直接输入默认地址就可以。
                // window.location.href="http://"+序列号+".xstrive.com:8080";
                     window.location.href="http://defaultRRR.xstrive.com:8080";
                 }else{
                     swal({
                            title : "车牌号格式有误",
                            type : "warning"
                        });
                     return;
                 }

                //return;
            }); */

            //实现跳转
            window.location.href = "http://defaultRRR.xstrive.com:8080";
        }
    }

    function reshow() {
        //选择车牌号
        var taxiNums = $("#show1").val();
        //单一判断
        taxinum = taxiNums.split(",");
        if (taxiNums == "") {
            swal({
                title: "请先选择要监控的车辆",
                type: "warning"
            });
            return;
        } else if (taxiNums.length == 1) {
            swal({
                title: "该公司暂无车辆或车牌号格式错误",
                type: "warning"
            });
            return;
        } else if (taxiNums.length < 7) {
            swal({
                title: "车牌号格式有误",
                type: "warning"
            });
            return;
        } else if (taxiNums.length > 7) {
            //遍历数组
            swal({
                title: "只允许选中一辆车",
                type: "warning"
            });
            return;
        } else {
            //判断是否指定时间（目前先不判断）
            //ajax根据选中的车牌号获取指定ip的终端号(11位),拼接Url
            //后期根据车牌号来产生序列号进行跳转的时候使用到该方法。
            /*
            var url = "getIsuNumBycar";
            var args = {
            "taxiNum" : taxiNums
            };
              $.post(url, args, function(data) {
                //后台查车辆的终端号。
             var a=eval('('+data+')');
                 if(a!==null){
                     //后期根据车牌号拼接字符串，目前测试直接输入默认地址就可以。
                // window.location.href="http://"+序列号+".xstrive.com:8080";
                     window.location.href="http://defaultRRR.xstrive.com:8080";
                 }else{
                     swal({
                            title : "车牌号格式有误",
                            type : "warning"
                        });
                     return;
                 }

                //return;
            }); */
            //实现跳转
            //window.location.href = "http://defaultRRR.xstrive.com:8080";
        }
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        function hideDiv() { //全屏观看时隐藏其他的div
            $('#carList').hide(); //左侧车辆列表
            $('#playvedio').hide(); //视频监控列表
            $('#mymenu1').hide(); //车辆信息
            $('#templist').hide(); //实时监控列表
            $('#playVedio').hide(); //视频监控按钮
        }

        function showDiv() { //退出全屏时显示被隐藏的div
            $('#carList').show();
            $('#playvedio').show();
            $('#mymenu1').show();
            $('#templist').show();
            $('#playVedio').show();
        }

        $("#fullScreen").click(function () {
            if (($("#fullScreen").val() == "全屏观看")) {
                hideDiv();
                //地图全屏
                if (($('#middleMap').hasClass('col-sm-10'))) {
                    $('#middleMap').removeClass('col-sm-10');
                    $('#middleMap').addClass('col-sm-12');
                } else if (($('#middleMap').hasClass('col-sm-8'))) {
                    $('#middleMap').removeClass('col-sm-8');
                    $('#middleMap').addClass('col-sm-12');
                }
                $("#fullScreen").val("退出全屏");
            } else {
                //退出全屏
                $('#middleMap').removeClass('col-sm-12');
                $('#middleMap').addClass('col-sm-8');
                $("#fullScreen").val("全屏观看");
                showDiv();
            }
        });

    });
    /*
     *alert($(window).height()); //浏览器当前窗口可视区域高度
     *alert($(document).height()); //浏览器当前窗口文档的高度
     *alert($(document.body).height());//浏览器当前窗口文档body的高度
     *alert($(document.body).outerHeight(true));//浏览器当前窗口文档body的总高度 包括border padding margin
     */
</script>
</body>
</html>
