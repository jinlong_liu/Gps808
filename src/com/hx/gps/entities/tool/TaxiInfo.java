package com.hx.gps.entities.tool;

public class TaxiInfo {
	private int taxiId;
	private String taxiNum;
	public int getTaxiId() {
		return taxiId;
	}
	public void setTaxiId(int taxiId) {
		this.taxiId = taxiId;
	}
	public String getTaxiNum() {
		return taxiNum;
	}
	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}
	public TaxiInfo(int taxiId, String taxiNum) {
		super();
		this.taxiId = taxiId;
		this.taxiNum = taxiNum;
	}
	public TaxiInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
