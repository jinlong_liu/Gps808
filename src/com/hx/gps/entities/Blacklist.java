package com.hx.gps.entities;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Blacklist entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "blacklist")
public class Blacklist implements java.io.Serializable {

	// Fields

	private Integer id;
	private String taxiNum;
	private Integer type;
	private String remark;
	private Timestamp time;

	// Constructors

	/** default constructor */
	public Blacklist() {
	}

	/** full constructor */
	public Blacklist(String taxiNum, Integer type, String remark, Timestamp time) {
		this.taxiNum = taxiNum;
		this.type = type;
		this.remark = remark;
		this.time = time;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "taxiNum")
	public String getTaxiNum() {
		return this.taxiNum;
	}

	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}

	@Column(name = "type")
	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "remark")
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "time", length = 19)
	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

}