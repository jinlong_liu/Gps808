package com.hx.gps.entities.tool;

import java.util.Date;

import com.bugull.mongo.SimpleEntity;
import com.bugull.mongo.annotations.Entity;
import com.hx.gps.util.FarmConstant;

/**
 * 待发送命令表 
 */
@Entity(name=FarmConstant.WAITORDER)
public class WaitOrder extends SimpleEntity {
	//添加时间（包含时分秒） 
	private Date time;
	//命令序号（流水号，每日更新）
	private int serialNum;
	//终端号（isu）
	private String isuNum;
	//命令类型
	private String orderType;
	//标识（命令类型）  
	private String identify;
	//发送人 
	private String people;
	//内容  
	private String context;
	//状态 
	private int state;
	//发送次数
	private int sendTime;
	
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public int getSerialNum() {
		return serialNum;
	}
	public void setSerialNum(int serialNum) {
		this.serialNum = serialNum;
	}
	public String getIsuNum() {
		return isuNum;
	}
	public void setIsuNum(String isuNum) {
		this.isuNum = isuNum;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getIdentify() {
		return identify;
	}
	public void setIdentify(String identify) {
		this.identify = identify;
	}
	public String getPeople() {
		return people;
	}
	public void setPeople(String people) {
		this.people = people;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public int getSendTime() {
		return sendTime;
	}
	public void setSendTime(int sendTime) {
		this.sendTime = sendTime;
	}
	@Override
	public String toString() {
		return "WaitOrder [time=" + time + ", serialNum=" + serialNum
				+ ", isuNum=" + isuNum + ", orderType=" + orderType
				+ ", identify=" + identify + ", people=" + people
				+ ", context=" + context + ", state=" + state + ", sendTime="
				+ sendTime + "]";
	}
}
