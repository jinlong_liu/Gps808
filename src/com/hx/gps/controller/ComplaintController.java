package com.hx.gps.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.jws.WebParam.Mode;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hx.gps.entities.Asscycle;
import com.hx.gps.entities.Complaints;
import com.hx.gps.entities.Comptype;
import com.hx.gps.entities.Deduct;
import com.hx.gps.entities.Driver;
import com.hx.gps.entities.Evalscore;
import com.hx.gps.entities.Scorerank;
import com.hx.gps.entities.tool.DriInfo;
import com.hx.gps.entities.tool.EvalVector;
import com.hx.gps.service.AssessService;
import com.hx.gps.service.TaxiService;
import com.hx.gps.util.DateUtil;

@Controller
public class ComplaintController {
	@Autowired
	private AssessService assessService;
	@Autowired
	private DateUtil dateUtil;
	@Autowired
	private TaxiService taxiService;
	private static Log logger = LogFactory.getLog(ComplaintController.class);

	// 进入用户举报界面
	@RequestMapping("/intoAddComplaint")
	public String getAllTaxiNum(Map<String, Object> map) {
		map.put("taxis", taxiService.getAllTaxi());
		// 查找举报类型
		map.put("compTypes", assessService.getCompType());
		// 查找待审核
		map.put("complaint", assessService.getCompByType(1));
		return "WEB-INF/views/complaint/addCompManage";
	}

	// 客户举报
	/**
	 * @param session
	 * @return
	 */
	@RequestMapping("/userAddComplaint")
	public String AddComplaint(HttpServletRequest request) {
		// session 来添加属性，防止投诉信息添加完毕转发后参数消失
		HttpSession session = request.getSession();
		// 获取所有车辆
		session.setAttribute("taxis", taxiService.getAllTaxi());
		// 查找举报类型
		session.setAttribute("compTypes", assessService.getCompType());
		// 查找待审核
		session.setAttribute("complaint", assessService.getCompByType(1));
		return "WEB-INF/views/complaint/addComplaint";
	}

	// 添加或修改投诉
	@RequestMapping("/addComplaint2")
	public String addComplaint2(HttpServletResponse response, Integer driver1, Integer taxi1, Complaints complaint,
			Integer reportType1, HttpServletRequest request) {
		String state = assessService.addComplaint(complaint, driver1, taxi1, reportType1);
		request.setAttribute("result", state);

		return "WEB-INF/views/complaint/addComplaint";
	}

	// 根据车牌号查询司机
	@RequestMapping("/getDriverByTaxi")
	public void getDriverByTaxi(Integer id, HttpServletResponse response) {
		List<Driver> drivers = taxiService.getDriverOfTaxi(id);
		List<DriInfo> dris = new ArrayList<>();
		for (Driver d : drivers) {
			dris.add(new DriInfo(d.getId(), d.getDname()));
		}
		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().print(mapper.writeValueAsString(dris));
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("根据车牌号查询司机出现异常" + e.getMessage());
		}
	}

	// 添加或修改投诉
	@RequestMapping("/addComplaint")
	public String addComplaint(Integer driver1, Integer taxi1, Complaints complaint, Integer reportType1,
			HttpServletRequest request) {
		String state = assessService.addComplaint(complaint, driver1, taxi1, reportType1);
		request.setAttribute("result", state);
		return "forward:intoAddComplaint";
	}

	// 删除未审核的投诉信息
	/*
	 * @RequestMapping("/deleteComp") public void daleteComp(HttpServletResponse
	 * response, Integer id) { boolean b = assessService.deleteComById(id);
	 * String result; if (b) { result = "操作执行成功"; logger.info("操作执行成功"); } else
	 * { result = "操作执行失败"; logger.info("操作执行失败"); }
	 * response.setContentType("text/html;charset=UTF-8");
	 * response.setCharacterEncoding("UTF-8"); try {
	 * 
	 * response.getWriter().print(result); } catch (IOException e) {
	 * e.printStackTrace(); logger.error("删除未审核的投诉信息出现异常" + e.getMessage()); } }
	 */
	@RequestMapping("/deleteComp")
	public String daleteComp(Model model, Integer id) {
		boolean b = assessService.deleteComById(id);
		String result = null;
		if (b) {
			result = "[删除投诉]操作执行成功";
			logger.info("操作执行成功");
		} else {
			result = "[删除投诉]操作执行失败";
			logger.info("操作执行失败");
		}
		model.addAttribute("result", result);
		return "forward:/intoAddComplaint";

	}

	// 查看待审核的投诉
	@RequestMapping("/toAssessOfComp")
	public String toAssessOfComp(Map<String, Object> map) {
		List<Complaints> complaint = assessService.getCompByType(1);
		map.put("complaint", complaint);
		return "WEB-INF/views/complaint/assessOfComp";
	}

	// 审核状态，使用ajax
	/*
	 * @RequestMapping("/alterTypeOfAssess") public void
	 * alterTypeOfAssess(Integer id, Integer state, HttpServletResponse
	 * response) { // 定义初始状态 Integer initType = 1; boolean s =
	 * assessService.alterTypeOfAssess(id, state, initType); String result; if
	 * (s) { result = "操作执行成功"; logger.info("【投诉审核】操作执行成功"); } else { result =
	 * "操作执行失败"; logger.info("【投诉审核】操作执行失败"); } // ObjectMapper mapper = new
	 * ObjectMapper(); // mapper.writeValueAsString(s)
	 * response.setContentType("text/html;charset=UTF-8");
	 * response.setCharacterEncoding("UTF-8"); try {
	 * response.getWriter().print(result); } catch (IOException e) {
	 * e.printStackTrace(); logger.error("修改审核状态出现异常" + e.getMessage()); } }
	 */
	// 投诉审核（表单提交）

	@RequestMapping("/alterTypeOfAssess")
	public String alterTypeOfAssess(Integer id, Integer state, Model model) {
		// 定义初始状态
		Integer initType = 1;
		boolean s = assessService.alterTypeOfAssess(id, state, initType);
		String result = null;
		if (s) {
			result = "操作执行成功";
			logger.info("【投诉审核】操作执行成功");
		} else {
			result = "操作执行失败";
			logger.info("【投诉审核】操作执行失败");
		}
		model.addAttribute("result", result);
		return "forward:/toAssessOfComp";
	}

	// 投诉处理，查询未处理的投诉相关信息,
	// 该处已进行修改，原先方法为只查询待处理的，现在还返回处理为通过的
	@RequestMapping("/getDealOfComp")
	public String getDealOfComp(Map<String, Object> map) {
		List<Complaints> complaint = new ArrayList<>();
		if(assessService.getCompByType(2)!=null)
		complaint.addAll(assessService.getCompByType(2));
		if(assessService.getCompByType(5)!=null)
		complaint.addAll(assessService.getCompByType(5));
		map.put("complaint", complaint);
		return "WEB-INF/views/complaint/dealOfComp";
	}

	// 重新处理，即把一条投诉信息的状态由5转换为3，且删除审批意见
	@RequestMapping("/updateType522")
	public String updateType523(Integer id) {
		assessService.alterTypeOfAssess(id, 2, 5);
		logger.info("重新处理投诉信息状态，删除审批意见(对应id)" + id);
		return "redirect:getDealOfComp";
	}

	// 执行处理
	@RequestMapping("/addDeal")
	public void addDeal(Deduct deduct, HttpServletResponse response) throws IOException {
		// 首先先修改投诉的状态
		// .out.println("执行处理");
		logger.info("执行处理");
		// System.out.println(deduct.toString());
		logger.info(deduct.toString());
		if (deduct.getCompId() == null) {
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().print("处理失败，请重试");
		} else {
			// 修改投诉状态
			boolean s = assessService.alterTypeOfAssess(deduct.getCompId(), 3, 2);
			String result;
			if (!s) {
				result = "投诉处理失败";
				logger.info("修改投诉状态失败(对应id)" + deduct.getCompId());
			} else if (assessService.addDeduct(deduct)) {
				result = "操作执行成功";
				logger.info("修改投诉状态成功(对应id)" + deduct.getCompId());
			} else {
				result = "保存处理方案失败";
				logger.info("保存处理方案失败(对应id)" + deduct.getCompId());

			}
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			try {
				response.getWriter().print(result);
			} catch (IOException e) {
				e.printStackTrace();
				result = "出现格式异常";
				logger.error("【投诉状态】格式异常" + e.getMessage());

			} catch (Exception e) {
				result = "处理失败";
				response.getWriter().print(result);
				// System.out.println("出现异常情况");
				logger.error("【投诉状态】未知异常" + e.getMessage());
			}
		}
	}

	// 首先获取待批准的投诉信息,进入投诉审批界面
	@RequestMapping("/intolCompApprova")
	public String intolCompApprova(Map<String, Object> map) {
			map.put("complaint", assessService.getCompByType(3));
		return "WEB-INF/views/complaint/approvalOfComp";
	}

	/*
	 * // 投诉审批,仍然使用ajax
	 * 
	 * @RequestMapping("/approvalComp") public void approvalComp(Integer id,
	 * Integer state, String apprOpinion, HttpServletResponse response) { String
	 * result = null; Boolean b; // System.out.println("当前审核状态:"+state);
	 * logger.info("当前审核状态:" + state); if (state == 1) { // 审批通过 b =
	 * assessService.approvalComp(id, 4, null); logger.info("开始审批并处理"); //
	 * System.out.println("开始审批并处理"); } else { // 审批不通过 b =
	 * assessService.approvalComp(id, 5, apprOpinion); logger.info("投诉审批不通过，id"
	 * + id); } if (b) { logger.info("投诉审批通过，id" + id); result = "操作执行成功"; }
	 * else { logger.info("投诉审批失败，id" + id); result = "操作执行失败"; }
	 * response.setContentType("text/html;charset=UTF-8");
	 * response.setCharacterEncoding("UTF-8"); try {
	 * response.getWriter().print(result); } catch (IOException e) {
	 * e.printStackTrace(); // System.out.println("审批出现异常");
	 * logger.error("投诉审批过程出现异常" + e.getMessage()); } }
	 */

	@RequestMapping("/approvalComp")
	public String approvalComp(Integer id, Integer state, String apprOpinion, Model model) {
		String result = null;
		Boolean b;
		logger.info("【投诉审批】" + id);
		// System.out.println("当前审核状态:"+state);
		if (state == 1) { // 审批通过
			b = assessService.approvalComp(id, 4, null);
			// System.out.println("开始审批并处理");
		} else { // 审批不通过
			b = assessService.approvalComp(id, 5, apprOpinion);
			logger.info("投诉审批不通过，id" + id);
		}
		if (b) {
			logger.info("投诉审批通过，id" + id);
			result = "操作执行成功";
		} else {
			logger.info("投诉审批失败，id" + id);
			result = "操作执行失败";
		}
		model.addAttribute("result", result);
		return "forward:/intolCompApprova";

	}

	// 添加投诉类型
	@RequestMapping("/addCompType")
	public String addCompType(Comptype compType, RedirectAttributes attributes) {
		// System.out.println(compType);
		String result = null;
		try {
			assessService.addOrUpdateCompType(compType);
			result = "操作执行成功";
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("【添加或更新投诉类型】出现异常" + e.getMessage());
			result = "操作失败";
			attributes.addFlashAttribute("result", result);
			return "redirect:/getAllCompType";
		}
		logger.info("添加或更新投诉类型:" + compType.getType());
		attributes.addFlashAttribute("result", result);
		return "redirect:/getAllCompType";
	}

	// 查看所有投诉类型
	@RequestMapping("/getAllCompType")
	public String getAllCompType(Map<String, Object> map) {
		map.put("compType", assessService.getCompType());
		return "WEB-INF/views/complaint/compType";
	}

	// 删除指定投诉类型(ajax版本)
	/*
	 * @RequestMapping("delectCompType") public void delectCompType(Integer id,
	 * HttpServletResponse response) { String result; if
	 * (assessService.deleteCompType(id)) { logger.info("删除投诉类型成功，id" + id);
	 * result = "操作执行成功"; } else { logger.info("投诉类型删除失败，id" + id + "原因：正在使用");
	 * result = "该类型正在被使用，不允许删除"; }
	 * response.setContentType("text/html;charset=UTF-8");
	 * response.setCharacterEncoding("UTF-8"); try {
	 * response.getWriter().print(result); } catch (Exception e) {
	 * e.printStackTrace(); logger.error("投诉信息删除过程出现异常" + e.getMessage()); } }
	 */
	@RequestMapping("delectCompType")
	public String delectCompType(Integer id, Model model) {
		String result = null;
		if (assessService.deleteCompType(id)) {
			logger.info("删除投诉类型成功，id" + id);
			result = "删除成功";
		} else {
			logger.info("投诉类型删除失败，id" + id + "原因：正在使用");
			result = "该类型正在被使用，不允许删除";
		}
		model.addAttribute("result", result);
		return "forward:/getAllCompType";
	}

	// 显示考核周期
	@RequestMapping("/getAsscycle")
	public String getAsscycle(Map<String, Object> map) {
		map.put("asscycle", assessService.getAsscycle());
		return "WEB-INF/views/complaint/asscycle";
	}

	// 添加或更新考核周期
	@RequestMapping("/updateAsscycle")
	public String updateAsscycle(Asscycle asscycle, RedirectAttributes attr) {
		// System.out.println(asscycle);
		String result = null;
		try {
			assessService.updateAssCycle(asscycle);
			result = "操作执行成功";
			logger.info("【添加或更新考核周期】操作执行成功");
		} catch (Exception e) {
			// e.printStackTrace();
			result = "操作失败";
			logger.error("【添加或更新考核周期】出现异常" + e.getMessage());
			attr.addFlashAttribute("result", result);
			return "redirect:/getAsscycle";
		}
		attr.addFlashAttribute("result", result);
		return "redirect:/getAsscycle";
	}

	// 删除考核周期
	@RequestMapping("/deleteAsscycle")
	public String deleteAsscycle(Integer id, Model model) {
		// System.out.println("删除" + id);
		logger.info("删除考核周期" + id);
		String result = null;
		try {
			assessService.deleteAsscycleById(id);
			result = "删除成功";
			logger.info("删除成功");
		} catch (Exception e1) {
			logger.error("【删除考核周期】出现异常" + e1.getMessage());
			result = "删除失败";
			model.addAttribute("result", result);
			return "forward:/getAsscycle";
		}
		model.addAttribute("result", result);
		return "forward:/getAsscycle";

	}

	// 返回分数等级对应表的信息
	@RequestMapping("/getScorerank")
	public String getScorerank(Map<String, Object> map) {
		map.put("scoreRank", assessService.getScorerank());
		return "WEB-INF/views/complaint/scoreRank";
	}

	// 考核等级设置，添加分数、等级对应信息
	@RequestMapping("/addOrUpdateScorerank")
	public String addOrUpdateScorerank(Scorerank scorerank, RedirectAttributes attr) {
		String result = null;
		try {
			assessService.addOrUpScorerank(scorerank);
			logger.info("【新增或更新考核等级】" + scorerank.toString());
			result = "操作执行成功";
		} catch (Exception e) {
			result = "操作失败";
			logger.error("【新增或更新考核等级】异常" + e.getMessage());
			attr.addFlashAttribute("result", result);
			return "redirect:/getScorerank";
			// e.printStackTrace();
		}
		attr.addFlashAttribute("result", result);
		return "redirect:/getScorerank";
	}

	// 删除分数等级对应信息,使用ajax
	@RequestMapping("/delectScorerank")
	public String delectScorerank(Integer id, Model model) {
		logger.info("【分数等级】删除：" + id);
		String result = null;
		try {
			assessService.deleteScorerank(id);
			result = "删除成功";
			logger.info("【分数等级】删除成功");
		} catch (Exception e1) {
			result = "删除失败";
			logger.info("【分数等级】删除出现异常" + e1.getMessage());
			model.addAttribute("result", result);
			return "forward:/getScorerank";
		}
		model.addAttribute("result", result);
		return "forward:/getScorerank";
	}

	// 评价分数对应表
	@RequestMapping("/getEvalScore")
	public String getEvalScore(Map<String, Object> map) {
		// System.out.println("输出测试" + assessService.getEvalscore());
		logger.info("输出测试" + assessService.getEvalscore());
		map.put("evalScore", assessService.getEvalscore());
		return "WEB-INF/views/complaint/evalScore";
	}

	// 更新或添加评价分数对应信息
	@RequestMapping("/addOrUpdateEvalScore")
	public String addOrUpdateEvalScore(Evalscore evalscore, RedirectAttributes attr) {
		String result = null;
		logger.info("[新增或修改评价等级]" + evalscore.toString());
		try {
			assessService.addOrUpdateEval(evalscore);
			logger.info("[新增或修改评价等级]:成功");
			result = "操作执行成功";
		} catch (Exception e) {
			logger.error("[新增或修改评价等级]：出现异常" + e.getMessage());
			result = "操作失败";
			attr.addFlashAttribute("result", result);
			return "redirect:getEvalScore";
		}
		attr.addFlashAttribute("result", result);
		return "redirect:getEvalScore";
	}

	/*
	 * (老版本) 删除评价分数对应记录
	 * 
	 * @RequestMapping("/deleteEvalScore") public void deleteEvalScore(Integer
	 * id, HttpServletResponse response ) { logger.info("【评价分数】删除：" + id);
	 * String result = null; response.setContentType("text/html;charset=UTF-8");
	 * response.setCharacterEncoding("UTF-8"); try {
	 * assessService.delectEval(id); result = "删除成功"; logger.info("【评价分数】删除成功");
	 * } catch (Exception e1) { result = "删除失败"; logger.info("【评价分数】删除出现异常" +
	 * e1.getMessage()); try { response.getWriter().print(result); } catch
	 * (IOException e) { logger.error("删除评价分数Io异常" + e.getMessage()); } } try {
	 * response.getWriter().print(result); } catch (IOException e) {
	 * logger.error("删除评价分数Io异常" + e.getMessage()); } }
	 */
	// 删除评价分数对应记录
	@RequestMapping("/deleteEvalScore")
	public String deleteEvalScore(Integer id, Model model) {
		logger.info("【评价分数】删除：" + id);
		String result = null;
		try {
			assessService.delectEval(id);
			result = "删除成功";
			logger.info("【评价分数】删除成功");
		} catch (Exception e1) {
			result = "删除失败";
			logger.info("【评价分数】删除出现异常" + e1.getMessage());
			model.addAttribute("result", result);
			return "forward:/getEvalScore";
		}
		model.addAttribute("result", result);
		return "forward:/getEvalScore";
	}

	// 查看当期考核
	@RequestMapping("/getCurrAssess")
	public String getCurrAssess(Map<String, Object> map) {
		// System.out.println("当前考核开始");
		logger.info("当前考核开始");
		if (assessService.getAsscycle()!= null) {
			List<EvalVector> evals = assessService.queryCurrAssess();
			// System.out.println("最后结果：" + evals);
			logger.info("最后结果：" + evals);
			map.put("evals", evals);
			return "WEB-INF/views/complaint/currAssess";
		}

		else {
			return "WEB-INF/views/complaint/currAssess?result='请先添加考核周期'";
		}
	}

	// 显示所有刚添加的投诉信息，type=1
	@RequestMapping("/getComplaintBy1")
	public String getComplaintBy1(Map<String, Object> map) {
		map.put("complaint", assessService.getCompByType(1));
		return "WEB-INF/views/complaint/addCompManage";
	}
	//投诉档案
	@RequestMapping("/getArchiveOfComp")
	public String getAllarchive(Map<String,Object> map){
		//保存投诉信息
		List<Complaints> complaint = new ArrayList<>();
		//获取所有投诉
		try {
			complaint=assessService.getAllCompliant();
		} catch (Exception e) {
			logger.info("获取投诉档案出错"+e.getMessage());
			return "WEB-INF/views/complaint/archiveOfComp";
		}
		map.put("complaints", complaint);
		return "WEB-INF/views/complaint/archiveOfComp";
	}
}

 
