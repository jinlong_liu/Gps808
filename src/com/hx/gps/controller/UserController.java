/**
 * 
 */
package com.hx.gps.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.ExpiredCredentialsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hx.gps.entities.User;
import com.hx.gps.service.UserService;

/**
 * @author Minazuki
 *
 */
@Controller
public class UserController {
	private static final String User = null;
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView loginPage(HttpServletRequest req) {
		ModelAndView mav = new ModelAndView();
		HttpSession httpSession = req.getSession();
		if (httpSession.getAttribute("isLogin") == null) {
			mav.setViewName("login");
			mav.addObject("page", "page");
		} else {
			mav.setViewName("redirect:/");
		}
		return mav;
	}

	@RequestMapping("/tt3")
	public void Test222(){
		User principal = (com.hx.gps.entities.User) SecurityUtils.getSubject().getPrincipal();
		System.out.println(principal.getName());
	}

	
	
	

	/**
	 * shiro登录
	 * 
	 * @param account
	 * @param password
	 * @param req
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView loginshiro(String account, String password, HttpServletRequest req, ModelAndView model) {
		UsernamePasswordToken token = new UsernamePasswordToken(account, password);
		Subject subject = SecurityUtils.getSubject();
		try {
			subject.login(token);
			if (subject.isAuthenticated()) {// 认证成功
				User user = userService.findUserByName(account);
				subject.getSession().setAttribute("rolename", user.getRole().getRemark());
				subject.getSession().setAttribute("userId", user.getId());
				System.out.println("登陆成功");
				model.setViewName("redirect:/");
				//检索cookie
	/*			if(true){
					//移除cookie
					Cookie cookies[] = req.getCookies();  
				      if (cookies != null)  
				      {  
				          for (int i = 0; i < cookies.length; i++)  
				          {  
				        	  System.out.println(cookies[i].getName()+","+cookies[i].getValue());
				              if (cookies[i].getName().equals("nick"))  
				              {  
				                  Cookie cookie = new Cookie("nick","ww");//这边得用"",不能用null  
				                  cookie.setPath("/");//设置成跟写入cookies一样的  
				                 // cookie.setDomain(".wangwz.com");//设置成跟写入cookies一样的  
				                  cookie.setMaxAge(0);  
				                  response.addCookie(cookie);  
				              }  
				          }  
				      }  
				}*/
			} else {
				model.setViewName("login");
				model.addObject("page", "error");
				System.out.println("登录失败");
			}
		} catch (IncorrectCredentialsException e) {
			System.out.println("登录密码错误->" + token.getPrincipal());
			// 登录密码错误
			model.setViewName("login");
			model.addObject("page", "error");
		} catch (ExcessiveAttemptsException e) {
			// msg = "登录失败次数过多";
			System.out.println("登录失败次数过多->" + token.getPrincipal());
		} catch (LockedAccountException e) {
			// msg = "帐号已被锁定. The account for username " + token.getPrincipal()
			System.out.println("账号已被锁定->" + token.getPrincipal());
		} catch (DisabledAccountException e) {
			// msg = "帐号已被禁用.
			System.out.println("当前用户已被禁用->" + token.getPrincipal());
			model.setViewName("login");
			model.addObject("state", "forbid");
		} catch (ExpiredCredentialsException e) {
			// msg = "帐号已过期.
			System.out.println("账号已过期->" + token.getPrincipal());
		} catch (UnknownAccountException e) {
			// msg = "帐号不存在.;
			System.out.println("账号不存在->" + token.getPrincipal());
			model.setViewName("login");
			model.addObject("page", "error");
		} catch (UnauthorizedException e) {
			System.out.println("无授权->" + token.getPrincipal());
			model.setViewName("login");
			model.addObject("state", "author");
			// msg = "您没有得到相应的授权！" + e.getMessage();
		} catch (AuthenticationException e) {
			System.out.println("登录频繁->" + token.getPrincipal());
			model.setViewName("login");
			model.addObject("state", "retry");
		}catch (Exception e) {
			System.out.println("登录失败->" + token.getPrincipal());
			model.setViewName("login");
			model.addObject("page", "error");
		}

		return model;
	}
}
