/**
 * 
 */
package com.hx.gps.entities.tool;

import com.hx.gps.util.DateUtil;
import com.mongodb.BasicDBObject;

/**
 * @Author: WK
 * @Title:
 * @Description:
 *
 */
public class TerminalState {
	
	String isuNum;
	String taxiNum;
	String state;
	String time;

	public TerminalState(String isuNum, String taxiNum, String state, String time) {
		super();
		this.isuNum = isuNum;
		this.taxiNum = taxiNum;
		this.state = state;
		this.time = time;
	}
	
	public TerminalState() {
	}

	public TerminalState(BasicDBObject finds) {
		super();
		this.isuNum = finds.getString("isuNum");
		this.state = finds.getString("state"); 
		this.taxiNum=finds.getString("taxiNum");
		this.time = DateUtil.dateToString(finds.getDate("time"));
	}


	public String getIsuNum() {
		return isuNum;
	}

	public void setIsuNum(String isuNum) {
		this.isuNum = isuNum;
	}

	public String getTaxiNum() {
		return taxiNum;
	}

	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "{isuNum:'" + isuNum + "', taxiNum:'" + taxiNum + "', state:'" + state + "', time:'" + time + "}";
	}
}


