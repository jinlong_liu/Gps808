package com.hx.gps.util;

import java.util.Date;

/**
 * 把时间戳转换为几秒前、几分钟前、几小时前或几天前
 * @author lkyskr
 *
 */
public class RelativeDateFormat2 {
	
	 	private static final long ONE_MINUTE = 60000L;
	    private static final long ONE_HOUR = 3600000L;
	    private static final long ONE_DAY = 86400000L;
	    private static final long ONE_WEEK = 604800000L;
	 
	    private static final String ONE_HOUR_IN = "小时内";
	    private static final String ONE_HOUR_AGO = "小时前";
	    private static final String ONE_DAY_AGO = "天前";
	   /* public static void main(String[] args) throws ParseException {
	        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:m:s");
	        Date date = format.parse("2013-11-11 18:35:35");
	        System.out.println(format(date));
	    }*/
	 
	    public static String format(Date date) {
	        long delta = new Date().getTime() - date.getTime();
//	        if (delta <=60L * ONE_MINUTE) {
//	            long seconds = toSeconds(delta);
//	            return (seconds/60/60) + "内";
//	        }
//	        if (delta > 60L * ONE_MINUTE&&delta<=60L * ONE_MINUTE*24) {
//	            long minutes = toMinutes(delta);
//	            return (minutes/60) + "小时前";
//	        }
//	        if (delta < 24L * ONE_HOUR) {
//	            long hours = toHours(delta);
//	            return (hours <= 0 ? 1 : hours) + ONE_HOUR_AGO;
//	        }else{
//	        	long hours = toHours(delta);
//		        return hours + ONE_HOUR_AGO;
//	        }
	        if(delta>0){
	        	 long hours = toHours(delta);
	        	 return hours/24 + ONE_DAY_AGO;
	        }else{
	        	return "数据有误";
	        }
	    }
	 
	    private static long toSeconds(long date) {
	        return date / 1000L;
	    }
	 
	    private static long toMinutes(long date) {
	        return toSeconds(date) / 60L;
	    }
	 
	    private static long toHours(long date) {
	        return toMinutes(date) / 60L;
	    }
	 
	    private static long toDays(long date) {
	        return toHours(date) / 24L;
	    }
	 
	    private static long toMonths(long date) {
	        return toDays(date) / 30L;
	    }
	 
	    private static long toYears(long date) {
	        return toMonths(date) / 365L;
	    }
	 

}
