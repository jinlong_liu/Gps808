package com.hx.gps.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hx.gps.entities.Company;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.tool.ComInfo2;
import com.hx.gps.entities.tool.MyPassVol;
import com.hx.gps.entities.tool.TaxiInfo2;
import com.hx.gps.service.InfoService;
import com.hx.gps.util.CalendarUtil;

@Controller
public class sta2 {

	@Autowired
	private InfoService infoService;
	// 日志记录
	private static Log logger = LogFactory.getLog(sta2.class);

	// 进入"运力分析"界面
	@RequestMapping("/enterTranCap")
	public String enterTranCap(Map<String, Object> map) {
		logger.info("进入运力分析界面");
		// List<Company> companies = infoService.getAllCompany();
		// ------------- ---------------------
		Subject subject = SecurityUtils.getSubject();
		// Session session = subject.getSession();

		List<Company> companies = new ArrayList<Company>();
		if (subject.isPermitted("allcompy:get")) {
			companies = infoService.getAllCompany();
		} else if (subject.isPermitted("bjcompy:get")) {
			companies = infoService.getAllCompanyBj();
		} else {
			String name = (String) subject.getPrincipal();

			// int id=(int)session.getAttribute("compyid");
			Company company = infoService.getComByUser(name);
			// System.out.println("公司id"+);
			companies.add(company);
		}

		// --------------------------------------
		List<ComInfo2> comInfo2s = new ArrayList<>();
		for (Company c : companies) {
			Set<Taxi> taxis = c.getTaxis();
			Set<TaxiInfo2> taxis2 = new HashSet<>();

			Iterator<Taxi> it = taxis.iterator();
			while (it.hasNext()) {
				Taxi next = it.next();
				TaxiInfo2 t2 = new TaxiInfo2(next.getId(), next.getTaxiNum());
				taxis2.add(t2);
			}
			comInfo2s.add(new ComInfo2(c.getId(), c.getName(), taxis2));
		}

		// System.out.println("comInfo2s: " + comInfo2s);
		logger.info("comInfo2s: " + comInfo2s);
		map.put("comInfo2s", comInfo2s);
		return "WEB-INF/views/statement/zhexian_yunli";
	}

	// axjx查询客运量
	@RequestMapping("/getTranCapByAjax")
	public void getTranCapByAjax(HttpServletResponse response, HttpServletRequest request) throws ParseException {

		String startTime = request.getParameter("startTime").substring(0, 10);
		String endTime = request.getParameter("endTime").substring(0, 10);
		String cars = request.getParameter("cars");
		// System.out.println("cars: " + cars);
		logger.info("查询客运量,cars: " + cars);

		// 获取x轴数据

		// x轴为日
		List<String> dates = new ArrayList<>();
		dates = CalendarUtil.getDays(startTime, endTime);

		// 模拟从数据库查出来的客运量数据
		List<MyPassVol> passVols = new ArrayList<>();
		for (int i = 0; i < dates.size(); i++) {
			passVols.add(new MyPassVol(dates.get(i), 100 + i * 10));
		}

		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		try {
			logger.info("取到营运信息的数据 " + passVols);
			response.getWriter().print(mapper.writeValueAsString(passVols));
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("获取 营运信息数据时出现异常" + e.getMessage());
		}

	}

}
