package com.hx.gps.entities;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Insure entity. @author MyEclipse Persistence Tools
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "insure" )
public class Insure implements java.io.Serializable {

	// Fields

	private Integer id;
	private String master;
	private String category;
	private String type;
	private Timestamp payTime;
	private Float payAmount;
	private Timestamp validityStart;
	private Timestamp validityEnd;
	private String remark;
	private String people;
	private Timestamp time;

	// Constructors

	/** default constructor */
	public Insure() {
	}

	/** full constructor */
	public Insure(String master, String category, String type,
			Timestamp payTime, Float payAmount, Timestamp validityStart,
			Timestamp validityEnd, String remark, String people, Timestamp time) {
		this.master = master;
		this.category = category;
		this.type = type;
		this.payTime = payTime;
		this.payAmount = payAmount;
		this.validityStart = validityStart;
		this.validityEnd = validityEnd;
		this.remark = remark;
		this.people = people;
		this.time = time;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "master")
	public String getMaster() {
		return this.master;
	}

	public void setMaster(String master) {
		this.master = master;
	}

	@Column(name = "category")
	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Column(name = "type")
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "payTime", length = 19)
	public Timestamp getPayTime() {
		return this.payTime;
	}

	public void setPayTime(Timestamp payTime) {
		this.payTime = payTime;
	}

	@Column(name = "payAmount", precision = 12, scale = 0)
	public Float getPayAmount() {
		return this.payAmount;
	}

	public void setPayAmount(Float payAmount) {
		this.payAmount = payAmount;
	}

	@Column(name = "validityStart", length = 19)
	public Timestamp getValidityStart() {
		return this.validityStart;
	}

	public void setValidityStart(Timestamp validityStart) {
		this.validityStart = validityStart;
	}

	@Column(name = "validityEnd", length = 19)
	public Timestamp getValidityEnd() {
		return this.validityEnd;
	}

	public void setValidityEnd(Timestamp validityEnd) {
		this.validityEnd = validityEnd;
	}

	@Column(name = "remark")
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "people")
	public String getPeople() {
		return this.people;
	}

	public void setPeople(String people) {
		this.people = people;
	}

	@Column(name = "time", length = 19)
	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

}