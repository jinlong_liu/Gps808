package com.hx.gps.entities;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Accident entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "accident")
public class Accident implements java.io.Serializable {

	// Fields

	private Integer id;
	private String accidentCode;
	private Accidenttype accidenttype;
	private Driver driver;
	private String duty;
	private Taxi taxi;
	private Timestamp accidentDate;
	private String accidentPlace;
	private String accidentDsc;
	private String isLoss;
	private String moneyLoss;
	private String moneyProg;
	private String driverPhone;
	private String checkName;
	private String bossReply;

	// Constructors

	/** default constructor */
	public Accident() {
	}

	/** full constructor */
	public Accident(Integer id, String accidentCode, Accidenttype accidenttype, Driver driver, String duty,
			Taxi taxi, Timestamp accidentDate, String accidentPlace, String accidentDsc, String isLoss,
			String moneyLoss, String moneyProg, String driverPhone, String checkName, String bossReply) {
		super();
		this.id = id;
		this.accidentCode = accidentCode;
		this.accidenttype = accidenttype;
		this.driver = driver;
		this.duty = duty;
		this.taxi = taxi;
		this.accidentDate = accidentDate;
		this.accidentPlace = accidentPlace;
		this.accidentDsc = accidentDsc;
		this.isLoss = isLoss;
		this.moneyLoss = moneyLoss;
		this.moneyProg = moneyProg;
		this.driverPhone = driverPhone;
		this.checkName = checkName;
		this.bossReply = bossReply;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
     
	@ManyToOne(fetch = FetchType.EAGER )
	@JoinColumn(name = "fk_type_id")
	public Accidenttype getAccidenttype() {
		return this.accidenttype;
	}

	public void setAccidenttype(Accidenttype accidenttype) {
		this.accidenttype = accidenttype;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_driver_id")
	public Driver getDriver() {
		return this.driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	@Column(name = "duty")
	public String getDuty() {
		return this.duty;
	}

	public void setDuty(String duty) {
		this.duty = duty;
	}
	@Column(name = "accident_code")
	public String getAccidentCode() {
		return accidentCode;
	}

	public void setAccidentCode(String accidentCode) {
		this.accidentCode = accidentCode;
	}
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_taxi_id")
	public Taxi getTaxi() {
		return this.taxi;
	}
	@JoinColumn(name = "taxi")
	public void setTaxi(Taxi taxi) {
		this.taxi = taxi;
	}

	@Column(name = "accidentDate", length = 19)
	public Timestamp getAccidentDate() {
		return this.accidentDate;
	}

	public void setAccidentDate(Timestamp accidentDate) {
		this.accidentDate = accidentDate;
	}

	@Column(name = "accidentPlace")
	public String getAccidentPlace() {
		return this.accidentPlace;
	}

	public void setAccidentPlace(String accidentPlace) {
		this.accidentPlace = accidentPlace;
	}

	@Column(name = "accidentDsc")
	public String getAccidentDsc() {
		return this.accidentDsc;
	}

	public void setAccidentDsc(String accidentDsc) {
		this.accidentDsc = accidentDsc;
	}

	@Column(name = "isLoss")
	public String getIsLoss() {
		return this.isLoss;
	}

	public void setIsLoss(String isLoss) {
		this.isLoss = isLoss;
	}

	@Column(name = "moneyLoss")
	public String getMoneyLoss() {
		return this.moneyLoss;
	}

	public void setMoneyLoss(String moneyLoss) {
		this.moneyLoss = moneyLoss;
	}

	@Column(name = "moneyProg")
	public String getMoneyProg() {
		return this.moneyProg;
	}

	public void setMoneyProg(String moneyProg) {
		this.moneyProg = moneyProg;
	}

	@Column(name = "driverPhone")
	public String getDriverPhone() {
		return this.driverPhone;
	}

	public void setDriverPhone(String driverPhone) {
		this.driverPhone = driverPhone;
	}

	@Column(name = "checkName")
	public String getCheckName() {
		return this.checkName;
	}

	public void setCheckName(String checkName) {
		this.checkName = checkName;
	}

	@Column(name = "bossReply")
	public String getBossReply() {
		return this.bossReply;
	}

	public void setBossReply(String bossReply) {
		this.bossReply = bossReply;
	}

}