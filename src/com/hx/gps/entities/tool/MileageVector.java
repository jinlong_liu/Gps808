package com.hx.gps.entities.tool;

public class MileageVector {
	
	private int taxiNumber;			//车辆数目
	private int	dayNumber;			//日期天数
	private float totalMileage;		//总营运里程
	private float mileageUtil;		//载客里程
	private float maxMileage;		//单车单日最大运营里程
	private float maxMileageUtil;	//单车单日最大载客里程
	private float rate;				//两位小数的形成利用率
	
	public int getTaxiNumber() {
		return taxiNumber;
	}
	public void setTaxiNumber(int taxiNumber) {
		this.taxiNumber = taxiNumber;
	}
	public int getDayNumber() {
		return dayNumber;
	}
	public void setDayNumber(int dayNumber) {
		this.dayNumber = dayNumber;
	}
	public float getTotalMileage() {
		return totalMileage;
	}
	public void setTotalMileage(float totalMileage) {
		this.totalMileage = totalMileage;
	}
	public float getMileageUtil() {
		return mileageUtil;
	}
	public void setMileageUtil(float mileageUtil) {
		this.mileageUtil = mileageUtil;
	}
	public float getMaxMileage() {
		return maxMileage;
	}
	public void setMaxMileage(float maxMileage) {
		this.maxMileage = maxMileage;
	}
	public float getMaxMileageUtil() {
		return maxMileageUtil;
	}
	public void setMaxMileageUtil(float maxMileageUtil) {
		this.maxMileageUtil = maxMileageUtil;
	}
	public float getRate() {
		return rate;
	}
	public void setRate(float rate) {
		this.rate = rate;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MileageVector other = (MileageVector) obj;
		if (dayNumber != other.dayNumber)
			return false;
		if (Float.floatToIntBits(maxMileage) != Float
				.floatToIntBits(other.maxMileage))
			return false;
		if (Float.floatToIntBits(maxMileageUtil) != Float
				.floatToIntBits(other.maxMileageUtil))
			return false;
		if (Float.floatToIntBits(mileageUtil) != Float
				.floatToIntBits(other.mileageUtil))
			return false;
		if (Float.floatToIntBits(rate) != Float.floatToIntBits(other.rate))
			return false;
		if (taxiNumber != other.taxiNumber)
			return false;
		if (Float.floatToIntBits(totalMileage) != Float
				.floatToIntBits(other.totalMileage))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "MileageUtil [taxiNumber=" + taxiNumber + ", dayNumber="
				+ dayNumber + ", totalMileage=" + totalMileage
				+ ", mileageUtil=" + mileageUtil + ", maxMileage=" + maxMileage
				+ ", maxMileageUtil=" + maxMileageUtil + ", rate=" + rate + "]";
	}
	
	

}
