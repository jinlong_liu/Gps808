<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>计价器锁机</title>
    <meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
    <meta name="description"
          content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">
    <link rel="shortcut icon" href="favicon.ico">
    <link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet"><!--表格排序相关 -->
    <!-- Data Tables -->
    <link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet"><!--与表格样式有点关系  -->
    <!-- <link href="css/animate.min.css" rel="stylesheet"> -->
    <link href="css/style.min.css?v=4.0.0" rel="stylesheet"><!-- 基本样式 -->
    <link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet"><!--表格样式相关  -->
    <link href="css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>

</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>计价器锁机</h5>
            </div>

            <div class="ibox-content form-group"   style="height: 770px">

                <form class="form-horizontal" action="saveSendTime" method="post"
                      id="getOneForm">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">选中的车牌号：</label>
                        <div class="col-sm-8">
								<textarea class="form-control" type="text" name="taxiNums"
                                          style="resize: none;"	id="taxiNums" >${taxiNum}</textarea>
                            <%-- <span >${taxiNum}</span> --%>
                            <%-- value="<%=request.getParameter("taxiNum")%>" --%>
                        </div>
                    </div>
                </form>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-4">

                        <button class="btn  btn-primary" type="text" id="getOneBtn">发&nbsp;&nbsp;&nbsp;送</button>&nbsp;&nbsp;

                        <!--  <a href="javascript:window.opener=null;window.close();"><button type="button"

                                class="btn">返&nbsp;&nbsp;&nbsp;回</button></a>-->

                        <!-- <a href="getTreeOfTemp"><button type="button">返回</button></a> -->
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
</div>
<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="js/jquery.min.js?v=2.1.4"></script>
<script src="js/bootstrap.min.js?v=3.3.5"></script>
<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="js/content.min.js?v=1.0.0"></script>
<script type="text/javascript">




    //点击"发送"按钮
    $("#getOneBtn").click(function() {
        var taxiNums = $("#taxiNums").val();
        var context = $("#context").val();
        if (taxiNums == "") {
            swal({
                title : "没有选中任何车辆,请点击'返回'按钮返回",
                type : "warning"
            });
        }else{
            /* swal({
                title : "",
                title : "发送成功,请前往'命令日志'页面查看",
                type : "warning"
            }, function() {
                $("#getOneForm").submit();
            }); */
            $("#getOneForm").submit();
        }
    });

    //页面加载完成后执行,获取执行状态
    $(document).ready(function(){
        var result='<%=request.getAttribute("result")%>';
        if (result != "null") {
            swal({
                title : result,
                type : "success"
            });
        }
    });


</script>
</body>
</html>
