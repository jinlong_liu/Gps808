<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">


<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">

		<div class="row">
		<div class="col-sm-12">
				<!-- 	<div class="ibox float-e-margins"> -->
				<form class="form-inline" id="getOneForm" name="getOneForm1" action="saveOverSpeed" method="post" onsubmit="return validate_channel_info(this);">
					<div class="ibox-title">
						<h5 class="text-center">设置超速规定</h5>
					</div>
					<div class="ibox-content">
						<div class="row">
							<div class="col-sm-5">
								<label class="control-label">最高车速速度限制（单位km/h）： </label><input type="text" class="form-control" placeholder="单位km/h" name="maxSpeed" id="setSpeed"
								>
							</div>
							<div class="col-sm-5">
								<label class="control-label">持续时间（单位秒，范围5-200）：</label> <input type="text" class="form-control" placeholder="单位秒" name="duration" id="setTime"
								>
							</div>
					
							<div class="col-sm-2">
								<button type="submit" class="btn btn-primary ">添加</button>
						
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>

	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/content.min.js?v=1.0.0"></script>
	<script>
		//页面加载完成后执行,获取执行状态
		$(document).ready(function(){
			
			$.ajax({
	            //提交数据的类型 POST GET
	            type:"POST",
	            //提交的网址
	            url:"getOverSpeed",
	            //返回数据的格式
	            datatype: "json",//"xml", "html", "script", "json", "jsonp", "text".
	            //成功返回之后调用的函数             
	            success:function(data){
	            	console.log("返回预设信息"+JSON.stringify(data));
	            	for (var i = 0; i < data.length; i++) {
	                    //result[i]表示获得第i个json对象即JSONObject
	                    //result[i]通过.字段名称即可获得指定字段的值
	                    //alert(data[i].maxSpeed);
	                    var s = data[i].maxSpeed;
	                    var t = data[i].duration;
	                }
	            	$("#setSpeed").val(s);
	    			$("#setTime").val(t);
	            } ,          
	         });
			
			var result = '<%=request.getAttribute("result")%>';
			if(result != null && result != "null"){
				if (result.indexOf("成功")>0) {
				swal({
					title : result,
					type : "success"
				});
			}else{
				swal({
					title :result,
					type : "error"
				});
			}
			}
		});
		
		function validate_channel_info(getOneForm1)  {
			var regPos = /^\d+(\.\d+)?$/; //非负浮点数
			var regNeg = /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/; //负浮点数
            if(getOneForm1.maxSpeed.value==""){  
            	swal({
					title :"最高车速限制不能为空",
					type : "error"
				}); 
                return false;  
            }
            else if(getOneForm1.duration.value==""){
            	swal({
					title :"持续时间不能为空",
					type : "error"
				}); 
                return false;  
            }
            else if(!((regPos.test(getOneForm1.maxSpeed.value) || regNeg.test(getOneForm1.maxSpeed.value))
            		&&(regPos.test(getOneForm1.duration.value) || regNeg.test(getOneForm1.duration.value)))){  
            	swal({
					title :"请输入数字",
					type : "error"
				}); 
                return false;  
            }
            else if(!((getOneForm1.maxSpeed.value>=0)&&(getOneForm1.maxSpeed.value<=999))){
            	swal({
					title :"请输入正确范围",
					type : "error"
				}); 
            	return false;
            }
            else if(!((getOneForm1.duration.value>=5)&&(getOneForm1.duration.value<=200))){
            	swal({
					title :"请输入正确范围",
					type : "error"
				}); 
            	return false;
            }
            return true;  
        }    
	</script>
</body>

</html>