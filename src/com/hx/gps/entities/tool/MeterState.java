package com.hx.gps.entities.tool;

import com.hx.gps.util.DateUtil;
import com.mongodb.BasicDBObject;

/**
 * 计价器状态
 * 
 * @author 作者 dream:
 * @version 创建时间：2018年7月3日 下午10:58:43 天道丶放飞梦想
 */
public class MeterState {
	String isuNum;
	String taxiNum;
	String state;
	String time;

	public MeterState(String isuNum, String taxiNum, String state, String time) {
		super();
		this.isuNum = isuNum;
		this.taxiNum = taxiNum;
		this.state = state;
		this.time = time;
	}
	
	public MeterState(BasicDBObject finds) {
		super();
		this.isuNum = finds.getString("isuNum");
		this.state = finds.getString("state"); 
		this.taxiNum=finds.getString("taxiNum");
		this.time = DateUtil.dateToString(finds.getDate("time"));
	}


	public String getIsuNum() {
		return isuNum;
	}

	public void setIsuNum(String isuNum) {
		this.isuNum = isuNum;
	}

	public String getTaxiNum() {
		return taxiNum;
	}

	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "{isuNum:'" + isuNum + "', taxiNum:'" + taxiNum + "', state:'" + state + "', time:'" + time + "}";
	}

}
