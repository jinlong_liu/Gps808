<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>运力分析</title>

<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link rel="shortcut icon" href="favicon.ico">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="css/plugins/jsTree/style.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<base target="_blank">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content  animated fadeInRight">
		<div class="row">
			<div class="col-sm-9">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>查询条件</h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
							</a> <a class="close-link"> <i class="fa fa-times"></i>
							</a>
						</div>
					</div>
					<div class="ibox-content">
						开始时间：<input type="text" id="startTime">
						&nbsp;&nbsp;&nbsp;&nbsp; 终止时间：<input type="text" id="endTime">
						<br>
						<button onclick="select()">查询</button>
					</div>
				</div>
			</div>

			<div class="col-sm-3">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>车辆列表</h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
							</a> <a class="close-link"> <i class="fa fa-times"></i>
							</a>
						</div>
					</div>
					<div class="ibox-content">
						<input type="text" id="show1">点击的车辆
						<div id="jstree1">
							<ul>
								<li class="jstree-open">所有公司
									<ul>
										<c:forEach items="${comInfo2s}" var="comInfo2s">
											<li>${comInfo2s.cname }
												<ul>
													<c:forEach items="${comInfo2s.taxis}" var="taxis">
														<li data-jstree='{"type":"car"}' id="${taxis.taxiNum }">
															${taxis.taxiNum }</li>
													</c:forEach>
												</ul>
											</li>
										</c:forEach>
									</ul>
								</li>
							</ul>
						</div>

					</div>
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-sm-9">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>折线图</h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
							</a> <a class="dropdown-toggle" data-toggle="dropdown"
								href="graph_flot.html#"> <i class="fa fa-wrench"></i>
							</a>
							<ul class="dropdown-menu dropdown-user">
								<li><a href="graph_flot.html#">选项1</a></li>
								<li><a href="graph_flot.html#">选项2</a></li>
							</ul>
							<a class="close-link"> <i class="fa fa-times"></i>
							</a>
						</div>
					</div>
					<div class="ibox-content">
						<div class="echarts" id="echarts-line-chart"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/content.min.js?v=1.0.0"></script>
	<script src="js/plugins/jsTree/jstree.min.js"></script>
	<style>
.jstree-open>.jstree-anchor>.fa-folder:before {
	content: "\f07c"
}

.jstree-default .jstree-icon.none {
	width: 0
}
</style>
	<script>
		$(document).ready(function() {

			$("#show1").val("");//加载页面后，框中值为空

			//加载树形菜单
			$("#jstree1").jstree({
				"core" : {
					"check_callback" : true
				},
				"plugins" : [ "types", "dnd", "checkbox" ],
				"types" : {
					"car" : {
						"icon" : "fa fa-car"
					},
					"default" : {
						"icon" : "fa fa-folder"
					}
				}
			})

			//动态获取选中节点的id
			$("#jstree1").on('changed.jstree', function(e, data) {
				r = [];
				var i, j;
				for (i = 0, j = data.selected.length; i < j; i++) {
					var node = data.instance.get_node(data.selected[i]);
					if (data.instance.is_leaf(node)) {
						r.push(node.id);
					}
				}
				$("#show1").val(r);
			})

		});

		//点击查询按钮
		function select() {
			var e = echarts.init(document.getElementById("echarts-line-chart")), a = {
				title : {
					text : "未来一周气温变化"
				},
				tooltip : {
					trigger : "axis"
				},
				legend : {
					data : [ "最高气温", "最低气温" ]
				},
				grid : {
					x : 40,
					x2 : 40,
					y2 : 24
				},
				calculable : !0,
				xAxis : [ {
					type : "category",
					boundaryGap : !1,
					data : [ "周一", "周二", "周三", "周四", "周五", "周六", "周日" ]
				} ],
				yAxis : [ {
					type : "value",
					axisLabel : {
						formatter : "{value} °C"
					}
				} ],
				series : [ {
					name : "最高气温",
					type : "line",
					data : [ 11, 11, 15, 13, 12, 13, 10 ],
					markPoint : {
						data : [ {
							type : "max",
							name : "最大值"
						}, {
							type : "min",
							name : "最小值"
						} ]
					},
					markLine : {
						data : [ {
							type : "average",
							name : "平均值"
						} ]
					}
				}, {
					name : "最低气温",
					type : "line",
					data : [ 1, -2, 2, 5, 3, 2, 0 ],
					markPoint : {
						data : [ {
							name : "周最低",
							value : -2,
							xAxis : 1,
							yAxis : -1.5
						} ]
					},
					markLine : {
						data : [ {
							type : "average",
							name : "平均值"
						} ]
					}
				} ]
			};
			e.setOption(a), $(window).resize(e.resize);
			alert("ss");
		}
	</script>
</body>

</html>