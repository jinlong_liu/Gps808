<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<!-- 蓝色按钮 黑色字体样式 -->
<link href="css/style.min_user.css" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<style>
.inputOne {
	/* 去掉input默认边框 */
	outline: none;
	border: none;
	line-height: 33px;
	width: 100%;
	height: 100%;
	display: inline;
	background-color: #ffe9ac;
}
</style>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">
		<div class="row">
			<div class="col-sm-12">
			<div class="ibox-title">
				<h4>车辆在线编辑表(双击单元格进行编辑)</h4>
			</div>
			<!-- 数据展示 -->
			<c:if test="${taxiList!=null}">
				<div class="ibox-content" style="overflow-x: scroll;">
					<button onclick="saveTaxis()" type="submit" class="btn btn-primary">保存</button>
					<table id="table1" width="100%"
						style="table-layout: fixed; background: #FFFFFF"
						class="footable table table-stripped" data-page-size="27"
						data-filter=#filter>
						<!-- 搜索框 -->
						<!-- <input type="text" width="100%" class="form-control input-sm m-b-xs" id="filter"
						placeholder="搜索"> -->
						<thead>
							<tr>
								<th width="70">车牌号</th>
								<th width="70">车辆类型</th>
								<th width="70">车主姓名</th>
								<th width="160">车主身份证号</th>
								<!-- 5 -->
	
								<th width="110">车主电话</th>
<!-- 								<th width="70">车主地址</th>
 -->								<th width="110">终端号</th>
								<th width="70">车架号</th>
								<th width="120">终端序列号</th>
								<!-- 10 -->
	
								<th width="90">注册日期</th>
								<th width="90">运营开始日期</th>
								<th width="90">运营结束日期</th>
<!-- 								<th width="90">购置时间</th>
 -->								<th width="90">购置金额</th>
								<!-- 15 -->
	
								<th width="90">操作人</th>
								<th width="90">添加时间</th>
								<th width="150">行驶证号</th>
<!-- 								<th width="90">燃料类型</th>
 --><!-- 								<th width="90">购车日期</th>
 -->								<!-- 20 -->
	
								<th width="90">道路运输证号码</th>
								<th width="90">运输证发证机构</th>
								<th width="90">年审日期</th>
								<th width="90">下次年审时间</th>
								<th width="90">承包人</th>
								<!-- 25 -->
	
								<th width="160">承包人身份证号</th>
								<th width="100">承包人电话</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${taxiList}" var="taxis">
								<tr id="taxiInfo">
									<%-- <td>${taxis.company.name}</td> --%>
									<td id="taxiNum">${taxis.taxiNum}</td>
									<td id="taxiType">${taxis.taxiType}</td>
									<td id="masterName">${taxis.masterName}</td>
									<td id="masterIdNum">${taxis.masterIdNum}</td>
	
									<td id="masterPhone">${taxis.masterPhone}</td>
									<%-- <td id="masterAddress">${taxis.masterAddress}</td> --%>
									<td id="isuNum">${taxis.isuNum}</td>
									<td id="taxiFrame">${taxis.taxiFrame}</td>
									<td id="isuPhone">${taxis.isuPhone}</td>
	
									<td id="registerDate">${taxis.registerDate}</td>
									<td id="operStartDate">${taxis.operStartDate}</td>
									<td id="operEndDate">${taxis.operEndDate}</td>
<%-- 									<td id="purDate">${taxis.purDate}</td>
 --%>									<td id="purAmount">${taxis.purAmount}</td>
	
									<td id="operationer">${taxis.operationer}</td>
									<td id="addDate">${taxis.addDate}</td>
									<td id="drivIdNum">${taxis.drivIdNum}</td>
<%-- 									<td id="fuelType">${taxis.fuelType}</td>
  								<td id="byCarDate">${taxis.byCarDate}</td>
 --%>	
									<td id="tranIdNum">${taxis.tranIdNum}</td>
									<td id="tranAgency">${taxis.tranAgency}</td>
									<td id="auditDate">${taxis.auditDate}</td>
									<td id="nextAuditDate">${taxis.nextAuditDate}</td>
									<td id="conter">${taxis.conter}</td>
	
									<td id="conterIdNum">${taxis.conterIdNum}</td>
									<td id="conterPhone">${taxis.conterPhone}</td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="22">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</c:if>
		</div>
		</div>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
	<script type="text/javascript">
		//点击保存 ，ajax 返回json到后台
		function saveTaxis(){
			//预设json;
			var args = {};
			//查出全局内容
			//计数器： 
			var count=0;
			//遍历table 每一行
			$("#table1").find("tr").each(function () {
				var tr=$(this).attr("id");
				if (tr=="taxiInfo"){
					var tdNum=0;
					args['taxis[' + count +'].taxiNum'] = $(this).find("#taxiNum").text();
					args['taxis[' + count +'].taxiType'] = $(this).find("#taxiType").text();
					args['taxis[' + count +'].masterName'] = $(this).find("#masterName").text();
					args['taxis[' + count +'].masterIdNum'] = $(this).find("#masterIdNum").text();
					args['taxis[' + count +'].masterPhone'] = $(this).find("#masterPhone").text();
/* 					args['taxis[' + count +'].masterAddress'] =$(this).find("#masterAddress").text();
 */					args['taxis[' + count +'].isuNum'] = $(this).find("#isuNum").text();
					args['taxis[' + count +'].taxiFrame'] = $(this).find("#taxiFrame").text();
					args['taxis[' + count +'].isuPhone'] = $(this).find("#isuPhone").text();
					
					args['taxis[' + count +'].registerDate'] = $(this).find("#registerDate").text();
					args['taxis[' + count +'].operStartDate'] = $(this).find("#operStartDate").text();
					args['taxis[' + count +'].operEndDate'] = $(this).find("#operEndDate").text();
/* 					args['taxis[' + count +'].purDate'] = $(this).find("#purDate").text();
 */					args['taxis[' + count +'].purAmount'] = $(this).find("#purAmount").text();
					args['taxis[' + count +'].operationer'] =$(this).find("#operationer").text();
					args['taxis[' + count +'].addDate'] = $(this).find("#addDate").text();
					args['taxis[' + count +'].drivIdNum'] = $(this).find("#drivIdNum").text();
/* 					args['taxis[' + count +'].fuelType'] = $(this).find("#fuelType").text();
					args['taxis[' + count +'].byCarDate'] = $(this).find("#byCarDate").text();
 */		
					args['taxis[' + count +'].tranIdNum'] = $(this).find("#tranIdNum").text();
					args['taxis[' + count +'].tranAgency'] = $(this).find("#tranAgency").text();
					args['taxis[' + count +'].auditDate'] = $(this).find("#auditDate").text();
					args['taxis[' + count +'].nextAuditDate'] = $(this).find("#nextAuditDate").text();
					args['taxis[' + count +'].conter'] =$(this).find("#conter").text();
					args['taxis[' + count +'].conterIdNum'] = $(this).find("#conterIdNum").text();
					args['taxis[' + count +'].conterPhone'] = $(this).find("#conterPhone").text();
					
					count=count+1;
				}
            });
			//提交ajax
			$.ajax({
				url : 'saveEditTaxis',
				data : args,
				dataType: "json",
				type : "POST",
				success : function(data) {
					//alert(JSON.stringify(data));
					console.log("操作成功");
					if(data  != null ){
							swal({
								title : "添加成功",
								type : "success"
							});
							//添加重试按钮
						}else if(data =="操作失败"){
							swal({
								title : "添加失败",
								type : "error"
							});
						} 
				}
			});
			
		}
	</script>
	<script>
		//加载footable的功能
		$(".footable").footable();
		//页面加载完成后执行,获取执行状态
		$(document).ready(function(){
			var result='<%=request.getAttribute("result")%>';
			if (result != "null") {
				swal({
					title : result,
					type : "warning"
				});
			}
		});

		//点击“保存”按钮
		function save() {

		}
	</script>
	<script type="text/javascript">
		window.onload = function(e) {
			// 获取所有的单元格
			var td = document.getElementsByTagName("td");
			for (var i = 0; i < td.length; i++) {
				td[i].index = i;
				td[i].onclick = function(e) {
					if (e.target.tagName.toLowerCase() == "td") {
						var input = document.createElement("input");
						input.type = "text";
						input.className = "inputOne";
						input.value = this.innerHTML;
						td[this.index].innerHTML = "";
						td[this.index].appendChild(input);
						input.focus();
						input.onblur = function() {
							this.parentNode.innerHTML = input.value;
							input.remove();
						}
						// 阻止冒泡
						input.onclick = function(evt) {
							evt = evt || window.event;
							evt.stopPropagation() ? e.stopPropagation()
									: e.cancelBubble = true;
						}
					}
				};
			}
		}
	</script>
</body>

</html>