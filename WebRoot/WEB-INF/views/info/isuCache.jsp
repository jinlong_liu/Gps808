<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">

<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">


		<!-- 数据展示 -->
	
		<div class="row" >
			<div class="col-sm-12">
			<div class="ibox-title">
				<h5>&nbsp;&nbsp;&nbsp;车辆信息缓存表</h5><br>
				
				<h6>&nbsp;&nbsp;&nbsp;待同步的车辆信息显示在这里</h6>
			</div>

		<div class="ibox-content" style="height: 760px">
			<table id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
		
				<thead>
					<tr>
						<th width="70">终端号</th>
						<th width="70">类型</th>
						<th width="70">时间</th>
					</tr>
				</thead>
				<tbody>
				<c:forEach items="${datas}" var="isuCache">
						<tr>
							<td>${isuCache.isu }</td>
							<td><c:if test="${isuCache.type eq 1 }">插入</c:if> <c:if
									test="${isuCache.type eq 2 }">更新，更新为：${isuCache.isu2 }</c:if> <c:if
									test="${isuCache.type eq 3 }">删除</c:if></td>
							<td>${isuCache.date }</td>
						</tr>
					</c:forEach>
				</tbody>
			
			</table>
			</div>
		</div>
</div>

	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script>
	//加载footable的功能
	$(document).ready(function() {
	$('#table1').DataTable({
		"pagingType" : "full_numbers"
	});
});

		//点击"添加"按钮
		function save() {
			//修改车牌号下拉框的值
			$.post("getAllTaxiByAjax", function(data) {
				if (data.length != 0) {
					//返回的数据不为空 
					var newdata = eval("(" + data + ")");
					$("#taxiSelect").html("");//避免选择框中的值持续叠加
					/* $("<option value='"+taxiId+"'>" + taxiNum + "</option>")
							.appendTo($("#taxiSelect")); */
					$("<option> </option>").appendTo($("#taxiSelect"));
					for (var i = 0; i < data.length; i++) {
						$("<option> " + newdata[i].taxiNum + "</option>")
								.appendTo($("#taxiSelect"));
					}
				}
			});
		}

		//点击"修改"按钮
		function update(id, taxiNum, remark) {
			$("#id").val(id);
			$("#taxiNum").val(taxiNum);
			$("#remark").val(remark);
		};

		//点击"删除"按钮
		function delete1(id, name) {
			swal({
				title : "",
				text : "您确定删除 " + name + " 的数据吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
				$("#deleteId").val(id);
				//提交表单
				$("#deleteForm").submit();
			});
		}
	</script>
</body>

</html>