package com.hx.gps.entities.tool;

import java.util.Date;

import com.hx.gps.util.DateUtil;
import com.mongodb.BasicDBObject;

/** 
* @author 作者 dream: 
* @version 创建时间：2017年8月16日 下午3:52:05 
* 天道丶放飞梦想
*/
/**
 * @author LFX
 *
 */
public class videoState {
	// 车牌号
	String taxiNum;
	// 终端号
	String isuNum;
	// 接收时间
	String time;
	// 摄像头一
	String camera1;
	// 摄像头二
	String camera2;
	// 摄像头三
	String camera3;
	// 摄像头四
	String camera4;
	// 拾音器
	String MIC;
	// Gps天线状态
	String gpsState;
	// 当前是否定位
	String locationState;
	// 当前温度
	String currentTemp;
	// 风扇开关
	String blowerON;
	// 视频供电回路
	String elecLoop;
	// ACC
	String accState;
	// 重新连接平台次数(BKP)
	String tryNum;
	// RTC是否有效
	String rtcValid;
	// RTC时间
	String rtcTime;
	// 收到硬盘故障次数(BKP)
	String bkpNum;
	// 计价器
	String fareMeter;
	// lcd
	String lcdState;

	public videoState() {
	}

	public videoState(String taxiNum, Date time, String isuNum, String camera1, String camera2, String camera3,
			String camera4, String volumnPick, String gpsState, String locationState, String currentTemp,
			String blowerON, String elecLoop, String accState, String tryNum, String rtcValid, String rtcTime,
			String bkpNum, String fareMeter, String lcdState) {
		super();
		this.taxiNum = taxiNum;
		this.time = DateUtil.dateToString(time);
		this.isuNum = isuNum;
		this.camera1 = camera1;
		this.camera2 = camera2;
		this.camera3 = camera3;
		this.camera4 = camera4;
		this.MIC = volumnPick;
		this.gpsState = gpsState;
		this.locationState = locationState;
		this.currentTemp = currentTemp;
		this.blowerON = blowerON;
		this.elecLoop = elecLoop;
		this.accState = accState;
		this.tryNum = tryNum;
		this.rtcValid = rtcValid;
		this.rtcTime = rtcTime;
		this.bkpNum = bkpNum;
		this.fareMeter = fareMeter;
		this.lcdState = lcdState;
	}

//	public videoState(BasicDBObject basicDBObject, String isuNum) {
//		/*
//		 * 
//		 * 
//		 * FAN=1,VVDD=1,ACC=1,reCONN=9,RTC=1, RTC-Time=2017-08-14 07:56:57,
//		 * HDD_err=0,TAXI=1,LCD
//		 * 
//		 */
//		if (basicDBObject.containsField("state")) {
//			String context = basicDBObject.getString("state");
//			Date time = basicDBObject.getDate("time");
//			this.time = DateUtil.dateToString(time);
//			// 先拆逗号
//			String[] States = context.split(",");
//			// 再拆等号,遍历最后一个位
//			this.camera1 = States[0].substring(States[0].length() - 1, States[0].length());
//			this.camera2 = States[1].substring(States[1].length() - 1, States[1].length());
//			this.camera3 = States[2].substring(States[2].length() - 1, States[2].length());
//			this.camera4 = States[3].substring(States[3].length() - 1, States[3].length());
//			this.MIC = States[4].substring(States[4].length() - 1, States[4].length());
//			this.gpsState = States[5].substring(States[5].length() - 1, States[5].length());
//			this.locationState = States[6].substring(States[6].length() - 1, States[6].length());
//			this.currentTemp = States[7].substring(5, States[7].length());
//			this.blowerON = States[8].substring(States[8].length() - 1, States[8].length());
//			this.elecLoop = States[9].substring(States[9].length() - 1, States[9].length());
//			this.accState = States[10].substring(States[10].length() - 1, States[10].length());
//			this.tryNum = States[11].substring(States[11].length() - 1, States[11].length());
//			this.rtcValid = States[12].substring(States[12].length() - 1, States[12].length());
//			this.rtcTime = States[13].substring(9, States[13].length());
//			this.bkpNum = States[14].substring(States[14].length() - 1, States[14].length());
//			this.fareMeter = States[15].substring(States[15].length() - 1, States[15].length());
//			this.lcdState = States[16].substring(States[16].length() - 1, States[16].length());
//			// 最后进行数据的拼接
//			this.isuNum = basicDBObject.getString("isuNum");
//			this.taxiNum = isuNum;
//			// 状态的判断
//
//		}
//
//	}
	public videoState(BasicDBObject basicDBObject, String isuNum) {
		/*
		 * 
		 * 
		 * FAN=1,VVDD=1,ACC=1,reCONN=9,RTC=1, RTC-Time=2017-08-14 07:56:57,
		 * HDD_err=0,TAXI=1,LCD
		 * 
		 */
		if (basicDBObject.containsField("state")) {
			String context = basicDBObject.getString("state");
			Date time = basicDBObject.getDate("time");
			this.time = DateUtil.dateToString(time);
			// 先拆逗号
//			String[] States = context.split("2C");
//			// 再拆等号,遍历最后一个位
//			this.camera1 = States[0].substring(States[0].length() - 2, States[0].length());
//			this.camera2 = States[1].substring(States[1].length() - 2, States[1].length());
//			this.camera3 = States[2].substring(States[2].length() - 2, States[2].length());
//			this.camera4 = States[3].substring(States[3].length() - 2, States[3].length());
//			this.MIC = States[4].substring(States[4].length() - 2, States[4].length());
//			this.gpsState = States[5].substring(States[5].length() - 2, States[5].length());
//			this.locationState = States[6].substring(States[6].length() - 2, States[6].length());
//			this.currentTemp = States[7].substring(10, States[7].length());
//			this.blowerON = States[8].substring(States[8].length() - 2, States[8].length());
//			this.elecLoop = States[9].substring(States[9].length() - 2, States[9].length());
//			this.accState = States[10].substring(States[10].length() - 2, States[10].length());
//			this.tryNum = States[11].substring(States[11].length() - 2, States[11].length());
//			this.rtcValid = States[12].substring(States[12].length() - 2, States[12].length());
//			this.rtcTime = States[13].substring(18, States[13].length());
//			this.bkpNum = States[14].substring(States[14].length() - 2, States[14].length());
//			this.fareMeter = States[15].substring(States[15].length() - 2, States[15].length());
//			this.lcdState = States[16].substring(States[16].length() - 2, States[16].length());
//			// 最后进行数据的拼接
			this.camera1=context;
			this.isuNum = basicDBObject.getString("isuNum");
			this.taxiNum = isuNum;
			// 状态的判断

		}

	}

	public String getTaxiNum() {
		return taxiNum;
	}

	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}

	public String getIsuNum() {
		return isuNum;
	}

	public void setIsuNum(String isuNum) {
		this.isuNum = isuNum;
	}

	public String getCamera1() {
		return camera1;
	}

	public void setCamera1(String camera1) {
		this.camera1 = camera1;
	}

	public String getCamera2() {
		return camera2;
	}

	public void setCamera2(String camera2) {
		this.camera2 = camera2;
	}

	public String getCamera3() {
		return camera3;
	}

	public void setCamera3(String camera3) {
		this.camera3 = camera3;
	}

	public String getCamera4() {
		return camera4;
	}

	public void setCamera4(String camera4) {
		this.camera4 = camera4;
	}

	public String getVolumnPick() {
		return MIC;
	}

	public void setVolumnPick(String volumnPick) {
		this.MIC = volumnPick;
	}

	public String getGpsState() {
		return gpsState;
	}

	public void setGpsState(String gpsState) {
		this.gpsState = gpsState;
	}

	public String getLocationState() {
		return locationState;
	}

	public void setLocationState(String locationState) {
		this.locationState = locationState;
	}

	public String getCurrentTemp() {
		return currentTemp;
	}

	public String getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = DateUtil.dateToString(time);
	}

	public void setCurrentTemp(String currentTemp) {
		this.currentTemp = currentTemp;
	}

	public String getBlowerON() {
		return blowerON;
	}

	public void setBlowerON(String blowerON) {
		this.blowerON = blowerON;
	}

	public String getElecLoop() {
		return elecLoop;
	}

	public void setElecLoop(String elecLoop) {
		this.elecLoop = elecLoop;
	}

	public String getAccState() {
		return accState;
	}

	public void setAccState(String accState) {
		this.accState = accState;
	}

	public String getTryNum() {
		return tryNum;
	}

	public void setTryNum(String tryNum) {
		this.tryNum = tryNum;
	}

	public String getRtcValid() {
		return rtcValid;
	}

	public void setRtcValid(String rtcValid) {
		this.rtcValid = rtcValid;
	}

	public String getRtcTime() {
		return rtcTime;
	}

	public void setRtcTime(String rtcTime) {
		this.rtcTime = rtcTime;
	}

	public String getBkpNum() {
		return bkpNum;
	}

	public void setBkpNum(String bkpNum) {
		this.bkpNum = bkpNum;
	}

	public String getFareMeter() {
		return fareMeter;
	}

	public void setFareMeter(String fareMeter) {
		this.fareMeter = fareMeter;
	}

	public String getLcdState() {
		return lcdState;
	}

	public void setLcdState(String lcdState) {
		this.lcdState = lcdState;
	}

	public String getMIC() {
		return MIC;
	}

	public void setMIC(String mIC) {
		MIC = mIC;
	}

	@Override
	public String toString() {
		return "{taxiNum:'" + taxiNum + "', isuNum:'" + isuNum + "', time:'" + time + "', camera1:'" + camera1
				+ "', camera2:'" + camera2 + "', camera3:'" + camera3 + "', camera4:'" + camera4 + "', MIC:'" + MIC
				+ "', gpsState:'" + gpsState + "', locationState:'" + locationState + "', currentTemp:'" + currentTemp
				+ "', blowerON:'" + blowerON + "', elecLoop:'" + elecLoop + "', accState:'" + accState + "', tryNum:'"
				+ tryNum + "', rtcValid:'" + rtcValid + "', rtcTime:'" + rtcTime + "', bkpNum:'" + bkpNum
				+ "', fareMeter:'" + fareMeter + "', lcdState:'" + lcdState + "}";
	}

}
