package com.hx.gps.entities;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * User entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "user" )
public class User implements java.io.Serializable {

	// Fields
	private Integer id;
	private Role role;
	private Company company;
	private String name;
	private String password;
	private Integer state;
	private String people;
	private Timestamp time;

	// Constructors

	/** default constructor */
	public User() {
	}

	/** minimal constructor */
	public User(Role role, Company company) {
		this.role = role;
		this.company = company;
	}

	/** full constructor */
	public User(Role role, Company company, String name, String password, Integer state,
			String people, Timestamp time) {
		this.role = role;
		this.company = company;
		this.name = name;
		this.password = password;
		this.state = state;
		this.people = people;
		this.time = time;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "roleId", nullable = true)
	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "comId", nullable = true)//nullable = true 可为空
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "password")
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "state")
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "people")
	public String getPeople() {
		return this.people;
	}

	public void setPeople(String people) {
		this.people = people;
	}

	@Column(name = "time", length = 19)
	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "User [id=" + id  + ", company=" + company + ", name=" + name + ", password="
				+ password + ", state=" + state + ", people=" + people + ", time=" + time + "]";
	}

	

}