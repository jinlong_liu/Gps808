package com.hx.gps.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Deduct entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "deduct" )
public class Deduct implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer scorer;
	private Integer scoreTaxi;
	private Integer scorePany;
	private Integer driverId;
	private Integer panyId;
	private String taxiNum;
	private Integer state;
	private Integer compId;
	private Date dealDate;
	private Set<Complaints> complaintses = new HashSet<Complaints>(0);

	// Constructors

	/** default constructor */
	public Deduct() {
	}

	/** full constructor */
	public Deduct(Integer scorer, Integer scoreTaxi, Integer scorePany,
			Integer driverId, Integer panyId, String taxiNum, Integer state,
			Integer compId, Date dealDate, Set<Complaints> complaintses) {
		this.scorer = scorer;
		this.scoreTaxi = scoreTaxi;
		this.scorePany = scorePany;
		this.driverId = driverId;
		this.panyId = panyId;
		this.taxiNum = taxiNum;
		this.state = state;
		this.compId = compId;
		this.dealDate = dealDate;
		this.complaintses = complaintses;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "scorer")
	public Integer getScorer() {
		return this.scorer;
	}

	public void setScorer(Integer scorer) {
		this.scorer = scorer;
	}

	@Column(name = "scoreTaxi")
	public Integer getScoreTaxi() {
		return this.scoreTaxi;
	}

	public void setScoreTaxi(Integer scoreTaxi) {
		this.scoreTaxi = scoreTaxi;
	}

	@Column(name = "scorePany")
	public Integer getScorePany() {
		return this.scorePany;
	}

	public void setScorePany(Integer scorePany) {
		this.scorePany = scorePany;
	}

	@Column(name = "driverId")
	public Integer getDriverId() {
		return this.driverId;
	}

	public void setDriverId(Integer driverId) {
		this.driverId = driverId;
	}

	@Column(name = "panyId")
	public Integer getPanyId() {
		return this.panyId;
	}

	public void setPanyId(Integer panyId) {
		this.panyId = panyId;
	}

	@Column(name = "taxiNum", length = 20)
	public String getTaxiNum() {
		return this.taxiNum;
	}

	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}

	@Column(name = "state")
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "compId")
	public Integer getCompId() {
		return this.compId;
	}

	public void setCompId(Integer compId) {
		this.compId = compId;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "dealDate", length = 10)
	public Date getDealDate() {
		return this.dealDate;
	}

	public void setDealDate(Date dealDate) {
		this.dealDate = dealDate;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "deduct")
	public Set<Complaints> getComplaintses() {
		return this.complaintses;
	}

	public void setComplaintses(Set<Complaints> complaintses) {
		this.complaintses = complaintses;
	}

	@Override
	public String toString() {
		return "Deduct [id=" + id + ", scorer=" + scorer + ", scoreTaxi="
				+ scoreTaxi + ", scorePany=" + scorePany + ", driverId="
				+ driverId + ", panyId=" + panyId + ", taxiNum=" + taxiNum
				+ ", state=" + state + ", compId=" + compId + ", dealDate="
				+ dealDate + "]";
	}

}