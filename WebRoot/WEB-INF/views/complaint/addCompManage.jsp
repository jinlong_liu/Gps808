<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
    <meta name="description"
          content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<!-- gy 添加表单验证 -->
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
</head>

<body class="gray-bg">
 <div class="wrapper wrapper-content animated ">
    <!-- <div class="row">
       <div class="col-sm-12">
            <div class="ibox-content">
                 投诉管理界面关闭添加功能，分离成独立功能放在 客户投诉 中
                <button type="button" class="btn btn-primary" data-toggle="modal"
                    data-target="#myModal5" onclick="init1()">添加</button>
                <h3> 投诉管理： </h3>
                点击添加按钮弹出来的form表单
                
            </div>
        </div> 
    </div> -->

<div class="modal inmodal fade" id="myModal5" tabindex="-1"
                     role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div style="margin: 10px 10px">
                                <form id="submitForm"  class="form-horizontal" action="addComplaint" method="post">
                                    <div class="ibox-title">
                                        <h5>修改投诉信息 <span style="color:red">（* 为必填项）</span>:</h5>
                                    </div>


                                    <input type="hidden" name="id" id="id">
                                    <fieldset>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class=" form-group ">
                                                    <label class=" col-sm-4 control-label"><span style="color:red">*  </span>举报车辆 ：</label> 
                                                    <div class="col-sm-8">
                                                    <select
                                                        class=" form-control" name="taxi1" id="taxi1">
                                                    <option value="">请选择投诉车牌号</option>
                                                    <c:forEach items="${taxis}" var="taxis">
                                                        <option value="${taxis.id}">${taxis.taxiNum}</option>
                                                    </c:forEach>
                                                </select>
                                               </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class=" form-group">
                                                    <label class=" col-sm-4   control-label"><span style="color:red">*  </span>上车时间 ：</label>
                                                      <div class="col-sm-8">
                                                     <input
                                                        class="form-control" type="text" name="startTime"
                                                        id="startTime" value="${sensorStartDate}" class="Wdate"
                                                        onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'endTime\')||\'new Date()\'}',onpicked:function(dp){refreshValidator(submitForm, name.valueOf())}})"/>
                                                   </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class=" form-group">
                                                    <label class=" col-sm-4 control-label"><span style="color:red">*  </span>举报司机 ：</label> 
                                                    <div class="col-sm-8">
                                                    <select
                                                        class="form-control" name="driver1" id="driver1">
                                                    <option value="">请选择投诉司机</option>
                                                </select>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class=" form-group">
                                                    <label class=" col-sm-4  control-label"><span style="color:red">*  </span>下车时间 ：</label>
                                                    <div class="col-sm-8">
                                                    <input
                                                        class="form-control" type="text" name="endTime" id="endTime"
                                                        value="${sensorEndDate}" class="Wdate"
                                                        onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'startTime\')}',maxDate:'#F{\'new Date()\'}',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(submitForm, name.valueOf())}})"/>
                                                   </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class=" form-group">
                                                    <label class=" col-sm-4 control-label"><span style="color:red">*  </span>投诉类型 ：</label>
                                                    <div class="col-sm-8">
                                                    <select
                                                        class="form-control" name="reportType1" id="reportType1">
                                                    <option value="">请选择投诉类型</option>
                                                    <c:forEach items="${compTypes}" var="compTypes">
                                                        <option value="${compTypes.id}">${compTypes.type}</option>
                                                    </c:forEach>
                                                </select>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class=" form-group">
                                                    <label class="col-sm-4  control-label">上车地点：</label>
                                                    <div class="col-sm-8">
                                                    <input
                                                        class="form-control" type="text" name="startPlace"
                                                        id="startPlace">
                                                      </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class=" form-group">
                                                    <label class="col-sm-4 control-label"><span style="color:red">*  </span>投诉事由 ：</label>
                                                    <div class="col-sm-8">
                                                    <input
                                                        class="form-control" type="text" name="reportReason"
                                                        id="reportReason">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class=" form-group">
                                                    <label class="col-sm-4  control-label">下车地点：</label>
                                                    <div class="col-sm-8">
                                                    <input
                                                        class="form-control" type="text" name="endPlace"
                                                        id="endPlace">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class=" form-group">
                                                    <label class="col-sm-4  control-label"><span style="color:red">*  </span>投诉内容 ：</label>
                                                    <div class="col-sm-8">
                                                    <textarea class="form-control" rows="5" name="reportContent"
                                                           style="resize: none;"   id="reportContent"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class=" form-group">
                                                    <label class="col-sm-4  control-label"><span style="color:red">*  </span>反&nbsp;馈&nbsp;人 &nbsp;：</label>
                                                    <div class="col-sm-8">
                                                    <input class="form-control" type="text" name="backer" id="backer"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class=" form-group">
                                                    <label class="col-sm-4 control-label"><span style="color:red">*  </span>反馈电话 ：</label>
                                                    <div class="col-sm-8">
                                                    <input
                                                        class="form-control" type="text" name="backPhone"
                                                        id="backPhone"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>


                                    <!-- -->
                                    <div class="modal-footer">

                                        <button type="button" class="btn btn-white"
                                                data-dismiss="modal">关闭
                                        </button>
                                        <button type="submit" class="btn btn-primary" ">保存</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
<div class="row">
    <div class="col-sm-12">
        <div class="ibox-title">
            <h5>投诉管理表</h5>
        </div>
        <div class="ibox-content" style="height: 780px">

            <table id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
                <thead>
                <tr>
                    <th>公司名称</th>
                    <th>车牌号</th>
                    <th>司机姓名</th>
                    <th>投诉类型</th>
                    <th>投诉事由</th>
                    <th>投诉内容</th>
                    <th>上车时间</th>
                    <th>下车时间</th>
                    <th>上车地点</th>
                    <th>下车地点</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${complaint}" var="complaint">
                    <tr id="${complaint.id}">
                        <td>${complaint.taxi.company.name}</td>
                        <td>${complaint.taxi.taxiNum}</td>
                        <td>${complaint.driver.dname}</td>
                        <td>${complaint.comptype.type}</td>
                        <td>${complaint.reportReason}</td>
                        <td>${complaint.reportContent}</td>
                        <td>${complaint.startTime}</td>
                        <td>${complaint.endTime}</td>
                        <td>${complaint.startPlace}</td>
                        <td>${complaint.endPlace}</td>

                        <td>
                            <button id="btn-update" type="button"
                                    class="btn btn-success btn-sm" data-toggle="modal"
                                    data-target="#myModal5"
                                    onclick="alter('${complaint.id}','${complaint.taxi.id}','${complaint.driver.id}','${complaint.comptype.id}',
                                            '${complaint.reportReason}','${complaint.reportContent}','${complaint.startTime}','${complaint.endTime}',
                                            '${complaint.backer}','${complaint.backPhone}','${complaint.startPlace}','${complaint.endPlace}')">
                                修改
                            </button>
                            <button type="button" class="btn btn-danger btn-sm"
                                    onclick="delete1('${complaint.id}')">删除
                            </button>
                            <!-- 导出功能还没有添加 -->
                            <!-- <button type="button" class="btn btn-white btn-sm"
                            data-dismiss="modal">导出</button> --></td>
                    </tr>
                </c:forEach>
                </tbody>
                <tfoot>
							<tr>
								<td colspan="11">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
            </table>
        </div>

    </div>
            <form id="deleteForm" action="deleteComp" method="post">
            <input type="hidden" id="deleteId" name="id">
        </form>
</div>
</div>

<script src="js/jquery.js"></script>
	<script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
<script>

$(document).ready(function() {
	$('#table1').DataTable({
		"pagingType" : "full_numbers"
	});
});
$(document).ready(function formValidator() {
    $('#submitForm').bootstrapValidator({
        message: '这个值无效',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        /*生效规则：字段值一旦变化就触发验证*/
        live: 'enabled',
        /*当表单验证不通过时，该按钮为disabled*/
        submitButtons: 'button[type="submit"]',
        fields: {
        	taxi1: {

                  validators: {
                      notEmpty: {
                          message: '请选择投诉车辆'
                      }
                  }
              },   driver1: {
                  validators: {
                      notEmpty: {
                          message: '请选择投诉司机'
                      }
                  }
              },reportType1: {
                  validators: {
                      notEmpty: {
                          message: '请选择投诉类型'
                      }
                  }
              },
            backer: {
                validators: {
                    notEmpty: {
                        message: '请填写反馈人'
                    }
                }
            },
            backPhone: {
                validators: {
                    notEmpty: {
                        message: '请填写反馈电话'
                    },stringLength: {
                        min: 11,
                        max: 11,
                        message: '请输入11位手机号码'
                    }
                }
            },startTime: {
                validators: {
                    notEmpty: {
                        message: '请选择上车时间'
                    }
                }
            },endTime: {
                validators: {
                    notEmpty: {
                        message: '请选择下车时间'
                    }
                }
            }
        }
    });
});
   

    function alter(id, taxi1, driver1, reportType1, reportReason,
                   reportContent, startTime, endTime, backer, backPhone,
                   startPlace, endPlace) {
        change2(taxi1, driver1);
        $("#id").val(id);
        $("#taxi1").val(taxi1);
        //$("#driver1").val(driver1);
        $("#reportType1").val(reportType1);
        $("#reportReason").val(reportReason);
        $("#reportContent").val(reportContent);
        $("#startTime").val(startTime);
        $("#endTime").val(endTime);
        $("#backer").val(backer);
        $("#backPhone").val(backPhone);
        $("#startPlace").val(startPlace);
        $("#endPlace").val(endPlace);
    };
    function init1() {
        $("#id").val("");
        $("#taxi1").val("");
        $("#driver1").val("");
        $("#reportType1").val("");
        $("#reportReason").val("");
        $("#reportContent").val("");
        $("#startTime").val("");
        $("#endTime").val("");
        $("#backer").val("");
        $("#backPhone").val("");
        $("#startPlace").val("");
        $("#endPlace").val("");
    } ;

    $('#taxi1').change(
            function () {
                var id = $('#taxi1').val();
                if (id != "") {
                    var driver = $('#driver1');
                    $.post("getDriverByTaxi", {
                        id: id
                    }, function (data) {
                        if (data.length != 0) {
                            //返回的数据不为空
                            var newdata = eval("(" + data + ")");
                            driver.html("");//避免选择框中的值持续叠加
                            $("<option value=''>请选择举报司机</option>")
                                    .appendTo(driver);

                            for (var i = 0; i < data.length; i++) {
                                $(
                                        "<option value ='" + newdata[i].driId +
                                        "'> "
                                        + newdata[i].driName
                                        + "</option>").appendTo(
                                        driver);
                            }
                        }
                    });
                }
            });

    function change2(id, driver2) {
        var driver = $("#driver1");
        $.post("getDriverByTaxi", {
            id: id
        }, function (data) {
            if (data.length != 0) {
                //返回的数据不为空
                var newdata = eval("(" + data + ")");
                driver.html("");//避免选择框中的值持续叠加
                /* $("<option value=''>请选择举报司机</option>").appendTo(driver); */

                for (var i = 0; i < data.length; i++) {
                    $(
                            "<option value ='" + newdata[i].driId +
                            "'> "
                            + newdata[i].driName + "</option>")
                            .appendTo(driver);
                }
                $("#driver1").val(driver2);
            }
        });
    }

    function delete1(id) {
        swal({
            title: "您确定执行该操作吗?",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "取消"
        }, function () {
          /*   $("#" + id).hide();
            var args = {
                "id": id
            };
            var url = "daleteComp";
            $.post(url, args, function (data) {
                var a = String(data);
                if (a == "操作执行成功") {
                	swal({
		                title: data,
		                type: "success"
		            },function(){
						window.location.reload();
						});
                } else {
                	swal({
		                title: data,
		                type: "warning"
		            });
                    $("#" + id).show();
                }
            });
           */
        	   //改为表单提交的方式
        	  $("#deleteId").val(id);  
				//提交表单
			  $("#deleteForm").submit();  
        })
    }

    //页面加载完成后执行,获取执行状态
    $(document).ready(function () {
        var result='<%=request.getAttribute("result")%>';
    	if(result != null && result != "null"){
    		if (result.indexOf("成功")>0) {
    		swal({
    			title : result,
    			type : "success"
    		});
    	}else{
    		swal({
    			title :result,
    			type : "warning"
    		});
    	}
    	}
    /*日历刷新验证*/
		function 
		refreshValidator(id, name) {
			$(id).data('bootstrapValidator').updateStatus(name,
					'NOT_VALIDATED', null).validateField(name);
		}
		 });
</script>
</body>

</html>