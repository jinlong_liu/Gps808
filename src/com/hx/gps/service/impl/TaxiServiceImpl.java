package com.hx.gps.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hx.gps.dao.AddressDao;
import com.hx.gps.dao.InfoDao;
import com.hx.gps.dao.TaxiDao;
import com.hx.gps.entities.Driver;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.tool.Address;
import com.hx.gps.entities.tool.TaxiInfo2;
import com.hx.gps.service.TaxiService;
import com.hx.gps.util.DBUtil;
import com.hx.gps.util.DateUtil;
import com.hx.gps.util.FarmConstant;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.QueryOperators;

@Service
public class TaxiServiceImpl implements TaxiService {
	@Autowired
	private AddressDao addressDao;
	@Autowired
	private TaxiDao taxiDao;

	@Override
	public List<Taxi> getAllTaxi() {

		return taxiDao.getAllTaxi();
	}

	@Override
	public String getTaxiNumByIsuNum(String isuNum) {
		return taxiDao.getTaxiByIsu(isuNum).getTaxiNum();
	}

	@Override
	public List<Driver> getDriverOfTaxi(Integer id) {

		return taxiDao.getDriverByTaxiId(id);
	}

	@Override
	public Taxi getIsuByTaxi(String taxiNum) {
		return taxiDao.getTaxiByTaxi(taxiNum);
	}

	@Override
	public String getIsuByid(Integer id) {
		return taxiDao.getIsuById(id);
	}

	@Override
	public int setTaxiStateByIsuNums(Map<String, TaxiInfo2> stateMap) {
		// mongo 在线状态表。
		DBCollection find = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.onlineTemp);
		DBCollection find2 = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ADDRESSTRMP);
		BasicDBObject query = new BasicDBObject();
		// 根据终端号集合获取在线状态 在线状态的判断由在线实时表来得出。（时差300秒以内视为在线）
		Date time = new Date(000000);
		if (stateMap.size() > 0) {
			query.append("isuNum", new BasicDBObject().append(QueryOperators.IN, stateMap.keySet().toArray()));
		} else {
			return 0;
		}
		DBCursor cursor = find.find(query);
		DBCursor cursor2 = find2.find(query);
		int count = 0;
		while (cursor.hasNext()) {
			BasicDBObject bdo = (BasicDBObject) cursor.next();
			Date isuTime = bdo.getDate("time");
			if (isuTime.getTime() >= DateUtil.stringToDate(DateUtil.countDate(FarmConstant.ONLINE)).getTime()) {
				if (stateMap.get(bdo.getString("isuNum")) != null) {
					stateMap.get(bdo.getString("isuNum")).setOnlineState("在线");
				}
				count++;
			} else {
				if (stateMap.get(bdo.getString("isuNum")) != null) {
					stateMap.get(bdo.getString("isuNum")).setOnlineState("不在线");
					stateMap.get(bdo.getString("isuNum")).setPositioningState("未定位");
				}
			}
		}
		while (cursor2.hasNext()) {
			BasicDBObject bdo = (BasicDBObject) cursor2.next();
			Date isuTime = bdo.getDate("time");
			if (bdo.containsField("time")) {
				isuTime = bdo.getDate("time");
			} else {
				isuTime = time;
			}
			Character sta = null;
			String state = bdo.getString("state");
			String s1 = state.substring(6, 8);
			Integer i1 = Integer.decode("0x" + s1);
			byte b1 = i1.byteValue();
			String ss = AddressServiceImpl.toBinaryString(b1);
			char[] c = ss.toCharArray();
			sta = c[6];
			if (isuTime.getTime() >= DateUtil.stringToDate(DateUtil.countDate(FarmConstant.ONLINE)).getTime()) {
				if (Objects.equals(sta, '1')) {
					if (stateMap.get(bdo.getString("isuNum")) != null) {
						stateMap.get(bdo.getString("isuNum")).setPositioningState("定位");
					}
				} else {
					if (stateMap.get(bdo.getString("isuNum")) != null) {
						stateMap.get(bdo.getString("isuNum")).setPositioningState("未定位");
					}
				}
			} else {
				if (stateMap.get(bdo.getString("isuNum")) != null) {
					stateMap.get(bdo.getString("isuNum")).setPositioningState("未定位");
				}
			}
		}
		return count;
	}

	@Override
	public boolean setTaxiStateByIsuNum(TaxiInfo2 taxiInfo2, String isuNum) {
		// mongo 在线状态表。
		DBCollection find = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.onlineTemp);
		BasicDBObject query = new BasicDBObject();

		// 根据终端号获取在线状态 在线状态的判断由在线实时表来得出。（时差300秒以内视为在线）
		Date time = new Date(000000);
		query.append("isuNum", isuNum);
		DBCursor cursor = find.find(query);
		if (cursor.hasNext()) {
			DBObject object = cursor.next();
			time = (Date) object.get("time");
		}
		// System.out.println("time" + time);
		// System.out.println("gy_终端号：" + isuNum);
		// 根据终端号获取车辆的定位状态
		DBCollection find2 = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ADDRESSTRMP);
		// 避免空指针问题。
		if (find2.find(query).hasNext()) {
			DBObject object = find2.findOne(query);
			Character sta = null;
			if (object != null) {
				// System.out.println("gy:" + object.toString());
				String state = (String) object.get("state");
				String s1 = state.substring(6, 8);
				Integer i1 = Integer.decode("0x" + s1);
				byte b1 = i1.byteValue();
				String ss = AddressServiceImpl.toBinaryString(b1);
				char[] c = ss.toCharArray();
				sta = c[6];
			}

			if (time.getTime() >= DateUtil.stringToDate(DateUtil.countDate(FarmConstant.ONLINE)).getTime()) {
				taxiInfo2.setOnlineState("在线");
				// 定位判断修改为通过最近一次定位的时间差来判断 两分钟以内视为条件:
				if (object.containsField("time")) {
					Date time2 = (Date) object.get("time");
					if (time2.getTime() >= DateUtil.stringToDate(DateUtil.countDate(360)).getTime()) {
						// 获取到
						if (Objects.equals(sta, '1')) {
							taxiInfo2.setPositioningState("定位");
						} else {
							taxiInfo2.setPositioningState("未定位");
						}

					} else {
						taxiInfo2.setPositioningState("未定位");
					}
				} else {
					taxiInfo2.setPositioningState("未定位");
				}
				query.clear();
				return true;
			} else {
				// 时差超过两分钟无定位数据，判断为不在线不定位。
				taxiInfo2.setOnlineState("离线");
				taxiInfo2.setPositioningState("未定位");
				query.clear();
				return false;
			}
		} else {
			// 如果无记录，判断为不在线，不定位。
			taxiInfo2.setOnlineState("离线");
			taxiInfo2.setPositioningState("未定位");
			return false;
		}
	}

	@Override
	public List<Address> getOnlineCarsByCompys(Integer type, List<Integer> companyIDs) {
		int lasttime = 0;
		// 开始解析查询条件(类型)
		switch (type) {
		case 0:
			lasttime = 5 * 60;
			break;
		case 1:
			lasttime = 2 * 60 * 60;
			break;
		case 2:
			lasttime = 24 * 60 * 60;
			break;
		case 3:
			lasttime = 48 * 60 * 60;
			break;
		default:
			break;
		}
		List<String> isuNums = new ArrayList<>();
		isuNums = taxiDao.getIsuNumsByPany(companyIDs);
		List<Address> address = new ArrayList<>();

		for (String isuNum : isuNums) {
			Address a = addressDao.findAddressByIsuNum(isuNum);
			if (a != null) {
				address.add(a);
			}
		}
		// 验证地址信息是否为空
		if (address.size() == 0) {
			return null;
		}
		// 在线状态表。
		DBCollection find = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.onlineTemp);
		BasicDBObject query = new BasicDBObject();

		Iterator<Address> it = address.iterator();
		while (it.hasNext()) {
			Address a = it.next();
			// 查询条件
			Date time = new Date(000000);
			System.out.println("time" + time);
			query.append("isuNum", a.getIsuNum());
			// System.out.println("手机号"+a.getIsuNum());

			if (find.find(query).hasNext()) {
				DBObject object = find.findOne(query);
				time = (Date) object.get("time");
				// System.out.println("time为"+time);
			}

			Taxi t = taxiDao.getTaxiByIsu(a.getIsuNum());
			a.setTaxiNum(t.getTaxiNum());
			query.clear();
			// 在线状态的判断由在线实时表来得出。（时差300秒以内视为在线）
			if (time.getTime() < DateUtil.stringToDate(DateUtil.countDate(lasttime)).getTime()) {
				it.remove();
				continue;
			}
			a.setState("在线");
		}
		return address;
	}

	// 根据公司id获取在线车辆数
	@Override
	public Integer getOnlineNumByCom(int id) {
		int lasttime = 5 * 60;
		int number = 0;
		List<String> taxiNums = new ArrayList<>();
		List<String> isuNums = new ArrayList<>();
		List<Integer> ids = new ArrayList<>();
		ids.add(id);
		if (taxiDao.getTaxiNumsByPany(ids) != null) {
			taxiNums = taxiDao.getTaxiNumsByPany(ids);
		}
		for (String taxiNum : taxiNums) {
			String isu = taxiDao.getTaxiByTaxi(taxiNum).getIsuNum();
			isuNums.add(isu);
		}
		// 在线状态表。
		DBCollection find = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.onlineTemp);
		Iterator<String> it = isuNums.iterator();
		while (it.hasNext()) {
			String a = it.next();
			// 查询条件
			BasicDBObject query = new BasicDBObject();
			Date time = new Date(000000);
			query.append("isuNum", a);
			if (find.find(query).hasNext()) {
				DBObject object = find.findOne(query);
				time = (Date) object.get("time");
			}
			// 在线状态的判断由在线实时表来得出。（时差300秒以内视为在线）
			if (time.getTime() > DateUtil.stringToDate(DateUtil.countDate(lasttime)).getTime()) {
				number += 1;
			}
		}
		return number;
	}

	@Override
	public List<Address> getOfflineCarsByCompys(Integer type, List<Integer> companyIDs) {
		int lasttime = 0;
		// 开始解析查询条件
		switch (type) {
		case 0:
			lasttime = 5 * 60;
			break;
		case 1:
			lasttime = 2 * 60 * 60;
			break;
		case 2:
			lasttime = 24 * 60 * 60;
			break;
		case 3:
			lasttime = 48 * 60 * 60;
			break;
		default:
			break;
		}
		List<String> isuNums = new ArrayList<>();
		isuNums = taxiDao.getIsuNumsByPany(companyIDs);
		List<Address> address = new ArrayList<>();

		for (String isuNum : isuNums) {
			Address a = addressDao.findAddressByIsuNum(isuNum);
			if (a != null) {
				address.add(a);
			}
		}
		// 验证地址信息是否为空
		if (address.size() == 0) {
			return null;
		}
		// 在线状态表。
		DBCollection find = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.onlineTemp);
		BasicDBObject query = new BasicDBObject();

		Iterator<Address> it = address.iterator();
		while (it.hasNext()) {
			Address a = it.next();
			// 查询条件
			Date time = new Date(000000);
			System.out.println("time" + time);
			query.append("isuNum", a.getIsuNum());
			// System.out.println("手机号"+a.getIsuNum());

			if (find.find(query).hasNext()) {
				DBObject object = find.findOne(query);
				time = (Date) object.get("time");
				// System.out.println("time为"+time);
			}

			Taxi t = taxiDao.getTaxiByIsu(a.getIsuNum());
			a.setTaxiNum(t.getTaxiNum());
			query.clear();
			// 在线状态的判断由在线实时表来得出。（时差300秒以内视为在线）
			if (time.getTime() >= DateUtil.stringToDate(DateUtil.countDate(lasttime)).getTime()) {
				it.remove();
				continue;
			}
			a.setState("不在线");
		}
		return address;
	}

}
