<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<script src="js/jquery.js"></script>
<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">

<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>

<!-- gy 添加表单验证 -->
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />
<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>

<!-- bootstrap table -->
<link
	href="${pageContext.request.contextPath}/css/plugins/bootstrap-table/bootstrap-table.min.css"
	rel="stylesheet">
<script
	src="${pageContext.request.contextPath}/js/plugins/bootstrap-table/bootstrap-table.min.js"></script>

<script
	src="${pageContext.request.contextPath}/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">
		<!-- 查询列表 -->
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>输入条件查询车辆信息</h5>
				</div>
				<div class="ibox-content">
					<!-- 					<form action="getTaxi" class="form-horizontal" method="post"> -->
					<form id="form_query" class="form-horizontal"
						enctype="multipart/form-data">
						<div class="row">
							<!-- gy -->
							<div class="form-group">
								<!-- 公司 -->
								<label class="col-sm-1 control-label">公司：</label>
								<div class="col-sm-2">
									<select id="comId" name="comId" class="form-control">
										<option id="taxi_query_condition_default" value="0">请选择公司</option>
										<c:forEach items="${companies}" var="companies">
											<option value="${companies.id }">${companies.name }</option>
										</c:forEach>
									</select>
								</div>
								<!-- 车牌号 -->
								<label class="col-sm-1 control-label">车牌号码：</label>
								<div class="col-sm-2">
									<input type="text" class="form-control" name="taxiNum"
										value="${taxiNumSelect}" id="taxiNumSel">
								</div>
								<!-- 车主姓名 -->
								<label class="col-sm-1 control-label">车主姓名：</label>
								<div class="col-sm-2">
									<input type="text" class="form-control" name="masterName"
										value="${masterNameSelect}" id="masterNameSel">
								</div>
							</div>
						</div>
						<!-- 第二行 -->
						<div class="row">
							<div class="form-group">
								<!-- 终端号 -->
								<label class="col-sm-1 control-label">终端号：</label>
								<div class="col-sm-2">
									<input type="text" name="isuNum" class="form-control"
										value="${isuNumSelect}" id="isuNumSel" maxlength="12">
								</div>
								<!-- 终端序列号 -->
								<label class="col-sm-1 control-label">序列号：</label>
								<div class="col-sm-2">
									<input type="text" class="form-control" name="isuPhone"
										value="${isuPhoneSelect}" id="isuPhoneSel">
								</div>

								<div class="col-sm-6">
									<button type="button" class="btn btn-primary"
										onclick="delAll()">清空查询条件</button>
									<button id="btn_query" type="button" class="btn btn-primary">查询</button>
									<shiro:hasPermission name="car:admin">
										<button id="addTaxi" type="button" class="btn btn-primary"
											data-toggle="modal" data-target="#myModal5">添加</button>
										<button type="button" class="btn btn-primary"
											data-toggle="modal" data-target="#myModal9">批量添加...</button>
										<button id="btn-export" type="button" class="btn btn-primary">数据导出</button>
									</shiro:hasPermission>
								</div>
							</div>
						</div>
					</form>

				</div>
			</div>
		</div>
		<!-- 点击批量添加按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal9" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<!-- 莱芜测试版，批量添加 -->
					<form action="uploadAndEditTaxi" method="post" id="uploadForm"
						enctype="multipart/form-data" onsubmit="return check()">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>批量新增车辆信息</h5>

							</div>
							<div class="ibox-content">
								<div class="row">
									<div class="col-sm-4">


										<input id="f_file" class="btn btn-white btn-sm" type="file"
											name="file" style="display: inline;"
											onchange="selectFile(this)"> <span id="comErr"
											style="color: red; font-size: 15px;"></span>
									</div>

								</div>

							</div>
							<div class="modal-footer">
								<div class="pull-right">
									<input type="submit" class="btn btn-primary"
										style="display: inline;" value="保存">
									<button type="button" class="btn btn-white "
										data-dismiss="modal">关闭</button>
								</div>
							</div>
						</div>
						<!-- <div class="modal-footer">
							<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
							<button type="button" class="btn btn-primary" id="saveBtn">保存</button>
						</div> -->
					</form>
					<script type="text/javascript">
						function selectFile(fnUpload) {
							var filename = fnUpload.value;
							var mime = filename.toLowerCase().substr(filename.lastIndexOf("."));
							if (mime != ".xls") {
								alert("请选择XLS类型文件");
								fnUpload.outerHTML = fnUpload.outerHTML;
							}
						}
						function check() {
							var str = document.getElementById("f_file").value;
							if (str.length == 0) {
								alert("文件不能为空，请选择XLS类型文件");
								return false;
							}
							return true;
						}
					</script>
				</div>
			</div>
		</div>
		<!--gy 修改后 点击添加按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal5" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form id="saveForm" class="form-horizontal" action="saveTaxi" method="post" onsubmit="return check(this)">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>
									新增车辆信息<span style="color:red">（* 为必填项）</span>
								</h5>
							</div>
							<div class="ibox-content">
								<!--第一行 -->
								<div class="row">
									<!-- 公司名 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>公司 ：</label>
											<div class="col-sm-8">
												<select class="form-control" name="company.id">
													<c:forEach items="${companies}" var="companies">
														<option value="${companies.id }">${companies.name }</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
									<!-- 车型-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>车型 ：</label>
											<div class="col-sm-8">
												<select class="form-control" name="taxiType"
													id="taxi_add_type">
													<option value="null" selected="selected">请选择车型</option>
													<c:forEach items="${taxitypes}" var="taxitypes"
														varStatus="status">
														<option
															<%-- <c:if test="${status.index == 0}">selected='true'</c:if> --%> value="${taxitypes.type }"
															carSize="${taxitypes.lengthOfCar}米*${taxitypes.widthOfCar}米*${taxitypes.heightOfCar}米"">
															${taxitypes.type }</option>

													</c:forEach>
												</select>
											</div>
										</div>
									</div>
								</div>

								<!-- 第二行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 车辆颜色-->
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>车辆颜色 ：</label>
											<div class="col-sm-8">
												<select class="form-control" name="taxiColor"
													id="color-select">
													<option value="">请选择颜色</option>
													<option value="黑色">黑色</option>
													<option value="白色">白色</option>
													<option value="红色">红色</option>
													<option value="黄色">黄色</option>
													<option value="蓝色">蓝色</option>
													<option value="绿色">绿色</option>
													<option value="橙色">橙色</option>
													<option value="紫色">紫色</option>
													<option value="青色">青色</option>
												</select>
											</div>
										</div>
									</div>
									<!--车牌号 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>车牌号 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="taxiNum" autocomplete="off"
													id="saveTaxiNum">
											</div>
										</div>
									</div>
								</div>
								<!-- 第三行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!--终 端 号 -->
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>终端号 ：</label>
											<div class="col-sm-8">
												<input class="form-control" type="text" maxlength="12" autocomplete="off"
													name="isuNum" id="saveIsuNum">
											</div>
										</div>
									</div>
									<!--车 架 号 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>车架号 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="taxiFrame" autocomplete="off">
											</div>
										</div>
									</div>
								</div>
								<!-- 第四行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 车主姓名-->
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>车主姓名 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="masterName" autocomplete="off">
											</div>
										</div>
									</div>

									<!-- 车主身份证号-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>车主身份证号 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="masterIdNum" autocomplete="off">
											</div>
										</div>
									</div>
								</div>

								<!-- 第五行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 车 主 手 机-->
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>车主手机 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="masterPhone" autocomplete="off">
											</div>
										</div>
									</div>
									<!-- 注 册 日 期-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>注册日期 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="registerDate" autocomplete="off"
													onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(saveForm, name.valueOf())}})">
											</div>
										</div>
									</div>
								</div>

								<!-- 第六行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 运营开始日期-->
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>运营开始日期 ：</label>
											<div class="col-sm-8">
												<input type="datetime" name="operStartDate"
													class="form-control" autocomplete="off"
													onfocus="WdatePicker({lang:'zh-cn' ,dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(saveForm, name.valueOf())}})">
											</div>
										</div>
									</div>
									<!-- 运营结束日期-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>运营结束日期 ：</label>
											<div class="col-sm-8">
												<input type="datetime" name="operEndDate"
													class="form-control" autocomplete="off"
													onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(saveForm, name.valueOf())}})">
											</div>
										</div>
									</div>
								</div>
								<!-- 第七行 -->
								<div class="row">
									<!-- 保险单号-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>保险单号 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="insuranceNum" autocomplete="off">
											</div>
										</div>
									</div>
									<!-- 保险期限-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>保险期限 ：</label>
											<div class="col-sm-8">
												<input type="text" name="insuranceTime" class="form-control" autocomplete="off"
													onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(saveForm, name.valueOf())}})">
											</div>
										</div>
									</div>
								</div>

								<!-- 第八行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 燃 料 类 型-->
											<label class="col-sm-4 control-label">燃料类型 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="fuelType" autocomplete="off">
											</div>
										</div>
									</div>
									<!-- 购置金额-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>购置金额 ：</label>
											<div class="col-sm-8">
												<input type="text" name="purAmount" class="form-control" autocomplete="off"
													id="savepurAmount">
											</div>
										</div>
									</div>
								</div>

								<!-- 第九行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 年审日期 -->
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>年审日期 ：</label>
											<div class="col-sm-8">
												<input type="text" name="auditDate" class="form-control" autocomplete="off"
													onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(saveForm, name.valueOf())}})">
											</div>
										</div>
									</div>
									<!-- 下次年审时间-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>下次年审时间 ：</label>
											<div class="col-sm-8">
												<input type="text" name="nextAuditDate" class="form-control" autocomplete="off"
													onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(saveForm, name.valueOf())}})">
											</div>
										</div>
									</div>
								</div>
								<!-- 第十行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 运输证发证机构-->
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>运输证发证机构 ：</label>
											<div class="col-sm-8">
												<input type="text" name="tranAgency" class="form-control" autocomplete="off">
											</div>
										</div>
									</div>
									<!--行 驶 证 号-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>行驶证号 ：</label>
											<div class="col-sm-8">
												<input type="text" name="drivIdNum" class="form-control" autocomplete="off">
											</div>
										</div>
									</div>
								</div>

								<!-- 第十一行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!--道路运输证号-->
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>道路运输证号 ：</label>
											<div class="col-sm-8">
												<input type="text" name="tranIdNum" class="form-control" autocomplete="off">
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label">车辆尺寸 ：</label>
											<div class="col-sm-8">
												<input id="car_add_carSize" type="text" name="carSize"
													class="form-control" autocomplete="off">
											</div>
										</div>
									</div>
								</div>
								<!-- 第十二行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!--发动机号-->
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>发动机号 ：</label>
											<div class="col-sm-8">
												<input type="text" name="engineNum" class="form-control" autocomplete="off">
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label">运营状态 ：</label>
											<div class="col-sm-8">
												<select class="form-control" name="operationStatus">
													<option value="运营">运营</option>
													<option value="更新失效">更新失效</option>
													<option value="更新新增">更新新增</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-warning btn-sm" id="clearForm"
											>清空数据</button>
									<button type="button" class="btn btn-white btn-sm"
										data-dismiss="modal">关闭</button>
									<button type="submit" class="btn btn-primary btn-sm"
										id="saveBtn">保存</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- 导出表格-gy-2018年10月31日 -->
		<div class="modal inmodal fade" id="modal-export" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-body">
						<div class="ibox-content">
							<span style="text-align: center;display:block;">数据加载中，请稍后。</span>
							<div class="spiner-example">
								<div class="sk-spinner sk-spinner-wave">
									<div class="sk-rect1"></div>
									<div class="sk-rect2"></div>
									<div class="sk-rect3"></div>
									<div class="sk-rect4"></div>
									<div class="sk-rect5"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- 数据展示 -->
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-title">
					<div>
						<h5>&nbsp;&nbsp;&nbsp;车辆信息表</h5>
					</div>
				</div>
				<div class="ibox-content" style="height:610px">

					<table id="table_result" width="100%"
						style="table-layout: fixed; background: #FFFFFF"
						class="footable table table-stripped" data-page-size="10">
					</table>
				</div>
			</div>
		</div>


		<!-- 删除表格 -->
		<form id="deleteForm" action="deleteTaxi" method="post">
			<input type="hidden" id="deleteId" name="id"> <input
				type="hidden" name="taxiNumDelete" id="taxiNumDelete">
		</form>
		<!-- gy 修改后 修改界面 -->
		<div class="modal inmodal fade" id="myModal1" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form id="updateTaxiForm" class="form-horizontal"
						action="updateTaxi" method="post">
						<input id="id" name="id" type="hidden">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>
									修改车辆信息 <span style="color:red">（* 为必填项）</span>
								</h5>
							</div>
							<div class="ibox-content">
								<!-- 第一行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 公 司 -->
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>公司：</label>
											<div class="col-sm-8">
												<select class="form-control" name="company.id"
													id="companySelect">
												</select>
											</div>
										</div>
									</div>
									<!-- 车 型-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>车型 ：</label>
											<div class="col-sm-8">
												<!-- <select class="form-control" name="taxiType"
													id="taxiTypeSelect">
												</select> -->
												<select class="form-control" name="taxiType"
													id="taxi_update_type">

												</select>
											</div>
										</div>
									</div>
								</div>
								<!-- 第二行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 车辆颜色 -->
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>车辆颜色 ：</label>
											<div class="col-sm-8">
												<select name="taxiColor" id="taxiColorSelect11"
													class="form-control">
													<option value="">请选择颜色</option>
													<option value="黑色">黑色</option>
													<option value="红色">红色</option>
													<option value="黄色">黄色</option>
													<option value="蓝色">蓝色</option>
													<option value="绿色">绿色</option>
													<option value="橙色">橙色</option>
													<option value="紫色">紫色</option>
													<option value="青色">青色</option>
												</select>

											</div>
										</div>
									</div>
									<!-- 车 牌 号-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>车牌号 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="taxiNum"
													id="taxiNum">
											</div>
										</div>
									</div>
								</div>
								<!-- 第三行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 终 端 号 -->
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>终端号 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="isuNum"
													id="isuNum">
											</div>
										</div>
									</div>
									<!-- 车 架 号-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>车架号 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="taxiFrame"
													id="taxiFrame">
											</div>
										</div>
									</div>
								</div>
								<!-- 第四行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 车 主 姓 名 -->
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>车主姓名 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="masterName"
													id="masterName">
											</div>
										</div>
									</div>
									<!-- 车主身份证号-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>车主身份证号 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="masterIdNum"
													id="masterIdNum">
											</div>
										</div>
									</div>
								</div>
								<!-- 第五行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 车 主 电 话-->
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>车主电话 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="masterPhone"
													id="masterPhone">
											</div>
										</div>
									</div>
									<!-- 注 册 日 期-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>注册日期 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="registerDate"
													id="registerDate"
													onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(updateTaxiForm, name.valueOf())}})">
											</div>
										</div>
									</div>
								</div>
								<!-- 第六行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 运营开始日期-->
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>运营开始日期 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="operStartDate"
													id="operStartDate"
													onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(updateTaxiForm, name.valueOf())}})">
											</div>
										</div>
									</div>
									<!-- 运营结束日期-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>运营结束日期 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="operEndDate"
													id="operEndDate"
													onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(updateTaxiForm, name.valueOf())}})">
											</div>
										</div>
									</div>
								</div>
								<!-- 第七行 -->
								<div class="row">
									<!-- 保险单号-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>保险单号 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control"
													id="taxi_update_insuranceNum" name="insuranceNum">
											</div>
										</div>
									</div>
									<!-- 保险期限-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>保险期限 ：</label>
											<div class="col-sm-8">
												<input type="text" name="insuranceTime" class="form-control"
													id="taxi_update_insuranceTime"
													onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(saveForm, name.valueOf())}})">
											</div>
										</div>
									</div>
								</div>
								<!-- 第八行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 行 驶 证 号-->
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>行驶证号 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="drivIdNum"
													id="drivIdNum">
											</div>
										</div>
									</div>
									<!-- 燃 料 类 型-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label">燃料类型：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="fuelType"
													id="fuelType">
											</div>
										</div>
									</div>
								</div>
								<!-- 第九行 -->
								<div class="row">

									<!-- 购置金额-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>购置金额 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="purAmount"
													id="purAmount">
											</div>
										</div>
									</div>
									<!-- 道路运输证号-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>道路运输证号 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="tranIdNum"
													id="tranIdNum">
											</div>
										</div>
									</div>
								</div>
								<!-- 第十行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 运输证发证机构-->
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>运输证发证机构 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="tranAgency"
													id="tranAgency">
											</div>
										</div>
									</div>
									<!-- 年 审 日 期-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>年审日期 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="auditDate"
													id="auditDate"
													onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(updateTaxiForm, name.valueOf())}})">
											</div>
										</div>
									</div>
								</div>
								<!-- 第十一行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 下次年审时间-->
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>下次年审时间 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="nextAuditDate"
													id="nextAuditDate"
													onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(updateTaxiForm, name.valueOf())}})">
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label">车辆尺寸 ：</label>
											<div class="col-sm-8">
												<input id="taxi_update_carSize" type="text" name="carSize"
													class="form-control">
											</div>
										</div>
									</div>
								</div>
								<!-- 第十二行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!--发动机号-->
											<label class="col-sm-4 control-label"><span
												style="color:red">* </span>发动机号 ：</label>
											<div class="col-sm-8">
												<input type="text" name="engineNum"
													id="taxi_update_engineNum" class="form-control">
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label">运营状态 ：</label>
											<div class="col-sm-8">
												<select class="form-control" name="operationStatus"
													id="taxi_update_operationStatus">
													<option value="运营">运营</option>
													<option value="更新失效">更新失效</option>
													<option value="更新新增">更新新增</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-white"
										data-dismiss="modal">关闭</button>
									<button type="submit" class="btn btn-primary">保存</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>




		<!-- 点击"查看详情"按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal2" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form action="updateTaxi" class="form-horizontal" method="post">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>查看车辆详情</h5>
							</div>
							<div class="ibox-content">
								<div class="row">
									<div class="col-sm-4">
										车 型：<span style="font-weight: bold" id="taxiType2"></span>
									</div>
									<div class="col-sm-4">
										公 司：<span style="font-weight: bold" id="company2"></span>
									</div>
									<div class="col-sm-4">
										颜色：<span style="font-weight: bold" id="taxiColor2"></span>
									</div>
								</div>
								<br>

								<div class="row">
									<div class="col-sm-4">
										车 牌 号： <span style="font-weight: bold" id="taxiNum2"></span>
									</div>
									<div class="col-sm-4">
										终 端 号：<span style="font-weight: bold" id="isuNum2"></span>
									</div>
									<div class="col-sm-4">
										车 架 号：<span style="font-weight: bold" id="taxiFrame2"></span>
									</div>
								</div>
								<br>

								<div class="row">
									<div class="col-sm-4">
										车 主 姓 名：<span style="font-weight: bold" id="masterName2"></span>
									</div>
									<div class="col-sm-4">
										车主身份证号： <span style="font-weight: bold" id="masterIdNum2"></span>
									</div>
									<div class="col-sm-4">
										车 主 电 话： <span style="font-weight: bold" id="masterPhone2"></span>
									</div>
								</div>
								<br>


								<div class="row">
									<div class="col-sm-4">
										注 册 日 期： <span style="font-weight: bold" id="registerDate2"></span>
									</div>
									<div class="col-sm-4">
										运营开始日期： <span style="font-weight: bold" id="operStartDate2"></span>
									</div>
									<div class="col-sm-4">
										运营结束日期： <span style="font-weight: bold" id="operEndDate2"></span>
									</div>
								</div>
								<br>

								<div class="row">
									<div class="col-sm-4">
										发 动 机 号： <span style="font-weight: bold"
											id="taxi_detail_engineNum"></span>
									</div>
									<div class="col-sm-4">
										购 置 金 额： <span style="font-weight: bold" id="purAmount2"></span>
									</div>
									<div class="col-sm-4">
										行 驶 证 号： <span style="font-weight: bold" id="drivIdNum2"></span>
									</div>
								</div>
								<br>

								<div class="row">
									<div class="col-sm-4">
										燃 料 类 型： <span style="font-weight: bold" id="fuelType2"></span>
									</div>
									<div class="col-sm-4">
										保 险 单 号： <span style="font-weight: bold"
											id="taxi_detail_insuranceNum"></span>
									</div>
									<div class="col-sm-4">
										道路运输证号码： <span style="font-weight: bold" id="tranIdNum2"></span>
									</div>
								</div>
								<br>


								<div class="row">
									<div class="col-sm-4">
										运输证发证机构： <span style="font-weight: bold" id="tranAgency2"></span>
									</div>
									<div class="col-sm-4">
										年 审 日 期： <span style="font-weight: bold" id="auditDate2"></span>
									</div>
									<div class="col-sm-4">
										下次年审时间： <span style="font-weight: bold" id="nextAuditDate2"></span>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-sm-4">
										保 险 期 限： <span style="font-weight: bold"
											id="taxi_detail_insuranceTime"></span>
									</div>
									<div class="col-sm-4">
										车 辆 尺 寸： <span style="font-weight: bold"
											id="taxi_detail_carSize"></span>
									</div>
									<div class="col-sm-4">
										营 运 状 态： <span style="font-weight: bold"
											id="taxi_detail_operationStatus"></span>
									</div>
								</div>


								<!-- 		<div class="row">
									<div class="col-sm-4">
										承 包&nbsp;人： <span style="font-weight: bold" id="conter2"></span>
									</div>
									<div class="col-sm-4">
										承包人身份证号： <span style="font-weight: bold" id="conterIdNum2"></span>
									</div>
									<div class="col-sm-4">
										承包人电话： <span style="font-weight: bold" id="conterPhone2"></span>
									</div>
								</div> -->
								<br>




								<div class="modal-footer">
									<button type="button" class="btn btn-dark" data-dismiss="modal">关闭</button>
									<!-- <button type="submit" class="btn btn-primary">保存</button> -->
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script src="js/jquery.editable-select.min.js"></script>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<!-- <script src="js/jquery.min.js?v=2.1.4"></script> -->
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
	<script>
		//监听变化-add模态框的车辆类型
		$("#taxi_update_type").change(function() {
			var carSize = $("#taxi_update_type option:selected").attr("carSize");
			//Console.log(carSize);
			//alert("---"+carSize);
			$("#taxi_update_carSize").text(carSize).val(carSize);
		});
		//监听变化-add模态框的车辆类型
		$("#taxi_add_type").change(function() {
			var carSize = $("#taxi_add_type option:selected").attr("carSize");
			//Console.log(carSize);
			//alert("---"+carSize);
			$("#car_add_carSize").val(carSize);
		});
		//添加编辑按钮和删除按钮
		function operateFormatter(value, row, index) {
			return [
				'<button id="btn-showinfo" type="button"  class="RoleOfDetial btn btn-info btn-sm" data-toggle="modal" data-target="#myModal2">详情</button><button id="btn-update" type="button" class="RoleOfEdit btn btn-success btn-sm" data-toggle="modal" data-target="#myModal1">修改</button><button type="button" class="RoleOfDelete btn btn-danger btn-sm">删除</button>',
			].join('');
		}
	
		//查看详情事件
		window.operateEvents = {
			//value:当前单元格的值；row:当前行；index:当前行的索引
			'click .RoleOfDetial' : function(e, value, row, index) {
				check2(row.taxiNum, row.taxiColor, row.masterName, row.masterIdNum
					, row.masterPhone, row.engineNum, row.isuPhone, row.taxiFrame, row.isuNum
					, row.registerDate, row.operStartDate, row.operEndDate, row.insuranceNum, row.purAmount
					, row.drivIdNum, row.fuelType, row.insuranceTime
					, row.tranIdNum, row.tranAgency, row.auditDate, row.nextAuditDate, row.conter
					, row.conterIdNum, row.conterPhone, row.taxiType, row.company.name, row.company.id, row.carSize, row.operationStatus);
			},
			'click .RoleOfEdit' : function(e, value, row, index) {
				update(row.id, row.taxiNum, row.taxiColor, row.masterName, row.masterIdNum, row.masterPhone, row.engineNum, row.isuPhone, row.taxiFrame, row.isuNum
					, row.registerDate, row.operStartDate, row.operEndDate, row.insuranceNum, row.purAmount
					, row.drivIdNum, row.fuelType, row.insuranceTime
					, row.tranIdNum, row.tranAgency, row.auditDate, row.nextAuditDate, row.conter
					, row.conterIdNum, row.conterPhone, row.taxiType, row.company.name, row.company.id, row.carSize, row.operationStatus);editCheck();
			},
			'click .RoleOfDelete' : function(e, value, row, index) {
				delete1(row.id, row.taxiNum);
			}
		}
		//请求服务数据时所传参数
		function queryParams(params) {
			var taxiNum = "";
			var masterName = "";
			var isuNum = "";
			var isuPhone = "";
	
			// 		if($('#comId').val() != "" && $('#comId').val()){}
	
			return {
				comId : $('#comId').val(),
				taxiNum : $('#taxiNumSel').val(),
				masterName : $('#masterNameSel').val(),
				isuNum : $('#isuNumSel').val(),
				isuPhone : $('#isuPhoneSel').val(),
				sort : params.sort,
				order : params.order,
				limit : params.limit, // 每页显示数量
				offset : params.offset, // SQL语句偏移量
			}
		}
		$("#clearForm").click(function () {
			$('#saveForm')[0].reset();
		})
		//查询按钮点击事件
		$('#btn_query').click(function() {
			//表单提交
			//先销毁表格再填充数据，避免第二次查询无法显示
			$("#table_result").bootstrapTable('destroy');
			//填充table数据
			$('#table_result').bootstrapTable({
				dataType : "json",
				//是否显示行间隔色  
				striped : true,
				pagination : true, //是否分页
				pageList : [ 5, 10, 20, 30 ], //分页页数选择  
				pageSize : 5, //默认每页的条数  
				pageNumber : 1,
				editable : false, //开启编辑模式
				uniqueId : 'index',
				queryParamsType : 'limit', //查询参数组织方式
				url : 'queryTaxi',
				queryParams : queryParams, //请求服务器时所传的参数
				sidePagination : 'server', //指定服务器端分页
				columns : [ {
					field : 'id',
					title : '序号',
					align : "center",
				// 					formatter : function(value, row, index) {
				// 						return index + 1;
				// 					}
				}, {
					field : 'company.name',
					sortable : true,
					title : '公司名称',
				}, {
					field : 'taxiNum',
					title : '车牌号',
				}, {
					field : 'taxiType',
					title : '车辆类型',
				}, {
					field : 'taxiColor',
					title : '车辆颜色',
				}, {
					field : 'masterName',
					title : '车主姓名',
				}, {
					field : 'masterIdNum',
					title : '车主身份证号',
				}, {
					field : 'masterPhone',
					title : '车主电话',
				}, {
					field : 'isuNum',
					title : '终端号',
				}, {
					title : '操作',
					visible : true,
					formatter : operateFormatter,
					events : operateEvents
				}
				],
			});
	
		});
		//清空查询框
		function delAll() {
			$("#taxiNumSel").val("");
			$("#masterNameSel").val("");
			$("#isuNumSel").val("");
			$("#isuPhoneSel").val("");
			$("#taxi_query_condition_default").attr("selected", "selected");
	
		}
		;
		//点击查看详情按钮
		function check2(taxiNum, taxiColor, masterName, masterIdNum, masterPhone,
			engineNum, isuPhone, taxiFrame, isuNum, registerDate,
			operStartDate, operEndDate, insuranceNum, purAmount, drivIdNum,
			fuelType, insuranceTime, tranIdNum, tranAgency, auditDate,
			nextAuditDate, conter, conterIdNum, conterPhone, taxiType, comName, comId, carSize, operationStatus) {
			$("#taxiType2").text(taxiType);
			$("#taxiColor2").text(taxiColor);
			$("#company2").text(comName);
			$("#taxiNum2").text(taxiNum);
			$("#masterName2").text(masterName);
			$("#masterIdNum2").text(masterIdNum);
	
			$("#masterPhone2").text(masterPhone);
			//$("#masterAddress2").text(masterAddress);
			$("#taxi_detail_engineNum").text(engineNum);
	
			$("#isuPhone2").text(isuPhone);
			$("#taxiFrame2").text(taxiFrame);
			$("#isuNum2").text(isuNum);
	
			$("#registerDate2").text(registerDate);
			$("#operStartDate2").text(operStartDate);
			$("#operEndDate2").text(operEndDate);
			//$("#purDate2").text(purDate);
			$("#taxi_detail_insuranceNum").text(insuranceNum);
	
			$("#purAmount2").text(purAmount);
	
			$("#drivIdNum2").text(drivIdNum);
			$("#fuelType2").text(fuelType);
			//$("#byCarDate2").text(byCarDate);
			$("#taxi_detail_insuranceTime").text(insuranceTime);
	
			$("#tranIdNum2").text(tranIdNum);
			$("#tranAgency2").text(tranAgency);
			$("#auditDate2").text(auditDate);
			$("#nextAuditDate2").text(nextAuditDate);
			$("#conter2").text(conter);
	
			$("#conterIdNum2").text(conterIdNum);
			$("#conterPhone2").text(conterPhone);
	
			$("#taxi_detail_carSize").text(carSize);
			$("#taxi_detail_operationStatus").text(operationStatus);
		}
	
		//点击"修改"按钮
		function update(id, taxiNum, taxiColor, masterName, masterIdNum, masterPhone,
			engineNum, isuPhone, taxiFrame, isuNum, registerDate,
			operStartDate, operEndDate, insuranceNum, purAmount, drivIdNum,
			fuelType, insuranceTime, tranIdNum, tranAgency, auditDate,
			nextAuditDate, conter, conterIdNum, conterPhone, taxiType, comName, comId, carSize, operationStatus) {
			$("#id").val(id);
			$("#taxiNum").val(taxiNum);
			$("#taxiColorSelect11").val(taxiColor);
			$("#masterName").val(masterName);
			$("#masterIdNum").val(masterIdNum);
	
	
			$("#masterPhone").val(masterPhone);
			//$("#masterAddress").val(masterAddress);
			$("#isuPhone").val(isuPhone);
			$("#taxiFrame").val(taxiFrame);
			$("#isuNum").val(isuNum);
	
			$("#registerDate").val(registerDate);
			$("#operStartDate").val(operStartDate);
			$("#operEndDate").val(operEndDate);
			//$("#purDate").val(purDate);
			$("#purAmount").val(purAmount);
	
			$("#drivIdNum").val(drivIdNum);
			$("#fuelType").val(fuelType);
			//$("#byCarDate").val(byCarDate);
	
			$("#tranIdNum").val(tranIdNum);
			$("#tranAgency").val(tranAgency);
			$("#auditDate").val(auditDate);
			$("#nextAuditDate").val(nextAuditDate);
			$("#conter").val(conter);
	
			$("#conterIdNum").val(conterIdNum);
			$("#conterPhone").val(conterPhone);
	
			$("#taxi_update_engineNum").val(engineNum);
			$("#taxi_update_insuranceNum").val(insuranceNum);
			$("#taxi_update_insuranceTime").val(insuranceTime);
			$("#taxi_update_carSize").val(carSize);
			$("#taxi_update_operationStatus").val(operationStatus);
	
			//ajax查出所有的车型，回显到"修改"页面，然后根据对应，去显示
			$.post("getAllTaxiTypeByAjaxWithJson", function(data) {
				if (data.length != 0) { //返回的数据不为空 
					$("#taxi_update_type").html(""); //避免选择框中的值持续叠加
					//判断一下目前是否是空值
					/* if(taxiType==null || taxiType.equal("")|| taxiType.equal("null")){//未选择车辆类型
						$("<option> " + 请选择车辆类型 + "</option>")
						.appendTo($("#taxi_update_type"));
						$.each(data,function(index,item){
								$("<option> " + item.type + "</option>").attr("carSize",item.lengthOfCar+"米*"+item.widthOfCar+"米*"+item.heightOfCar+"米").
								appendTo($("#taxi_update_type"));
						});
					}else{} */
					//alert(taxiType);
					//遍历返回的list-json，取出值。
					$("<option> " + "请选择车辆类型" + "</option>").appendTo($("#taxi_update_type"));
					$.each(data, function(index, item) {
						if (item.type != taxiType) {
							$("<option> " + item.type + "</option>").attr("carSize", item.lengthOfCar + "米*" + item.widthOfCar + "米*" + item.heightOfCar + "米").appendTo($("#taxi_update_type"));
						} else {
							$("<option> " + item.type + "</option>")
								.attr("carSize", item.lengthOfCar + "米*" + item.widthOfCar + "米*" + item.heightOfCar + "米")
								.attr("selected", true)
								.appendTo($("#taxi_update_type"));
							$("#taxi_update_carSize").val(item.lengthOfCar + "米*" + item.widthOfCar + "米*" + item.heightOfCar + "米");
						}
					});
	
	
				}
			});
	
			//ajax查出所有的公司，回显到"修改"页面
			$.post("getAllComByAjax", function(data) {
				if (data.length != 0) { //返回的数据不为空 
					var newdata = eval("(" + data + ")");
					$("#companySelect").html(""); //避免选择框中的值持续叠加
					/* $("<option value='"+taxiId+"'>" + taxiNum + "</option>")
							.appendTo($("#taxiSelect")); */
					$("<option value='" + comId + "'>" + comName + "</option>")
						.appendTo($("#companySelect")); //数据回显
					for (var i = 0; i < data.length; i++) { //数据库所有数据
	
						if (newdata[i].cname != comName) {
							$("<option value ='" + newdata[i].comId +
								"'> "
								+ newdata[i].cname + "</option>")
								.appendTo($("#companySelect"));
						}
					}
				}
			});
		}
		;
	
	
	
		//点击"删除"按钮
		function delete1(id, name) {
			swal({
				title : "您确定删除 " + name + " 的数据吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
				var s1 = $("#taxiNumSelect").val();
				$("#taxiNumDelete").val(s1);
				$("#deleteId").val(id);
				// 	提交表单
				$("#deleteForm").submit();
	
			});
		}
	
	
	
	
		//页面加载完成后执行,获取执行状态
		$(document).ready(function() {
			var result = '<%=request.getAttribute("result")%>';
			if (result != "null") {
				if (result.indexOf("成功") > -1) {
					swal({
						title : result,
						type : "success"
					});
				} else {
					swal({
						title : result,
						type : "warning"
					});
				}
			}
		});
		$(document).ready(function() {
			$('#table1').DataTable({
				"pagingType" : "full_numbers"
			});
		});
	</script>

	<script type="text/javascript">
		/*gy 添加界面表单验证*/
		/*添加车辆界面的表单验证*/
		$('#addTaxi').click(function() {
			$('#saveForm').bootstrapValidator({
				feedbackIcons : { /*输入框不同状态，显示图片的样式*/
					valid : 'glyphicon glyphicon-ok',
					invalid : 'glyphicon glyphicon-remove',
					validating : 'glyphicon glyphicon-refresh'
				},
				/*生效规则：字段值一旦变化就触发验证*/
				live : 'enabled',
				/*当表单验证不通过时，该按钮为disabled*/
				submitButtons : 'button[type="submit"]',
				/*验证*/
				fields : {
					//李泽添加新的4元素，去掉之前的购车日期，购置日期，车主地址
					insuranceNum : {
						validators : {
							notEmpty : {
								message : '保险号不能为空'
							}
						}
					},
					insuranceTime : {
						validators : {
							notEmpty : {
								message : '保险期限不能为空'
							}
						}
					},
					carSize : {
						validators : {
							notEmpty : {
								message : '车辆尺寸不能为空'
							}
						}
					},
					engineNum : {
						validators : {
							notEmpty : {
								message : '发动机号不能为空'
							}
						}
					},
					//到此添加结束
					taxiNum : { /*键名username和input name值对应*/
						message : 'The username is not valid',
						validators : {
							notEmpty : { /*非空提示*/
								message : '车牌号不能为空'
							}
						}
					},
					isuNum : {
						validators : {
							notEmpty : {
								message : '终端号不能为空'
							},
							regexp : {
								regexp : /^0[0-9]{11}$/,
								message : '请输入正确的终端号(以0开头,12位数字)'
							}
						}
					},
					taxiFrame : {
						validators : {
							notEmpty : {
								message : '车架号不能为空'
							}
						}
					},
					masterName : {
						validators : {
							notEmpty : {
								message : '车主姓名不能为空'
							}
						}
					},
					masterIdNum : {
						validators : {
							notEmpty : {
								message : '车主身份证号码不能为空'
							}
						}
					},
					masterPhone : {
						validators : {
							notEmpty : {
								message : '手机号码不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					registerDate : {
						validators : {
							notEmpty : {
								message : '注册日期不能为空'
							}
						}
					},
					operStartDate : {
						validators : {
							notEmpty : {
								message : '运营开始日期不能为空'
							}
						}
					},
					operEndDate : {
						validators : {
							notEmpty : {
								message : '运营结束日期不能为空'
							}
						}
					},
					auditDate : {
						validators : {
							notEmpty : {
								message : '年审日期不能为空'
							}
						}
					},
					purAmount : {
						validators : {
							notEmpty : {
								message : '购置金额不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					nextAuditDate : {
						validators : {
							notEmpty : {
								message : '下次年审日期不能为空'
							}
						}
					},
					tranAgency : {
						validators : {
							notEmpty : {
								message : '运输证发证机构不能为空'
							}
						}
					},
					drivIdNum : {
						validators : {
							notEmpty : {
								message : '行驶证号不能为空'
							}
						}
					},
					tranIdNum : {
						validators : {
							notEmpty : {
								message : '道路运输证号不能为空'
							}
						}
					},
					conter : {
						validators : {
							notEmpty : {
								message : '承包人不能为空'
							}
						}
					},
					conterIdNum : {
						validators : {
							notEmpty : {
								message : '承包人身份证号不能为空'
							}
						}
					},
					conterPhone : {
						validators : {
							notEmpty : {
								message : '承包人电话不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					}
				}
			});
		});
		/*修改界面验证*/
		function editCheck() {
			$('#updateTaxiForm').bootstrapValidator({
				feedbackIcons : { /*输入框不同状态，显示图片的样式*/
					valid : 'glyphicon glyphicon-ok',
					invalid : 'glyphicon glyphicon-remove',
					validating : 'glyphicon glyphicon-refresh'
				},
				/*生效规则：字段值一旦变化就触发验证*/
				live : 'enabled',
				/*当表单验证不通过时，该按钮为disabled*/
				submitButtons : 'button[type="submit"]',
				/*验证*/
				fields : {
	
					//李泽添加新的4元素，去掉之前的购车日期，购置日期，车主地址
					insuranceNum : {
						validators : {
							notEmpty : {
								message : '保险号不能为空'
							}
						}
					},
					insuranceTime : {
						validators : {
							notEmpty : {
								message : '保险期限不能为空'
							}
						}
					},
					engineNum : {
						validators : {
							notEmpty : {
								message : '发动机号不能为空'
							}
						}
					},
					//到此添加结束
					taxiNum : { /*键名username和input name值对应*/
						message : 'The username is not valid',
						validators : {
							notEmpty : { /*非空提示*/
								message : '车牌号不能为空'
							}
						}
					},
					isuNum : {
						validators : {
							notEmpty : {
								message : '终端号不能为空'
							},
							regexp : {
								regexp : /^0[0-9]{11}$/,
								message : '请输入正确的终端号(以0开头,12位数字)'
							}
						}
					},
					taxiFrame : {
						validators : {
							notEmpty : {
								message : '车架号不能为空'
							}
						}
					},
					masterName : {
						validators : {
							notEmpty : {
								message : '车主姓名不能为空'
							}
						}
					},
					masterIdNum : {
						validators : {
							notEmpty : {
								message : '车主身份证号码不能为空'
							}
						}
					},
					masterPhone : {
						validators : {
							notEmpty : {
								message : '手机号码不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					registerDate : {
						validators : {
							notEmpty : {
								message : '注册日期不能为空'
							}
						}
					},
					operStartDate : {
						validators : {
							notEmpty : {
								message : '运营开始日期不能为空'
							}
						}
					},
					operEndDate : {
						validators : {
							notEmpty : {
								message : '运营结束日期不能为空'
							}
						}
					},
					auditDate : {
						validators : {
							notEmpty : {
								message : '年审日期不能为空'
							}
						}
					},
					purAmount : {
						validators : {
							notEmpty : {
								message : '购置金额不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					nextAuditDate : {
						validators : {
							notEmpty : {
								message : '下次年审日期不能为空'
							}
						}
					},
					tranAgency : {
						validators : {
							notEmpty : {
								message : '运输证发证机构不能为空'
							}
						}
					},
					drivIdNum : {
						validators : {
							notEmpty : {
								message : '行驶证号不能为空'
							}
						}
					},
					tranIdNum : {
						validators : {
							notEmpty : {
								message : '道路运输证号不能为空'
							}
						}
					},
					conter : {
						validators : {
							notEmpty : {
								message : '承包人不能为空'
							}
						}
					},
					conterIdNum : {
						validators : {
							notEmpty : {
								message : '承包人身份证号不能为空'
							}
						}
					},
					conterPhone : {
						validators : {
							notEmpty : {
								message : '承包人电话不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					}
				}
			});
		}
		;
		/*日历刷新验证*/
		function refreshValidator(id, name) {
			$(id).data('bootstrapValidator').updateStatus(name,
				'NOT_VALIDATED', null).validateField(name);
		}
	
		/* 导出表格点击事件-gy-2018年10月31日 */
		$("#btn-export").click(function() {
			//发送ajax请求
			window.location.href = "${pageContext.request.contextPath}/exportTaxiData";
			// 			$.ajax({
			// 				type : 'POST',
			// 				url : '${pageContext.request.contextPath}/exportTaxiData',
			// 				data : file,
			// 				beforeSend : function() {
			// 					$('#modal-export').modal('show');
			// 				},
			// 				complete : function() {
			// 					$('#modal-export').modal('hide');
			// 				},
			// 				success : function(data) {
	
		// 				}
		// 			})
		})
	</script>
	<script type="text/javascript">
		//声明全局数组，用于存放取值
		var inputArr = document.getElementsByTagName("input");

		function check(o)
		{
			var nameStr = "";
			for (var i = 0; i < inputArr.length-1; i++)
			{
				nameStr += inputArr[i].value + ";";
			}
			nameStr += inputArr[inputArr.length - 1];
			window.name = nameStr;
		}

		if (window.name)
		{
			//声明数组，用于存放从window.name中分离出的值
			var nameArr = new Array();
			nameArr = window.name.split(";");

			for (var i = 0; i < nameArr.length; i++)
			{
				if (inputArr[i].type == "text")
				{
					inputArr[i].value = nameArr[i];
				}
			}
		}
	</script>
</body>

</html>