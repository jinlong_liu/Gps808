/**
 * 
 */
package com.hx.gps.service;

import com.hx.gps.entities.User;

/**
 * @author Minazuki
 *
 */
public interface UserService {
	
	//用户登录
	public User login(User user);
		
	//修改密码
	public void updatePass(User user,String newPassword);
	
	public User findUserByName(String username);
}
