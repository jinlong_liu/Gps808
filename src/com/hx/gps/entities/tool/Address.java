package com.hx.gps.entities.tool;

import com.hx.gps.util.DateUtil;
import com.mongodb.BasicDBObject;

public class Address {

	private String isuNum;
	private String taxiNum;	//数据库中不包含车牌号，添加该属性只是为了方便数据的传输
	private String state;	//数据库中不包含，用于显示车辆的在线状态
	private String locateState;	//数据库中不包含，表示定位状态
	private String operateState;//数据库中不包含，运营状态
	private int serialNum;
	private String alarm;
	private double longitude;
	private double latitude;
	private float speed;
	private int direction;
	private String time;
	
	public String getIsuNum() {
		return isuNum;
	}
	public void setIsuNum(String isuNum) {
		this.isuNum = isuNum;
	}

	public String getTaxiNum() {
		return taxiNum;
	}
	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	public String getLocateState() {
		return locateState;
	}
	public void setLocateState(String locateState) {
		this.locateState = locateState;
	}
	public String getOperateState() {
		return operateState;
	}
	public void setOperateState(String operateState) {
		this.operateState = operateState;
	}
	public int getSerialNum() {
		return serialNum;
	}
	public void setSerialNum(int serialNum) {
		this.serialNum = serialNum;
	}
	public String getAlarm() {
		return alarm;
	}
	public void setAlarm(String alarm) {
		this.alarm = alarm;
	}
	/*public String getState() {
		String s1 = state.substring(0,2);
		String s2 = state.substring(2,4);
		String s3 = state.substring(4,6);
		String s4 = state.substring(6,8);

		Integer i1 = Integer.decode("0x"+s1);
		Integer i2 = Integer.decode("0x"+s2);
		Integer i3 = Integer.decode("0x"+s3);
		Integer i4 = Integer.decode("0x"+s4);

		byte b1 = i1.byteValue();
		byte b2 = i2.byteValue();
		byte b3 = i3.byteValue();
		byte b4 = i4.byteValue();

		String ss = toBinaryString(b1) + 
				toBinaryString(b2) + 
				toBinaryString(b3) + 
				toBinaryString(b4);

		char[] c = ss.toCharArray();

		*//**
		 * 位为1时的状态
		 *//*
		String[] issue1 = new String[32];
		issue1[31] = "未定位";
//		issue1[30] = "南纬";
//		issue1[29] = "西经";
//		issue1[28] = "停运状态";
//		issue1[27] = "预约(任务车)";
//		issue1[26] = "空转重";
//		issue1[25] = "重转空";
		//issue1[24] = "";//预留
//		issue1[23] = "ACC开";
		issue1[22] = "重车";
//		issue1[21] = "车辆油路断开";
//		issue1[20] = "车门电路断开";
//		issue1[19] = "车门加锁";
//		issue1[18] = "车辆锁定";
//		issue1[17] = "已达到限制营运次数/时间";
		//其他预留

		*//**
		 * 位为0时的状态
		 *//*
		String[] issue0 = new String[32];
		issue0[31] = "定位";
//		issue0[30] = "北纬";
//		issue0[29] = "东经";
//		issue0[28] = "营运状态";
//		issue0[27] = "未预约";
//		issue0[26] = "默认";//默认是啥状态？？
//		issue0[25] = "默认";
		//issue1[24] = "";//预留
//		issue0[23] = "ACC关";
		issue0[22] = "空车";
//		issue0[21] = "车辆油路正常";
//		issue0[20] = "车门电路正常";
//		issue0[19] = "车门解锁";
//		issue0[18] = "车辆未锁定";
//		issue0[17] = "未达到限制营运次数/时间";
		//其他预留

		StringBuffer result = new StringBuffer();
		for (int i = 0; i < c.length; i++) {
			char issueChar = c[i];

			if (issueChar == '1') {
				if (issue1[i] != null) {
					result.append(issue1[i]+"/");
				}
			}

			if (issueChar == '0') {
				if (issue0[i] != null) {
					result.append(issue0[i]+"/");
				}
			}
		}
		if (result.length() < 1) {
			result.append("无故障");
		}

		return result.toString().substring(0, result.length()-1);
	}
	public void setState(String state) {
		this.state = state;
	}*/
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public float getSpeed() {
		return speed;
	}
	public void setSpeed(float speed) {
		this.speed = speed;
	}
	public int getDirection() {
		return direction;
	}
	public void setDirection(int direction) {
		this.direction = direction;
	}
	public Address() {
		super();
	}

	
	public Address(BasicDBObject finds) {
		super();
		this.isuNum = finds.getString("isuNum");
		this.serialNum = finds.getInt("serialNum");
		this.alarm = finds.getString("alarm");
		 this.state = finds.getString("state"); 
		this.latitude = finds.getDouble("latitude");
		this.longitude = finds.getDouble("longitude");
		this.speed = (float)finds.getDouble("speed");
		this.direction = finds.getInt("direction");
		this.time = DateUtil.dateToString(finds.getDate("time"));
	}
	
	public Address(String isuNum, String taxiNum, String state, int serialNum,
			String alarm, double longitude, double latitude, float speed,
			int direction, String time) {
		super();
		this.isuNum = isuNum;
		this.taxiNum = taxiNum;
		this.state = state;
		this.serialNum = serialNum;
		this.alarm = alarm;
		this.longitude = longitude;
		this.latitude = latitude;
		this.speed = speed;
		this.direction = direction;
		this.time = time;
	}
	
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	@Override
	public String toString() {
		return "Address [isuNum=" + isuNum + ", taxiNum=" + taxiNum
				+ ", state=" + state + ", locateState=" + locateState
				+ ", operateState=" + operateState + ", serialNum=" + serialNum
				+ ", alarm=" + alarm + ", longitude=" + longitude
				+ ", latitude=" + latitude + ", speed=" + speed
				+ ", direction=" + direction + ", time=" + time + "]";
	}
	
	
	//把"二进制"字节 转为 8位的二进制
	/*public static String toBinaryString(byte b){
		String binary = null;
		int i = (int)b;
		String s = Integer.toBinaryString(i);
		if(i >= 0){
			int len = s.length();
			if(len < 8){
				int offset = 8 - len;
				for(int j=0; j<offset; j++){
					s = "0" + s;
				}
			}
			binary = s;
		}
		else{
			binary = s.substring(24);
		}
		return binary;
	}*/


	



}
