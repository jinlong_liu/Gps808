<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link rel="stylesheet"
	href="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.css" />
<link rel="stylesheet"
	href="http://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.css" />
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>报警信息</h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
							</a>
						</div>
					</div>
					<div class="ibox-content" style="height: 780px">
						<br>
						<div>
							<button type="button" class="btn btn-primary" id="demo11">导出表格</button>
							<button type="button" class="btn btn-primary" id="clear">清除未处理报警信息</button>
						</div>
						<table style="text-align: center"
							class="table table-striped table-bordered table-hover dataTables-example">
							<thead>
								<tr>
									<th style="text-align: center">车牌号</th>
									<th style="text-align: center">报警类型</th>
									<th style="text-align: center">报警时间</th>
									<th style="text-align: center">处理状态</th>
									<!-- <th>载客状态</th> -->
									<th style="text-align: center">操作</th>
									<!-- <th>是否处理</th> -->
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${alarm}" var="alarm">
									<tr id="${alarm.time}">
										<td>${alarm.taxiNum}</td>
										<td>${alarm.alarmType}</td>
										<td>${alarm.time}</td>
										<td>${alarm.dealState}</td>
										<td style="text-align: center"><c:choose>
												<c:when test="${alarm.alarmType eq '进出区域' }">
													<button type="button" class="btn btn-success btn-sm"
														data-toggle="modal" data-target="#myModal1"
														onclick="showInfo('${alarm.isuNum}','${alarm.alarmType}','${alarm.time}','${alarm.dealState}')">查看详情</button>
											&nbsp;&nbsp;&nbsp;&nbsp;
											<c:if test="${! empty alarm.taxiNum}">
														<a href="enterTakePic2?taxiNum=${alarm.taxiNum}"><button
																type="button" class="btn btn-primary">下发拍照指令</button></a>
													</c:if>
													<c:if test="${! empty alarm.taxiNum}">
														<a href="intoBackdail2?taxiNum=${alarm.taxiNum}"><button
																type="button" class="btn btn-primary">电话回拨</button></a>
													</c:if>
												</c:when>
										 		<c:when test="${alarm.alarmType eq '磁盘已满' }">
													<c:if test="${! empty alarm.taxiNum}">
														<button type="button" class="btn btn-danger btn-sm"
																onclick="dealAlarm('${alarm.isuNum}','${alarm.alarmType}','${alarm.time}','${alarm.dealState}')">终止报警</button>
														<button type="button" class="btn btn-info"
															onclick="formatDisc('${alarm.isuNum}','${alarm.alarmType}','${alarm.time}','${alarm.dealState}')">格式化磁盘</button>
														<button type="button" class="btn btn-success"
																onclick="getdealAlarmByIsu('${alarm.isuNum}','${alarm.alarmType}','${alarm.time}','${alarm.dealState}')">更多报警</button></c:if>
												</c:when>
												<c:when test="${alarm.alarmType eq '磁盘数据错误' }">
													<c:if test="${! empty alarm.taxiNum}">
														<button type="button" class="btn btn-danger btn-sm"
																onclick="dealAlarm('${alarm.isuNum}','${alarm.alarmType}','${alarm.time}','${alarm.dealState}')">终止报警</button>
														<button type="button" class="btn btn-info"
																onclick="formatDisc('${alarm.isuNum}','${alarm.alarmType}','${alarm.time}','${alarm.dealState}')">格式化磁盘</button>
														<button type="button" class="btn btn-success"
																onclick="getdealAlarmByIsu('${alarm.isuNum}','${alarm.alarmType}','${alarm.time}','${alarm.dealState}')">更多报警</button></c:if>
												</c:when>
												<c:when test="${alarm.alarmType eq '未检测到磁盘' }">
													<c:if test="${! empty alarm.taxiNum}">
														<button type="button" class="btn btn-danger btn-sm"
																onclick="dealAlarm('${alarm.isuNum}','${alarm.alarmType}','${alarm.time}','${alarm.dealState}')">终止报警</button>
														<button type="button" class="btn btn-info"
																onclick="formatDisc('${alarm.isuNum}','${alarm.alarmType}','${alarm.time}','${alarm.dealState}')">格式化磁盘</button>
														<button type="button" class="btn btn-success"
																onclick="getdealAlarmByIsu('${alarm.isuNum}','${alarm.alarmType}','${alarm.time}','${alarm.dealState}')">更多报警</button>
													</c:if>
												</c:when>
												<c:otherwise>
													<button type="button" class="btn btn-danger btn-sm"
														onclick="dealAlarm('${alarm.isuNum}','${alarm.alarmType}','${alarm.time}','${alarm.dealState}')">终止报警</button>
													<button type="button" class="btn btn-success"
															onclick="getdealAlarmByIsu('${alarm.isuNum}','${alarm.alarmType}','${alarm.time}','${alarm.dealState}')">更多报警</button>
											&nbsp;&nbsp;&nbsp;&nbsp;
													<c:if test="${! empty alarm.taxiNum}">
														<a href="enterTakePic2?taxiNum=${alarm.taxiNum}"><button
																type="button" class="btn btn-primary">下发拍照指令</button></a>
													</c:if>
													<c:if test="${! empty alarm.taxiNum}">
														<a href="intoBackdail2?taxiNum=${alarm.taxiNum}"><button
																type="button" class="btn btn-primary">电话回拨</button></a>
													</c:if>
												</c:otherwise>
											</c:choose></td>
									</tr>
								</c:forEach>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="5">
										<ul class="pagination pull-right"></ul>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<%-- <div class="col-sm-5">
				<!-- 日期+精确查询+导出表格开始 -->
				<!-- 	<div class="ibox float-e-margins"> -->
				<form action="getAlarmByTaxi" id="getOneForm" method="post">
					<div class="ibox-title">
						<h5 class="text-center">查询报警记录</h5>
					</div>
					<div class="ibox-content">
						<div class="row">
							<div class="col-sm-6">
								开始时间： <input type="text" name="startDate" id="startDate"
									value="${sensorStartDate}" class="Wdate"
									onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'endDate\')||\'new Date()\'}'})">
							</div>
							<div class="col-sm-6">
								处理状态：<select name="dealState">
									<option value="0">全部</option>
									<option value="1">未处理</option>
									<option value="2">已处理</option>
								</select>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-6">
								结束时间： <input type="text" name="endDate" id="endDate"
									placeholder="默认为当前时间" value="${sensorEndDate}" class="Wdate"
									onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'startDate\')}',maxDate:'#F{\'new Date()\'}',dateFmt:'yyyy-MM-dd HH:mm:ss'})">
							</div>
							<div class="col-sm-6">
								车牌号：<input placeholder="如B00001" type="text" id="taxiNum"
									name="taxiNum" value="${sensorTaxiNum}" />
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-6">
								<button id="getOneBtn" type="button" class="btn btn-primary ">查询</button>
							</div>
							<!-- <div class="col-sm-4">
								<button type="button" class="btn btn-primary demo2">处理</button>
							</div> -->
							<div class="col-sm-6">
								<button type="button" class="btn btn-primary demo1">导出</button>
							</div>
						</div>
					</div>
				</form>
			</div> --%>
			<!--进出区域相关弹窗  -->
			<div class="modal inmodal fade" id="myModal1" tabindex="-1"
				role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>区域报警详情</h5>
							</div>
							<!-- 地图显示区域 -->
							<div class="ibox-content">
								<div class="row">
									<!-- 车牌号 -->
									<label class="col-sm-2 control-label">车牌号:</label>
									<div class="col-sm-3">
										<input type="text" class="form-control" id="carNum"
											name="carNum" readonly="readonly">
									</div>
									<!-- 报警类型 -->
									<label class="col-sm-2 control-label">报警类型:</label>
									<div class="col-sm-3">
										<input type="text" class="form-control" id="alarmType"
											name="alarmType" readonly="readonly">
									</div>
								</div>
								<br>
								<div class="row">
									<!-- 区域id -->
									<label class="col-sm-2 control-label">区域id:</label>
									<div class="col-sm-3">
										<input type="text" class="form-control" id="areaId"
											name="areaId" readonly="readonly">
									</div>
									<!-- 实时速度 -->
									<label class="col-sm-2 control-label">实时速度:</label>
									<div class="col-sm-3">
										<input type="text" class="form-control" id="speed"
											name="speed" readonly="readonly">
									</div>
								</div>
								<br>
								<div class="row">
									<!-- 实时方向-->
									<label class="col-sm-2 control-label">实时方向:</label>
									<div class="col-sm-3">
										<input type="text" class="form-control" id="direction"
											name="direction" readonly="readonly">
									</div>
									<!-- 报警时间 -->
									<label class="col-sm-2 control-label">报警时间:</label>
									<div class="col-sm-3">
										<input type="text" class="form-control" id="alarmTime"
											name="alarmTime" readonly="readonly">
									</div>
								</div>
								<br>
								<div class="row">
									<!-- 备注 -->
									<label class="col-sm-2 control-label">备注:</label>
									<div class="col-sm-3">
										<input type="text" class="form-control" id="remark"
											name="remark" value="无备注信息" readonly="readonly">
									</div>
								</div>
								<br>
								<!-- 显示地图 -->
								<div class="row">
									<div id="allmap">
										<div id="map" style="height: 600px; width: 800px"></div>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" id="taxiNum1">
						<input type="hidden" id="alarmType1">
						<input type="hidden" id="time1">
						<input type="hidden" id="dealState1"> 
						<div class="modal-footer">
							<button id="btn_addPreInfo" type="button" class="btn btn-white"
								data-dismiss="modal">关闭</button>
							<button  type="button" class="btn btn-primary" onclick="dealArea()">标记已查看</button>
						</div>
					</div>
				</div>
			</div>
			<!--进出区域结束  -->
			<form action="derivedFormOfDealAlarm" method="post" id="subForm">
			</form>
		</div>

	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/content.min.js?v=1.0.0"></script>
	<!--百度地图相关  -->
	<!-- 加载检索信息窗口 -->
	<script type="text/javascript"
		src="http://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.js"></script>
	<script type="text/javascript"
		src="http://api.map.baidu.com/api?v=2.0&ak=xj2Gd1nMEwDHFcGeQvr0WORGk0t1Dlhw"></script>
	<!-- 加载鼠标绘制工具 -->
	<script type="text/javascript"src="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.js"></script>
<%-- 	<script>
		$(document).ready(function()
				{
			var aa='<%=request.getAttribute("points")%>';
				})
		</script> --%>
	<script>
	//分页
  	function fnClickAddRow() {
		$("#editable").dataTable()
				.fnAddData(
						[ "Custom row", "New row", "New row", "New row",
								"New row" ])
	};  
	//进出区域详情
	function showInfo1(isuNum,alarmIdentify,type,time,dealState){
		swal({
			title : "进出区域详情",
			text:"终端号:"+isuNum+" \n"+" 区域id:"+alarmIdentify+"\n"+"时间:"+time+"\n"+" 状态:"+"已查看"+"\n",
			showCancelButton : true,
			cancelButtonText : "关闭",
			confirmButtonText : "标记已查看",
		},function() {
		//查看完就关闭。
		deal(isuNum,type,time,dealState);
		});
		
	}
	function deal(isuNum,alarmType,time,dealState){
	 
			window.location.href="dealAlarm?isuNum="+isuNum+"&alarmType="+alarmType+"&time="+time+"&dealState="+dealState; 
		 
	}
	function dealAlarm(isuNum,alarmType,time,dealState){
			swal({
				title: "您确定执行该操作吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
				window.location.href="dealAlarm?isuNum="+isuNum+"&alarmType="+alarmType+"&time="+time+"&dealState="+dealState; 
			});
		}
	function getdealAlarmByIsu(isuNum,alarmType,time,dealState){{
		var url="getdealAlarmByIsu?isuNum="+isuNum+"&alarmType="+alarmType+"&time="+time+"&dealState="+dealState;
		pageUp(url,"未处理报警详情");
	}}
	function formatDisc(isuNum,alarmType,time,dealState){
		swal({
			title: "您确定要格式化磁盘吗?",
			type : "warning",
			showCancelButton : true,
			cancelButtonText : "取消"
		}, function() {
			window.location.href="formatDisc?isuNum="+isuNum+"&alarmType="+alarmType+"&time="+time+"&dealState="+dealState;
		});
	}
		$(document)
				.ready(
						function() {
						//返回执行状态
						var result='<%=request.getAttribute("result")%>';
						//避免状态为空弹出错误弹框的问题出现。
							if (result != null && result !== "null"&&result!==""&& typeof result !== "null") {
								if (result.indexOf("成功") > 0) {
									swal({
										title : result,
										type : "success"
									});
								} else {
									swal({
										title : result,
										type : "error"
									});
								}
							}else{
								
							}

							//保留分页后的checkbox的值
							var checkedIds = "";
							$("input[name='chbox']")
									.change(
											function() {
												var oneches = document
														.getElementsByName("chbox");
												for (var i = 0; i < oneches.length; i++) {
													if (oneches[i].checked == true) {
														//避免重复累计id （不含该id时进行累加）
														if (checkedIds
																.indexOf(oneches[i].value) == -1) {
															checkedIds = checkedIds
																	+ oneches[i].value
																	+ ",";
														}
													}
													if (oneches[i].checked == false) {
														//取消复选框时 含有该id时将id从全局变量中去除
														if (checkedIds
																.indexOf(oneches[i].value) != -1) {
															checkedIds = checkedIds
																	.replace(
																			(oneches[i].value + ","),
																			"");
														}
													}
												}
												$("#manyIsu").val(checkedIds);
											});

							//点击"精确搜索"
							$("#getOneBtn").click(function() {
								var startTime = $("#startDate").val();
								var endTime = $("#endDate").val();
								var taxiNum = $("#taxiNum").val();
								var endYear = null;
								//开始时间的年份
								var startYear = startTime.substr(0, 4);
								//结束时间的年份
								if (endTime == "") {
									endYear = new Date().getFullYear();
								} else {
									endYear = endTime.substr(0, 4);
								}

								if (startTime == "") {
									swal({
										title : "开始时间不能为空",
										type : "warning"
									});
								} else if (endYear != startYear) {
									swal({
										title : "不支持跨年查询，请重新选择开始时间或结束时间",
										type : "warning"
									});
								} else if (taxiNum == "") {
									swal({
										title : "请输入车牌号",
										type : "warning"
									});
								} else {
									$("#getOneForm").submit();
								}
							});

							//点击"导出表格"按钮
							$("#demo11").click(function() {
								swal({
									title : "",
									text : "您确定导出未处理报警的数据吗?",
									type : "warning",
									showCancelButton : true,
									cancelButtonText : "取消"
								}, function() {
									//赋值
									$("#subForm").submit();
								});
							});

							$(".dataTables-example").dataTable();
							var oTable = $("#editable").dataTable();
							oTable
									.$("td")
									.editable(
											"../example_ajax.php",
											{
												"callback" : function(sValue, y) {
													var aPos = oTable
															.fnGetPosition(this);
													oTable.fnUpdate(sValue,
															aPos[0], aPos[1])
												},
												"submitdata" : function(value,
														settings) {
													return {
														"row_id" : this.parentNode
																.getAttribute("id"),
														"column" : oTable
																.fnGetPosition(this)[2]
													}
												},
												"width" : "90%",
												"height" : "100%"
											})
						});

		function fnClickAddRow() {
			$("#editable").dataTable()
					.fnAddData(
							[ "Custom row", "New row", "New row", "New row",
									"New row" ])
		};
	//弹出一个新tab
	function pageUp(url, title){
		var nav = $(window.parent.document).find('.J_menuTabs .page-tabs-content ');
		$(window.parent.document).find('.J_menuTabs .page-tabs-content ').find(".J_menuTab.active").removeClass("active");
		$(window.parent.document).find('.J_mainContent').find("iframe").css("display", "none");
		var iframe = '<iframe class="J_iframe" name="iframe10000" width="100%" height="100%" src="' + url + '" frameborder="0" data-id="' + url
				+ '" seamless="" style="display: inline;"></iframe>';
		$(window.parent.document).find('.J_menuTabs .page-tabs-content ').append(
				' <a href="javascript:;" class="J_menuTab active" data-id="'+url+'">' + title + ' <i class="fa fa-times-circle"></i></a>');
		$(window.parent.document).find('.J_mainContent').append(iframe);
	};
	</script>
	<script type="text/javascript">
		$("#clear").click(function () {
			$.ajax({
				url:"dealAllAlarm",
				success:function (data) {
					alert("清理"+data+"条未处理信息");
				}
			})
		})
		/* 区域报警弹框相关 */
		//首先加载地图类
		// 百度地图API功能
		var map = new BMap.Map("map");
		//必须设置中心点，否则初始化地图会失败
		var poi = new BMap.Point(116.307852, 40.057031);
		map.centerAndZoom("汤原", 10);
		//添加在地图上添加覆盖物的控件
		map.addControl(new BMap.NavigationControl());
		//map.setCenter('青岛');
		map.enableScrollWheelZoom(); //添加滚轮缩放地图功能  

		function showInfo(isuNum,alarm,time,dealState) {
			//首先清除地图覆盖物
		    map.clearOverlays();
			//区域信息
		    var aa='<%=request.getAttribute("points")%>';
		    //点数据
		    var aaa=eval("("+aa+")");
			//填充隐藏值
		    $("#taxiNum1").val(isuNum);
			$("#alarmType1").val(alarm);
			$("#time1").val(time);
			$("#dealState1").val(dealState); 
			//根据终端号和时间去后台查找对应的报警点
			var args = {
				"isuNum" : isuNum,
				"time" : time
			};
			var url = "getAreaAlarm";

			$.post(url, args, function(data) {
				//alert(data[0].isuNum); 
				//车牌号：
				$("#carNum").attr("value", data[0].taxiNum);
				//报警类型:
				$("#alarmType").attr("value", data[0].aresDirection);
				//区域id：
				$("#areaId").attr("value", data[0].areaId);
				//实时速度：
				$("#speed").attr("value", data[0].speed+"km/h");
				//实时方向:
				$("#direction").attr("value", data[0].direction+"°");
				//报警时间:
				$("#alarmTime").attr("value", data[0].time);
				//备注:
				$("#remark").attr("value", data[0].remark);
				//经纬度:
				var lat = data[0].latitude;
				var lon = data[0].longitude;
				var point = new BMap.Point(lon,lat);
				//然后填充到地图进行显示(小车的位置)
				var icon = new BMap.Icon('car.png', new BMap.Size(50, 40));
				console.log("经度:"+lon+"纬度:"+lat);
				var mkr = new BMap.Marker(point,
						{
							icon : icon
						});
				
				map.addOverlay(mkr);
				//定位到当前点。
				map.centerAndZoom(point, 12);
	        	//var temp = "${item}";  
	        	//准备points数组
	        	var points=[];
	        	var first=new BMap.Point(aaa[0].point[0].longitude,aaa[0].point[0].latitude);;
	        	for(var i=0;i<aaa[0].pointsNum;i++){
	        		points[i]= new BMap.Point(aaa[0].point[i].longitude,aaa[0].point[i].latitude);
	        	}
	        	points[aaa[0].pointsNum]=first;
				//描多边形轨迹
	        	 var pline=new BMap.Polyline(points, {
       			 strokeColor : "blue",  
       			 strokeWeight : 3,  
       			 strokeOpacity : 0.5  
   				 }); // 画线  
   				map.addOverlay(pline);
				//首先获取集合
				/* 	swal({
					title : data,
					type : "success"
				}); */

			});

		}
		//函数:标记查看记录为已查看,更新数据库
	</script>
	<script type="text/javascript">
	//进出区域报警处理方法
	function dealArea(){
			var isuNum = $("#taxiNum1").val();
			var alarmType = $("#alarmType1").val();
			var time = $("#time1").val();
			var dealState = $("#dealState1").val();
			//具体方法来处理
			//dealAlarm(isuNum,alarmType,time,dealState);		
			window.location.href="dealAlarm?isuNum="+isuNum+"&alarmType="+alarmType+"&time="+time+"&dealState="+dealState; 
	}
	</script>
</body>

</html>