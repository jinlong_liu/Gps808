package com.hx.gps.dao;

import java.util.List;

import com.hx.gps.entities.Accident;
import com.hx.gps.entities.Accidenttype;
import com.hx.gps.entities.Blacklist;
import com.hx.gps.entities.Breakrule;
import com.hx.gps.entities.Check;
import com.hx.gps.entities.Company;
import com.hx.gps.entities.Driver;
import com.hx.gps.entities.Fare;
import com.hx.gps.entities.Insure;
import com.hx.gps.entities.Power;
import com.hx.gps.entities.Role;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.Taxitype;
import com.hx.gps.entities.User;
import com.hx.gps.entities.Yearinspect;
import com.hx.gps.entities.tool.DatatablesQuery;
import com.hx.gps.entities.tool.InfoQuery;
import com.hx.gps.entities.tool.InfoQuery2;
import com.hx.gps.entities.tool.MeterState;
import com.hx.gps.entities.tool.TerminalState;
import com.hx.gps.entities.tool.videoState;

public interface InfoDao {
	// 查询所有用户
	List<User> getAllUser();

	// 查询低级别用户
	List<User> getUserByCom(Company company);

	// 新增用户
	void saveUser(User user);

	// 更新用户
	void updateUser(User user);

	// 删除用户
	void deleteUser(User user);

	// 用户角色
	void updateState(User user);

	// 查询所有角色
	List<Role> getAllRole();

	// 新增角色
	void saveRole(Role role);

	// 更新角色
	void updateRole(Role role);

	// 删除角色
	void deleteRole(Role role);

	// 新增权限
	void addPower(Power power);

	// 删除权限
	void deletePowerByID(Integer powerID);

	// 查询权限
	Power getPowerByID(Integer powerID);

	// 条件查询公司
	Company getComByUser(String name);

	// 查询所有公司
	List<Company> getAllCompany();

	// 新增公司
	void saveCompany(Company company);

	// 更新公司
	void updateCompany(Company company);

	// 删除公司
	public void deleteCompany(Company company) throws Exception;

	// 多条件查询车辆信息
	List<Taxi> getTaxi(InfoQuery infoQuery);

	// 多条件查询车辆数量
	int getTaxiNumber(InfoQuery infoQuery);

	// 增
	boolean saveTaxi(Taxi taxi);

	// 改
	void updateTaxi(Taxi taxi);

	// 删
	void deleteTaxi(Taxi taxi);

	// 查询公司车辆
	String[] getTaxiByCompany(Integer id);

	/**
	 * 车辆类型
	 */
	// 查所有
	List<Taxitype> getAllTaxiType();

	// 增
	boolean saveTaxiType(Taxitype taxitype);

	// 改
	void updateTaxiType(Taxitype taxitype);

	// 删
	void deleteTaxiType(Taxitype taxitype);

	/**
	 * 从业人员
	 */
	// 查
	List<Driver> getDriver(InfoQuery infoQuery);

	// 增
	//分页查
	List<Driver> listDrivers(DatatablesQuery datatablesQuery, InfoQuery infoQuery);
	//分页查获取大小
	Integer getDriversSize(DatatablesQuery datatablesQuery, InfoQuery infoQuer);
	//增
	void saveDriver(Driver driver);

	// 改
	void updateDriver(Driver driver);

	// 删
	void deleteDriver(Driver driver);

	// 根据TaxiNum找到相应的taxiId
	int getTaxiIdBytaxiNum(InfoQuery infoQuery);

	// 查询所有的车辆
	List<Taxi> getAllTaxi();

	/**
	 * 保险信息
	 */
	// 查
	List<Insure> getInsure(InfoQuery infoQuery);

	// 增
	void saveInsure(Insure insure);

	// 改
	void updateInsure(Insure insure);

	// 删
	void deleteInsure(Insure insure);

	/**
	 * 黑名单
	 */
	// 查
	List<Blacklist> getBlacklist(InfoQuery2 infoQuery);

	// 增
	void saveBlacklist(Blacklist blacklist);

	// 改
	void updateBlacklist(Blacklist blacklist);

	// 删
	void deleteBlacklist(Blacklist blacklist);

	/**
	 * 稽查信息
	 */
	// 查
	List<Check> getCheck(InfoQuery2 infoQuery);

	// 增
	void saveCheck(Check check);

	// 改
	void updateCheck(Check check);

	// 删
	void deleteCheck(Check check);

	/**
	 * 运价信息
	 */
	List<Fare> getFare(String type);

	void saveFare(Fare fare);

	void updateFare(Fare fare);

	void deleteFare(Fare fare);

	/**
	 * 年审信息
	 */
	List<Yearinspect> getYearinspect(InfoQuery2 infoQuery2);

	void saveYearinspect(Yearinspect yearinspect);

	void updateYearinspect(Yearinspect yearinspect);

	void deleteYearinspect(Yearinspect yearinspect);

	/**
	 * 违章信息
	 */
	List<Breakrule> getBreakrule(InfoQuery2 infoQuery2);

	void saveBreakrule(Breakrule breakrule);

	void updateBreakrule(Breakrule breakrule);

	void deleteBreakrule(Breakrule breakrule);

	/**
	 * 视频监控
	 */
	List<videoState> getIsuVideoState(String isu, String isuNum);

	// 查询所有安全事故信息
	List<Accident> listAccidents(InfoQuery query);

	// 保存事故信息
	void saveAccident(Accident accident);

	// 删除事故
	void deleteAccident(Accident accident);

	// 查询事故类型
	List<Accidenttype> getAccidentType();

	boolean saveAccidentType(Accidenttype accidentType);
	/**
	 * @return
	 */
	List<Accidenttype> getAllAccidentType();
	/**
	 * @param accidenttype
	 */
	void updateAccidentType(Accidenttype accidenttype);
	/**
	 * @param accidenttype
	 */
	void deleteAccidentType(Accidenttype accidenttype);
	/**
	 * @param accident
	 */
	void updateAccident(Accident accident);

	List<MeterState> getMeterStates(String taxiNum);
	
	List<TerminalState> getTerminalStates(List<String> isuNums);
	//定点定时查车专用
	List<Taxi> getTaxiByQuery(InfoQuery infoQuery);
	//判断公司是否存在
		boolean haveCompany(Company company);
	//添加车辆信息到广告表
		void addCjTaxiNum(Taxi taxi);
		//获取BJ公司
		List<Company> getAllCompanyBj();
		//审核从业人员
		void ApproalDriverById(Integer id, boolean b);
	
}
