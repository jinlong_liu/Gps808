package com.hx.gps.dao;

import com.bugull.mongo.AdvancedDao;
import com.hx.gps.entities.tool.OrderLog;
public class OrderLogDao  extends AdvancedDao<OrderLog>{

	public OrderLogDao() {
		super(OrderLog.class);
	}

}
