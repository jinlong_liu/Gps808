package com.hx.gps.entities;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Staff entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "staff" )
public class Staff implements java.io.Serializable {

	// Fields

	private Integer id;
	private String company;
	private String taxiNum;
	private String driver;
	private String sex;
	private Integer age;
	private String qdType;
	private String capacityNum;
	private String driverPhone;
	private String identityCard;
	private String driveNum;
	private String home;
	private String people;
	private Timestamp time;
	private String occSendIns;
	private Timestamp occStartTime;
	private Timestamp occEndTime;
	private String contractor;
	private String contId;
	private String contPhone;
	private String evaGrade;
	private String moniUnit;
	private String moniPhone;
	private String comPhone;

	// Constructors

	/** default constructor */
	public Staff() {
	}

	/** full constructor */
	public Staff(String company, String taxiNum, String driver, String sex,
			Integer age, String qdType, String capacityNum, String driverPhone,
			String identityCard, String driveNum, String home, String people,
			Timestamp time, String occSendIns, Timestamp occStartTime,
			Timestamp occEndTime, String contractor, String contId,
			String contPhone, String evaGrade, String moniUnit,
			String moniPhone, String comPhone) {
		this.company = company;
		this.taxiNum = taxiNum;
		this.driver = driver;
		this.sex = sex;
		this.age = age;
		this.qdType = qdType;
		this.capacityNum = capacityNum;
		this.driverPhone = driverPhone;
		this.identityCard = identityCard;
		this.driveNum = driveNum;
		this.home = home;
		this.people = people;
		this.time = time;
		this.occSendIns = occSendIns;
		this.occStartTime = occStartTime;
		this.occEndTime = occEndTime;
		this.contractor = contractor;
		this.contId = contId;
		this.contPhone = contPhone;
		this.evaGrade = evaGrade;
		this.moniUnit = moniUnit;
		this.moniPhone = moniPhone;
		this.comPhone = comPhone;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "company")
	public String getCompany() {
		return this.company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@Column(name = "taxiNum")
	public String getTaxiNum() {
		return this.taxiNum;
	}

	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}

	@Column(name = "driver")
	public String getDriver() {
		return this.driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	@Column(name = "sex", length = 1)
	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	@Column(name = "age")
	public Integer getAge() {
		return this.age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	@Column(name = "qdType")
	public String getQdType() {
		return this.qdType;
	}

	public void setQdType(String qdType) {
		this.qdType = qdType;
	}

	@Column(name = "capacityNum")
	public String getCapacityNum() {
		return this.capacityNum;
	}

	public void setCapacityNum(String capacityNum) {
		this.capacityNum = capacityNum;
	}

	@Column(name = "driverPhone")
	public String getDriverPhone() {
		return this.driverPhone;
	}

	public void setDriverPhone(String driverPhone) {
		this.driverPhone = driverPhone;
	}

	@Column(name = "identityCard")
	public String getIdentityCard() {
		return this.identityCard;
	}

	public void setIdentityCard(String identityCard) {
		this.identityCard = identityCard;
	}

	@Column(name = "driveNum")
	public String getDriveNum() {
		return this.driveNum;
	}

	public void setDriveNum(String driveNum) {
		this.driveNum = driveNum;
	}

	@Column(name = "home")
	public String getHome() {
		return this.home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	@Column(name = "people")
	public String getPeople() {
		return this.people;
	}

	public void setPeople(String people) {
		this.people = people;
	}

	@Column(name = "time", length = 19)
	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@Column(name = "occSendIns")
	public String getOccSendIns() {
		return this.occSendIns;
	}

	public void setOccSendIns(String occSendIns) {
		this.occSendIns = occSendIns;
	}

	@Column(name = "occStartTime", length = 19)
	public Timestamp getOccStartTime() {
		return this.occStartTime;
	}

	public void setOccStartTime(Timestamp occStartTime) {
		this.occStartTime = occStartTime;
	}

	@Column(name = "occEndTime", length = 19)
	public Timestamp getOccEndTime() {
		return this.occEndTime;
	}

	public void setOccEndTime(Timestamp occEndTime) {
		this.occEndTime = occEndTime;
	}

	@Column(name = "contractor")
	public String getContractor() {
		return this.contractor;
	}

	public void setContractor(String contractor) {
		this.contractor = contractor;
	}

	@Column(name = "contId")
	public String getContId() {
		return this.contId;
	}

	public void setContId(String contId) {
		this.contId = contId;
	}

	@Column(name = "contPhone")
	public String getContPhone() {
		return this.contPhone;
	}

	public void setContPhone(String contPhone) {
		this.contPhone = contPhone;
	}

	@Column(name = "evaGrade")
	public String getEvaGrade() {
		return this.evaGrade;
	}

	public void setEvaGrade(String evaGrade) {
		this.evaGrade = evaGrade;
	}

	@Column(name = "moniUnit")
	public String getMoniUnit() {
		return this.moniUnit;
	}

	public void setMoniUnit(String moniUnit) {
		this.moniUnit = moniUnit;
	}

	@Column(name = "moniPhone")
	public String getMoniPhone() {
		return this.moniPhone;
	}

	public void setMoniPhone(String moniPhone) {
		this.moniPhone = moniPhone;
	}

	@Column(name = "comPhone")
	public String getComPhone() {
		return this.comPhone;
	}

	public void setComPhone(String comPhone) {
		this.comPhone = comPhone;
	}

}