package com.hx.gps.dao.impl;

import org.springframework.stereotype.Repository;

import com.hx.gps.dao.OrderDao;
import com.hx.gps.entities.tool.WaitOrder;
import com.hx.gps.util.DBUtil;
import com.hx.gps.util.FarmConstant;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
@Repository
public class OrderDaoImpl implements OrderDao {

	@Override
	public void addWaitOrder(WaitOrder waitOrder) {
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB,FarmConstant.WAITORDER);
		BasicDBObject finds=new BasicDBObject();
		finds.put("time",waitOrder.getTime());
		finds.put("serialNum",waitOrder.getSerialNum());
		finds.put("isuNum",waitOrder.getIsuNum());
		finds.put("orderType",waitOrder.getOrderType());//类型
		finds.put("identify",waitOrder.getIdentify());// 断油
		finds.put("people",waitOrder.getPeople());
		finds.put("context",waitOrder.getContext());//断电
		finds.put("state",0);
		finds.put("sendTime",0);
		coll.save(finds);
	}

}
