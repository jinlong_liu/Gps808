package com.hx.gps.dao;

import java.util.List;

import com.bugull.mongo.AdvancedDao;
import com.hx.gps.entities.tool.WaitOrder;

public class WaitOrderDao extends AdvancedDao<WaitOrder>{
	
	public WaitOrderDao() {
		super(WaitOrder.class);
	}


	//根据 流水号+ISU 查询 "待发送命令"表,取出时间最晚的一条数据
	public WaitOrder getWaitOrderByTimeAndSer(String isuNum,int resSerialNum){
		WaitOrder w = null;
		WaitOrderDao waitOrderDao  = new WaitOrderDao();
		List<WaitOrder> results = waitOrderDao.query()
				.is("isuNum", isuNum)
				.is("serialNum", resSerialNum)
				.sort("time:-1").results();
		if(results.size()!=0){
			w = results.get(0);
		}
		return w;
	}
}
