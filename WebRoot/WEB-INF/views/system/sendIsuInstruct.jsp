<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title></title>
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="css/plugins/jsTree/style.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<!-- <base target="_blank"> -->
<style>
.jstree-open>.jstree-anchor>.fa-folder:before {
	content: "\f07c"
}

.jstree-default .jstree-icon.none {
	width: 0
}

.red{

    color:#fda6a6;
}

.green{
color: #2b6901 ;
font-weight:900;
}
.black{

    color:#000000;

}
textarea {
	resize: none;
}
</style>
</head>

<body class="gray-bg">

	<div class="wrapper wrapper-content animated fadeInRight">

			<div class="row">
			<div class="col-sm-9">
				
					<table id="table1" 
 
							style="table-layout: fixed; background: #FFFFFF; font-size: 18px;width:100%"
 
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
							<tbody>
                                   
                                       <tr>
                                            <th width="20%"></th>
                                       		<th>请选择下发指令</th>
                                       		<th>操作</th>
                                       </tr>
                                         <tr>
                                            <td></td>
											<td>清除星历</td>
											<td><input value="0" id="instructType0" type="radio" style="width:20px;height:20px"
													name="instructType">&nbsp;&nbsp;
											</td>
                                         </tr>
                                         <tr>
                                        	 <td></td>
											 <td>终端复位</td>
							   				 <td><input value="1" id="instructType1" name="instructType"  style="width:20px;height:20px"
								type="radio"></td>
										</tr>
  
							</tbody>
						</table>
				</div>

			<!--右侧公司选择栏开始  -->
			<div class="col-sm-3">
				<div class="ibox float-e-margins">

					<div class="ibox-content"  style="height: 820px">
						<button type="button" class="btn btn-primary btn-sm"
							onclick="select()">下发</button>

						<br> <input type="hidden" name="taxiNums" id="taxiNums">
						<input type="hidden" name="instructId" id="instructId">
						 
						<hr>
						<h4>请选择车辆</h4>
						<div id="jstree1" class="trdemo"
							style=" overflow-x: auto; overflow-y: auto; height: 700px;">
							<ul>
							</ul>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- 删除表格 -->
		<form id="deleteForm" action="deleteAdsById" method="post">
			<input type="hidden" id="deleteId" name="id">
		</form>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/echarts/echarts-all.js"></script>
	<script src="js/content.min.js?v=1.0.0"></script>
	<script src="js/demo/echarts-demo2.min.js"></script>
	<script src="js/plugins/jsTree/jstree.min.js"></script>
	<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
 
	<script type="text/javascript">
		//选择公司开始
		$(document).ready(function() {
			/* $("#miletable").hide(); */
			$("#taxiNums").val("");
			// 异步刷新 【车辆列表】
			function pollingTree(){
		  	      var newdata = sessionStorage.getItem("treelist");
		  	  if(newdata!=null){
		  	    var tree= $("#jstree1")
         	   tree.jstree(true).settings.core.data = JSON.parse(newdata);
         	   tree.jstree(true).refresh(true);
		  	  }
			}
			//实现jQuery轮询树的状态
			$(document).ready(function(){
				setInterval(pollingTree, 10*1000);  
				});
			$("#jstree1").jstree({
				"core" : {
					"check_callback" : true,
					'data' : function (node, cb) {
						if(sessionStorage.getItem("treelist")==null){
						    $.ajax({
						    	type:"POST",
						    	dataType:"json",
						    	url : "/GpsManage/getTreeOfTempJson",
						    	success: function(data) {
						    		cb.call(this,data);
						   		}
							});
						}else{
							var newdata = sessionStorage.getItem("treelist");
							cb.call(this,JSON.parse(newdata));
						}
					}
				},
				"plugins" : [ "types", "dnd", "checkbox" ],
				"types" : {
					"car" : {
						"icon" : "fa fa-car"
					},
					"default" : {
						"icon" : "fa fa-folder"
					}
				}
			});
			//页面加载的时候选中转发过来的id
			var idSelect='<%=request.getAttribute("selected")%>';
			
			$(document).ready(function(){
				if(idSelect!=null){ 
				$("input[name=instructType][value="+idSelect+"]").attr("checked", 'checked');
				/* swal({
					title : "指定信息号已被选中",
					type : "success"
				}); */
				}
				})
			//获取选择节点的id
			$("#jstree1").on('changed.jstree', function(e, data) {
				r = [];
				var i, j;
				for (i = 0, j = data.selected.length; i < j; i++) {
					var node = data.instance.get_node(data.selected[i]);
					if (data.instance.is_leaf(node)) {
						r.push(node.id);
					}
				}
				$("#taxiNums").val(r);
			});
		});
	</script>
	<script>
		var trs = document.getElementById('table1').getElementsByTagName('tr');
		window.onload = function() {
			for (var i = 0; i < trs.length; i++) {
				trs[i].onmousedown = function() {
					tronmousedown(this);
				}
			}
		}
		function tronmousedown(obj) {
			for (var o = 0; o < trs.length; o++) {
				if (trs[o] == obj) {
					trs[o].style.backgroundColor = '#DFEBF2';
				} else {
					trs[o].style.backgroundColor = '';
				}
			}
		}
		
		 $(function () {
             $("input:radio[name='instructType']").click(function () {
           	  
		  		 var val=$(this).val();
		  		 $("#instructId").val(val);
             });
         });

		</script>
 
	<script>
		function select() {
			//获取选中的车辆
			var taxiNums = $("#taxiNums").val();
			//选中的广告id
			var taxinum;
			taxinum=taxiNums.split(",");
			//记录长度
			var len=taxiNums.length;
			var len2=taxinum.length;
			console.log(len);
			//选中的指令
			var instructId = $("#instructId").val();
	         if (instructId == "") {	
				swal({
					title : "请选择要下发的指令",
					type : "warning"
				});
				return;
			} else if (taxiNums == "") {
				swal({
					title : "请先选择车辆",
					type : "warning"
				});
				return;
			}else if(len==1){
				swal({
					title : "该公司暂无车辆或车牌号格式有误",
					type : "warning"
				});
				return;
			}
			else if(len<7){
				swal({
					title : "车牌号长度有误",
					type : "warning"
				});
				return;
			} 
			else if(len>7){
				//遍历数组
				for(var i=0;i<len2;i++){
					var taxi=taxinum[i];
					if(taxi.length<7){
						swal({
							title : "部分车辆车牌号格式不对或选定的公司没有车辆",
							type : "warning"
						});
						return;
					}
					
				}
			}  
				var url = "SendisuControlByAjax";
				$.post(url, {
					"taxiNums" : taxiNums,
					"instructId" : instructId,
					 
				}, function(data) {
					if (data.indexOf("成功")>0) {
						swal({
							title : data,
							type : "success"
						});
					}else{
						swal({
							title :data,
							type : "error"
						});
					}
				});

			 
				 
		}
	</script>


	<script>
	//页面加载
	$(document).ready(function(){
			var result='<%=request.getAttribute("result")%>';
			if(result != null && result != "null"){
				if (result.indexOf("成功")>0) {
				swal({
					title : result,
					type : "success"
				});
			}else{
				swal({
					title :result,
					type : "warning"
				});
			}
			}

		});
	//默认隐藏 
	</script>

</body>

</html>