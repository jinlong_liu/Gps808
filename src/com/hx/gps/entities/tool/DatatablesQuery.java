package com.hx.gps.entities.tool;

import java.io.Serializable;

/**
 * 
 * @ClassName: DatatablesQuery.java
 * @Description: 分页查询帮助类
 *
 * @version: v1.0.0
 * @author: AN
 * @date: 2018年5月4日 下午7:15:09 
 * 
 */
public class DatatablesQuery implements Serializable{

	private static final long serialVersionUID = 1L;
	//1.起始页
	private Integer start;
	//2.长度
	private Integer length;
	//3.排序的列名
    private String orderColumn;
    //4.排序的种类
    private String orderType;
    //5.拉取次数
    private Integer draw;
    //6.查询参数
    private String search;
    
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	public String getOrderColumn() {
		return orderColumn;
	}
	public void setOrderColumn(String orderColumn) {
		this.orderColumn = orderColumn;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public Integer getDraw() {
		return draw;
	}
	public void setDraw(Integer draw) {
		this.draw = draw;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	@Override
	public String toString() {
		return "DatatablesQuery [start=" + start + ", length=" + length + ", orderColumn=" + orderColumn
				+ ", orderType=" + orderType + ", draw=" + draw + ", search=" + search + "]";
	}

}
