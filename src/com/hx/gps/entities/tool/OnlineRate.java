package com.hx.gps.entities.tool;

public class OnlineRate {
	
	private String currOnline;		//当前在线率
	private String histOnline;		//二十四小时在线率
	private Integer operNum;		//重车数量
	private Integer empNum;			//暂停数量
	private Integer callNum;		//电召数量
	
	public String getCurrOnline() {
		return currOnline;
	}
	public void setCurrOnline(String currOnline) {
		this.currOnline = currOnline;
	}
	public String getHistOnline() {
		return histOnline;
	}
	public void setHistOnline(String histOnline) {
		this.histOnline = histOnline;
	}
	public Integer getOperNum() {
		return operNum;
	}
	public void setOperNum(Integer operNum) {
		this.operNum = operNum;
	}
	public Integer getEmpNum() {
		return empNum;
	}
	public void setEmpNum(Integer empNum) {
		this.empNum = empNum;
	}
	public Integer getCallNum() {
		return callNum;
	}
	public void setCallNum(Integer callNum) {
		this.callNum = callNum;
	}
	@Override
	public String toString() {
		return "OnlineRate [currOnline=" + currOnline + ", histOnline="
				+ histOnline + ", operNum=" + operNum + ", empNum=" + empNum
				+ ", callNum=" + callNum + "]";
	}
	
	
}
