<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">

<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
</head>
<style>
table.dataTable tbody tr.selected {
	background-color: #b0bed9;
}
</style>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">

		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-content">
					<button type="button" class="btn btn-primary" data-toggle="modal"
						data-target="#myModal5" onclick="init1()">上传升级文件...</button>
						<label>文件名:${fileName}</label>
						<label>文件大小:${fileSize}</label>
						<label>修改日期:${fileTime}</label>
					<button type="button" class="btn btn-info pull-right" onclick="selectAll(1)" style="float:left;margin-left:10px;">该页全选</button>
					<button type="button" class="btn btn-info pull-right" onclick="selectAll(2)">全部全选</button>

					<!-- 点击添加按钮弹出来的form表单 -->
					<div class="modal inmodal fade" id="myModal5" tabindex="-1"
						role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<form action="sysUpdateUpload" method="post" id="uploadForm"
									enctype="multipart/form-data" onsubmit="return check()">
									<div class="modal-body">
										<div class="ibox-title">
											<h5>上传升级文件</h5>
											<div class="pull-right">
												<button type="button" class="btn btn-white btn-sm"
													data-dismiss="modal">关闭</button>
											</div>
										</div>
										<div class="ibox-content">
											<div class="row">
												<div class="col-sm-4">


													<input id="f_file" class="btn btn-white btn-sm" type="file"
														name="file" accept=".bin" style="display: inline;"
														onchange="selectFile(this)"> <span id="comErr"
														style="color: red; font-size: 15px;"></span>
												</div>
												<div class="col-sm-4">
													<button type="submit" class="btn btn-primary"
														style="display: inline;"  >保存
													</button>
												</div>
											</div>


										</div>
									</div>
									<!-- <div class="modal-footer">
							<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
							<button type="button" class="btn btn-primary" id="saveBtn">保存</button>
						</div> -->
								</form>
								<script type="text/javascript">
function selectFile(fnUpload) {
var filename = fnUpload.value; 
var mime = filename.toLowerCase().substr(filename.lastIndexOf(".")); 
if(mime!=".bin") 
{ 
alert("请选择BIN类型文件"); 
fnUpload.outerHTML=fnUpload.outerHTML; 
}
}
setInterval(function (){
	window.location.reload();
},10000);
function check(){
	var str = document.getElementById("f_file").value;
    if(str.length==0)
    {
        alert("文件不能为空，请选择BIN类型文件");
        return false;
    }
    return true;
}
</script>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>终端远程升级</h5>
				</div>
				<div class="ibox-content" style="height: 610px">
				 
					<table  id ="table1"   class=" table table-stripped" data-page-size="10" 
					data-filter=#filter>
						<thead>
							<tr>
								<td>公司id</td>
								<th>车牌号</th>
								<th>终端号</th>
								<th>当前版本</th>
								<th>版本获取时间</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
						 
							<c:forEach items="${taxis}" var="taxi" varStatus="loop">
								<tr id="${taxi.id}">
									<td>${taxi.company.name}</td>
									<td>${taxi.taxiNum}</td>
									<td>${taxi.isuNum}</td>
									<td>${version[loop.count-1]}</td>
									<td>${isuDate[loop.count-1]}</td>
									<td> <button type="button" class="btn btn-primary"  onclick="getVersion('${taxi.isuNum}')">获取版本号</button>
				
		</td>
									</tr>
							</c:forEach>

						 

						</tbody>
						<tfoot>
							<tr>
								<td colspan="5">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				
				<button type="button" class="btn btn-primary pull-right" data-toggle="modal" onclick="getAllVersion()" style="float:left;margin-left:10px;">一键获取全部版本号</button>
				<button type="button" class="btn btn-primary pull-right" data-toggle="modal"
			style="text-align: right;" onclick="upGrade(states)">升级选中的终端</button>
				
		
				</div>
			</div>
		</div>
	 </div>
	
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script>
	
	var updateIsu; //待升级的终端号数组声明
	var table; //数据表声明
	states = null; //升级选择状态
	function upGrade() {
		  var isOk = confirm('确定要对选中的终端进行升级吗？');
		/* swal({
				title : "确定要对选中的终端进行升级吗？",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() { */
		 if(isOk===true){
			//alert(states);
			if(states==1){
				//alert("升级成功");return;
				updateIsu = new Array();
		    	for(i=0;i<table.rows('.selected').data().length;i++){
		    		updateIsu[i]=table.rows('.selected').data()[i][2];
		    	}
				if(updateIsu.length==0){
					swal({
						title : "请至少选择一个终端",
						type : "warning"
					});
					//alert('请至少选择一个终端进行升级');
				}else{
					$.ajax({ 
				        type:'post',
				        url:'sysUpdate', 
				        data:{isu:updateIsu},
				        dataType:'text',
				        success:function(msg){ 
				        	console.log("xu");
				         
				        	swal({
								title : msg,
								type : "success"
							},function(){
								window.location.reload();});
				        	}, 
				        error:function(){ 
				        	console.log("xu");	
				        	swal({
								title : "发送失败",
								type  :  "error"
							});
				        	} 
				        	});
				}
			}else if(states==2){
				updateIsu = new Array();
		    	for(i=0;i<table.rows('.selected').data().length;i++){
		    		updateIsu[i]=table.rows('.selected').data()[i][2];
		    	}
				if(updateIsu.length==0){
					swal({
						title : "请至少选择一个终端",
						type : "warning"
					});
				}else{
					$.ajax({ 
				        type:'post',
				        url:'sysUpdate2', 
				        dataType:'text',
				        success:function(msg){ 
				        	console.log("xu");
				        	swal({
								title : msg,
								type : "success"
							},function(){
								window.location.reload();});
				        	}, 
				        error:function(){ 
				        	console.log("xu");	
				        	swal({
								title : "发送失败",
								type  :  "error"
							});
				        	} 
				     });
				}
			} else if(states==null){
				updateIsu = new Array();
		    	for(i=0;i<table.rows('.selected').data().length;i++){
		    		updateIsu[i]=table.rows('.selected').data()[i][2];
		    	}
				if(updateIsu.length==0){
					swal({
						title : "请至少选择一个终端",
						type : "warning"
					});
				}else{
					$.ajax({ 
				        type:'post',
				        url:'sysUpdate',
				        data:{isu:updateIsu},
				        dataType:'text',
				        success:function(msg){ 
				        	console.log("xu");
				        	swal({
								title : msg,
								type : "success"
							},function(){
								window.location.reload();});
				        	}, 
				        error:function(){ 
				        	console.log("xu");	
				        	swal({
								title : "发送失败",
								type  :  "error"
							});
				        	} 
				     });
				}			
			}
		} 
	}
		//页面加载完成后执行,获取执行状态
		$(document).ready(function(){
		var result='<%=request.getAttribute("result")%>';
			if(result != null && result != "null"){
				if (result.indexOf("成功")>0) {
				swal({
					title : result,
					type : "success"
				});
			}else{
				swal({
					title :result,
					type : "error"
				});
			}
			}

	});
		$(document).ready(function() {
			table = $('#table1').DataTable({
				"pagingType" : "full_numbers"
			});
			$('#table1 tbody').on('click', 'tr', function() {
				$(this).toggleClass('selected');
			});
		});
		//全选  table 
		function selectAll(state) {
			if(state==1){
				//alert("单页全选");	
				$('#table1 tbody tr').toggleClass('selected');
				states = 1;
			}else if(state==2){
				//alert("全部全选");
				$('#table1 tbody tr').toggleClass('selected');
				states = 2;
			}
			
		}
		//获取终端版本号
		function getVersion(isu){
			//在进页面之前就缓存终端版本号
			var args={"isu":isu};
			//根据终端号去请求对应的表，下发命令。（获取版本号）
			$.post("getIsuVersion", args, function(data) {
			//然后等待时间去查找版本表指定的版本信息，如果有就返回，先更新再返回。
				if (data[0] != null) {
			//刷新页面数据。(只是代表下发成功了。)
				swal({
					title:data[0],
					type:"success"
				});
			//返回新的版本号
				}
			});
			
		}
		// 获取全部版本号
		function getAllVersion(){
			//在进页面之前就缓存终端版本号
			var args={"isu":"all"};
			//根据终端号去请求对应的表，下发命令。（获取版本号）
			$.post("getAllIsuVersion", args, function(data) {
			//然后等待时间去查找版本表指定的版本信息，如果有就返回，先更新再返回。
				if (data[0] != null) {
			//刷新页面数据。(只是代表下发成功了。)
				swal({
					title:data[0],
					type:"success"
				});
			//返回新的版本号
				}
			});
			
		}
	</script>
</body>

</html>