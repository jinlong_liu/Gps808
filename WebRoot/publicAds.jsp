<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<base target='_self'>

<title>公益广告下发</title>
<script src="js/jquery.js"></script>
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

<link rel="shortcut icon" href="favicon.ico">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="css/plugins/jsTree/style.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">

<!--  添加表单验证 -->
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />

<base target="_blank">
<style>
table.dataTable tbody tr.selected {
	background-color: #b0bed9;
}

.red {
	color: #fda6a6;
}

.green {
	color: #2b6901;
	font-weight: 900;
}

.black {
	color: #000000;
}

.jstree-open>.jstree-anchor>.fa-folder:before {
	content: "\f07c"
}

.jstree-default .jstree-icon.none {
	width: 0
}
</style>
<script src="js/jquery.min.js?v=2.1.4"></script>
<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>
<script src="js/bootstrap.min.js?v=3.3.5"></script>
<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="js/plugins/footable/footable.all.min.js"></script>
<script src="js/content.min.js?v=1.0.0"></script>
<script src="js/plugins/jsTree/jstree.min.js"></script>
</head>

<body class="gray-bg">
	<div class="wrapper animated ">
		<div class="row">
			<div class="col-sm-12">
				<!-- 点击添加按钮弹出来的form表单 -->
				<div class="modal inmodal fade" id="myModal5" tabindex="-1"
					role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<form action="addpublicAds"  class="form-horizontal" method="post" id="addpublicAdsForm">
								<div class="modal-body">
									<div class="ibox-title">
										<h5>新增公益广告</h5>
									</div>
									<div class="ibox-content">
										<div class="row">
											<div class="form-group">
											<label class="col-sm-3 control-label">信息号：</label>
											<div class="col-sm-5">
												<input name="playId" class="form-control" type="text"
													placeholder="请输入数字(0-255)">
											</div>
											</div>
										</div>
										
										<div class="row">
                                           <div class="form-group">
											<label class="col-sm-3 control-label">播放模式：</label>
											<div class="col-sm-5">
												<select class="form-control" name="playMode" id="playMode">
													<option value="0">连续左移</option>
													<option value="1">立即显示</option>
													<option value="2">左移</option>
													<option value="3">右移</option>
													<option value="4">上移</option>
													<option value="5">下移</option>
												</select>
											</div>
											</div>
										</div>
										
										<div class="row">
 											 <div class="form-group">
											<label class="col-sm-3 control-label">播放速度:</label>
											<div class="col-sm-5">
												<input name="playRate" class="form-control" type="text"
													placeholder="范围：(0-14)">
											</div>
											</div>
										</div>
										
										<div class="row">
										 <div class="form-group">
											<label class="col-sm-3 control-label">广告内容：</label> 还可以输入<span
												style="font-family: Georgia; font-size: 20px;"
												id="wordCheck1">64</span>个汉字
											<div class="col-sm-5">
												<textarea style="height: 250px;resize: none;" name="adsContext"
													class="form-control" required="" aria-required="true"
													onKeyUp="javascript:checkWord(this);"
													onMouseDown="javascript:checkWord(this);"
													style="overflow-y:scroll"></textarea>
											</div>
											</div>
										</div>
										
										<div class="row">
										 <div class="form-group">
											<div class="col-sm-5">

												<input name="adsName" class="form-control" type="hidden"
													value="公益广告">

											</div>
											</div>
										</div>
										

									</div>
								</div>
									<div class="modal-footer">

										<button type="button" class="btn btn-white"
											data-dismiss="modal">关闭</button>
										<button type="submit" class="btn btn-primary">保存</button>
									</div>
							</form>
						</div>
					</div>
				</div>

				<!-- 点击 "修改"按钮弹出来的form表单-->
				<div class="modal inmodal fade" id="myModal1" tabindex="-1"
					role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<form action="updatePublicAds" class="form-horizontal"
								method="post" id="submitForm">
								<div class="modal-body">
									<div class="ibox-title">
										<h5>更新公益广告</h5>
									</div>
								 
									<div class="ibox-content">
										<div class="row">
 											 <div class="form-group">
											<label class="col-sm-3 control-label">信息号：</label>
											<div class="col-sm-5">
												<input name="playId" id="playId1" class="form-control"
													type="text" disabled="disabled"> <input
													name="playId" id="playId" type="hidden">
											</div>
											</div>
										</div>
								
										<div class="row">
										<div class="form-group">
											<label class="col-sm-3 control-label">播放模式：</label>
											<div class="col-sm-5">
												<select class="form-control" name="playMode" id="playMode1">
													<option value="0">连续左移</option>
													<option value="1">立即显示</option>
													<option value="2">左移</option>
													<option value="3">右移</option>
													<option value="4">上移</option>
													<option value="5">下移</option>
												</select>
											</div>
											</div>
										</div>
									
										<div class="row">
											 <div class="form-group">
											<label class="col-sm-3 control-label">播放速度:</label>
											<div class="col-sm-5">
												<input name="playRate" id="playRate" class="form-control"
													type="text">
											</div>
											</div>
										</div>


										<div class="row">



										 <div class="form-group">

											<label class="col-sm-3 control-label">广告内容：</label> 还可以输入<span
												style="font-family: Georgia; font-size: 20px;"
												id="wordCheck">64</span>个汉字
											<div class="col-sm-5">

												<textarea style="height: 250px;resize: none;" name="adsContext"
													id="adsContext" class="form-control" required=""
													aria-required="true" onKeyUp="javascript:checkWord(this);"
													onMouseDown="javascript:checkWord(this);"
													style="overflow-y:scroll"></textarea>
											</div>


 											</div>
										</div>
									</div>
								</div>
									<div class="modal-footer">

										<button type="button" class="btn btn-white"
											data-dismiss="modal">关闭</button>
										<button type="submit" class="btn btn-primary">提交</button>
									</div>
							</form>



						</div>
					</div>
				</div>

			</div>
		</div>

		<div class="row">
			<div class="col-sm-9">
 
				<div class="ibox-content" 
					style="overflow-x: hidden; overflow-y: auto; height: 820px;">
 
					<div class="form-group">
						<button id="btn_addPublicAds" type="button"
							class="btn btn-primary btn-add" data-toggle="modal"
							data-target="#myModal5">添加</button>
						<!-- <input type="text" class="form-control form-control-gy"
							id="filter" placeholder="搜索"> -->
					</div>
					<table id="table1"
						class="footable table  table-hover table-stripped"
						data-page-size="6" data-filter=#filter>
						<thead>
							<tr>
								<th>广告名称</th>
								<th>内容</th>
								<th>记录时间</th>
								<th>信息号</th>
								<th>操作</th>
								<!-- <th><input type="checkbox" id="selectAll">全选</th> -->
							  <th>选择</th>
								
							</tr>

						</thead>

						<tbody>
							<c:forEach items="${publicAdvs}" var="advs">
								<tr id="${advs.playId}">
									<td>${advs.adsName}</td>
									<td>${advs.adsContext}</td>
									<td>${advs.addTime}</td>
									<td>${advs.playId}</td>
									<td><button id="btn-update" type="button"
											class="btn btn-success btn-sm" data-toggle="modal"
											data-target="#myModal1"
											onclick="change('${advs.playId}','${advs.playMode}','${advs.playRate}','${advs.adsContext}')">修改</button>

										<button id="btn-delete" type="button"
											class="btn btn-danger btn-sm"
											onclick="delete1('${advs.playId}')">删除</button></td>
									<td><input type="radio" name="selectTr"
										value="${advs.playId}"></td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="6">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>

			</div>
			<script type="text/javascript">
$("#selectAll").click(function() {
    $("input[name='selectTr']").prop("checked", this.checked);
  });
  
  $("input[name='selectTr']").click(function() {
    var $subs = $("input[name='selectTr']");
    $("#selectAll").prop("checked" , $subs.length == $subs.filter(":checked").length ? true :false);
  });



		</script>
			<!--右侧公司选择栏开始  -->
			<div class="col-sm-3">
				<div class=" float-e-margins">
					<input type="hidden" name="taxiNums" id="taxiNums"> <input
						type="hidden" name="adsPlayId" id="adsPlayId">
					<div class="ibox-content" style="height: 820px">



						<hr>
						<button type="button" class="btn btn-primary " onclick="select()">下发</button>
						<label class="control-label"><h4>请选择车辆</h4></label>&nbsp;&nbsp;&nbsp;
						<input type="text" class="form-control form-control-sm"
							id="txtIndustryArea"> <br>

						<div id="jstree1" class="trdemo"
							style="overflow-x: auto; overflow-y: auto; height: 660px;">
							<%-- <ul>
 
								<li class="jstree-open">所有公司
									<ul>
										<c:forEach items="${comInfo2s}" var="comInfo2s">
											<li id="${comInfo2s.comId}">${comInfo2s.cname }
												<ul>
													<c:forEach items="${comInfo2s.taxis}" var="taxis">
														<li id="${taxis.taxiNum}" data-jstree='{"type":"car"}'>
															${taxis.taxiNum }
															[<span class="${taxis.onlineState=='离线'?'red':'green' }"> ${taxis.onlineState}</span> ]
															</li>
													</c:forEach>
												</ul>
											</li>
										</c:forEach>
									</ul>
								</li>
							</ul> --%>
						</div>

					</div>
				</div>
			</div>

		</div>
		<!-- 删除表格 -->
		<form id="deleteForm" action="deletePublicAdsById" method="post">
			<input type="hidden" id="deleteId" name="id">
		</form>
	</div>





	<script>
	//页面加载的时候选中转发过来的id
	var idSelect='<%=request.getAttribute("selected")%>';
	
	$(document).ready(function(){
		if(idSelect!=null){ 
		$("input[name=selectTr][value="+idSelect+"]").attr("checked", 'checked');
	/* 	swal({
			title : "指定信息号已被选中",
			type : "success"
		}); */
		}
	})
	function pollingTree(){
	      var newdata = sessionStorage.getItem("treelist");
	  if(newdata!=null){
	    var tree= $("#jstree1")
 	   tree.jstree(true).settings.core.data = JSON.parse(newdata);
 	   tree.jstree(true).refresh(true);
	  }
	}
	//实现jQuery轮询树的状态
	$(document).ready(function(){
		setInterval(pollingTree, 10*1000);  
		});
			//选择公司开始
			$(document).ready(function() {
				/* $("#miletable").hide(); */
				$("#taxiNums").val("");

				$("#jstree1").jstree({
					"core" : {
						"check_callback" : true,
						'data' : function (node, cb) {
							if(sessionStorage.getItem("treelist")==null){
							    $.ajax({
							    	type:"POST",
							    	dataType:"json",
							    	url : "/GpsManage/getTreeOfTempJson",
							    	success: function(data) {
							    		cb.call(this,data);
							   		}
								});
							}else{
								var newdata = sessionStorage.getItem("treelist");
								cb.call(this,JSON.parse(newdata));
							}
						}
					},
					"plugins" : [ "types", "dnd", "checkbox", "sort",
						"search", "unique" ],
					"types" : {
						"car" : {
							"icon" : "fa fa-car"
						},
						"default" : {
							"icon" : "fa fa-folder"
						}
					}
				});

				//获取选择节点的id
				$("#jstree1").on('changed.jstree', function(e, data) {
					r = [];
					var i, j;
					for (i = 0, j = data.selected.length; i < j; i++) {
						var node = data.instance.get_node(data.selected[i]);
						if (data.instance.is_leaf(node)) {
							r.push(node.id);
						}
					}
					$("#taxiNums").val(r);
				}).on("search.jstree", function(e, data) {
					if (data.nodes.length) {
						var matchingNodes = data.nodes; // change
						$(this).find(".jstree-node").hide().filter(
								'.jstree-last').filter(function() {
							return this.nextSibling;
						}).removeClass('jstree-last');
						data.nodes.parentsUntil(".jstree")
								.addBack()
								.show()
								.filter(".jstree-children")
								.each(function() {
											$(this).children(".jstree-node:visible")
													.eq(-1)
													.addClass("jstree-last");
										});
						// nodes need to be in an expanded state
						matchingNodes.find(".jstree-node").show(); // change
					}
				}).on( "clear_search.jstree", function(e, data) {
					if (data.nodes.length) {
						$(this).find(".jstree-node").css("display", "")
								.filter('.jstree-last').filter( function() {
											return this.nextSibling;
						}).removeClass('jstree-last');
					}
				});
			});
			//jsTree的模糊查询
			var to = false;
			$("#txtIndustryArea").keyup(function() {
				var v = $("#txtIndustryArea").val();
				$("#jstree1").jstree(true).search(v);
			});
	</script>

	<script>
		function select() {
			//获取选中的车辆
			var taxiNums = $("#taxiNums").val();
			//选中的广告id
			var taxinum;
			taxinum=taxiNums.split(",");
			//记录长度
			var len=taxiNums.length;
			var len2=taxinum.length;
		 	obj = document.getElementsByName("selectTr");
			//数组接收  ，被选中的广告
		    check_val = [];
		    for(k in obj){
		        if(obj[k].checked)
		            check_val.push(obj[k].value);
		    }
			if (taxiNums == "") {
				swal({
					title : "请先选择车辆",
					type : "warning"
				});
				return;
			} else if (check_val == "") {
				swal({
					title : "请选择要下发的广告",
					type : "warning"
				});
				return;
			}else if(len==1){
				swal({
					title : "该公司暂无车辆或车牌号格式错误",
					type : "warning"
				});
				return;
			}else if(len<7){
				swal({
					title : "车牌号长度有误",
					type : "warning"
				});
				return;
			}  
			var url = "SendPublicAdsByAjax";
			$.post(url, {
				"taxiNums" : taxiNums,
				"playIdArray" :check_val
			}, function(data) {
				swal({
					title : data,
					type : "success"
				});
			});
		}
	</script>



	<script type="text/javascript">
	$(document).ready(function(){
		var result='<%=request.getAttribute("state")%>';
			if (result != null && result != "null") {
				if (result.indexOf("成功") > 0) {
					swal({
						title : result,
						type : "success"
					});
				} else {
					swal({
						title : result,
						type : "error"
					});
				}
			}

		});
		function delete1(id) {
			swal({
				title : "确认删除该条公益广告吗",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
				$("#deleteId").val(id);
				//提交表单
				$("#deleteForm").submit();

			});

		}

		function change(playId, playMode, playRate, Context) {
			$("#playId").val(playId);
			$("#playId1").val(playId);
			$("#playMode1").val(playMode);
			$("#playRate").val(playRate);
			$("#adsContext").val(Context);
		}
		/* 		var maxstrlen = 100;

		 function Q(s) {
		 return document.getElementById(s);
		 }

		 function checkWord(c) {

		 len = maxstrlen;
		 var str = c.value;
		 myLen = getStrleng(str);
		 var wck = Q("wordCheck");
		 if (myLen > len * 2) {
		 c.value = str.substring(0, i + 1);
		 } else {
		 wck.innerHTML = Math.floor((len * 2 - myLen) / 2);
		 }

		 }

		 function getStrleng(str) {

		 myLen = 0;
		 i = 0;
		 for (; (i < str.length) && (myLen <= maxstrlen * 2); i++) {
		 if (str.charCodeAt(i) > 0 && str.charCodeAt(i) < 128)
		 myLen++;
		 else
		 myLen += 2;
		 }
		 return myLen;
		 } */
		var maxstrlen = 64;

		function Q(s) {
			return document.getElementById(s);
		}

		function checkWord(c) {

			len = maxstrlen;
			var str = c.value;
			myLen = getStrleng(str);
			var wck = Q("wordCheck");
			var wck1 = Q("wordCheck1");
			if (myLen > len * 2) {
				c.value = str.substring(0, i - 1);
			} else {
				wck.innerHTML = Math.floor((len * 2 - myLen) / 2);
				wck1.innerHTML = Math.floor((len * 2 - myLen) / 2);
			}

		}

		function getStrleng(str) {

			myLen = 0;
			i = 0;
			for (; (i < str.length) && (myLen <= maxstrlen * 2); i++) {
				if (str.charCodeAt(i) > 0 && str.charCodeAt(i) < 128)
					myLen++;
				else
					myLen += 2;
			}
			return myLen;
		}
	</script>

 <script type="text/javascript">
$(document).ready(function formValidator() {
			$('#addpublicAdsForm').bootstrapValidator({
				feedbackIcons : {/*输入框不同状态，显示图片的样式*/
					valid : 'glyphicon glyphicon-ok',
					invalid : 'glyphicon glyphicon-remove',
					validating : 'glyphicon glyphicon-refresh'
				},
				/*生效规则：字段值一旦变化就触发验证*/
				live : 'enabled',
				/*当表单验证不通过时，该按钮为disabled*/
				submitButtons : 'button[type="submit"]',
				/*验证*/
				fields : {
					playId : {
						
						validators : {
							notEmpty : {
								message : '信息号不能为空'
							},numeric : {
								message : '请输入数字'
							}, remote: { 
							     url: 'hasPlayId',//验证地址
		                         message: '信息号已存在', 
		                         delay :  800, 
		                         type: 'POST' 
		                         
		                     }
						}
					},playMode : {
						message : '',
						validators : {
							notEmpty : {/*非空提示*/
								message : '播放模式不能为空'
							}
						}
					},
					playRate : {
						message : '',
						validators : {
							notEmpty : {
								message : '播放速度不能为空'
							},
							between : {
								min : 0,
								max : 15,
								message : '请输入0-15之间的数'
							}
						}
					},
					adsContext : {
						validators : {
							notEmpty : {
								message : '广告内容不能为空'
							}
						}
					}
					}
					});
					});
				$(document).ready(function formValidator() {
			$('#submitForm').bootstrapValidator({
				feedbackIcons : {/*输入框不同状态，显示图片的样式*/
					valid : 'glyphicon glyphicon-ok',
					invalid : 'glyphicon glyphicon-remove',
					validating : 'glyphicon glyphicon-refresh'
				},
				/*生效规则：字段值一旦变化就触发验证*/
				live : 'enabled',
				/*当表单验证不通过时，该按钮为disabled*/
				submitButtons : 'button[type="submit"]',
				/*验证*/
				fields : {
				playMode : {
						message : '',
						validators : {
							notEmpty : {/*非空提示*/
								message : '播放模式不能为空'
							}
						}
					},
					playRate : {
						message : '',
						validators : {
							notEmpty : {
								message : '播放速度不能为空'
							},
							between : {
								min : 0,
								max : 15,
								message : '请输入0-15之间的数'
							}
						}
					},
					adsContext : {
						validators : {
							notEmpty : {
								message : '广告内容不能为空'
							}
						}
					}
					}
					});
					});	
</script>




</body>
</html>



