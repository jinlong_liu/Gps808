package com.hx.gps.entities;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Driver entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "driver" )
public class Driver implements java.io.Serializable {

	// Fields

	private Integer id;
	private Company company;
	private Taxi taxi;
	private String dname;
	private String driNum;
	private String phone;
	private String quaNum;
	private String sex;
	private boolean state;
	private Integer age; // 年龄
	private String qdType;
	private String identityCard;
	private String home;
	private String people;
	private Timestamp time;
	private String occSendIns;
	private Timestamp occStartTime;
	private Timestamp occEndTime;
	private String contractor;
	private String contId;
	private String contPhone;
	private String evaGrade;
	private String moniUnit;
	private String moniPhone;
	private String comPhone;
	private Set<Assarch> assarchs = new HashSet<Assarch>(0);
	private Set<Complaints> complaintses = new HashSet<Complaints>(0);
	private String photoAddr;

	// Constructors

	/** default constructor */
	public Driver() {
	}

	/** full constructor */
	public Driver(Integer id, Company company, Taxi taxi, String dname, String driNum, String phone, String quaNum,
			String sex, boolean state, Integer age, String qdType, String identityCard, String home, String people,
			Timestamp time, String occSendIns, Timestamp occStartTime, Timestamp occEndTime, String contractor,
			String contId, String contPhone, String evaGrade, String moniUnit, String moniPhone, String comPhone,
			Set<Assarch> assarchs, Set<Complaints> complaintses, String photoAddr) {
		super();
		this.id = id;
		this.company = company;
		this.taxi = taxi;
		this.dname = dname;
		this.driNum = driNum;
		this.phone = phone;
		this.quaNum = quaNum;
		this.sex = sex;
		this.state = state;
		this.age = age;
		this.qdType = qdType;
		this.identityCard = identityCard;
		this.home = home;
		this.people = people;
		this.time = time;
		this.occSendIns = occSendIns;
		this.occStartTime = occStartTime;
		this.occEndTime = occEndTime;
		this.contractor = contractor;
		this.contId = contId;
		this.contPhone = contPhone;
		this.evaGrade = evaGrade;
		this.moniUnit = moniUnit;
		this.moniPhone = moniPhone;
		this.comPhone = comPhone;
		this.assarchs = assarchs;
		this.complaintses = complaintses;
		this.photoAddr = photoAddr;
	}
	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPhotoAddr() {
		return photoAddr;
	}

	@Column(name = "photoAddr", length = 30)
	public void setPhotoAddr(String photoAddr) {
		this.photoAddr = photoAddr;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "panyId")
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "taxiId")
	public Taxi getTaxi() {
		return this.taxi;
	}

	public void setTaxi(Taxi taxi) {
		this.taxi = taxi;
	}

	@Column(name = "dname", length = 20)
	public String getDname() {
		return this.dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	@Column(name = "driNum", length = 30)
	public String getDriNum() {
		return this.driNum;
	}

	public void setDriNum(String driNum) {
		this.driNum = driNum;
	}

	@Column(name = "phone", length = 20)
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "quaNum", length = 30)
	public String getQuaNum() {
		return this.quaNum;
	}

	public void setQuaNum(String quaNum) {
		this.quaNum = quaNum;
	}
	@Column(name = "state")
	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	@Column(name = "sex", length = 5)
	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	@Column(name = "age")
	public Integer getAge() {
		return this.age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	@Column(name = "qdType")
	public String getQdType() {
		return this.qdType;
	}

	public void setQdType(String qdType) {
		this.qdType = qdType;
	}

	@Column(name = "identityCard")
	public String getIdentityCard() {
		return this.identityCard;
	}

	public void setIdentityCard(String identityCard) {
		this.identityCard = identityCard;
	}

	@Column(name = "home")
	public String getHome() {
		return this.home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	@Column(name = "people")
	public String getPeople() {
		return this.people;
	}

	public void setPeople(String people) {
		this.people = people;
	}

	@Column(name = "time", length = 19)
	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@Column(name = "occSendIns")
	public String getOccSendIns() {
		return this.occSendIns;
	}

	public void setOccSendIns(String occSendIns) {
		this.occSendIns = occSendIns;
	}

	@Column(name = "occStartTime", length = 19)
	public Timestamp getOccStartTime() {
		return this.occStartTime;
	}

	public void setOccStartTime(Timestamp occStartTime) {
		this.occStartTime = occStartTime;
	}

	@Column(name = "occEndTime", length = 19)
	public Timestamp getOccEndTime() {
		return this.occEndTime;
	}

	public void setOccEndTime(Timestamp occEndTime) {
		this.occEndTime = occEndTime;
	}

	@Column(name = "contractor")
	public String getContractor() {
		return this.contractor;
	}

	public void setContractor(String contractor) {
		this.contractor = contractor;
	}

	@Column(name = "contId")
	public String getContId() {
		return this.contId;
	}

	public void setContId(String contId) {
		this.contId = contId;
	}

	@Column(name = "contPhone")
	public String getContPhone() {
		return this.contPhone;
	}

	public void setContPhone(String contPhone) {
		this.contPhone = contPhone;
	}

	@Column(name = "evaGrade")
	public String getEvaGrade() {
		return this.evaGrade;
	}

	public void setEvaGrade(String evaGrade) {
		this.evaGrade = evaGrade;
	}

	@Column(name = "moniUnit")
	public String getMoniUnit() {
		return this.moniUnit;
	}

	public void setMoniUnit(String moniUnit) {
		this.moniUnit = moniUnit;
	}

	@Column(name = "moniPhone")
	public String getMoniPhone() {
		return this.moniPhone;
	}

	public void setMoniPhone(String moniPhone) {
		this.moniPhone = moniPhone;
	}

	@Column(name = "comPhone")
	public String getComPhone() {
		return this.comPhone;
	}

	public void setComPhone(String comPhone) {
		this.comPhone = comPhone;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "driver")
	public Set<Assarch> getAssarchs() {
		return this.assarchs;
	}

	public void setAssarchs(Set<Assarch> assarchs) {
		this.assarchs = assarchs;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "driver")
	public Set<Complaints> getComplaintses() {
		return this.complaintses;
	}

	public void setComplaintses(Set<Complaints> complaintses) {
		this.complaintses = complaintses;
	}

	@Override
	public String toString() {
		return "{id:'" + id + "', company:'" + company + "', taxi:'" + taxi + "', dname:'" + dname + "', driNum:'"
				+ driNum + "', phone:'" + phone + "', quaNum:'" + quaNum + "', sex:'" + sex + "', state:'" + state
				+ "', age:'" + age + "', qdType:'" + qdType + "', identityCard:'" + identityCard + "', home:'" + home
				+ "', people:'" + people + "', time:'" + time + "', occSendIns:'" + occSendIns + "', occStartTime:'"
				+ occStartTime + "', occEndTime:'" + occEndTime + "', contractor:'" + contractor + "', contId:'"
				+ contId + "', contPhone:'" + contPhone + "', evaGrade:'" + evaGrade + "', moniUnit:'" + moniUnit
				+ "', moniPhone:'" + moniPhone + "', comPhone:'" + comPhone + "', assarchs:'" + assarchs
				+ "', complaintses:'" + complaintses + "', photoAddr:'" + photoAddr + "}";
	}
 
}