package com.hx.gps.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.hx.gps.entities.Taxi;
import com.mongodb.MongoClientOptions;

/**
 * 常量类: farmconstant
 * 
 * @author alex
 */
@Component
public class FarmConstant {

//	// 版本控制
//	// 表示是否含有计价器，1表示含有计价器，则判断是否在线的方式为：使用计价器实时表。
//	// 若为0，则使用终端心跳进行判断，其它状态通过位置信息表实时判断。
//
//	public final static int OnlineState = 1;
//
//	// 项目名字
//	public final static String PROJECT_NAME = "/GpsManage";
//
//	// 登录拦截开关,0:关闭；1:打开
//	public final static int LOGIN_AUTH = 0;
//
//	// 本机mongoDb数据库IP地址/服务器端也采用本机mongo地址(不对外网公开)。
//	// public final static String MONGOIP = "localhost";
//
//	// 测试用的mongoDb数据库IP地址
//
//	public final static String MONGOIP = "localhost";
//
////	 public final static String MONGOIP = "219.146.67.235";
//
//	// 905测试数据库
//	// public final static String MONGODB = "HXGPSTest";
//	// 808测试数据库
//	public final static String MONGODB = "HxGps808YATest";
//	// 808正式数据库
//	// public final static String MONGODB = "HxGps808";
//
//	// 导出表格时，每个sheet最大的行数
//	public final static int MAXROWS = 20000;
//	public final static boolean isCjAds=true;
//	// 实时表名称
//	public final static String ADDRESSTRMP = "addressTemp";
//
//	// 历史表名称
//	public final static String ADDRESSHIS = "addressHistory";
//
//	//广告车辆表
//	public final static String CJTAXINUM = "cjTaxiNum";
//
//	// 判断是否定位的时间间隔
//	public final static int ONLINE = 300;
//
//	// 判断进出区域的时间间隔
//	public final static int INTERVAL = 30;
//
//	// 报警表的名称
//	public final static String ALARM = "alarmLog";
//
//	// 等待发送命令表的名称
//	public final static String WAITORDER = "waitOrder";
//
//	// 命令日志表的名称
//	public final static String ORDERLOG = "orderLog";
//
//	// 评价表名称
//	public final static String Eval = "evaluate";
//	// 视频设备状态表
//	public final static String vedioState = "vedioState";
//	// 计价器实时表
//	public static final String Mongo_col_MeterTemp = "meterTemp";
//	// 下发区域 预设信息表
//	public final static String preInfo2 = "preinfo2";
//	// 计价器表名称
//	// public final static String OPERATE="operate";
//
//	// 修改后的营运信息表
//	public static final String METER = "meter";
//
//	// 修改后的营运信息实时表
//	public static final String METERTEMP = "meterTemp";
//
//	// 图片信息表,分月
//	public static final String PICTURE = "picture";
//
//	// 终端心跳临时表
//	public static final String ISUHEART = "isuHeart";
//
//	// 终端鉴权表
//	public static final String ISU = "isu";
//
//	// 版本数据表
//	public static final String UPDATEFILE = "updateFile";
//
//	// 待升级终端表
//	public static final String WAIRUPGRADE = "wairUpgrade";
//	// 终端在线表
//	public static final String onlineTemp = "onlineTemp";
//	// 定位判断
//	public static final int LOCATION = 360;
//
//	// 超速设置表
//	public static final String OVERSPEED = "overSpeed";
//
//	public static final int MONGOPORT = 27017;
////	public static final int MONGOPORT = 10005;
//	//车辆批量上传集合
//	public static  List<Taxi> taxis = new ArrayList<>();


	public final static int OnlineState = 1;

	// 项目名字
	public final static String PROJECT_NAME = "/GpsManage";

	// 登录拦截开关,0:关闭；1:打开
	public final static int LOGIN_AUTH = 0;

	// 本机mongoDb数据库IP地址/服务器端也采用本机mongo地址(不对外网公开)。
	// public final static String MONGOIP = "localhost";

	// 测试用的mongoDb数据库IP地址

	public final static String MONGOIP = "localhost";
//	public final static String MONGOIP = "219.146.67.245";
	// public final static String MONGOIP = "219.146.67.235";

	// 905测试数据库
	// public final static String MONGODB = "HXGPSTest";
	// 808测试数据库
	public final static String MONGODB = "HxGps808TRTest";
	// 808正式数据库
//	 public final static String MONGODB = "HxGps905Test";

	// 导出表格时，每个sheet最大的行数
	public final static int MAXROWS = 20000;
	public final static boolean isCjAds=true;
	// 实时表名称
	public final static String ADDRESSTRMP = "addressTemp";

	// 历史表名称
	public final static String ADDRESSHIS = "addressHistory";

	//广告车辆表
	public final static String CJTAXINUM = "cjTaxiNum";

	// 判断是否定位的时间间隔
	public final static int ONLINE = 300;

	// 判断进出区域的时间间隔
	public final static int INTERVAL = 30;

	// 报警表的名称
	public final static String ALARM = "alarmLog";

	// 等待发送命令表的名称
	public final static String WAITORDER = "waitOrder";

	// 命令日志表的名称
	public final static String ORDERLOG = "orderLog";

	// 评价表名称
	public final static String Eval = "evaluate";
	// 视频设备状态表
	public final static String vedioState = "vedioState";
	// 计价器实时表
	public static final String Mongo_col_MeterTemp = "meterTemp";
	// 下发区域 预设信息表
	public final static String preInfo2 = "preinfo2";
	// 计价器表名称
	// public final static String OPERATE="operate";

	// 修改后的营运信息表
	public static final String METER = "meter";

	// 修改后的营运信息实时表
	public static final String METERTEMP = "meterTemp";

	// 图片信息表,分月
	public static final String PICTURE = "picture";

	// 终端心跳临时表
	public static final String ISUHEART = "isuHeart";

	// 终端鉴权表
	public static final String ISU = "isu";

	// 版本数据表
	public static final String UPDATEFILE = "updateFile";

	// 待升级终端表
	public static final String WAIRUPGRADE = "wairUpgrade";
	// 终端在线表
	public static final String onlineTemp = "onlineTemp";
	// 定位判断
	public static final int LOCATION = 360;

	// 超速设置表
	public static final String OVERSPEED = "overSpeed";

	public static final int MONGOPORT = 25500;

	//车辆批量上传集合
	public static  List<Taxi> taxis = new ArrayList<>();
}
