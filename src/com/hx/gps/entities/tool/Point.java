package com.hx.gps.entities.tool;

import java.io.Serializable;

//点
public class Point implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4691619495083594515L;
	private double longitude;
	private double latitude;
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	@Override
	public String toString() {
		return "Point [longitude=" + longitude + ", latitude=" + latitude + "]";
	}
	public Point() {
		super();
	}
	public Point(double longitude, double latitude) {
		super();
		this.longitude = longitude;
		this.latitude = latitude;
	}
	public Point(String point) {
		super();
		String arr[] = point.split(",");
		this.longitude = Double.parseDouble(arr[0]);
		this.latitude = Double.parseDouble(arr[1]);
	}
	
	

}
