package com.hx.gps.service;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.tool.Alarm;
import com.hx.gps.entities.tool.ExportPoint;
import com.hx.gps.entities.tool.OrderVector;
import com.hx.gps.entities.tool.OverSpeed;
import com.hx.gps.entities.tool.Picture;
import com.hx.gps.entities.tool.WaitOrder;

public interface SystemService {

	//终止报警,把报警信息的处理状态由未处理修改为已处理
	public String dealAlarm(Alarm alarm);
	//根据车牌号，查询指定时间段内的拍照信息
	public List<Picture> getPicTureByIsu(String taxiNum,String startDate,String endDate);

	//查询拍照记录表中的最后五条记录
	public List<Picture> getLastPic();

	//查看时间段内所有的命令日志
	public List<OrderVector> getOrderByDate(String startDate,String endDate);

	//根据时间排序，获取最后五条记录
	public List<OrderVector> getOrderLast5();
	//获取终端当前版本号
	List<String> getIsuVersion(List<Taxi> taxis);
	//读取并保存升级文件数据
	public boolean saveUpdateData(File file);
	
	//保存待升级终端数据
	public String saveIsuToWairUpgrade(String[] isu);
	//根据起始时间查询拍照
	public Object getPicTureByDate(String startDate, String endDate);
	//获取版本号记录时间
	public List<String> getIsuversionDate(List<Taxi> taxis);
	//下发终端指令
	public int sendIsuContorl(WaitOrder w, String[] taxiNum);
	
	//wk新增 全选升级添加
	public String saveIsuToWairUpgrade2();
	//wk新增 进入超速设置界面
	public List<OverSpeed> getOverSpeed();
	//wk新增 保存超速设置
	public void saveOverSpeed(OverSpeed overSpeed);

	//wk新增 导出坐标点
	public void exportPoint(ExportPoint exportPoint,HttpServletResponse response);

	//获取 当日全部报警
	public int getAlarmNum();
	//获取 当日全部未处理报警
	public int getAlarmNums();
}
