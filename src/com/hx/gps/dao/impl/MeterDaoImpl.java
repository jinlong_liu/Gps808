package com.hx.gps.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.hx.gps.dao.MeterDao;
import com.hx.gps.entities.tool.Meter;
import com.hx.gps.util.DBUtil;
import com.hx.gps.util.DateUtil;
import com.hx.gps.util.FarmConstant;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

@Repository
public class MeterDaoImpl implements MeterDao {

	@Override
	public List<Meter> getMetersByFare(String startDate, String endDate,
			String taxiNum, String fare, String dbname) {
		// 根据时间 车牌号 运价查询
		System.out.println("gy__dbname:" + dbname + ";taxinum:" + taxiNum
				+ ";fare:" + fare);
		DBCollection coll = DBUtil
				.getDBCollection(FarmConstant.MONGODB, dbname);
		BasicDBObject finds = new BasicDBObject();
		List<Meter> meters = new ArrayList<>();

		finds.put("taxiNum", taxiNum);
		finds.put("money", Float.parseFloat(fare));
		// 上车时间小于等于开始时间
		finds.put("upTime",
				new BasicDBObject("$gte", DateUtil.stringToDate(startDate)));
		finds.put("downTime",
				new BasicDBObject("$lte", DateUtil.stringToDate(endDate)));
		DBCursor cursor = coll.find(finds);

		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			Meter meter = new Meter(finds);
			meters.add(meter);
		}

		System.out.println("gy____meters:" + meters);
		return meters;
	}

	@Override
	public List<Meter> getMetersByFloatingFare(String floatingFare1,
			String floatingFare2, String startDate, String endDate,
			String taxiNum, String dbname) {
		// 根据时间 车牌号 浮动运价查询
		System.out.println("gy__dbname:" + dbname + ";taxinum:" + taxiNum
				+ ";floatingFare1,:" + floatingFare1 + ";floatingFare2:"
				+ floatingFare2);
		
		DBCollection coll = DBUtil
				.getDBCollection(FarmConstant.MONGODB, dbname);
		BasicDBObject finds = new BasicDBObject();
		List<Meter> meters = new ArrayList<>();

		finds.put("taxiNum", taxiNum);
		// 租金大于等于floatingFare1小于等于floatingFare2
		finds.put("money",
				new BasicDBObject("$gte", Float.parseFloat(floatingFare1))
						.append("$lte", Float.parseFloat(floatingFare2)));

		// 上车时间大于等于开始时间
		finds.put("upTime",
				new BasicDBObject("$gte", DateUtil.stringToDate(startDate)));
		// 下车时间小于等于结束时间
		finds.put("downTime",
				new BasicDBObject("$lte", DateUtil.stringToDate(endDate)));
		
		DBCursor cursor = coll.find(finds);

		while (cursor.hasNext()) {
			finds = (BasicDBObject) cursor.next();
			Meter meter = new Meter(finds);
			meters.add(meter);
		}

		System.out.println("gy2____meters:" + meters);
		return meters;
	}

}
