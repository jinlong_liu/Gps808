<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">


<script src="js/jquery.js"></script>
<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">

<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>

<!-- gy 添加表单验证 -->
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />
<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>

<!--gy 自定义css -->
<link rel="stylesheet" href="css/myhx.css" />
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">
		<div class="row">
			<div class="col-sm-12">
				<!-- 新增开始 -->
				<div class="modal inmodal fade" id="myModal5" tabindex="-1"
					role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<form id="addForm" class="form-horizontal" action="saveCompany"
								method="post">
								<div class="modal-body">
									<div class="ibox-title">
										<h5>新增公司信息 <span style="color:red">（* 为必填项）</span></h5>
									</div>
									<div class="ibox-content">
										<div class="row">
											<!--公司名 -->
											<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red">*  </span>公司名 ：</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" name="name"
														id="name1">
												</div>
											</div>
											<!--公司电话 -->
											<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red">*  </span>公司电话 ：</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" name="cphone"
														id="cphone1" placeholder="如0532-86667395">
												</div>
											</div>
											<!--负责人姓名 -->
											<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red">*  </span>负责人姓名 ：</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" name="principal">
												</div>
											</div>
											<!--负责人电话 -->
											<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red">*  </span>负责人电话 ：</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" name="pphone"
														id="pphone1">
												</div>
											</div>
											<!--法人代表姓名 -->
											<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red">*  </span>法人代表姓名 ：</label>
												<div class="col-sm-7">
													<input class="form-control" type="text" name="lagal">
												</div>
											</div>
											<!--公司地址-->
											<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red">*  </span>公司地址 ：</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" name="address">
												</div>
											</div>
											<!--营业执照号-->
											<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red">*  </span>营业执照号 ：</label>
												<div class="col-sm-7">
													<input class="form-control" type="text" name="blNum">
												</div>
											</div>
											<!--企业组织机构代码-->
											<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red">*  </span>企业组织机构代码 ：</label>
												<div class="col-sm-7">
													<input class="form-control" type="text" name="ccode">
												</div>
											</div>
											<!--注册资金 -->
											<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red">*  </span>注册资金 ：</label>
												<div class="col-sm-7">
													<input class="form-control" type="text" name="refund"
														id="refund1" placeholder="请输入正整数，如123456">
												</div>
											</div>
											<!--经济类型 -->
											<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red">*  </span>经济类型 ：</label>
												<div class="col-sm-7">
													<input class="form-control" type="text" name="etype"
														placeholder="如私企、国企">
												</div>
											</div>
											<!--备 注 -->
											<div class="form-group">
												<label class="col-sm-3 control-label">备 注：</label>
												<div class="col-sm-7">
													<textarea style="height:60px;resize: none;" name="remark"
														class="form-control" aria-required="true"></textarea>
												</div>
											</div>
										</div>

										<div class="modal-footer">
											<button type="button" class="btn btn-white"
												data-dismiss="modal">关闭</button>
											<!--gy <button type="button" class="btn btn-primary"
										onclick="addForm()">保存</button> -->
											<button type="submit" class="btn btn-primary">保存</button>
										</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 点击添加按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal6" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form id="addForm" action="saveCompany" method="post">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>新增公司信息</h5>
							</div>
							<div class="ibox-content">
								<div class="row">
									<div class="col-sm-4">
										公 司 名：<br> <input type="text" name="name" id="name1">
										<span style="color:red;font-size:20px;"> * </span>
									</div>
									<div class="col-sm-4">
										公 司 电 话：<br> <input type="text" name="cphone"
											id="cphone1" placeholder="如0532-86667395">
									</div>
									<div class="col-sm-4">
										负责人姓名：<br> <input type="text" name="principal">
									</div>
								</div>
								<br>

								<div class="row">
									<div class="col-sm-4">
										负责人电话：<br> <input type="text" name="pphone" id="pphone1"
											placeholder="">
									</div>
									<div class="col-sm-4">
										法人代表姓名：<br> <input type="text" name="lagal">
									</div>
									<div class="col-sm-4">
										公 司 地 址：<br> <input type="text" name="address">
									</div>
								</div>
								<br>

								<div class="row">
									<div class="col-sm-4">
										营业执照号：<br> <input type="text" name="blNum">
									</div>
									<div class="col-sm-4">
										企业组织机构代码：<br> <input type="text" name="ccode">
									</div>
									<div class="col-sm-4">
										注 册 资 金：<br> <input type="text" name="refund"
											id="refund1" placeholder="请输入正整数，如123456">
									</div>
								</div>
								<br>

								<div class="row">
									<div class="col-sm-4">
										经 济 类 型：<br> <input type="text" name="etype"
											placeholder="如私企、国企">
									</div>
									<div class="col-sm-4">
										备 注：
										<textarea style="height:60px;resize: none;" name="remark"
											class="form-control" aria-required="true"></textarea>
									</div>
								</div>

							</div>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
							<!--gy <button type="button" class="btn btn-primary"
										onclick="addForm()">保存</button> -->
							<button type="submit" class="btn btn-primary">保存</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- 点击"修改"按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal1" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form action="updateCompany" class="form-horizontal" method="post"
						id="updateForm">
						<input id="id" name="id" type="hidden">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>修改公司信息<span style="color:red">（* 为必填项）</span></h5>
							</div>
							<div class="ibox-content">
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 公司名 -->
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>公司名 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="name" autocomplete="off"
													id="updateName">
											</div>
										</div>
									</div>
									<!-- 公司电话 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>公司电话 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="cphone" autocomplete="off"
													id="cphone">
											</div>
										</div>

									</div>
								</div>
								<!-- 第二行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 负责人姓名 -->
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>负责人姓名 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="principal" autocomplete="off"
													id="principal">
											</div>
										</div>
									</div>
									<!-- 负责人电话 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>负责人电话 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="pphone" autocomplete="off"
													id="pphone">
											</div>
										</div>
									</div>
									
								</div>
								
								<!-- 第三行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 公司地址 -->
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>公司地址 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="address" autocomplete="off"
													id="address">
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 营业执照号-->
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>营业执照号 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="blNum" autocomplete="off"
													id="blNum">
											</div>
										</div>
									</div>
								</div>
								<!-- 第四行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 企业组织机构代码 -->
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>企业组织机构代码 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="ccode" autocomplete="off"
													id="ccode">
											</div>
										</div>
									</div>
									<!-- 注册资金-->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>注册资金 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="refund" autocomplete="off"
													id="refund">
											</div>
										</div>
									</div>
								</div>
								<!-- 第五行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 经济类型 -->
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>经济类型 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="etype" autocomplete="off"
													id="etype">
											</div>
										</div>
									</div>
									
									<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label"><span style="color:red">*  </span>法人代表姓名 ：</label>
										<div class="col-sm-8">
											<input class="form-control" type="text" id="lagal" name="lagal" autocomplete="off">
										</div>
										</div>
									</div>
							
								</div>
								<div class="row">
											<!-- 备注-->
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-sm-2 control-label">备注：</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" name="remark" autocomplete="off"
													id="remark">
											</div>
										</div>
									</div>
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-white"
										data-dismiss="modal">关闭</button>
									<!-- 						gy修改前			<button type="button" class="btn btn-primary" -->
									<!-- 										id="updateSaveBtn">保存</button> -->
									<button type="submit" class="btn btn-primary">保存</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>


		<!-- 点击"查看详情"按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal2" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form action="updateCompany" method="post">
						<!-- 			gy修改前		<form action="updateCompany" method="post" id="updateForm"> -->
						<input id="id" name="id" type="hidden">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>查看公司详情</h5>
							</div>
							<div class="ibox-content" >
								<div class="row">
									<div class="col-sm-4">
										公&nbsp;&nbsp;司&nbsp;&nbsp;名&nbsp;&nbsp;：&nbsp;&nbsp; <span
											id="name2" style="font-weight:bold"></span>
									</div>
									<div class="col-sm-4">
										公&nbsp;&nbsp;司&nbsp;&nbsp;电&nbsp;&nbsp;话：&nbsp;&nbsp;<span
											id="cphone2" style="font-weight:bold"></span>
									</div>
									<div class="col-sm-4">
										负责人姓名：&nbsp;&nbsp;<span id="principal2"
											style="font-weight:bold"></span>
									</div>
								</div>
								<br>

								<div class="row">
									<div class="col-sm-4">
										负责人电话：&nbsp;&nbsp;<span id="pphone2" style="font-weight:bold"></span>
									</div>
									<div class="col-sm-4">
										法人代表姓名：&nbsp;&nbsp;<span id="lagal2" style="font-weight:bold"></span>
									</div>
									<div class="col-sm-4">
										公 司 地 址：&nbsp;&nbsp;<span id="address2"
											style="font-weight:bold"></span>
									</div>
								</div>
								<br>

								<div class="row">
									<div class="col-sm-4">
										营业执照号：&nbsp;&nbsp; <span style="font-weight:bold" id="blNum2"></span>
									</div>
									<div class="col-sm-4">
										组织机构代码：&nbsp;&nbsp;<span style="font-weight:bold" id="ccode2"></span>
									</div>
									<div class="col-sm-4">
										注 册 资 金：&nbsp;&nbsp;<span style="font-weight:bold"
											id="refund2"></span>
									</div>
								</div>
								<br>

								<div class="row">
									<div class="col-sm-4">
										经 济 类 型：&nbsp;&nbsp; <span id="etype2"
											style="font-weight:bold"></span>
									</div>
									<div class="col-sm-8">
										备 注：&nbsp;&nbsp;<span id="remark2" style="font-weight:bold"></span>
									</div>
								</div>
								<div class="modal-footer" style="height: 40px">
									<button type="button" class="btn btn-white"
										data-dismiss="modal">关闭</button>
								</div>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
	 
	<!-- 数据展示 -->
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox-title">
				<h5>公司信息表</h5>
			</div>
			<div class="ibox-content"  style="height:790px">

			
				<table  id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
					<thead>
					<tr>
					<td><button id="btn_addCompany" type="button"
						class="btn btn-primary btn-add1" data-toggle="modal"
						data-target="#myModal5">添&nbsp;&nbsp;&nbsp;加</button></td>
					</tr>
						<tr>
							<th>公司名称</th>
							<th>公司电话</th>
							<th>负责人</th>
							<th>联系电话</th>
							<!-- <th>法人代表</th> -->

							<th>公司地址</th>
							<th>营业执照注册号码</th>
							<!-- <th>企业组织机构代码</th>
							<th>注册资金</th> -->
							<th>经济类型</th>

							<!-- <th>备注</th> -->
							<!-- <th>操作人</th>
							<th>添加时间</th> -->
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${companies}" var="companies">
							<tr id="${companies.id}">
								<td>${companies.name }</td>
								<td>${companies.cphone }</td>
								<td>${companies.principal}</td>
								<td>${companies.pphone}</td>
								<%-- <td>${companies.lagal }</td> --%>

								<td>${companies.address}</td>
								<td>${companies.blNum}</td>
								<%-- <td>${companies.ccode }</td>
								<td>${companies.refund}</td> --%>
								<td>${companies.etype}</td>

								<%-- <td>${companies.remark }</td> --%>
								<%-- <td>${companies.people}</td>
								<td>${companies.time}</td> --%>
								<td>
									<button type="button" class="btn btn-success btn-sm"
										data-toggle="modal" data-target="#myModal2"
										onclick="check2('${companies.name}','${companies.cphone}','${companies.principal}','${companies.pphone}','${companies.lagal}'
										,'${companies.address}','${companies.blNum}','${companies.ccode}','${companies.refund}','${companies.etype}'
										,'${companies.remark}')">查看详情</button>
									<button id="btn-update" type="button"
										class="btn btn-success btn-sm" data-toggle="modal"
										data-target="#myModal1"
										onclick="update('${companies.id}','${companies.name}','${companies.cphone}','${companies.principal}','${companies.pphone}','${companies.lagal}'
										,'${companies.address}','${companies.blNum}','${companies.ccode}','${companies.refund}','${companies.etype}'
										,'${companies.remark}');ss()">修改</button>
									<button type="button" class="btn btn-danger btn-sm"
										onclick="delete1('${companies.id}','${companies.name}')">删除</button>
								</td>
							</tr>
						</c:forEach>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="8">
								<ul class="pagination pull-right"></ul>
							</td>
						</tr>
					</tfoot>
				</table>
			</div>
			<!-- 删除表格 -->
			<form id="deleteForm" action="deleteCompany" method="post">
				<input type="hidden" id="deleteId" name="id">
			</form>
		</div>
	</div>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<!--  <script src="js/jquery.min.js?v=2.1.4"></script>-->
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script>
		//加载footable的功能
		$(document).ready(function() {
		$('#table1').DataTable({
			"pagingType" : "full_numbers"
		});
	});
		
		//点击查看详情
		function check2(name, cphone, principal, pphone, lagal, address,
				blNum, ccode, refund, etype, remark){
			$("#name2").text(name);
			$("#cphone2").text(cphone);
			$("#principal2").text(principal);
			$("#pphone2").text(pphone);
			$("#lagal2").text(lagal);

			$("#address2").text(address);
			$("#blNum2").text(blNum);
			$("#ccode2").text(ccode);
			$("#refund2").text(refund);
			$("#etype2").text(etype);
			$("#remark2").text(remark);
		}

		//验证添加的信息
		function addForm() {
			var name1 = $("#name1").val();//公司名
			var cphone1 = $("#cphone1").val();//公司电话
			var pphone1 = $("#pphone1").val();//负责人联系方式（可以是电话、手机）
			var refund1 = $("#refund1").val();
			var phone = /^((0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/;//电话格式验证
			//var cellPhone = /^1[34578]\d{9}$/;//手机号格式验证
			//var money=/^\d{n}$/; //纯数字验证
			var money = /^[0-9]*[0-9]$/;//正整数验证
			if (name1 == "") {
				$("#name1").focus();//光标移动到公司名输入框
				swal({
					title : "公司名称不能为空",
					type : "warning"
				});
			} else if (!phone.test(cphone1) && cphone1 != "") {
				$("#cphone1").focus();
				swal({
					title : "请输入正确的公司电话",
					type : "warning"
				});
			} /* else if(!phone.test(pphone1) && pphone1 != "") {
					swal({
						title : "请输入正确的负责人电话",
						type : "warning"
					});
			} */ else if (!money.test(refund1) && refund1 != "") {
				$("#refund1").focus();
				swal({
					title : "注册资金应为一个正整数",
					type : "warning"
				});
			} else {
				$("#addForm").submit();
			}

		}

		//点击"修改"按钮，回显数据
		function update(id, name, cphone, principal, pphone, lagal, address,
				blNum, ccode, refund, etype, remark) {
			$("#id").val(id);
			$("#updateName").val(name);
			$("#cphone").val(cphone);
			$("#principal").val(principal);
			$("#pphone").val(pphone);
			$("#lagal").val(lagal);

			$("#address").val(address);
			$("#blNum").val(blNum);
			$("#ccode").val(ccode);
			$("#refund").val(refund);
			$("#etype").val(etype);
			$("#remark").val(remark);
		};

		//点击"删除"按钮
		function delete1(id, name) {
			swal({
				title: "您确定删除 " + name + " 的数据吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
				/* var url="deleteCompany";
				$.post(url,{"id":id},function(date){
					if(date == "操作执行成功"){
						swal({
							title : date,
							type : "success"
						});
				
					}else{
						swal({
							title : date,
							type : "warning"
						});
					}
		
				if(date!="出现未知错误"){
					$("#"+id).hide();
				}
				});
			}); */
				 $("#deleteId").val(id);
					//提交表单
				$("#deleteForm").submit();
		});
		}
		//页面加载完成后执行,获取执行状态
		$(document).ready(function() {
			//这里请保证 ''在一行，不可分割
			var result = '<%=request.getAttribute("result")%>';
			if (result != null && result != "null") {
				if(result.indexOf("成功")>0){
					swal({
						title : result,
						type : "success"
					});
			
				}else{
					swal({
						title : result,
						type : "warning"
					});
				}
				
			}
		});

		//修改-点击保存“添加”的按钮
		$("#updateSaveBtn").click(function() {
			var dName = $("#updateName").val();
			if (dName == "" || dName == null) {
				$("#dName").focus();
				swal({
					title : "姓名不能为空！",
					type : "warning"
				});
			} else {
				$("#updateForm").submit();
			}
		});
	</script>

	<!-- gy 添加界面表单验证 -->
	<script type="text/javascript">
		/*添加公司界面的表单验证*/
		$('#btn_addCompany')
				.click(
						function() {
							$('#addForm')
									.bootstrapValidator(
											{
												feedbackIcons : {/*输入框不同状态，显示图片的样式*/
													valid : 'glyphicon glyphicon-ok',
													invalid : 'glyphicon glyphicon-remove',
													validating : 'glyphicon glyphicon-refresh'
												},
												/*生效规则：字段值一旦变化就触发验证*/
												live : 'enabled',
												/*当表单验证不通过时，该按钮为disabled*/
												submitButtons : 'button[type="submit"]',
												/*验证*/
												fields : {
													name : {/*键名username和input name值对应*/
														message : 'The username is not valid',
														validators : {
															notEmpty : {/*非空提示*/
																message : '公司名不能为空'
															}
														}
													},
													cphone : {
														message : '',
														validators : {
															notEmpty : {
																message : '公司电话不能为空'
															},
															numeric : {
																message : '请输入正确的电话号码'
															}
														}
													},
													principal : {
														validators : {
															notEmpty : {
																message : '负责人姓名不能为空'
															}
														}
													},
													pphone : {
														validators : {
															notEmpty : {
																message : '负责人电话不能为空'
															},
													
														}
													},
													lagal : {
														validators : {
															notEmpty : {
																message : '法人代表姓名不能为空'
															}
														}
													},
													address : {
														validators : {
															notEmpty : {
																message : '公司地址不能为空'
															}
														}
													},
													blNum : {
														validators : {
															notEmpty : {
																message : '营业执照号码不能为空'
															}
														}
													},
													ccode : {
														validators : {
															notEmpty : {
																message : '企业组织机构代码不能为空'
															}
														}
													},
													refund : {
														validators : {
															notEmpty : {
																message : '注册资金不能为空'
															},
															numeric : {
																message : '请输入数字'
															}
														}
													},
													etype : {
														validators : {
															notEmpty : {
																message : '经济类型不能为空'
															}
														}
													}

												}
											});
						});

		/*公司界面的表单验证*/

		function ss() {
			$('#updateForm')
					.bootstrapValidator(
							{
								feedbackIcons : {/*输入框不同状态，显示图片的样式*/
									valid : 'glyphicon glyphicon-ok',
									invalid : 'glyphicon glyphicon-remove',
									validating : 'glyphicon glyphicon-refresh'
								},
								/*生效规则：字段值一旦变化就触发验证*/
								live : 'enabled',
								/*当表单验证不通过时，该按钮为disabled*/
								submitButtons : 'button[type="submit"]',
								/*验证*/
								fields : {
									name : {/*键名username和input name值对应*/
										validators : {
											notEmpty : {/*非空提示*/
												message : '公司名不能为空'
											}
										}
									},
									cphone : {
										message : '',
										validators : {
											notEmpty : {
												message : '公司电话不能为空'
											},
											numeric : {
												message : '请输入正确的电话号码'
											}
										}
									},
									principal : {
										validators : {
											notEmpty : {
												message : '负责人姓名不能为空'
											}
										}
									},
									pphone : {
										validators : {
											notEmpty : {
												message : '负责人电话不能为空'
											},
										}
									},
									lagal : {
										validators : {
											notEmpty : {
												message : '法人代表姓名不能为空'
											}
										}
									},
									address : {
										validators : {
											notEmpty : {
												message : '公司地址不能为空'
											}
										}
									},
									blNum : {
										validators : {
											notEmpty : {
												message : '营业执照号码不能为空'
											}
										}
									},
									ccode : {
										validators : {
											notEmpty : {
												message : '企业组织机构代码不能为空'
											}
										}
									},
									refund : {
										validators : {
											notEmpty : {
												message : '注册资金不能为空'
											},
											numeric : {
												message : '请输入数字'
											}
										}
									},
									etype : {
										validators : {
											notEmpty : {
												message : '经济类型不能为空'
											}
										}
									}

								}
							});
		};
	</script>
</body>

</html>