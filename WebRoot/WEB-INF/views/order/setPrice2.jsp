<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>H+ 后台主题UI框架 - 基本表单</title>
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link rel="shortcut icon" href="favicon.ico">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- <base target="_blank"> -->

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="ibox float-e-margins" id="sendText">
				<div class="ibox-title">
					<h5>远程调价设置:</h5>
				</div>
				<div class="ibox-content">
					<form class="form-horizontal" action="savePrice" method="post"
						id="getOneForm">  
						<div class="form-group">
							<label class="col-sm-3">选中的车牌号：</label>
							<div class="col-sm-8">
								<input class="form-control" type="text" name="taxiNums"
									id="taxiNums" value="${taxiNum}">   
								 
							</div>
							<br>
						</div>
			 			<div class="row">
							<label class="col-sm-3 label-control ">设置单价(元):</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" name="price"
									id="rentControl">
							</div>
							<br> <br>
						</div>
						<div class="row">
							<label class="col-sm-3 label-control ">设置租金(元):</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" name="rent"
									id="priceControl">
							</div>
						</div>
						<div class="row">
							<label class="col-sm-3 label-control ">设置续程公里(千米):</label>
							<div class="col-sm-3">
								<input type="text" class="form-control" name="km"
									id="kmControl">
							</div>
							<br> <br> <br> <br> <br> <br> <br>
							<br> <br>
						</div>
					 
					</form>
					 	<div class="form-group">
						<div class="col-sm-offset-3 col-sm-4">
 
							<button class="btn  btn-primary" type="text" id="getOneBtn">发&nbsp;&nbsp;&nbsp;送</button>&nbsp;&nbsp;
 
							<a href="getTreeOfTemp"><button type="button"
 
									class="btn">返&nbsp;&nbsp;&nbsp;回</button></a>
 
							<!-- <a href="getTreeOfTemp"><button type="button">返回</button></a> -->
						</div>
					</div>
					<br>
				</div>
			</div>

			<!-- 发送结果展示 -->
			<div class="ibox float-e-margins" id="resultShow">
				<div class="ibox-title">
					<h5>远程调价下发结果展示</h5>
				</div>
				<div class="ibox-content"  style="height: 770px">

					<table width="100%" style="table-layout:fixed;background:#FFFFFF"
						class="footable table table-stripped" data-page-size="4"
						data-filter=#filter>
						<thead>
							<tr>
								<th width="100">车牌号</th>
								<th width="100">异常状态</th>
							</tr>
						</thead>
						<tbody>
					<c:forEach items="${taxi}" var="taxi">
						<tr>
							<td>${taxi.taxiNum}</td>
							<td>${taxi.taxiType}</td>
					</tr>
					</c:forEach>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="2">
							<ul class="pagination pull-right"></ul>
						</td>
					</tr>
				</tfoot>
			</table>
			<br>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-4">
							<button class="btn btn-primary" type="button" id="getOneBtn">发&nbsp;&nbsp;&nbsp;送</button>
							&nbsp;&nbsp; <a href="getTreeOfTemp"><button type="button"
									class="btn  btn-white">返&nbsp;&nbsp;&nbsp;回</button></a>

					</div>
				</div>
				</div>
			</div>
		</div>
	</div>

	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/content.min.js?v=1.0.0"></script>
	<script type="text/javascript">
 
		//页面加载完成后执行,获取执行状态
		$(document).ready(function(){
			var result='<%=request.getAttribute("result")%>';
			if (result != "null") {

				if (result != "发送成功") {
					//当发送不完全成功时隐藏发送框
					$("#sendText").hide();
					$("#resultShow").show();
					swal({
						title : result,
						type : "error"
					});
				} else {
					$("#resultShow").hide();
					$("#sendText").show();
					swal({
						title : result,
						type : "success"
					});
				}
			}
		});
	</script>
</body>

</html>