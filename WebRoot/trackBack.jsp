<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">


<script src="js/jquery.js"></script>
<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/myhx.css" rel="stylesheet">

<script type="text/javascript"
	src="http://api.map.baidu.com/api?v=2.0&ak=xj2Gd1nMEwDHFcGeQvr0WORGk0t1Dlhw"></script>
<!--加载鼠标绘制工具-->
<script type="text/javascript"
	src="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.js"></script>
<link rel="stylesheet"
	href="http://api.map.baidu.com/library/DrawingManager/1.4/src/DrawingManager_min.css" />
<!--加载检索信息窗口-->
<script type="text/javascript"
	src="http://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.js"></script>
<link rel="stylesheet"
	href="http://api.map.baidu.com/library/SearchInfoWindow/1.4/src/SearchInfoWindow_min.css" />

<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="js/lushu.js"></script>

<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="js/bootstrap.min.js?v=3.3.5"></script>
<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="js/content.min.js?v=1.0.0"></script>
<script src="js/plugins/footable/footable.all.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/layer.js"></script>
<script src="js/jquery-2.0.0.min.js"></script>
<!-- 路书的js -->
<style type="text/css">
#allmap label {
	max-width: none;
}

.state-wrap {
	padding-bottom: 16px;
	position: absolute;
	left: 0;
	top: 0;
	font-size: 12px;
	line-height: 16px;
	background: url(img/map_label.png) no-repeat left bottom;
}

.logistics-wrap {
	background: #fff;
	padding: 10px;
}

.logistics-taxinum, .logistics-time, .logistics-direction {
	padding-left: 25px;
	height: 25px;
	line-height: 25px;
	/*                 width: 125px; */
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}

.logistics-taxinum {
	margin-left: 5px;
	background: url(img/plate_icon.png) no-repeat left center;
}

.logistics-time {
	margin-left: 5px;
	background: url(img/clock_icon.png) no-repeat left center;
}

.logistics-direction {
	margin-left: 5px;
	background: url( img/direction_icon.png) no-repeat left center;
}
</style>
</head>

<body class="gray-bg">
	<div class="wrapper animated fadeInRight ">
		<!-- row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-content" style="height: 830px">
						<%
							String taxiNum3 = request.getParameter("thisTaxiNum");
							//  String taxiNum3 = (String) session.getAttribute("taxiNum");
							System.out.println("here!!!!!!"+taxiNum3);
						%>
  					<h5>请输入查询条件</h5>
						<form class="form-horizontal" id="getAllAddressForm">
						<div class="row">
							<div class="form-group">
								<label class="col-sm-1 control-label">开始时间</label>
								<div class="col-sm-2">
									<input type="text" class="form-control" name="startDate"
										id="startDate" class="Wdate"
										onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'endDate\')||\'new Date()\'}',readOnly:true})">
								</div>
								<label class="col-sm-1 control-label">结束时间</label>
								<div class="col-sm-2">
									<input type="text" class="form-control" name="endDate"
										id="endDate" class="Wdate"
										onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'startDate\')}',maxDate:'#F{$dp.$D(\'startDate\',{H:4})}',dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true})">
								</div>
								<label class="col-sm-1 control-label">车牌号</label>
								<div class="col-sm-2">
									<input placeholder="鲁B00001" class="form-control"
										value="<%=taxiNum3%>" name="taxiNum" id="taxiNum" type="text" />
								</div>
									<div class="col-sm-2 col-sm-offset-1">
									<button id="selectTrack" type="button" onclick="query()" style="position: absolute ;left:-50px;width:100px"
										class="btn btn-primary ">查询轨迹</button>
								</div>
							</div>
						
							<!-- <div class="form-group">
								<label class="col-sm-1 control-label">精确运价</label>
								<div class="col-sm-2">
									<input class="form-control" name="fare" id="fare" type="text" />
								</div>
								<label class="col-sm-2 control-label">浮动运价起</label>
								<div class="col-sm-2">
									<input class="form-control" name="floatingFare1"
										id="floatingFare1" type="text" />
								</div>
								<label class="col-sm-2 control-label">浮动运价止</label>
								<div class="col-sm-2">
									<input class="form-control" name="floatingFare2"
										id="floatingFare2" type="text" />
								</div> 
								<div class="col-sm-2 ">
									<button id="selectTrack" type="button" onclick="query()"
										class="btn btn-primary ">查询轨迹</button>
								</div>
							</div> -->
						</div>
					</form>
					
				<div class="col-sm-11">
						<div id="allmap">
 
							<div id="map_canvas" style="height: 700px"></div>
 

			<!-- 				<div class="ibox float-e-margins">
								<br>
								<button id="run">开始</button>
								<button id="stop">停止</button>
								<button id="pause">暂停</button>
								<button id="speedUp">加速</button>
								<button id="speedCut">减速</button>
							</div> -->
						</div>
					</div>
					<div class="col-sm-1">
								<button id="run" class="btn btn-success btn-block">开始</button><br><br>
								<button id="stop" class="btn btn-danger btn-block">停止</button><br><br>
								<button id="pause" class="btn btn-warning btn-block">暂停</button><br><br>
								<button id="speedUp" class="btn btn-info btn-block">加速</button><br><br>
								<button id="speedCut" class="btn btn-primary btn-block">减速</button>
					</div>
					
					</div>
					</div>
 
				</div>
			
			</div><!-- row -->
		</div>

	</div>
	<!-- row -->

	<script type="text/javascript">
		function getQueryString(name) {
			var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
			var r = window.location.search.substr(1).match(reg);
			if (r != null) return unescape(r[2]); return null;
		}
		var date = new Date();
		var year = date.getFullYear();
		var month = date.getMonth()+1;
		var day = date.getDate();
		startDate = year + "-" + month + "-"+day+" "+"00:00:00";
		var hour = date.getHours();
		var minute = date.getMinutes();
		var seacond = date.getSeconds();
		if(seacond<10){
			seacond = "0" + seacond;
		}
		endDate = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seacond;
		document.getElementById("startDate").value = startDate;
		document.getElementById("endDate").value = endDate;

		//car的点击，用来判断小车的信息框是否隐藏,true的隐藏，false的时候显示
		var isClick = true;
		//加载中遮罩
		var ii;
		//车速，默认200
		var speed = 200;
		//是否暂停
		var isPause = false;
		//是否停止
		var isStop = false;
		//marker移动中的文字信息[车牌号 时间 速度方向]
		var label;
		//转成百度坐标后的点的集合的索引
		var index = 0;
		//所有点的集合	
		var datas = [];
		//车牌号
		var TAXINUM = "";
		var map = new BMap.Map('map_canvas');
		var overView = new BMap.OverviewMapControl();
		//添加是
		var overViewOpen = new BMap.OverviewMapControl({
			isOpen : true,
			anchor : BMAP_ANCHOR_BOTTOM_RIGHT
		});
		//map.enableScrollWheelZoom();//添加鼠标滚动功能
		map.addControl(overView); //添加默认缩略地图控件
		// 		map.addControl(overViewOpen); //右下角，打开
		map.addControl(new BMap.NavigationControl());
		map.centerAndZoom("阜阳", 13);
		map.enableScrollWheelZoom();
		var points = [];
		//转成百度坐标后点的集合
		var b = [];
		var shuzu = 0; //数组的索引

		// 实例化一个驾车导航用来生成路线
		var drv = new BMap.DrivingRoute('阜阳', {
			onSearchComplete : function() {
				//数据库中的坐标点进行转换，转换完成绘制路线
				convert(datas);
			}
		});//onSearchComplete结束

		//绑定事件
		$("run").onclick = function() {
			isStop = false;
			isPause = false;
			//marker开始移动
			play();
		};
		$("stop").onclick = function() {
			isStop = true;
		};
		$("pause").onclick = function() {
			isPause = true;
		};

		//加速按钮，不能将时间减到0
		$("speedUp").onclick = function() {

			if (speed > 0) {
				speed = speed - 50;
			}
		};
		//减速按钮，时间不能无限大
		$("speedCut").onclick = function() {
			if (speed < 400) {
				speed = speed + 50;
			}
		};

		function $(element) {
			return document.getElementById(element);
		}

		//将数据库中所有的坐标点进行分组
		function convert(datas) {
			//逻辑是：每十个点作为一个小数组，这些小数组再放到大数组中，这么做的原因是百度地图限制只能描绘10个点的路线
			var len = datas.length / 10 + (1 - (datas.length / 10 % 1)) % 1;
			for (var i = 0; i < len; i++) {
				eval("var arrPois" + i + " = []");
			}
			for (var j = 0; j < datas.length; j++) {

				eval("var point" + j + " = new BMap.Point("
						+ datas[j].longitude + "," + datas[j].latitude + ")");
				eval("arrPois" + parseInt(j / 10) + " = arrPois"
						+ parseInt(j / 10) + ".concat(point" + j + ")");
			}

			//转换坐标回调函数
			translateCallback = function(data) {
				if (data.status === 0) {
					var points = [];
					for (var i = 0; i < data.points.length; i++) {
						//遍历获取到转换后的坐标
						points = points.concat(data.points[i]);
						//points.push(data.points[i]);
					}

					//将转换后的坐标数组并到大数组里
					b = b.concat(points);

					//判断转换是否全部完成，若完成则绘制路线，若没有完成则继续进行转换
					if (b.length == datas.length) {
						//将滚蛋条滑动到顶端
						document.getElementsByTagName('body')[0].scrollTop = 0;

						//设置marker的文字标注的样式
						label = new BMap.Label("", {
							offset : new BMap.Size(-26, -90)
						});
						label
								.setStyle({
									paddingBottom : "16px",
									position : "absolute",
									left : "0",
									top : "0",
									fontFamily : "微软雅黑",
									color : "black",
									border : "0",
									fontSize : "12px", //字号
									height : "100px", //高度
									width : "210px", //宽
									backgroundColor : "0.05",
									lineHeight : "16px", //行高，文字垂直居中显示
									background : "url(img/map_label.png) no-repeat left bottom", //背景图片，这是房产标注的关键！
								});
						//设置第一个marker显示的内容：第一个点的内容
						label.setContent('<div class="logistics-taxinum">'
								+ "车牌号:" + TAXINUM + '</div>'
								+ '<div class="logistics-direction">' + "速度："
								+ datas[0].speed + '</div>'
								+ '<div class="logistics-time">' + "时间:"
								+ datas[0].time + '</div>');
						var myIcon = new BMap.Icon('img/car.png',
								new BMap.Size(26, 52)); //定义自己的标注
						car = new BMap.Marker(
								new BMap.Point(b[0].lng, b[0].lat), {
									icon : myIcon
								});
						// 							car = new BMap.Marker(new BMap.Point(
						// 									b[0].lng, b[0].lat));
						car.setLabel(label);
						car.setRotation(datas[0].direction);
						map.addOverlay(car);

						//给小车添加点击事件监听
						car.addEventListener("click", promptSwitch);

						//设置终点的marker的内容
						label2 = new BMap.Label("", {
							offset : new BMap.Size(-20, -20)
						});
						car2 = new BMap.Marker(new BMap.Point(
								b[b.length - 1].lng, b[b.length - 1].lat));
						map.addOverlay(car2);

						//关闭遮罩
						layer.close(ii);
						//调用显示路线的函数
						display();
					} else {
						shuzu++; //数组标识加1
						//没有转换完成，继续转换
						var convertor = new BMap.Convertor();
						eval("convertor.translate(arrPois" + shuzu
								+ ", 1, 5,translateCallback)");
					}
				}
			};
			//定时器，为了实现循环调用坐标转换函数
			setTimeout(function() {
				var convertor = new BMap.Convertor();
				convertor.translate(arrPois0, 1, 5, translateCallback);
			}, 0);
		}

		//显示路线的函数			
		function display() {
			//视野范围、中心点自适应
			var view = map.getViewport(b);
			var mapZoom = view.zoom;
			var centerPoint = view.center;
			map.centerAndZoom(centerPoint, mapZoom);

			//根据转换后的百度坐标点绘制路线
			map.addOverlay(new BMap.Polyline(b, {
				strokeColor : 'blue'
			}));//向地图中添加导航线
			// 				将起始坐标点设置为地图的中心点显示
			// 				map.centerAndZoom(new BMap.Point(b[0].lng,b[0].lat), 15);

			// 				var points = [point1, point2,point3];  
			// 				map.setViewport(b);//设置视图端口
		}

		//让marker动起来
		function play() {
			var point = b[index];

			//从第2个点开始，每相邻两个绘制路线
			if (index > 0) {
				map.addOverlay(new BMap.Polyline([ b[index - 1], point ], {
					// 						strokeColor : "red",
					strokeWeight : 1,
					strokeOpacity : 1
				}));
			}
			//设置每个点要显示的内容以及内容的格式
			label.setContent('<div class="logistics-taxinum">' + "车牌号:"
					+ TAXINUM + '</div>' + '<div class="logistics-direction">'
					+ "速度:" + datas[index].speed + 'km/h' + '</div>'
					+ '<div class="logistics-time">' + "时间:"
					+ datas[index].time + '</div>');
			//设置小车位置
			car.setPosition(point);
			//设置小车角度
			car.setRotation(datas[index].direction);
			//每8个点调整一下地图的中心点，确保移动的marker在地图的中心显示
			if (index % 8 == 0) {
				map.centerAndZoom(point, map.getZoom());
			}
			// 				 map.setViewport([ b[index - 1], point ]);
			index = index + 1;
			//如果没有到最后一个点并且没有暂停没有停止,那么继续调用显示函数
			if (index < b.length && index != 0 && !isStop && !isPause) {
				timer = window.setTimeout("play(" + index + ")", speed);
			} else if (index == b.length || isStop) {
				//如果到最后一个点或者点击停止了，将索引归0
				index = 0;
			}
		}

		//显示或隐藏小车的信息
		function promptSwitch() {
			if (isClick) {
				map.removeOverlay(label);
				isClick = false;
			} else {
				car.setLabel(label);
				isClick = true;
			}
		}
	</script>

	<!-- row2 -->

	<script type="text/javascript">
		//按钮单击时执行
		function query() {
			var startTime = $("startDate").value;
			var endTime = $("endDate").value;
			var taxiNum = $("taxiNum").value;
	 	/* 	var fare = $("fare").value;
			var floatingFare1 = $("floatingFare1").value;
			var floatingFare2 = $("floatingFare2").value;   */
			var fare = "";
			var floatingFare1 ="";
			var floatingFare2 ="";  
			if (startTime == "" || endTime == "") {
				swal({
					title : "开始或结束时间不能为空",
					type : "warning"
				});
			} else if (taxiNum == "") {
				swal({
					title : "请输入车牌号",
					type : "warning"
				});
			} /* else if (!isDigit(fare)) {
				swal({
					title : "精准运价要求填数字",
					type : "warning"
				});
			} else if (!isDigit(floatingFare1)) {
				swal({
					title : "浮动运价起要求填数字",
					type : "warning"
				});
			} else if (!isDigit(floatingFare2)) {
				swal({
					title : "浮动运价止要求填数字",
					type : "warning"
				});
			} else if (floatingFare1 >= floatingFare2 && floatingFare1 != ""
					&& floatingFare2 != "") {
				swal({
					title : "浮动运价起的大小要小于浮动运价起的大小",
					type : "warning"
				});
			}  */
			else {
				//显示加载中...遮罩，绘制完路线后消失
				ii = layer.load(2, {
					shade : [ 0.3, '#393D49' ]
				});
				var str = "startDate=" + startTime + "&endDate=" + endTime
						+ "&taxiNum=" + taxiNum  + "&fare=" + fare
						+ "&floatingFare1=" + floatingFare1 + "&floatingFare2="
						+ floatingFare2 ;
				jQuery.ajax({
					type : "post",
					url : "getAllAddress",
					data : str,
					timeout : 60000,
					success : function(data) {
						//查询为空时弹窗提示
						if (data.result == "zero") {
							toastNull();
						} else if (data.result == "many") {
							toastMany();
						} else {
							//判断是否为空值
							var d = eval(data.result);
							if (typeof (d) == "undefined") {
								layer.close(ii);
								swal({
									title : "未查询到轨迹",
									type : "error"
								});
							} else {
								//清空所有覆盖物和坐标，避免对下一次查询有影响。
								//清空所有的覆盖
								map.clearOverlays();
								//数组计数归0
								shuzu = 0;
								//数组清空
								b = [];

								//查询数据不为空，将json数据转换为数组
								datas = eval(data.result);
								//获取车牌号
								TAXINUM = data.taxiNum;
								//把坐标转换为百度坐标，并绘制路线
								convert(datas);
							}
						}
					},
					//ajax获取数据失败
					error : function(data, e) {
						//关闭遮罩
						layer.close(ii);
						swal({
							title : "查询失败或超时了...",
							type : "error"
						});
					}
				});
			}
		}

		//查询信息为空时的提示框
		function toastNull() {
			swal({
				title : "未查询到该车辆在指定时间段内的位置信息",
				type : "warning"
			});
			layer.close(ii);
		}
		//查询信息的提示框
		function toastLoading() {
			swal({
				title : "轨迹加载中",
				type : "warning"
			});
			layer.close(ii);
		}
		//查询信息的提示框
		function toastMany() {
			swal({
				title : "有多条轨迹",
				type : "warning"
			});
			layer.close(ii);
		}
		function isDigit(s) {
			var patrn = /^[0-9]+([.]{1}[0-9]+){0,1}$/;
			if (!patrn.exec(s) && s != "")
				return false;
			return true;
		}
	</script>
</body>

</html>