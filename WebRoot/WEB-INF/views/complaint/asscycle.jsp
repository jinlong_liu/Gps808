<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">

<!-- gy 添加表单验证 -->
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />

<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
<style type="text/css">
.form-control-gy {
	width: 30%;
	position:relative;
	left:650px;
	display: inline;
}
</style>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">	
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox-title">
			<h5>考核周期表</h5>
			</div>
			<div class="ibox-content" style="height: 780px">
					
				<table  id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
					<thead>
					<tr><td><button type="button" class="btn btn-primary" data-toggle="modal"
						data-target="#myModal5">添&nbsp;&nbsp;&nbsp;加</button>&nbsp;</td></tr>
						<tr>
							<th>周期名称</th>
							<th>司机初始分数</th>
							<th>司机分数下限</th>
							<th>司机分数上限</th>
							<th>汽车初始分数</th>
							<th>汽车分数下限</th>
							<th>汽车分数上限</th>
							<th>公司初始分数</th>
							<th>公司分数下限</th>
							<th>公司分数上限</th>
							<th>操作</th>

						</tr>
					</thead>
					<tbody>
						<c:forEach items="${asscycle}" var="asscycle">
							<tr id="${asscycle.id}">
								<td>${asscycle.name}</td>
								<td>${asscycle.initScorer}</td>
								<td>${asscycle.lower}</td>
								<td>${asscycle.toper}</td>
								<td>${asscycle.initScorCar}</td>
								<td>${asscycle.lowCar}</td>
								<td>${asscycle.topCar}</td>
								<td>${asscycle.initScorPany}</td>
								<td>${asscycle.lowPany}</td>
								<td>${asscycle.topPany}</td>
								<td><button id="btn-update" type="button"
										class="btn btn-success btn-sm" data-toggle="modal"
										data-target="#myModal1"
										onclick="alterScore('${asscycle.id}','${asscycle.name}','${asscycle.initScorer}','${asscycle.lower}',
											'${asscycle.toper}','${asscycle.initScorCar}','${asscycle.lowCar}','${asscycle.topCar}','${asscycle.initScorPany}',
											'${asscycle.lowPany}','${asscycle.topPany}')">更改</button>
									<button type="button" class="btn btn-danger btn-sm"
										onclick="submit('${asscycle.id}')">删除</button></td>

							</tr>
						</c:forEach>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="11">
								<ul class="pagination pull-right"></ul>
							</td>
						</tr>
					</tfoot>
				</table>
			</div>
			<form id="deleteForm" action="deleteAsscycle" method="post">
				<input type="hidden" id="deleteId" name="id">
			</form>
		</div>
	<!-- 点击添加按钮弹出来的form表单 -->
					<div class="modal inmodal fade" id="myModal5" tabindex="-1"
						role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">

							<form action="updateAsscycle" class="form-horizontal" id="addAsscycleForm" method="post">
							     <fieldset>
							        <div class="modal-body">
							            <div class="ibox-title">
							               	 <h5>添加考核周期信息<span style="color:red">（*为必填项） </span></h5>
							            </div>
							            <div class="ibox-content">
							                <div class="row">
							              <!-- 修改後的週期名稱 -->
											<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red">* </span>周期名称：</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" name="name" />
												</div>
											</div>
<!-- 							                    <div class="col-sm-6"> -->
<!-- 							                        <div class="form-group"> -->
<!-- 							                            <label class="col-sm-4 control-label">周期名称：</label> -->
<!-- 							                            <input class="form-control" type="text" name="name"> -->
<!-- 							                        </div> -->
<!-- 							                    </div> -->
							              <!-- 修改後的週期名稱 -->
											<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red">* </span>司机初始分数：</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" name="initScorer" />
												</div>
											</div>
							               
<!-- 							                    <div class="col-sm-6"> -->
<!-- 							                        <div class="form-group"> -->
<!-- 							                            <label class="col-sm-4 control-label">司机初始分数：</label> -->
<!-- 							                            <input class="form-control" type="text" name="initScorer"> -->
<!-- 							                        </div> -->
<!-- 							                    </div> -->
                                            <!-- 修改後的車輛初始分數 -->
											<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red">* </span>车辆初始分数：</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" name="initScorCar" />
												</div>
											</div>
<!-- 							                <div class="row"> 這一行不一定要註釋掉-->
<!-- 							                    <div class="col-sm-6"> -->
<!-- 							                        <div class="form-group"> -->
<!-- 							                            <label class="col-sm-4 control-label">车辆初始分数：</label> -->
<!-- 							                            <input class="form-control" type="text" name="initScorCar"> -->
<!-- 							                        </div> -->
<!-- 							                    </div> -->
                                            <!-- 修改後的車輛初始分數 -->
											<div class="form-group">
												<label class="col-sm-3 control-label"><span style="color:red">* </span>公司初始分数：</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" name="initScorPany" />
												</div>
											</div>							
<!-- 							                    <div class="col-sm-6"> -->
<!-- 							                        <div class="form-group"> -->
<!-- 							                            <label class="col-sm-4 control-label">公司初始分数：</label> -->
<!-- 							                            <input class="form-control" type="text" name="initScorPany"> -->
<!-- 							                        </div> -->
<!-- 							                    </div> -->
							                </div>
							<!-- 页脚  -->
							        <div class="modal-footer">
							            <button type="button" class="btn btn-white"
							                    data-dismiss="modal">取消
							            </button>
							            <button type="submit" class="btn btn-primary">提交</button>
							        </div>
							        </div>
							        </div>
							
							
							    </fieldset>
							</form>
							</div>
						</div>
					</div>
					
					</div>
		<!-- 点击"修改"按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal1" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form action="updateAsscycle"  class="form-horizontal"  id="updateAsscycleForm" method="post">
					    <fieldset>
					        <div class="modal-body">
					            <div class="ibox-title">
					                <h5>修改考核周期信息 <span style="color:red">（*为必填项） </span></h5>
					            </div>
					            <div class="ibox-content">
					                <input type="hidden" name="id" id="id">
					                <div class="row">
					                    
					                        <div class="form-group">
					                            <label class="col-sm-3 control-label"><span style="color:red">* </span>周期名称：</label>
					                            <div class="col-sm-7">
					                            <input class="form-control" type="text" name="name" id="name" autocomplete="off">
					                            </div>
					                        </div>
					               
					                    </div>
					                    <div class="row">
					                   
					                        <div class="form-group">
					                            <label class="col-sm-3 control-label"><span style="color:red">* </span>司机初始分数：</label>
					                            <div class="col-sm-7">
					                            <input class="form-control" type="text" name="initScorer"
													   id="initScorer" autocomplete="off">
					                            </div>
					                        </div>
					                  
					                </div>
					                <div class="row">
					                  
					                        <div class="form-group">
					                            <label class="col-sm-3 control-label"><span style="color:red">* </span>车辆初始分数：</label>
					                           <div class="col-sm-7">
					                            <input class="form-control" type="text" name="initScorCar"
													   id="initScorCar" autocomplete="off">
					                            </div>
					                        </div>
					                    
					                   </div>
					                   <div class="row">
					                    
					                        <div class="form-group">
					                            <label class="col-sm-3 control-label"><span style="color:red">* </span>公司初始分数：</label>
					                            <div class="col-sm-7">
					                            <input class="form-control" type="text" name="initScorPany"
													   id="initScorPany" autocomplete="off">
					                            </div>
					                        </div>
					                    
					                </div>
					
					           
					        <div class="modal-footer">
					            <button type="button" class="btn btn-white"
					                    data-dismiss="modal">取消
					            </button>
					            <button type="submit" class="btn btn-primary">提交</button>
					        </div>
					        </div>
					        </div>
					    </fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>
	<script>
	//加载footable的功能
		$(document).ready(function() {
		$('#table1').DataTable({
			"pagingType" : "full_numbers"
		});
	});
	
		$(document).ready(function () {
	        $('#addAsscycleForm').bootstrapValidator({
	            message: '这个值无效',
	            feedbackIcons: {
	                valid: 'glyphicon glyphicon-ok',
	                invalid: 'glyphicon glyphicon-remove',
	                validating: 'glyphicon glyphicon-refresh'
	            },
	            /*生效规则：字段值一旦变化就触发验证*/
	            live: 'enabled',
	            /*当表单验证不通过时，该按钮为disabled*/
	            submitButtons: 'button[type="submit"]',
	            fields: {
	                name: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写周期名称"
	                        },
	                    }
	                },
	                initScorer: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写司机初始分数"
	                        },
	
	                        between: {
	                            min: 0,
	                            max: 100,
	                            message: '请输入0-100之间的整数'
	                        },
	                        numeric : {
								message : '请输入数字'
							}
	                    }
	                }, initScorCar: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写车辆初始分数"
	                        },
	
	                        between: {
	                            min: 0,
	                            max: 100,
	                            message: '请输入0-100之间的整数'
	                        },
	                        numeric : {
								message : '请输入数字'
							}
	                    }
	                }, initScorPany: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写公司初始分数"
	                        },
	
	                        between: {
	                            min: 0,
	                            max: 100,
	                            message: '请输入0-100之间的整数'
	                        },
	                        numeric : {
								message : '请输入数字'
							}
	                    }
	                }
	            }
	        });
	    });
	
	$(document).ready(function () {
	        $('#updateAsscycleForm').bootstrapValidator({
	            message: '这个值无效',
	            feedbackIcons: {
	                valid: 'glyphicon glyphicon-ok',
	                invalid: 'glyphicon glyphicon-remove',
	                validating: 'glyphicon glyphicon-refresh'
	            },
	            /*生效规则：字段值一旦变化就触发验证*/
	            live: 'enabled',
	            /*当表单验证不通过时，该按钮为disabled*/
	            submitButtons: 'button[type="submit"]',
	            fields: {
	                name: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写周期名称"
	                        },
	                    }
	                },
	                initScorer: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写司机初始分数"
	                        },
	
	                        between: {
	                            min: 0,
	                            max: 100,
	                            message: '请输入0-100之间的整数'
	                        },
	                        numeric : {
								message : '请输入数字'
							}
	                    }
	                }, initScorCar: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写车辆初始分数"
	                        },
	
	                        between: {
	                            min: 0,
	                            max: 100,
	                            message: '请输入0-100之间的整数'
	                        },
	                        numeric : {
								message : '请输入数字'
							}
	                    }
	                }, initScorPany: {
	                    validators: {
	                        notEmpty: {
	                            message: "请填写公司初始分数"
	                        },
	
	                        between: {
	                            min: 0,
	                            max: 100,
	                            message: '请输入0-100之间的整数'
	                        },
	                        numeric : {
								message : '请输入数字'
							}
	                    }
	                }
	            }
	        });
	    });
	
		//点击修改按钮
		function alterScore(id, name, initScorer, lower, toper, initScorCar,
				lowCar, topCar, initScorPany, lowPany, topPany) {
			$("#id").val(id);
			$("#name").val(name);
			$("#initScorer").val(initScorer);
			$("#lower").val(lower);
			$("#toper").val(toper);
			$("#initScorCar").val(initScorCar);
			$("#lowCar").val(lowCar);
			$("#topCar").val(topCar);
			$("#initScorPany").val(initScorPany);
			$("#lowPany").val(lowPany);
			$("#topPany").val(topPany);
		};

		function getVal(state) {
			var id = $("#complaintId").val();
			var apprOpinion = $("#apprOpinion").val();
			deal(id, state, apprOpinion);
		}

/* 		function delect(id) {
			$("#" + id).hide();
			var args = {
				"id" : id,
			};
			var url = "deleteAsscycle";
			$.post(url, args, function(data) {
				var a = String(data);
				if (a == "操作执行成功") {
					alert(data);
				} else {
					alert(data);
					$("#" + id).show();
				}
			});
		} */
		function delect(id){
			$("#"+id).hide();
			var args = {
					"id":id,
				};
			var url="deleteAsscycle";
			$.post(url, args,function(data){
				var a = String(data);
				if (a == "删除成功") {
					swal({
		                title: a,
		                type: "success"
		            },function(){
		            	window.location.reload();
		            });
				} else {
					swal({
		                title: a,
		                type: "error"
		            });
					$("#" + id).show();
				}
			});
		}	
		
		/* function delete1(id, name) {
			swal({
				title : "",
				text : "您确定删除 " + name + " 的数据吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
				$("#deleteId").val(id);
				//提交表单
				$("#deleteForm").submit();
			})
		} */
		//点击删除按钮
		function submit(id) {
			swal({
				title : "您确定执行该操作吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			},function() {
		  	// delect(id);
				$("#deleteId").val(id);
				//提交表单
				$("#deleteForm").submit();
			});
		}
		//该函数没有用到
		function sub(id, state) {
			swal({
				title : "您确定执行该操作吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
			//	deal(id, state, null);
				$("#deleteId").val(id);
				//提交表单
				$("#deleteForm").submit();
			});
		}
		
		//页面加载完成后执行,获取执行状态
		$(document).ready(function(){
		var result='<%=request.getAttribute("result")%>';
		if(result != null && result != "null"){
			if (result.indexOf("成功")>0) {
			swal({
				title : result,
				type : "success"
			});
		}else{
			swal({
				title :result,
				type : "warning"
			});
		}
		}

	});
	</script>
</body>

</html>