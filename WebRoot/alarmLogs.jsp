<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
    <meta name="description"
          content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">
    <script src="js/jquery.js"></script>
    <link href="css/plugins/footable/footable.core.css" rel="stylesheet">
    <link rel="shortcut icon" href="favicon.ico">
    <link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <!-- Data Tables -->
    <link href="css/plugins/dataTables/dataTables.bootstrap.css"
          rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/style.min.css?v=4.0. 0" rel="stylesheet">

    <script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>

    <!-- gy 添加表单验证 -->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="dist/css/bootstrapValidator.css" />
    <script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>

    <!-- bootstrap table -->
    <link
            href="${pageContext.request.contextPath}/css/plugins/bootstrap-table/bootstrap-table.min.css"
            rel="stylesheet">
    <script
            src="${pageContext.request.contextPath}/js/plugins/bootstrap-table/bootstrap-table.min.js"></script>

    <script
            src="${pageContext.request.contextPath}/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
</head>

<body class="gray-bg">
<div class="wrapper animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <!-- 日期+精确查询+导出表格开始 -->
            <!-- 	<div class="ibox float-e-margins"> -->
            <form action="getAlarmByTaxi" id="getOneForm" method="post">
                <div class="ibox-title">
                    <h5 class="text-center">查询报警记录</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                            <input placeholder="如B00001" type="text" style="display:none"  class="form-control " id="alarmType"
                                   name="taxiNum" value="${alarmType}" />
                        <div class="col-sm-3">
                            <label class="control-label">开始时间：</label>
                            <input type="text" class="form-control" name="startDate" id="startDate"
                                   value="${startDate}" class="Wdate"
                                   onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',startDate:'%y-%M-%d 00:00:00',readOnly:true})">
                        </div>
                        <div class="col-sm-3">
                            <label class="control-label">结束时间： </label>
                            <input type="text"  class="form-control " name="endDate" id="endDate"
                                   value="${endDate}" class="Wdate"
                                   onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'startDate\')}',dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true})">
                        </div>
                        <div class="col-sm-3">
                            <label id="taxiNumTotal" class="control-label">车牌号：(选填)</label>
                            <input placeholder="如B00001" type="text" class="form-control " id="taxiNum"
                                   name="taxiNum" value="${taxiNum}" />
                        </div>
                        <br>
                        <div class="col-sm-0">
                            <button id="getOneBtn" type="button" class="btn btn-primary " onclick="listAlarmLog()">查询</button>

                            <button type="button" class="btn btn-success demo1">导出</button>
                        </div>
                    </div>
                </div>
            </form>
            <form action="derivedFormOfAlarm" method="post" id="getExcelForm">
                <input type="hidden" name="startDate" id="startDate1">
                <input type="hidden" name="endDate" id="endDate1">
                <input type="hidden" name="alarmType" id="alarmType1">
                <input type="hidden" name="taxiNum" id="taxiNum1">
            </form>
        </div>

        <!--王康警日志信息展示修改 -->
        <div class="col-sm-12">
            <div class="ibox-title">
                <div>
                    <h5>&nbsp;&nbsp;&nbsp;报警信息（模糊报警类型、处理状态）</h5>
                </div>
            </div>
            <div class="ibox-content">
                <table id="table_alarm"
                       style="table-layout: fixed; background: #FFFFFF"
                       class="footable table table-stripped" data-page-size="10">
                </table>
            </div>
        </div>

        <%-- <div class="col-sm-12">
            <div class="float-e-margins">
                <div class="ibox-title">
                    <h5>报警信息</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link"> <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="height: 637px">
                    <table id="table_alarm"
                        class="table table-striped table-bordered table-hover dataTables-example">
                        <thead>
                            <tr>
                                <th>车牌号</th>
                                <th>报警类型</th>
                                <th>报警时间</th>
                                <th>处理状态</th>
                                <!-- <th>载客状态</th> -->
                                <!-- <th>是否处理</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${alarm}" var="alarm">
                                <tr>
                                    <td>${alarm.taxiNum}</td>
                                    <td>${alarm.alarmType}</td>
                                    <td>${alarm.time}</td>
                                    <td>${alarm.dealState}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="4">
                                <ul class="pagination pull-right"></ul>
                            </td>
                        </tr>
                    </tfoot>
                    </table>
                </div>
            </div>
        </div>--%>

    </div>

</div>
<script src="js/jquery.editable-select.min.js"></script>
<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
<!-- <script src="js/jquery.min.js?v=2.1.4"></script> -->
<script src="js/bootstrap.min.js?v=3.3.5"></script>
<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="js/plugins/footable/footable.all.min.js"></script>
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
<script>
    var alarmTypes= $("#alarmType").val();
    if (alarmTypes == "紧急报警") {
        $('#alarmType').val(1);
    } else if ( alarmTypes == "疲劳驾驶") {
        $('#alarmType').val(2);
    } else if (alarmTypes==  "超速报警") {
        $('#alarmType').val(3);
    } else if (alarmTypes == "GNSS模块发生故障") {
        $('#alarmType').val(4);
    } else if (alarmTypes == "GNSS天线未接或被剪断") {
        $('#alarmType').val(5);
    }else if (alarmTypes== "GNSS天线短路") {
        $('#alarmType').val(6);
    } else if (alarmTypes=="终端主电源欠压") {
        $('#alarmType').val(7);
    } else if (alarmTypes == "终端主电源掉电") {
        $('#alarmType').val(8);
    } else if (alarmTypes == "终端LED或显示器故障") {
        $('#alarmType').val(9);
    } else if (alarmTypes== "TTS模块故障") {
        $('#alarmType').val(10);
    } else if (alarmTypes == "摄像头故障") {
        $('#alarmType').val(11);
    } else if (alarmTypes == "磁盘数据错误") {
        $('#alarmType').val(12);
    } else if (alarmTypes =="未检测到磁盘") {
        $('#alarmType').val(13);
    }else if (alarmTypes== "磁盘已满") {
        $('#alarmType').val(14);
    } else if (alarmTypes== "当天累计驾驶超时") {
        $('#alarmType').val(15);
    } else if (alarmTypes == "超时停车") {
        $('#alarmType').val(16);
    } else if (alarmTypes =="进出区域") {
        $('#alarmType').val(17);
    } else if (alarmTypes == "进出路线") {
        $('#alarmType').val(18);
    } else if (alarmTypes== "路段行驶时间不足") {
        $('#alarmType').val(19);
    } else if (alarmTypes == "路线偏移报警") {
        $('#alarmType').val(20);
    } else if (alarmTypes == "车辆VSS故障") {
        $('#alarmType').val(21);
    } else if (alarmTypes == "车辆油量异常") {
        $('#alarmType').val(22);
    } else if (alarmTypes== "车辆被盗") {
        $('#alarmType').val(23);
    } else if (alarmTypes== "车辆非法点火") {
        $('#alarmType').val(24);
    } else if (alarmTypes == "车辆非法位移") {
        $('#alarmType').val(25);
    } else if (alarmTypes== "预警") {
        $('#alarmType').val(26);
    }
    $("#alarmType").change(function() {
        var alarmType=$("#alarmType").val();
        if(alarmType==2){
            $("#taxiNumTotal").hide();
            $("#taxiNum").hide();
        }else{
            $("#taxiNumTotal").show();
            $("#taxiNum").show();
        }
    });

    $(document)
        .ready(
            function() {
                //保留分页后的checkbox的值
                var checkedIds = "";
                $("input[name='chbox']")
                    .change(
                        function() {
                            var oneches = document
                                .getElementsByName("chbox");
                            for (var i = 0; i < oneches.length; i++) {
                                if (oneches[i].checked == true) {
                                    //避免重复累计id （不含该id时进行累加）
                                    if (checkedIds
                                        .indexOf(oneches[i].value) == -1) {
                                        checkedIds = checkedIds
                                            + oneches[i].value
                                            + ",";
                                    }
                                }
                                if (oneches[i].checked == false) {
                                    //取消复选框时 含有该id时将id从全局变量中去除
                                    if (checkedIds
                                        .indexOf(oneches[i].value) != -1) {
                                        checkedIds = checkedIds
                                            .replace(
                                                (oneches[i].value + ","),
                                                "");
                                    }
                                }
                            }
                            $("#manyIsu").val(checkedIds);
                        });


                //点击"导出表格"按钮
                $(".demo1").click(
                    function() {
                        var startDate = $("#startDate").val();
                        var endDate = $("#endDate").val();
                        var nowDate = $("#endDate").val();//页面显示的结束时间
                        var taxiNum = $("#taxiNum").val();
                        var alarmType = $("#alarmType").val();
                        if (nowDate == "" || nowDate == null) {
                            nowDate = "现在时刻";
                        }
                        if (startDate == "") {
                            swal({
                                title : "导出表格时，开始时间不能为空",
                                type : "warning"
                            });
                        } else {
                            swal({
                                title : "",
                                text : "您确定导出\n" + startDate
                                    + " 到 " + nowDate
                                    + " 该期间的数据吗?",
                                type : "warning",
                                showCancelButton : true,
                                cancelButtonText : "取消"
                            }, function() {
                                //赋值
                                $("#startDate1").val(startDate);
                                $("#endDate1").val(endDate);
                                $("#taxiNum1").val(taxiNum);
                                $("#alarmType1").val(alarmType);
                                //提交表单
                                $("#getExcelForm").submit();
                            });
                        }
                    });
                listAlarmLog();
            });

    function fnClickAddRow() {
        $("#editable").dataTable()
            .fnAddData(
                [ "Custom row", "New row", "New row", "New row",
                    "New row" ]);
    };
</script>

<!-- 王康报警日志为后台分页 -->
<script type="text/javascript">
    var tables;
    //请求服务数据时所传参数
    function queryParams(params) {
        return {
            alarmType : $('#alarmType').val(),
            startDate : $('#startDate').val(),
            endDate : $('#endDate').val(),
            taxiNum : $('#taxiNum').val(),
            sort : params.sort,
            order : params.order,
            limit : params.limit, // 每页显示数量
            offset : params.offset, // SQL语句偏移量
            search : params.search
        };
    }
    //请求服务数据时所传参数
    function queryParams2(params) {
        return {
            alarmType : $('#alarmType').val(),
            startDate : $('#startDate').val(),
            endDate : $('#endDate').val(),
            sort : params.sort,
            order : params.order,
            limit : params.limit, // 每页显示数量
            offset : params.offset, // SQL语句偏移量
            search : params.search
        };
    }

    //添加、修改异步提交地址
    function listAlarmLog(){
        var startTime = $("#startDate").val();
        var endTime = $("#endDate").val();
        //var taxiNum = $("#taxiNum").val();
        var alarmType = $("#alarmType").val();
        var endYear = null;
        //开始时间的年份
        var startYear = startTime.substr(0, 4);
        //结束时间的年份
        if (endTime == "") {
            endYear = new Date().getFullYear();
        } else {
            endYear = endTime.substr(0, 4);
        }

        if (startTime == "") {
            swal({
                title : "开始时间不能为空",
                type : "warning"
            });
        } else if (endTime== ""){
            swal({
                title : "结束时间不能为空",
                type : "warning"
            });
        } else if (endYear != startYear) {
            swal({
                title : "不支持跨年查询，请重新选择开始时间或结束时间",
                type : "warning"
            });
        } else if(alarmType==2){
            $("#lable").show();
            //表单提交
            //先销毁表格再填充数据，避免第二次查询无法显示
            $("#table_alarm").bootstrapTable('destroy');
            //填充table数据
            $('#table_alarm').bootstrapTable({
                dataType : "json",
                //是否显示行间隔色
                striped : true,
                pagination : true, //是否分页
                pageList : [ 7, 10, 20, 30 ], //分页页数选择
                pageSize : 7, //默认每页的条数
                pageNumber : 1,
                search:true, //显示搜索框
                editable : false, //开启编辑模式
                uniqueId : 'index',
                queryParamsType : 'limit', //查询参数组织方式
                url : 'getAlarmByTaxi1',
                queryParams : queryParams2, //请求服务器时所传的参数
                sidePagination : 'server', //指定服务器端分页
                columns : [ {
                    field : 'taxiNum',
                    title : '车牌号',
                }, {
                    field : 'alarmType',
                    title : '报警类型',
                }, {
                    field : 'time',
                    title : '报警时间',
                }, {
                    field : 'dealState',
                    title : '处理状态',
                },
                ],
            });
        }else{
            $("#lable").show();
            //表单提交
            //先销毁表格再填充数据，避免第二次查询无法显示
            $("#table_alarm").bootstrapTable('destroy');
            //填充table数据
            $('#table_alarm').bootstrapTable({
                dataType : "json",
                //是否显示行间隔色
                striped : true,
                pagination : true, //是否分页
                pageList : [ 7, 10, 20, 30 ], //分页页数选择
                pageSize : 7, //默认每页的条数
                pageNumber : 1,
                search:true, //显示搜索框
                editable : false, //开启编辑模式
                uniqueId : 'index',
                queryParamsType : 'limit', //查询参数组织方式
                url : 'getAlarmByTaxi1',
                queryParams : queryParams, //请求服务器时所传的参数
                sidePagination : 'server', //指定服务器端分页
                columns : [ {
                    field : 'taxiNum',
                    title : '车牌号',
                }, {
                    field : 'alarmType',
                    title : '报警类型',
                }, {
                    field : 'time',
                    title : '报警时间',
                }, {
                    field : 'dealState',
                    title : '处理状态',
                },,
                ],
            });
        }
    }
</script>
</body>

</html>