package com.hx.gps.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Assarch entity. @author MyEclipse Persistence Tools
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "assarch" )
public class Assarch implements java.io.Serializable {

	// Fields

	private Integer id;
	private Driver driver;
	private Date archDate;//存档时间
	private String assDate;//存档对应的年月
	private String currRank;
	private Integer currScore;
	private Integer initScore;

	// Constructors

	/** default constructor */
	public Assarch() {
	}

	/** full constructor */
	public Assarch(Driver driver, Date archDate, String assDate,
			String currRank, Integer currScore, Integer initScore) {
		this.driver = driver;
		this.archDate = archDate;
		this.assDate = assDate;
		this.currRank = currRank;
		this.currScore = currScore;
		this.initScore = initScore;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "driverId")
	public Driver getDriver() {
		return this.driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "archDate", length = 10)
	public Date getArchDate() {
		return this.archDate;
	}

	public void setArchDate(Date archDate) {
		this.archDate = archDate;
	}

	@Column(name = "assDate", length = 15)
	public String getAssDate() {
		return this.assDate;
	}

	public void setAssDate(String assDate) {
		this.assDate = assDate;
	}

	@Column(name = "currRank", length = 30)
	public String getCurrRank() {
		return this.currRank;
	}

	public void setCurrRank(String currRank) {
		this.currRank = currRank;
	}

	@Column(name = "currScore")
	public Integer getCurrScore() {
		return this.currScore;
	}

	public void setCurrScore(Integer currScore) {
		this.currScore = currScore;
	}

	@Column(name = "initScore")
	public Integer getInitScore() {
		return this.initScore;
	}

	public void setInitScore(Integer initScore) {
		this.initScore = initScore;
	}

}