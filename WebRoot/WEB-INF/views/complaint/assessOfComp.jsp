<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">

<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">

					<!-- 点击添加按钮弹出来的form表单 -->
					<div class="modal inmodal fade" id="myModal5" tabindex="-1"
						role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<form action="saveUser" method="post">
									<div class="modal-body">
										<div class="ibox-title">
											<h5>新增用户信息</h5>
										</div>
										<div class="ibox-content">
											<div class="row">
												<div class="col-sm-6">
													公司名: <select name="company.id">
														<c:forEach items="${companies}" var="companies">
															<option value="${companies.id }">${companies.name }</option>
														</c:forEach>
													</select>
												</div>
												<div class="col-sm-6">
													用户名:<input type="text" name="name">
												</div>
											</div>
											<br>

											<div class="row">
												<div class="col-sm-6">
													角色名: <select name="role.id">
														<!-- <option value="">请选择角色</option> -->
														<c:forEach items="${roles}" var="roles">
															<option value="${roles.id}">${roles.name }</option>
														</c:forEach>
													</select>
												</div>
												<div class="col-sm-6"></div>
											</div>
										</div>
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-white"
											data-dismiss="modal">关闭</button>
										<button type="submit" class="btn btn-primary">保存</button>
									</div>
								</form>
							</div>
						</div>
					</div>

					<!-- 点击"修改"按钮弹出来的form表单 -->
					<div class="modal inmodal fade" id="myModal1" tabindex="-1"
						role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<form action="updateUser" method="post">
									<div class="modal-body">
										<div class="ibox-title">
											<h5>修改用户信息</h5>
										</div>
										<div class="ibox-content">
											<input type="hidden" name="id" id="id">
											<div class="row">
												<div class="col-sm-6">
													公司名: <select name="company.id">
														<!-- <option value="0">所有公司</option> -->
														<c:forEach items="${companies}" var="companies">
															<option value="${companies.id }">${companies.name }</option>
														</c:forEach>
													</select>
												</div>
												<div class="col-sm-6">
													用户名:<input type="text" name="name" id="nameUpdate">
												</div>
											</div>
											<br>

											<div class="row">
												<div class="col-sm-6">
													角色名: <select name="role.id">
														<!-- <option value="">请选择角色</option> -->
														<c:forEach items="${roles}" var="roles">
															<option value="${roles.id}">${roles.name }</option>
														</c:forEach>
													</select>
												</div>
												<div class="col-sm-6"></div>
											</div>
										</div>
									</div>

								 
								</form>
							</div>
						</div>
					</div>


		<div class="row">

			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>投诉审核表</h5>
				</div>
				<div class="ibox-content"  style="height: 780px">
			
					<table id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
						<thead>
							<tr>
								<th>公司名称</th>
								<th>车牌号</th>
								<th>司机姓名</th>
								<th>投诉类型</th>
								<th>投诉事由</th>
								<th>投诉内容</th>
								<th>上车时间</th>
								<th>下车时间</th>
								<th>上车地点</th>
								<th>下车地点</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${complaint}" var="complaint">
								<tr id="${complaint.id}">
									<td>${complaint.taxi.company.name}</td>
									<td>${complaint.taxi.taxiNum}</td>
									<td>${complaint.driver.dname}</td>
									<td>${complaint.comptype.type}</td>
									<td>${complaint.reportReason}</td>
									<td>${complaint.reportContent}</td>
									<td>${complaint.startTime}</td>
									<td>${complaint.endTime}</td>
									<td>${complaint.startPlace}</td>
									<td>${complaint.endPlace}</td>
									
									<td><button id="btn-update" type="button"
											class="btn btn-success btn-sm"
											onclick="assess('${complaint.id}',2)">通过</button>
										<button type="button" class="btn btn-danger btn-sm"
											onclick="assess('${complaint.id}',0)">不通过</button></td>
								</tr>
							</c:forEach>
						</tbody>
					<tfoot>
							<tr>
								<td colspan="11">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
				<form id="deleteForm" action="alterTypeOfAssess" method="post">
					<input type="hidden" id="deleteId" name="id">
					<input type="hidden" id="deleteState" name="state">
				</form>
			</div>
		</div>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script>
	$(document).ready(function() {
		$('#table1').DataTable({
			"pagingType" : "full_numbers"
		});
		var result='<%=request.getAttribute("result")%>';
		if(result != null && result != "null"){
			if (result.indexOf("成功")>0) {
			swal({
				title : result,
				type : "success"
			});
		}else{
			swal({
				title :result,
				type : "warning"
			});
		}
		}
	});
		function assess(id,state) {
			swal({
				title : "您确定执行该操作吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
	/* 		$("#"+id).hide();
			var args = {
					"id" : id,
					"state":state
				};
				var url="alterTypeOfAssess";
			$.post(url, args, function(data) {
				var a  = String(data);
				if(a=="操作执行成功"){
					  swal({
			                title: data,
			                type: "success"
			            },function(){
							window.location.reload();
							});
				}else{
					swal({
		                title: data,
		                type: "warning"
		            });
					 $("#"+id).show();
				} 
			}); */
				$("#deleteId").val(id);
				$("#deleteState").val(state);
				 // 提交表单
				$("#deleteForm").submit();
				})
		}
	</script>
</body>

</html>