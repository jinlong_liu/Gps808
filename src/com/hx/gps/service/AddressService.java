package com.hx.gps.service;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.hx.gps.entities.Graph;
import com.hx.gps.entities.Preinfo;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.tool.Address;
import com.hx.gps.entities.tool.Alarm;
import com.hx.gps.entities.tool.AreaAddress;
import com.hx.gps.entities.tool.Blur;
import com.hx.gps.entities.tool.ComInfo2;
import com.hx.gps.entities.tool.DatatablesQuery;
import com.hx.gps.entities.tool.DatatablesView;
import com.hx.gps.entities.tool.InfoQuery;
import com.hx.gps.entities.tool.Meter;
import com.hx.gps.entities.tool.MileageVector;
import com.hx.gps.entities.tool.MyPassVol;
import com.hx.gps.entities.tool.OnlineRate;
import com.hx.gps.entities.tool.OperateTest;
import com.hx.gps.entities.tool.Point;
import com.hx.gps.entities.tool.PointsInfo;
import com.hx.gps.entities.tool.TaxiDetails;

public interface AddressService {

	// 根据时间段查询所有地址信息
	public List<Address> findAddressByDate(String startDate, String endDate,
			String taxiNum);

	// 失物招领，查询符合条件的车辆在规定的时间段内是否在表示的范围内
	public List<Taxi> lostAndFound(Graph graph, String startDate,
			String endDate, InfoQuery infoQuery);

	// 聚集报警
	public void alarmOfGather1(int state, List<Point> points);

	// 添加预设信息
	public List<Preinfo> addPreInfo(Preinfo preinfo, Graph graph);

	// 模拟轮询功能 ,没用到。。。
	public void preInfoPolling();

	// 查詢预设信息
	public List<Preinfo> getPreInfo();
	//根据id 查询预设信息
	public Preinfo getPreInfoById(Integer id);
	// 查询所有车辆列表，(实时表中的所有记录)
	public List<Address> findAllTaxi();

	// wk新增 车辆的实时监控，查询选中车辆的地址信息  后台分页、轮询相关
	public DatatablesView<Address> monitoring1(DatatablesQuery datatablesQuery, String[] isuNums);
	
	// 车辆的实时监控，查询选中车辆的地址信息
	public List<Address> monitoring(String[] isuNums);

	public List<Address> monitor(String[] isuNums);

	//查看未处理的报警信息的条数
	public Integer pollingOfAlarm();
	//删除预设信息
	public void deletePreInfo( Preinfo preinfo );
	// 查询在线率
	public List<Integer> creenOnlin(Integer type, List<Integer> companyId);

	// 里程利用率
	public MileageVector mileageUtil(List<Integer> companyId, String startDate,
			String endDate);

	// 营收报表
	public List<ComInfo2> getRevenueReport(List<String> taxiNums,
			String startDate, String endDate);

	// 根据车牌号及时间段，查询出符合条件的计价器记录
	public List<Meter> getMetersByTaxi(List<String> taxiNums, String startDate,
			String endDate);

	// 根据条件，查看报警日志
	public List<Alarm> getAlarmByTaxi(String taxiNum, String startDate,
			String endDate, Integer dealState);
	
	// Wk后台分页，根据条件，查看报警日志
	public Blur getAlarmByTaxi1(InfoQuery infoQuery);

	// 查看未处理的报警状态
	public List<Alarm> getdealAlarm();

	// 根据车牌号，查询时间段内的计价器记录，并进行客运分析
	public List<OperateTest> flowOfAnalyze(String startDate, String endDate,
			List<String> taxiNums);

	// 客运量分析
	public List<MyPassVol> getFareFlow(String startDate, String endDate,
			String type, List<String> taxiNums);

	// 在index界面实时显示在线率,根据是否存在计价器，即是否根据终端心跳判断在线状态
	public OnlineRate getOnlineRateOf0();// 根据终端心跳判断

	public OnlineRate getOnlineRateOf1();// 根据计价器心跳判断
	//获取所有车辆的总数
	public int getOnlineAll();
	//获取当前在线车辆的数量
	public int getOnlineCurrent();
	// 显示指定车辆的详细状态

	public TaxiDetails getDetailsByisu(String isu);
	//保存下发区域的预设信息到mongo中。
	public void addPreInfo2(PointsInfo pointsInfo);
	//查询下发区域的所有预设信息  
	public List<PointsInfo> getPreInfo2();
	//删除下发区域的预设信息
	public boolean  deletePreInfo2(Integer serial);
	//根据报警时间和终端号来确定报警信息
	public AreaAddress getAreaAlarm(String isuNum, String time);

	//获取限制条件下所有的报警类型
	public List<Alarm> getAllAlarmType(Date startDate, Date endDate, String alarmType);

	//获取限制条件下所有未处理报警
	public List<Alarm> getDealAlarmByIsu(String alarmType,String isu);

	//根据终端号查车牌
	public Taxi getTaxiByIsu(String isu);
	//根据车牌查询车辆信息
	public Taxi getTaxisByTaxiNum(String taxiNum);
}
