package com.hx.gps.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import org.springframework.stereotype.Component;

@Component
// 工具类或者交换类等中立类通过该注解交给spring维护
public class DateUtil {
	public static Date dateToISODate(String dateStr) {
		// T代表后面跟着时间，Z代表UTC统一时间
		Date date = formatD(dateStr);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS'Z'");
		format.setCalendar(new GregorianCalendar(new SimpleTimeZone(0, "GMT")));
		String isoDate = format.format(date);
		try {
			return format.parse(isoDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/** 时间格式(yyyy-MM-dd HH:mm:ss) */
	public final static String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

	public static Date formatD(String dateStr) {
		return formatD(dateStr, DATE_TIME_PATTERN);
	}

	public static Date formatD(String dateStr, String format) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		Date ret = null;
		try {
			ret = simpleDateFormat.parse(dateStr);
		} catch (ParseException e) {
			//
		}
		return ret;
	}

	// date转化为字符串类型
	public static String dateToString(Date date) {
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateStr = sdf.format(date);
		return dateStr;
	}

	// 字符串类型转换成date
	public static Date stringToDate(String date) {
		Date newDate = null;
		try {
			SimpleDateFormat sim = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			newDate = sim.parse(date);
		} catch (ParseException e) {
		}
		return newDate;
	}

	// 返回当前年月
	public static String getNewDate() {
		DateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
		String dateStr = sdf.format(new Date());
		String s = dateStr.substring(2, 6);
		return s;
	}

	// 返回当前时间减去相应秒数
	public static String countDate(int m) {
		// getTime()返回的是毫秒数
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		long n = (long) new Date().getTime();
		long time = n - m * 1000;
		Date date = new Date(time);
		String timeStr = sdf.format(date);
		return timeStr;
	}

	// 判断当前时间是否在时间段内
	public static boolean timeInInterval(Timestamp startTime, Timestamp endTime) {
		// 首先转成date
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String startTime1 = sdf.format(startTime);
		String endTime1 = sdf.format(endTime);
		Date startTime2 = null;
		Date endTime2 = null;
		try {
			startTime2 = sdf.parse(startTime1);
			endTime2 = sdf.parse(endTime1);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (startTime2.before(new Date()) && endTime2.after(new Date())) {
			return true;
		} else {
			return false;
		}
	}

	// String -> Timestamp
	public static Timestamp getTimestampByStr(String s) {
		Timestamp startTime = new Timestamp(System.currentTimeMillis());
		return startTime.valueOf(s);
	}

	// Date -> Timestamp
	public static Timestamp getTimestampByDate(Date d) {

		return new Timestamp(d.getTime());
	}

	// 根据传入的参数返回下个月的开始 日期
	public static Date getLastDate(String year, int n) {
		int yy = Integer.parseInt("20" + year.substring(0, 2));
		int MM = Integer.parseInt(year.substring(2)) - n;
		if (MM == 0) {
			yy--;
		}

		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd ");
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, yy);
		calendar.set(Calendar.MONTH, MM);
		calendar.set(Calendar.DAY_OF_MONTH, 0);
		String date = format1.format(calendar.getTime()) + "23:59:59";

		return stringToDate(date);
	}

	// 计价器中，修改等待时间的格式
	public static String changeWaitTime(String s) {
		StringBuilder str = new StringBuilder();
		if (s.length() == 6) {
			str.append(s.substring(0, 2));
			str.append("时");
			str.append(s.substring(2, 4));
			str.append("分");
			str.append(s.substring(4, 6));
			str.append("秒");
		}
		return str.toString();
	}

	/**
	 * 传入Data类型日期，返回字符串类型时间（ISO8601标准时间）
	 *
	 * @param date
	 * @return
	 */
	public static Date getISO8601Timestamp(String date) {
		Date newDate = null;
		try {
			SimpleDateFormat sim = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			newDate = sim.parse(date);
		} catch (ParseException e) {
		}
		Date now = new Date(newDate.getTime() - 1000 * 60 * 60 * 8);
		return now;
	}

	/*
	 * //字符串 public static String dateToString2(Date date){ DateFormat sdf = new
	 * SimpleDateFormat("yyyy-MM-dd"); String dateStr = sdf.format(date); return
	 * dateStr; }
	 *
	 * //字符串类型转换成date,只包含年月日 public static Date stringToDate2(String date) {
	 * Date newDate = null; try { SimpleDateFormat sim=new
	 * SimpleDateFormat("yyyy-MM-dd"); newDate=sim.parse(date); } catch
	 * (ParseException e) { } return newDate; }
	 */
}
