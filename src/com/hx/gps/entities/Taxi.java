package com.hx.gps.entities;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Taxi entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "taxi" )
public class Taxi implements java.io.Serializable {

	// Fields

	private Integer id;
	private Company company;
	private String taxiNum;
	private String taxiType;
	private String taxiColor;
	private String masterName;
	private String masterIdNum;
	private String masterPhone;
	//车主地址
	//private String masterAddress;
	private String isuNum;
	private String isuPhone;
	private String taxiFrame;
	private Timestamp registerDate;
	private Timestamp operStartDate;
	private Timestamp operEndDate;
	//购置日期
	//private Timestamp purDate;
	private Integer purAmount;
	private String operationer;
	private Timestamp addDate;
	private String drivIdNum;
	private String fuelType;
	//购车日期
	//private Timestamp byCarDate;
	private String tranIdNum;
	private String tranAgency;
	private Timestamp auditDate;
	private Timestamp nextAuditDate;
	private String conter;
	private String conterIdNum;
	private String conterPhone;
	private String gpsState;
	private String company_1;
	private Set<Complaints> complaintses = new HashSet<Complaints>(0);
	private Set<Driver> drivers = new HashSet<Driver>(0);
	/*
	 * 2018年10-29，李泽添加发动机号、保险单号、保险期限[一个时间点，通过控件选择]、车辆长宽高（通过车型选择自动录入）、运营状态
	 * 
	 */
	/**
	 * 发动机号
	 */
	private String engineNum;
	/**
	 * 保险单号
	 */
	private String insuranceNum;
	/**
	 * 保险期限
	 */
	private Timestamp insuranceTime;
	/**
	 * 车辆尺寸
	 */
	private String carSize;
	/**
	 * 运营状态
	 */
	private String operationStatus;
	// Constructors
/**
 * 
 * 2.车辆管理中 添加发动机号、保险单号、保险期限[一个时间点，通过控件选择]、车辆长宽高（通过车型选择自动录入）、运营状态[下拉框]（包含运营、更新失效、更新新增）的录入。

 * 
 */
	/** default constructor */
	public Taxi() {
	}
	
	

	public Taxi(String taxiNum, String taxiType) {
		super();
		this.taxiNum = taxiNum;
		this.taxiType = taxiType;
	}

	

	/** full constructor */

	public Taxi(Integer id, Company company, String taxiNum, String taxiType, String taxiColor, String masterName,
			String masterIdNum, String masterPhone, String isuNum, String isuPhone, String taxiFrame,
			Timestamp registerDate, Timestamp operStartDate, Timestamp operEndDate, Integer purAmount,
			String operationer, Timestamp addDate, String drivIdNum, String fuelType, String tranIdNum,
			String tranAgency, Timestamp auditDate, Timestamp nextAuditDate, String conter, String conterIdNum,
			String conterPhone, String gpsState, String company_1, Set<Complaints> complaintses, Set<Driver> drivers,
			String engineNum, String insuranceNum, Timestamp insuranceTime, String carSize, String operationStatus) {
		super();
		this.id = id;
		this.company = company;
		this.taxiNum = taxiNum;
		this.taxiType = taxiType;
		this.taxiColor = taxiColor;
		this.masterName = masterName;
		this.masterIdNum = masterIdNum;
		this.masterPhone = masterPhone;
		this.isuNum = isuNum;
		this.isuPhone = isuPhone;
		this.taxiFrame = taxiFrame;
		this.registerDate = registerDate;
		this.operStartDate = operStartDate;
		this.operEndDate = operEndDate;
		this.purAmount = purAmount;
		this.operationer = operationer;
		this.addDate = addDate;
		this.drivIdNum = drivIdNum;
		this.fuelType = fuelType;
		this.tranIdNum = tranIdNum;
		this.tranAgency = tranAgency;
		this.auditDate = auditDate;
		this.nextAuditDate = nextAuditDate;
		this.conter = conter;
		this.conterIdNum = conterIdNum;
		this.conterPhone = conterPhone;
		this.gpsState = gpsState;
		this.company_1 = company_1;
		this.complaintses = complaintses;
		this.drivers = drivers;
		this.engineNum = engineNum;
		this.insuranceNum = insuranceNum;
		this.insuranceTime = insuranceTime;
		this.carSize = carSize;
		this.operationStatus = operationStatus;
	}



	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}



	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "comId")
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Column(name = "taxiNum", length = 20)
	public String getTaxiNum() {
		return this.taxiNum;
	}

	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}

	@Column(name = "taxiType", length = 20)
	public String getTaxiType() {
		return this.taxiType;
	}

	public void setTaxiType(String taxiType) {
		this.taxiType = taxiType;
	}

	@Column(name = "taxiColor", length = 20)
	public String getTaxiColor() {
		return this.taxiColor;
	}

	public void setTaxiColor(String taxiColor) {
		this.taxiColor = taxiColor;
	}

	@Column(name = "masterName", length = 10)
	public String getMasterName() {
		return this.masterName;
	}

	public void setMasterName(String masterName) {
		this.masterName = masterName;
	}

	@Column(name = "masterIdNum", length = 20)
	public String getMasterIdNum() {
		return this.masterIdNum;
	}

	public void setMasterIdNum(String masterIdNum) {
		this.masterIdNum = masterIdNum;
	}

	@Column(name = "masterPhone", length = 20)
	public String getMasterPhone() {
		return this.masterPhone;
	}

	public void setMasterPhone(String masterPhone) {
		this.masterPhone = masterPhone;
	}

	

	@Column(name = "isuNum", unique=true,length = 20)
	public String getIsuNum() {
		return this.isuNum;
	}

	public void setIsuNum(String isuNum) {
		this.isuNum = isuNum;
	}

	@Column(name = "isuPhone", length = 20)
	public String getIsuPhone() {
		return this.isuPhone;
	}

	public void setIsuPhone(String isuPhone) {
		this.isuPhone = isuPhone;
	}

	@Column(name = "taxiFrame", length = 20)
	public String getTaxiFrame() {
		return this.taxiFrame;
	}

	public void setTaxiFrame(String taxiFrame) {
		this.taxiFrame = taxiFrame;
	}

	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") 
	@Column(name = "registerDate", length = 19)
	public Timestamp getRegisterDate() {
		return this.registerDate;
	}

	public void setRegisterDate(Timestamp registerDate) {
		this.registerDate = registerDate;
	}

	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") 
	@Column(name = "operStartDate", length = 19)
	public Timestamp getOperStartDate() {
		return this.operStartDate;
	}

	public void setOperStartDate(Timestamp operStartDate) {
		this.operStartDate = operStartDate;
	}

	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") 
	@Column(name = "operEndDate", length = 19)
	public Timestamp getOperEndDate() {
		return this.operEndDate;
	}

	public void setOperEndDate(Timestamp operEndDate) {
		this.operEndDate = operEndDate;
	}




	@Column(name = "purAmount")
	public Integer getPurAmount() {
		return this.purAmount;
	}

	public void setPurAmount(Integer purAmount) {
		this.purAmount = purAmount;
	}

	@Column(name = "operationer", length = 30)
	public String getOperationer() {
		return this.operationer;
	}

	public void setOperationer(String operationer) {
		this.operationer = operationer;
	}

	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") 
	@Column(name = "addDate", length = 19)
	public Timestamp getAddDate() {
		return this.addDate;
	}

	public void setAddDate(Timestamp addDate) {
		this.addDate = addDate;
	}

	@Column(name = "drivIdNum", length = 20)
	public String getDrivIdNum() {
		return this.drivIdNum;
	}

	public void setDrivIdNum(String drivIdNum) {
		this.drivIdNum = drivIdNum;
	}

	@Column(name = "fuelType", length = 20)
	public String getFuelType() {
		return this.fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	

	@Column(name = "tranIdNum", length = 20)
	public String getTranIdNum() {
		return this.tranIdNum;
	}

	public void setTranIdNum(String tranIdNum) {
		this.tranIdNum = tranIdNum;
	}

	@Column(name = "tranAgency", length = 20)
	public String getTranAgency() {
		return this.tranAgency;
	}

	public void setTranAgency(String tranAgency) {
		this.tranAgency = tranAgency;
	}

	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") 
	@Column(name = "auditDate", length = 19)
	public Timestamp getAuditDate() {
		return this.auditDate;
	}

	public void setAuditDate(Timestamp auditDate) {
		this.auditDate = auditDate;
	}

	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") 
	@Column(name = "nextAuditDate", length = 19)
	public Timestamp getNextAuditDate() {
		return this.nextAuditDate;
	}

	public void setNextAuditDate(Timestamp nextAuditDate) {
		this.nextAuditDate = nextAuditDate;
	}

	@Column(name = "conter", length = 20)
	public String getConter() {
		return this.conter;
	}

	public void setConter(String conter) {
		this.conter = conter;
	}

	@Column(name = "conterIdNum", length = 20)
	public String getConterIdNum() {
		return this.conterIdNum;
	}

	public void setConterIdNum(String conterIdNum) {
		this.conterIdNum = conterIdNum;
	}

	@Column(name = "conterPhone", length = 20)
	public String getConterPhone() {
		return this.conterPhone;
	}

	public void setConterPhone(String conterPhone) {
		this.conterPhone = conterPhone;
	}

	@Column(name = "gpsState", length = 10)
	public String getGpsState() {
		return this.gpsState;
	}

	public void setGpsState(String gpsState) {
		this.gpsState = gpsState;
	}

	@Column(name = "company", length = 30)
	public String getCompany_1() {
		return this.company_1;
	}

	public void setCompany_1(String company_1) {
		this.company_1 = company_1;
	}

	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "taxi")
	public Set<Complaints> getComplaintses() {
		return this.complaintses;
	}

	public void setComplaintses(Set<Complaints> complaintses) {
		this.complaintses = complaintses;
	}

	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "taxi")
	public Set<Driver> getDrivers() {
		return this.drivers;
	}

	public void setDrivers(Set<Driver> drivers) {
		this.drivers = drivers;
	}



	@Override
	public String toString() {
		return "Taxi [id=" + id + ", taxiNum=" + taxiNum + ", taxiType="
				+ taxiType + "]";
	}


	@Column(name = "engineNum", length = 20)
	public String getEngineNum() {
		return engineNum;
	}



	public void setEngineNum(String engineNum) {
		this.engineNum = engineNum;
	}


	@Column(name = "insuranceNum", length = 20)
	public String getInsuranceNum() {
		return insuranceNum;
	}



	public void setInsuranceNum(String insuranceNum) {
		this.insuranceNum = insuranceNum;
	}


	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") 
	@Column(name = "insuranceTime", length = 19)
	public Timestamp getInsuranceTime() {
		return insuranceTime;
	}



	public void setInsuranceTime(Timestamp insuranceTime) {
		this.insuranceTime = insuranceTime;
	}


	@Column(name = "carSize", length = 20)
	public String getCarSize() {
		return carSize;
	}



	public void setCarSize(String carSize) {
		this.carSize = carSize;
	}



	public String getOperationStatus() {
		return operationStatus;
	}



	public void setOperationStatus(String operationStatus) {
		this.operationStatus = operationStatus;
	}
	
	
	

}