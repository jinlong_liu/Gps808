package com.hx.gps.entities.tool;

import com.hx.gps.util.DateUtil;
import com.mongodb.BasicDBObject;

public class Picture {
	
	private String taxiNum;
	private String isuNum;
	private String time;//上传的时间
	private String picAddress;
	public String getTaxiNum() {
		return taxiNum;
	}
	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}
	public String getIsuNum() {
		return isuNum;
	}
	public void setIsuNum(String isuNum) {
		this.isuNum = isuNum;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getPicAddress() {
		return picAddress;
	}
	public void setPicAddress(String picAddress) {
		this.picAddress = picAddress;
	}
	public Picture() {
		super();
	}
	public Picture(BasicDBObject finds) {
		super();
		this.isuNum = finds.getString("isuNum");
		this.time = DateUtil.dateToString(finds.getDate("time"));
		this.picAddress = finds.getString("picAddress");
	}
	@Override
	public String toString() {
		return "Picture [taxiNum=" + taxiNum + ", isuNum=" + isuNum + ", time=" + time + ", picAddress=" + picAddress
				+ "]";
	}
	
	
}
