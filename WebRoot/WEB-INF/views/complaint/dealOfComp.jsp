<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<!-- gy 添加表单验证 -->
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
<!-- <style>
#table1 td{
overflow:hidden; 
white-space:nowrap;
}
</style>
 -->
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">


<!-- 点击"修改"按钮弹出来的form表单 -->
					<div class="modal inmodal fade" id="myModal1" tabindex="-1"
						role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<form action="updateUser" class="form-horizontal"  method="post" id="updateForm">
								<fieldset>
									<div class="modal-body">
										<div class="ibox-title">
											<h5>默认处理信息</h5>
										</div>
										<div class="ibox-content">
													<input type="hidden" name="complaintId" id="complaintId">
													<input type="hidden" name="companyId" id="companyId">
													<input type="hidden" name="taxiNum" id="taxiNum">
													<input type="hidden" name="driverId" id="driverId">
											<div class="row">
											
												
													 <div class="form-group">
														 <label class="col-sm-3 control-label">司机得分:</label>
														 <div class="col-sm-7">
														 <input type="text" class="form-control" name="scorer" id="scorer">
														 </div>
													</div>
													
													</div>
													<div class="row">
													
													 <div class="form-group">
														 <label class="col-sm-3 control-label">车辆得分:</label>
														 <div class="col-sm-7">
														 <input type="text" class="form-control" name="scoreTaxi" id="scoreTaxi">
														 </div>
													</div>
												
												</div>
												<div class="row">
												
													 <div class="form-group">
														 <label class="col-sm-3 control-label">公司得分:</label>
														 <div class="col-sm-7">
														 <input type="text" class="form-control" name="scorePany" id="scorePany">
														 </div>
													</div>
												
											</div>
											<br>
											
											
									<div class="modal-footer">
										<button type="button" class="btn btn-white"
											data-dismiss="modal">关闭</button>
											<button type="button" id="saveDefaultDealBtn" class="btn btn-primary"
											data-dismiss="modal">保存</button>
										<!-- <button type="submit" class="btn btn-primary">保存</button> -->
									</div>
											</div>
										</div>
									
											

									</fieldset>
								</form>
							</div>
						</div>
					</div>

		<div class="row">

			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>投诉处理表</h5>
				</div>
				<div class="ibox-content" style="height: 780px">
					<table  id="table1"  width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
						<thead>
							<tr>
								<th>公司名称</th>
								<th>车牌号</th>
								<th>司机姓名</th>
								<th>投诉类型</th>
								<th>投诉事由</th>
								<th>投诉内容</th>
								<th>上车时间</th>
								<th>下车时间</th>
								<th>上车地点</th>
								<th>下车地点</th>
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${complaint}" var="complaint">
								<tr id="${complaint.id}">
									<td>${complaint.taxi.company.name}</td>
									<td>${complaint.taxi.taxiNum}</td>
									<td>${complaint.driver.dname}</td>
									<td>${complaint.comptype.type}</td>
									<td>${complaint.reportReason}</td>
									<td>${complaint.reportContent}</td>
									<td>${complaint.startTime}</td>
									<td>${complaint.endTime}</td>
									<td>${complaint.startPlace}</td>
									<td>${complaint.endPlace}</td>
									
									<td>
									<c:if test="${complaint.type==2}">
									<button id="btn-update" type="button"
											class="btn btn-success btn-sm" data-toggle="modal"
										data-target="#myModal1"
											onclick="alterScore('${complaint.id}','${complaint.taxi.company.id}','${complaint.taxi.taxiNum}','${complaint.driver.id}','${complaint.comptype.scorer}','${complaint.comptype.scoreTaxi}','${complaint.comptype.scorePany}')">默认打分</button>
										<button type="button" class="btn btn-danger btn-sm"
											onclick="submit('${complaint.id}','${complaint.taxi.company.id}','${complaint.taxi.taxiNum}','${complaint.driver.id}','${complaint.comptype.scorer}','${complaint.comptype.scoreTaxi}','${complaint.comptype.scorePany}')">确认处理</button>
									</c:if>
									<c:if test="${complaint.type==5}">
									 
										<a href="updateType522?id=${complaint.id}"><button type="button" class="btn btn-info btn-sm" data-toggle="tooltip"
       									 data-placement="left" title="处理驳回原因：${complaint.apprOpinion}">重新处理</button></a>
									</c:if>
											</td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="11">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
				<form id="deleteForm" action="deleteUser" method="post">
					<input type="hidden" id="deleteId" name="id">
				</form>
			</div>
		</div>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>
	<script>
    $(function () { $("[data-toggle='tooltip']").tooltip(); });
	</script>
	<script>

	$(document).ready(function() {
		
		$('#table1').DataTable({
			"pagingType" : "full_numbers"
		});
	});
	$(document).ready(function () {
        $('#updateForm').bootstrapValidator({
            message: '这个值无效',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*生效规则：字段值一旦变化就触发验证*/
            live: 'enabled',
            /*当表单验证不通过时，该按钮为disabled*/
            submitButtons: 'button[type="submit"]',
            fields: {
                 scorer: {
                  	validators: {
                          notEmpty: {
                              message: '请输入司机得分'
                          },between: {
	                            min: 0,
	                            max: 100,
	                            message: '请输入0-100之间的整数'
	                        }
                        }
                    
                  },  scoreTaxi : {
                      validators: {
                          notEmpty: {
                              message: '请输入车辆得分'
                          },between: {
	                            min: 0,
	                            max: 100,
	                            message: '请输入0-100之间的整数'
	                        }
                      }
                  },scorePany: {
                      validators: {
                          notEmpty: {
                              message: '请输入公司得分'
                          },between: {
	                            min: 0,
	                            max: 100,
	                            message: '请输入0-100之间的整数'
	                        }
                      }
                  }
          }
        });
    });

	//查看默认处理
		function alterScore(complaintId,companyId,taxiNum,driverId,scorer,scoreTaxi,scorePany) {
			var s=$("#scorer").val();
			if(s==""){
				$("#complaintId").val(complaintId);
				$("#companyId").val(companyId);
				$("#taxiNum").val(taxiNum);
				$("#driverId").val(driverId);
				$("#scorer").val(scorer);
				$("#scoreTaxi").val(scoreTaxi);
				$("#scorePany").val(scorePany);
			}
		};
  
		function submit(id,comId,taxiNum,dirId,score,scoTaxi,scoPany) {
		
			swal({
				title: "您确定执行该操作吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
			$("#"+id).hide();
			var complaintId=id;
		 //alert(id);
			var driverId=comId;
			var taxiNum=taxiNum;
			var companyId=dirId;
			var scorer=score;
			var scoreTaxi=scoTaxi;
			var scorePany=scoPany;
			var args = {
					"compId" :complaintId,
					"driverId":driverId,
					"taxiNum":taxiNum,
					"panyId":companyId,
					"scorer":scorer,
					"scoreTaxi":scoreTaxi,
					"scorePany":scorePany
				};
				var url="addDeal";
			 
			$.post(url, args, function(data) {
				var a  = String(data);
				 if(a=="操作执行成功"){
					  swal({
			                title: data,
			                type: "success"
			            },function(){
							window.location.reload();});
				}else{
					swal({
		                title: data,
		                type: "warning"
		            });
					 $("#"+id).show();
				} 
		 
			});
			})
			 
		}
	</script>
</body>

</html>