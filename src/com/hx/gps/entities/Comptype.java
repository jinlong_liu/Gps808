package com.hx.gps.entities;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Comptype entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "comptype" )
public class Comptype implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer scorePany;
	private Integer scoreTaxi;
	private Integer scorer;
	private String type;
	private Set<Complaints> complaintses = new HashSet<Complaints>(0);

	// Constructors

	/** default constructor */
	public Comptype() {
	}

	/** full constructor */
	public Comptype(Integer scorePany, Integer scoreTaxi, Integer scorer,
			String type, Set<Complaints> complaintses) {
		this.scorePany = scorePany;
		this.scoreTaxi = scoreTaxi;
		this.scorer = scorer;
		this.type = type;
		this.complaintses = complaintses;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "scorePany")
	public Integer getScorePany() {
		return this.scorePany;
	}

	public void setScorePany(Integer scorePany) {
		this.scorePany = scorePany;
	}

	@Column(name = "scoreTaxi")
	public Integer getScoreTaxi() {
		return this.scoreTaxi;
	}

	public void setScoreTaxi(Integer scoreTaxi) {
		this.scoreTaxi = scoreTaxi;
	}

	@Column(name = "scorer")
	public Integer getScorer() {
		return this.scorer;
	}

	public void setScorer(Integer scorer) {
		this.scorer = scorer;
	}

	@Column(name = "type", length = 100)
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "comptype")
	public Set<Complaints> getComplaintses() {
		return this.complaintses;
	}

	public void setComplaintses(Set<Complaints> complaintses) {
		this.complaintses = complaintses;
	}

	@Override
	public String toString() {
		return "Comptype [id=" + id + ", scorePany=" + scorePany
				+ ", scoreTaxi=" + scoreTaxi + ", scorer=" + scorer + ", type="
				+ type + "]";
	}

}