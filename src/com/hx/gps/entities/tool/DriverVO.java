package com.hx.gps.entities.tool;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

import com.hx.gps.entities.Assarch;
import com.hx.gps.entities.Company;
import com.hx.gps.entities.Complaints;
import com.hx.gps.entities.Driver;
import com.hx.gps.entities.Taxi;

public class DriverVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private Company company;
	private Taxi taxi;
	private String dname;
	private String driNum;
	private String phone;
	private String quaNum;
	private String sex;
	private boolean state;
	private Integer age; // 年龄
	private String qdType;
	private String identityCard;
	private String home;
	private String people;
	private Timestamp time;
	private String occSendIns;
	private Timestamp occStartTime;
	private Timestamp occEndTime;
	private String contractor;
	private String contId;
	private String contPhone;
	private String evaGrade;
	private String moniUnit;
	private String moniPhone;
	private String comPhone;
	private String photoAddr;

	// Constructors

	/** default constructor */
	public DriverVO() {
	}

	// @Override
	// public int compareTo(DriverVO o) {
	// if(o.getTaxi() != null && this.getTaxi() != null){
	// Integer aInteger =
	// this.getTaxi().getTaxiNum().compareTo(o.getTaxi().getTaxiNum());
	// System.out.println("排序整形结果："+aInteger);
	// return aInteger;
	// }
	// return -19021;
	// }

	public DriverVO(Driver driver) {
		this.id = driver.getId();
		this.company = driver.getCompany();
		this.taxi = driver.getTaxi();
		this.dname = driver.getDname();
		this.driNum = driver.getDriNum();
		this.phone = driver.getPhone();
		this.quaNum = driver.getQuaNum();
		this.sex = driver.getSex();
		this.age = driver.getAge();
		this.qdType = driver.getQdType();
		this.identityCard = driver.getIdentityCard();
		this.home = driver.getHome();
		this.people = driver.getPeople();
		this.time = driver.getTime();
		this.occSendIns = driver.getOccSendIns();
		this.occStartTime = driver.getOccStartTime();
		this.occEndTime = driver.getOccEndTime();
		this.contractor = driver.getContractor();
		this.contId = driver.getContId();
		this.contPhone = driver.getContPhone();
		this.evaGrade = driver.getEvaGrade();
		this.moniUnit = driver.getMoniUnit();
		this.moniPhone = driver.getMoniPhone();
		this.comPhone = driver.getComPhone();
		this.state=driver.isState();
		this.photoAddr = driver.getPhotoAddr();
	}

	/** full constructor */
	public DriverVO(Company company, Taxi taxi, String dname, String driNum, String phone, String quaNum, String sex,
			Integer age, String qdType, String identityCard, String home, String people, Timestamp time,
			String occSendIns, Timestamp occStartTime, Timestamp occEndTime, String contractor, String contId,
			String contPhone, String evaGrade, String moniUnit, String moniPhone, String comPhone,
			Set<Assarch> assarchs, Set<Complaints> complaintses, String photoAddr) {
		this.company = company;
		this.taxi = taxi;
		this.dname = dname;
		this.driNum = driNum;
		this.phone = phone;
		this.quaNum = quaNum;
		this.sex = sex;
		this.age = age;
		this.qdType = qdType;
		this.identityCard = identityCard;
		this.home = home;
		this.people = people;
		this.time = time;
		this.occSendIns = occSendIns;
		this.occStartTime = occStartTime;
		this.occEndTime = occEndTime;
		this.contractor = contractor;
		this.contId = contId;
		this.contPhone = contPhone;
		this.evaGrade = evaGrade;
		this.moniUnit = moniUnit;
		this.moniPhone = moniPhone;
		this.comPhone = comPhone;
		this.photoAddr = photoAddr;
	}

	public DriverVO(Integer id, Company company, Taxi taxi, String dname, String driNum, String phone, String quaNum,
			String sex, boolean state, Integer age, String qdType, String identityCard, String home, String people,
			Timestamp time, String occSendIns, Timestamp occStartTime, Timestamp occEndTime, String contractor,
			String contId, String contPhone, String evaGrade, String moniUnit, String moniPhone, String comPhone,
			String photoAddr) {
		super();
		this.id = id;
		this.company = company;
		this.taxi = taxi;
		this.dname = dname;
		this.driNum = driNum;
		this.phone = phone;
		this.quaNum = quaNum;
		this.sex = sex;
		this.state = state;
		this.age = age;
		this.qdType = qdType;
		this.identityCard = identityCard;
		this.home = home;
		this.people = people;
		this.time = time;
		this.occSendIns = occSendIns;
		this.occStartTime = occStartTime;
		this.occEndTime = occEndTime;
		this.contractor = contractor;
		this.contId = contId;
		this.contPhone = contPhone;
		this.evaGrade = evaGrade;
		this.moniUnit = moniUnit;
		this.moniPhone = moniPhone;
		this.comPhone = comPhone;
		this.photoAddr = photoAddr;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Taxi getTaxi() {
		return this.taxi;
	}

	public void setTaxi(Taxi taxi) {
		this.taxi = taxi;
	}

	public String getDname() {
		return this.dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	public String getDriNum() {
		return this.driNum;
	}

	public void setDriNum(String driNum) {
		this.driNum = driNum;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getQuaNum() {
		return this.quaNum;
	}

	public void setQuaNum(String quaNum) {
		this.quaNum = quaNum;
	}

	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Integer getAge() {
		return this.age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getQdType() {
		return this.qdType;
	}

	public void setQdType(String qdType) {
		this.qdType = qdType;
	}

	public String getIdentityCard() {
		return this.identityCard;
	}

	public void setIdentityCard(String identityCard) {
		this.identityCard = identityCard;
	}

	public String getHome() {
		return this.home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public String getPeople() {
		return this.people;
	}

	public void setPeople(String people) {
		this.people = people;
	}

	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public String getOccSendIns() {
		return this.occSendIns;
	}

	public void setOccSendIns(String occSendIns) {
		this.occSendIns = occSendIns;
	}

	public Timestamp getOccStartTime() {
		return this.occStartTime;
	}

	public void setOccStartTime(Timestamp occStartTime) {
		this.occStartTime = occStartTime;
	}

	public Timestamp getOccEndTime() {
		return this.occEndTime;
	}

	public void setOccEndTime(Timestamp occEndTime) {
		this.occEndTime = occEndTime;
	}

	public String getContractor() {
		return this.contractor;
	}

	public void setContractor(String contractor) {
		this.contractor = contractor;
	}

	public String getContId() {
		return this.contId;
	}

	public void setContId(String contId) {
		this.contId = contId;
	}

	public String getContPhone() {
		return this.contPhone;
	}

	public void setContPhone(String contPhone) {
		this.contPhone = contPhone;
	}

	public String getEvaGrade() {
		return this.evaGrade;
	}

	public void setEvaGrade(String evaGrade) {
		this.evaGrade = evaGrade;
	}

	public String getMoniUnit() {
		return this.moniUnit;
	}

	public void setMoniUnit(String moniUnit) {
		this.moniUnit = moniUnit;
	}

	public String getMoniPhone() {
		return this.moniPhone;
	}

	public void setMoniPhone(String moniPhone) {
		this.moniPhone = moniPhone;
	}

	public String getComPhone() {
		return this.comPhone;
	}

	public void setComPhone(String comPhone) {
		this.comPhone = comPhone;
	}

	public String getPhotoAddr() {
		return photoAddr;
	}

	public void setPhotoAddr(String photoAddr) {
		this.photoAddr = photoAddr;
	}

	@Override
	public String toString() {
		return "Driver [id=" + id + ", company=" + company + ", taxi=" + taxi + ", dname=" + dname + ", driNum="
				+ driNum + ", phone=" + phone + ", quaNum=" + quaNum + ", sex=" + sex + ", age=" + age + ", qdType="
				+ qdType + ", identityCard=" + identityCard + ", home=" + home + ", people=" + people + ", time=" + time
				+ ", occSendIns=" + occSendIns + ", occStartTime=" + occStartTime + ", occEndTime=" + occEndTime
				+ ", contractor=" + contractor + ", contId=" + contId + ", contPhone=" + contPhone + ", evaGrade="
				+ evaGrade + ", moniUnit=" + moniUnit + ", moniPhone=" + moniPhone + ", comPhone=" + comPhone + ", state=" + state + "]";
	}
}
