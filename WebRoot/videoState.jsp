<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>视频设备状态</title>
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/jsTree/style.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<style>
.table {
	word-wrap: break-word;
	word-break: break-all;
}

.jstree-open>.jstree-anchor>.fa-folder:before {
	content: "\f07c"
}

.jstree-default .jstree-icon.none {
	width: 0
}

.red {
	color: #fda6a6;
}

.green {
	color: #2b6901;
	font-weight: 900;
}

.black {
	color: #000000;
}
</style>
<body class="gray-bg">
	<div class="wrapper animated fadeInRight">

		<div class="row">
			<div class="col-sm-10">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<div>
							<h5>摄像头设备实时参数展示</h5>
						</div>
					</div>
					<div class="ibox-content"
						style="overflow-x: auto; overflow-y: auto;" id="echartsShow">
						<!-- 这里显示表数据  -->
						<!-- 修改结束 -->
						<div class="form-group"></div>
						<table id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="12"
							data-filter=#filter>
							<thead>
								<tr>
									<th>车牌号</th>
									<th>接收时间</th>
									<th width="72%">数据:</th>
									<!-- <th>摄像头一</th>
									<th>摄像头三</th>
									<th>摄像头四</th>
									<th>拾音器</th>
									<th>gps状态</th>
									<th>定位</th>
									<th>温度</th>
									<th>开关</th>
									<th>供电回路</th>
									<th>Acc状态</th>
									<th>重连次数</th>
									<th>RTC</th>
									<th>RTC时间</th>
									<th>硬盘故障次数</th>
									<th>计价器</th>
									<th>lcd</th> -->
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${videoStates}" var="v">
									<tr id="${v.isuNum}">
										<td>${v.taxiNum}</td>
										<td>${v.time}</td>
										<td>${v.camera1}</td>
										<%-- <td>${v.camera2}</td>
										<td>${v.camera3}</td>
										<td>${v.camera4}</td>
										<!-- 5 -->

										<td>${v.MIC}</td>
										<td>${v.gpsState}</td>
										<td>${v.locationState}</td>
										<td>${v.currentTemp}</td>
										<td>${v.blowerON}</td>
										<!-- 5 -->
										<td>${v.elecLoop}</td>
										<td>${v.accState}</td>
										<td>${v.tryNum}</td>
										<td>${v.rtcValid}</td>
										<td>${v.rtcTime}</td>  
										<td style="word-wrap: break-word;">${v.bkpNum}</td>
										<td style="word-wrap: break-word;">${v.fareMeter}</td>
										<td style="word-wrap: break-word;">${v.lcdState}</td>--%>
									</tr>
								</c:forEach>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="3">
										<ul class="pagination pull-right"></ul>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>


					<!--结束  -->
				</div>
			</div>

			<!--提交表单  -->
			<form action="getIsuVideoState" action="post" id="form1">
				<input type="hidden" id="isuNum" name="isuNum" />
			</form>

			<!--右侧公司选择栏开始  -->
			<div class="col-sm-2">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>查询条件</h5>
					</div>
					<div class="ibox-content">
						<button type="button" class="btn btn-primary btn-sm-2"
							onclick="select2()">查询</button>
						<hr>
						<h4>请选择车辆</h4>
						<input type="text" class="form-control form-control-sm"
							id="txtIndustryArea"> <br> <input type="hidden"
							id="taxiNums">
						<div id="jstree1"
							style="overflow: auto; height: 950px; width: 250px;">
							<ul>
								<li class="jstree-open">所有公司
									<ul>
										<c:forEach items="${comInfo2s}" var="comInfo2s">
											<li id="${comInfo2s.comId}">${comInfo2s.cname }
												<ul>
													<c:forEach items="${comInfo2s.taxis}" var="taxis">
														<li id="${taxis.taxiNum}" data-jstree='{"type":"car"}'>
															${taxis.taxiNum }</li>
													</c:forEach>
												</ul>
											</li>
										</c:forEach>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	</div>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/content.min.js?v=1.0.0"></script>
	<script src="js/demo/echarts-demo2.min.js"></script>
	<script src="js/plugins/jsTree/jstree.min.js"></script>
	<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script type="text/javascript">
		//
		$(document).ready(function() {
			$('#table1').DataTable({
				"pagingType" : "full_numbers"
			});
		});
		//选择公司开始
		$(document)
				.ready(
						function() {
							$("#taxiNums").val("");

							$("#jstree1").jstree(
									{
										"core" : {
											//单选
											"multiple" : false,
											"check_callback" : true
										},
										"plugins" : [ "types", "dnd",
												"checkbox", "sort", "search",
												"unique" ],
										"types" : {
											"car" : {
												"icon" : "fa fa-car"
											},
											"default" : {
												"icon" : "fa fa-folder"
											}
										}
									});

							//获取选择节点的id
							$("#jstree1")
									.on(
											'changed.jstree',
											function(e, data) {
												r = [];
												var i, j;
												for (
														i = 0,
														j = data.selected.length; i < j; i++) {
													var node = data.instance
															.get_node(data.selected[i]);
													if (data.instance
															.is_leaf(node)) {
														r.push(node.id);
													}
												}
												$("#taxiNums").val(r);
											})
									.on(
											"search.jstree",
											function(e, data) {
												if (data.nodes.length) {
													var matchingNodes = data.nodes; // change
													$(this)
															.find(
																	".jstree-node")
															.hide()
															.filter(
																	'.jstree-last')
															.filter(
																	function() {
																		return this.nextSibling;
																	})
															.removeClass(
																	'jstree-last');
													data.nodes
															.parentsUntil(
																	".jstree")
															.addBack()
															.show()
															.filter(
																	".jstree-children")
															.each(
																	function() {
																		$(this)
																				.children(
																						".jstree-node:visible")
																				.eq(
																						-1)
																				.addClass(
																						"jstree-last");
																	});
													// nodes need to be in an expanded state
													matchingNodes.find(
															".jstree-node")
															.show(); // change
												}
											})
									.on(
											"clear_search.jstree",
											function(e, data) {
												if (data.nodes.length) {
													$(this)
															.find(
																	".jstree-node")
															.css("display", "")
															.filter(
																	'.jstree-last')
															.filter(
																	function() {
																		return this.nextSibling;
																	})
															.removeClass(
																	'jstree-last');
												}
											});

							$("#txtIndustryArea").keyup(function() {
								var v = $("#txtIndustryArea").val();
								$("#jstree1").jstree(true).search(v);
							});

						});
		//查询之前判断
		function select2() {
			//获取选择的车辆
			var taxis = $("#taxiNums").val();
			console.log("当前选中车辆" + taxis);
			//判断
			if (taxis == "") {
				swal({
					title : "请先选择车辆",
					type : "warning"
				});
				return;
			} else if (taxis.length == 1) {
				swal({
					title : "该公司暂无车辆或车牌号格式错误",
					type : "warning"
				});
				return;

			} else if (taxis.length < 7) {
				swal({
					title : "车牌号长度有误",
					type : "warning"
				});
				return;
			} else {
				//填充车牌号
				$("#isuNum").val(taxis);
				console.log("填充数据" + taxis);
				//提交表单
				$("#form1").submit();

			}

			//返回数据
		}
	</script>
</body>

</html>