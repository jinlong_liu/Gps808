<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<!-- gy 添加表单验证 -->
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />
<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">


		<div class="row">

			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>投诉档案</h5>
				</div>
				<div class="ibox-content" style="height: 780px">
					<table  id="table1"  width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
						<thead>
							<tr>
								<th>公司名称</th>
								<th>车牌号</th>
								<th>司机姓名</th>
								<th>投诉类型</th>
								<th>投诉事由</th>
								<th>投诉内容</th>
								<th>上车时间</th>
								<th>下车时间</th>
								<th>上车地点</th>
								<th>下车地点</th>
								<th>状态</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${complaints}" var="c">
								<tr id="${c.id}">
									<td>${c.taxi.company.name}</td>
									<td>${c.taxi.taxiNum}</td>
									<td>${c.driver.dname}</td>
									<td>${c.comptype.type}</td>
									<td>${c.reportReason}</td>
									<td>${c.reportContent}</td>
									<td>${c.startTime}</td>
									<td>${c.endTime}</td>
									<td>${c.startPlace}</td>
									<td>${c.endPlace}</td>
									<c:if test="${c.type==0}"><td>审核不通过</td></c:if>
									<c:if test="${c.type==1}"><td>待审核</td></c:if>
									<c:if test="${c.type==2}"><td>待处理</td></c:if>
									<c:if test="${c.type==3}"><td>待审批</td></c:if>
									<c:if test="${c.type==4}"><td>已审批</td></c:if>
									<c:if test="${c.type==5}"><td>待重审</td></c:if>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="11">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
				 
			</div>
		</div>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>
	<script>
	$(document).ready(function() {
		$('#table1').DataTable({
			"pagingType" : "full_numbers"
		});
	});
	</script>
</body>

</html>