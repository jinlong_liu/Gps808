<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>H+ 后台主题UI框架 - 基本表单</title>
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link rel="shortcut icon" href="favicon.ico">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- <base target="_blank"> -->

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>计价器下行透传</h5>
				</div>

				<div class="ibox-content" style="height: 770px">

					<form class="form-horizontal" action="saveDataTrans" method="post"
						id="getOneForm">
						<div class="form-group">
							<label class="col-sm-3 control-label">选中的车牌号：</label>
							<div class="col-sm-8">
								<textarea class="form-control" type="text" name="taxiNums"
									style="resize: none;" id="taxiNums">${taxiNum}</textarea>
								<%-- <span >${taxiNum}</span> --%>
								<%-- value="<%=request.getParameter("taxiNum")%>" --%>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">请输入命令字：</label>
							<div class="col-sm-8">
								<input class="form-control" type="text" name="orderType">

							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">文本输入提示：</label>
							<div class="col-sm-8">
								<font color="red">请输入下行透传信息</font>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">数据：</label>
							<div class="col-sm-8">
								<textarea style="height: 250px; resize: none;" name="context"
									id="context" class="form-control" required=""
									aria-required="true" style="overflow-y:scroll"></textarea>
							</div>
						</div>
					</form>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-4">

							<button class="btn  btn-primary" type="text" id="getOneBtn">发&nbsp;&nbsp;&nbsp;送</button>
							&nbsp;&nbsp;
							
							<!-- <a href="getTreeOfTemp"><button type="button"
									class="btn">返&nbsp;&nbsp;&nbsp;回</button></a>  -->

							<!-- <a href="getTreeOfTemp"><button type="button">返回</button></a> -->
						</div>
					</div>
					<br>
				</div>
			</div>
		</div>
	</div>

	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/content.min.js?v=1.0.0"></script>
	<script>
		//点击"发送"按钮
		$("#getOneBtn").click(function() {
			var taxiNums = $("#taxiNums").val();
			var context = $("#context").val();
			if (taxiNums == "") {
				swal({
					title : "没有选中任何车辆,请点击'返回'按钮返回",
					type : "warning"
				});
			} else if (context == "") {
				swal({
					title : "请输入透传内容",
					type : "warning"
				});
			} else {
				$("#getOneForm").submit();
			}
		});
	</script>
</body>

</html>