<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title> </title>
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<link rel="shortcut icon" href="favicon.ico">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="css/plugins/jsTree/style.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">

<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">


<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">



<base target="_blank">

<style>
.jstree-open>.jstree-anchor>.fa-folder:before {
	content: "\f07c"
}

.jstree-default .jstree-icon.none {
	width: 0
}
</style>

</head>

<body class="gray-bg">

<!-- <div class="ibox-title"> -->
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
		<div class="col-sm-3">
		<div class="ibox-title">
		<h5> 在线详情</h5>
		</div>
					<div  class="ibox-content" style=" overflow: auto; height: 782px;">
					<table class="footable table table-stripped" data-page-size="3"
							data-filter=#filter style="cursor: pointer;">
							<thead>
								<tr>
									<th>车牌号</th>
									<th>在线状态</th>
								</tr>
							</thead>
							<tbody id="tbody" >

							</tbody>
							<tfoot>
								<tr>
									<td colspan="12">
										<ul class="pagination pull-right"></ul>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			<div class="col-sm-6" style="padding:1px 1px">
				<div class="ibox float-e-margins">
					<div class="ibox-title" style="height:60px;line-height:30px">
						 
					</div>
					 <div  class="ibox-content">
					 <div class="form-group">
							<div class="col-sm-2">查看在线率</div>
							<div class="col-sm-4">选择要查看的在线率种类：</div>
							<div class="col-sm-3">
								<select name="type" id="type" class="form-control">
									<option value="0">查看实时在线率</option>
									<option value="1">查看2小时在线率</option>
									<option value="2">查看24小时在线率</option>
									<option value="3">查看48小时在线率</option>
								</select>
							</div>
							<div class="col-sm-1"><input id="getOneBtn" type="button" class="btn btn-primary btn-sm-2" value="查看" onclick="click()"></div>
						</div>
						<div class="echarts"  style="height:719px" id="echarts-pie-chart"></div>
					</div>
				</div>
				
			</div>
			
			<!--右侧公司选择栏开始  -->
			<div class="col-sm-3">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
					<h5>公司选择</h5>
					<br>
					</div>
					<div class="ibox-content">
						<input type="hidden" id="show1">
						<div id="jstree1" class="trdemo"
							style=" overflow-x: auto; overflow-y: auto; height: 745px;">
						
							<ul>
								<li class="jstree-open">所有公司
									<ul>
										<c:forEach items="${comInfo2s}" var="comInfo2s">
											<li id="${comInfo2s.comId}">${comInfo2s.cname }<%-- <ul>
													<c:forEach items="${comInfo2s.taxis}" var="taxis">
														<li data-jstree='{"type":"car"}'>
															${taxis.taxiNum }
														</li>
													</c:forEach>
												</ul> --%>
											</li>
										</c:forEach>
									</ul>
								</li>
							</ul>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 
	</div>
	<div class="ibox-content"></div> -->
	<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	
	<script src="js/plugins/echarts/echarts-all.js"></script>
	<script src="js/content.min.js?v=1.0.0"></script>
	<script src="js/demo/echarts-demo2.min.js"></script>
	<script src="js/plugins/jsTree/jstree.min.js"></script>
	
	<script type="text/javascript"
		src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>	
	<script type="text/javascript">
	var detailURL;
	function getOnlineOfflineDetail(){	
		var type=$("#type").val();
		var companys=$("#show1").val();
		//alert(type+companys);
	
		if(companys!=""){
				var args = {
				"type" : type,
				"companys" : companys
			};
			var url = detailURL;
			$.post(url, args, function(datas){
				if(datas.length>0){
					var trStr = ""
					var d= datas;
					for (var i = 0; i < d.length; i++) {
						trStr += "<tr onclick=trclick(this)>"
							+ "<td>"
							+ d[i].taxiNum
							+ "</td>"
						
							+ "<td>"
							+ d[i].state
							+ "</td>"
							+ "</tr>";
					}
					//显示实时监控列表
					console.log(trStr)	
					$("#tbody").html(trStr)
				}else{
					$("#tbody").html("")
					alert("数据为空")
				}
			});
		}
	}

	$("#getOneBtn").click(function() {
		$("#tbody").html("")
			var companys=$("#show1").val();
				if(companys==""){
				swal({
					title :"请先选择公司",
					type : "warning"
					});
					} else {
									check();
								}
			});

		//开始查询数据
		function check(){
			var type=$("#type").val();
			var companys=$("#show1").val();
			//alert(type+companys);
		
			if(companys!=""){
					var args = {
					"type" : type,
					"companys" : companys,
				};
			//获取上线率
				var url = "getOnlineState";
				$.post(url, args, function(datas){
					var newdate=String(datas);
					var newdate2=newdate.substring(1,newdate.length-1);
					var str=newdate2.split(",");
					var total=str[0];
					var online=str[1];
					var noonline=total-online;
					//alert(noonline+" , "+online);
					graph(noonline,online);
				});
			}
			
			
		}

    	function graph(noonline,online){
    		var l = echarts.init(document.getElementById("echarts-pie-chart")),
		u = {
			title: {
				text: "",
				/* subtext: "纯属虚构", */
				x: "center"
			}, 
			tooltip: {
				trigger: "item",
				formatter: "{a} <br/>{b} : {c} ({d}%)"
			},
			legend: {
				orient: "vertical",
				x: "left",
				data: ["在线", "不在线"],
				 textStyle: {
			           fontSize: 20 // 用 legend.textStyle.fontSize 更改示例大小
			        },
				itemWidth: 50,             // 图例图形宽度
			    itemHeight: 40,
			        
				
			},
			calculable: !0,
			series: [{
				name: "访问来源：数据库",
				type: "pie",
				radius: "60%",
				center: ["50%", "40%"],
				data: [
				{
					value: noonline,
					name: "不在线",
				       itemStyle:{
			                 normal:{ 
			                            label:{
			                            	textStyle:{
			                            		fontSize:18
			                            	},
			                                 show: function(){
			                                    //console.log(value);
			                                     if(noonline == 0.00){
			                                         return false;
			                                     }
			                                 }()
			                              }, 
			                              labelLine :{
			                                   show:function(){
			                                     if(noonline == 0.00) {
			                                         return false; 
			                                     }
			                                }()
			                              } 
			                        } 
			            }
				},
				{
					value: online,
					name: "在线",
				       itemStyle:{
			                 normal:{ 
			                            label:{ 
				                            	textStyle:{
				                            		fontSize:18
				                            	},
			                                 show: function(){
			                                    //console.log(value);
			                                     if(online == 0.00){
			                                         return false;
			                                     }
			                                 }()
			                              }, 
			                              labelLine :{
			                                   show:function(){
			                                     if(online == 0.00) {
			                                         return false; 
			                                     }
			                                }()
			                              } 
			                        } 
			            }
				}]
			}]
		};
		l.setOption(u),
		l.on('click', function (params) {
		    if(params.name=="不在线"){
		    	detailURL="getOfflineDetail";
		    	getOnlineOfflineDetail();
		    }else if(params.name=="在线"){
		    	detailURL="getOnlineDetail";
		    	getOnlineOfflineDetail();
		    }
		});
		$(window).resize(l.resize);
    	}
  
  //选择公司开始
		$(document).ready(function() {
		
			$("#show1").val("");
			
			$("#jstree1").jstree({
				"core" : {
					"check_callback" : true
				},
				"plugins" : [ "types", "dnd", "checkbox" ],
				"types" : {
					"car" : {
						"icon" : "fa fa-car"
					},
					"default" : {
						"icon" : "fa fa-folder"
					}
				}
			})
			
			//获取选择节点的id
			$("#jstree1").on('changed.jstree', function(e, data) {
				r = [];
				var i, j;
				for (i = 0, j = data.selected.length; i < j; i++) {
					var node = data.instance.get_node(data.selected[i]);
					if (data.instance.is_leaf(node)) {
						r.push(node.id);
					}
				}
				$("#show1").val(r);
				//alert(r);  
			})
			//
			/* .on('changed.jstree', function(e, data){
						    $("#show1").val(data.instance.get_node(data.selected[0]).text);
							//alert ();
						}); */

		});
	</script>
</body>

</html>