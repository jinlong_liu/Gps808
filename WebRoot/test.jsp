<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>视频监控页面</title>
<meta charset="utf-8">
<!--设置浏览器可以等比例缩放 -->
<%--<meta name="viewport" content="width=device-width, initial-scale=1.0">--%>
	<meta name="viewport" content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<!--设置主地址栏颜色  -->
<meta name="theme-color" content="#000000">
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">
<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<style>
.row {
	margin-left: -15px;
	margin-right: -15px;
}
*{
	padding:0;            /*清除内边距*/
	margin:0;              /*清除外边距*/
}
.video-window{
	float: left;
	margin-left: 1%;
	margin-bottom: 1%;
	width: 48%;
	height: 0;
	position: relative;
	padding-bottom: 28%;
	background-color: #000;
}
</style>
<link href="css/video.css" rel="stylesheet">
<base target="_blank">
</head>
<body style="margin-left: 10px;background-color: #f5f5f5" >
<div class="container-fluid" style="margin-top: 0px; height:100%;width:100%;padding-top: 0px;padding-right: 0px;padding-left: 0px;margin-left: 0px;margin-right: 0px" >
<%--	<center><h3  class="row col-md-1 "  ><svg t="1606301730348" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2303" width="16" height="16"><path d="M512 177.252481c11.289967 0 20.32194-9.596472 20.32194-20.32194 0-11.289967-9.596472-20.32194-20.32194-20.32194-11.289967 0-20.32194 9.596472-20.32194 20.32194C491.113561 168.220507 500.710033 177.252481 512 177.252481z" p-id="2304" fill="#13227a"></path><path d="M924.648291 416.599779c0-228.057332-184.590959-412.648291-412.648291-412.648291-228.057332 0-412.648291 184.590959-412.648291 412.648291 0 182.332966 117.980154 336.441014 281.684675 391.197354l-120.238148 194.187431c0 10.725469 7.902977 18.063947 18.063947 18.063947l466.275634 0c10.725469 0 18.063947-7.902977 18.063947-18.063947l-130.399118-190.800441C801.587652 759.814774 924.648291 602.884234 924.648291 416.599779zM512 108.948181c25.966924 0 47.417861 22.015436 47.417861 47.417861 0 25.966924-20.886439 47.417861-47.417861 47.417861-25.966924 0-47.417861-20.886439-47.417861-47.417861S485.468578 108.948181 512 108.948181zM512 724.251378c-125.318633 0-226.928335-101.045204-226.928335-226.928335 0-125.318633 101.045204-226.928335 226.928335-226.928335 125.318633 0 226.928335 101.045204 226.928335 226.928335C738.363837 623.206174 637.318633 724.251378 512 724.251378z" p-id="2305" fill="#13227a"></path><path d="M512 337.570011c-88.061742 0-159.753032 71.69129-159.753032 159.753032 0 88.061742 71.69129 159.753032 159.753032 159.753032 88.061742 0 159.753032-71.69129 159.753032-159.753032C671.188534 409.261301 600.061742 337.570011 512 337.570011z" p-id="2306" fill="#13227a"></path></svg>  出租车监控模块</h3></center>--%>
	<div class="row col-md-12 video-show" style="margin-top: 22px;"  >
		<div class="col-md-6 window1 video-window" alt="1" >
			<video style="position: absolute; top:0; left: 0; width: 100%; height: 100%;object-fit: fill"
				   id="my_video_1" class="video-js vjs-tech " controls poster="https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=196350403,3586493192&fm=26&gp=0.jpg"
				   preload='auto' data-setup='{}'>
				<source id="v1" src="${vedio1}" type="application/x-mpegURL">
			</video>
		</div>
		<div class="col-md-6 window2 video-window" alt="2">
			<video style="position: absolute; top: 0; left: -5px; width: 100%; height: 100%;object-fit: fill"
				   id="my_video_2" class="video-js vjs-tech" controls poster="https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=196350403,3586493192&fm=26&gp=0.jpg"
				   preload="none" data-setup='{}'>
				<source id="v2" src="${vedio2}" type="application/x-mpegURL">
			</video>
		</div>
		<div class="col-md-6 window3 video-window" alt="3">
			<video style="position: absolute; top: -5px; left: -0; width: 100%; height: 100%;object-fit: fill"
					id="my_video_3" class="video-js vjs-default-skin" controls poster="https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=196350403,3586493192&fm=26&gp=0.jpg"
					preload="auto" data-setup='{}'>
				<source id="v3" src="${vedio3}" type="application/x-mpegURL">
			</video>
		</div>
		<div class="col-md-6 window4 video-window" alt="4">
			<video
					style="position: absolute; top: -5px; left: -5px; width: 100%; height: 100%;object-fit: fill"
					id="my_video_4" class="video-js vjs-default-skin" controls poster="https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=196350403,3586493192&fm=26&gp=0.jpg"
					preload="auto" data-setup='{}'>
				<source id="v4" src="${vedio4}" type="application/x-mpegURL">
			</video>
		</div>
	</div>
<%--		<div class="row no-gutters" id="playvedio">--%>
<%--		<!-- 最左侧视频1 视频2-->--%>
<%--			<div id="vedio1" class="col-sm-6"  style="position: relative; padding-top: 45%; ">--%>
<%--				<video style="position: absolute; top:0; left: 0; width: 100%; height: 50%"--%>
<%--						id="my_video_1" class="video-js vjs-default-skin " controls--%>
<%--						preload='auto' data-setup='{}'>--%>
<%--					<source id="v1" src="${vedio1}" type="application/x-mpegURL">--%>
<%--				</video>--%>
<%--				<video style="position: absolute; top: 50%; left: 0; width: 100%; height: 50%"--%>
<%--						id="my_video_2" class="video-js vjs-default-skin" controls--%>
<%--						preload="auto" data-setup='{}'>--%>
<%--					<source id="v2" src="${vedio2}" type="application/x-mpegURL">--%>
<%--				</video>--%>
<%--			</div>--%>
<%--			&lt;%&ndash;最右侧视频 3 4&ndash;%&gt;--%>
<%--			<div id="vedio3" class="col-sm-6 player-audio player" style="position: relative; padding-top: 45%; ">--%>
<%--				<video--%>
<%--						style="position: absolute; top: 0; left: 0; width: 100%; height: 50%"--%>
<%--						id="my_video_3" class="video-js vjs-default-skin" controls--%>
<%--						preload="auto" data-setup='{}'>--%>
<%--					<source id="v3" src="${vedio3}" type="application/x-mpegURL">--%>
<%--				</video>--%>
<%--				<video--%>
<%--						style="position: absolute; top: 50%; left: 0; width: 100%; height: 50%"--%>
<%--						id="my_video_4" class="video-js vjs-default-skin" controls--%>
<%--						preload="auto" data-setup='{}'>--%>
<%--					<source id="v4" src="${vedio4}" type="application/x-mpegURL">--%>
<%--				</video>--%>
<%--			</div>--%>
<%--		</div>--%>
</div>
	<script src="js/jquery.min.js"></script>
	<script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>
	<script src="js/plugins/gritter/jquery.gritter.min.js"></script>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script src="js/plugins/jsTree/jstree.min.js"></script>
	<script src="js/video.js"></script>
	<script src="js/videojs-live.js"></script>
	<script type="text/javascript">

function reshow(){
		/*
		var url = "getIsuNumBycar";
		var args = {
		"taxiNum" : taxiNums
		};
		  $.post(url, args, function(data) {
			//后台查车辆的终端号。
		 var a=eval('('+data+')');
			 if(a!==null){
				 //后期根据车牌号拼接字符串，目前测试直接输入默认地址就可以。
			// window.location.href="http://"+序列号+".xstrive.com:8080";
				 window.location.href="http://defaultRRR.xstrive.com:8080";
			 }else{
				 swal({
						title : "车牌号格式有误",
						type : "warning"
					});
				 return;
			 }
			//return;
		}); */
		//实现跳转
		//window.location.href = "http://defaultRRR.xstrive.com:8080";
		var an = '<%=request.getAttribute("vedio0")%>';
		/*先指定回放车辆  */
		if (an == null || an == "" || an == "null") {
			alert("请先指定回放车辆");
		} else {
			// 指定父窗口跳转地址。
			var storage=window.localStorage;
			storage.setItem("url",an);
			storage.setItem("isu",an.substr(7,12));
			// window.top.href="timeTest.jsp";
			parent.open("timeTest.jsp");
			//  parent.location.href = an;
			}
		}
	</script>
</body>

</html>
