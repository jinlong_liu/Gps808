package com.hx.gps.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hx.gps.dao.SerialDao;
import com.hx.gps.dao.WaitOrderDao;
import com.hx.gps.entities.Graph;
import com.hx.gps.entities.Preinfo;
import com.hx.gps.entities.Taxi;
import com.hx.gps.entities.tool.Gps;
import com.hx.gps.entities.tool.PictureInfo;
import com.hx.gps.entities.tool.Point;
import com.hx.gps.entities.tool.PointsInfo;
import com.hx.gps.entities.tool.WaitOrder;
import com.hx.gps.service.AddressService;
import com.hx.gps.service.OrderService;
import com.hx.gps.service.TaxiService;
import com.hx.gps.util.DateUtil;
import com.hx.gps.util.PositionUtil;

@Controller
public class OrderController {

	WaitOrderDao waitOrderDao = new WaitOrderDao();
	@Autowired
	private TaxiService taxiService;
	@Autowired
	private OrderService orderService;
	// 日志记录类
	private static Log logger = LogFactory.getLog(OrderController.class);
	@Autowired
	private AddressService addressService;

	// 进入终端控制设置页面
	@RequestMapping("/enterSetControl")
	public String enterSetControl(Map<String, Object> map, String taxiNum) {
		map.put("taxiNum", taxiNum);
		return "WEB-INF/views/order/setControl";
	}
	
	// 进入文本下发页面
	@RequestMapping("/enterSendText")
	public String enterSendText(Map<String, Object> map, String taxiNum) {
		map.put("taxiNum", taxiNum);
		return "WEB-INF/views/order/sendText";
	}

	// 进入断油断电设置页面
	@RequestMapping("/enterSetRelay")
	public String enterSetRelay(Map<String, Object> map, String taxiNum) {
		map.put("taxiNum", taxiNum);
		return "WEB-INF/views/order/setRelay";
	}

	// 进入超速设置 
	@RequestMapping("/enterSetSpeed")
	public String enterSetSpeed(Map<String, Object> map, String taxiNum) {
		map.put("taxiNum", taxiNum);
		return "WEB-INF/views/order/setSpeed";
	}

	// 进入远程调价页面
	@RequestMapping("/enterSetPrice")
	public String enterSetPrice(Map<String, Object> map, String taxiNum) {
		map.put("taxiNum", taxiNum);
		return "WEB-INF/views/order/setPrice";
	}
	//进入远程锁机  
	@RequestMapping("/enterSetLock")
	public String enterSetLock(Map<String, Object> map, String taxiNum) {
		map.put("taxiNum", taxiNum);
		return "WEB-INF/views/order/setLock";
	}
	// 进入远程开关机页面
	@RequestMapping("/enterSetPower")
	public String enterSetPower(Map<String, Object> map, String taxiNum) {
		map.put("taxiNum", taxiNum);
		return "WEB-INF/views/order/setPower";
	}

	// 进入删除区域 下发界面
	@RequestMapping("/enterDeleteArea")
	public String enterDeleteArea(Map<String, Object> map, String taxiNum) {
		map.put("taxiNum", taxiNum);
		return "WEB-INF/views/order/deleteArea";
	}

	// 进入下发区域设置界面
	@RequestMapping("/enterSetArea")
	public String enterSetArea(Map<String, Object> map, String taxiNum) {
		// 填充已选择的车辆
		map.put("taxiNum", taxiNum);
		// 获取所有的预设信息
		List<PointsInfo> pointsInfos = addressService.getPreInfo2();
		if (pointsInfos.size() > 0) {
			map.put("preinfo", pointsInfos);
		}
		return "WEB-INF/views/order/setArea";
	}

	// 保存删除下发区域的命令
	@RequestMapping("/deleteArea")
	public String saveDeleArea(Map<String, Object> map, String taxiNums, String areaId, HttpServletRequest request) {
		/**
		 * 页面上传的数据有：车牌号(可能是多个组成的字符串)、文本标识
		 */
		String[] taxiNum = taxiNums.split(",");
		String result = null;

		WaitOrder w = new WaitOrder();
		w.setTime(new Date());
		w.setSerialNum(SerialDao.getSerialNum());
		w.setOrderType("删除区域");
		w.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
		w.setContext(areaId);

		List<Taxi> strList = orderService.deleArea(w, taxiNum);
		if (strList.size() == 0) {
			logger.info("下发删除区域成功" + w.toString());
			result = "发送成功";
		} else if (strList.size() == taxiNum.length) {
			logger.info("下发删除区域失败" + w.toString());
			result = "发送失败";
		} else {
			logger.info("下发删除区域发送异常" + w.toString());
			result = "部分车辆发送异常";
		}
		// System.out.println("strList"+strList);
		map.put("taxi", strList);
		map.put("taxiNum", taxiNums);// 完成回显
		request.setAttribute("result", result);
		return "WEB-INF/views/order/deleteArea2";
	}
	// 保存要下发的远程锁机  
	@RequestMapping("/saveLockStyle")
	public String saveLockStyle(Map<String, Object> map, String taxiNums, String lockStyle ,	HttpServletRequest request) {
		/**
		 * 页面上传的数据有：车牌号(可能是多个组成的字符串)、文本标识
		 */
		String[] taxiNum = taxiNums.split(",");
		String result = null;

		WaitOrder w = new WaitOrder();
		w.setTime(new Date());
		w.setSerialNum(SerialDao.getSerialNum());
		w.setOrderType("远程锁机");
		w.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
		w.setContext(lockStyle);

		List<Taxi> strList = orderService.sendLock(w, taxiNum);
		if (strList.size() == 0) {
			logger.info("下发远程锁机成功" + w.toString());
			result = "发送成功";
		} else if (strList.size() == taxiNum.length) {
			logger.info("下发远程锁机失败" + w.toString());
			result = "发送失败";
		} else {
			logger.info("远程锁机发送异常" + w.toString());
			result = "部分车辆发送异常";
		}
		map.put("taxi", strList);
		map.put("taxiNum", taxiNums);// 完成回显
		request.setAttribute("result", result);
		return "WEB-INF/views/order/setLock2";
	}
	// 保存要下发的断油断电
	@RequestMapping("/saveRelay")
	public String saveRelay(Map<String, Object> map, String taxiNums, String oilControl, String energyControl,
			HttpServletRequest request) {
		/**
		 * 页面上传的数据有：车牌号(可能是多个组成的字符串)、文本标识
		 */
		String[] taxiNum = taxiNums.split(",");
		String result = null;

		WaitOrder w = new WaitOrder();
		w.setTime(new Date());
		w.setSerialNum(SerialDao.getSerialNum());
		w.setOrderType("断油断电");
		w.setIdentify(oilControl);
		w.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
		w.setContext(energyControl);

		List<Taxi> strList = orderService.sendRelay(w, taxiNum);
		if (strList.size() == 0) {
			logger.info("下发断油断电成功" + w.toString());
			result = "发送成功";
		} else if (strList.size() == taxiNum.length) {
			logger.info("下发断油断电失败" + w.toString());
			result = "发送失败";
		} else {
			logger.info("断油断电发送异常" + w.toString());
			result = "部分车辆发送异常";
		}
		// System.out.println("strList"+strList);
		map.put("taxi", strList);
		map.put("taxiNum", taxiNums);// 完成回显
		request.setAttribute("result", result);
		return "WEB-INF/views/order/setRelay2";
	}

	// 下发 远程开关机
	@RequestMapping("/savePower")
	public String savePower(Map<String, Object> map, String taxiNums, String power, Timestamp date,
			HttpServletRequest request) {
		/**
		 * 页面上传的数据有：车牌号(可能是多个组成的字符串)、文本标识
		 */
		String[] taxiNum = taxiNums.split(",");
		String result = null;

		WaitOrder w = new WaitOrder();
		w.setTime(date);
		w.setSerialNum(SerialDao.getSerialNum());
		w.setOrderType("远程开关机");
		w.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
		w.setContext(power);

		List<Taxi> strList = orderService.sendPower(w, taxiNum);
		if (strList.size() == 0) {
			logger.info("下发远程开关机成功" + w.toString());
			result = "发送成功";
		} else if (strList.size() == taxiNum.length) {
			logger.info("下发远程开关机失败" + w.toString());
			result = "命令发送失败";
		} else {
			logger.info("远程开关机发送异常" + w.toString());
			result = "部分车辆发送异常";
		}
		// System.out.println("strList"+strList);
		map.put("taxi", strList);
		map.put("taxiNum", taxiNums);// 完成回显
		request.setAttribute("result", result);
		return "WEB-INF/views/order/setPower2";
	}

	// 下发远程调价
	@RequestMapping("/savePrice")
	public String saveRent(Map<String, Object> map, String taxiNums, String rent, String price, String km,
			HttpServletRequest request) {
		/**
		 * 页面上传的数据有：车牌号(可能是多个组成的字符串)、文本标识
		 */
		String[] taxiNum = taxiNums.split(",");
		String result = null;
		WaitOrder w = new WaitOrder();
		w.setTime(new Date());
		w.setSerialNum(SerialDao.getSerialNum());
		w.setOrderType("远程调价");
		w.setIdentify(price);
		w.setPeople(km);
		w.setContext(rent);
		List<Taxi> strList = orderService.sendRent(w, taxiNum);
		if (strList.size() == 0) {
			logger.info("下发远程调价成功" + w.toString());
			result = "发送成功";
		} else if (strList.size() == taxiNum.length) {
			logger.info("下发远程调价失败" + w.toString());
			result = "命令发送失败";
		} else {
			logger.info("远程调价发送异常" + w.toString());
			result = "部分车辆发送异常";
		}
		// System.out.println("strList"+strList);
		map.put("taxi", strList);
		map.put("taxiNum", taxiNums);// 完成回显
		request.setAttribute("result", result);
		return "WEB-INF/views/order/setPrice2";
	}

	// 下发超速设置
	@RequestMapping("/saveSpeed")
	public String saveRent(Map<String, Object> map, String taxiNums, String speed, String time,
			HttpServletRequest request) {
		/**
		 * 页面上传的数据有：车牌号(可能是多个组成的字符串)、文本标识
		 */
		String[] taxiNum = taxiNums.split(",");
		String result = null;
		WaitOrder w = new WaitOrder();
		w.setTime(new Date());
		w.setSerialNum(SerialDao.getSerialNum());
		w.setOrderType("超速设置");
		w.setIdentify(speed);
		w.setContext(time);
		List<Taxi> strList = orderService.sendSpeed(w, taxiNum);
		if (strList.size() == 0) {
			logger.info("超速参数设置成功" + w.toString());
			result = "发送成功";
		} else if (strList.size() == taxiNum.length) {
			logger.info("超速参数设置失败" + w.toString());
			result = "命令发送失败";
		} else {
			logger.info("超速参数设置异常" + w.toString());
			result = "部分车辆发送异常";
		}
		map.put("taxi", strList);
		map.put("taxiNum", taxiNums);// 完成回显
		request.setAttribute("result", result);
		return "WEB-INF/views/order/setSpeed2";
	}

	// 下发区域，100个点
	@RequestMapping("/saveArea2")
	public String SaveArea2(Map<String, Object> map, String taxiNums, Integer serial) {
		/**
		 * 页面上传的数据有：车牌号(可能是多个组成的字符串)、文本标识
		 */
		String[] taxiNum = taxiNums.split(",");
		String result = null;
		int ser = serial;
		List<Taxi> strList = new ArrayList<>();
		WaitOrder w = new WaitOrder();
		w.setSerialNum(SerialDao.getSerialNum());
		w.setTime(new Date());
		w.setOrderType("区域下发");
		// 记录区域id.
		w.setIdentify(String.valueOf(serial));
		w.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
		List<PointsInfo> pointsInfos = addressService.getPreInfo2();
		if (pointsInfos.size() > 0) {
			System.out.println("开始下发区域");
			for (PointsInfo pointsInfo : pointsInfos) {
				// 得到指定的编号的预设信息
				if (pointsInfo.getSerial() == ser) {
					System.out.println("期望serial" + ser + "当前serial" + pointsInfo.getSerial());
					// 进行数据的拼接和下发
					StringBuffer context = new StringBuffer();
					// 先添加总的点数
					context.append(pointsInfo.getPointsNum());
					List<Point> points = pointsInfo.getPoint();
					// 遍历点的总数
					for (Point point : points) {
						Gps gps = PositionUtil.bd09_To_Gps84(point.getLatitude(), point.getLongitude());
						// 循环遍历输出到拼接字符串。
						context.append(",");
						context.append(gps.getWgLon() + "," + gps.getWgLat());
					}
					// 记录到命令中
					w.setContext(context.toString());
					System.out.println("下发区域内容:" + context.toString());
				}
			}
			// 没有获取到数据
			if (w.getContext() == null || w.getContext().equals("")) {
				result = "指定预设信息没有点信息";
			} else {
				strList = orderService.sendArea(w, taxiNum);
				if (strList.size() == 0) {
					logger.info("下发区域成功" + w.toString());
					result = "发送成功";
				} else if (strList.size() == taxiNum.length) {
					logger.info("下发区域失败" + w.toString());
					result = "发送失败";
				} else if (w.getContext() == null) {
					logger.info("下发区域失败" + w.toString());
					result = "区域设置有误";

				} else {
					logger.info("下发区域部分车辆发送异常" + w.toString());
					result = "部分车辆发送异常";
				}
			}
		} else {
			result = "未查询到相关记录";
		}
		// 有结果直接在if中返回，否则走不到这一步 ，返回下发失败，未查询到记录。
		// context中设置点的总数和每个点的经纬度。 通过preinfo获取
		map.put("taxi", strList);
		map.put("taxiNum", taxiNums);// 完成回显
		map.put("result", result);
		return "WEB-INF/views/order/setArea2";
	}

	// 保存要下发的区域，六个点
	@RequestMapping("/saveArea")
	public String SaveArea(Map<String, Object> map, String taxiNums, Integer areaId) {
		/**
		 * 页面上传的数据有：车牌号(可能是多个组成的字符串)、文本标识
		 */
		String[] taxiNum = taxiNums.split(",");
		String result = null;

		WaitOrder w = new WaitOrder();
		w.setSerialNum(SerialDao.getSerialNum());
		w.setTime(new Date());
		w.setOrderType("区域下发");
		// 记录区域id.
		w.setIdentify(String.valueOf(areaId));
		w.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
		Preinfo preinfo = addressService.getPreInfoById(areaId);
		// context中设置点的总数和每个点的经纬度。 通过preinfo获取
		if (preinfo != null) {
			Graph graph = preinfo.getGraph();
			StringBuffer context = new StringBuffer();
			// 先添加总的点数
			context.append(graph.getPointNum());
			for (int i = 0; i < graph.getPointNum(); i++) {
				// 逐一获取每个点
				context.append(",");
				if (i == 0) {
					context.append(graph.getOneP());
				} else if (i == 1) {
					context.append(graph.getTwoP());
				} else if (i == 2) {
					context.append(graph.getThreeP());
				} else if (i == 3) {
					context.append(graph.getFourP());
				} else if (i == 4) {
					context.append(graph.getFiveP());
				} else if (i == 5) {
					context.append(graph.getSixP());
				}
			}
			w.setContext(context.toString());
		}
		List<Taxi> strList = orderService.sendArea(w, taxiNum);
		if (strList.size() == 0) {
			logger.info("下发区域成功" + w.toString());
			result = "发送成功";
		} else if (strList.size() == taxiNum.length) {
			logger.info("下发区域失败" + w.toString());
			result = "发送失败";
		} else if (w.getContext() == null) {
			logger.info("下发区域失败" + w.toString());
			result = "区域设置有误";

		} else {
			logger.info("下发区域部分车辆发送异常" + w.toString());
			result = "部分车辆发送异常";
		}
		map.put("taxi", strList);
		map.put("taxiNum", taxiNums);// 完成回显
		map.put("result", result);
		return "WEB-INF/views/order/setArea2";
	}

	// 保存要下发的文本信息
	@RequestMapping("/saveText")
	public String saveText(Map<String, Object> map, String taxiNums, String identify, String context,
			HttpServletRequest request) {
		/**
		 * 页面上传的数据有：车牌号(可能是多个组成的字符串)、文本标识
		 */
		String[] taxiNum = taxiNums.split(",");
		String result = null;
		WaitOrder w = new WaitOrder();
		w.setTime(new Date());
		w.setSerialNum(SerialDao.getSerialNum());
		w.setOrderType("文本消息下发");
		w.setIdentify(identify);
		w.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
		w.setContext(context);

		List<Taxi> strList = orderService.sendText(w, taxiNum);
		if (strList.size() == 0) {
			logger.info("下发文本成功" + w.toString());
			result = "发送成功";
		} else if (strList.size() == taxiNum.length) {
			logger.info("下发文本发送失败" + w.toString());
			result = "发送失败";
		} else {
			logger.info("下发文本部分车辆发送异常" + w.toString());
			result = "部分车辆发送异常";
		}
		// System.out.println("strList"+strList);
		map.put("taxi", strList);
		map.put("taxiNum", taxiNums);// 完成回显
		request.setAttribute("result", result);
		return "WEB-INF/views/order/sendText2";
	}

	// 进入拍照命令下发页面
	@RequestMapping("/enterTakePic")
	public String enterTaxi(Map<String, Object> map, String taxiNum) {
		// System.out.println("taxiNum: " + taxiNum);
		map.put("taxiNum", taxiNum);
		return "WEB-INF/views/order/takePic";
	}

	// 进入拍照命令下发页面
	@RequestMapping("/enterTakePic2")
	public String enterTaxi2(Map<String, Object> map, String taxiNum) {
		// System.out.println("taxiNum: " + taxiNum);
		map.put("taxiNum", taxiNum);
		return "WEB-INF/views/order/takePic2";
	}

	// 保存要下发的立即拍摄命令
	@RequestMapping("/saveTakePic")
	public String saveTakePic(Map<String, Object> map, String taxiNums, PictureInfo pictureInfo) {
		String[] taxiNum = taxiNums.split(",");
		// System.out.println("saveTakePic的taxiNums长度:" + taxiNum.length);

		for (int i = 0; i < taxiNum.length; i++) {
			// 内容：通道 分辨率 图片质量 亮度 对比度 饱和度 色度
			String context = pictureInfo.getGallery() + "," + pictureInfo.getResolution() + ","
					+ pictureInfo.getPicQuality() + "," + pictureInfo.getLuminance() + "," + pictureInfo.getContrast()
					+ "," + pictureInfo.getSaturability() + "," + pictureInfo.getChroma();

			// 根据车牌号查询相应的isuNum
			Taxi taxi = taxiService.getIsuByTaxi(taxiNum[i]);

			WaitOrder w = new WaitOrder();
			w.setTime(new Date());
			w.setSerialNum(SerialDao.getSerialNum());
			w.setIsuNum(taxi.getIsuNum());
			w.setOrderType("立即拍摄");
			w.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
			w.setContext(context);
			w.setState(0);
			w.setSendTime(0);
			waitOrderDao.insert(w);
			map.put("result", "下发拍照指令成功，请前往命令日志界面查看");
			logger.info("保存立即拍摄命令" + w.toString());
		}

		map.put("taxiNum", taxiNums);
		return "WEB-INF/views/order/takePic";
	}

	// 图片保存相关？
	@RequestMapping("/getPictureByAddr")
	public void getPictureByAddr(HttpServletResponse response, HttpServletRequest request, String picAddress) {
		response.setContentType("image/*");
		FileInputStream fis = null;
		OutputStream os = null;
		try {
			fis = new FileInputStream(picAddress);
			os = response.getOutputStream();
			int count = 0;
			byte[] buffer = new byte[1024 * 8];
			while ((count = fis.read(buffer)) != -1) {
				os.write(buffer, 0, count);
				os.flush();
				logger.info("保存图片信息成功，路径：" + picAddress);
				// System.out.println("保存图片成功"+picAddress);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("保存图片信息出现异常" + e.getMessage());
		} finally {
			try {
				fis.close();
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
				logger.error("关闭图片缓存区时出现异常" + e.getMessage());
			}
		}
	}

	// 进入电话回拨界面,在报警处理界面进入
	@RequestMapping("/intoBackdail2")
	public String intoBackdail2(Map<String, Object> map, String taxiNum, HttpServletRequest request) {
		// System.out.println("taxiNum: " + taxiNum);
		map.put("taxiNum", taxiNum);
		// System.out.println(request.getHeader("REFERER"));
		// System.out.println(request.getRequestURI()+" ,
		// "+request.getContextPath()+" , "+request.getPathInfo());
		return "WEB-INF/views/order/backdial";
	}

	// 进入电话回拨界面,在主界面进入
	@RequestMapping("/intoBackdail")
	public String intoBackdail(Map<String, Object> map, String taxiNum, HttpServletRequest request) {
		// System.out.println("taxiNum: " + taxiNum);
		map.put("taxiNum", taxiNum);

		// System.out.println(request.getHeader("REFERER"));
		// System.out.println(request.getRequestURI()+" ,
		// "+request.getContextPath()+" , "+request.getPathInfo());
		return "WEB-INF/views/order/backdial";
	}

	// 保存电话回拨信息
	@RequestMapping("/savePhone")
	public String savePhone(Map<String, Object> map, String taxiNum, String identify, String context,
			HttpServletRequest request) {
		/**
		 * 页面上传的数据有：车牌号(可能是多个组成的字符串)、文本标识
		 * 
		 */
		// 根据车牌号查询相应的isuNum
		Taxi taxi = taxiService.getIsuByTaxi(taxiNum);

		WaitOrder w = new WaitOrder();
		w.setTime(new Date());
		w.setIsuNum(taxi.getIsuNum());
		w.setSerialNum(SerialDao.getSerialNum());
		w.setOrderType("电话回拨");
		w.setIdentify(identify);
		w.setPeople(SecurityUtils.getSubject().getPrincipal().toString());
		w.setContext(context);

		waitOrderDao.insert(w);
		map.put("taxiNum", taxiNum);// 完成回显
		logger.info("保存电话回拨命令" + w.toString());
		request.setAttribute("result", "发送成功，请前往命令日志页面查看");
		return "WEB-INF/views/order/backdial";
	}
}
