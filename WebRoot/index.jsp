﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<title>铜仁出租车智能服务平台</title>

<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- <link rel="shortcut icon" href="favicon.ico"> -->
<!--不知道干啥的  -->
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<!-- 左侧导航栏的样式 -->
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- 左侧导航栏隐藏按钮的样式 -->
<link href="css/animate.min.css" rel="stylesheet">
<!-- 注掉就没有跳转到主页的按钮的动画效果了 -->
<!-- <link href="css/style.min.css?v=4.0.0" rel="stylesheet"> -->
<!-- 悬停提示相关 -->
<link rel="stylesheet" type="text/css" href="css/tooltipster.css" />
<link rel="stylesheet" type="text/css" href="css/style2.css" />

<!-- <script type="text/javascript" src="js/jquery-2.0.0.min.js"></script> -->
<script type="text/javascript" src="js/jquery.tooltipster.js"></script>

<!-- Gritter -->
<link href="js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

<!--  <link href="css/animate.min.css" rel="stylesheet"> -->
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<!-- 弹出的样式 -->
<!-- 导入右侧车辆侧边栏样式-->
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/component.css" />
<!--导入界面点击js事件 -->
<script src="js/modernizr.custom.js"></script>
<style type="text/css">
.refresh {
	borderRight: 1px solid buttonhighlight;
	borderLeft: 1px solid buttonshadow;
	borderBottom: 1px solid buttonhighlight;
	borderTop: 1px solid buttonshadow;
	background-image: url(img/gy.png);
	background-position: center;
	width: 40px;
	display: block;
	float: right;
	display: block;
	borderRight: 1px solid buttonhighlight;
}

#ulright {
	list-style-image: url(img/right.png);
	paddingleft: -10px;
	border: 1px solid green;
	border-radius: 5px 0;
	padding-bottom: -10px;
}

#list1 {
	list-style-image: url(img/gy_company.png);
	font-style: normal;
	text-align: left;
}

#list1 li:hover {
	background: #53c2e0;
}

.btn19 {
	text-align: left;
	border: 1px solid grey;
	border-radius: 5px;
	font-family: 幼圆;
	font-size: 14px;
	font-weight: bold;
	color: white;
	width: 130px;
	height: 25px;
	background: url("img/btn10.png");
	background-position: left -36px;
	font-weight: bold;
}

.company_li {
	cursor: pointer;
	font-family: 微软雅黑;
	font-size: 17px;
	color: white
}
</style>
</head>
<body class="fixed-sidebar full-height-layout gray-bg"
	style="overflow: hidden">

	<div id="wrapper">
		<!--左侧导航开始-->
		<nav class="navbar-default navbar-static-side" role="navigation">
		<div class="nav-close">
			<i class="fa fa-times-circle"></i>
		</div>

		<div class="sidebar-collapse">
			<ul class="nav" id="side-menu">
				<li class="nav-header">
					<div class="dropdown profile-element col-sm-offset-2">
						<span><img alt="image" class="img-circle"
							src="img/profile_small.png"></span> <a data-toggle="dropdown"
							class="dropdown-toggle" href="#" aria-expanded="false"> <span
							class="clear"> <span class="text-muted text-xs block"><strong
									class="font-bold"><shiro:principal /></strong> <b
									class="caret"></b> </span>
						</span>
						</a>
						<ul class="dropdown-menu animated fadeInRight m-t-xs">
							<li class="divider"></li>
							<li><a href="logout">安全退出</a></li>
						</ul>
					</div>
				</li>

				<!-- <li class="active"><a href="#"> <i class="fa fa-home"></i>
						<span class="nav-label">车辆定位</span> <span class="fa arrow"></span> -->

				<!-- 车辆监控模块 class="active" 默认打开这个列表 -->
				<shiro:hasPermission name="location:mode">
					<li class="active"><a href="#"> <i
							class="fa fa fa-map-marker"></i> <span class="nav-label">车辆定位</span>
							<span class="fa arrow"></span>
					</a>
						<ul class="nav nav-second-level">
							<li><a class="J_menuItem" href="getTreeOfTemp">车辆实时监控</a></li>
<%--							<li><a class="J_menuItem" href="carMoni.jsp">车辆监控</a></li>--%>
							<li><a class="J_menuItem" href="trackBack.jsp">轨迹回放</a></li>
							<li><a class="J_menuItem" href="enterSelectCar">定点定时查车</a></li>
							<li><a class="J_menuItem" href="getPreInfo2">预设区域</a></li>
							<%-- <shiro:hasPermission name="video:admin">
 
=======
							<shiro:lacksPermission name="comuser:admin">
								<li><a class="J_menuItem" href="getPreInfo2">预设区域</a></li>
								<%-- <shiro:hasPermission name="video:admin">
>>>>>>> .r709
								<li><a class="J_menuItem" href="enterVideoState">视频设备实时状态</a></li>
							</shiro:hasPermission>
<<<<<<< .mine
							<li><a class="J_menuItem" href="getTreeOfTemp3">视频监控</a></li> --%>
							<!-- 莱芜特有 -->
							<li><a class="J_menuItem" href="enterVideoState">设备实时状态</a></li>
<%--			 				 <li><a class="J_menuItem" href="enterMeterState">计价器状态</a></li>  --%>
<%--			 				 <li><a class="J_menuItem" href="enterTerminalState">终端状态</a></li>  --%>
 
							<!-- <li><a class="J_menuItem" href="getPreInfo">平台预设信息</a></li>
							<li><a class="J_menuItem" href="http://192.168.1.10:8081/"> 视频监控</a>   -->
							<!-- <li><a class="J_menuItem" href="getPreInfo">预设信息</a></li> -->
							<!-- <li><a class="J_menuItem" href="preInfoPolling">模拟轮询</a></li> -->
							<!-- <li><a class="J_menuItem" href="tempMoni2.jsp">车辆实时监控2</a></li> -->
							<!-- <li><a class="J_menuItem" href="index_v2.html">弹出框测试</a></li> -->
						</ul></li>
				</shiro:hasPermission>
				<!-- 信息管理模块 -->
				<shiro:hasPermission name="info:mode">
					<li><a href="#"> <i class="fa fa fa-home"></i> <span
							class="nav-label">信息管理</span> <span class="fa arrow"></span>
					</a>
						<ul class="nav nav-second-level">
							<shiro:hasPermission name="user:admin">
								<li><a class="J_menuItem" href="getAllUser">用户管理</a></li>
							</shiro:hasPermission>
							<shiro:hasPermission name="role:admin">
								<li><a class="J_menuItem" href="getAllRole">角色管理</a></li>
							</shiro:hasPermission>
							<shiro:hasPermission name="compy:admin">
								<li><a class="J_menuItem" href="getAllCompany">公司管理</a></li>
							</shiro:hasPermission>
							<shiro:hasPermission name="car:admin">
								<li><a class="J_menuItem" href="enterTaxi">车辆管理</a></li>
							</shiro:hasPermission>
							<shiro:hasPermission name="isucache:admin">
								<li><a class="J_menuItem" href="enterIsuCache">车辆缓存表</a></li>
							</shiro:hasPermission>
							<shiro:hasPermission name="cartype:admin">
								<li><a class="J_menuItem" href="getAllTaxiType">车型管理</a></li>
							</shiro:hasPermission>
							<shiro:hasPermission name="staff:admin">
								<li><a class="J_menuItem" href="enterDriver">从业人员管理</a></li>
							</shiro:hasPermission>
							<shiro:hasPermission name="blacklist:admin">
								<li><a class="J_menuItem" href="enterBlacklist">黑名单</a></li>
							</shiro:hasPermission>
 
							<!-- <li><a class="J_menuItem" href="enterAccident">安全事故</a></li> -->
							<li><a class="J_menuItem" href="enterYearinspect">年检管理</a></li>
							<shiro:lacksPermission name="comuser:admin">
								<li><a class="J_menuItem" href="enterInsure">保险管理</a></li>
								<li><a class="J_menuItem" href="enterCheck">稽查管理</a></li>
								<li><a class="J_menuItem" href="enterFare">运价管理</a></li>
								<li><a class="J_menuItem" href="enterBreakrule">违章管理</a></li>
							</shiro:lacksPermission>
						</ul></li>
				</shiro:hasPermission>
				<!-- 系统管理模块 -->
				<shiro:hasPermission name="system:mode">
					<li><a href="#"> <i class="fa fa-gears"></i> <span
							class="nav-label">系统管理</span> <span class="fa arrow"></span>
					</a>
						<ul class="nav nav-second-level">
							<li><a class="J_menuItem" href="alarmLog.jsp" id="alarmLog">报警日志</a></li>
							<li><a class="J_menuItem" href="getDealAlarm" id="getDealAlarm">未处理报警信息</a></li>
							<shiro:lacksPermission name="comuser:admin">
								<shiro:lacksPermission name="comuser:admin">
									<li><a class="J_menuItem" href="inotOrderLog">命令日志</a></li>
									<li><a class="J_menuItem" href="getPictureByTaxi">拍照记录</a></li>
								</shiro:lacksPermission>
								<shiro:hasPermission name="terminal:update">
									<li><a class="J_menuItem" href="intoIsuControl">指令控制</a></li>
									<li><a class="J_menuItem" href="sysUpdate">终端远程升级</a></li>
<%--									 <li><a class="J_menuItem" href="intoOverSpeed">超速设置</a></li>--%>
<%--									 <li><a class="J_menuItem" href="setControl">终端控制</a></li>--%>
								</shiro:hasPermission>
								<!--  -->
								<!-- <li><a class="J_menuItem" href="test.jsp">测试</a></li> -->
								<!-- <li><a class="J_menuItem" href="">创建新表</a></li> -->
							</shiro:lacksPermission>
						</ul></li>
				</shiro:hasPermission>
				<!-- 信用审核模块 -->
				<shiro:hasPermission name="belive:mode">
					<li><a href="#"> <i class="fa fa-list-alt"></i> <span
							class="nav-label">信用审核</span> <span class="fa arrow"></span>
					</a>
						<ul class="nav nav-second-level">
							<li><a class="J_menuItem" href="userAddComplaint">客户投诉</a></li>
							<shiro:lacksPermission name="comuser:admin">
								<li><a class="J_menuItem" href="intoAddComplaint">投诉管理</a></li>
<%--								<li><a class="J_menuItem" href="toAssessOfComp">投诉审核</a></li>--%>
<%--								<li><a class="J_menuItem" href="getDealOfComp">投诉处理</a></li>--%>
<%--								<li><a class="J_menuItem" href="intolCompApprova">投诉审批</a></li>--%>
								<li><a class="J_menuItem" href="getArchiveOfComp">投诉档案</a></li>
								<li><a class="J_menuItem" href="getAllCompType">投诉类型</a></li>
								<li><a class="J_menuItem" href="getAsscycle">考核周期</a></li>
								<li><a class="J_menuItem" href="getScorerank">等级设置</a></li>
								<li><a class="J_menuItem" href="getEvalScore">评价管理</a></li>
<%--								<li><a class="J_menuItem" href="getCurrAssess">当前考核</a></li>--%>
							</shiro:lacksPermission>
							<!--  <li><a class="J_menuItem" href="">信用档案</a></li> -->
						</ul></li>
				</shiro:hasPermission>


				<!-- 营运信息模块 -->
				<shiro:hasPermission name="operationinfo:mode">
					<li><a href="#"> <i class="fa fa fa-taxi"></i> <span
							class="nav-label">营运信息</span> <span class="fa arrow"></span>
					</a>
						<ul class="nav nav-second-level">
							<li><a class="J_menuItem" href="intoCheckMeter">营运信息</a></li>
							<li><a class="J_menuItem" href="getRevenue">营收报表</a></li>
						</ul></li>
				</shiro:hasPermission>


				<!-- 报表模块 -->
				<shiro:hasPermission name="chart:mode">
					<li><a href="#"> <i class="fa fa fa-bar-chart-o"></i> <span
							class="nav-label">报表</span> <span class="fa arrow"></span>
					</a>
						<ul class="nav nav-second-level">
							<li><a class="J_menuItem" href="getTreeOfOnline">上线率报表</a></li>
							<!-- <li><button class="J_menuItem" id="showRight" >显示车辆列表</button></li> -->
							<li><a class="J_menuItem" href="enterOperate">单车营运信息</a></li>
							<li><a class="J_menuItem" href="getmileageUtil">里程利用率</a></li>
							<li><a class="J_menuItem" href="enterPassVol">客运量分析</a></li>
							<li><a href="dataShow.jsp">数据可视化</a></li>

							<!-- <li><a class="J_menuItem" href="getRevenue">营收报表</a></li> -->
							<!-- <li><a class="J_menuItem" href="">合租比率</a></li> -->
							<!-- <li><a class="J_menuItem" href="enterTranCap">运力分析</a></li> -->
							<!-- <li><a class="J_menuItem" href="">服务评价</a></li>
						<li><a class="J_menuItem" href="">服务排名</a></li>
						<li><a class="J_menuItem" href="">公司评价</a></li> -->
						</ul></li>
				</shiro:hasPermission>
				<!-- 广告模块 -->
				<%-- <shiro:hasPermission name="advs:mode">

					<li class="<shiro:hasRole name="advser">active</shiro:hasRole>">
						<a href="#"> <i class="fa fa fa-volume-up"></i> <span
							class="nav-label">广告管理</span> <span class="fa arrow"></span>
					</a>
						<ul class="nav nav-second-level">
							<li><a class="J_menuItem" href="enterAds">添加广告</a></li>
							<li><a class="J_menuItem" href="intoPubSend">公益广告</a></li>
							<li><a class="J_menuItem" href="intoAdsSend">广告下发</a></li>
							<li><a class="J_menuItem" href="getAllAdsCustomer">客户管理</a></li>
							<li><a class="J_menuItem" href="getAllAdsControl">时段设置</a></li>
							<li><a class="J_menuItem" href="intoInsSend">设备维护</a></li>
							<li><a class="J_menuItem" href="enterHistory">历史记录</a></li>
							<li><a class="J_menuItem" href="historyTemp">最近记录</a></li>
						</ul>
					</li>
				</shiro:hasPermission> --%>

			</ul>
		</div>

		</nav>
		<!--左侧导航结束-->

		<!--右侧部分开始-->
		<!-- 右侧车辆信息栏 -->
		<!-- 
		<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right"
			id="cbp-spmenu-s2" style="background:#2F4050;color:#A7B1C2;">
	
		<h3 style="background:#233645;height:65px;">
			车辆状态栏 <input type="button" class="refresh" title="刷新车辆列表"
				onmouseup='movr(this)' onmousedown='mdwn(this)' id="freshTaxi">
		</h3>
		
		gy
<li data-toggle="collapse"  href="#list1" data-parent="#accordion"><a href="#"> <i class="fa fa-gears"></i> <span
							class="nav-label">选择公司</span></a></li>
			<ul  id="list1" class="collapse">
							</ul> -->



		<!-- 		gy -->

		<!-- 		<ul id="ulright" style="background:#2F4050; border:1px solid;"> -->

		<!-- 			<li><a href="javascript:show(1);" -->
		<!-- 				style="height:52px;font-size:18px;">点击选择公司:</a> -->
		<!-- 				<ul -->
		<!-- 					style="display: none; cursor: pointer; font-family: 微软雅黑; font-size: 17px; color: white" -->
		<!-- 					id="list1"> -->
		<!-- 				</ul></li> -->
		<!-- 		</ul> -->
		<!-- 设置车辆超出范围时显示滚动 -->
		<!-- 		<table
			style=" background:#2F4050;font-family: 微软雅黑; color: #A7B1C2; font-size: 12px; text-align: left; overflow: auto;"
			class="table table-stripped" id="table-state"
			 width="300px" bgcolor="black">
			<thead>
				<tr>
					<th></th>
					<th style="color: white;">车牌号</th>
					<th style="color: white;">在线状态</th>
					<th style="color: white;">定位状态</th>
					<th style="color: white;">营运状态</th>
				</tr>
			</thead>
			<tbody id="statetbody">

			</tbody>
			<tfoot>
				<tr>
					<td colspan="5">
						<ul class="pagination pull-right"></ul>
					</td>
				</tr>
			</tfoot>
		</table>
	
		</nav>
 -->

		<div id="page-wrapper" class="gray-bg dashbard-1">

			<!-- gy			最顶端的隐藏左侧导航栏的按钮 -->
			<!-- <nav class="navbar navbar-static-top" role="navigation"
				style="margin-bottom: 0">
			<div class="navbar-header">
				<a class="navbar-minimalize minimalize-styl-2 btn btn-primary "
					href="#"><i class="fa fa-bars"></i> </a>
			</div>
			</nav> -->
			<!-- 			标签栏 -->
			<div class="row content-tabs">
				<!--  修改隐藏按钮的位置到导航栏-->
				<div class="roll-nav"
					style="width: 50px; padding-top: 7px; margin-left: 5px;">
					<a class="navbar-minimalize minimalize-styl-1 btn btn-primary "
						href="#"><i class="fa fa-bars"></i> </a>
				</div>
				<!--  修改结束-->
				<!-- 向左的双箭头 -->
				<button class="roll-nav roll-left J_tabLeft"
					style="margin-left: 60px;">
					<i class="fa fa-backward"></i>
				</button>

				<!--车辆实时监控标签 -->
				<nav class="page-tabs J_menuTabs" style="margin-left:100px;">
				<div class="page-tabs-content">

					<shiro:lacksRole name="advser">
						<a href="javascript:;" class="active J_menuTab"
							data-id="getTreeOfTemp">车辆实时监控</a>
					</shiro:lacksRole>
					<shiro:hasRole name="advser">
						<a href="javascript:;" class="active J_menuTab"
							data-id="getTreeOfTemp">添加广告</a>
					</shiro:hasRole>
				</div>
				</nav>
				<!-- 				<span id="fackbook" class="tooltip" title="js-css.cn">www.js-css.cn</span> -->
				<button class="roll-nav roll-right J_tabRight">
					<i class="fa fa-forward"></i>
				</button>

				<div class="btn-group roll-nav roll-right">

					<button class="dropdown J_tabClose" data-toggle="dropdown">
						关闭操作<span class="caret"></span>

					</button>
					<ul role="menu" class="dropdown-menu dropdown-menu-right">
						<li class="J_tabShowActive"><a>定位当前选项卡</a></li>
						<li class="divider"></li>
						<li class="J_tabCloseAll"><a>关闭全部选项卡</a></li>
						<li class="J_tabCloseOther"><a>关闭其他选项卡</a></li>
					</ul>
				</div>

				<a class="roll-nav roll-right J_tabExit" id="exit"> <i
					class="fa fa fa-sign-out"></i> 退出
				</a>
			</div>
			<div class="row J_mainContent" id="content-main">
				<iframe class="J_iframe" name="iframe0" allowfullscreen="true"
					allowtransparency="true" width="100%" height="100%"
					src="<shiro:hasRole name="advser">enterAds</shiro:hasRole><shiro:lacksRole name="advser">getTreeOfTemp</shiro:lacksRole> "
					frameborder="0" data-id="getTreeOfTemp" seamless></iframe>
			</div>
			<div class="footer  navbar-fixed-bottom" style="height: 42px">
				<div class="col-sm-offset-2 ">
					<shiro:lacksRole name="advser">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				实时在线率(重车/电召/暂停)：<input type="button" id="online0">
				&nbsp;|&nbsp; 24小时在线率：<input type="button" id="online24">
				&nbsp;|&nbsp;&nbsp;&nbsp;
				<!-- 暂时隐藏车辆实时状态栏 -->
						<!-- <input type="button" class="btn19"
						id="showRight" value="车辆实时状态"
						onmouseover="this.style.backgroundPosition='left -40px'"
						onmouseout="this.style.backgroundPosition='left -36px'"> -->
					</shiro:lacksRole>
					<div class="pull-right">
						<p>
							Copyright &copy 2018 v1.0 ; <a href="http://group.hx.cn/"
								target="_blank" style="color: gray">恒星集团</a><br>
					</div>
				</div>
			</div>
		</div>

	</div>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<!-- SweetAlert 提示框 -->
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<!-- 左边导航栏的收起以及上下滚动 -->
	<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<!--不打开新界面  -->
	<script src="js/plugins/layer/layer.min.js"></script>
	<!--不打开新界面，且左侧导航栏无样式  -->
	<script src="js/hplus.min.js?v=4.0.0"></script>
	<!-- 左侧导航栏无样式 -->
	<script type="text/javascript" src="js/contabs.min.js"></script>
	<script src="js/plugins/pace/pace.min.js"></script>
	<!--弹出框  -->
	<script src="js/content.min.js?v=1.0.0"></script>
	<script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>
	<!-- 导入按钮弹出侧边栏事件-->
	<script src="js/classie.js"></script>
	<!-- 右侧边栏显示/隐藏 -->
	<script>
		//设置开关函数（右侧菜单）
		/* 	var menuRight = document.getElementById('cbp-spmenu-s2'), showRight = document
					.getElementById('showRight'), showRightPush = document
					.getElementById('showRightPush'), body = document.body;
			//添加函数开关，防止公司添加多次。

			var flag = false;//标识是否隐藏右侧状态栏（）

			var power = true;
			showRight.onclick = function() {

				//添加判断是否是第一次点击（）
				flag = !flag;
				if (i == 1) {
					i--;
					flag = false;
				}

				if (power) {
					addCompany();
					power = false;
				}
				classie.toggle(this, 'active');
				classie.toggle(menuRight, 'cbp-spmenu-open');
				disableOther('showRight');
			};

			//窗口的鼠标点击监听（）
			var i = 0;
			document.onclick = function(ev) {
				if (flag) {
					if (i == 0) {
						i++;
						findDimensions();
						return;
					}
					var oEvent = ev || event;
					var toRight = winWidth - oEvent.clientX;
					if (toRight > 300) {
						flag = false;
						i--;
						classie.toggle(menuRight, 'cbp-spmenu-open');
					} else {
						flag = true;
						return;
					}
				}
			};

			//获取浏览器的场合宽（）
			var winWidth = 0;
			var winHeight = 0;
			function findDimensions() {
				//获取窗口宽度 
				if (window.innerWidth)
					winWidth = window.innerWidth;
				else if ((document.body) && (document.body.clientWidth))
					winWidth = document.body.clientWidth;
				//获取窗口高度 
				if (window.innerHeight)
					winHeight = window.innerHeight;
				else if ((document.body) && (document.body.clientHeight))
					winHeight = document.body.clientHeight;
				//通过深入Document内部对body进行检测，获取窗口大小 
				if (document.documentElement
						&& document.documentElement.clientHeight
						&& document.documentElement.clientWidth) {
					winWidth = document.documentElement.clientWidth;
					winHeight = document.documentElement.clientHeight;
				}
			}

			//获取鼠标点击的位置（）
			function getMousePos() {
				if (flag) {
					findDimensions();
					//获取浏览器的宽度和高度
					document.onclick = function(ev) {
						var oEvent = ev || event;
						var toRight = winWidth - oEvent.clientX;
						var toTop = oEvent.clientY;
						if (toRight > 300 || toTop < 110) {
							flag = false;
							classie.toggle(menuRight, 'cbp-spmenu-open');
						}
					};
				}
			}

			function disableOther(button) {
				if (button !== 'showRight') {
					classie.toggle(showRight, 'disabled');
				}

			} */
	</script>
	<script src="js/plugins/gritter/jquery.gritter.min.js"></script>
	<!-- 点击退出连接 -->
	<script type="text/javascript">
		$("#exit").click(function() {
			sweetAlert({
				title : "确定退出？",
				text : "将退出系统并返回登录界面",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : "#DD6B55",
				confirmButtonText : "确定",
				cancelButtonText : "取消",
				closeOnConfirm : true
			}, function() {

				$("#exit").attr("href", "logout");
				var exit = document.getElementById("exit");
				exit.click();
			});
		})
		// 代码转移到后台
	</script>
	<script>
		/*开始对在线信息的轮询  */
		$(function() {
			setInterval(function() {
				online();
			}, 5000); //注意:执行的函数需要加引号,否则会报错的 
		});
		/*开始对报警信息的轮询  */
		$(function() {
			setInterval(function() {
				pollingOfAlarm();
				pollingOfReport();
			}, 60000); //注意:执行的函数需要加引号,否则会报错的
		});
		/* 报警信息的轮询方法 */
		function pollingOfAlarm() {
			var url = "pollingOfAlarm";
			$.post(url, null, function(data) {
				var newdata = eval("(" + data + ")");
				if (newdata != 0) {
					chlink(data);
				}
			});
		}
		/* 打开新界面 */
		function onclickOfAlarm() {
			var oBtn = document.getElementById('getDealAlarm');
			oBtn.click();
			$.gritter.removeAll();
		}

		/* 投诉信息的轮询方法 */
		function pollingOfReport() {
			var url = "pollingOfReport";
			$.post(url, null, function(data) {
				var newdata = eval("(" + data + ")");
				if (newdata != 0) {
					chlink2(data);
				}
			});
		}

		/** 在线率显示的轮询方法 **/
		function online() {
			var url = "getOnlineRate";
			$.post(url, null, function(data) {
				//alert(data);
				var newdata = eval("(" + data + ")");
				var currOnline = newdata.currOnline;
				var histOnline = newdata.histOnline;
				var operNum = newdata.operNum;
				var empNum = newdata.empNum;
				var callNum = newdata.callNum;
				//alert(currOnline+" "+histOnline+" "+operNum+" "+empNum+" "+callNum);
				$("#online0").val(
						currOnline + "(" + operNum + "/" + callNum + "/"
								+ empNum + ")");
				$("#online24").val(histOnline);
			});
		}
		/* 预设信息轮询*/
		function preInfoPolling() {
			var url = "preInfoPolling";
			$.post(url, null, function(data) {
			});
		}

		/* 有未处理的报警信息时提示 */
		function chlink(n) {
			$.gritter
					.add({
						/*  class="text-warning" */
						title : "您有" + n + "条报警消息 <b>未处理</b>",
						text : "请前往<input type='button' id='getNew' value='报警日志' onclick='onclickOfAlarm()'  style='color:#EEB422'>查看报警信息",
						time : 10000
					//表示提示框存在的毫秒數
					});
			var audio = document.createElement("audio");
			audio.src = "6000.mp3";
			audio.play();
		}
		/* 有未处理的投诉信息时提示 */
		function chlink2(n) {
			$.gritter
					.add({
						title : "您有" + n + "条投诉消息 <b>未处理</b>",
						text : '请前往<a href="mailbox.html" class="text-warning">投诉日志</a>查看未处理投诉信息',
						time : 20000
					//表示提示框存在的毫秒數
					});
			var audio = document.createElement("audio");
			audio.src = "6000.mp3";
			audio.play();
		}
	</script>
	<!-- <script type="text/JavaScript">
		function show(index) {
			//然后下拉进行显示
			var elem = document.getElementById('list' + index);
			if (elem.style.display == 'none')
				elem.style.display = 'block';
			else
				elem.style.display = 'none';
		}
	</script>
	<script type="text/javascript">
		//如果参数存在则显示所选条件 
		function search(index, id) {
			var obj = document.getElementById("down");
			var tablename = index;
			obj.src = index + "search?&tablename=" + index;
			$('#li' + id).css({
				'background-color' : '#53c2e8'
			});
		}
	</script>
	右侧车辆信息（指定公司）的定时显示
	<script>
		function showInfo(data) {

			isu = eval("(" + data + ")");
			//alert("手机号"+isu);

			var url = "getMonitoring";

			//根据终端号去查找是否在线
			//将返回数据显示到菜单的列表中。

			var args = {
				"isuNums" : isu
			};

			$.post(url,
							args,
							function(data) {
								//提前清空车辆信息。进行刷新。
								$("#statetbody  tr ").empty("");
								var trStr = "";

								var d = eval("(" + data + ")");
								for (var i = 0; i < d.length; i++) {
									if (d[i].state == "在线") {
										trStr += "<tr>"
												+ "<td><img  src='img/gy_car.png'>"
												+ "</td>" + "<td style='color: white;'>"
												+ d[i].taxiNum + "</td>"
												+ "<td style='color: green;'>"
												+ d[i].state + "</td>" + "<td style='color: white;'>"
												+ d[i].locateState + "</td>"
												+ "<td style='color: white;'>" + d[i].operateState
												+ "</td>" + "</tr>";
									} else if (d[i].state == "离线") {
										trStr += "<tr>"
												+ "<td><img  src='img/gy_car.png'>"
												+ "</td>" + "<td style='color: white;'>"
												+ d[i].taxiNum + "</td>"
												+ "<td style='color: red;'>"
												+ d[i].state + "</td>" + "<td style='color: white;'>"
												+ d[i].locateState + "</td>"
												+ "<td style='color: white;'>" + d[i].operateState
												+ "</td>" + "</tr>";
									} else {
										trStr += "<tr>"
												+ "<td><img  src='img/gy_car.png'>"
												+ "</td>" + "<td style='color: white;'>"
												+ d[i].taxiNum + "</td>"
												+ "<td style='color: gray;'>"
												+ d[i].state + "</td>" + "<td style='color: white;'>"
												+ d[i].locateState + "</td>"
												+ "<td>" + d[i].operateState
												+ "</td>" + "</tr>";
									}
								}
								document.getElementById("statetbody").innerHTML = trStr;
							});
		}
	</script>
	<script type="text/javascript">
		var id = null;
		function addCompany() {
			//alert("调用函数成功");
			//添加点击开关。防止每次选择公司都传递一次请求，该功能通过刷新按钮来实现。
			var state = true;
			var ul = document.getElementById('list1');
			var url = "getAllCompanyAjax";

			$.get(url, function(data) {
				var companies = eval("(" + data + ")");
				// 输出 所有公司 
				//alert(companies);
				var state = true;
				for (var i = 0; i < companies.length; i++) {
					var text = companies[i].name;
					//state=true;
					var newli = document.createElement("li");
					newli.className="company_li";
					newli.id = companies[i].id;
					var textNode = document.createTextNode(text);
					newli.appendChild(textNode);
					//添加点击函数，实时显示该公司车辆信息
					var statee = true;
					newli.onclick = function() {
						//if(state){ 

						//$("#table-state  tr:not(:first)").html("");
						if (id !== this.id) {
							//区别公司，防止更换公司后点击事件被锁死。
							state = true;
							id = this.id;
							//清空表格
							$("#table-state tr:not(:first)").html("");
							//alert(id);
						}
						var isu = [];
						// alert("id"+id); 
						var url = "getTaxiByCom";
						$.get(url, {
							"id" : id
						}, function(data) {
							isu = eval("(" + data + ")");
							//alert(data);
							//如果该公司没有车辆的话，直接清空表。
							if (isu == "" || isu == null) {
								alert("当前公司暂无车辆");
								$("#table-state tr:not(:first)").html("");
							} else {
								if (state) {
									//限制点击次数
									state = false;
									showInfo(data);

								}
							}
						});
						//}
					};
					ul.appendChild(newli);

				}

			});
		}
		$("#freshTaxi").click(function() {

			if (id == null) {
				alert("请先选择公司");
			} else {
				//清空表格。更换车辆、
				$("#statetbody  tr ").empty("");
				var url = "getTaxiByCom";
				$.get(url, {
					"id" : id
				}, function(data) {

					//如果该公司没有车辆的话，直接清空表。
					//alert("data");
					//每次刷新都清空表格。
					showInfo(data);
				});

			}

		})
	</script> -->
	<script type="text/javascript">
		//格式化字符串
		function StringBuffer() {
			this._strs = new Array;
		}
		StringBuffer.prototype.append = function(str) {
			this._strs.push(str);
		};
		StringBuffer.prototype.toString = function() {
			return this._strs.join("");
		};
	</script>
	<!-- 鼠标悬停提示 -->
	<!-- 	<script type="text/javascript"> -->
	<!-- 		$(document).ready(function() { -->
	<!-- 			$('.refresh').tooltipster(); -->
	<!-- 		}); -->
	<!-- 	</script> -->
	<!-- 刷新按钮动态效果 -->
	<script language=javascript>
		function movr(src) {
			src.onselectstart = new Function("return false");
			with (src.style) {

				borderLeft = "4px solid buttonhighlight";
				borderRight = "4px solid buttonshadow";
				borderTop = "4px solid buttonhighlight";
				borderBottom = "4px solid buttonshadow";
				padding = "0";
				cursor = "hand";
			}
		}
		function mdwn(src) {
			src.onselectstart = new Function("return false");
			with (src.style) {

				borderRight = "1px solid buttonhighlight";
				borderLeft = "1px solid buttonshadow";
				borderBottom = "1px solid buttonhighlight";
				borderTop = "1px solid buttonshadow";
				padding = "0";
			}
		}
		/* function mout(src)  
		 {    src.onselectstart = new Function("return false");  
		 with (src.style) {  
		 color="black"  
		 background = "buttonface"; //此为鼠标移动后的颜色，可根据需要改变 
		 border = "1px solid buttonface";  
		 padding   = "0";  
		 }  
		 } */
	</script>

</body>

</html>
