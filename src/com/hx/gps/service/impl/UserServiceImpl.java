/**
 * 
 */
package com.hx.gps.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hx.gps.dao.UserDao;
import com.hx.gps.entities.User;
import com.hx.gps.service.UserService;

/**
 * @author Minazuki
 *
 */
@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao userDao;

	@Override
	public User login(User user) {
		return userDao.findUserByAccount(user.getName(), user.getPassword());
	}

	@Override
	public void updatePass(User user, String newPassword) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public User findUserByName(String username) {
		
		return userDao.findUserByName(username);
	}

}
