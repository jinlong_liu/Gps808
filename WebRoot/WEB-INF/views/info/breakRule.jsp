<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<script src="js/jquery.js"></script>

<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">

<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>

<!-- gy 添加表单验证 -->
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />
<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">
		<!-- 查询列表 -->
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>输入条件查询违章信息</h5>
				</div>
				<div class="ibox-content">
					<form action="getBreakrule" class="form-horizontal" method="post">
						<div class="row">
							<div class="form-group">
								<!-- 违章时间 -->
								<label class="col-sm-1 control-label">违章时间:</label>
								<div class="col-sm-2">
									<input type="text" class="form-control" name="breakTime" autocomplete="off"
										onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss'})">
								</div>
								<!-- 车牌号码 -->
								<label class="col-sm-1 control-label">车牌号码:</label>
								<div class="col-sm-2">
									<input type="text" class="form-control" name="taxiNum" autocomplete="off">
								</div>
								<!-- 处理情况 -->
								<label class="col-sm-1 control-label">处理情况:</label>
								<div class="col-sm-2" style="margin-top: 6px">
									 
								 <input checked="checked" type="radio"
											name="state" value=0> 	 未处理 
									 <input type="radio" name="state" value=1>  已处理
								        <input type="radio" name="state" value=2
											checked="checked">      全部
							 
								</div>

								<div class="col-sm-2">
									<button type="submit" class="btn btn-primary">查询</button>
									<button id="btn_addBreakRule" type="button"
										class="btn btn-primary" data-toggle="modal"
										data-target="#myModal5">添加</button>

								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

			<!--gy修改后 点击添加按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal5" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form id="saveBreakRuleForm" class="form-horizontal"
						action="saveBreakrule" method="post">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>新增违章信息 <span style="color:red">（* 为必填项）</span></h5>
							</div>
							<div class="ibox-content">
								<div class="row">
									<!-- 违章时间-->
									<div class="form-group">
										<label class="col-sm-2 control-label"><span style="color:red">*  </span>违章时间 ： </label>
										<div class="col-sm-7">
											<input type="text" class="form-control" name="breakTime" autocomplete="off"
												onfocus="WdatePicker({maxDate:'%y-%M-%d',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(saveBreakRuleForm, name.valueOf())}})">
										</div>
									</div>
									<!-- 违章车辆-->
									<div class="form-group">
										<label class="col-sm-2 control-label"><span style="color:red">*  </span>违章车辆 ： </label>
										<div class="col-sm-7">
											<input type="text" class="form-control" name="taxiNum" autocomplete="off">
										</div>
									</div>
									<!-- 违章驾驶员-->
									<div class="form-group">
										<label class="col-sm-2 control-label"><span style="color:red">*  </span>违章驾驶员 ： </label>
										<div class="col-sm-7">
											<input type="text" class="form-control" name="driver" autocomplete="off">
										</div>
									</div>
									<!-- 违章类型-->
									<div class="form-group">
										<label class="col-sm-2 control-label"><span style="color:red">*  </span>违章类型 ： </label>
										<div class="col-sm-7">
											<input type="text" class="form-control" name="type" autocomplete="off">
										</div>
									</div>
									<!-- 扣分-->
									<div class="form-group">
										<label class="col-sm-2 control-label"><span style="color:red">*  </span>扣分 ： </label>
										<div class="col-sm-7">
											<input type="text" class="form-control" name="reScore" autocomplete="off">
										</div>
									</div>
									<!-- 罚款-->
									<div class="form-group">
										<label class="col-sm-2 control-label"><span style="color:red">*  </span>罚款  ：</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" name="reMoney" autocomplete="off">
										</div>
									</div>

									<!-- 处理意见-->
									<div class="form-group">
										<label class="col-sm-2 control-label">处理意见 ： </label>
										<div class="col-sm-7">
											<input type="text" class="form-control" name="remark" autocomplete="off">
										</div>
									</div>

									<!-- 处理情况-->
									<div class="form-group">
										<label class="col-sm-2 control-label">处理情况  ：</label>
										<div class="col-sm-7">
											<input type="checkbox" name="state" value="1">
										</div>
									</div>

									<!-- 处理时间-->
									<div class="form-group">
										<label class="col-sm-2 control-label">处理时间 ：</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" name="dealTime" autocomplete="off"
												onfocus="WdatePicker({maxDate:'%y-%M-%d',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(saveBreakRuleForm, name.valueOf())}})">
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-white"
										data-dismiss="modal">关闭</button>
									<button type="submit" class="btn btn-primary">保存</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!--gy修改前 点击添加按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal52222" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form id="saveBreakRuleForm" action="saveBreakrule" method="post">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>新增违章信息</h5>
							</div>
							<div class="ibox-content">
								<div class="row">
									<div class="col-sm-4">
										违章时间：<input type="text"  name="breakTime"
											onfocus="WdatePicker({maxDate:'%y-%M-%d',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(saveBreakRuleForm, name.valueOf())}})">
									</div>
									<div class="col-sm-4">
										违章车辆：<input type="text" name="taxiNum">
									</div>
									<div class="col-sm-4">
										违章驾驶员：<input type="text" name="driver">
									</div>
								</div>
								<br>

								<div class="row">
									<div class="col-sm-4">
										违章类型：<input type="text" name="type">
									</div>
									<div class="col-sm-4">
										扣分：<input type="text" name="reScore">
									</div>
									<div class="col-sm-4">
										罚款：<input type="text" name="reMoney">
									</div>
								</div>
								<br>

								<div class="row">
									<div class="col-sm-4">
										处理意见：<input type="text" name="remark">
									</div>
									<div class="col-sm-4">
										处理情况：<input type="checkbox" name="state" value="1">
									</div>
									<div class="col-sm-4">
										处理时间：<input type="text" readonly="readonly" name="dealTime"
											onfocus="WdatePicker({maxDate:'%y-%M-%d'})">
									</div>
								</div>
								<br>

							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
							<button type="submit" class="btn btn-primary">保存</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- 数据展示 -->
		<c:if test="${breakrules!=null}">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-title">
					<div>
						<h5>&nbsp;&nbsp;&nbsp;违章信息表</h5>
					</div>
				</div>
				<div class="ibox-content" style="height: 660px">
					<table id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
						
						<thead>
							<tr>
								<th>违章时间</th>
								<th>扣分</th>
								<th>罚款</th>
								<th>处理意见</th>
								<th>是否处理完成</th>
								<!-- 5 -->

								<th>处理时间</th>
								<th>车牌号</th>
								<th>驾驶员</th>
								<th>违章类型</th>
								<!-- 9 -->

								<th width="120">操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${breakrules}" var="breakrules">
								<tr>
									<td>${breakrules.breakTime }</td>
									<td>${breakrules.reScore }</td>
									<td>${breakrules.reMoney }</td>
									<td>${breakrules.remark }</td>
									<td><c:if test="${breakrules.state ==0 }">未处理</c:if> <c:if
											test="${breakrules.state ==1 }">已处理 </c:if></td>
									<!-- 5 -->

									<td>${breakrules.dealTime }</td>
									<td>${breakrules.taxiNum }</td>
									<td>${breakrules.driver }</td>
									<td>${breakrules.type }</td>
									<!-- 9 -->

									<td><shiro:hasPermission name="breakrule:admin">
											<button id="btn-update" type="button"
												class="btn btn-success btn-sm" data-toggle="modal"
												data-target="#myModal1"
												onclick="update('${breakrules.id }','${breakrules.breakTime }','${breakrules.reScore}','${breakrules.reMoney}','${breakrules.remark}','${breakrules.state}'
									,'${breakrules.dealTime}','${breakrules.taxiNum}','${breakrules.driver}','${breakrules.type}');editBreakRule()">修改</button>
											<button type="button" class="btn btn-danger btn-sm"
												onclick="delete1('${breakrules.id}','${breakrules.driver}')">删除</button>
										</shiro:hasPermission></td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="10">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		</c:if>
		<!-- 删除表格 -->
		<form id="deleteForm" action="deleteBreakrule" method="post">
			<input type="hidden" id="deleteId" name="id">
		</form>

		<!-- 点击"修改"按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal1" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form id="updateBreakruleForm" action="updateBreakrule" class="form-horizontal"
						method="post">
						<input id="id" name="id" type="hidden">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>修改违章信息（*为必填项）</h5>
							</div>
							<div class="ibox-content">
								<div class="row">
									<div class="form-group">
										<!--违章时间  -->
										<label class="col-sm-3 control-label">违章时间 *</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" name="breakTime"
												id="breakTime"
												onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{\'new Date()\'}',onpicked:function(dp){refreshValidator(updateBreakruleForm, name.valueOf())}})">
										</div>
									</div>
								</div>
								<!--违章车辆  -->
								<div class="row">
									<div class="form-group">
										<label class="col-sm-3 control-label">违章车辆 *</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" name="taxiNum"
												id="taxiNum">
										</div>
									</div>
								</div>

								<!--违章驾驶员  -->
								<div class="row">
									<div class="form-group">
										<label class="col-sm-3 control-label">违章驾驶员 *</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" name="driver"
												id="driver">
										</div>
									</div>
								</div>
								<!--违章类型  -->
								<div class="row">
									<div class="form-group">
										<label class="col-sm-3 control-label">违章类型 *</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" name="type" id="type">
										</div>
									</div>
								</div>
								<!--扣分  -->
								<div class="row">
									<div class="form-group">
										<label class="col-sm-3 control-label">扣分 *</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" name="reScore"
												id="reScore">
										</div>
									</div>
								</div>
								<!--罚款  -->
								<div class="row">
									<div class="form-group">
										<label class="col-sm-3 control-label">罚款 *</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" name="reMoney"
												id="reMoney">
										</div>
									</div>
								</div>
								<!--处理意见  -->
								<div class="row">
									<div class="form-group">
										<label class="col-sm-3 control-label">处理意见</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" name="remark"
												id="remark">
										</div>
									</div>
								</div>
								<!--是否处理完成  -->
								<div class="row">
									<div class="form-group">
										<label class="col-sm-3 control-label">是否处理完成</label>
										<div class="col-sm-7">
											<input type="checkbox" name="state" value="1" id="state">
										</div>
									</div>
								</div>
								<!--处理时间  -->
								<div class="row">
									<div class="form-group">
										<label class="col-sm-3 control-label">处理时间</label>
										<div class="col-sm-7">
											<input type="text" class="form-control" name="dealTime"
												id="dealTime"
												onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{\'new Date()\'}',onpicked:function(dp){refreshValidator(updateBreakruleForm, name.valueOf())}})">
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-white"
										data-dismiss="modal">关闭</button>
									<button type="submit" class="btn btn-primary">保存</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<!-- 	<script src="js/jquery.min.js?v=2.1.4"></script> -->
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script>
		//加载footable的功能
				$(document).ready(function() {
		$('#table1').DataTable({
			"pagingType" : "full_numbers"
		});
	});

		//点击"修改"按钮
		function update(id, breakTime, reScore, reMoney, remark, state,
				dealTime, taxiNum, driver, type) {
			$("#id").val(id);
			$("#breakTime").val(breakTime);
			$("#reScore").val(reScore);
			$("#reMoney").val(reMoney);
			$("#remark").val(remark);
			// $("#state").val(state);
			if (state == 1) {
				$("[name = state]:checkbox").attr("checked", true);

			}
			$("#dealTime").val(dealTime);
			$("#taxiNum").val(taxiNum);
			$("#driver").val(driver);
			$("#type").val(type);
			//alert("state"+state);
		};

		//点击"删除"按钮
		function delete1(id, name) {
			swal({
				title: "您确定删除 " + name + " 的数据吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
				$("#deleteId").val(id);
				//提交表单
				$("#deleteForm").submit();
			});
		}
		   //页面加载完成后执行,获取执行状态
		$(document).ready(function(){
			var result='<%=request.getAttribute("result")%>';
				if(result != null && result != "null"){
				if (result.indexOf("成功")>0) {
				
				swal({
					title : result,
					type : "success"
				});
			}else{
				swal({
					title :result,
					type : "error"
				});
			}
			}
		});
	</script>

	<script type="text/javascript">
		/*添加违章界面的表单验证*/
		$('#btn_addBreakRule').click(function() {
			$('#saveBreakRuleForm').bootstrapValidator({
				feedbackIcons : {/*输入框不同状态，显示图片的样式*/
					valid : 'glyphicon glyphicon-ok',
					invalid : 'glyphicon glyphicon-remove',
					validating : 'glyphicon glyphicon-refresh'
				},
				/*生效规则：字段值一旦变化就触发验证*/
				live : 'enabled',
				/*当表单验证不通过时，该按钮为disabled*/
				submitButtons : 'button[type="submit"]',
				/*验证*/
				fields : {
					breakTime : {/*键名username和input name值对应*/
						validators : {
							notEmpty : {/*非空提示*/
								message : '违章时间不能为空'
							}
						}
					},
					taxiNum : {
						validators : {
							notEmpty : {
								message : '违章车辆不能为空'
							}
						}
					},
					driver : {
						validators : {
							notEmpty : {
								message : '违章驾驶员不能为空'
							}
						}
					},
					type : {
						validators : {
							notEmpty : {
								message : '违章类型不能为空'
							}
						}
					},
					reScore : {
						validators : {
							notEmpty : {
								message : '扣分不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					reMoney : {
						validators : {
							notEmpty : {
								message : '罚款不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					dealTime : {
						validators : {
							
						}
					}
				}
			});
		});
		
		/*修改违章界面的表单验证*/
		function editBreakRule() {
			$('#updateBreakruleForm').bootstrapValidator({
				feedbackIcons : {/*输入框不同状态，显示图片的样式*/
					valid : 'glyphicon glyphicon-ok',
					invalid : 'glyphicon glyphicon-remove',
					validating : 'glyphicon glyphicon-refresh'
				},
				/*生效规则：字段值一旦变化就触发验证*/
				live : 'enabled',
				/*当表单验证不通过时，该按钮为disabled*/
				submitButtons : 'button[type="submit"]',
				/*验证*/
				fields : {
					breakTime : {/*键名username和input name值对应*/
						validators : {
							notEmpty : {/*非空提示*/
								message : '违章时间不能为空'
							}
						}
					},
					taxiNum : {
						validators : {
							notEmpty : {
								message : '违章车辆不能为空'
							}
						}
					},
					driver : {
						validators : {
							notEmpty : {
								message : '违章驾驶员不能为空'
							}
						}
					},
					type : {
						validators : {
							notEmpty : {
								message : '违章类型不能为空'
							}
						}
					},
					reScore : {
						validators : {
							notEmpty : {
								message : '扣分不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					reMoney : {
						validators : {
							notEmpty : {
								message : '罚款不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					dealTime : {
						validators : {
							notEmpty : {
								message : '处理时间不能为空'
							}
						}
					}
				}
			});
		};
				/*日历刷新验证*/
		function refreshValidator(id,name){
		$(id).data('bootstrapValidator').updateStatus(name,
		'NOT_VALIDATED',null).validateField(name);
		}
	</script>
</body>

</html>