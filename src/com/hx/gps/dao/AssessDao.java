package com.hx.gps.dao;

import java.util.Date;
import java.util.List;

import com.hx.gps.entities.Assarch;
import com.hx.gps.entities.Asscycle;
import com.hx.gps.entities.Complaints;
import com.hx.gps.entities.Comptype;
import com.hx.gps.entities.Deduct;
import com.hx.gps.entities.Evalscore;
import com.hx.gps.entities.Scorerank;

public interface AssessDao {
	/**
	 * 投诉相关
	 * @param compType
	 */
	//添加举报类型
	public void addOrUpdateCompType(Comptype compType);
	
	//根据id删除未审核的投诉记录
	public boolean daleteComById(Integer id); 
	
	//获取所有举报类型
	public List<Comptype> getCompType();
	
	//添加用户投诉
	public boolean addComplaint(Complaints complaint);
	
	//更新用户投诉
	public void updateComplaint(Complaints complaint);
	
	//查看type状态下的投诉
	public List<Complaints> getCompByType(Integer type);
	
	//根据id查找投诉信息
	public Complaints getCompById(Integer id);
	
	//修改投诉信息的状态
	public void updateTypeForComp(Integer id,Integer type);
	
	//添加投诉处理方案
	public boolean addDeduct(Deduct deduct);
	
	//确认修改处理方案的执行
	public void updateDeduct(Integer id);
	
	//删除修改处理方案
	public void deleteDeduct(Integer compId);
	
	//修改用户投诉
//	public void updateComplaint(Complaints complaint);
	
	//关联投诉和处理
	public void connectComp(Complaints complaint);
	
	//删除投诉类型
	public boolean deleteCompType(Integer id);
	
	//获得考核周期
	public List<Asscycle> getAsscycle();
	
	//修改考核信息
	public void updateAsscycle(Asscycle asscycle);
	
	//获取分数等级对应表的内容
	public List<Scorerank> getScorerank();
	
	//添加或更新分数等级对应信息
	public void addOrUpdateScorerank(Scorerank scorerank);
	
	//删除分数等级对应记录
	public void deleteScorerank(Integer id);
	
	//获取评价分数对应信息
	public List<Evalscore> getEvalscore();
	
	//更新或添加评价分数对应信息
	public void addOrUpdateEval(Evalscore evalscoer);
	
	//删除评价分数对应信息
	public void deleteEvalScore(Integer id);
	
	//查找指定月份内的所有扣分记录
	public List<Deduct> getDeductByDate(Date currDate,Date lastDate);
	
	//查询考核信息表，根据传入的名称
	public Asscycle getAsscycleByName(String name);
	
	//根据月份，查看是否有该月份的考核信息存档
	public List<Assarch> getAssarsByDate(String time);
	
	//添加考核记录存储
	public void addAssarch(List<Assarch> asses);
	//删除考核周期
	public void deleteAsscycleById(Integer id);
	//获取投诉信息
	public List<Complaints> getAllComplaints();

	//获取当前考核
	public String[] getAllQuaNums();
}
