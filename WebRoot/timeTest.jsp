
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>视频</title>
<meta charset="utf-8">
<!--设置浏览器可以等比例缩放 -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--设置主地址栏颜色  -->
<meta name="theme-color" content="#000000">
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">
<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="css/plugins/jsTree/style.min.css" rel="stylesheet">
<link href="css/video.css" rel="stylesheet">
<base target="_blank">
</head>
<body class="gray-bg">
<p/>
	<label class="col-sm-1 control-label">选择通道: </label>
	<div class="col-sm-2">
		<select class="form-control" id="channel" name="channel" placeholder="选择通道">
			<option selected="selected" name="通道1" value="record0">通道1</option>
			<option name="通道2" value="record1">通道2</option>
			<option name="通道3" value="record2">通道3</option>
			<option name="通道4" value="record3">通道4</option>
		</select>
	</div>
	<div class="col-sm-1">
		<label class=" control-label">开始时间： </label>
	</div>
	<div class="col-sm-2">
		<input class="form-control" type="text" name="startDate"
			id="startDate" class="Wdate"
			onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true})">
	</div>
	<!-- placeholder="默认为当前时间" -->
	<div class="col-sm-1">
		<label class=" control-label">结束时间：</label>
	</div>
	<div class="col-sm-2">
		<input class="form-control" type="text" name="endDate" id="endDate"
			class="Wdate" value=""
			onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',readOnly:true})">
	</div>
	</div>
	<div class="col-sm-2">
	<a onclick="downloadVideo(this)"  target="_blank" src="">
		<button id="start" type="button" class="btn btn-primary ">点击下载</button>
	</a>
	</div>
	</div>
		<div class="row">
		<div class="col-sm-12">
			<iframe id="reShowVedio" allowfullscreen="true"
				allowtransparency="true" width="100%" height="780px"
				frameborder="no" border="0" scrolling="no"></iframe>
		</div>
	</div>
	 <script src="js/jquery.min.js"></script>
	<script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>
	<script src="js/plugins/gritter/jquery.gritter.min.js"></script>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
	<!-- <script src="js/jquery.min.js?v=2.1.4"></script> -->
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script src="js/plugins/jsTree/jstree.min.js"></script>
	<script src="js/video.js"></script>
	<script src="js/videojs-live.js"></script>
	<script type="text/javascript">
		document.getElementById('reShowVedio').src = window.localStorage
				.getItem("url");

		function downloadVideo(e) {
			var start = $("#startDate").val();
			var end = $("#endDate").val();
			var record=$("#channel").val();
			var start_time = Number(Date.parse(start));
			var end_time = Number(Date.parse(end));
			var url = "http:" + "//" + localStorage.getItem("isu")
					+ ".hxybvideo.com:8090" + "/" + "ts" + "/" + "?command=record"
				    + "&start_time=" + start_time + "&end_time="
					+ end_time + "&channel=" + record;
			$(e).attr("href",url);
			$(e).click();
		}
	</script>
</body>

</html>