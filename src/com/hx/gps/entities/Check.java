package com.hx.gps.entities;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Check entity. @author MyEclipse Persistence Tools
 */
/**
 * @author LFX
 *
 */
@Entity
@Table(name = "check")
public class Check implements java.io.Serializable {

	// Fields

	private Integer id;
	private Timestamp time;
	private String place;
	private String type;
	private String taxiNum;
	private Integer reScore;
	private Float reMoney;
	private String remark;
	private String driver;
	private Integer state;

	// Constructors

	/** default constructor */
	public Check() {
	}

	/** full constructor */
	public Check(Timestamp time, String place, String type, String taxiNum, Integer reScore, Float reMoney,
			String remark, String driver, Integer state) {
		this.time = time;
		this.place = place;
		this.type = type;
		this.taxiNum = taxiNum;
		this.reScore = reScore;
		this.reMoney = reMoney;
		this.remark = remark;
		this.driver = driver;
		this.state = state;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "time", length = 19)
	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@Column(name = "place")
	public String getPlace() {
		return this.place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	@Column(name = "type")
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "taxiNum", length = 20)
	public String getTaxiNum() {
		return this.taxiNum;
	}

	public void setTaxiNum(String taxiNum) {
		this.taxiNum = taxiNum;
	}

	@Column(name = "reScore")
	public Integer getReScore() {
		return this.reScore;
	}

	public void setReScore(Integer reScore) {
		this.reScore = reScore;
	}

	@Column(name = "reMoney", precision = 12, scale = 0)
	public Float getReMoney() {
		return this.reMoney;
	}

	public void setReMoney(Float reMoney) {
		this.reMoney = reMoney;
	}

	@Column(name = "remark")
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "state")
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Column(name = "driver")
	public String getDriver() {
		return this.driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	@Override
	public String toString() {
		return "Check [id=" + id + ", time=" + time + ", place=" + place + ", type=" + type + ", taxiNum=" + taxiNum
				+ ", reScore=" + reScore + ", reMoney=" + reMoney + ", remark=" + remark + ", driver=" + driver
				+ ", state=" + state + "]";
	}

	 

}