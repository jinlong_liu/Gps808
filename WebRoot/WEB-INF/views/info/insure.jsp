<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

<script src="js/jquery.js"></script>
<link href="css/plugins/footable/footable.core.css" rel="stylesheet">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/style.min.css?v=4.0. 0" rel="stylesheet">

<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>

<!-- gy 添加表单验证 -->
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="dist/css/bootstrapValidator.css" />
<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated ">
		<!-- 查询列表 -->
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox-title">
					<h5>输入条件查询保险信息</h5>
				</div>
				<div class="ibox-content">
					<form action="getInsure" class="form-horizontal" method="post">
						<div class="row">
							<div class="form-group">
								<!--缴费日期起  -->
								<label class="col-sm-2 control-label">缴费日期起：</label>
								<div class="col-sm-2">
									<input id="startDate" type="text" class="form-control" autocomplete="off"
										name="payTimeStartString"
										onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss'})">
								</div>
								<!--缴费日期止 -->
								<label class="col-sm-2 control-label">缴费日期止：</label>
								<div class="col-sm-2">
									<input id="endDate" type="text" id="d4322" class="form-control" autocomplete="off"
										name="payTimeEndString"
										onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'startDate\')}',dateFmt:'yyyy-MM-dd HH:mm:ss'})">
								</div>
								<label class="col-sm-1 control-label">车主或车牌号：</label>
								<div class="col-sm-2">
									<input type="text" class="form-control" name="master" autocomplete="off">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label class="col-sm-2 control-label">保险类型：</label>
								<div class="col-sm-2">
									<input type="text" class="form-control" name="type" autocomplete="off">
								</div>
								<div class="col-sm-2 col-sm-offset-2">
									<button type="submit" class="btn btn-primary">查询</button>
									<shiro:hasPermission name="insure:admin">
										<button id="btn_addInsure" type="button"
											class="btn btn-primary" data-toggle="modal"
											data-target="#myModal5">添加</button>
									</shiro:hasPermission>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!--gy修改后  点击添加按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal5" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form id="saveInsureForm" class="form-horizontal"
						action="saveInsure" method="post">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>新增保险信息<span style="color:red">（* 为必填项）</span></h5>
							</div>
							<div class="ibox-content">
								<!-- 第一行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 保险类别 -->
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>保险类别 ：</label>
											<div class="col-sm-8"> 
												<select class="form-control" name="category">
													<!-- 只能给车辆添加保险信息<option>驾驶员</option> -->
													<option>车辆</option>
												</select>
											</div>
										</div>
									</div>
									<!-- 车主或车牌号 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span style="color:red">*  </span>车主或车牌号 ：</label>
											<div class="col-sm-7">
												<input type="text" class="form-control" name="master" autocomplete="off"/>
											</div>
										</div>
									</div>
								</div>

								<!-- 第二行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 保险类型 -->
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>保险类型 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="type" autocomplete="off">
											</div>
										</div>
									</div>
									<!-- 有效期起 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span style="color:red">*  </span>有效期起 ：</label>
											<div class="col-sm-7">
												<input id="GPStart" class="form-control" type="text" autocomplete="off"
													name="validityStart"
													onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(saveInsureForm, name.valueOf())}})">
											</div>
										</div>
									</div>
								</div>
								<!-- 第三行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 有效期止 -->
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>有效期止 ：</label>
											<div class="col-sm-8">
												<input id="GPEnd" type="text" name="validityEnd" autocomplete="off"
													class="form-control"
													onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(saveInsureForm, name.valueOf())}})">
											</div>
										</div>
									</div>
									<!-- 保险额度 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span style="color:red">*  </span>保险额度 ：</label>
											<div class="col-sm-7">
												<input name="coverage" class="form-control" type="text" autocomplete="off">
											</div>
										</div>
									</div>
								</div>
								<!-- 第四行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 缴费日期 -->
											<label class="col-sm-4 control-label"><span style="color:red">*  </span>缴费日期 ：</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="payTime" autocomplete="off"
													onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'GPStart\')}',dateFmt:'yyyy-MM-dd HH:mm:ss',onpicked:function(dp){refreshValidator(saveInsureForm, name.valueOf())}})">
											</div>
										</div>
									</div>
									<!-- 缴费金额 -->
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-5 control-label"><span style="color:red">*  </span>缴费金额 ：</label>
											<div class="col-sm-7">
												<input name="payAmount" class="form-control" type="text" autocomplete="off">
											</div>
										</div>
									</div>
								</div>
								<!-- 第五行 -->
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<!-- 备注 -->
											<label class="col-sm-4 control-label">备注：</label>
											<div class="col-sm-8">
												<textarea style="height: 60px;resize: none;" name="remark"
													class="form-control" aria-required="true"></textarea>
											</div>
										</div>
									</div>
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-white"
										data-dismiss="modal">关闭</button>
									<button type="submit" class="btn btn-primary">保存</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>



		<!-- 数据展示 -->
		<c:if test="${insures!=null}">
				<div class="row">
			<div class="col-sm-12">
				<!-- 				<div width="1000"> -->
				<!-- 					<h4>&nbsp;&nbsp;&nbsp;保险信息表</h4> -->
				<!-- 				</div> -->
				<div class="ibox-title">
					<div>
						<h5>&nbsp;&nbsp;&nbsp;保险信息表</h5>
					</div>
				</div>
				<div class="ibox-content" style="height: 610px">

					<table id="table1" width="100%"
							style="table-layout: fixed; background: #FFFFFF"
							class="footable table table-stripped" data-page-size="10"
							data-filter=#filter>
						<thead>
							<tr>
								<th width="70">车主或车牌号</th>
								<th width="70">保险类别</th>
								<th width="70">保险类型</th>
								<th width="70">缴费日期</th>
								<th width="70">缴费金额</th>
								<!-- 5 -->

								<th width="70">有效日期起</th>
								<th width="70">有效日期止</th>
								<th width="70">备注</th>
								<th width="70">操作人</th>
								<th width="70">添加日期</th>
								<!-- 10 -->


								<th width="100">操作</th>
							</tr>
						</thead>
						<tbody>
 
							<c:forEach items="${insures}" var="insures">
								<tr>
									<td>${insures.master}</td>
									<td>${insures.category}</td>
									<td>${insures.type}</td>
									<td>${insures.payTime}</td>
									<td>${insures.payAmount }</td>
									<!-- 5 -->

									<td>${insures.validityStart }</td>
									<td>${insures.validityEnd }</td>
									<td>${insures.remark }</td>
									<td>${insures.people }</td>
									<td>${insures.time }</td>
									<td><shiro:hasPermission name="insure:admin">
											<button id="btn-update" type="button"
												class="btn btn-success btn-sm" data-toggle="modal"
												data-target="#myModal1"
												onclick="update('${insures.id }','${insures.master }','${insures.category }', '${insures.type }','${insures.payTime }','${insures.payAmount }'
									,'${insures.validityStart }','${insures.validityEnd }','${insures.remark }')">修改</button>
											<button type="button" class="btn btn-danger btn-sm"
												onclick="delete1('${insures.id}','${insures.master}')">删除</button>
										</shiro:hasPermission></td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="11">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		</c:if>
		<!-- 删除表格 -->
		<form id="deleteForm" action="deleteInsure" method="post">
			<input type="hidden" id="deleteId" name="id">
		</form>

		<!-- 点击"修改"按钮弹出来的form表单 -->
		<div class="modal inmodal fade" id="myModal1" tabindex="-1"
			role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<form action="updateInsure"  method="post">
					<fieldset>
						<input id="id" name="id" type="hidden">
						<div class="modal-body">
							<div class="ibox-title">
								<h5>修改保险信息（*为必填项）</h5>
							</div>
							<div class="ibox-content">
								<div class="row">
									<div class="col-sm-4">
										<label class=" control-label">保险类别 *：</label><select name="category" class="form-control" id="category">
											 <option>驾驶员</option>
											<option>车辆</option>
										</select>
									</div>
									<div class="col-sm-4">
										<label class=" control-label">车主或车牌号 *：</label><input type="text" class="form-control" name="master" id="master">
									</div>
									<div class="col-sm-4">
										<label class=" control-label">保险类型 *：</label> <input type="text" class="form-control" name="type" id="type">
									</div>
								</div>
								<br>

								<div class="row">
									<div class="col-sm-4">
										<label class=" control-label">有效期起 *：</label><input type="text" class="form-control" name="validityStart"
											id="validityStart" onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'GPStart\')}',dateFmt:'yyyy-MM-dd HH:mm:ss'})">
									</div>
									<div class="col-sm-4">
										<label class=" control-label">有效期止 *：</label><input type="text" class="form-control" name="validityEnd" id="validityEnd" onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'GPStart\')}',dateFmt:'yyyy-MM-dd HH:mm:ss'})">
									</div>
<!-- 									<div class="col-sm-4">
										<label class=" control-label">保险额度 *：</label><input class="form-control" id="coverage" name="coverage" type="text">
									</div> -->
									<div class="col-sm-4">
										<label class=" control-label">缴费日期 *：</label><input class="form-control" type="text" name="payTime" id="payTime" onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'GPStart\')}',dateFmt:'yyyy-MM-dd HH:mm:ss'})">
									</div>
								</div>
								<br>

								<div class="row">
									<div class="col-sm-4">
										<label class=" control-label">缴费金额 *：</label><input class="form-control" type="text" name="payAmount" id="payAmount">
									</div>
									<div class="col-sm-4">
										<label class=" control-label">备注：</label>
										<textarea style="height: 150px;resize: none;" class="form-control" name="remark" id="remark"
											class="form-control" aria-required="true"></textarea>
									</div>
								</div>
								<br>
							
							<div class="modal-footer">
							<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
							<button type="submit" class="btn btn-primary">保存</button>
						</div>
							
							</div> 
						</div>
						</fieldset>
							
					</form>

				</div>
					
			</div>
		</div>
	</div>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<!-- 	<script src="js/jquery.min.js?v=2.1.4"></script> -->
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="js/plugins/footable/footable.all.min.js"></script>
	<script>
	//加载footable的功能
	$(document).ready(function() {
$('#table1').DataTable({
"pagingType" : "full_numbers"
});
});

		//点击"修改"按钮
		function update(id, master, category, type, payTime, payAmount,
				validityStart, validityEnd, remark) {
			$("#id").val(id);
			$("#master").val(master);
			$("#category").val(category);
			$("#type").val(type);
			$("#payTime").val(payTime);
			$("#payAmount").val(payAmount);
/* 			$("#coverage").val(payAmount); */
			$("#validityStart").val(validityStart);
			$("#validityEnd").val(validityEnd);
			$("#remark").val(remark);
		};

		//点击"删除"按钮
		function delete1(id, name) {
			swal({
				title: "您确定删除 " + name + " 的数据吗?",
				type : "warning",
				showCancelButton : true,
				cancelButtonText : "取消"
			}, function() {
				$("#deleteId").val(id);
				//提交表单
				$("#deleteForm").submit();
			})
		}
		//加载完之后显示执行状态
		$(document).ready(function(){
			var result='<%=request.getAttribute("result")%>';
				if(result != null && result != "null"){
				if (result.indexOf("成功")>0) {
				
				swal({
					title : result,
					type : "success"
				});
			}else{
				swal({
					title :result,
					type : "error"
				});
			}
			}
		});
	</script>

	<script type="text/javascript">
		/*添加保险界面的表单验证*/
		$('#btn_addInsure').click(function() {
			$('#saveInsureForm').bootstrapValidator({
				feedbackIcons : {/*输入框不同状态，显示图片的样式*/
					valid : 'glyphicon glyphicon-ok',
					invalid : 'glyphicon glyphicon-remove',
					validating : 'glyphicon glyphicon-refresh'
				},
				/*生效规则：字段值一旦变化就触发验证*/
				live : 'enabled',
				/*当表单验证不通过时，该按钮为disabled*/
				submitButtons : 'button[type="submit"]',
				/*验证*/
				fields : {
					master : {/*键名username和input name值对应*/
						message : 'The username is not valid',
						validators : {
							notEmpty : {/*非空提示*/
								message : '车主或车牌号不能为空'
							}
						}
					},
					type : {
						validators : {
							notEmpty : {
								message : '保险类型不能为空'
							}
						}
					},
					coverage : {
						validators : {
							notEmpty : {
								message : '保险额度不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					payAmount : {
						validators : {
							notEmpty : {
								message : '缴费金额不能为空'
							},
							numeric : {
								message : '请输入数字'
							}
						}
					},
					validityStart : {
						validators : {
							notEmpty : {
								message : '有效期起不能为空'
							}
						}
					},
					validityEnd : {
						validators : {
							notEmpty : {
								message : '有效期止不能为空'
							}
						}
					},
					payTime : {
						validators : {
							notEmpty : {
								message : '缴费日期不能为空'
							}
						}
					}
				}
			});
		});
		/*日历刷新验证*/
		function refreshValidator(id,name){
		$(id).data('bootstrapValidator').updateStatus(name,
		'NOT_VALIDATED',null).validateField(name);
		}		
	</script>
</body>

</html>