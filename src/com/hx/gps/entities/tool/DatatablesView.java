package com.hx.gps.entities.tool;

import java.io.Serializable;
import java.util.List;
/**
 * 
 * @ClassName: DatatablesView.java
 * @Description: 分页展示的帮助类
 *
 * @version: v1.0.0
 * @author: AN
 * @date: 2018年5月4日 下午7:10:27 
 * 
 */
public class DatatablesView<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	//1.数据
	private List<T> data; //data 与datatales 加载的“dataSrc"对应  
	//2.总记录数
    private int recordsTotal;   
    //3.过滤的条数（不清楚什么作用）
    private int recordsFiltered;  
    //4.拉取（请求）的次数
    private int draw;

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public int getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public int getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	@Override
	public String toString() {
		return "DatatablesView [data=" + data + ", recordsTotal=" + recordsTotal + ", recordsFiltered="
				+ recordsFiltered + ", draw=" + draw + "]";
	}
    
    
}
