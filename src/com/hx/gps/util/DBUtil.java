package com.hx.gps.util;
import com.mongodb.*;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DBUtil {
	//连接 
	public static MongoClient mg=null;  
	public static DB db=null;     
	public static DBCollection collection;  
	public static DBCollection getDBCollection(String dbName,String colName){
		if(mg==null){
			try {
//			 	  mg=new MongoClient("222.91.146.150",10005);
			 	  //mg=new MongoClient(FarmConstant.MONGOIP,60006);
				mg=new MongoClient(FarmConstant.MONGOIP,FarmConstant.MONGOPORT);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(db==null){
			db=mg.getDB(dbName);
		}
		return db.getCollection(colName);
	}

	public static void main(String[] args) throws Exception{
//		DBCollection addressHistory2011 = DBUtil.getDBCollection(FarmConstant.MONGODB, "addressHistory2011");
//		DBCursor dbObjects = addressHistory2011.find();
//		System.out.println(dbObjects.toArray());
//		Timestamp timestamp = Timestamp.valueOf("Sun Nov 01 21:01:00 CST 2020");
		//Date date = new Date("Sun Nov 01 21:01:00 CST 2020");

//		Timestamp timestamp = Timestamp.valueOf(date.toString());
//		System.out.println(timestamp);
		DBCollection coll = DBUtil.getDBCollection(FarmConstant.MONGODB, FarmConstant.ALARM);
		BasicDBObject finds = new BasicDBObject();
		ArrayList<String> list1 = new ArrayList<>();
		list1.add("紧急报警");
		list1.add("磁盘已满");
		list1.add("进出区域");
		finds.put("dealState", "未处理");
		// finds.put("alarmType",new BasicDBObject("$ne","终端主电源欠压"));
		finds.put("alarmType", new BasicDBObject("$in", list1));


//		finds.put("time", new BasicDBObject("$lte", DateUtil.stringToDate(endDate.toString())).append("$gte",
//				DateUtil.stringToDate(startDate.toString())));

		DBCursor cursor = coll.find(finds);
	}
}
