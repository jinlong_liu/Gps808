<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>H+ 后台主题UI框架 - 百度ECHarts</title>
<meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
<meta name="description"
	content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/style.min.css?v=4.0.0" rel="stylesheet">
<link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
<link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="css/plugins/jsTree/style.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">

<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">


<!-- Data Tables -->
<link href="css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">






<!-- <link rel="shortcut icon" href="favicon.ico"> -->
<!-- <link href="css/style.min.css?v=4.0.0" rel="stylesheet"> -->
<!-- <link href="css/bootstrap.min.css?v=3.3.5" rel="stylesheet"> -->
<!-- <link href="css/font-awesome.min.css?v=4.4.0" rel="stylesheet"> -->
<!-- <link href="css/plugins/jsTree/style.min.css" rel="stylesheet"> -->
<!-- <link href="css/animate.min.css" rel="stylesheet"> -->
<base target="_blank">

<style>
.jstree-open>.jstree-anchor>.fa-folder:before {
	content: "\f07c"
}

.jstree-default .jstree-icon.none {
	width: 0
}
</style>

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-9">
				<div class="form-group">
				<div class="ibox float-e-margins">
					<div class="ibox-title" style="height:60px;line-height:30px">
						查看里程利用率
					 
					</div>
						<div  class="ibox-content  text-center" style="height:770px">
						 <div class="pull-right" >
					<form action="" class="form-inline">
					
						开始时间：
							 <input type="text" name="startDate" id="startDate" class="form-control"
							value="${meterStartDate }" class="Wdate"
							onfocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm:ss'})">
					
					
							结束时间：
							 <input type="text" name="endDate" id="endDate" class="form-control"
							  value="${meterEndDate }" class="Wdate"  
							onfocus="WdatePicker({lang:'zh-cn',minDate:'#F{$dp.$D(\'startDate\')}',dateFmt:'yyyy-MM-dd HH:mm:ss'})">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						
							
								<input id="getOneBtn"  type="button" value="查看" class="btn btn-primary ">
						
							</form>
						</div>
						<div id="miletable" >
							<table  width="1000px" height="500px"  class="footable table table-stripped" data-page-size="6"
						data-filter=#filter  >
						<tbody>
								<tr>
									<td  colspan="2" align="center"><h3>里程利用率报表</h3></td>
								</tr>
								
								<tr >
									<td align="left">总营运里程</td>
									<td ><div id="totalMileage"></div></td>
								</tr>
								<tr  >	
									<td align="left" >总载客里程</td>
									<td  ><div id="mileageUtil"></div></td>
								</tr>
								<tr  >
									<td align="left">单车日均营运里程</td>
									<td><div id="avgmileage1"></div></td>
								</tr>
								<tr >
									<td align="left">单车日均载客里程</td>
									<td><div id="avgmileage2"></div></td>
								</tr>
								<tr>
									<td align="left" >里程利用率</td>
									<td ><div id="rate"></div></td>
								</tr>
 
 
								<tr >
									<td colspan="2" align="right"><div id="taxiNumber"></div></td>
 
 
								</tr>
								</tbody>
								<tfoot>
							<tr>
								<td colspan="2">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
							</table>
						</div>
					</div>
				</div>
				</div>
			</div>
			<!--右侧公司选择栏开始  -->
			<div class="col-sm-3">
				<div class="ibox float-e-margins">
					<div class="ibox-content">
					<div class="ibox-title">
					<h5>公司选择</h5>
					</div>
					<br>
						<input type="hidden" id="show1">
							<div id="jstree1" class="trdemo"
							style=" overflow-x: auto; overflow-y: auto; height: 724px;">
							<ul>
								<li class="jstree-open">所有公司
									<ul>
										<c:forEach items="${comInfo2s}" var="comInfo2s">
											<li id="${comInfo2s.comId}">${comInfo2s.cname }<%-- <ul>
													<c:forEach items="${comInfo2s.taxis}" var="taxis">
														<li data-jstree='{"type":"car"}'>
															${taxis.taxiNum }
														</li>
													</c:forEach>
												</ul> --%>
											</li>
										</c:forEach>
									</ul>
								</li>
							</ul>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
   <script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script>
	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
	<script src="js/jquery.min.js?v=2.1.4"></script>
	<script src="js/bootstrap.min.js?v=3.3.5"></script>
	<script src="js/plugins/jeditable/jquery.jeditable.js"></script>
	<script src="js/plugins/dataTables/jquery.dataTables.js"></script>
	<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
	
	<script src="js/plugins/echarts/echarts-all.js"></script>
	<script src="js/content.min.js?v=1.0.0"></script>
	<script src="js/demo/echarts-demo2.min.js"></script>
	<script src="js/plugins/jsTree/jstree.min.js"></script>	
	
	
	
	
	
<!-- 	<script src="js/jquery.min.js?v=2.1.4"></script> -->
<!-- 	<script src="js/bootstrap.min.js?v=3.3.5"></script> -->
<!-- 	<script src="js/plugins/echarts/echarts-all.js"></script> -->
<!-- 	<script src="js/content.min.js?v=1.0.0"></script> -->
<!-- 	<script src="js/demo/echarts-demo2.min.js"></script> -->
<!-- 	<script src="js/plugins/jsTree/jstree.min.js"></script> -->
<!-- 	<script type="text/javascript" src="js/My97DatePicker/WdatePicker.js"></script> -->
	<script type="text/javascript"
		src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>	
	<script type="text/javascript">
	$("#getOneBtn").click(function() {
			var startDate = $("#startDate").val();
		 	var endDate =  $("#endDate").val();
		 var companys=$("#show1").val();
		 
	          if (startDate == "") {
				swal({
					title : "开始时间不能为空",
					type : "warning",
				});
				
			}else if (endDate == "") {
				swal({
					title : "结束时间不能为空",
					type : "warning",
				});
				
			}else if (companys == "") {
				swal({
					title : "请选择公司",
					type : "warning",
				});
			
			}else{
			      check();
			
			}
	});
		//开始查询数据
		function check(){
			var companys=$("#show1").val();
			var startDate=$("#startDate").val();
			var endDate=$("#endDate").val();
			//alert(type+companys);
			
			if(companys!=""){
				var args = {
					"companys" : companys,
					"startDate" : startDate,
					"endDate" : endDate
				};
				var url = "mileageUtil";
				$.post(url,args,function(data){
					var newdata = eval("(" + data + ")");
					//alert(data);
					document.getElementById('totalMileage').innerHTML=(newdata.totalMileage).toFixed(2)+' Km';
					document.getElementById('mileageUtil').innerHTML=(newdata.mileageUtil).toFixed(2)+' Km';
					document.getElementById('avgmileage1').innerHTML=(newdata.totalMileage/newdata.taxiNumber/newdata.dayNumber).toFixed(2)+' Km';	//单车日均营运
					document.getElementById('avgmileage2').innerHTML=(newdata.mileageUtil/newdata.taxiNumber/newdata.dayNumber).toFixed(2)+' Km';	//单车日均载客
					document.getElementById('rate').innerHTML=newdata.rate+"%";
					document.getElementById('taxiNumber').innerHTML="该表统计了"+newdata.taxiNumber+"辆汽车在"+newdata.dayNumber+"天内的行程记录";
					//$("#totalMileage").val(newdata.totalMileage);
				});
				$("#miletable").show();
			}
			
			
		}
		
  		//选择公司开始
		$(document).ready(function() {
			$("#miletable").hide();
			$("#show1").val("");
			
			$("#jstree1").jstree({
				"core" : {
					"check_callback" : true
				},
				"plugins" : [ "types", "dnd", "checkbox" ],
				"types" : {
					"car" : {
						"icon" : "fa fa-car"
					},
					"default" : {
						"icon" : "fa fa-folder"
					}
				}
			});
			
			//获取选择节点的id
			$("#jstree1").on('changed.jstree', function(e, data) {
				r = [];
				var i, j;
				for (i = 0, j = data.selected.length; i < j; i++) {
					var node = data.instance.get_node(data.selected[i]);
					if (data.instance.is_leaf(node)) {
						r.push(node.id);
					}
				}
				$("#show1").val(r);
			});
		});
	</script>
</body>

</html>