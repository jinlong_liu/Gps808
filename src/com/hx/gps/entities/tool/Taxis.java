package com.hx.gps.entities.tool;

import java.util.List;

import com.hx.gps.entities.Taxi;

/**
 * @author 作者 dream:
 * @version 创建时间：2018年7月26日 下午11:49:31 天道丶放飞梦想
 */
public class Taxis {

	private List<Taxi> taxis;

	public List<Taxi> getTaxis() {
		return taxis;
	}

	public void setTaxis(List<Taxi> taxis) {
		this.taxis = taxis;
	}

	public Taxis(List<Taxi> taxis) {
		super();
		this.taxis = taxis;
	}

	public Taxis() {
	}

}
