package com.hx.gps.entities;

/**
 * RolePower entity. @author MyEclipse Persistence Tools
	
	20170318 fmy 这个实体类已废弃,角色和权限的关联关系由hibernate自动维护,并由角色一方维护
 */
/*@Entity
@Table(name = "role_power" )*/
public class RolePower implements java.io.Serializable {
/*
	// Fields

	private Integer id;
	private Integer roleId;
	private Integer powerId;

	// Constructors

	*//** default constructor *//*
	public RolePower() {
	}

	*//** full constructor *//*
	public RolePower(Integer roleId, Integer powerId) {
		this.roleId = roleId;
		this.powerId = powerId;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "roleId")
	public Integer getRoleId() {
		return this.roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	@Column(name = "powerId")
	public Integer getPowerId() {
		return this.powerId;
	}

	public void setPowerId(Integer powerId) {
		this.powerId = powerId;
	}
*/
}